<?php //print_r($_SESSION);die; ?>

<?php //print_r($errors); ?>
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>JobYoda</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <meta name="author" content="CreativeLayers">
      <!-- Styles -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap-grid.css" />
      <link rel="icon" href="<?php echo base_url(); ?>recruiterfiles/images/fav.png" type="image/png" sizes="16x16">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/icons.css">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/animate.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/all.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/fontawesome.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/responsive.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/chosen.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/colors/colors.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/style.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/jquery-ui.min.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/jquery-ui.theme.min.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/jquery.timepicker.min.css" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
   </head>
   <style>
      a:hover {
      text-decoration: none;
      }
      .tech i.fa.fa-ellipsis-h {
      float: right;
      }
      .progr span.progress-bar-tooltip {
      position: initial;
      color: #fff;
      }
      .filterchekers ul {
      width: 100%;
      }
      .manage-jobs-sec {
      float: none;
      width: 100%;
      margin: 0 auto;
      }
      .link a {
      background: #26ae61;
      color: #fff;
      word-break: keep-all;
      font-size: 11px;
      line-height: 0;
      padding: 10px;
      }
      .link {
      float: left;
      width: 100px;
      }
      .btn-extars{
      display:none;}
      .inner-header {
      float: left;
      width: 100%;
      position: relative;
      padding-top: 60px; padding-bottom: 15px;
      z-index: 0;
      }
      .tree_widget-sec > ul > li.inner-child.active > a {
      color: #26ae61;
      }
      .tree_widget-sec > ul > li.inner-child.active > a i {
      color: #26ae61;}
      .contact-edit .srch-lctn:hover {
      background: #26ae61;
      color: #ffffff;
      border-color: #26ae61;
      }
      .contact-edit .srch-lctn {
      color: #26ae61;
      border: 2px solid #26ae61;
      border: 2px solid #26ae61;
      }
      .contact-edit > form button{
      border: 2px solid #26ae61;}
      .profile-form-edit > form button:hover, .contact-edit > form button:hover {
      background: #26ae61;
      color: #ffffff;
      }
      .step.active i {
      background: #26ae61;
      border-color: #26ae61;
      color: #ffffff;
      }
      .step i{
      color: #26ae61;}
      .step.active span {
      color: #000;
      }
      .menu-sec nav > ul > li.menu-item-has-children > a::before{
      display:none;}
      .inner-header {
      float: left;
      width: 100%;
      position: relative;
      padding-top: 60px; padding-bottom: 15px;
      z-index: 0;
      }
      .manage-jobs-sec > table thead tr td{
      color: #26ae61;}
      .extra-job-info > span i {
      float: left;
      font-size: 30px;
      color: #26ae61;
      width: 30px;
      margin-right: 12px;
      }
      .action_job > li span {
      background: #26ae61;}
      .action_job > li span::before{
      background: #26ae61;}
      .action_job > li {
      float: left;
      margin: 0;
      position: relative;
      width: 15px;
      }
      .manage-jobs-sec > h3 {
      padding-left: 0px; 
      margin-top: 40px;
      text-align: center;
      }
      .fall {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 23px 10px;
      }
      .progress-bar{
      background-color: #47b476;
      }
      .details {
      float: left;
      width: 100%;
      padding: 5px 17px;
      background-color: #47b476;
      color: #fff;
      margin: 15px;
      border-radius: 20px
      }
      .leftdetails {
      float: left;
      width: 100%;
      }
      .left {
      float: left;
      width: 100%;
      margin: 0px;
      padding: 0px;
      }
      .detail span {
      display: block;
      float: right;
      margin: 12px 8px;  
      border: 1px solid#ddd;
      padding: 10px;
      }
      .detail strong {
      float: left;
      width: 100%;
      margin: 0px;
      padding: 0px 55px;
      font-weight: 700;
      font-size: 20px;
      }
      .detail, .detai {
      float: left;
      width: 100%;
      }
      .detai span {
      display: -webkit-box;
      width: 100%;
      font-weight: 600;
      margin: 0px;
      padding: 12px 0px;
      font-size: 20px;
      }
      .progras {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 10px;
      }
      .detai strong {
      float: right;
      font-size: 20px;
      }
      .detail p, .detai p {
      font-weight: 600;
      font-size: 16px;
      color: #000;
      }
      .detail .progress span {
      position: absolute;
      right: 0px;
      font-size: 13px;
      color: #fff;
      border: 0px solid#ddd;
      }
      .head {
      float: left;
      width: 100%;
      padding: 10px;
      background-color: #47b476;
      color: #fff;
      margin-top: 10px;
      margin-bottom: 10px;
      }
      .num {
      float: left;
      margin: 20px 0px;
      }
      .para {
      float: left;
      width: 100%;
      border: 1px solid#dddd;
      padding: 10px;
      background-color: #47b476;
      color: #fff;
      margin-top: 10px;
      margin-bottom: 10px;
      }
      .tech {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 10px;
      margin: 10px 0px;
      }
      .search-container {
      float: left;
      }
      .search button {
      padding: 7px 4px;
      color: #fff;
      background-color: #47b476;
      border: none;
      width: 36%;
      }
      .upload a {
      color: #ffffff;
      text-decoration: none;
      background: #47b476;
      padding: 10px;
      border-radius: 50px;
      }
      .search input[type="text"] {
      padding: 9px 6px;
      width: 63%;
      background-color: #fff;
      border: 1px solid#ddd;
      }
      .details i.fa.fa-filter {
      float: right;
      margin: 5px 0px;
      padding: 0px;
      }
      .details p {
      float: left;
      margin: 0px;
      padding: 0px;
      color: #fff;
      }
      .head p {
      color: #fff;
      }
      .para p {
      color: #fff;
      }
      .upload {
      float: right;
      padding: 8px;
      margin: px;
      }
      border: 1px solid#ddd;
      padding: 10px;
      }
      .dot {
      float: right;
      }
      .progr {
      float: left;
      width: 100%;
      margin-bottom: 18px;
      }
      .progr .progress {
      position: absolute;
      left: 9%;
      height: 20px;
      -webkit-border-radius: 8px;
      -moz-border-radius: 8px;
      -ms-border-radius: 8px;
      -o-border-radius: 8px;
      border-radius: 8px;
      top: initial;
      width: 80%;
      }
      .progr .progress-label {
      margin: 12px 0px;
      }
      .progress-bar.progress-bar-primary {
      padding: 0px;
      }
      .texts p {
      position: absolute;
      top: 70%;
      left: 29%;
      }
      p.dropdown-toggle {
      font-size: 34px;
      margin: -30px;
      }
      .dot {
      float: right;
      }
      .texts {
      margin-top: 20px;
      }
      .text p {
      margin: 0px;
      padding: 0px;
      font-size: 13px;
      }
      .right .progress{
      width: 150px;
      height: 150px;
      line-height: 150px;
      background: none;
      margin: 0 auto;
      box-shadow: none;
      position: relative;
      }
      .right .progress:after{
      content: "";
      width: 100%;
      height: 100%;
      border-radius: 50%;
      border: 2px solid #fff;
      position: absolute;
      top: 0;
      left: 0;
      }
      .right .progress > span{
      width: 50%;
      height: 100%;
      overflow: hidden;
      position: absolute;
      top: 0;
      z-index: 1;
      }
      .progress .progress-left{
      left: 0;
      }
      .right .progress .progress-bar{
      width: 100%;
      height: 100%;
      background: none;
      border-width: 2px;
      border-style: solid;
      position: absolute;
      top: 0;
      }
      .right .progress .progress-left .progress-bar{
      left: 100%;
      border-top-right-radius: 80px;
      border-bottom-right-radius: 80px; 
      border-left: 0;
      -webkit-transform-origin: center left;
      transform-origin: center left;
      }
      .right .progress .progress-right{
      right: 0;
      }
      .right .progress .progress-right .progress-bar{
      left: -100%;
      border-top-left-radius: 80px;
      border-bottom-left-radius: 80px;
      border-right: 0;
      -webkit-transform-origin: center right;
      transform-origin: center right;
      animation: loading-1 1.8s linear forwards;
      }
      .right .progress .progress-value{
      width: 59%;
      height: 59%;
      border-radius: 50%;
      border: 2px solid #ebebeb;
      font-size: 28px;
      line-height: 82px;
      text-align: center;
      position: absolute;
      top: 7.5%;
      left: 7.5%;
      }
      .right .progress.blue .progress-bar{
      border-color: #049dff;
      }
      .right .progress.blue .progress-value{
      color: #47b476;
      }
      .right .progress.blue .progress-left .progress-bar{
      animation: loading-2 1.5s linear forwards 1.8s;
      }
      .progress.yellow .progress-bar{
      border-color: #fdba04;
      }
      .right .progress.yellow .progress-value{
      color: #fdba04;
      }
      .right .progress.yellow .progress-left .progress-bar{
      animation: loading-3 1s linear forwards 1.8s;
      }
      .right .progress.pink .progress-bar{
      border-color: #ed687c;
      }
      .right .progress.pink .progress-value{
      color: #ed687c;
      }
      .right .progress.pink .progress-left .progress-bar{
      animation: loading-4 0.4s linear forwards 1.8s;
      }
      .right .progress.green .progress-bar{
      border-color: #1abc9c;
      }
      .right .progress.green .progress-value{
      color: #1abc9c;
      }
      .right .progress.green .progress-left .progress-bar{
      animation: loading-5 1.2s linear forwards 1.8s;
      }
      @keyframes loading-1{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(180deg);
      transform: rotate(180deg);
      }
      }
      @keyframes loading-2{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(144deg);
      transform: rotate(144deg);
      }
      }
      @keyframes loading-3{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(90deg);
      transform: rotate(90deg);
      }
      }
      @keyframes loading-4{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(36deg);
      transform: rotate(36deg);
      }
      }
      @keyframes loading-5{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(126deg);
      transform: rotate(126deg);
      }
      }
      @media only screen and (max-width: 990px){
      .right .progress{ margin-bottom: 20px; }
      }
      .validError{
      color:#f00;font-size: 12px;
      margin: 0;
      float: left;
      }
      .insertMsg {
      text-align: center;
      color: #27aa60;
      font-size: 18px;
      }
      .filldetails label{width:100%;}
      .form-control.locselect{padding:0px!important;}
      .filterchekers input[type="checkbox"]{
      position: absolute;
      opacity: 0;
      z-index: auto;
      margin: 0;
      width: 90px;
      height: 55px;
      }
      table#myTable1 {
      width: 100%;
      }
      table#myTable td {
      padding: 0 9px 0 0;
      }
      a.remove i.fa.fa-trash {
      color: #fff;
      }
      input.form-control {
      margin: 20px 0px;
      }
      a.remove {
      padding: 3px 4px;
      text-align: center;
      background: #26ae61;
      border-radius: 100px;
      margin-top: 10px;
      float: left;
      color: #fff;
      border-radius: 0pc;
      font-size: 10px;
      }
      input.form-control.addinput {
      width: 80% !important;
      float: left;
      margin-right: 33px !important;
      }
      }
      @media only screen and (max-width: 767px){
      .psthbs .filterchekers ul {
      display: inline-block;
      width: 100%;
      }
      }  
   </style>
   <body>
      <div class="theme-layout" id="scrollup">
         <header class="stick-top nohdascr">
            <div class="menu-sec">
               <div class="container-fluid dasboardareas">
                  
                  <!-- Logo -->
                  <?php include_once('headermenu.php');?>
                  <!-- Menus -->
               </div>
            </div>
         </header>
         <section>
            <div class="block no-padding">
               <div class="container-fluid dasboardareas">
                  <div class="row">
                     <aside class="col-md-3 col-lg-2 column border-right">
                        <div class="widget">
                           <div class="menuinside">
                              <?php include_once('sidebar.php'); ?>
                           </div>
                        </div>
                     </aside>
                     <div class="col-md-9 col-lg-10">
                        <div class="row">
                           <div class="col-md-12 psthbs">
                              <div class="companyprofileforms fullwidthform">
                                 <?php if($this->session->tempdata('inserted')) {?>
                                 <p class="insertMsg"><?php echo $this->session->tempdata('inserted'); ?></p>
                                 <?php } ?>
                                 <?php if($this->session->tempdata('postError')) {?>
                                 <p class="errorMsg"><?php echo $this->session->tempdata('postError'); ?></p>
                                 <?php } ?>
                                 <form id="jobpostid" method="post" action="<?php echo base_url();?>recruiter/jobpost/jobpostInsert" enctype="multipart/form-data">
                                    <div class="myhdainside">
                                       <h6>Post Job</h6>
                                    </div>
                                    <div class="filldetails">
                                       <div class="stepcountfrms">
                                          <div class="headsteps-gt">
                                             <h5>Step 1</h5>
                                             <img src="<?php echo base_url().'recruiterfiles/';?>images/fav.png">
                                          </div>
                                          <div class="dividehalfd">
                                             <div class="row">
                                                <div class="col-md-12 stepsinfosd">
                                                   <p>Let's get the general details locked for this post</p>
                                                </div>
                                                <div class="col-md-6"> 
                                                   <label>What is the Job Title?</label>
                                                </div>
                                                <div class="col-md-6">
                                                   <input type="text" class="form-control" name="jobTitle" placeholder="Job Title" value="<?php if(!empty($jobdata['jobTitle'])){ echo $jobdata['jobTitle'];}?>">
                                                   <?php if(!empty($errors['jobTitle'])){echo "<p class='validError'>".$errors['jobTitle']."</p>";}?>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label>Which site is the opening for (Target)?</label>
                                                </div>
                                                <div class="col-md-6">
                                                   <select class="locselect" name="jobLoc" id="jobLoc">
                                                      <option value=""> Select Location </option>
                                                      <?php
                                                         if($addresses) {
                                                            //print_r($addresses);die;
                                                             foreach($addresses as $address) {
                                                         ?>
                                                      <option value="<?php echo $address['recruiter_id']; ?>" > <?php echo $address['cname']; ?> </option>
                                                      <?php
                                                         }
                                                         }
                                                         ?>
                                                   </select>
                                                   <?php if(!empty($errors['jobLoc'])){ echo "<p class='validError'>".$errors['jobLoc']."</p>";} ?>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label>How many people do you need for this job?</label>
                                                </div>
                                                <div class="col-md-6">
                                                   <input type="number" class="form-control" name="opening" placeholder="No. of Openings" min="1" value="<?php if(!empty($jobdata['opening'])){ echo $jobdata['opening'];}?>">
                                                   <?php if(!empty($errors['opening'])){echo "<p class='validError'>".$errors['opening']."</p>";}?>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label>When do you want us take down this post?</label>
                                                </div>
                                                <div class="col-md-6">
                                                   <input type="text" class="form-control" id="jobExpireid" name="jobExpire" placeholder="Expire Date" value="<?php if(!empty($jobdata['jobExpire'])){ echo $jobdata['jobExpire'];}?>">
                                                   <?php if(!empty($errors['jobExpire'])){echo "<p class='validError'>".$errors['jobExpire']."</p>";}?>
                                                   <?php $userSession = $this->session->userdata('userSession');
                                                   if(!empty($userSession['label']) && $userSession['label']=='3'){
                                                            $userSession['id'] = $userSession['parent_id'];
                                                        }else{
                                                            $userSession['id'] = $userSession['id'];
                                                        }
                                                    ?>
                                                   <input type="hidden" name="recruId" value="<?php echo $userSession['id']; ?>">
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label>What is the Job level for this post?</label>
                                                </div>
                                                <div class="col-md-6">
                                                   <select name="level" class="halfsideth">
                                                      <option value=""> Select Level </option>
                                                      <?php
                                                         if($levels) {
                                                             foreach($levels as $level) {
                                                         ?>
                                                      <option <?php if(!empty($jobdata['level']) && $jobdata['level']==$level['id']){ echo "selected"; }?>  value="<?php echo $level['id']; ?>" > <?php echo $level['level']; ?> </option>
                                                      <?php
                                                         }
                                                         }
                                                         ?>
                                                   </select>
                                                   <label class="switch" style="width:66px;">
                                                   <input type="checkbox" name="level_status" checked>
                                                   <span class="slider"></span>
                                                   </label>
                                                   <?php if(!empty($errors['level'])){echo "<p class='validError'>".$errors['level']."</p>";}?>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label>Required level of education?</label>
                                                </div>
                                                <div class="col-md-6">
                                                   <select name="education" class="halfsideth">
                                                      <option value=""> Select Education Level </option>
                                                      <option value="No Requirement" <?php if(!empty($jobdata['education'])) { if($jobdata['education'] == 'No Requirement'){  echo "selected"; }}?>>No Requirement</option>
                                                      <option value="Vocational" <?php if(!empty($jobdata['education'])){ if($jobdata['education'] == 'Vocational'){ echo "selected"; }}?>>Vocational</option>
                                                      <option value="High School Graduate" <?php if(!empty($jobdata['education'])){ $jobdata['education'] == 'High School Graduate' ? ' selected="selected"' : '';}?>>High School Graduate</option>
                                                      
                                                      <option value="Undergraduate" <?php if(!empty($jobdata['education'])){ if($jobdata['education'] == 'Undergraduate'){ echo  "selected"; }}?>>Undergraduate</option>
                                                      <option value="Associate Degree" <?php if(!empty($jobdata['education'])){ if($jobdata['education'] == 'Associate Degree'){ echo "selected"; }}?>>Associate Degree</option>
                                                      <option value="College Graduate" <?php if(!empty($jobdata['education'])){ if($jobdata['education'] == 'College Graduate'){ echo "selected"; }}?>>College Graduate</option>
                                                      <option value="Post Graduate" <?php if(!empty($jobdata['education'])){ if($jobdata['education'] == 'Post Graduate'){ echo "selected"; }}?>>Post Graduate</option>
                                                   </select>
                                                   <label class="switch" style="width:66px;">
                                                   <input type="checkbox" name="education_status" checked>
                                                   <span class="slider"></span>
                                                   </label>
                                                   <?php if(!empty($errors['education'])){echo "<p class='validError'>".$errors['education']."</p>";}?>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label>Certification Required? </label>
                                                </div>
                                                <div class="col-md-6">
                                                   <input type="text" class="form-control" name="certification"  placeholder="Enter Certifications" value="<?php if(!empty($jobdata['certification'])){ echo $jobdata['certification'];}?>">
                                                   <?php if(!empty($errors['certification'])){echo "<p class='validError'>".$errors['certification']."</p>";}?>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label>Required Level of Experience?</label>
                                                </div>
                                                <div class="col-md-6">
                                                   <select name="experience" class="halfsideth">
                                                      <option value=""> Select Experience </option>
                                                      <option value="No Experience" <?php if(!empty($jobdata['experience'])){ if($jobdata['experience'] == 'No Experience'){ echo  "selected"; }}?>>No Experience</option>
                                                      <option value="All Tenure" <?php if(!empty($jobdata['experience'])){ if($jobdata['experience'] == 'All Tenure'){ echo  "selected"; }}?>>All Tenure</option>

                                                      <option value="less than 6 months" <?php if(!empty($jobdata['experience'])){ if($jobdata['experience'] == 'less than 6 months'){ echo "selected"; }}?>>Less than 6 months</option>
                                                      
                                                      <option value="6mo to 1 yr" <?php if(!empty($jobdata['experience'])){ if(strcmp($jobdata['experience'], "6mo to 1 yr")){ "selected"; }}?>>6mo to 1 yr</option>
                                                      <option value="1 yr to 2 yr" <?php if(!empty($jobdata['experience'])){ if($jobdata['experience'] == '1 yr to 2 yr'){ echo "selected"; }}?>>1 yr to 2 yr</option>
                                                      <option value="2yr to 3 yr" <?php if(!empty($jobdata['experience'])){ if($jobdata['experience'] == '2yr to 3 yr'){ echo "selected"; }}?>>2yr to 3 yr</option>
                                                      <option value="3yr and up" <?php if(!empty($jobdata['experience'])){ if($jobdata['experience'] == '3yr and up'){ echo "selected"; }}?>>3yr and up</option>
                                                   </select>
                                                   <label class="switch" style="width:66px;">
                                                   <input type="checkbox" name="experience_status" checked>
                                                   <span class="slider"></span>
                                                   </label>
                                                   <?php if(!empty($errors['experience'])){echo "<p class='validError'>".$errors['experience']."</p>";}?>
                                                </div>
                                             </div>
                                             <div class="">
                                                <label>Offers can vary depending on experience, you might want to put an amount based on experience range</label>
                                                <table id="myTable">
                                                   <tbody>
                                                      <tr>
                                                         <td>
                                                            <select name="expRange[]" required="">
                                                               <option value="">Select Experience</option>
                                                               <option value="All Tenure" <?php if(!empty($jobdata['education'])){ $jobdata['expRange'] == 'All Tenure' ? ' selected="selected"' : '';}?>>All Tenure</option>
                                                               <option value="less than 6 months" <?php if(!empty($jobdata['education'])){ $jobdata['expRange'] == 'less than 6 months' ? ' selected="selected"' : '';}?>>less than 6 months</option>
                                                               <option value="6mo to 1 yr" <?php if(!empty($jobdata['education'])){ $jobdata['expRange'] == '6mo to 1 yr' ? ' selected="selected"' : '';}?>>6mo to 1 yr</option>
                                                               <option value="1 yr to 2 yr" <?php if(!empty($jobdata['education'])){ $jobdata['expRange'] == '1 yr to 2 yr' ? ' selected="selected"' : '';}?>>1 yr to 2 yr</option>
                                                               <option value="2yr to 3 yr" <?php if(!empty($jobdata['education'])){ $jobdata['expRange'] == '2yr to 3 yr' ? ' selected="selected"' : '';}?>>2yr to 3 yr</option>
                                                               <option value="3yr and up" <?php if(!empty($jobdata['education'])){ $jobdata['expRange'] == '3yr and up' ? ' selected="selected"' : '';}?>>3yr and up</option>
                                                            </select>
                                                         </td>
                                                         <td><input name="expBasicSalary[]" type="number" min="1" class="form-control" placeholder="Enter Basic Salary per Month" required=""></td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                                <p onclick="myFunction()" class="addmre">Add More Salary Range</p>
                                             </div>
                                             <div style="clear:both;"></div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label>How much is the Total Guaranteed Allowance? </label>
                                                </div>
                                                <div class="col-md-6">
                                                   <input type="number" class="form-control" name="allowance" min='0' placeholder="Enter Total Guaranteed Allowance" value="<?php if(!empty($jobdata['allowance'])){ echo $jobdata['allowance'];}?>">
                                                   <?php if(!empty($errors['allowance'])){echo "<p class='validError'>".$errors['allowance']."</p>";}?>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label>How much is the Joining Bonus? </label>
                                                   <input style="opacity: 1;float: right;position: absolute;top: 16%;z-index: 0;left: 70%;" type="checkbox" name="bonuscheck" id="bonuscheck" onclick="checkstatus()" <?php if(!empty($jobdata['bonuscheck'])){ echo "checked";}?>>
                                                </div>
                                                <div class="col-md-6" id="bonusdiv" <?php if(!empty($jobdata['bonuscheck'])){?> style="visibility:visible;opacity:1;" <?php } else { ?> style="visibility:hidden;opacity:0;" <?php }?> >
                                                   <input type="number" class="form-control" id="bonus_amount" name="bonus_amount" placeholder="Enter Joining Bonus" value="<?php if(!empty($jobdata['bonus_amount'])){ echo $jobdata['bonus_amount'];}?>">
                                                   <p class='validError customerror'></p>
                                                </div>
                                             </div>
                                             
                                          </div>
                                       </div>
                                    </div>
                                    <div class="filldetails">
                                       <div class="stepcountfrms">
                                          <div class="headsteps-gt">
                                             <h5>Step 2</h5>
                                             <img src="<?php echo base_url().'recruiterfiles/';?>images/fav.png">
                                          </div>
                                          <div class="dividehalfd">
                                             <div class="row">
                                                <div class="col-md-12 stepsinfosd">
                                                   <p>To help narrow down the talents, let's categorize this post.</p>
                                                </div>
                                                <div class="col-md-6">
                                                   <label>Please choose the best category for the job.</label>
                                                </div>
                                                <div class="col-md-6">
                                                   <select name="category" id="category">
                                                      <option value=""> Select Category </option>
                                                      <?php
                                                         if($category) {
                                                             foreach($category as $categorys) {
                                                         ?>
                                                      <option  value="<?php echo $categorys['id']; ?>" <?php if(!empty($jobdata['category'])){ if($jobdata['category'] == $categorys['id']){ echo "selected";}}?>> <?php echo $categorys['category']; ?> </option>
                                                      <?php
                                                         }
                                                         }
                                                         ?>
                                                   </select>
                                                   <?php if(!empty($errors['category'])){ echo "<p class='validError'>".$errors['category']."</p>";} ?>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label>Let's put in some more detailed category.</label>
                                                </div>
                                                <div class="col-md-6">
                                                <select name="subcategory" id="subcategory">
                                                <?php 
                                                      if(!empty($jobdata['category'])){
                                                      foreach($subcategory as $sub) {
                                                ?>
                                                      <option value="<?php echo $sub['id'] ?>" <?php if($jobdata['subcategory']==$sub['id']){echo "selected";}?>> <?php echo $sub['subcategory'] ?> </option>
                                                <?php
                                                      }
                                                } else {
                                                ?>
                                                  
                                                      <option value="">Select SubCategory</option>
                                                   
                                                <?php
                                                }
                                                ?>
                                                </select>
                                                <?php if(!empty($errors['subcategory'])){ echo "<p class='validError'>".$errors['subcategory']."</p>";} ?>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label>Can you tell us about the language to be supported?</label>
                                                </div>
                                                <div class="col-md-6">
                                                   <select name="lang">
                                                      <option value="">Select Language</option>
                                                      <?php
                                                         foreach($langs as $lang) {
                                                         ?>
                                                      <option value="<?php echo $lang['id'];?>" <?php if(!empty($jobdata['lang'])){ if($jobdata['lang'] == $lang['id']) {echo "selected";}} ?>><?php echo $lang['name'];?></option>
                                                      <?php }?>
                                                   </select>
                                                   <?php if(!empty($errors['lang'])){ echo "<p class='validError'>".$errors['lang']."</p>";} ?>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label>If you need bilingual agents, please specify the other language.</label>
                                                </div>
                                                <div class="col-md-6">
                                                   <div class="autocomplete">
                                                      <input id="myInput" type="text" name="otherlanguage" placeholder="Select Languange" class="form-control">
                                                   </div>
                                                </div>
                                             </div>
                                             <!-- <div class="row">
                                                <div class="col-md-6">
                                                   <label>Type of Job</label>
                                                </div>
                                                <div class="col-md-6">
                                                   <select name="job_type" id="job_type">
                                                         <option value="">Select Type of Job</option>   
                                                         <option value="1">Direct</option>
                                                         <option value="2">Walk In</option>
                                                   </select>
                                                </div>
                                                </div>
                                                <div class="hidden_div">
                                                <div class="row">
                                                <div class="col-md-6">
                                                   <label>Walkin Date</label>
                                                </div>
                                                <div class="col-md-6">
                                                   <input type="date" class="form-control" name="walkin_date" value="<?php if(!empty($jobdata['walkin_date'])){ echo $jobdata['walkin_date'];}?>">
                                                </div>
                                                </div>
                                                <div class="row">
                                                <div class="col-md-6">
                                                   <label>From Time</label>
                                                </div>
                                                <div class="col-md-6">
                                                   <input type="text" class="form-control" name="walkin_from" id="walkin_from" value="<?php if(!empty($jobdata['walkin_from'])){ echo $jobdata['walkin_from'];}?>">
                                                </div>
                                                </div>
                                                <div class="row">
                                                <div class="col-md-6">
                                                   <label>Walkin To</label>
                                                </div>
                                                <div class="col-md-6">
                                                   <input type="text" class="form-control" name="walkin_to" id="walkin_to" value="<?php if(!empty($jobdata['walkin_to'])){ echo $jobdata['walkin_to'];}?>">
                                                </div>
                                                </div>
                                                </div> -->
                                          </div>
                                       </div>
                                    </div>
                                    <div class="filldetails">
                                       <div class="stepcountfrms">
                                          <div class="headsteps-gt">
                                             <h5>Step 3</h5>
                                             <img src="<?php echo base_url().'recruiterfiles/';?>images/fav.png">
                                          </div>
                                          <div class="dividehalfd">
                                             <div class="row">
                                                <div class="col-md-12 stepsinfosd">
                                                   <p>Let's get to know more about the job</p>
                                                </div>
                                                <div class="col-md-4">
                                                   <label>Create your job Pitch!</label>
                                                </div>
                                                <div class="col-md-8">
                                                   <textarea maxlength="1000" class="form-control" placeholder="Make it count! This will attract jobseekers to view your job posting!" name="jobPitch"><?php if(!empty($jobdata['jobPitch'])){ echo $jobdata['jobPitch'];}?></textarea>
                                                   <?php if(!empty($errors['jobPitch'])){echo "<p class='validError'>".$errors['jobPitch']."</p>";}?>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-4">
                                                   <label>Please describe the Job</label>
                                                </div>
                                                <div class="col-md-8">
                                                   <textarea maxlength="1000" rows="6" class="form-control" placeholder="Job Description" name="jobDesc"><?php if(!empty($jobdata['jobDesc'])){ echo $jobdata['jobDesc'];}?></textarea>
                                                   <?php if(!empty($errors['jobDesc'])){echo "<p class='validError'>".$errors['jobDesc']."</p>";}?>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label>What skills do you require for this job?</label>
                                                </div>
                                                <div class="col-md-6 multipleopyodf">
                                                      <select name="skills[]" id="multiselect" multiple="multiple">
                                                      
                                                      <?php
                                                         if($skills) {
                                                             foreach($skills as $skill) {
                                                         ?>
                                                      <option  value="<?php echo $skill['id']; ?>" <?php if(!empty($jobdata['skills'])){ if($jobdata['skills'] == $skill['id']){ echo "selected";}}?>> <?php echo $skill['skill']; ?> </option>
                                                      <?php
                                                         }
                                                         }

                                                         ?>
                                                      <option id="others" value="12" <?php if(!empty($jobdata['skills'])){ if($jobdata['skills'] == 12){ echo "selected";}}?>>Others</option>   
                                                   </select>
                                                    <input type="text" class="form-control" name="skill" id="other_skill" placeholder="Other Skills" value="<?php if(!empty($jobdata['skill'])){ echo $jobdata['skill'];}?>" style="visibility: hidden; opacity: 0;"> 
                                                   <?php if(!empty($errors['skills'])){echo "<p class='validError'>".$errors['skills']."</p>";}?>
                                                </div>
                                             </div>
                                             <!-- <div class="row">
                                                <div class="col-md-6">
                                                   <label>What Qualifications do you require for this job?</label>
                                                </div>
                                                <div class="col-md-6">
                                                   <input type="text" class="form-control" name="qualification" placeholder="Qualifications" value="<?php if(!empty($jobdata['qualification'])){ echo $jobdata['qualification'];}?>">
                                                   <?php if(!empty($errors['qualification'])){echo "<p class='validError'>".$errors['qualification']."</p>";}?>
                                                   <select>
                                                      <option value="Bs Graduate">Bs Graduate</option>
                                                      <option value="Undergraduate">Undergraduate</option>
                                                      <option value="Associate Degree">Associate Degree</option>
                                                      <option value="College Graduate">College Graduate</option>
                                                      <option value="Post Graduate">Post Graduate</option>
                                                      
                                                      </select>
                                                </div>
                                             </div> -->
                                          </div>
                                       </div>
                                    </div>
                                    <div class="filldetails">
                                       <div class="stepcountfrms">
                                          <div class="headsteps-gt">
                                             <h5>Step 4</h5>
                                             <img src="<?php echo base_url().'recruiterfiles/';?>images/fav.png">
                                          </div>
                                          <div class="dividehalfd">
                                             <div class="row">
                                                <div class="col-md-12 stepsinfosd">
                                                   <p>You can upload up to 3 pictures which would show on the job posting</p>
                                                </div>
                                                <div class="col-md-4">
                                                   <label>If you don't, we'll get the pictures from your site profile</label>
                                                </div>
                                                <div class="col-md-8">
                                                   <table id="myTable1">
                                                      <tbody>
                                                         <tr>
                                                            <td><input type="file" name="job_image[]" class="form-control" required="" accept="image/*"></td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                   <p onclick="myFunction1()" class="addmre" id="addmr">Add Picture</p>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="filldetails">
                                       <div class="stepcountfrms">
                                          <div class="headsteps-gt">
                                             <h5>Step 5</h5>
                                             <img src="<?php echo base_url().'recruiterfiles/';?>images/fav.png">
                                          </div>
                                          <div class="dividehalfd">
                                             <div class="row">
                                                <div class="col-md-12 stepsinfosd">
                                                   <p>We have taken the benefits offered by your company and at the site level.  Please review the list and update as necessary to include offers exclusive for this job.</p>
                                                </div>
                                                <div class="col-md -12 showformsdf">
                                                   <h6>Top Picks</h6>
                                                   <div class="filterchekers">
                                                      <ul>
                                                         <li id="topicks1">
                                                            <input type="checkbox" id="toppic1" name="toppicks[]" value="1" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/joining-bonus.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/joining-bonus-1.png" class="hvrsicos">
                                                            <p> Joining<br>Bonus</p>
                                                         </li>
                                                         <li id="topicks2">
                                                            <input type="checkbox" id="toppic2" name="toppicks[]" value="2" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-food.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-food-1.png" class="hvrsicos">                                    
                                                            <p> Free <br>Food</p>
                                                         </li>
                                                         <li id="topicks3">
                                                            <input type="checkbox" id="toppic3" name="toppicks[]" value="3" onclick="clickfuncheck(this.id)">
                                                            <!--  <i class="fas fa-heartbeat"></i> -->
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/day-1-hmo.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/day-1-hmo-white.png" class="hvrsicos">     
                                                            <p>Day 1 HMO</p>
                                                         </li>
                                                         <li id="topicks4">
                                                            <input type="checkbox" id="toppic4" name="toppicks[]" value="4" onclick="clickfuncheck(this.id)">
                                                            <!--   <i class="fas fa-heartbeat"></i> -->
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/day-1-hmo-for-depended.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/day-1-hmo-for-depended-1.png" class="hvrsicos"> 
                                                            <p> Day 1 HMO<br> for Dependent</p>
                                                         </li>
                                                         <li id="topicks5">
                                                            <input type="checkbox" id="toppic5" name="toppicks[]" value="5" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/day-shift.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/day-shift-1.png" class="hvrsicos">
                                                            <p>Day Shift</p>
                                                         </li>
                                                         <li id="topicks6">
                                                            <input type="checkbox" id="toppic6" name="toppicks[]" value="6" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/14-month-pay.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/14-month-pay-1.png" class="hvrsicos">
                                                            <p> 14th Month Pay</p>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                                <div class="col-md-12 showformsdf">
                                                   <h6>Medical Benefits</h6>
                                                   <div class="filterchekers">
                                                      <ul>
                                                         <li id="medical1">
                                                            <input type="checkbox" id="medi1" name="medical[]" value="1" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-hmo-for-dependents.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-hmo-for-dependents-1.png" class="hvrsicos">
                                                            <p> Free HMO for<br>Dependents</p>
                                                         </li>
                                                         <li id="medical2">
                                                            <input type="checkbox" id="medi2" name="medical[]" value="2" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/critical-illness-benefits.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/critical-illness-benefits1.png" class="hvrsicos">
                                                            <p> Critical Illness <br>Benefits</p>
                                                         </li>
                                                         <li id="medical3">
                                                            <input type="checkbox" id="medi3" name="medical[]" value="3" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/life-insurence.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/life-insurence-1.png" class="hvrsicos">
                                                            <p>Life <br>Insurance</p>
                                                         </li>
                                                         <li id="medical4">
                                                            <input type="checkbox" id="medi4" name="medical[]" value="4" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/maternity-assistance.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/maternity-assistance-1.png" class="hvrsicos">
                                                            <p> Maternity<br> Assistance</p>
                                                         </li>
                                                         <li id="medical5">
                                                            <input type="checkbox" id="medi5" name="medical[]" value="5" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/medicine-reimbursemer.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/medicine-reimbursemer-1.png" class="hvrsicos">
                                                            <p>Medicine <br>Reimbursement</p>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                                <div class="col-md-12 showformsdf">
                                                   <h6>Allowances and Incentives</h6>
                                                   <div class="filterchekers">
                                                      <ul>
                                                         <li id="allowance1">
                                                            <input type="checkbox" id="allowanc1" name="allowances[]" value="1" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/cell-phone-allowance.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/cell-phone-allowance-1.png" class="hvrsicos">
                                                            <p> Cell <br>Allowances</p>
                                                         </li>
                                                         <li id="allowance2">
                                                            <input type="checkbox" id="allowanc2" name="allowances[]" value="2" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-parking.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-parking-1.png" class="hvrsicos">
                                                            <p>Free <br>Parking</p>
                                                         </li>
                                                         <li id="allowance3">
                                                            <input type="checkbox" id="allowanc3" name="allowances[]" value="3" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-shuttle.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-shuttle-1.png" class="hvrsicos"> 
                                                            <p> Free <br> Shuttle</p>
                                                         </li>
                                                         <li id="allowance4">
                                                            <input type="checkbox" id="allowanc4" name="allowances[]" value="4" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/annual--performance-bonus.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/annual--performance-bonus-1.png" class="hvrsicos">
                                                            <p> Annual <br> Performance Bonus</p>
                                                         </li>
                                                         <li id="allowance5">
                                                            <input type="checkbox" id="allowanc5" name="allowances[]" value="5" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/reteirment-benifits.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/reteirments-benefits-1.png" class="hvrsicos">
                                                            <p> Retirements <br> Benefits</p>
                                                         </li>
                                                         <li id="allowance6">
                                                            <input type="checkbox" id="allowanc6" name="allowances[]" value="6" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/transportation.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/transportation-1.png" class="hvrsicos">
                                                            <p> Transporter <br> Allowance</p>
                                                         </li>
                                                         <li id="allowance7">
                                                            <input type="checkbox" id="allowanc7" name="allowances[]" value="7" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/monthly-performance-incentive.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/monthly-performance-incentive-1.png" class="hvrsicos"> 
                                                            <p> Monthly Performance <br> Incentives</p>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                                <div class="col-md-12 showformsdf">
                                                   <h6>Schedule</h6>
                                                   <div class="filterchekers">
                                                      <ul>
                                                         <li id="leaves1" class="leave-class">
                                                            <input type="checkbox" id="leave1" class="leave-radio" name="leavs[]" value="41" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/weekend-off.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/weekend-off-1.png" class="hvrsicos">
                                                            <p> Weekend Off</p>
                                                         </li>
                                                         <li id="leaves2" class="leave-class">
                                                            <input type="checkbox" id="leave2" class="leave-radio" name="leavs[]" value="42" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/holiday-off.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/holiday-off-1.png" class="hvrsicos">
                                                            <p>Holiday Off</p>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                                <div class="col-md-12 showformsdf">
                                                   <h6>Work Shifts</h6>
                                                   <div class="filterchekers1">
                                                      <ul>
                                                         <li id="workshift1" class="work-class">
                                                            <input type="radio" id="works1" class="work-radio" name="shifts[]" value="1">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/mid-shift.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/mid-shift-1.png" class="hvrsicos">
                                                            <p> Mid Shift </p>
                                                         </li>
                                                         <li id="workshift2" class="work-class">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/night-shift.png" class="normicos">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/night-shift-1.png" class="hvrsicos">
                                                            <input type="radio" id="works2" class="work-radio" name="shifts[]" value="2">
                                                            <p> Night Shift </p>
                                                         </li>
                                                         <li id="workshift3" class="work-class">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/24.png" class="normicos">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/24-1.png" class="hvrsicos">
                                                            <input type="radio" id="works3" class="work-radio" name="shifts[]" value="3">
                                                            <p>24/7 </p>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                             <button class="updts" id="post_button" type="submit">Post Job</button>
                                          </div>
                                 </form>
                                 </div>     
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
      </div>
      </section>
      <?php include_once("modalpassword.php");?>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/modernizr.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/script.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/wow.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/slick.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/parallax.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/select-chosen.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.scrollbar.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/popper.min.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/bootstrap.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/recruiteryoda.js"></script>
      <script type="text/javascript" src="<?php echo base_url().'recruiterfiles/';?>js/jquery.timepicker.min.js"></script>
      <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
      <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script> 
      <script type="text/javascript">
         $(document).ready(function(){  
            $.datepicker.setDefaults({  
                 dateFormat: 'yy-mm-dd'   
            });  
            $(function(){  
                 $("#jobExpireid").datepicker({ minDate: +1}); 
            });
           });
      </script>
      <script>
         function myFunction() {
         
           var table = document.getElementById("myTable");
           var rows = document.getElementById("myTable").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length;
           
           var row = table.insertRow(rows);
           var cell1 = row.insertCell(0);
           var cell2 = row.insertCell(1);
              <?php
            /*$exp ="<select name='expRange[]'><option> Select Experience</option>";
            
            $exp .= '<option value="All Tenure">All Tenure</option>
            <option value="less than 6 months">less than 6 months</option>
            <option value="6mo to 1 yr">6mo to 1 yr</option>
            <option value="1 yr to 2 yr">1 yr to 2 yr</option>
            <option value="2yr to 3 yr">2yr to 3 yr</option>
            <option value="3yr and up">3yr and up</option>';
            
            $exp .= "</select>";*/
            
            
            ?>
           cell1.innerHTML = '<select name="expRange[]"><option value="All Tenure">All Tenure</option><option value="less than 6 months">less than 6 months</option><option value="6mo to 1 yr">6mo to 1 yr</option><option value="1 yr to 2 yr">1 yr to 2 yr</option><option value="2yr to 3 yr">2yr to 3 yr</option><option value="3yr and up">3yr and up</option></select>';
           cell2.innerHTML = "<input type='number' min='1' name='expBasicSalary[]' class='form-control' placeholder='Enter Basic Salary per Month'/>";
         }
      </script>
      <script type="text/javascript">
         function checkstatus(){
                var check = $('#bonuscheck').is(':checked');
                if(check==true){
                     $('#bonusdiv').css('visibility','visible');
                     $('#bonusdiv').css('opacity','1');
                     $('#topicks1').addClass('selectedgreen');
                     $('#toppic1').attr("checked", "checked");
                }else{
                     $('#bonusdiv').css('visibility','hidden');
                     $('#bonusdiv').css('opacity','0');
                     $('#topicks1').removeClass('selectedgreen');
                }
         }
               
      </script>
      <script type="text/javascript">
         function autocomplete(inp, arr) {
           /*the autocomplete function takes two arguments,
           the text field element and an array of possible autocompleted values:*/
           var currentFocus;
           /*execute a function when someone writes in the text field:*/
           inp.addEventListener("input", function(e) {
               var a, b, i, val = this.value;
               /*close any already open lists of autocompleted values*/
               closeAllLists();
               if (!val) { return false;}
               currentFocus = -1;
               /*create a DIV element that will contain the items (values):*/
               a = document.createElement("DIV");
               a.setAttribute("id", this.id + "autocomplete-list");
               a.setAttribute("class", "autocomplete-items");
               /*append the DIV element as a child of the autocomplete container:*/
               this.parentNode.appendChild(a);
               /*for each item in the array...*/
               for (i = 0; i < arr.length; i++) {
                 /*check if the item starts with the same letters as the text field value:*/
                 if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                   /*create a DIV element for each matching element:*/
                   b = document.createElement("DIV");
                   /*make the matching letters bold:*/
                   b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                   b.innerHTML += arr[i].substr(val.length);
                   /*insert a input field that will hold the current array item's value:*/
                   b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                   /*execute a function when someone clicks on the item value (DIV element):*/
                   b.addEventListener("click", function(e) {
                       /*insert the value for the autocomplete text field:*/
                       inp.value = this.getElementsByTagName("input")[0].value;
                       /*close the list of autocompleted values,
                       (or any other open lists of autocompleted values:*/
                       closeAllLists();
                   });
                   a.appendChild(b);
                 }
               }
           });
           /*execute a function presses a key on the keyboard:*/
           inp.addEventListener("keydown", function(e) {
               var x = document.getElementById(this.id + "autocomplete-list");
               if (x) x = x.getElementsByTagName("div");
               if (e.keyCode == 40) {
                 /*If the arrow DOWN key is pressed,
                 increase the currentFocus variable:*/
                 currentFocus++;
                 /*and and make the current item more visible:*/
                 addActive(x);
               } else if (e.keyCode == 38) { //up
                 /*If the arrow UP key is pressed,
                 decrease the currentFocus variable:*/
                 currentFocus--;
                 /*and and make the current item more visible:*/
                 addActive(x);
               } else if (e.keyCode == 13) {
                 /*If the ENTER key is pressed, prevent the form from being submitted,*/
                 e.preventDefault();
                 if (currentFocus > -1) {
                   /*and simulate a click on the "active" item:*/
                   if (x) x[currentFocus].click();
                 }
               }
           });
           function addActive(x) {
             /*a function to classify an item as "active":*/
             if (!x) return false;
             /*start by removing the "active" class on all items:*/
             removeActive(x);
             if (currentFocus >= x.length) currentFocus = 0;
             if (currentFocus < 0) currentFocus = (x.length - 1);
             /*add class "autocomplete-active":*/
             x[currentFocus].classList.add("autocomplete-active");
           }
           function removeActive(x) {
             /*a function to remove the "active" class from all autocomplete items:*/
             for (var i = 0; i < x.length; i++) {
               x[i].classList.remove("autocomplete-active");
             }
           }
           function closeAllLists(elmnt) {
             /*close all autocomplete lists in the document,
             except the one passed as an argument:*/
             var x = document.getElementsByClassName("autocomplete-items");
             for (var i = 0; i < x.length; i++) {
               if (elmnt != x[i] && elmnt != inp) {
                 x[i].parentNode.removeChild(x[i]);
               }
             }
           }
           /*execute a function when someone clicks in the document:*/
           document.addEventListener("click", function (e) {
               closeAllLists(e.target);
           });
         }
         
         /*An array containing all the country names in the world:*/
         var countries = ["English", "Afar", "Abkhazian", "Afrikaans","Amharic","Arabic","Assamese","Aymara","Azerbaijani","Bashkir","Belarusian","Bulgarian","Bihari","Bislama","Bengali/Bangla","Tibetan","Breton","Catalan","Corsican","Czech","Welsh","Danish","German","Bhutani","Greek","Esperanto",
              "Spanis","Estonian","Basque","Persian","Finnish","Fiji","Faeroese","French","Frisian","Irish","Scots/Gaelic","Galician","Guarani","Gujarati","Hausa","Hindi","Croatian","Hungarian","Armenian","Interlingua","Interlingue","Inupiak","Indonesian","Icelandic","Italian","Hebrew","Japanese","Yiddish","Javanese","Georgian","Kazakh","Greenlandic","Cambodian","Kannada","Korean","Kashmiri","Kurdish","Kirghiz","Latin","Lingala","Laothian","Lithuanian","Latvian/Lettish","Malagasy","Maori","Macedonian","Malayalam","Mongolian","Moldavian","Marathi","Malay","Maltese","Burmese","Nauru","Nepali","Dutch","Norwegian","Occitan","(Afan)/Oromoor/Oriya","Punjabi","Polish","Pashto/Pushto","Portuguese","Quechua","Rhaeto-Romance","Kirundi","Romanian","Russian","Kinyarwanda","Sanskrit","Sindhi","Sangro","Serbo-Croatian","Singhalese","Slovak","Slovenian","Samoan","Shona","Somali","Albanian","Serbian","Siswati","Sesotho","Sundanese","Swedish","Swahili","Tamil","Telugu","Tajik","Thai","Tigrinya","Turkmen","Tagalog","Setswana","Tonga","Turkish","Tsonga","Tatar","Twi","Ukrainian","Urdu","Uzbek","Vietnamese","Volapuk","Wolof","Xhosa","Yoruba","Chinese","Zulu"];
         
         /*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/
         autocomplete(document.getElementById("myInput"), countries);
      </script>
      <style>
         table#myTable {
         width: 100%;
         }
         table#myTable td {
         padding: 0 9px 0 0;
         }
         .addmre {
         margin: 0;
         float: left;
         color: #fff;
         padding: 3px 8px;
         margin-bottom: 20px;
         cursor: pointer;
         float: right;
         }
         table#myTable td:last-child {
         padding: 0;
         }
      </style>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
      <script type="text/javascript">
         $(document).ready(function(){
            //e.preventDefault();
               $('#jobpostid').validate({
                    rules: {
                      jobTitle: {required:true},
                      jobLoc: {required:true},
                      opening: {required:true, number: true},
                      experience: { required: true},
                      salaryOffered: { required: true, number: true},
                      allowance: { required: true, number: true},
                      industry: { required: true},
                      level: { required: true},
                      education: { required: true},
                      category: { required: true},
                      subcategory: { required: true},
                      lang: { required: true},
                      jobDesc: { required: true},
                      jobPitch: { required: true},
                      skills: { required: true},
                      qualification: { required: true},
                      jobExpire: { required: true},
                      bonus_amount:{required: "#bonuscheck:checked"}
                    },
                    messages: {
                      jobTitle: {required:'Job Title field is required'},
                      jobLoc: {required:'Job Location field is required'},
                      opening: {required:'No. of Opening field is required'},
                      experience: { required:'Experience field is required'},
                      salaryOffered: { required:'Salary Offered field is required'},
                      industry: {required:'Industry field is required'},
                      level: {required:'Level field is required'},
                      education: {required:'Education field is required'},
                      category: {required:'Category field is required'},
                      subcategory: {required:'Sub-Category field is required'},
                      allowance: {required:'Guaranteed Allowance field is required'},
                      lang: {required:'Language field is required'},
                      jobDesc: { required:'Job Description field is required'},
                      jobPitch: { required:'Job Pitch field is required'},
                      skills: { required:'Skills field is required'},
                      qualification: { required:'Qualification field is required'},
                      jobExpire: { required:'Job Expire field is required'},
                      bonus_amount:{required:'Bonus Amount field is required'}
                    }
               });
         });
      </script>

      <script>
         $(function() {
             $( "#date" ).datepicker({ minDate: +1 });
         });
         
         $(document).ready(function(){
             $('#jobLoc').change(function() {
                 var recruiter_id = $('#jobLoc').val();
                 //alert(recruiter_id);
                 $.ajax({
                 type:"POST",
                 url : "<?php echo base_url(); ?>/recruiter/recruiter/recruiter_location",
                 data : {recruiter_id:recruiter_id},
                 success : function(response1) {
                     var response2 = JSON.parse(response1);
                     
                     if(response2['topPicks2'].length >= 1) {
                         var topicks = [1,2,3,4,5,6];
                         for($i=0; $i<response2['topPicks2'].length; $i++) {
                             var abc = topicks.indexOf(response2['topPicks2'][$i]);
                             if(abc) {
                                 var customclass = "#topicks" + response2['topPicks2'][$i];
                                 $(customclass).addClass('selectedgreen');
                                 var cusid = "#toppic" + response2['topPicks2'][$i];
                                 $(cusid).attr("checked", "checked");
                             }
                         }
                     }
                     
                     if(response2['allowance2'].length >= 1) {
                         var allowance = [1,2,3,4,5,6,7];
                         for($i=0; $i<response2['allowance2'].length; $i++) {
                             var abc = allowance.indexOf(response2['allowance2'][$i]);
                             if(abc) {
                                 var customclass = "#allowance" + response2['allowance2'][$i];
                                  $(customclass).addClass('selectedgreen');
                                  var cusid = "#allowanc" + response2['allowance2'][$i];
                                  $(cusid).attr("checked", "checked");
                             }
                         }
                     }
                     
                     if(response2['medical2'].length >= 1) {
                         var medical = [1,2,3,4,5];
                         for($i=0; $i<response2['medical2'].length; $i++) {
                             var abc = medical.indexOf(response2['medical2'][$i]);
                             if(abc) {
                                 var customclass = "#medical" + response2['medical2'][$i];
                                 $(customclass).addClass('selectedgreen');
                                 var cusid = "#medi" + response2['medical2'][$i];
                                 $(cusid).attr("checked", "checked");
                             }
                         }
                     }
                     
                     
                     if(response2['leave2'].length >= 1) {
                         var leave = [1,2];
                         for($i=0; $i<response2['leave2'].length; $i++) {
                             var abc = leave.indexOf(response2['leave2'][$i]);
                             if(abc) {
                                 var customclass = "#leaves" + response2['leave2'][$i];
                                 $(customclass).addClass('selectedgreen');
                                 var cusid = "#leave" + response2['leave2'][$i];
                                 $(cusid).attr("checked", "checked");
                             }
                         }
                     }
                     
                     
                     if(response2['workshift2'].length >= 1) {
                         var workshift = [1,2,3];
                         for($i=0; $i<response2['workshift2'].length; $i++) {
                             var abc = workshift.indexOf(response2['workshift2'][$i]);
                             if(abc) {
                                 var customclass = "#workshift" + response2['workshift2'][$i];
                                 $(customclass).addClass('selectedgreen');
                                 var cusid = "#works" + response2['workshift2'][$i];
                                 $(cusid).attr("checked", "checked");
                             }
                         }
                     }
                     
                 },
                 error: function() {
                     alert('Error occured');
                 }
                 });
             });
         });
      </script>
      <script>
         $(".filterchekers li").click(function(){
             $(this).toggleClass("selectedgreen");
         });
         
         
         function clickfuncheck(id) {
            if(id=='toppic1'){
                  $('#toppic1').addClass("selectedgreen");
                  $('#toppic1').prop('checked', true);
            }
             var cid =  "#"+id;
             if (document.getElementById(id).checked) {
                 $(cid).prop('checked', true);
             } else {
                 $(cid).prop('checked', false);
             }
         }
         
         
         $('.work-radio').change(function() {
             $(".work-class").removeClass("selectedgreen");
             if ($(this).is(':checked')){
                 $(this).closest("li").addClass("selectedgreen");
               }
               else
                 $(this).closest("li").removeClass("selectedgreen");
         });
      </script>
      <script>
         function myFunction1() {
          // var limit=5;
           var table = document.getElementById("myTable1");
           var rows = document.getElementById("myTable1").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length;
            if(rows<=2)
            {
               var row = table.insertRow(rows);
               var cell1 = row.insertCell(0);
                  
               cell1.innerHTML = "<input type='file' name='job_image[]' class='form-control addinput' ><a href='javascript:void(0);' class='remove'><i class='fa fa-close'></i></a>";
            }
            else{
                $('#addmr').hide();
            }
           
           
         }
         $(document).on("click", "a.remove" , function() {
            $(this).parent().remove();
         });
         
         $("#category").change(function(){
             //get category value
             var cat_val = $("#category").val();
             //alert(cat_val);
             // put your ajax url here to fetch subcategory
             var url             =   '<?php echo base_url(); ?>/recruiter/Jobpost/fetchSubcategory';
             // call subcategory ajax here 
             $.ajax({
                            type:"POST",
                            url:url,
                            data:{
                                cat_val : cat_val
                            },
         
                            success:function(data)
         
                             {
                              
                           $("#subcategory").html(data);
                        
                             }
                         });
         });
         
         $('#walkin_from').timepicker({ 'timeFormat': 'H:i' });
         $('#walkin_to').timepicker({ 'timeFormat': 'H:i' });
         
         /*$(document).ready(function(){
           $('.hidden_div').hide();  
         });
           
         $('#job_type').on('change', function() {
              if( this.value =='2'){
                  $(".hidden_div").show();
              }
              if( this.value =='1'){
                  $(".hidden_div").hide();
              }
            });*/
      </script>
      <style>
         .psthbs .filldetails select.halfsideth {
         width: 63%;
         float: left;
         }
         .psthbs .filldetails select.halfsideth {
         width: 63%;
         float: left;
         }



         .filldetails .multipleopyodf button {
    padding: 0;
    float: left;
    width: 100%;
    background: transparent;
    color: #000;
    font-weight: 100;
    font-size: 12px;
    margin: 0;
    outline: none !important;
}

.multipleopyodf {
    position: relative;
}

.filldetails .multipleopyodf .btn-group {
    width: 100%;
}

.multipleopyodf input[type="checkbox"] {
    opacity: 1;
    position: relative;
    left: 3px;
    top: 2px;
    margin-right: 6px;
}

.multipleopyodf .dropdown-menu {
    padding: 10px;
    width: 100%;
}

      </style>

      <script type="text/javascript">
            $(document).ready(function () {
  $('#multiselect').multiselect({
    includeSelectAllOption: true,
    nonSelectedText: 'Select an Option' });
  $('#multiselect').on('change', function() {

      
  if($("#multiselect").val().includes("12")){
      $('#other_skill').css('visibility','visible');
      $('#other_skill').css('opacity','1');
  }else{
      $('#other_skill').css('visibility','hidden');
      $('#other_skill').css('opacity','0');
  }
});

});

function getSelectedValues() {
  var selectedVal = $("#multiselect").val();
  for (var i = 0; i < selectedVal.length; i++) {
    function innerFunc(i) {
      setTimeout(function () {
        location.href = selectedVal[i];
      }, i * 2000);
    }
    innerFunc(i);
  }
}
      </script>

      <script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.min.js'></script>

   </body>
</html>

