// Ofer Slider

$(document).ready(function () {
    $("#Advertisement").owlCarousel({
        margin: 10,
        smartSpeed: 1000,
        autoplay: false,
        center: false,
        nav: true,
        dots: false,
        autoplayHoverPause: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        loop: false,
        responsive: {
            0: {
                items: 1,
            },
            767: {
                items: 2,
            },
            1000: {
                items: 3,
            },
        },
    });
});

$(document).ready(function () {
    $("#Company").owlCarousel({
        margin: 10,
        smartSpeed: 1000,
        autoplay: false,
        autoplay: false,
        nav: true,
        dots: false,
        autoplayHoverPause: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        loop: false,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 3,
            },
            1000: {
                items: 4,
            },
        },
    });
});

$(document).ready(function () {
    $("#topCompany").owlCarousel({
        margin: 10,
        smartSpeed: 1000,
        autoplay: false,
        autoplay: false,
        nav: true,
        dots: false,
        autoplayHoverPause: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        loop: false,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 3,
            },
            1000: {
                items: 4,
            },
        },
    });
});

$(document).ready(function () {
    $("#Expertise").owlCarousel({
        margin: 0,
        smartSpeed: 1000,
        autoplay: false,
        nav: true,
        dots: false,
        autoplayHoverPause: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        loop: false,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 3,
            },
            1000: {
                items: 4,
            },
        },
    });
});

$(document).ready(function () {
    $("#City").owlCarousel({
        margin: 10,
        smartSpeed: 1000,
        autoplay: false,
        nav: true,
        dots: false,
        autoplayHoverPause: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        loop: false,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 3,
            },
            1000: {
                items: 4,
            },
        },
    });
});

$(document).ready(function () {
    $("#Check").owlCarousel({
        margin: 0,
        center: true,
        smartSpeed: 1000,
        autoplay: 5000,
        nav: true,
        dots: false,
        autoplayHoverPause: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        loop: true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 2,
            },
            1000: {
                items: 3,
            },
        },
    });
});

$(document).ready(function () {
    $(".JobSlider").owlCarousel({
        margin: 0,
        smartSpeed: 1000,
        autoplay: 5000,
        nav: true,
        dots: false,
        autoplayHoverPause: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        loop: true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 2,
            },
            1000: {
                items: 3,
            },
        },
    });
});

$(document).ready(function () {
    $(".recomendSlider").owlCarousel({
        margin: 0,
        smartSpeed: 1000,
        autoplay: 5000,
        nav: true,
        dots: false,
        autoplayHoverPause: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        loop: false,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 2,
            },
            1000: {
                items: 3,
            },
        },
    });
});

$(document).ready(function () {
    $("#JobsAvailable").owlCarousel({
        margin: 0,
        nav: true,
        dots: false,
        autoplayHoverPause: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        loop: false,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 3,
            },
            1000: {
                items: 5,
            },
        },
    });
});

$(document).ready(function () {
    $("#Locations").owlCarousel({
        margin: 0,
        nav: true,
        dots: false,
        autoplayHoverPause: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        loop: false,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 3,
            },
            1000: {
                items: 4,
            },
        },
    });
});
// $(document).ready(function () {
//     $("#searchCom").owlCarousel({
//         margin: 0,
//         nav: true,
//         dots: false,
//         autoplayHoverPause: true,
//         navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
//         loop: false,
//         responsive: {
//             0: {
//                 items: 2,
               
//             },
//                 400: {
//                 items: 2,
                 
//             },
//             600: {
//                 items: 4,
//             },
//             1000: {
//                 items: 7,
//             },
//         },
//     });
// });

$(window).scroll(function () {
    var scroll = $(window).scrollTop();
    if (scroll >= 80) {
        $(".Header").addClass("Fixed");
    } else {
        $(".Header").removeClass("Fixed");
    }
});

$(window).scroll(function () {
    var scroll = $(window).scrollTop();
    if (scroll >= 100) {
        $(".JobListingArea .scrolllbarr").addClass("SideFilter");
    } else {
        $(".JobListingArea .scrolllbarr").removeClass("SideFilter");
    }
});

$.fn.jQuerySimpleCounter = function (options) {
    var settings = $.extend({
        start: 0,
        end: 100,
        easing: 'swing',
        duration: 400,
        complete: ''
    }, options);

    var thisElement = $(this);

    $({
        count: settings.start
    }).animate({
        count: settings.end
    }, {
        duration: settings.duration,
        easing: settings.easing,
        step: function () {
            var mathCount = Math.ceil(this.count);
            thisElement.text(mathCount);
        },
        complete: settings.complete
    });
};
var numberhtml = $('#Number').html();
var number1html = $('#Number1').html();
$('#Number').jQuerySimpleCounter({
    end: numberhtml,
    duration: 3000
});
$('#Number1').jQuerySimpleCounter({
    end: number1html,
    duration: 3000
});


// let spinnerWrapper = document.querySelector('.Loader');

// window.addEventListener('load', function () { 
//     spinnerWrapper.parentElement.removeChild(spinnerWrapper);
// });


//new js start
$(document).on('click', '.arrowbtn,.staleAll', function () {
    $(this).siblings('.iconDrop .dropAl').slideToggle();
});
$(document).on('click', '.dropAl li', function () {
    var x = $(this).text();
    $(this).parents('.iconDrop').find('.staleAll').val(x);
});
$(document).on('click', '.dropAl ul li', function () {
    $('.dropAl').slideUp();
});
document.addEventListener("mousedown", function (event) {
    if (event.target.closest(".dropAl , .dropAl ul li"))
        return;
    $('.dropAl').slideUp();
});
//new js add
$(document).on('click', '.searhDropUl li', function () {
    $('.searhDropUl li').removeClass('active');
    $(this).addClass('active');
});
$(document).on('click', '.searhDropUl1 li', function () {
    $('.searhDropUl1 li').removeClass('active');
    $(this).addClass('active');
});

// $(document).on('focus', '.FocusShow', function () {
//     $('.searhDrop').slideDown();
// });
$(document).on('input', '.FocusShow', function () {
    $('.inputDetails').slideDown();
    $('.searhDrop').hide();

});
//   if($('.FocusShow').val() == " "){
//       $('.inputDetails').hide(); 
//       $('.searhDrop').slideDown();
//   }
// $(document).on('focus', '.locationShow', function () {
//     $('.locationDetail').slideDown();
// });
document.addEventListener("mousedown", function (event) {
    if (event.target.closest(".locationShow,.locationDetail"))
        return;
    $('.locationDetail').hide();
});
document.addEventListener("mousedown", function (event) {
    if (event.target.closest(".searhDrop,.FocusShow"))
        return;
    $('.searhDrop').hide();
});
document.addEventListener("mousedown", function (event) {
    if (event.target.closest(".inputDetails,.FocusShow"))
        return;
    $('.inputDetails').hide();
});
//new js end


// Also see: https://www.quirksmode.org/dom/inputfile.html

var inputs = document.querySelectorAll('.file-input')

for (var i = 0, len = inputs.length; i < len; i++) {
    customInput(inputs[i])
}

function customInput(el) {
    const fileInput = el.querySelector('[type="file"]')
    const label = el.querySelector('[data-js-label]')

    fileInput.onchange =
        fileInput.onmouseout = function () {
            if (!fileInput.value) return

            var value = fileInput.value.replace(/^.*[\\\/]/, '')
            el.className += ' -chosen'
            label.innerText = value
        }
}



//new slider code start


jQuery("#carousel6").owlCarousel({
    autoplay: false,
    lazyLoad: true,
    loop: true,
    margin: 20,
    center: true,
    dots: false,
    /*
  animateOut: 'fadeOut',
  animateIn: 'fadeIn',
  */
    responsiveClass: true,
    autoHeight: true,
    autoplayTimeout: 7000,
    smartSpeed: 800,
    nav: true,
    responsive: {
        0: {
            items: 1
        },

        600: {
            items: 2
        },

        1024: {
            items: 3
        },

        1366: {
            items: 3
        }
    }
});


// accordian
// $(document).ready(function () {
//    $(".set > a").on("click", function () {
//        if ($(this).hasClass("active")) {
//            $(this).removeClass("active");
//            $(this)
//                .siblings(".content")
//                .slideUp(200);
//            $(".set > a i")
//                .removeClass("fa-minus")
//                .addClass("fa-plus");
//        } else {
//            $(".set > a i")
//                .removeClass("fa-minus")
//                .addClass("fa-plus");
//            $(this)
//                .find("i")
//                .removeClass("fa-plus")
//                .addClass("fa-minus");
//            $(".set > a").removeClass("active");
//            $(this).addClass("active");
//            $(".content").slideUp(200);
//            $(this)
//                .siblings(".content")
//                .slideDown(200);
//        }
//    }); 
// });
$(document).on('click','.set.set2 a',function(){
    
    var getclass = $(this).find("i").attr("class");
    if(getclass == "fa fa-minus") {
        $(this).find("i").addClass('fa-plus');
        $(this).find("i").removeClass('fa-minus');
    } else {
        $(this).find("i").addClass('fa-minus');
        $(this).find("i").removeClass('fa-plus');
    }
    $(this).siblings('.content.content2').slideToggle();
});
// accordian end

$('document').on('click', '.optBtn ul li', function () {
    $('.optBtn ul li').removeClass('active');
    $(this).addClass('active');
    alert('okk');
});


// input js
$(document).on('focus', '.formPost input ,.formPost  textarea, .inputAll ', function () {
    $('.form-group').each(function () {
        var labelVal = $(this).children('label.lab').text();
        if (labelVal != '') {
            $(this).children('input ,.inputAll').attr('placeholder', labelVal);
            $(this).children('label.lab').text('');
        }
    });
    var placeholder = $(this).attr('placeholder');
    $(this).siblings('label.lab').text(placeholder);
    $(this).attr('placeholder', '');
});

