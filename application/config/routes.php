<?php
defined('BASEPATH') OR exit('No direct script access allowed');
session_start();
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//$route['user/(:any)'] = 'user/otpverification/$1';
//$CI = &get_instance();
//$CI->load->library("session");

$route['default_controller'] = 'homepage/home_demo';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['home'] = 'homepage/home_demo';

$route['home_demo'] = 'homepage/home_demo';

$route['login'] = 'auth/login';
$route['signup'] = 'auth/signup';
$route['signup_step_two'] = 'auth/signuptwo';
$route['verication_account'] = 'auth/verification';
$route['signup_success'] = 'auth/successsignup';
$route['signup_successone'] = 'auth/successsignupone';
$route['other_details'] = 'auth/otherdetails';

$route['forgot_password'] = 'auth/forgotpassword';
$route['forgot/(:any)'] = 'user/forgotpasswordview/$1';
$route['forgotpassword/(:any)'] = 'user/forgotpasswordviewapp/$1';

$route['jobs'] = 'user/find_jobs';
$route['jobs/(:any)'] = 'user/find_jobs';
$route['jobs/(:any)/(:any)?'] = 'user/find_jobs';
$route['jobs/(:any)/(:any)?/(:any)?'] = 'user/find_jobs';

$route['jobs_filter'] = 'user/find_jobs_filter';
$route['jobs_filter/(:any)'] = 'user/find_jobs_filter';
$route['jobs_filter/(:any)/(:any)?'] = 'user/find_jobs_filter';
$route['jobs_filter/(:any)/(:any)?/(:any)?'] = 'user/find_jobs_filter';

$route['job_listings/(:any)/(:any)?'] = 'dashboard/find_jobs';

$route['site_details/(:any)'] = 'user/companyProfileDetails';
$route['company_details/(:any)'] = 'user/recruiterProfileDetails';
$route['search'] = 'user/home_search';

$route['job/description/(:any)'] = 'user/jobDescription';

$route['chatbot/(:any)'] = 'dashboard/chatbot_job';
$route['save_chatbot'] = 'dashboard/chatbot_job_save';
$route['chatbot_history'] = 'dashboard/chatbot_history';
$route['chatbot_view/(:any)'] = 'dashboard/chatbot_view';

$route['explore_jobs'] = 'homepage/explorejobs';
$route['our_partners'] = 'homepage/ourpartners';
$route['top_partners'] = 'homepage/toppartners';
$route['city_search'] = 'homepage/citysearch';
$route['your_expertise'] = 'homepage/yourexpertise';
$route['videos'] = 'homepage/videos';

$route['contact'] = 'frontpage/contact';
$route['contact_submit'] = 'frontpage/usercontact';
$route['thank_you'] = 'frontpage/thankyou';

$route['sitemap'] = 'frontpage/sitemap';
$route['privacy_policy'] = 'frontpage/privacy';
$route['terms'] = 'frontpage/terms';
$route['about'] = 'frontpage/aboutUs';
$route['faq'] = 'frontpage/faq';
$route['how_it_works'] = 'frontpage/howitworks';

$route['profile'] = 'dashboard/myprofile';
$route['resume'] = 'dashboard/myresume';
$route['resume_download'] = 'dashboard/mydownload';
$route['resume_delete'] = 'dashboard/myresumedelete';

$route['resume_download_new'] = 'user/mydownloadnew';
$route['resume_download_one'] = 'user/mydownloadone';
$route['resume_download_two'] = 'user/mydownloadtwo';

$route['signinbonus'] = 'dashboard/signinBonus';
$route['savedjob'] = 'dashboard/savedjob';
$route['hotjob'] = 'dashboard/hotjob';
$route['appliedjobs'] = 'dashboard/appliedJobs';
$route['notifications'] = 'dashboard/notifications';
$route['successstory'] = 'dashboard/successstory';
//$route['search'] = 'user/search1';

//  new urls
	$route['new_forogt_password'] = 'auth/forgotpassword';
	$route['new_set_password'] = 'auth/setpassword';
	$route['new_success_reset'] = 'auth/successreset';
//

$route['recruiter'] = 'recruiter/testimonial/testimonial_view';
$route['recruiter/login'] = 'recruiter/recruiter/index';

$route['recruiter/signup'] = 'recruiter/recruiter/signup';

$route['recruiter/subscription'] = 'recruiter/recruiter/subscription_plans';
$route['recruiter/change_subscription'] = 'recruiter/recruiter/change_subscription_plan';

$route['recruiter/subscription_view'] = 'recruiter/recruiter/subscription_view';
$route['recruiter/subscription_history'] = 'recruiter/recruiter/subscription_history';

$route['recruiter/questionnaire/(:any)'] = 'recruiter/questionnaire/questionnaire_add';
$route['recruiter/questionnaire_save'] = 'recruiter/questionnaire/questionnairesave';
$route['recruiter/questionnaire_view/(:any)'] = 'recruiter/questionnaire/questionnaire_view';
$route['recruiter/questionnaire_edit/(:any)'] = 'recruiter/questionnaire/questionnaireedit';
$route['recruiter/questionnaire_update'] = 'recruiter/questionnaire/questionnaireupdate';

$route['recruiter/questionnaire_user_view/(:any)/(:any)'] = 'recruiter/questionnaire/questionnaire_user_view';

$route['recruiter/subrecuriterlist'] = 'recruiterN/recruiter/recruiterlist';
$route['recruiter/addsubrecruiter'] = 'recruiterN/recruiter/addrecruiter';
$route['recruiter/createpassword/(:any)'] = 'recruiterN/recruiter/createpassword/$1';
$route['recruiter/passwordchange/(:any)'] = 'recruiter/recruiter/passwordchange/$1';
$route['recruiter/companyprofile'] = 'recruiter/recruiter/companyprofile';
$route['recruiter/postjob'] = 'recruiter/jobpost/index';

$route['recruiter/assignjob'] = 'recruiter/jobpost/assignjob';

$route['recruiter/managejobs'] = 'recruiter/jobpost/manageJobView';
$route['recruiter/managecandidates'] = 'recruiter/candidate/manageCandidates';
$route['recruiter/recruiterprofile'] = 'recruiter/recruiter/recruiterprofile';
$route['recruiter/transaction'] = 'recruiter/recruiter/transaction';
$route['recruiter/rating'] = 'recruiter/recruiter/rating';
$route['recruiter/jobwisereport'] = 'recruiter/report/jobwise_report';
$route['recruiter/candidatereport'] = 'recruiter/report/candidate_report';
$route['recruiter/screeningreport'] = 'recruiter/report/screening_report';
$route['recruiter/contact'] = 'recruiter/recruiter/contact';
$route['recruiter/faq'] = 'recruiter/recruiter/faq';
$route['recruiter/manageCompany'] = 'recruiter/recruiter/manageCompany';
$route['recruiter/filtercandidate'] = 'recruiter/candidate/filtercandidate';
$route['recruiter/filtermanagecandidate'] = 'recruiter/candidate/filtermanagecandidate';
$route['recruiter/searchCandidates'] = 'recruiter/candidate/searchCandidates';

$route['recruiter/advertisement'] = 'recruiter/recruiter/add_advertisement';
$route['recruiter/advertisementlist'] = 'recruiter/recruiter/advertise_list';

$route['recruiter/videoadvertisement'] = 'recruiter/recruiter/add_videoadvertisement';
$route['recruiter/videoadvertisementlist'] = 'recruiter/recruiter/videoadvertise_list';

$route['recruiter/testimonialview'] = 'recruiter/testimonial/testimonial_view';
$route['recruiter/testimoniallist'] = 'recruiter/testimonial/testimonial_list';
$route['recruiter/testimonialadd'] = 'recruiter/testimonial/testimonial_add';
$route['recruiter/testimonialedit'] = 'recruiter/testimonial/testimonial_edit';

$route['recruiter/transfer_recruiter'] = 'recruiter/recruiter/transfer';


//  Admin

$route['administrator'] = 'administrator/admin/index';
$route['administrator/dashboard'] = 'administrator/dashboard/index';
$route['administrator/data_metrics'] = 'administrator/dashboard/data_metrics';
$route['administrator/company_wise_data_metrics'] = 'administrator/dashboard/company_wise_data_metrics';

$route['administrator/forgot'] = 'administrator/admin/forgot';
$route['administrator/recruiterList'] = 'administrator/recruiter/recruiterListing';
$route['administrator/pendingrecruiters'] = 'administrator/recruiter/pending_recruiters';
$route['administrator/companyList'] = 'administrator/recruiter/companyListing';

$route['administrator/joblist'] = 'administrator/Jobpost/index';
$route['administrator/questionnaire_view/(:any)'] = 'administrator/Jobpost/questionnaire_view';
$route['administrator/questionnaire_user_view/(:any)/(:any)'] = 'administrator/Jobpost/questionnaire_user_view';

$route['administrator/jobexpirelist'] = 'administrator/Jobpost/jobexpirelist';
$route['administrator/jobseekerlist/(:any)?'] = 'administrator/JobSeeker/jobseekerlisting';
$route['administrator/storylist'] = 'administrator/recruiter/storyListing';
$route['administrator/contentlist'] = 'administrator/recruiter/staticcontentlist';
$route['administrator/loadingscreen'] = 'administrator/JobSeeker/loadingscreen';
$route['administrator/addrecruiter'] = 'administrator/recruiter/addRecruiter';
$route['administrator/boostjobs'] = 'administrator/Jobpost/boostjobs';

$route['administrator/activelyjobs'] = 'administrator/Jobpost/activelyjobs';

$route['administrator/addprice'] = 'administrator/Recruiter/addprice';

//$route['administrator/hiringreport'] = 'administrator/Jobpost/hiring_reports';

$route['administrator/hiring_report'] = 'administrator/Jobpost/hiring_report';
$route['administrator/hiring_report/(:any)?'] = 'administrator/Jobpost/hiring_report';
$route['administrator/hiring_report/(:any)?/(:any)?'] = 'administrator/Jobpost/hiring_report';

$route['administrator/job_match'] = 'administrator/Jobpost/jobmatch_report';
$route['administrator/job_match/(:any)?'] = 'administrator/Jobpost/jobmatch_report';
$route['administrator/job_match/(:any)?/(:any)?'] = 'administrator/Jobpost/jobmatch_report';

$route['administrator/job_matching/(:any)?/(:any)?'] = 'administrator/Jobpost/jobmatching';
$route['administrator/admin_applied/(:any)?'] = 'administrator/Jobpost/adminjobapplied';



$route['administrator/screening_report'] = 'administrator/Jobpost/screening_reports';
$route['administrator/screening_report/(:any)?'] = 'administrator/Jobpost/screening_reports';
$route['administrator/screening_report/(:any)?/(:any)?'] = 'administrator/Jobpost/screening_reports';

$route['administrator/notappliedreport'] = 'administrator/Jobpost/notappliedjob_reports';

$route['administrator/userreport'] = 'administrator/Jobpost/user_reports';
$route['administrator/userreport/(:any)?'] = 'administrator/Jobpost/user_reports';
$route['administrator/userreport/(:any)?/(:any)?'] = 'administrator/Jobpost/user_reports';

$route['administrator/nonuserreport'] = 'administrator/Jobpost/non_user_reports';

$route['administrator/invoice'] = 'administrator/Recruiter/invoice';
$route['administrator/contactlist'] = 'administrator/JobSeeker/contactlisting';
$route['administrator/resetpassword'] = 'administrator/admin/reset';
$route['administrator/addfaq'] = 'administrator/recruiter/addfaq';
$route['administrator/faqlist'] = 'administrator/recruiter/faqlist';
$route['administrator/addjobtitle'] = 'administrator/recruiter/addJobTitle';
$route['administrator/addindustry'] = 'administrator/recruiter/addIndustry';
$route['administrator/addjoblevel'] = 'administrator/recruiter/addJobLevel';
$route['administrator/addjobcategory'] = 'administrator/recruiter/addJobCategory';
$route['administrator/addjobsubcategory'] = 'administrator/recruiter/addJobSubCategory';
$route['administrator/addskills'] = 'administrator/recruiter/addSkills';
$route['administrator/addstaticcontent'] = 'administrator/recruiter/addstaticcontent';
$route['administrator/addsuperpower'] = 'administrator/recruiter/addPower';
$route['administrator/addtopbpo'] = 'administrator/recruiter/addtopbpo';
$route['administrator/reconciliation'] = 'administrator/JobSeeker/reconciliation';
$route['administrator/notifications'] = 'administrator/JobSeeker/notifications';

$route['administrator/notificationlist'] = 'administrator/JobSeeker/notificationslist';

$route['administrator/jobnotificationlist'] = 'administrator/JobSeeker/jobnotificationslist';

$route['administrator/addreferral'] = 'administrator/recruiter/addreferral';
$route['administrator/referralreport'] = 'administrator/recruiter/referralreport';
$route['administrator/hirecost'] = 'administrator/Jobpost/hirecost';

$route['administrator/adadvertise'] = 'administrator/Recruiter/adadvertise';
$route['administrator/advertisement'] = 'administrator/Recruiter/advertiselist';

$route['administrator/videoadvertise'] = 'administrator/Recruiter/videoadvertise';
$route['administrator/videoadvertisement'] = 'administrator/Recruiter/videoadvertiselist';
$route['administrator/addvideoprice'] = 'administrator/Recruiter/addvideoprice';

$route['administrator/news'] = 'administrator/Recruiter/newsadvertise';
$route['administrator/newslist'] = 'administrator/Recruiter/newsadvertiselist';

$route['administrator/app_reviews'] = 'administrator/Recruiter/appreviews';
$route['administrator/app_review_lists'] = 'administrator/Recruiter/appreviewlist';
$route['administrator/app_review_save'] = 'administrator/Recruiter/appreviewsave';

$route['administrator/apiversioning'] = 'administrator/JobSeeker/apiversioning';

$route['administrator/recruitertransfer'] = 'administrator/Recruiter/recruitertransfer';
$route['administrator/recruiter_testimonial'] = 'administrator/Recruiter/testimonial_view';

$route['administrator/subscription_plans'] = 'administrator/subscription/subscriptionlist';
$route['administrator/subscription_plan_add'] = 'administrator/subscription/subscriptionadd';
$route['administrator/subscription_plan_create'] = 'administrator/subscription/subscriptioncreate';
$route['administrator/subscription_plan_edit'] = 'administrator/subscription/subscriptionedit';
$route['administrator/subscription_plan_update'] = 'administrator/subscription/subscriptionupdate';

$route['administrator/subscription_request'] = 'administrator/subscription/subscriptionrequest';



//SEO MANAGEMENT


$route['seo'] = 'seo/admin/index';
$route['seo/dashboard'] = 'seo/dashboard/index';
$route['seo/forgot'] = 'seo/admin/forgot';
$route['seo/recruiterList'] = 'seo/recruiter/recruiterListing';
$route['seo/pendingrecruiters'] = 'seo/recruiter/pending_recruiters';
$route['seo/companyList'] = 'seo/recruiter/companyListing';
$route['seo/joblist'] = 'seo/Jobpost/index';
$route['seo/jobexpirelist'] = 'seo/Jobpost/jobexpirelist';
$route['seo/jobseekerlist'] = 'seo/JobSeeker/jobseekerlisting';
$route['seo/storylist'] = 'seo/recruiter/storyListing';
$route['seo/contentlist'] = 'seo/recruiter/staticcontentlist';
$route['seo/loadingscreen'] = 'seo/JobSeeker/loadingscreen';
$route['seo/addrecruiter'] = 'seo/recruiter/addRecruiter';
$route['seo/boostjobs'] = 'seo/Jobpost/boostjobs';
$route['seo/addprice'] = 'seo/Recruiter/addprice';

$route['seo/hiring_report'] = 'seo/Jobpost/hiring_report';
$route['seo/hiring_report/(:any)?'] = 'seo/Jobpost/hiring_report';
$route['seo/hiring_report/(:any)?/(:any)?'] = 'seo/Jobpost/hiring_report';

$route['seo/screening_report'] = 'seo/Jobpost/screening_reports';
$route['seo/screening_report/(:any)?'] = 'seo/Jobpost/screening_reports';
$route['seo/screening_report/(:any)?/(:any)?'] = 'seo/Jobpost/screening_reports';

$route['seo/notappliedreport'] = 'seo/Jobpost/notappliedjob_reports';

$route['seo/userreport'] = 'seo/Jobpost/user_reports';
$route['seo/userreport/(:any)?'] = 'seo/Jobpost/user_reports';
$route['seo/userreport/(:any)?/(:any)?'] = 'seo/Jobpost/user_reports';

$route['seo/nonuserreport'] = 'seo/Jobpost/non_user_reports';


$route['seo/invoice'] = 'seo/Recruiter/invoice';
$route['seo/contactlist'] = 'seo/JobSeeker/contactlisting';
$route['seo/resetpassword'] = 'seo/admin/reset';
$route['seo/addfaq'] = 'seo/recruiter/addfaq';
$route['seo/faqlist'] = 'seo/recruiter/faqlist';
$route['seo/faq_meta_tag'] = 'seo/recruiter/faqMetaTag';
$route['seo/video_meta_tag'] = 'seo/recruiter/videoMetaTag';

$route['seo/addjobtitle'] = 'seo/recruiter/addJobTitle';
$route['seo/addindustry'] = 'seo/recruiter/addIndustry';
$route['seo/addjoblevel'] = 'seo/recruiter/addJobLevel';
$route['seo/addjobcategory'] = 'seo/recruiter/addJobCategory';
$route['seo/addjobsubcategory'] = 'seo/recruiter/addJobSubCategory';
$route['seo/addskills'] = 'seo/recruiter/addSkills';
$route['seo/addstaticcontent'] = 'seo/recruiter/addstaticcontent';
$route['seo/addsuperpower'] = 'seo/recruiter/addPower';
$route['seo/addtopbpo'] = 'seo/recruiter/addtopbpo';
$route['seo/reconciliation'] = 'seo/JobSeeker/reconciliation';
$route['seo/notifications'] = 'seo/JobSeeker/notifications';

$route['seo/notificationlist'] = 'seo/JobSeeker/notificationslist';

$route['seo/jobnotificationlist'] = 'seo/JobSeeker/jobnotificationslist';

$route['seo/addreferral'] = 'seo/recruiter/addreferral';
$route['seo/referralreport'] = 'seo/recruiter/referralreport';
$route['seo/hirecost'] = 'seo/Jobpost/hirecost';

$route['seo/adadvertise'] = 'seo/Recruiter/adadvertise';
$route['seo/advertisement'] = 'seo/Recruiter/advertiselist';

$route['seo/videoadvertise'] = 'seo/Recruiter/videoadvertise';
$route['seo/videoadvertisement'] = 'seo/Recruiter/videoadvertiselist';
$route['seo/addvideoprice'] = 'seo/Recruiter/addvideoprice';

$route['seo/news'] = 'seo/Recruiter/newsadvertise';
$route['seo/newslist'] = 'seo/Recruiter/newsadvertiselist';

$route['seo/apiversioning'] = 'seo/JobSeeker/apiversioning';

$route['seo/recruitertransfer'] = 'seo/Recruiter/recruitertransfer';
$route['seo/recruiter_testimonial'] = 'seo/Recruiter/testimonial_view';

$route['seo/subscription_plans'] = 'seo/subscription/subscriptionlist';
$route['seo/subscription_plan_add'] = 'seo/subscription/subscriptionadd';
$route['seo/subscription_plan_create'] = 'seo/subscription/subscriptioncreate';
$route['seo/subscription_plan_edit'] = 'seo/subscription/subscriptionedit';
$route['seo/subscription_plan_update'] = 'seo/subscription/subscriptionupdate';
$route['seo/how_it_works'] = 'seo/Recruiter/howItsWork';
$route['seo/contact_us'] = 'seo/Recruiter/contactus';
$route['seo/homepage'] = 'seo/Recruiter/homepage';