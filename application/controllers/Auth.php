<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittall
 * @license         Mobulous
 */
class Auth extends CI_Controller {
    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('newmodels/User_Model');
        $this->load->model('newmodels/recruiter/Jobpost_Model');
        $this->load->model('newmodels/recruiter/Newpoints_Model');
        $this->load->model('newmodels/Common_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');
        $this->load->library('google');
        $this->load->library('Facebook');
    }

    public function login() {

        if($this->session->userdata('usersess')) {

            redirect('/');
        }

        $data['loginURL'] = $this->google->loginURL();
        $data['authUrl'] = $this->facebook->login_url();

        $this->load->view('new_design_login', $data);
    }

    public function logincheck() {
        $userData = $this->input->post();
        $password = $userData['password'];
        $email = $userData['email'];
        $data = ["email" => $userData['email'], "type"=>"normal"];
        
        if (empty($email)) {
            $errorMSG = 'Please enter your email.';
            $error = ["error" => $errorMSG];
            echo json_encode($error);
            die;

        } else if (empty($password)) {
            $errorMSG = 'Please enter your password.';
            $error = ["error" => $errorMSG];
            echo json_encode($error);
            die;

        } else {
            $userCheck = $this->User_Model->user_login($data);
            
            if ($userCheck) {

                $getPass = $userCheck[0]['password'];
                $vPass = $this->encryption->decrypt($getPass);

                if ($password == $vPass) {
                    if($userCheck[0]['verify_status'] == 1) {

                        $lastlogin = ["last_used"=>date("Y-m-d H:i:s")];
                        $this->User_Model->update_token($lastlogin, $userCheck[0]['id']);

                        $getData = $this->fetchUserSingleData($userCheck[0]['id']);
                        $getCompletenes = $this->fetchUserCompleteData($userCheck[0]['id']);
                        $email = $userData['email'];
                        $remeber = $this->input->post('rememberval');
                        if ($remeber == 1) {
                            $hour = time() + 3600 * 24 * 30;
                            setcookie('emaill', $email, $hour);
                            setcookie('passwordd', $vPass, $hour);
                            setcookie('checkval', $remeber, $hour);
                        }
                        $this->session->set_userdata('usersess', $getData);
                        if($this->session->userdata('previous_url_again')) {
                            $urlnew = $this->session->userdata('previous_url_again');
                        } else {
                            $urlnew = "";
                        }
                        $success = ["success" => "logindone", "url"=>$urlnew];
                        echo json_encode($success);
                        die;

                    } else {

                        $sessData = ["id"=>$userCheck[0]['id'], "name"=>$userCheck[0]['name'], "phone"=>$userCheck[0]['phone'], "country_code"=>$userCheck[0]['country_code'], "email"=>$userCheck[0]['email'], "type"=>$userCheck[0]['type']];

                        $this->session->set_userdata('sessionagainverify', $sessData);

                        $redirecturl = base_url().'verication_account';
                        $success = ["success" => "redirect", "url"=>$redirecturl];
                        echo json_encode($success);
                        die;
                    }

                } else {
                    $errorMSG = 'Password is incorrect.';
                    $error = ["error" => $errorMSG];
                    echo json_encode($error);
                    die;
                }
                
            } else {
                $errorMSG = 'Please signup to proceed';
                $error = ["error" => $errorMSG];
                echo json_encode($error);
                die;
            }
        }
    }

    public function google_login() {

        if (isset($_GET['code'])) {
            //authenticate user
            $this->google->getAuthenticate();
            //get user info from google
            $gpInfo = $this->google->getUserInfo();
            //preparing data for database insertion
            $userData['oauth_provider'] = 'google';
            $userData['oauth_uid'] = $gpInfo['id'];
            $userData['first_name'] = $gpInfo['given_name'];
            $userData['last_name'] = $gpInfo['family_name'];
            $userData['email'] = $gpInfo['email'];
            $userData['gender'] = !empty($gpInfo['gender']) ? $gpInfo['gender'] : '';
            $userData['locale'] = !empty($gpInfo['locale']) ? $gpInfo['locale'] : '';
            $userData['profile_url'] = !empty($gpInfo['link']) ? $gpInfo['link'] : '';
            $userData['picture_url'] = !empty($gpInfo['picture']) ? $gpInfo['picture'] : '';
            //insert or update user data to the database
            
            $data = ['fname' => ucwords(strtolower($userData['first_name'])), "lname"=>ucwords(strtolower($userData['last_name'])), 'email' => $userData['email'], 'password' => "", 'type' => "gmail", 'socialToken'=>$userData['oauth_uid']];

            $socialCheck = $this->User_Model->user_socialagain($userData['oauth_uid'], 'gmail');
            if (!$socialCheck) {
                $this->session->set_userdata('usersocialsession', $data);
                redirect('signup');

            } else {

                if($socialCheck[0]['status'] == 0) {
                    
                    $this->session->set_userdata('usersocialsession', $data);
                    redirect('signup');

                } else {

                    $getCompletenes = $this->fetchUserCompleteData($userData['email']);
                    $getData = $this->fetchUserSingleData1($userData['email']);

                    $lastlogin = ["last_used"=>date("Y-m-d H:i:s")];
                    $this->User_Model->update_token($lastlogin, $getData['id']);

                    $this->session->set_userdata('usersess', $getData);
                    
                    if($this->session->userdata('previous_url_again')) {
                        $previous_url = $this->session->userdata('previous_url_again');
                        redirect($previous_url);
                    } else {
                        if($previous_url = $this->session->userdata('previous_url')) {
                            redirect($previous_url);
                        } else {
                            redirect('resume');
                        }
                    }
                }
            }
        }
    }

    public function facebookSignup() {
        $userData = array();
        if ($this->facebook->is_authenticated()) {
            // Get user facebook profile details
            $userProfile = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,gender,locale,picture');
            // Preparing data for database insertion
            $userData['oauth_provider'] = 'facebook';
            $userData['oauth_uid'] = $userProfile['id'];
            $userData['first_name'] = $userProfile['first_name'];
            $userData['last_name'] = $userProfile['last_name'];
            $userData['email'] = $userProfile['email'];
            $userData['profile_url'] = 'https://www.facebook.com/' . $userProfile['id'];
            $userData['picture_url'] = $userProfile['picture']['data']['url'];
            // Insert or update user data

            $data = ['fname' => ucwords(strtolower($userData['first_name'])), "lname"=> ucwords(strtolower($userData['last_name'])), 'email' => $userData['email'], 'password' => "", 'type' => "facebook", 'socialToken'=>$userData['oauth_uid']];

            $socialCheck = $this->User_Model->user_socialagain($userData['oauth_uid'], "facebook");
            
            // Check user data insert or update status
            if (!$socialCheck) {

                $this->session->set_userdata('usersocialsession', $data);
                redirect('signup');

            } else {

                if($socialCheck[0]['status'] == 0) {
                
                    $this->session->set_userdata('usersocialsession', $data);    
                    redirect('signup');

                } else {

                    $getCompletenes = $this->fetchUserCompleteData($socialCheck[0]['id']);
                    $getData = $this->fetchUserSingleData($socialCheck[0]['id']);

                    $lastlogin = ["last_used"=>date("Y-m-d H:i:s")];
                    $this->User_Model->update_token($lastlogin, $getData['id']);
                    
                    $this->session->set_userdata('usersess', $getData);
                    
                    if($this->session->userdata('previous_url_again')) {
                        $previous_url = $this->session->userdata('previous_url_again');
                        redirect($previous_url);
                    } else {
                        if($previous_url = $this->session->userdata('previous_url')) {
                            redirect($previous_url);
                        } else {
                            redirect('user/homeafterlogin');
                        }
                    }
                }
            }
        }
    }

    public function signup() {

        $this->session->unset_userdata('sessionone');
        $this->session->unset_userdata('sessionagainverify');
        $this->session->unset_userdata('sessionsuccess');
        $this->session->unset_userdata('sessionverify');

        $data["phonecodes"] = $this->Common_Model->phonecode_lists();
        $data["nations"] = $this->Common_Model->nation_lists();
        $data["states"] = $this->User_Model->getownstate();

        if($this->session->userdata('usersocialsession')) {
            $socialSignup = $this->session->userdata('usersocialsession');
            $data['type'] = $socialSignup["type"];
            $socialData = ["fname"=>$socialSignup["fname"],"lname"=>$socialSignup["lname"],"email"=>$socialSignup["email"]];
            $data['userData'] = $socialData;
        }

        $this->load->view('new_design_register_one', $data);
    }

    public function signupstepone() {

        $userData = $this->input->post();
        $this->form_validation->set_rules('fname', 'First Name', 'trim|required');
        $this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[70]|callback_webemail_check');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required|min_length[4]|callback_webphone_check');
        $this->form_validation->set_rules('country_code', 'Country Code', 'trim|required');
        $this->form_validation->set_rules('location', 'Location', 'trim|required');
        $this->form_validation->set_rules('state', 'State', 'trim');
        $this->form_validation->set_rules('city', 'City', 'trim');
        $this->form_validation->set_rules('nationality', 'Nationality', 'trim');
        $this->form_validation->set_rules('timeforcall', 'Best time for call', 'trim');
        
        if($userData['signupType'] == "normal") {
            $this->form_validation->set_rules('pass', 'password', 'trim|required|callback_valid_password');
            $this->form_validation->set_rules('cpass', 'Confirm password', 'trim|required|matches[pass]');
        }
        
        if ($this->form_validation->run() == FALSE) {

            $data['type'] = $userData["signupType"];
            $data['userData'] = $userData;
            $data["phonecodes"] = $this->Common_Model->phonecode_lists();
            $data["nations"] = $this->Common_Model->nation_lists();
            $data["states"] = $this->User_Model->getownstate();
            $data['signuperrors'] = $this->form_validation->error_array();         

            $this->load->view('new_design_register_one', $data);
        } else {

            if($userData['signupType'] == "normal") {
                $hash = $this->encryption->encrypt($userData['pass']);
            } else {
                $hash = "pass";
            }
            $token = $this->User_Model->randomstring();
            $randomnumber = rand(1000, 9999);

            if($this->session->set_userdata('usersocialsession')) {
                $socialSignup = $this->session->set_userdata('usersocialsession');
                $data['socialToken'] = $socialSignup["socialToken"];
            } else {
                $data['socialToken'] = "";
            }

            $data1 = ['socialToken'=>$data['socialToken'], 'name' => ucwords(strtolower($userData['fname'])) . ' ' . ucwords(strtolower($userData['lname'])), 'email' => $userData['email'], 'phone' => $userData['phone'], 'country_code' => $userData['country_code'], 'location' => $userData['location'], 'nationality' => $userData['nationality'], 'password' => $hash, 'type' => $userData['signupType'], 'token' => $token, 'status' => 1, 'verify_status' => 0, 'verify_code' => $randomnumber, 'referral_used'=>$userData['refer_code'], "state"=> $userData['state'], "city"=> $userData['city'], 'timeforcall'=>$userData['timeforcall']];

            $this->session->set_userdata('sessionone', $data1);
            redirect("signup_step_two");
            exit;
        }
    }

    public function signuptwo() {

        if(!$this->session->userdata('sessionone')) {
            redirect('signup');
        }
        $data['categories'] = $this->Jobpost_Model->category_lists();
        $data['levellists'] = $this->Jobpost_Model->level_lists();
        $data['industries'] = $this->Common_Model->industry_lists();
        $data['topbpos'] = $this->Common_Model->bpo_lists();
        $data["intrestedin"] = $this->Jobpost_Model->filtersubcategory_listsbycatid(9);

        $benefits_array = array();

        $intestedin_lists = $this->Common_Model->jobbenefits_list();
        if($intestedin_lists) {
            
            foreach ($intestedin_lists as $intestedin_list) {
                
                $benefits_array[] = [
                      "id" => $intestedin_list['id'],
                      "name" => $intestedin_list['sub_benefits']
                  ];
            }
        }

        $data["jobbenefits"] = $benefits_array;
        
        $this->load->view('new_design_register_two', $data);
    }

    public function signupsteptwo() {

        $this->session->unset_userdata('usersocialsession');

        $userData = $this->input->post();

        $this->form_validation->set_rules('expyear', 'Experience in year', 'trim|required');
        $this->form_validation->set_rules('expmonth', 'Experience in month', 'trim|required');
        $this->form_validation->set_rules('bpoyear', 'Experience in year', 'trim|required');
        $this->form_validation->set_rules('bpomonth', 'Experience in month', 'trim|required');
        $this->form_validation->set_rules('topbpo', 'Your Current/Last BPO', 'trim|required');
        $this->form_validation->set_rules('education', 'Highest Qualification', 'trim|required');
        $this->form_validation->set_rules('joblevel', 'Job Level', 'trim|required');
        $this->form_validation->set_rules('industry', 'Industry', 'trim|required');
        $this->form_validation->set_rules('specialization', 'Specialization', 'trim|required');
        $this->form_validation->set_rules('sub_specialization', 'Sub-Specialization', 'trim|required');
        $this->form_validation->set_rules('option1', 'Terms & Conditions and Privacy Policy', 'trim|required');

        if($userData['topbpo'] == "other") {
            $this->form_validation->set_rules('topbpoother', 'Your Current/Last BPO Other', 'trim|required');
        }
        if (!empty($_FILES['resumeFile']['name'])) {

            $this->form_validation->set_rules('resumeFile', '', 'callback_file_check');
        }
        if ($this->form_validation->run() == FALSE) {

            $data['userData'] = $userData;
            $data['categories'] = $this->Jobpost_Model->category_lists();
            $data['levellists'] = $this->Jobpost_Model->level_lists();
            $data['industries'] = $this->Common_Model->industry_lists();
            $data['topbpos'] = $this->Common_Model->bpo_lists();
            $data["intrestedin"] = $this->Jobpost_Model->filtersubcategory_listsbycatid(9);
            $data['signuperrors'] = $this->form_validation->error_array();

            $benefits_array = array();
            $intestedin_lists = $this->Common_Model->jobbenefits_list();
            if($intestedin_lists) {
                
                foreach ($intestedin_lists as $intestedin_list) {
                    
                    $benefits_array[] = [
                          "id" => $intestedin_list['id'],
                          "name" => $intestedin_list['sub_benefits']
                      ];
                }
            }

            $data["jobbenefits"] = $benefits_array; 

            $this->load->view('new_design_register_two', $data);

        } else {

            if (!empty($_FILES['resumeFile']['name'])) {
                $config = array('upload_path' => "./files/", 'allowed_types' => "pdf|doc|docx");
                $this->load->library('upload', $config);
                if ($this->upload->do_upload('resumeFile')) {
                    $data = array('upload_data' => $this->upload->data());
                    $resumePath = base_url() . "files/" . $data['upload_data']['file_name'];
                }
            } else {
                $resumePath = "";
            }

            if($userData['topbpo'] == "other") {
                $userData['topbpo'] = $userData['topbpoother'];
            }

            if($userData['newsletters'] == 1) {
                $userData['newsletters'] = 1;
            } else {
                $userData['newsletters'] = 0;
            }

            $data2 = ['expyear' => $userData['expyear'], 'expmonth' => $userData['expmonth'], 'bpoyear' => $userData['bpoyear'], 'bpomonth' => $userData['bpomonth'], 'topbpo' => $userData['topbpo'], 'education' => $userData['education'], 'jobLevel' => $userData['joblevel'], 'industry' => $userData['industry'], 'superpower' => $userData['specialization'], 'specialization' => $userData['specialization'], 'sub_specialization' => $userData['sub_specialization'], 'intrested'=>$userData['intrested'], 'benefits'=>$userData['benefits'],'newsletters'=>$userData['newsletters'], "resumePath"=>$resumePath];

            $signupone = $this->session->userdata('sessionone');
            $mobileNumber = $signupone['country_code'].$signupone['phone'];
            $phoneMSG = "Your verification OTP is " . $signupone['verify_code'];

            $subject = "JobYoDA! Verification code";
            $data = ["username"=>$signupone['name'], "code_otp"=>$phoneMSG];
            $msgEmailText = $this->load->view('recruiter/userverificationemail', $data, TRUE);
            $this->load->library('email');
            $this->email->initialize([
                    'protocol' => 'smtp',
                    'smtp_host' => 'smtpout.asia.secureserver.net',
                    'smtp_user' => 'otp@jobyoda.com',
                    'smtp_pass' => 'Jobyoda@2k21',
                    'smtp_port' => 465,
                    'smtp_crypto' => 'ssl',
                    'charset'=>'utf-8',
                    'mailtype' => 'html',
                    'crlf' => "\r\n",
                    'newline' => "\r\n"
            ]);
            $this->email->from('help@jobyoda.com', "JobYoDA");
            $this->email->to($signupone['email']);
            $this->email->subject($subject);
            $this->email->message($msgEmailText);
            $this->email->send();
            
            //$this->SendAwsSms($mobileNumber, $phoneMSG);
            $insertDataOne = [
                              'name' => $signupone['name'], 
                              'email' => $signupone['email'], 
                              'country_code' => $signupone['country_code'], 
                              'phone' => $signupone['phone'],
                              'password' => $signupone['password'], 
                              'type' => $signupone['type'], 
                              'token' => $signupone['token'], 
                              'socialToken'=>$signupone['socialToken'], 
                              'location' => $signupone['location'], 
                              'status' => $signupone['status'],

                              'exp_month' => $data2['expmonth'],
                              'exp_year' => $data2['expyear'],
                              'education' => $data2['education'],
                              'nationality' => $signupone['nationality'], 
                              'verify_status' => $signupone['verify_status'], 
                              'verify_code' => $signupone['verify_code'],
                              'referral_used'=>$signupone['referral_used'],
                              'platform' => 'web',
                              "state"=> $signupone['state'], 
                              "city"=> $signupone['city'],
                              "jobsInterested" => implode(',', $data2['intrested']), 
                              "jobsbenefits" => implode(',', $data2['benefits']), 
                              'timeforcall'=>$signupone['timeforcall'],
                              'bpoyear'=>0,
                              'bpomonth'=>0,
                              'topbpo' => $data2['topbpo'],
                              'industry' => $data2['industry'],
                              'superpower' => $data2['specialization'],
                              'specialization' => $data2['specialization'],
                              'sub_specialization' => $data2['sub_specialization'],
                              'jobLevel' => $data2['jobLevel'],
                              'newsletters' => $data2['newsletters']
                        ];

            $userInserted = $this->User_Model->user_insert($insertDataOne);
            if(strlen($resumePath) > 0) {
                $this->User_Model->user_resume_insert($resumePath, $userInserted);
                $resume_percent = 10;
            } else {
                $resume_percent = 0;
            }

            $comProfile = ["user_id"=>$userInserted, "personal"=>10,"proffesional"=>10, "resume" => $resume_percent];
            $this->User_Model->insert_profileComplete($comProfile);
            $this->session->unset_userdata('sessionone');
            $this->session->set_userdata('sessionverify', ["id"=>$userInserted, "name"=>$signupone['name'], "email"=>$signupone['email'], "country_code"=>$signupone['country_code'], "phone"=>$signupone['phone'], "type"=>$signupone['type']]);
            redirect("verication_account");
            exit;
        }
    }

    public function verification() {

        if($this->session->userdata('sessionagainverify')) {

        } else if($this->session->userdata('sessionsuccess')) {

            redirect('other_details');

        } else {

            if(!$this->session->userdata('sessionone') && !$this->session->userdata('sessionverify')) {
                redirect('signup');
            
            } else if(!$this->session->userdata('sessionverify')) {

                if($this->session->userdata('sessionagainverify')) {

                } else {
                    redirect('signup_step_two');
                }
            }
        }

        if($this->session->userdata('sessionverify')) {
            $signupone = $this->session->userdata('sessionverify');
            $data['phone_number'] = $signupone['phone'];
            $data['email'] = $signupone['email'];
        
        } else {
            $sessData = $this->session->userdata('sessionagainverify');

            $re_verify_code = rand(1000, 9999);

            $condition_array = array('phone' => $sessData['phone'], "email"=>$sessData['email'], "type"=>$sessData['type']);
            $data_array = array('verify_code' => $re_verify_code);
            $update = $this->User_Model->update_data('user', $condition_array, $data_array);

            $mobileNumber = $sessData['country_code'].$sessData['phone'];
            $phoneMSG = "Your verification OTP is " . $re_verify_code;

            $subject = "JobYoDA! Verification code";
            $data = ["username"=>$sessData['name'], "code_otp"=>$phoneMSG];
            $msgEmailText = $this->load->view('recruiter/userverificationemail', $data, TRUE);
            $this->load->library('email');
            $this->email->initialize([
                    'protocol' => 'smtp',
                    'smtp_host' => 'smtpout.asia.secureserver.net',
                    'smtp_user' => 'otp@jobyoda.com',
                    'smtp_pass' => 'Jobyoda@2k21',
                    'smtp_port' => 465,
                    'smtp_crypto' => 'ssl',
                    'charset'=>'utf-8',
                    'mailtype' => 'html',
                    'crlf' => "\r\n",
                    'newline' => "\r\n"
            ]);
            $this->email->from('help@jobyoda.com', "JobYoDA");
            $this->email->to($sessData['email']);
            $this->email->subject($subject);
            $this->email->message($msgEmailText);
            $this->email->send();

            //$this->SendAwsSms($mobileNumber, $phoneMSG);
            $data['phone_number'] = $sessData['phone'];
            $data['email'] = $sessData['email'];
            $this->session->set_userdata('sessionverify', ["id"=>$sessData['id'], "email"=>$sessData['email'], "phone"=>$sessData['phone'], "type"=>$sessData['type']]);
            $this->session->unset_userdata('sessionagainverify');
        }

        $data['captcha_img'] = $this->generatecaptcha();
        $this->load->view('new_design_otp', $data);
    }

    public function generatecaptcha() {
        
        $random = md5(rand());
        $captcha_code = substr($random, 0, 6);
        $this->session->set_userdata('captcha_code', ["code"=>$captcha_code]);
        return $captcha_code;
        die;
    }

    public function regeneratecaptcha() {
        
        $random = md5(rand());
        $captcha_code = substr($random, 0, 6);
        $this->session->set_userdata('captcha_code', ["code"=>$captcha_code]);
        echo $captcha_code;
        die;
    }

    public function validatingcaptcha() {

        $captcha = $this->input->post('captcha');
        $saved_captcha = $this->session->userdata('captcha_code');
        if($captcha == $saved_captcha['code']) {
            echo 1;
            die;
        } else {
            echo 0;
            die;
        }
    }

    public function resendotp() {
        $signupone = $this->session->userdata('sessionverify');
        $mobile_number = $signupone['phone'];
        $data['phone_number'] = $signupone['phone'];

        $re_verify_code = rand(1000, 9999);

        $condition_array = array('phone' => $signupone['phone'], "email"=>$signupone['email'], "type"=>$signupone['type']);
        $data_array = array('verify_code' => $re_verify_code);
        $update = $this->User_Model->update_data('user', $condition_array, $data_array);

        $mobileNumber = $signupone['country_code'].$signupone['phone'];
        $phoneMSG = "Your verification OTP is " . $re_verify_code;

        $subject = "JobYoDA! Verification code";
        $data = ["username"=>$signupone['name'], "code_otp"=>$phoneMSG];
        $msgEmailText = $this->load->view('recruiter/userverificationemail', $data, TRUE);
        $this->load->library('email');
        $this->email->initialize([
                'protocol' => 'smtp',
                'smtp_host' => 'smtpout.asia.secureserver.net',
                'smtp_user' => 'otp@jobyoda.com',
                'smtp_pass' => 'Jobyoda@2k21',
                'smtp_port' => 465,
                'smtp_crypto' => 'ssl',
                'charset'=>'utf-8',
                'mailtype' => 'html',
                'crlf' => "\r\n",
                'newline' => "\r\n"
        ]);
        $this->email->from('help@jobyoda.com', "JobYoDA");
        $this->email->to($signupone['email']);
        $this->email->subject($subject);
        $this->email->message($msgEmailText);
        $this->email->send();

        //$this->SendAwsSms($mobileNumber, $phoneMSG);

        echo "true";
        exit;
    }

    public function verifyotp() {
        $signupone = $this->session->userdata('sessionverify');
        $mobile_number = $signupone['phone'];
        $data['phone_number'] = $signupone['phone'];

        $otp1 = $this->input->post('number1');
        $otp = $otp1;

        $mobileCheck = $this->User_Model->mobile_check($mobile_number, $signupone['type']);

        if ($otp == '') {
            $data['captcha_img'] = $this->generatecaptcha();
            $data['error'] = '<span style="color:red">Please enter verification code!!</span>';
            $this->load->view('new_design_otp', $data);

        } else if ($mobileCheck[0]['verify_code'] != $otp) {
            $data['captcha_img'] = $this->generatecaptcha();
            $data['error'] = '<span style="color:red">Please enter correct verification code!!</span>';
            $this->load->view('new_design_otp', $data);

        } else {
            $condition_array = array('phone' => $mobile_number);
            $data_array = array('verify_status' => 1);
            $update = $this->User_Model->update_data('user', $condition_array, $data_array);
            $this->session->set_userdata('sessionsuccess', $signupone);
            $this->session->unset_userdata('sessionverify');
            
            redirect('other_details');
            exit;
        }
    }

    public function otherdetails() {

        $this->load->view('new_design_register_three');
    }

    public function otherdetailupdate() {
        
        $signupone = $this->session->userdata('sessionsuccess');

        $userData = $this->input->post();

        $this->form_validation->set_rules('jobsearch', 'Job Search', 'trim|required');
        $this->form_validation->set_rules('internetspeed', 'Speed', 'trim|required');
        $this->form_validation->set_rules('current_salary', 'Current Salary', 'trim|required');
        $this->form_validation->set_rules('timeforcall', 'Best time to call', 'trim|required');
        $this->form_validation->set_rules('work_mode', 'Preferred work mode', 'trim|required');
        $this->form_validation->set_rules('vaccination', 'Vaccination status', 'trim|required');
        $this->form_validation->set_rules('relocate', 'Willing to relocate', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {

            $data['userData'] = $userData;
            $data['signuperrors'] = $this->form_validation->error_array(); 

            $this->load->view('new_design_register_three', $data);
        } else {

            $data = ['jobsearch_status' => $userData['jobsearch'], 'internetspeed' => $userData['internetspeed'], 'current_salary' => $userData['current_salary'], 'timeforcall' => $userData['timeforcall'], 'work_mode' => $userData['work_mode'], 'vaccination' => $userData['vaccination'], 'relocate' => $userData['relocate'], 'last_used' => date('Y-m-d H:i:s')];

            $this->User_Model->user_update($data, $signupone['id']);

            $data = ["email" => $signupone['email'], "type"=>$signupone['type']];
            $userCheck = $this->User_Model->user_login($data);
            $getData = $this->fetchUserSingleData($userCheck[0]['id']);
            $this->session->set_userdata('usersess', $getData);
            $getCompleteness = $this->User_Model->check_profileComplete($signupone['id']);
            if($getCompleteness) {
                    
                $comProfile = ["other"=>10,"other_more"=>10,"dynamic_resume"=>10];
                $this->User_Model->update_profileComplete($comProfile, $signupone['id']);
            
            } else {

                $comProfile = ["user_id"=>$signupone['id'], "personal"=>10,"proffesional"=>10, "other"=>10,"other_more"=>10,"resume"=>0,"dynamic_resume"=>10];
                $this->User_Model->insert_profileComplete($comProfile);
            }

            $this->successsignupone();
            redirect('resume');
        }
    }


    public function successsignupone() {

        $userSess = $this->session->userdata('usersess');
        $dataToken = ["id" => $userSess['id']];
        $userInfo = $this->User_Model->user_match($dataToken);

        $dynamicResumeCheck = $this->User_Model->user_dynamic_resume_single($userInfo[0]['id']);
        if($dynamicResumeCheck) {

        } else {

            $data['profileDetail'] = $this->fetchUserSingleData($userInfo[0]['id']);
            $data['strengthdata'] = $this->User_Model->user_more($userInfo[0]['id']);
            $data["phonecodes"] = $this->Common_Model->phonecode_lists();
            $data['languageData'] = $this->User_Model->lang_single($userInfo[0]['id']);
            $data['clientData'] = $this->User_Model->client_single($userInfo[0]['id']);
            $data['skillData'] = $this->User_Model->skills_single($userInfo[0]['id']);
            $data['assessData'] = $this->User_Model->user_assessment($userInfo[0]['id']);
            $data['expertData'] = $this->User_Model->user_expert($userInfo[0]['id']);
            $data['industrylists'] = $this->Common_Model->industry_lists();
            $data['levellist'] = $this->Common_Model->level_lists();
            $data['categorylist'] = $this->Common_Model->category_lists();
            $data['subcategorylist'] = $this->Common_Model->subcategory_lists();
            $data['subcategorylist1'] = $this->Common_Model->subcategory_lists1();
            $data['channellists'] = $this->Common_Model->channel_lists();
            $data['channellistss'] = $this->Common_Model->channel_lists();
            $data["nations"] = $this->Common_Model->nation_lists();
            $data["powers"] = $this->Common_Model->power_lists();
            $data['workData'] = $this->fetchWorkSingleData($userInfo[0]['id']);
            $data['industries'] = $this->Common_Model->industry_lists();
            $data['topbpos'] = $this->Common_Model->bpo_lists();
            $data['getCompletenes'] = $this->fetchUserCompleteData($userInfo[0]['id']);
            $data['languagelists'] = $this->Common_Model->language_lists();
            $data['eduData'] = $this->fetchEduSingleData($userInfo[0]['id']);
            $data['checkResume'] = $this->User_Model->resume_single($userInfo[0]['id']);
            $data['checkStatus'] = $this->User_Model->user_single($userInfo[0]['id']);
            $data["intrestedin"] = $this->Jobpost_Model->filtersubcategory_listsbycatid(9);
            $data["states"] = $this->User_Model->getownstate();

            $this->load->library('pdf');
            $html = $this->load->view('dynamic_resume', $data, true);

            $filename = md5(rand()). '.pdf';
            $file_name = "./dynamic_resume/".$filename;
            $file_name_one = "/dynamic_resume/".$filename;
            $this->pdf->loadHtml($html);
            $this->pdf->setPaper('A4', 'portrait');
            $this->pdf->render();
            $file = $this->pdf->output();

            file_put_contents($file_name,  $file);
        
            $dataResume = ["user_id"=>$userInfo[0]['id'], "resume"=>$file_name_one];
            $this->User_Model->user_dynamic_resume($dataResume);
        
            //$this->pdf->stream($file_name, array("Attachment"=>0));

            $this->load->library('email');
            $config=array(
                'charset'=>'utf-8',
                'wordwrap'=> TRUE,
                'mailtype' => 'html'
            );
            $email_data = ["email"=>$data['profileDetail']['email'], "name"=>$data['profileDetail']['name'], "resume_path"=> base_url().$file_name];
            $msg = $this->load->view('resume_email', $email_data, TRUE);

            $this->email->initialize([
                          'protocol' => 'smtp',
                          'smtp_host' => 'smtpout.asia.secureserver.net',
                          'smtp_user' => 'jyresume@jobyoda.com',
                          'smtp_pass' => 'Jobyoda@2k21',
                          'smtp_port' => 465,
                          'smtp_crypto' => 'ssl',
                          'charset'=>'utf-8',
                          'mailtype' => 'html',
                          'crlf' => "\r\n",
                          'newline' => "\r\n"
            ]);
            $this->email->from('help@jobyoda.com', "JobYoDA");
            $this->email->to($data['profileDetail']['email']);
            $this->email->subject('JobYoDA Free Resume');
            $this->email->message($msg);
            $this->email->send();

        }
        return true;
    }

    public function forgotpassword() {
        
        $this->load->view('new_design_forogt_password');
    }

    public function setpassword() {
        
        $this->load->view('new_design_set_password');
    }

    public function successreset() {
        
        $this->load->view('new_design_success_reset');
    }

    public function getownstate() {
        $data = $this->input->post();
        $result = $this->User_Model->getowncity($data['state']);
        if ($result) {
            echo "<option value=''>Select City</option>";
            foreach ($result as $resultcity) {
                echo "<option value='" . $resultcity['city'] . "'>" . $resultcity['city'] . "</option>";
            }
        } else {
            echo "<option value=''>Select City</option>";
        }  
    }

    public function getjobintrestedin() {
        $data = $this->input->post();
        $result = $this->User_Model->getintrestedin($data['industry']);

        if ($result) {

            foreach ($result as $resultcity) {
                echo "<option value='" . $resultcity['name'] . "'>" . $resultcity['name'] . "</option>";
            }
        } else {
            echo "<option value=''> Select </option>";
        }  
        exit;
    }

    public function getcategory() {
        $data = $this->input->post();
        $result = $this->User_Model->getcat($data['industry']);

        if ($result) {
            echo "<option value=''> Select Your Specialization </option>";
            foreach ($result as $resultcity) {
                echo "<option value='" . $resultcity['category'] . "'>" . $resultcity['category'] . "</option>";
            }
        } else {
            echo "<option value=''> Select Your Specialization </option>";
        }  
        exit;
    }

    public function getsubcategory() {
        $data = $this->input->post();

        $getcatid = $this->User_Model->getsubcatid($data['cat'],$data['industry']);
   
        $result = $this->User_Model->getsubcat($getcatid[0]['id']);

        if ($result) {
            echo "<option value=''> Select Your Sub Specialization </option>";
            foreach ($result as $resultcity) {
                echo "<option value='" . $resultcity['subcategory'] . "'>" . $resultcity['subcategory'] . "</option>";
            }
        } else {
            echo "<option value=''> Select Your Sub Specialization </option>";
        }  
        exit;
    }

    public function webemail_check($email) {

        $emailMatch = $this->User_Model->checkwebemail($email);
        if ($emailMatch) {

            if($emailMatch[0]['type'] == "normal") {

                $this->form_validation->set_message('webemail_check', 'This email already exists');
                return FALSE;

            } else if($emailMatch[0]['type'] == "gmail" || $emailMatch[0]['type'] == "google" || $emailMatch[0]['type'] == "facebook") {

                if(!empty($emailMatch[0]['education']) && !empty($emailMatch[0]['superpower']) && !empty($emailMatch[0]['location'])) {

                    $this->form_validation->set_message('webemail_check', 'This email already exists with social account');
                    return FALSE;
                } else {

                    return TRUE;       
                }
            } else {

                $this->form_validation->set_message('webemail_check', 'This email already exists');
                return FALSE;
            }
        } else {
            return TRUE;
        }
    }

    public function webphone_check($phone) {

        $emailMatch = $this->User_Model->checkwebphone($phone);
        if ($emailMatch) {

            if($emailMatch[0]['type'] == "normal") {

                $this->form_validation->set_message('webphone_check', 'This phone number already exists');
                return FALSE;

            } else if($emailMatch[0]['type'] == "gmail" || $emailMatch[0]['type'] == "google" || $emailMatch[0]['type'] == "facebook") {

                if(!empty($emailMatch[0]['education']) && !empty($emailMatch[0]['superpower']) && !empty($emailMatch[0]['location'])) {

                    $this->form_validation->set_message('webphone_check', 'This phone number already exists with social account');
                    return FALSE;
                } else {

                    return TRUE;       
                }
            } else {

                $this->form_validation->set_message('webphone_check', 'This phone number already exists');
                return FALSE;
            }
        } else {
            return TRUE;
        }
    }

    public function valid_password($password = '') {
        $password = trim($password);
        $regex_lowercase = '/[a-z]/';
        $regex_uppercase = '/[A-Z]/';
        $regex_number = '/[0-9]/';
        $regex_special = '/[!@#$%^&*()\-_=+{};:,<.>§~]/';

        if (empty($password)){
            $this->form_validation->set_message('valid_password', 'Password is required.');
            return FALSE;
        }

        // if (preg_match_all($regex_lowercase, $password) < 1){
        //     $this->form_validation->set_message('valid_password', 'Password should be mixture of alphabets and numeric characters.');
        //     return FALSE;
        // }

        // if (preg_match_all($regex_uppercase, $password) < 1){
        //     $this->form_validation->set_message('valid_password', 'Password should be mixture of alphabets and numeric characters.');
        //     return FALSE;
        // }

        // if (preg_match_all($regex_number, $password) < 1) {
        //     $this->form_validation->set_message('valid_password', 'Password should be mixture of alphabets and numeric characters.');
        //     return FALSE;
        // }

        /*if (preg_match_all($regex_special, $password) < 1){
            $this->form_validation->set_message('valid_password', 'Password should be mixture of alphabets,numeric and special characters.');
            return FALSE;
        }*/
        /*if (strlen($password) < 5){
            $this->form_validation->set_message('valid_password', 'Password must be at least 5 characters in length.');
            return FALSE;
        }
        if (strlen($password) > 32){
            $this->form_validation->set_message('valid_password', 'Password cannot exceed 32 characters in length.');
            return FALSE;
        }*/
        return TRUE;
    }  

    function check_default($intrested) {
        $choice = $intrested;
        if(is_null($choice)) {
            $choice = array();
        }
        $travel_cat = implode(',', $choice);

        if($travel_cat != '')
            return true;
        else
            $this->form_validation->set_message('intrested', 'Intrested in field is required.');
            return FALSE;   
    }

    public function file_check($str){
        $allowed_mime_type_arr = array('application/pdf', 'application/doc', 'application/docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        $mime = get_mime_by_extension($_FILES['resumeFile']['name']);
        
        if(isset($_FILES['resumeFile']['name']) && $_FILES['resumeFile']['name']!=""){
            
            if(in_array($mime, $allowed_mime_type_arr)) {
                return true;
            
            }else{
                $this->form_validation->set_message('file_check', 'Please select only pdf/Doc file.');
                return false;
            }

        }else{
            $this->form_validation->set_message('file_check', 'Please choose a file to upload.');
            return false;
        }
    }

    public function SendAwsSms($to,$sms_message) {
      require 'vendor/autoload.php';
        try {
            $params = array(
                    'credentials'=> array(
                    'key'=>'AKIA2PEGBF6OC7RXCBWM',
                    'secret'=>'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',
                ),
                'region'=>'us-west-2',
                'version'=>'latest'
            );
            $sns = new \Aws\Sns\SnsClient($params);
            $args = array(
                "SenderID"=>"JobYoDA",
                "SMSType"=>"Transactional",
                "Message"=>$sms_message,
                "PhoneNumber"=>$to,
            );
            $result = $sns->publish($args);
            return true;
        } catch(Exception $e) {
            return true;
        }
    }

    public function fetchUserSingleData($id) {
        $userData = $this->User_Model->user_single($id);

        $intrestedGET = explode(',',$userData[0]['jobsInterested']);
        $intrestedGETNew = array();
        foreach($intrestedGET as $intrestedG) {
            $intrestedGETNew[] = $intrestedG;
        }

        $data = ["token" => $userData[0]['token'], "id" => $userData[0]['id'], "name" => $userData[0]['name'], "email" => $userData[0]['email'], "type" => $userData[0]['type'], "profilePic" => $userData[0]['profilePic'], "phone" => $userData[0]['phone'], "country_code" => $userData[0]['country_code'], "dob" => $userData[0]['dob'], "location" => $userData[0]['location'], "designation" => $userData[0]['designation'], "current_salary" => $userData[0]['current_salary'], "exp_month" => $userData[0]['exp_month'], "exp_year" => $userData[0]['exp_year'],"education" => $userData[0]['education'],"nationality" => $userData[0]['nationality'],"superpower" => $userData[0]['superpower'],"jobsInterested" => $intrestedGETNew,"state" => $userData[0]['state'],"city" => $userData[0]['city'], "timeforcall" => $userData[0]['timeforcall'], "bpoyear" => $userData[0]['bpoyear'], "bpomonth" => $userData[0]['bpomonth'], "jobsearch_status" => $userData[0]['jobsearch_status'], "topbpo" => $userData[0]['topbpo'], "industry" => $userData[0]['industry'], "specialization" => $userData[0]['specialization'], "sub_specialization" => $userData[0]['sub_specialization'], "haveinternet" => $userData[0]['haveinternet'], "internetspeed" => $userData[0]['internetspeed'], "howhear" => $userData[0]['howhear']];
        
        return $data;
    }

    public function fetchUserSingleData1($email) {
        $userData = $this->User_Model->user_single1($email);
        $data = ["id" => $userData[0]['id'], "token" => $userData[0]['token'], "name" => $userData[0]['name'], "email" => $userData[0]['email'], "type" => $userData[0]['type'], "profilePic" => $userData[0]['profilePic'], "phone" => $userData[0]['phone'], "dob" => $userData[0]['dob']];
        return $data;
    }

    public function fetchUserCompleteData($id) {
        $userData = $this->User_Model->userComplete_single($id);
        if ($userData) {
            $data['comp'] = $userData[0]['signup'] + $userData[0]['profile'] + $userData[0]['profilePic'] + $userData[0]['resume'] + $userData[0]['experience'] + $userData[0]['education'] + $userData[0]['assessment'] + $userData[0]['language'] + $userData[0]['expert'] + $userData[0]['topClient']+ $userData[0]['dob']+ $userData[0]['location']+ $userData[0]['designation']+ $userData[0]['current_salary']+ $userData[0]['exp'];
        } else {
            $data['comp'] = 0;
        }
        return $data;
    }

    public function fetchWorkSingleData($id) {
        $workDatas = $this->User_Model->work_single($id);
        if (!empty($workDatas)) {
            foreach ($workDatas as $workData) {
                $industryName = $this->User_Model->industryName_single($workData['industry']);
                $data[] = ["id" => $workData['id'], "title" => $workData['title'], "company" => $workData['company'], "desc" => $workData['jobDesc'], "from" => $workData['workFrom'], "to" => $workData['workTo'], "channel" => $workData['channel']];
            }
            return $data;
        } else {
            $data[] = "";
            return $data;
        }
    }

    public function fetchEduSingleData($id) {
        $eduDatas = $this->User_Model->edu_single($id);
        if ($eduDatas) {
            foreach ($eduDatas as $eduData) {
                $data[] = ["id" => $eduData['id'], "attainment" => $eduData['attainment'], "degree" => $eduData['degree'], "university" => $eduData['university'], "from" => $eduData['degreeFrom'], "to" => $eduData['degreeTo']];
            }
            return $data;
        } else {
            $data[] = "";
            return $data;
        }
    }
}
?>