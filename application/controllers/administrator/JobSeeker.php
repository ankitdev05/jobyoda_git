<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */
date_default_timezone_set('Asia/Manila');
define('API_ACCESS_KEY','AIzaSyCTzjJxETlJxp18hCwYHLFETLZRkbGFiGw');

class JobSeeker extends CI_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('newmodels/Jobseekeradmin_Model');
        $this->load->model('newmodels/Common_Model');
        $this->load->model('newmodels/User_Model');
        $this->load->model('newmodels/recruiter/Candidate_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');
        // Load Pagination library
        $this->load->library('pagination');
        $this->load->helper('url');
        if($this->session->userdata('adminSession')) {
            
        } else {
            redirect("administrator/admin/index");
        }
    }
    
    public function jobseekerListing1() {
        $listings = $this->Jobseekeradmin_Model->jobseeker_lists();
        $listget = array();

        if($listings) {
            foreach($listings as $listing) {
               $Listcount = $this->Jobseekeradmin_Model->jobseeker_detail_fetch($listing['id']);
               if(count($Listcount) >0){
                  $cc= 1;
               } else{
                  $cc=0;
               }
               $listget[] = ["name"=> $listing['name'],"phone"=> $listing['phone'] ,"app_version"=> $listing['app_version'] ,"last_used"=> $listing['last_used'] ,"platform"=> $listing['platform'] ,"countid"=>$listing['countid'], "active"=> $listing['active'], "email"=>$listing['email'],"education"=>$listing['education'],"location"=>$listing['location'],"nationality"=>$listing['nationality'],"superpower"=>$listing['superpower'],"exp_month"=>$listing['exp_month'],"exp_year"=>$listing['exp_year'], "id"=> $listing['id'],"created_at"=> $listing['created_at'], 'type'=>$listing['type'], 'device_type'=>$listing['device_type'], "haveList"=> $cc];
            }
        }
        $data["Lists"] = $listget;
        $this->load->view('administrator/jobseekerLists1', $data);
    }

    public function fetchJobseekerAjax(){
        $fetch_data = $this->Jobseekeradmin_Model->make_datatables();  
           $data = array();  
           $i=1;
           foreach($fetch_data as $row)  
           {    
                if($row->exp_year>1){ 
                    $row->exp_year = $row->exp_year.' Years '; 
                }else{ 
                    $row->exp_year = $row->exp_year.' Year ' ;
                }  
                if($row->exp_month>1){ 
                    $row->exp_month = $row->exp_month.' Months '; 
                }else{ 
                    $row->exp_month = $row->exp_month.' Month ';
                }

                if($row->bpoyear>1){ 
                    $row->bpoyear = $row->bpoyear.' Years '; 
                }else{ 
                    $row->bpoyear = $row->bpoyear.' Year ' ;
                }  
                if($row->bpomonth>1){ 
                    $row->bpomonth = $row->bpomonth.' Months '; 
                }else{ 
                    $row->bpomonth = $row->bpomonth.' Month ';
                }

                if(!empty($row->platform)){ 
                    if($row->platform=='ios'){  
                        $row->platform = "iOS";
                    } elseif ($row->platform=='android') {
                        $row->platform = "Android"; 
                    } 
                } else{
                        $row->platform = "Web";
                    }
                 if(!empty($row->type)){ 
                    if($row->type=='gmail'){ 
                        $row->type = "Gmail";
                    } elseif($row->type=='facebook'){ 
                        $row->type = "Facebook";
                    } else{
                        $row->type = "Normal";
                    }
                }

                if($row->newsletters == 1) {
                    $rownewsletters = "Yes"; 
                } else {
                    $rownewsletters = "No"; 
                }

                $a1 = '<a title="View" href="'.base_url().'administrator/JobSeeker/fetchJobseeker?id='. base64_encode($row->id).'" class="btn btn-icon usricos" aria-label="Product details"><i class="icon icon-eye s-4"></i></a>';
                if($row->active == 1){
                  $a2 = '<a title="Active" href="#" data-toggle="modal" id="'.$row->id.'" onclick="getaid(this.id)" data-target="#activeModal" class="btn btn-icon usricos" ><i class="icon icon-checkbox-marked-circle-outline s-4"></i></a>';               
                } else{ 
                   '<a title="Block" href="#" data-toggle="modal" id="'.$row->id.'" onclick="getbid(this.id)" data-target="#blockModal" class="btn btn-icon usricos" ><i class="icon icon-block-helper s-4"></i></a>'; 
                } 
                $a3 ='<a title="Delete" href="#" data-toggle="modal" id="'.$row->id.'" onclick="getrid(this.id)" data-target="#myModal1" class="btn btn-icon usricos" ><i class="icon s-4 icon-trash"></i></a>';
                $a4 = '<a title="App Sign in bonus" data-toggle="modal" id="'.$row->id.'" onclick="getBonus(this.id)" data-target="#modalBonus" class="btn btn-icon usricos fuse-ripple-ready" aria-label="Product details"><i class="icon icon-login s-4"></i></a>';
                $a5='<a title="Withdraw" href="#" data-toggle="modal" id="'.$row->id.'" onclick="getuid(this.id)" data-target="#myModal2" class="btn btn-icon usricos fuse-ripple-ready" aria-label="Product details"><i class="icon icon-looks s-4"></i></a>';
                $a6='<a title="Bonus History" href="#" data-toggle="modal" id="'.$row->id.'" onclick="getid2(this.id)" data-target="#myModal3" class="btn btn-icon usricos fuse-ripple-ready" aria-label="Product details"><i class="icon icon-history s-4"></i></a>';
                $sub_array = array();  
                $sub_array[] = $i;  
                $sub_array[] = $row->name;  
                $sub_array[] = $row->email;  
                $sub_array[] = $row->phone;  
                $sub_array[] = date('Y-m-d',strtotime($row->created_at));  
                $sub_array[] = $row->exp_year;
                $sub_array[] = $row->exp_month;
                $sub_array[] = $row->education;  
                $sub_array[] = $row->location;
                $sub_array[] = $row->state??' ';
                $sub_array[] = $row->city??' ';
                $sub_array[] = $row->jobsInterested??' ';
                $sub_array[] = $row->jobsbenefits??' ';
                $sub_array[] = $row->current_salary??' ';
                $sub_array[] = $rownewsletters;
                $sub_array[] = $row->jobLevel;  
                $sub_array[] = $row->industry;  
                $sub_array[] = $row->specialization;  
                $sub_array[] = $row->sub_specialization;
                $sub_array[] = $row->internetspeed;  
                $sub_array[] = $row->platform;  
                $sub_array[] = $row->app_version;  
                $sub_array[] = $row->last_used;  
                $sub_array[] = $row->type;  
                $sub_array[] = $a1.$a2.$a3;  
                $sub_array[] = $a4;  
                $sub_array[] = $row->countid;  
                $sub_array[] = $a5;  
                $sub_array[] = $a6;   
                $data[] = $sub_array;
                $i++;  
           }  
           $output = array(  
                "draw"                    =>     intval($_POST["draw"]),  
                "recordsTotal"          =>      $this->Jobseekeradmin_Model->get_all_data(),  
                "recordsFiltered"     =>     $this->Jobseekeradmin_Model->get_filtered_data(),  
                "data"                    =>     $data  
           );  
           echo json_encode($output);
    }

    public function fetchnotificationAjax() {

        $fetch_data = $this->Jobseekeradmin_Model->make_datatables();  
           $data = array();  
           $i=1;
           foreach($fetch_data as $row)  
           {    
                if($row->exp_year>1){ 
                    $row->exp_year = $row->exp_year.' Years '; 
                }else{ 
                    $row->exp_year = $row->exp_year.' Year ' ;
                }  
                if($row->exp_month>1){ 
                    $row->exp_month = $row->exp_month.' Months '; 
                }else{ 
                    $row->exp_month = $row->exp_month.' Month ';
                }
                if(!empty($row->platform)){ 
                    if($row->platform=='ios'){  
                        $row->platform = "iOS";
                    } elseif ($row->platform=='android') {
                        $row->platform = "Android"; 
                    } 
                } else{
                        $row->platform = "Web";
                }  

                if(!empty($row->superpower)){
                    $row->superpower = $row->superpower;
                }
                else{
                    $row->superpower = '';
                }
                $sub_array = array();  
                $sub_array[] = '<input type="checkbox" name="check_users[]" class="check_users" value="'.$row->id.'">';  
                $sub_array[] = $i;  
                $sub_array[] = $row->name;  
                $sub_array[] = $row->email;  
                $sub_array[] = $row->phone;   
                $sub_array[] = $row->platform;   
                $sub_array[] = $row->last_used;  
                $sub_array[] = $row->app_version;    
                $sub_array[] = $row->superpower;    
                $sub_array[] = $row->location;  
                $sub_array[] = $row->current_salary;  
                $sub_array[] = $row->exp_year.' '.$row->exp_month;    
                $data[] = $sub_array;
                $i++;  
           }  
           $output = array(  
                "draw"                    =>     intval($_POST["draw"]),  
                "recordsTotal"          =>      $this->Jobseekeradmin_Model->get_all_data(),  
                "recordsFiltered"     =>     $this->Jobseekeradmin_Model->get_filtered_data(),  
                "data"                    =>     $data  
           );


           echo json_encode($output);
    }

    public function jobseekerListing() {
        
        $rowno = $this->uri->segment(3);
        $pagenumber = $rowno;
        // Search text
        $search_text = "";
        if($this->input->post('submit') == "submit") {

            $search_text = $this->input->post('search');

            $this->session->set_userdata(array("searchhh"=>$search_text));
        
        } else if($this->input->post('clear') == "clear") {
            
            $this->session->unset_userdata('searchhh');
            $search_text = "";
        } else {
            
            if($this->session->userdata('searchhh') != NULL){
                $search_text = $this->session->userdata('searchhh');
            }
        }

        // Row per page
        $rowperpage = 10;

        // Row position
        if($rowno != 0){
          $rowno = ($rowno-1) * $rowperpage;
        }
        // All records count
        $allcount = $this->Jobseekeradmin_Model->get_count_jobseekerrecord($search_text);
        //var_dump($allcount);die;
        // Get records
        $users_record = $this->Jobseekeradmin_Model->get_all_jobseekerrecord($rowno,$rowperpage,$search_text);

        // Pagination Configuration
        // $config['base_url'] = base_url().'administrator/jobseekerlist';
        // $config['use_page_numbers'] = TRUE;
        // $config['total_rows'] = $allcount;
        // $config['per_page'] = $rowperpage;
        // $config["uri_segment"] = 3;
        // $choice = $config["total_rows"]/$config["per_page"];
        // $config["num_links"] = floor($choice);

        // Initialize
        //$this->pagination->initialize($config);

        //$data['pagination'] = $this->pagination->create_links();

        $data['result'] = $users_record;
        $data['rowno'] = $rowno;
        $data['row'] = $pagenumber;
        $data['num_links'] = floor($allcount/$rowperpage);
        $data['row_count'] = $allcount;
        $data['search'] = $search_text;

        $this->load->view('administrator/jobseekerLists2', $data);
    }

    public function export_csv_report() {

        $filename = 'users_'.date('Ymd').'.csv'; 
        header("Content-Description: File Transfer"); 
        header("Content-Disposition: attachment; filename=$filename"); 
        header("Content-Type: application/csv; ");

        $search_text = "";
        if($this->session->userdata('search') != NULL) {
            $search_text = $this->session->userdata('search');
        }

        $users_record = $this->Jobseekeradmin_Model->get_search_jobseekerrecord($search_text);

        // file creation 
        $file = fopen('php://output','w');
        $header = array("SNo","Name","Email","Phone","Registration Date","Experience (Year)","Experience (Month)","Education", "Location","State","City","Intrested In","Benefits", "Salary", "Newsletter", "Job Level","Industry","Specialization","Sub specialization","25 MBPS Internet", "Work Mode", "Vaccination", "Relocate", "Platform","App Version","Last Used","Signup Via");
        fputcsv($file, $header);

        $x1 =1;
        $dataArray = array();
          
        foreach($users_record as $userList) {

                if($userList['phone']==0) {
                  $userList['phone']='';
                }else{
                  $userList['phone']=$userList['phone'];
                }
                
                if($userList['exp_year']>1){
                  $exp_year = $userList['exp_year'].' Years'; 
                }else{
                  $exp_year = $userList['exp_year'].' Year';
                }
                
                if($userList['exp_month']>1){
                  $exp_month = $userList['exp_month'].' Months'; 
                }else{
                  $exp_month = $userList['exp_month'].' Month';
                }
                if(!empty($userList['platform'])){
                  $userList['device_type']=$userList['platform'];
                }else{
                  $userList['device_type']='web';
                }

                if(!empty($userList['state'])){
                  $userList['state']=$userList['state'];
                }else{
                  $userList['state']=" ";
                }

                if(!empty($userList['city'])){
                  $userList['city']=$userList['city'];
                }else{
                  $userList['city']=" ";
                }

                if(!empty($userList['jobsInterested'])){
                  $userList['jobsInterested']=$userList['jobsInterested'];
                }else{
                  $userList['jobsInterested']=" ";
                }

                if(!empty($userList['app_version'])){
                  $userList['app_version']=$userList['app_version'];
                }else{
                  $userList['app_version']=" ";
                }

                if(!empty($userList['last_used'])){
                  $userList['last_used']=$userList['last_used'];
                }else{
                  $userList['last_used']=" ";
                }

                if($userList['newsletters'] == 1) {
                  $userList['newsletters'] = "Yes";
                } else {
                  $userList['newsletters'] = "No";
                }

                $dataArray[] = ["srno"=>$x1, 
                               "name"=>$userList['name'], 
                               "email"=>$userList['email'],
                               "phone"=>$userList['phone'],
                               "created_at"=>date('Y-m-d', strtotime($userList['created_at'])),
                               "experience_year"=>$exp_year,
                               "experience_month"=>$exp_month,
                               "education"=>$userList['education'],
                               "location"=>$userList['location'],
                               "state"=>$userList['state'],
                               "city"=>$userList['city'],
                               "jobsInterested"=>$userList['jobsInterested'],
                               "jobsbenefits"=>$userList['jobsbenefits'],
                               "current_salary"=>$userList['current_salary'],
                               "newsletters"=>$userList['newsletters'],
                               "joblevel"=>$userList['jobLevel'],
                               "industry"=>$userList['industry'],
                               "specialization"=>$userList['specialization'],
                               "sub_specialization"=>$userList['sub_specialization'],
                               "internetspeed"=>$userList['internetspeed'],
                               "work_mode"=>$userList['work_mode'],
                               "vaccination"=>$userList['vaccination'],
                               "relocate"=>$userList['relocate'],
                               "device_type"=>$userList['device_type'],
                               "app_version"=>$userList['app_version'],
                               "last_used"=>$userList['last_used'],
                               "type"=>$userList['type'],
                            ];
              $x1++;
        }
        
        foreach ($dataArray as $key=>$line) { 
          fputcsv($file,$line); 
        }
        fclose($file);
        exit; 
    }

    public function fetchbonusHistory(){
        $rid = $this->input->post('rid');
        $fetch_bonus_data = $this->Jobseekeradmin_Model->fetch_bonus($rid); 
        if(!empty($fetch_bonus_data)){
            $x1 =1;
          foreach($fetch_bonus_data as $List) {
            if($List['bonus']==0){
                $status="Dr";
            }
            if($List['bonus']>0){
                $status="Cr";
            }
           echo '<tr><td style="width:20px;">'.$x1.'</td><td style="width:20px;">'.$List['name'].'</td><td style="width:20px;">'.$List['bonus'].' '.$status.'</td><td style="width:20px;">'.$List['added_at'].
              '</td>   
              
           </tr>';
              $x1++;
           }
        }else{
            echo 'No data found';
        }             
    }


    public function contactlisting() {
        $data["Lists"] = $this->Jobseekeradmin_Model->contact_lists();
        //print_r($data["Lists"]);
        $this->load->view('administrator/contactlist', $data);
    }

    public function reconciliation() {
        $data["Lists"] = $this->Jobseekeradmin_Model->reconciliation_lists();
        //echo $this->db->last_query();die;
        $this->load->view('administrator/reconciliation', $data);
    }

    public function loadingscreen() {
        $id = base64_decode($this->input->get('id'));
        if(!empty($id)){
            $data['singlequote'] = $this->Jobseekeradmin_Model->singlequote($id); 
        }
        $data["Lists"] = $this->Jobseekeradmin_Model->quote_lists();
        //print_r($data["Lists"]);
        $this->load->view('administrator/loadingscreen', $data);
    }

    public function apiversioning(){
        $data['appversion']=$this->Jobseekeradmin_Model->fetchversion();
        $this->load->view('administrator/apiversioning',$data);
    }

    public function versionUpdate(){
        $userData = $this->input->post();
        $data = ['api_version'=>$userData['api_version'], 'recommend_update'=>$userData['recommend'], 'force_update'=>$userData['force'],'message'=>urlencode($userData['message'])]; 
        $updated = $this->Jobseekeradmin_Model->updateVersion($data);
        if($updated){
            redirect('administrator/JobSeeker/apiversioning');
        }
    }

    public function quoteInsert() {
        $userData = $this->input->post();
        $data['Lists'] = $this->Jobseekeradmin_Model->quote_lists();
        $this->form_validation->set_rules('title', 'Screen Name', 'trim|required');
        $this->form_validation->set_rules('quote', 'Description', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $this->load->view('administrator/loadingscreen',$data);
        } else {
            if(!empty($userData['quote_id'])){
                $data = ["title" => $userData['title'], "quote" => $userData['quote']];
                $this->Jobseekeradmin_Model->quote_update($data,$userData['quote_id']);
            }else{
                $quotesData = $this->Jobseekeradmin_Model->quotebyScreen($userData['title']);
                if(!empty($quotesData)){
                    $this->session->set_tempdata('quoterr','The message is already added for this screen',5);
                    redirect("administrator/JobSeeker/loadingscreen");        
                }else{
                    $data = ["title" => $userData['title'], "quote" => $userData['quote']];
                    $this->Jobseekeradmin_Model->quote_insert($data);   
                }
            }
            redirect("administrator/JobSeeker/loadingscreen");
        }
    }

    public function bonusamountInsert() {
        $date = date('Y-m-d H:i:s');
        $userId = $this->input->post('userId');
        $bonus_amount = $this->input->post('bonus_amount');
    
        $data = ["user_id" => $userId, "bonus" => $bonus_amount, "notification"=>'Congratulation you have received '.$bonus_amount.' Peso bonus.', "added_at"=> $date];
        $this->Jobseekeradmin_Model->bonusinert($data);
        $user_data = $this->User_Model->user_single($userId);
        $fetchnotifications = $this->Candidate_Model->fetchnotifications($userId);
        if($user_data[0]['sign_in_bonus']=='0'){
             $data1 = ["sign_in_bonus"=>$bonus_amount];
             $jobseekerInserted = $this->Jobseekeradmin_Model->jobseekerStatus($data1, $userId);
        }else{
             $fetchBonus = $user_data[0]['sign_in_bonus'];
             $newBonus = $fetchBonus+$bonus_amount;
             $data1 = ["sign_in_bonus"=>$newBonus];
             $jobseekerInserted = $this->Jobseekeradmin_Model->jobseekerStatus($data1, $userId);
        }
       
        $msg ="Congratulation you have received"." ". $bonus_amount ." "."Peso bonus.";
        
        if($user_data[0]['notification_status']=='1') {
            $tokendata = ['user_id'=>$userId];
            $userTokenCheck = $this->User_Model->user_token_check($tokendata);
            foreach ($userTokenCheck as $usertoken) {
                if($usertoken['device_type']=="ios"){
                    $this->pushiphon(array($usertoken['device_token'],$msg,'bonus', 'Sign In Bonus',count($fetchnotifications)));
                
                 } else {
                      $this->push_notification_android_customer($usertoken['device_token'],"Sign In Bonus", $msg,"bonus",count($fetchnotifications));
                }
             }
        }
        $to = $user_data[0]['country_code'].$user_data[0]['phone'];
        $sms_message = $msg;
        $this->sendSms($to,$sms_message);

        redirect("administrator/JobSeeker/jobseekerListing");
       
    }

    public function deleteJobSeeker() {
        $rid = $_GET['id'];
        $this->Jobseekeradmin_Model->delete_user($rid);
        $this->Jobseekeradmin_Model->delete_userAssessment($rid);
        $this->Jobseekeradmin_Model->delete_userEducation($rid);
        $this->Jobseekeradmin_Model->delete_userExpert($rid);
        $this->Jobseekeradmin_Model->delete_userForgot($rid);
        $this->Jobseekeradmin_Model->delete_userLanguage($rid);
        $this->Jobseekeradmin_Model->delete_userMoreDetails($rid);
        $this->Jobseekeradmin_Model->delete_userResume($rid);
        $this->Jobseekeradmin_Model->delete_userSkills($rid);
        $this->Jobseekeradmin_Model->delete_userTopclients($rid);
        $this->Jobseekeradmin_Model->delete_userWorkExperience($rid);
        $this->Jobseekeradmin_Model->delete_verificationEmail($rid);
        $this->Jobseekeradmin_Model->delete_token($rid);
        redirect("administrator/JobSeeker/jobseekerListing");
    }

    public function deleteJobSeekerQuery() {
        $rid = $_GET['id'];
        $this->Jobseekeradmin_Model->delete_userquery($rid);
        redirect("administrator/JobSeeker/contactlisting");
    }

    public function deleteQuote() {
        $rid = $_GET['id'];
        $this->Jobseekeradmin_Model->delete_quote($rid);
        redirect("administrator/JobSeeker/loadingscreen");
    }
    
    public function jobseekerBlock() {
        $rid = $_GET['id'];
        $data1 = ["active"=>0,"token"=>''];
        $jobseekerInserted = $this->Jobseekeradmin_Model->jobseekerStatus($data1, $rid);
        if($jobseekerInserted) {
            
            redirect("administrator/JobSeeker/jobseekerListing");
        } else {
            redirect("administrator/JobSeeker/jobseekerListing");
        }
        
    }
    
    public function jobseekerActive() {
        $rid = $_GET['id'];
        $data1 = ["active"=>1];
        $jobseekerInserted = $this->Jobseekeradmin_Model->jobseekerStatus($data1, $rid);
        if($jobseekerInserted) {
            
            redirect("administrator/JobSeeker/jobseekerListing");
        } else {
            redirect("administrator/JobSeeker/jobseekerListing");
        }
        
    }

    public function readquery(){
        $id = base64_decode($_GET['id']);
        $data1 = ["status"=>1];
        $jobseekerInserted = $this->Jobseekeradmin_Model->jobseekerQuery($data1, $id);
        //echo $this->db->last_query();die;
        if($jobseekerInserted) {
            
            redirect("administrator/JobSeeker/contactlisting");
        } else {
            redirect("administrator/JobSeeker/contactlisting");
        }
    }

    public function withdrawBonus() {
        $rid = $_GET['id'];
        $user_data = $this->User_Model->user_single($rid);
        $data1 = ["sign_in_bonus"=>0];
        $jobseekerInserted = $this->Jobseekeradmin_Model->jobseekerStatus($data1, $rid);
        $date = date('Y-m-d H:i:s');
        $data = ["user_id" => $rid, "bonus" => '0', "notification"=>'Money has been withdrawn', "added_at"=>$date];
        $this->Jobseekeradmin_Model->bonusinert($data);
        $msg ="Admin has been credited "." ".$user_data[0]['sign_in_bonus']." "."SignIn bonus";
        $fetchnotifications = $this->Candidate_Model->fetchnotifications( $user_data[0]['id']);
        if($user_data[0]['notification_status']=='1'){
            $tokendata = ['user_id'=> $user_data[0]['id']];
            $userTokenCheck = $this->User_Model->user_token_check($tokendata);
            //print_r($userTokenCheck);die;
            foreach ($userTokenCheck as $usertoken) {
                //print_r($usertoken);die;
                if($usertoken['device_type']=="ios"){
                    $this->pushiphon(array($usertoken['device_token'],$msg,'bonus','Sign In Bonus Withdrawn',count($fetchnotifications)));
                
                 } else {
                      $this->push_notification_android_customer($usertoken['device_token'],"Sign In Bonus Withdrawn", $msg,"bonus",count($fetchnotifications));
                }
             }
        }
        if($jobseekerInserted) {
            
            redirect("administrator/JobSeeker/jobseekerListing");
        } else {
            redirect("administrator/JobSeeker/jobseekerListing");
        }
        
    }
    
    public function fetchBonusAmount(){
        $rid = $this->input->post('rid');
        $data = $this->Jobseekeradmin_Model->jobseeker_detail_fetch($rid);
        echo $data[0]['sign_in_bonus'];
    }
    
    public function fetchJobseeker() {
        $userId = base64_decode($_GET['id']);
        $data['users'] = $this->Jobseekeradmin_Model->jobseeker_detail_fetch($userId);
        $data['usersExps'] = $this->Jobseekeradmin_Model->jobseeker_exp_fetch($userId);
        $data['usersEdus'] = $this->Jobseekeradmin_Model->jobseeker_edu_fetch($userId);
        $data['usersassis'] = $this->Jobseekeradmin_Model->jobseeker_assisment_fetch($userId);
        $data['usersexperts'] = $this->Jobseekeradmin_Model->jobseeker_expert_fetch($userId);
        $data['usersskills'] = $this->Jobseekeradmin_Model->jobseeker_skills_fetch($userId);
        
        $this->load->view('administrator/jobSeekerView', $data);
    }

    public function viewcontact(){
        $userId = base64_decode($_GET['id']);
        $data['users'] = $this->Jobseekeradmin_Model->contactbyuserid($userId);
        $this->load->view('administrator/contactview',$data);
    }

    
    public function boostamountInsert() {
        
        $boost_amount = $this->input->post('boost_amount');
    
        $data = ["boost_amount" => $boost_amount];
        $amount = $this->Jobseekeradmin_Model->getboostamount();
        if($amount) {
            $this->Jobseekeradmin_Model->boostupdate($data,$amount[0]['id']);
        }else{
            $this->Jobseekeradmin_Model->boostinert($data);
        }
        redirect("administrator/dashboard");
    }

    public function activelyamountInsert() {
        
        $actively_amount = $this->input->post('actively_amount');
    
        $data = ["amount" => $actively_amount];
        $amount = $this->Jobseekeradmin_Model->getactivelyamount();
        if($amount) {
            $this->Jobseekeradmin_Model->activelyupdate($data, $amount[0]['id']);
        }else{
            $this->Jobseekeradmin_Model->activelyinert($data);
        }
        redirect("administrator/dashboard");
    }



    public function notifications() {
        $data["Lists"] = $this->Jobseekeradmin_Model->jobseeker_lists();
        
        $this->load->view('administrator/notifications1', $data);
    }

    public function notifications1() {
        $data["Lists"] = $this->Jobseekeradmin_Model->jobseeker_lists();
        
        $this->load->view('administrator/notifications1', $data);
    }

    public function notificationslist() {
        $data["Lists"] = $this->Jobseekeradmin_Model->notification_lists();
        //echo $this->db->last_query();die;
        $notSendlistcount = $this->Jobseekeradmin_Model->notificationcount_notsend();
        $sendlistcount = $this->Jobseekeradmin_Model->notificationcount_send();
        $data["notsend"] = $notSendlistcount;
        $data["send"] = $sendlistcount;

        $this->load->view('administrator/notificationslist', $data);
    }

    public function jobnotificationslist() {
        $lists = $this->Jobseekeradmin_Model->jobrecordnotification_lists();
        //echo $this->db->last_query();die;
        foreach($lists as $list) {
            $inQue = 0;
            $listcount = $this->Jobseekeradmin_Model->jobnotificationcount_lists($list['id']);
            $notSendlistcount = $this->Jobseekeradmin_Model->jobnotificationcount_lists_notsend($list['id']);

            $data["Lists"][] = ["id"=>$list['id'], "job_id"=>$list['job_id'], "title"=>$list['title'], "notification"=> $list['notification'], "intrestedIn"=> $list['intrestedIn'], "superpower"=> $list['superpower'], "created_at"=> $list['created_at'], "listcount"=>$listcount[0]['countid'], "notsend"=>$notSendlistcount[0]['countid']]; 
        }
        $this->load->view('administrator/jobrecordnotificationslist', $data);
    }

    public function alljobnotificationslist() {
        $notifyId = base64_decode($_GET['id']);

        $data["Lists"] = $this->Jobseekeradmin_Model->jobnotification_lists($notifyId);
        //echo $this->db->last_query();die;
        $this->load->view('administrator/jobnotificationslist', $data);
    }

    public function send_notification(){
        $ids = json_decode($this->input->post('ids'));
        $title = $this->input->post('title');
        $message = $this->input->post('message');
        $save_status = $this->input->post('save_status');
        $superpower = $this->input->post('superpower');
        $location = $this->input->post('location');

        if(strlen($superpower) > 0 || strlen($location) > 0) {

            $getallusers = $this->Jobseekeradmin_Model->jobseeker_lists_by_filters($superpower, $location);
            foreach ($getallusers as $user) {
                $data = ['user_id'=>$user['id'], "title" => urlencode($title), "message" => urlencode($message), "save_status" => $save_status
                , "badge_count"=>1,"cron_status"=>1];
                $this->Jobseekeradmin_Model->promo_notification_insert($data);
            }

        } else {
        
            foreach ($ids as $user) {
                $data = ['user_id'=>$user, "title" => urlencode($title), "message" => urlencode($message), "save_status" => $save_status
                , "badge_count"=>1,"cron_status"=>1];
                $this->Jobseekeradmin_Model->promo_notification_insert($data);
            }
        }

        echo "notification send successfully";
        exit;
    }

    function push_notification_android_customer($regid,$title,$body,$type,$badgecount){
      //  echo $regid;die;
        $access_key = API_ACCESS_KEY;
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $notification = [
            'title' =>$title,
            'body' => $body,
            'type' => $type,
            'recruiter_id' => '12',
            'story_id' => '13',
            'companyname' => '21',
            'notification_id' => '52',
            'timestamp' => date('H:i:s'),
            'suggestid' => '2',
            'badgecount' => $badgecount,
            'promo_id' => '002'
        ];
        //$extraNotificationData = $notification;
        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to'        => $regid,
            //'notification' => $notification,
            'data' => $notification
        ];
        $headers1 = [
            'Authorization: key=' . $access_key,
            'Content-Type: application/json'
        ];
        //print_r($fcmNotification);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
      }

      function push_notification_ios_customer($regid,$title,$body,$type,$badgecount){
      //  echo $regid;die;
        $access_key = API_ACCESS_KEY1;
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $notification = [
            'title' =>$title,
            'body' => $body,
            'type' => $type,
            'recruiter_id' => '12',
            'story_id' => '13',
            'companyname' => '21',
            'notification_id' => '52',
            'timestamp' => date('H:i:s'),
            'suggestid' => '2',
            'badgecount' => $badgecount,
            'promo_id' => '002'
        ];
        //$extraNotificationData = $notification;
        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to'        => $regid,
            //'notification' => $notification,
            'data' => $notification
        ];
        $headers1 = [
            'Authorization: key=' . $access_key,
            'Content-Type: application/json'
        ];
        //print_r($fcmNotification);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
      }

      
      function pushiphon($array)       
      {                               
           $deviceToken = $array[0];           
           $passphrase = '1234';  
           $this->autoRender = false;
           $this->layout = false;                  
           $basePath = "JobYoda.pem";                             
           if(file_exists($basePath))             
           {       
           $ctx = stream_context_create(); 
           stream_context_set_option($ctx, 'ssl', 'local_cert', $basePath);
           stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
           $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,  
           $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx); 
           if (!$fp)     
           exit("Failed to connect: $err $errstr" . PHP_EOL);
          $body['aps'] = array('alert' => array('title' => $array[3], 'body'=> $array[1]), 'badge' => $array[4],'sound' =>'default' , 'apns-push-type' => 'alert');
          $body['body'] = $array[1];
          $body['type'] = $array[2];
          //echo"<pre>";print_r($body);die; 
          $payload = json_encode($body);
          //echo $payload;die; 
           $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
          $result = fwrite($fp, $msg, strlen($msg));

           if (!$result)
           {           
                    
           }     
            else  
           {    
            echo "<pre>";print_r($body['aps']); 
              
           }       
             fclose($fp); 
           }    
      }

      public function sendSms($to,$sms_message){

        $this->load->library('twilio');
        //$sms_sender = trim($this->input->post('sms_sender'));
        //$sms_reciever = $this->input->post('sms_recipient');
        //$sms_message = trim($this->input->post('sms_message'));
        //$from = '+'.$sms_sender; //trial account twilio number
        //$to = '+'.$sms_reciever; //sms recipient number
        $response = $this->twilio->sms("+12014823386",$to,$sms_message);
        //print_r($response);
        if($response->IsError){

        echo 'Sms Has been Not sent';
        }
        else{

        echo 'Sms Has been sent';
        }
        }
}
?>