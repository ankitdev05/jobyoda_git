<?php
ob_start();
ini_set('display_errors', 1);

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */
class Subscription extends CI_Controller {
    
    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('newmodels/Subscription_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');
        $this->load->model('newmodels/Recruiteradmin_Model');
        $this->load->model('newmodels/recruiter/Recruit_Model');
        $this->load->library('pdf');

        $this->session->unset_userdata('search');
        
        if($this->session->userdata('adminSession')) {
            
        } else {
            redirect("administrator/admin/index");
        }
    }

    public function subscriptionlist() {

        $data['subscriptions'] = $this->Subscription_Model->list();
        $this->load->view('administrator/subscriptions', $data);
    }

    public function subscriptionadd() {

        $this->load->view('administrator/subscription_add');
    }

    public function subscriptioncreate() {

        $userData = $this->input->post();
        
        $this->form_validation->set_rules('plan_name', 'Plan Name', 'trim|required');
        $this->form_validation->set_rules('duration', 'Duration', 'trim|required');
        $this->form_validation->set_rules('cost', 'Cost', 'trim|required');
        $this->form_validation->set_rules('total_mail', 'Total Mail', 'trim|required');
        $this->form_validation->set_rules('total_job_post', 'Total Job Post', 'trim|required');
        $this->form_validation->set_rules('total_resume_access', 'Total Resume Access', 'trim|required');
        $this->form_validation->set_rules('total_video_post', 'Total Video Post', 'trim|required');
        $this->form_validation->set_rules('total_advertise', 'Total Advertise', 'trim|required');
        $this->form_validation->set_rules('total_job_boost', 'Total Job Boost', 'trim|required');
        $this->form_validation->set_rules('total_candidate_profile', 'Total Candidate Profile', 'trim|required');
        $this->form_validation->set_rules('plan_image', 'Plan Image', 'trim|required');
        $this->form_validation->set_rules('status', 'Status', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            
            $data['errors'] = $this->form_validation->error_array();
            $data['userData'] = $userData;
            $this->load->view('administrator/subscription_add',$data);

        } else {

            $record = ["plan_name"=>$userData['plan_name'], 'duration'=>$userData['duration'], 'cost'=>$userData['cost'] , 'total_mail'=>$userData['total_mail'], "total_job_post"=> $userData['total_job_post'], 'total_resume_access'=>$userData['total_resume_access'], "total_video_post"=> $userData['total_video_post'], "total_advertise"=> $userData['total_advertise'], "total_job_boost"=>$userData['total_job_boost'], "total_candidate_profile"=>$userData['total_candidate_profile'], "plan_image"=> $userData['plan_image'], "status"=>$userData['status']];

            $this->Subscription_Model->subscribe_insert($record);
            return redirect('administrator/subscription_plans');
        }
    }

    public function subscriptionedit() {
        $id = base64_decode($_GET['id']);
        
        $userData = $this->Subscription_Model->view($id);
        $data['userData'] = $userData[0];
        $data['id'] = base64_encode($id);
        $this->load->view('administrator/subscription_edit', $data);
    }

    public function subscriptionupdate() {
        $userData = $this->input->post();
        
        $this->form_validation->set_rules('plan_name', 'Plan Name', 'trim|required');
        $this->form_validation->set_rules('duration', 'Duration', 'trim|required');
        $this->form_validation->set_rules('cost', 'Cost', 'trim|required');
        $this->form_validation->set_rules('total_mail', 'Total Mail', 'trim|required');
        $this->form_validation->set_rules('total_job_post', 'Total Job Post', 'trim|required');
        $this->form_validation->set_rules('total_resume_access', 'Total Resume Access', 'trim|required');
        $this->form_validation->set_rules('total_video_post', 'Total Video Post', 'trim|required');
        $this->form_validation->set_rules('total_advertise', 'Total Advertise', 'trim|required');
        $this->form_validation->set_rules('total_job_boost', 'Total Job Boost', 'trim|required');
        $this->form_validation->set_rules('total_candidate_profile', 'Total Job Boost', 'trim|required');
        $this->form_validation->set_rules('plan_image', 'Plan Image', 'trim|required');
        $this->form_validation->set_rules('status', 'Status', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            
            $data['errors'] = $this->form_validation->error_array();
            $data['userData'] = $userData;
            $data['id'] = base64_encode($_GET['id']);
            $this->load->view('administrator/subscription_edit',$data);

        } else {

            $id = base64_decode($_GET['id']);

            $record = ["plan_name"=>$userData['plan_name'], 'duration'=>$userData['duration'], 'cost'=>$userData['cost'] , 'total_mail'=>$userData['total_mail'], "total_job_post"=> $userData['total_job_post'], 'total_resume_access'=>$userData['total_resume_access'], "total_video_post"=> $userData['total_video_post'], "total_advertise"=> $userData['total_advertise'], "total_job_boost"=>$userData['total_job_boost'], "total_candidate_profile"=>$userData['total_candidate_profile'], "plan_image"=> $userData['plan_image'], "status"=>$userData['status']];

            $this->Subscription_Model->subscribe_update($record, $id);
            return redirect('administrator/subscription_plans');
        }
    }

    public function deletePlan() {
        $rid = $_GET['id'];
        $this->Subscription_Model->delete_plan($rid);

        redirect("administrator/subscription_plans");
    }

    public function ajaxplan() {
        $id = $_POST['pid'];
        
        $userData = $this->Subscription_Model->view($id);
        $datauserData = $userData[0];
        
        echo json_encode($datauserData);
        exit;
    }

    public function ajaxexistingplan() {
        $id = $_POST['rid'];
        
        $userData = $this->Subscription_Model->view_existing($id);
        $getPlan = $userData[0];

        $userData = $this->Subscription_Model->view($getPlan["plan_id"]);
        $datauserData = $userData[0];

        $planArrays = [
            "plan_name" => $datauserData["plan_name"],
            "duration" => $getPlan["duration"],
            "start_date" => $getPlan["start_date"],
            "end_date" => $getPlan["expiry_date"],
            "plan_cost" => $getPlan["plan_cost"],
            "total_mail" => $getPlan["total_mail"],
            "remaining_mail" => $getPlan["remaining_mail"],
            "total_job_post" => $getPlan["total_job_post"],
            "remaining_job_post" => $getPlan["remaining_job_post"],
            "total_resume_access" => $getPlan["total_resume_access"],
            "remaining_resume_access" => $getPlan["remaining_resume_access"],
            "total_video_post" => $getPlan["total_video_post"],
            "remaining_video_post" => $getPlan["remaining_video_post"],
            "total_advertise" => $getPlan["total_advertise"],
            "remaining_advertise" => $getPlan["remaning_advertise"],
            "total_job_boost" => $getPlan["total_job_boost"],
            "remaining_job_boost" => $getPlan["remaining_job_boost"],
            "total_candidate_profile" => $getPlan["total_candidate_profile"],
            "remaining_candidate_profile" => $getPlan["remaining_candidate_profile"],
        ];
        
        echo json_encode($planArrays);
        exit;
    }

    public function subscribed() {

        $userData = $this->input->post();

        $current_date = date('d-m-Y');
        $expiry_date = date('d-m-Y', strtotime("+".$userData['duration']." months"));

        if($userData['plan_action'] == "renew") {

            $plans = $this->Subscription_Model->view($userData['plan']);
            $plan =  $plans[0];
            
            $record = ["status"=>2];
            $this->Subscription_Model->recruiter_subscribe_update($record, $userData['recruiter_id']);

            $record = ["recruiter_id" => $userData['recruiter_id'], 
                       "plan_id" => $userData['plan'], 
                       'start_date' => $current_date, 
                       'expiry_date' => $expiry_date,
                       'duration' => $userData['duration'],
                       'actual_cost' => $plan['cost'], 
                       "plan_cost"=> $userData['cost'],
                       'total_mail'=>$userData['total_mail'], 
                       "remaining_mail"=> $userData['total_mail'],
                       "total_job_post"=> $userData['total_job_post'], 
                       "remaining_job_post"=>$userData['total_job_post'], 
                       "total_resume_access"=>$userData['total_resume_access'],
                       "remaining_resume_access"=>$userData['total_resume_access'],
                       "total_video_post"=>$userData['total_video_post'],
                       "remaining_video_post"=>$userData['total_video_post'],
                       "total_advertise"=>$userData['total_advertise'],
                       "remaning_advertise"=>$userData['total_advertise'],
                       "total_job_boost"=>$userData['total_job_boost'],
                       "remaining_job_boost"=>$userData['total_job_boost'],
                       "total_candidate_profile" => $userData["total_candidate_profile"],
                       "remaining_candidate_profile" => $userData["remaining_candidate_profile"],
                       "status"=>1,
                   ];

            $this->Subscription_Model->recruiter_subscribe_insert($record);
        
        } else if($userData['plan_action'] == "new") {

            $plans = $this->Subscription_Model->view($userData['plan']);
            $plan =  $plans[0];
            
            $record = ["recruiter_id" => $userData['recruiter_id'], 
                       "plan_id" => $userData['plan'], 
                       'start_date' => $current_date, 
                       'expiry_date' => $expiry_date,
                       'duration' => $userData['duration'],
                       'actual_cost' => $plan['cost'], 
                       "plan_cost"=> $userData['cost'],
                       'total_mail'=>$userData['total_mail'], 
                       "remaining_mail"=> $userData['total_mail'],
                       "total_job_post"=> $userData['total_job_post'], 
                       "remaining_job_post"=>$userData['total_job_post'], 
                       "total_resume_access"=>$userData['total_resume_access'],
                       "remaining_resume_access"=>$userData['total_resume_access'],
                       "total_video_post"=>$userData['total_video_post'],
                       "remaining_video_post"=>$userData['total_video_post'],
                       "total_advertise"=>$userData['total_advertise'],
                       "remaning_advertise"=>$userData['total_advertise'],
                       "total_job_boost"=>$userData['total_job_boost'],
                       "remaining_job_boost"=>$userData['total_job_boost'],
                       "total_candidate_profile" => $userData["total_candidate_profile"],
                       "remaining_candidate_profile" => $userData["remaining_candidate_profile"],
                       "status"=>1,
                   ];

            $this->Subscription_Model->recruiter_subscribe_insert($record);
        
        } else {

            $userPlan = $this->Subscription_Model->view_existing($userData['recruiter_id']);

            $remaining_mail = $userPlan[0]['remaining_mail'] + ($userData['total_mail'] - $userPlan[0]['total_mail']);
            $remaining_job_post = $userPlan[0]['remaining_job_post'] + $userData['total_job_post'] - $userPlan[0]['total_job_post'];
            $remaning_advertise = $userPlan[0]['remaning_advertise'] + $userData['total_advertise'] - $userPlan[0]['total_advertise'];
            $remaining_video_post = $userPlan[0]['remaining_video_post'] + $userData['total_video_post'] - $userPlan[0]['total_video_post'];
            $remaining_resume_access = $userPlan[0]['remaining_resume_access'] + $userData['total_resume_access'] - $userPlan[0]['total_resume_access'];
            $remaining_job_boost = $userPlan[0]['remaining_job_boost'] + $userData['total_job_boost'] - $userPlan[0]['total_job_boost'];
            $remaining_candidate_profile = $userPlan[0]['remaining_candidate_profile'] + $userData['total_candidate_profile'] - $userPlan[0]['total_candidate_profile'];
            
            $record = [
                       'start_date' => $current_date, 
                       'expiry_date' => $expiry_date,
                       'duration' => $userData['duration'],
                       "plan_cost" => $userData['cost'],
                       'total_mail' => $userData['total_mail'], 
                       "remaining_mail" => $remaining_mail,
                       "total_job_post" => $userData['total_job_post'], 
                       "remaining_job_post" => $remaining_job_post, 
                       "total_resume_access" => $userData['total_resume_access'],
                       "remaining_resume_access" => $remaining_resume_access,
                       "total_video_post" => $userData['total_video_post'],
                       "remaining_video_post" => $remaining_video_post,
                       "total_advertise" => $userData['total_advertise'],
                       "remaning_advertise" => $remaning_advertise,
                       "total_job_boost" => $userData['total_job_boost'],
                       "remaining_job_boost" => $remaining_job_boost,
                       "total_candidate_profile" => $userData["total_candidate_profile"],
                       "remaining_candidate_profile" => $remaining_candidate_profile,
                       "status"=>1,
                   ];

            $this->Subscription_Model->recruiter_subscribe_update($record, $userData['recruiter_id']);

        }
        return redirect('administrator/recruiterList');
    }


    public function subscriptionrequest() {
        $record = array();
        $data_subscriptions = $this->Subscription_Model->get_subscribe_request();
        if($data_subscriptions) {
            
            foreach($data_subscriptions as $data_subscription) {
                
                if(!empty($data_subscription['recruite_id'])) {
                    
                    $data_recruite = $this->Recruit_Model->recruiter_single($data_subscription['recruite_id']);

                    $userPlan = $this->Subscription_Model->view_existing($data_subscription['recruite_id']);
                    if($userPlan) {
                        $existing = "Existing";
                    } else {
                        $existing = "New";
                    }

                    $record[] = [
                        "reneweal" => $existing,
                        "recruit_name" => $data_recruite[0]['fname'].' '.$data_recruite[0]['lname'],
                        "recruit_cname" => $data_recruite[0]['cname'],
                        "recruit_email" => $data_recruite[0]['email'],
                        "recruit_id" => $data_recruite[0]['id'],
                        "name" => $data_subscription['name'],
                        "email" => $data_subscription['email'],
                        "phone" => $data_subscription['phone'],
                        "plan" => $data_subscription['plan'],
                        "subject" => $data_subscription['subject'],
                        "description" => $data_subscription['description'],
                        "date" => date('d-m-Y', strtotime($data_subscription['created_at'])),
                    ];
                } else {

                    $existing = "New";

                    $record[] = [
                        "reneweal" => $existing,
                        "recruit_name" => "--",
                        "recruit_cname" => "--",
                        "recruit_email" => "--",
                        "recruit_id" => "--",
                        "name" => $data_subscription['name'],
                        "email" => $data_subscription['email'],
                        "phone" => $data_subscription['phone'],
                        "plan" => $data_subscription['plan'],
                        "subject" => $data_subscription['subject'],
                        "description" => $data_subscription['description'],
                        "date" => date('d-m-Y', strtotime($data_subscription['created_at'])),
                    ];
                }
            }
        }

        $this->Subscription_Model->subscribe_request_update();

        $data['subscriptions'] = $record;
        $this->load->view('administrator/subscription_request', $data);
    }

}

?>