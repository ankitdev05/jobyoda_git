<?php
ob_start();
ini_set("allow_url_fopen", 1);
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */
class Jobpost extends CI_Controller {
    
   function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('newmodels/Jobseekeradmin_Model');
        $this->load->model('newmodels/Jobpostadmin_Model');
        $this->load->model('newmodels/recruiter/Jobpost_Model');
        $this->load->model('newmodels/recruiter/Questionnaire_Model');
        $this->load->model('newmodels/Recruiteradmin_Model');
        $this->load->model('newmodels/Common_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');
         $this->load->library('pdf');
         if($this->session->userdata('adminSession')) {
            
        } else {
            redirect("administrator/admin/index");
        }

        if($this->session->userdata('searchhh')) {
            $this->session->unset_userdata('searchhh');
        }
    }

    public function index() {

        $this->session->unset_userdata('search');

        $listings = $this->Jobpostadmin_Model->job_fetchAll();
        $listget = array();
        $cdate = date('y-m-d');

        if($listings) {
            foreach($listings as $listing) {

              if(strtotime($cdate) <= strtotime($listing['jobexpire'])) {
                 $Listcount = $this->Jobpostadmin_Model->job_fetchsingle($listing['id']);
                 $site_detail = $this->Jobpostadmin_Model->companyname_fetch($listing['company_id']);
                 if(count($Listcount) >0){
                    $cc= 1;
                 } else{
                    $cc=0;
                 }

                 $getCat = $this->Jobpostadmin_Model->categoryname_fetch($listing['category']);
                 $getSubCat = $this->Jobpostadmin_Model->subcategoryname_fetch($listing['subcategory']);

                 $currentDate = date("Y-m-d");
                 $diff = strtotime($listing['jobexpire']) - strtotime($currentDate);
                 $dyasLeft = abs(round($diff / 86400));
                 $dyasLeft = $dyasLeft. " Days ";

                 $listget[] = ["jobtitle"=> $listing['jobtitle'], 
                            "category"=>$getCat[0]['category'], 
                            "subcategory"=>$getSubCat[0]['subcategory']??'',
                            "created_at" => $listing['created_at'], 
                            "jobexpire" => $listing['jobexpire'], 
                            "daysleft"=>$dyasLeft, 
                            "experience"=>$listing['experience'], 
                            "address"=> $site_detail[0]['address'], 
                            "id" => $listing['id'], 
                            "cname" => $listing['cname'],
                            'opening' => $listing['opening'], 
                            "site_name" => $site_detail[0]['cname'], 
                            "company_id" =>$listing['company_id'], 
                            "haveList"=> $cc, 
                            'mode' => $listing['mode'],
                            'chatbot' => $listing['chatbot'],
                            'actively' => $listing['actively'],
                            'actively_date' => $listing['actively_date'],
                        ];
              }
            }
        }
        $data["jobLists"] = $listget;

        $subcategoryArray1 = array();
        $subcategory_list = $this->Jobpost_Model->filtersubcategory_listsbycatid(9);
        foreach ($subcategory_list as $subcategory_lists) {
            $subcategoryArray1[] = [
              "id" => $subcategory_lists['id'],
              "category" => $subcategory_lists['category_id'],                    
              "subcategory" => $subcategory_lists['subcategory'],
            ];
        }

        $powerArray = array();
        $power_lists = $this->Jobpost_Model->category_lists();
        foreach ($power_lists as $power_list) {
            
            $powerArray[] = [
              "id" => $power_list['id'],                  
              "category" => $power_list['category'],
            ];
        }

        $data["intrestedLists"] = $subcategoryArray1;
        $data["powerLists"] = $powerArray;
        $data['activelyAmountFetch'] = $this->Jobpost_Model->activelyamount_fetch();
        $this->load->view('administrator/jobListing', $data);
    }

    public function activelystatus() {
        
        $this->session->unset_userdata('search');

        $jid = $this->input->post('jid');
        $data1 = ["actively"=>1, "actively_date"=> date("y-m-d H:i:s")];
        $jobseekerInserted = $this->Jobpost_Model->jobboostStatus($data1, $jid);
        
        if($jobseekerInserted) {

            $userSession = $this->session->userdata('userSession');
            if(!empty($userSession['label']) && $userSession['label']=='3'){
                $subrecruiter_id = $userSession['id'];
                $userSession['id'] = $userSession['parent_id'];
            }else{
                $userSession['id'] = $userSession['id'];
                $subrecruiter_id = 0;
            }

            redirect("administrator/joblist");
        } else {
            redirect("administrator/joblist");
        }
        
    }

    public function activelydisable() {
        
        $jid = $this->input->post('jid');
        $data1 = ["actively"=>0];
        $jobseekerInserted = $this->Jobpost_Model->jobboostStatus($data1, $jid);
        
        if($jobseekerInserted) {

            $userSession = $this->session->userdata('userSession');
            if(!empty($userSession['label']) && $userSession['label']=='3'){
                $subrecruiter_id = $userSession['id'];
                $userSession['id'] = $userSession['parent_id'];
            }else{
                $userSession['id'] = $userSession['id'];
                $subrecruiter_id = 0;
            }

            redirect("administrator/joblist");
        } else {
            redirect("administrator/joblist");
        }
        
    }

    public function questionnaire_view() {

        $this->session->unset_userdata('search');

        $in = base64_decode($this->uri->segment(3));
        $data['jobid'] = $in;

        $quesDatas = $this->Questionnaire_Model->questionnaire_fetch($data['jobid']);
        foreach($quesDatas as $quesData) {
            $data['questionData'][] = ["question"=>$quesData['question'], "type"=>$quesData['type'], "options"=>explode(',', $quesData['options']), "answers"=>$quesData['answer'], "haverequired"=>$quesData['haverequired']];
        }
        $this->load->view('administrator/questionnaire_view', $data);
    }

    public function questionnaire_user_view() {

        $this->session->unset_userdata('search');

        $in = base64_decode($this->uri->segment(3));
        $user_id = base64_decode($this->uri->segment(4));
        $data['jobid'] = $in;

        $quesDatas = $this->Questionnaire_Model->questionnaire_fetch($data['jobid']);
        $data["score"] =  0;

        foreach($quesDatas as $quesData) {

            $userDatas = $this->Questionnaire_Model->questionnaire_user_fetch($data['jobid'], $quesData['id'], $user_id);
            if($userDatas) {
                $data['questionData'][] = ["question"=>$quesData['question'], "type"=>$quesData['type'], "options"=>explode(',', $quesData['options']), "answers"=>$quesData['answer'], "useranswer"=>$userDatas[0]['answer'], "haverequired"=>$quesData['haverequired']];

                $data["score"] =  $userDatas[0]['percentage'];
                $data["scoreResult"] =  $userDatas[0]['result'];
            }
        }
        $this->load->view('administrator/questionnaire_user_view', $data);
    }

    public function jobnotifications() {

        $this->session->unset_userdata('search');

        $jobIds = explode(',',$this->input->post('checkarr'));
        $title = $this->input->post('title');
        $message = $this->input->post('message');
        $intrestedid = $this->input->post('intrestedid');
        $powerid = $this->input->post('powerid');
        
        if(count($title) > 0 && count($message) > 0) { 

          if(count($intrestedid) > 0 && isset($intrestedid)) { 

              foreach ($jobIds as $jobid) {
                  $getJob = $this->Jobpostadmin_Model->job_fetchnotificationsingle($jobid);
                  $jobsubcat = $getJob[0]['subcategory'];
                  $jobtitle = @$getJob[0]['jobTitle'];

                  if($getJob[0]['subrecruiter_id'] == 0) {
                    $joblocationid = $getJob[0]['recruiter_id'];
                  } else {
                    $joblocationid = $getJob[0]['subrecruiter_id'];
                  }
                  $joblocationid = $getJob[0]['company_id'];
                  $getLocation = $this->Common_Model->company_address_location($joblocationid);
                  $joblocation = $getLocation[0]['city'];

                  $notifyMsg = $jobtitle . "  - new job has been posted";
                  $getCatData = $this->Common_Model->subcategory_listsbysubcatid($jobsubcat);

                  $userIdArr = array();
                  $getusers1 = $this->Jobseekeradmin_Model->jobseeker_bylocationlists_newone($getLocation[0]['latitude'], $getLocation[0]['longitude']);
                  if($getusers1) {
                    foreach($getusers1 as $getuser1) {
                      
                        foreach ($intrestedid as $intrested) {

                           if(strpos($getuser1['jobsInterested'], $intrested) !== false) {

                              $userIdArr[] = $getuser1['id'];
                           }
                        }
                    }
                  }

                  $userIdArr = array_unique($userIdArr);
                  if(count($userIdArr) > 0) {
                  
                    $getusers = $this->Jobseekeradmin_Model->jobseeker_byIntrestedInId($userIdArr);
                    $record = ["job_id"=>$jobid, "recruiter_id"=>$joblocationid, "title"=>urlencode($title), "notification"=> urlencode($message), "intrestedIn"=>implode(',', $intrestedid), "send_type"=>2];
                    $notifyid = $this->Jobpost_Model->jobrecord_notifydata($record);

                    if($getusers) {                

                        foreach($getusers as $getuser) {

                          if(strlen($getuser['latitude']) > 0) {

                            $findDistance = $this->distance($getLocation[0]['latitude'], $getLocation[0]['longitude'], $getuser['latitude'], $getuser['longitude'], "K");

                            if($findDistance <= 25) {
                              $checkToken = $this->Jobseekeradmin_Model->fetchUserDeviceToken($getuser['id']);
                              if($checkToken) {
                                
                                foreach($checkToken as $checkT) {

                                    $saveNotify = ["notify_id"=>$notifyid, "user_id"=>$getuser['id'], "device_token"=>$checkT['device_token'], "device_type"=>$checkT['device_type'], "status"=>1];
                                    $this->Jobpost_Model->newjob_notifydata($saveNotify);
                                }
                              }
                            }
                          }
                        }
                    }

                  } else {
                      $this->session->set_flashdata('success','Notification not able to send');
                      redirect("administrator/Jobpost/index");      
                  }
              }

              $this->session->set_flashdata('success','Notification send successfully');
              redirect("administrator/Jobpost/index");
          
          } else if(count($powerid) > 0 && isset($powerid)) { 

              foreach ($jobIds as $jobid) {

                  $getJob = $this->Jobpostadmin_Model->job_fetchnotificationsingle($jobid);
                  $jobsubcat = $getJob[0]['subcategory'];
                  $jobtitle = @$getJob[0]['jobTitle'];

                  if($getJob[0]['subrecruiter_id'] == 0) {
                    $joblocationid = $getJob[0]['recruiter_id'];
                  } else {
                    $joblocationid = $getJob[0]['subrecruiter_id'];
                  }
                  $joblocationid = $getJob[0]['company_id'];

                  $getLocation = $this->Common_Model->company_address_location($joblocationid);

                  $joblocation = $getLocation[0]['city'];
                  $notifyMsg = $jobtitle . "  - new job has been posted";
                  $getCatData = $this->Common_Model->subcategory_listsbysubcatid($jobsubcat);

                  $userIdArr = array();
                  $getusers1 = $this->Jobseekeradmin_Model->jobseeker_bylocationlists_newone($getLocation[0]['latitude'], $getLocation[0]['longitude']);
                  if($getusers1) {
                    foreach($getusers1 as $getuser1) {
                      
                        foreach ($powerid as $power) {

                           if(strpos($getuser1['superpower'], $power) !== false) {

                              $userIdArr[] = $getuser1['id'];
                           }
                        }
                    }
                  }

                  $userIdArr = array_unique($userIdArr);

                  if(count($userIdArr) > 0) {
                    $getusers = $this->Jobseekeradmin_Model->jobseeker_byIntrestedInId($userIdArr);

                    $record = ["job_id"=>$jobid, "recruiter_id"=>$joblocationid, "title"=>urlencode($title), "notification"=> urlencode($message), "superpower"=>implode(',', $powerid), "send_type"=>2];
                    $notifyid = $this->Jobpost_Model->jobrecord_notifydata($record);

                    if($getusers) {                

                        foreach($getusers as $getuser) {

                          if(strlen($getuser['latitude']) > 0) {

                            $findDistance = $this->distance($getLocation[0]['latitude'], $getLocation[0]['longitude'], $getuser['latitude'], $getuser['longitude'], "K");

                            if($findDistance <= 25) {
                              $checkToken = $this->Jobseekeradmin_Model->fetchUserDeviceToken($getuser['id']);
                              if($checkToken) {
                                
                                foreach($checkToken as $checkT) {

                                    if(strlen($checkT['device_token']) > 0) {
                                      $saveNotify = ["notify_id"=>$notifyid, "user_id"=>$getuser['id'], "device_token"=>$checkT['device_token'], "status"=>1];
                                      $this->Jobpost_Model->newjob_notifydata($saveNotify);
                                    }
                                }
                              }
                            }
                          }
                        }
                    }
                  } else {
                      $this->session->set_flashdata('success','Notification not able to send');
                      redirect("administrator/Jobpost/index");      
                  }
              }

              $this->session->set_flashdata('success','Notification send successfully');
              redirect("administrator/Jobpost/index");
          
          } else {
              $this->session->set_flashdata('success','Notification not able to send');
              redirect("administrator/Jobpost/index");
          }
          
        } else {
              $this->session->set_flashdata('success','Notification not able to send');
              redirect("administrator/Jobpost/index");
        }
    }

    public function jobexpirelist() {

        $this->session->unset_userdata('search');

        $listings = $this->Jobpostadmin_Model->jobexpire_fetchAll();
        $listget = array();

        if($listings) {
            foreach($listings as $listing) {
               $Listcount = $this->Jobpostadmin_Model->job_fetchsingle($listing['id']);
               $site_detail = $this->Jobpostadmin_Model->companyname_fetch($listing['company_id']);
               if(count($Listcount) >0){
                  $cc= 1;
               } else{
                  $cc=0;
               }

               $getCat = $this->Jobpostadmin_Model->categoryname_fetch($listing['category']);
               $getSubCat = $this->Jobpostadmin_Model->subcategoryname_fetch($listing['subcategory']);

               $currentDate = date("Y-m-d");
               $diff = strtotime($listing['jobexpire']) - strtotime($currentDate);
               $dyasLeft = abs(round($diff / 86400));
               $dyasLeft = $dyasLeft. " Days ";

               $listget[] = ["jobtitle"=> $listing['jobtitle'], 
                            "category"=>$getCat[0]['category'], 
                            "subcategory"=>$getSubCat[0]['subcategory']??'',
                            "created_at" => $listing['created_at'], 
                            "jobexpire" => $listing['jobexpire'], 
                            "daysleft"=>$dyasLeft, 
                            "experience"=>$listing['experience'], 
                            "address"=> $site_detail[0]['address'], 
                            "id" => $listing['id'], 
                            "cname" => $listing['cname'],
                            'opening' => $listing['opening'], 
                            "site_name" => $site_detail[0]['cname'], 
                            "company_id" =>$listing['company_id'], 
                            "haveList"=> $cc, 
                            'mode' => $listing['mode'],
                            'chatbot' => $listing['chatbot'],
                        ];

              //$listget[] = ["jobtitle"=> $listing['jobtitle'], "created_at" => $listing['created_at'], "jobexpire" => $listing['jobexpire'], "experience"=>$listing['experience'], "address"=> $site_detail[0]['address'], "id" => $listing['id'], "cname" => $listing['cname'],'opening' => $listing['opening'], "site_name" => $site_detail[0]['cname'], "company_id" =>$listing['company_id'], "haveList"=> $cc];
            }
        }
        $data["jobLists"] = $listget;
        $this->load->view('administrator/jobExpireListing', $data);
    }

    public function hiring_report() {

        $this->session->unset_userdata('search');

        $data = array();
        $data["jobLists"] = [];
        //---------------------------------------------------------------------------------------
        
        $perPage1 = 10;

        $this->load->helper('url');
        $this->load->library('pagination');
        $this->load->library('session');

        // // If search request submitted

        if($this->input->post('submitSearch')) {

            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if(!empty($searchKeyword)){
                $this->session->set_userdata('searchKeyword',$searchKeyword);
            }else{
                $this->session->unset_userdata('searchKeyword');
            }

            $inputStatus = $this->input->post('status_dropdown');
            $searchStatus = strip_tags($inputStatus);

            if(!empty($searchStatus)) {
                $this->session->set_userdata('searchStatus',$searchStatus);
            }else{
                $this->session->unset_userdata('searchStatus');
            }

            if($this->input->post('from_date') || $this->input->post('to_date')) {

                $inputFrom = $this->input->post('from_date');
                $searchFrom = strip_tags($inputFrom);
                if(!empty($searchFrom)) {
                    $this->session->set_userdata('searchFrom',$searchFrom);
                }else{
                    $this->session->unset_userdata('searchFrom');
                }

                $inputTo = $this->input->post('to_date');
                $searchTo = strip_tags($inputTo);
                if(!empty($searchTo)) {
                    $this->session->set_userdata('searchTo',$searchTo);
                }else{
                    $this->session->unset_userdata('searchTo');
                }
            }
            
        } elseif($this->input->post('submitSearchReset')) {

            $this->session->unset_userdata('searchKeyword');
            $this->session->unset_userdata('searchStatus');
            $this->session->unset_userdata('searchFrom');
            $this->session->unset_userdata('searchTo');
        
        } else {

            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if(!empty($searchKeyword)){
                $this->session->set_userdata('searchKeyword',$searchKeyword);
            }else{
                if($this->uri->segment(3) == 0) {
                  $this->session->unset_userdata('searchKeyword');
                }
            }

            $inputStatus = $this->input->post('status_dropdown');
            $searchStatus = strip_tags($inputStatus);
            if(!empty($searchStatus)) {
                $this->session->set_userdata('searchStatus',$searchStatus);
            }else{
                if($this->uri->segment(3) == 0) {
                  $this->session->unset_userdata('searchStatus');
                }
            }

            if($this->input->post('from_date') || $this->input->post('to_date')) {

                $inputFrom = $this->input->post('from_date');
                $searchFrom = strip_tags($inputFrom);
                if(!empty($searchFrom)) {
                    $this->session->set_userdata('searchFrom',$searchFrom);
                }else{
                    if($this->uri->segment(3) == 0) {
                      $this->session->unset_userdata('searchFrom');
                    }
                }

                $inputTo = $this->input->post('to_date');
                $searchTo = strip_tags($inputTo);
                if(!empty($searchTo)) {
                    $this->session->set_userdata('searchTo',$searchTo);
                }else{
                    if($this->uri->segment(3) == 0) {
                      $this->session->unset_userdata('searchTo');
                    }
                }
            }
        }

        $data['searchKeyword'] = $this->session->userdata('searchKeyword');
        if(isset($_GET['status'])) {
          $data['searchStatus'] = 1;
        } else {
          $data['searchStatus'] = $this->session->userdata('searchStatus');
        }
        $data['searchFrom'] = $this->session->userdata('searchFrom');
        $data['searchTo'] = $this->session->userdata('searchTo');

        if($this->input->post('tabtype')) {
          $tabType = $this->input->post('tabtype');
        } else {
          $tabType = "";
        }

        if($tabType == "daywise") {

            if($this->uri->segment(3)) {
              $whichTab = $this->uri->segment(3);
            } else {
              $whichTab = "days";
            }

            // // Get rows count
            $conditions1['searchKeyword'] = $data['searchKeyword'];
            $conditions1['searchStatus'] = $data['searchStatus'];
            $conditions1['returnType']    = 'count';
            $conditions1['returnDays']    = 'days';
            $rowsCount1 = $this->Jobpostadmin_Model->getRowsHiring($conditions1);
            $config1['base_url']    = base_url().'administrator/hiring_report/days';
            $config1['total_rows']  = $rowsCount1;
            if($whichTab == "days") {
              
              if($this->uri->segment(4)) {
                $offset1 = $this->uri->segment(4);
              } else {
                $offset1 = 0;
              }
            } else {
              $offset1 = 0;
            }
            $config1['per_page']    = 10;
            $config1['show_link'] = $offset1;
            // Get rows
            $conditions1['returnType'] = '';
            $conditions1['start'] = $offset1;
            $conditions1['limit'] = $config1['per_page'];

            $data_result1 = $this->Jobpostadmin_Model->getRowsHiring($conditions1);
            $data['result1'] = $this->modify_hiring_data($data_result1);
            $data['resultTotal1'] = $rowsCount1;
            $data['pagination1'] = $config1;


            // Get rows count
            $conditions2['searchKeyword'] = '';
            $conditions2['searchStatus'] = '';
            $conditions2['returnType']    = 'count';
            $conditions2['returnDays']    = 'week';
            $rowsCount2 = $this->Jobpostadmin_Model->getRowsHiring($conditions2);
            $config2['base_url']    = base_url().'administrator/hiring_report/week';
            $config2['total_rows']  = $rowsCount2;
              $offset2 = 0;
            $config2['per_page'] = 10;
            $config2['show_link'] = $offset2;
            // Get rows
            $conditions2['returnType'] = '';
            $conditions2['start'] = $offset2;
            $conditions2['limit'] = $config2['per_page'];
            $data_result2 = $this->Jobpostadmin_Model->getRowsHiring($conditions2);
            $data['result2'] = $this->modify_hiring_data($data_result2);
            $data['resultTotal2'] = $rowsCount2;
            $data['pagination2'] = $config2;


            // Get rows count
            $conditions3['searchKeyword'] = '';
            $conditions3['searchStatus'] = '';
            $conditions3['returnType']    = 'count';
            $conditions3['returnDays']    = 'month';
            $rowsCount3 = $this->Jobpostadmin_Model->getRowsHiring($conditions3);
            
            $config3['base_url']    = base_url().'administrator/hiring_report/month';
            $config3['total_rows']  = $rowsCount3;
              $offset3 = 0;
            $config3['per_page'] = 10;
            $config3['show_link'] = $offset3;
            // // Get rows
            $conditions3['returnType'] = '';
            $conditions3['start'] = $offset3;
            $conditions3['limit'] = $config3['per_page'];
            $data_result3 = $this->Jobpostadmin_Model->getRowsHiring($conditions3);
            $data['result3'] = $this->modify_hiring_data($data_result3);
            $data['resultTotal3'] = $rowsCount3;
            $data['pagination3'] = $config3;


            // Get rows count
            $conditions4['searchKeyword'] = '';
            $conditions4['searchStatus'] = '';
            $conditions4['returnType']    = 'count';
            $conditions4['returnDays']    = 'year';
            $rowsCount4 = $this->Jobpostadmin_Model->getRowsHiring($conditions4);
            $config4['base_url']    = base_url().'administrator/hiring_report/year';
            $config4['total_rows']  = $rowsCount4;
              $offset4 = 0;
            $config4['per_page']    = 10;
            $config4['show_link'] = $offset4;
            // // Get rows
            $conditions4['returnType'] = '';
            $conditions4['start'] = $offset4;
            $conditions4['limit'] = $config4['per_page'];
            $data_result4 = $this->Jobpostadmin_Model->getRowsHiring($conditions4);
            $data['result4'] = $this->modify_hiring_data($data_result4);
            $data['resultTotal4'] = $rowsCount4;
            $data['pagination4'] = $config4;

            // Get rows count
            $conditions5['searchKeyword'] = '';
            $conditions5['searchStatus'] = '';
            $conditions5['returnType']    = 'count';
            $conditions5['returnDays']    = 'all';
            $rowsCount5 = $this->Jobpostadmin_Model->getRowsHiring($conditions5);
            $config5['base_url']    = base_url().'administrator/hiring_report/datewise';
            $config5['total_rows']  = $rowsCount5;
              $offset5 = 0;
            $config5['per_page']    = 10;
            $config5['show_link'] = $offset5;
            // // Get rows
            $conditions5['returnType'] = '';
            $conditions5['start'] = $offset5;
            $conditions5['limit'] = $config5['per_page'];
            $data_result5 = $this->Jobpostadmin_Model->getRowsHiring($conditions5);
            $data['result5'] = $this->modify_hiring_data($data_result5);
            $data['resultTotal5'] = $rowsCount5;
            $data['pagination5'] = $config5;

            $data['checkactive'] = "tab1"; 

        } else if($tabType == "weekwise") {

            if($this->uri->segment(3)) {
              $whichTab = $this->uri->segment(3);
            } else {
              $whichTab = "days";
            }

            // // Get rows count
            $conditions1['searchKeyword'] = '';
            $conditions1['searchStatus'] = '';
            $conditions1['returnType']    = 'count';
            $conditions1['returnDays']    = 'days';
            $rowsCount1 = $this->Jobpostadmin_Model->getRowsHiring($conditions1);
            $config1['base_url']    = base_url().'administrator/hiring_report/days';
            $config1['total_rows']  = $rowsCount1;
              $offset1 = 0;
            $config1['per_page']    = 10;
            $config1['show_link'] = $offset1;
            // Get rows
            $conditions1['returnType'] = '';
            $conditions1['start'] = $offset1;
            $conditions1['limit'] = $config1['per_page'];
            $data_result1 = $this->Jobpostadmin_Model->getRowsHiring($conditions1);
            $data['result1'] = $this->modify_hiring_data($data_result1);
            $data['resultTotal1'] = $rowsCount1;
            $data['pagination1'] = $config1;

            // Get rows count
            $conditions2['searchKeyword'] = $data['searchKeyword'];
            $conditions2['searchStatus'] = $data['searchStatus'];
            $conditions2['returnType']    = 'count';
            $conditions2['returnDays']    = 'week';
            $rowsCount2 = $this->Jobpostadmin_Model->getRowsHiring($conditions2);
            $config2['base_url']    = base_url().'administrator/hiring_report/week';
            $config2['total_rows']  = $rowsCount2;
            if($whichTab == "week") {
              
              if($this->uri->segment(4)) {
                $offset2 = $this->uri->segment(4);
              } else {
                $offset2 = 0;
              }

            } else {
              $offset2 = 0;
            }
            $config2['per_page'] = 10;
            $config2['show_link'] = $offset2;
            // Get rows
            $conditions2['returnType'] = '';
            $conditions2['start'] = $offset2;
            $conditions2['limit'] = $config2['per_page'];
            $data_result2 = $this->Jobpostadmin_Model->getRowsHiring($conditions2);
            $data['result2'] = $this->modify_hiring_data($data_result2);
            $data['resultTotal2'] = $rowsCount2;
            $data['pagination2'] = $config2;


            // Get rows count
            $conditions3['searchKeyword'] = '';
            $conditions3['searchStatus'] = '';
            $conditions3['returnType']    = 'count';
            $conditions3['returnDays']    = 'month';
            $rowsCount3 = $this->Jobpostadmin_Model->getRowsHiring($conditions3);
            $config3['base_url']    = base_url().'administrator/hiring_report/month';
            $config3['total_rows']  = $rowsCount3;
              $offset3 = 0;
            $config3['per_page'] = 10;
            $config3['show_link'] = $offset3;
            // // Get rows
            $conditions3['returnType'] = '';
            $conditions3['start'] = $offset3;
            $conditions3['limit'] = $config3['per_page'];
            $data_result3 = $this->Jobpostadmin_Model->getRowsHiring($conditions3);
            $data['result3'] = $this->modify_hiring_data($data_result3);
            $data['resultTotal3'] = $rowsCount3;
            $data['pagination3'] = $config3;


            // Get rows count
            $conditions4['searchKeyword'] = '';
            $conditions4['searchStatus'] = '';
            $conditions4['returnType']    = 'count';
            $conditions4['returnDays']    = 'year';
            $rowsCount4 = $this->Jobpostadmin_Model->getRowsHiring($conditions4);
            $config4['base_url']    = base_url().'administrator/hiring_report/year';
            $config4['total_rows']  = $rowsCount4;
              $offset4 = 0;
            $config4['per_page']    = 10;
            $config4['show_link'] = $offset4;
            // // Get rows
            $conditions4['returnType'] = '';
            $conditions4['start'] = $offset4;
            $conditions4['limit'] = $config4['per_page'];
            $data_result4 = $this->Jobpostadmin_Model->getRowsHiring($conditions4);
            $data['result4'] = $this->modify_hiring_data($data_result4);
            $data['resultTotal4'] = $rowsCount4;
            $data['pagination4'] = $config4;


            // Get rows count
            $conditions5['searchKeyword'] = '';
            $conditions5['searchStatus'] = '';
            $conditions5['returnType']    = 'count';
            $conditions5['returnDays']    = 'all';
            $rowsCount5 = $this->Jobpostadmin_Model->getRowsHiring($conditions5);
            $config5['base_url']    = base_url().'administrator/hiring_report/datewise';
            $config5['total_rows']  = $rowsCount5;
              $offset5 = 0;
            $config5['per_page']    = 10;
            $config5['show_link'] = $offset5;
            // // Get rows
            $conditions5['returnType'] = '';
            $conditions5['start'] = $offset5;
            $conditions5['limit'] = $config5['per_page'];
            $data_result5 = $this->Jobpostadmin_Model->getRowsHiring($conditions5);
            $data['result5'] = $this->modify_hiring_data($data_result5);
            $data['resultTotal5'] = $rowsCount5;
            $data['pagination5'] = $config5;

            $data['checkactive'] = "tab2";

        } else if($tabType == "monthwise") {

            if($this->uri->segment(3)) {
              $whichTab = $this->uri->segment(3);
            } else {
              $whichTab = "days";
            }

            // // Get rows count
            $conditions1['searchKeyword'] = '';
            $conditions1['searchStatus'] = '';
            $conditions1['returnType']    = 'count';
            $conditions1['returnDays']    = 'days';
            $rowsCount1 = $this->Jobpostadmin_Model->getRowsHiring($conditions1);
            $config1['base_url']    = base_url().'administrator/hiring_report/days';
            $config1['total_rows']  = $rowsCount1;
              $offset1 = 0;
            $config1['per_page']    = 10;
            $config1['show_link'] = $offset1;
            // Get rows
            $conditions1['returnType'] = '';
            $conditions1['start'] = $offset1;
            $conditions1['limit'] = $config1['per_page'];
            $data_result1 = $this->Jobpostadmin_Model->getRowsHiring($conditions1);
            $data['result1'] = $this->modify_hiring_data($data_result1);
            $data['resultTotal1'] = $rowsCount1;
            $data['pagination1'] = $config1;

            // Get rows count
            $conditions2['searchKeyword'] = '';
            $conditions2['searchStatus'] = '';
            $conditions2['returnType']    = 'count';
            $conditions2['returnDays']    = 'week';
            $rowsCount2 = $this->Jobpostadmin_Model->getRowsHiring($conditions2);
            $config2['base_url']    = base_url().'administrator/hiring_report/week';
            $config2['total_rows']  = $rowsCount2;
              $offset2 = 0;
            $config2['per_page'] = 10;
            $config2['show_link'] = $offset2;
            // Get rows
            $conditions2['returnType'] = '';
            $conditions2['start'] = $offset2;
            $conditions2['limit'] = $config2['per_page'];
            $data_result2 = $this->Jobpostadmin_Model->getRowsHiring($conditions2);
            $data['result2'] = $this->modify_hiring_data($data_result2);
            $data['resultTotal2'] = $rowsCount2;
            $data['pagination2'] = $config2;


            // Get rows count
            $conditions3['searchKeyword'] = $data['searchKeyword'];
            $conditions3['searchStatus'] = $data['searchStatus'];
            $conditions3['returnType']    = 'count';
            $conditions3['returnDays']    = 'month';
            $rowsCount3 = $this->Jobpostadmin_Model->getRowsHiring($conditions3);
            
            $config3['base_url']    = base_url().'administrator/hiring_report/month';
            $config3['total_rows']  = $rowsCount3;
            if($whichTab == "month") {
              if($this->uri->segment(4)) {
                $offset3 = $this->uri->segment(4);
              } else {
                $offset3 = 0;
              }
            } else {
              $offset3 = 0;
            }
            $config3['per_page'] = 10;
            $config3['show_link'] = $offset3;
            // // Get rows
            $conditions3['returnType'] = '';
            $conditions3['start'] = $offset3;
            $conditions3['limit'] = $config3['per_page'];
            $data_result3 = $this->Jobpostadmin_Model->getRowsHiring($conditions3);
            $data['result3'] = $this->modify_hiring_data($data_result3);
            $data['resultTotal3'] = $rowsCount3;
            $data['pagination3'] = $config3;

            // Get rows count
            $conditions4['searchKeyword'] = '';
            $conditions4['searchStatus'] = '';
            $conditions4['returnType']    = 'count';
            $conditions4['returnDays']    = 'year';
            $rowsCount4 = $this->Jobpostadmin_Model->getRowsHiring($conditions4);
            $config4['base_url']    = base_url().'administrator/hiring_report/year';
            $config4['total_rows']  = $rowsCount4;
              $offset4 = 0;
            $config4['per_page']    = 10;
            $config4['show_link'] = $offset4;
            // // Get rows
            $conditions4['returnType'] = '';
            $conditions4['start'] = $offset4;
            $conditions4['limit'] = $config4['per_page'];
            $data_result4 = $this->Jobpostadmin_Model->getRowsHiring($conditions4);
            $data['result4'] = $this->modify_hiring_data($data_result4);
            $data['resultTotal4'] = $rowsCount4;
            $data['pagination4'] = $config4;


            // Get rows count
            $conditions5['searchKeyword'] = '';
            $conditions5['searchStatus'] = '';
            $conditions5['returnType']    = 'count';
            $conditions5['returnDays']    = 'all';
            $rowsCount5 = $this->Jobpostadmin_Model->getRowsHiring($conditions5);
            $config5['base_url']    = base_url().'administrator/hiring_report/datewise';
            $config5['total_rows']  = $rowsCount5;
              $offset5 = 0;
            $config5['per_page']    = 10;
            $config5['show_link'] = $offset5;
            // // Get rows
            $conditions5['returnType'] = '';
            $conditions5['start'] = $offset5;
            $conditions5['limit'] = $config5['per_page'];
            $data_result5 = $this->Jobpostadmin_Model->getRowsHiring($conditions5);
            $data['result5'] = $this->modify_hiring_data($data_result5);
            $data['resultTotal5'] = $rowsCount5;
            $data['pagination5'] = $config5;

            $data['checkactive'] = "tab3";
          
        } else if($tabType == "yearwise") {

            if($this->uri->segment(3)) {
              $whichTab = $this->uri->segment(3);
            } else {
              $whichTab = "days";
            }

            // // Get rows count
            $conditions1['searchKeyword'] = '';
            $conditions1['searchStatus'] = '';
            $conditions1['returnType']    = 'count';
            $conditions1['returnDays']    = 'days';
            $rowsCount1 = $this->Jobpostadmin_Model->getRowsHiring($conditions1);
            $config1['base_url']    = base_url().'administrator/hiring_report/days';
            $config1['total_rows']  = $rowsCount1;
              $offset1 = 0;
            $config1['per_page']    = 10;
            $config1['show_link'] = $offset1;
            // Get rows
            $conditions1['returnType'] = '';
            $conditions1['start'] = $offset1;
            $conditions1['limit'] = $config1['per_page'];
            $data_result1 = $this->Jobpostadmin_Model->getRowsHiring($conditions1);
            $data['result1'] = $this->modify_hiring_data($data_result1);
            $data['resultTotal1'] = $rowsCount1;
            $data['pagination1'] = $config1;

            // Get rows count
            $conditions2['searchKeyword'] = '';
            $conditions2['searchStatus'] = '';
            $conditions2['returnType']    = 'count';
            $conditions2['returnDays']    = 'week';
            $rowsCount2 = $this->Jobpostadmin_Model->getRowsHiring($conditions2);
            $config2['base_url']    = base_url().'administrator/hiring_report/week';
            $config2['total_rows']  = $rowsCount2;
              $offset2 = 0;
            $config2['per_page'] = 10;
            $config2['show_link'] = $offset2;
            // Get rows
            $conditions2['returnType'] = '';
            $conditions2['start'] = $offset2;
            $conditions2['limit'] = $config2['per_page'];
            $data_result2 = $this->Jobpostadmin_Model->getRowsHiring($conditions2);
            $data['result2'] = $this->modify_hiring_data($data_result2);
            $data['resultTotal2'] = $rowsCount2;
            $data['pagination2'] = $config2;

            // Get rows count
            $conditions3['searchKeyword'] = '';
            $conditions3['searchStatus'] = '';
            $conditions3['returnType']    = 'count';
            $conditions3['returnDays']    = 'month';
            $rowsCount3 = $this->Jobpostadmin_Model->getRowsHiring($conditions3);
            $config3['base_url']    = base_url().'administrator/hiring_report/month';
            $config3['total_rows']  = $rowsCount3;
              $offset3 = 0;
            $config3['per_page'] = 10;
            $config3['show_link'] = $offset3;
            // // Get rows
            $conditions3['returnType'] = '';
            $conditions3['start'] = $offset3;
            $conditions3['limit'] = $config3['per_page'];
            $data_result3 = $this->Jobpostadmin_Model->getRowsHiring($conditions3);
            $data['result3'] = $this->modify_hiring_data($data_result3);
            $data['resultTotal3'] = $rowsCount3;
            $data['pagination3'] = $config3;

            // Get rows count
            $conditions4['searchKeyword'] = $data['searchKeyword'];
            $conditions4['searchStatus'] = $data['searchStatus'];
            $conditions4['returnType']    = 'count';
            $conditions4['returnDays']    = 'year';
            $rowsCount4 = $this->Jobpostadmin_Model->getRowsHiring($conditions4);
            $config4['base_url']    = base_url().'administrator/hiring_report/year';
            $config4['total_rows']  = $rowsCount4;
            if($whichTab == "year") {
              if($this->uri->segment(4)) {
                $offset4 = $this->uri->segment(4);
              } else {
                $offset4 = 0;
              }
            } else {
              $offset4 = 0;
            }
            $config4['per_page']    = 10;
            $config4['show_link'] = $offset4;
            // // Get rows
            $conditions4['returnType'] = '';
            $conditions4['start'] = $offset4;
            $conditions4['limit'] = $config4['per_page'];
            $data_result4 = $this->Jobpostadmin_Model->getRowsHiring($conditions4);
            $data['result4'] = $this->modify_hiring_data($data_result4);
            $data['resultTotal4'] = $rowsCount4;
            $data['pagination4'] = $config4;


            // Get rows count
            $conditions5['searchKeyword'] = '';
            $conditions5['searchStatus'] = '';
            $conditions5['returnType']    = 'count';
            $conditions5['returnDays']    = 'all';
            $rowsCount5 = $this->Jobpostadmin_Model->getRowsHiring($conditions5);
            $config5['base_url']    = base_url().'administrator/hiring_report/datewise';
            $config5['total_rows']  = $rowsCount5;
              $offset5 = 0;
            $config5['per_page']    = 10;
            $config5['show_link'] = $offset5;
            // // Get rows
            $conditions5['returnType'] = '';
            $conditions5['start'] = $offset5;
            $conditions5['limit'] = $config5['per_page'];
            $data_result5 = $this->Jobpostadmin_Model->getRowsHiring($conditions5);
            $data['result5'] = $this->modify_hiring_data($data_result5);
            $data['resultTotal5'] = $rowsCount5;
            $data['pagination5'] = $config5;

            $data['checkactive'] = "tab4";
            
        } else if($tabType == "datewise") {

            if($this->uri->segment(3)) {
              $whichTab = $this->uri->segment(3);
            } else {
              $whichTab = "days";
            }

            // // Get rows count
            $conditions1['searchKeyword'] = '';
            $conditions1['searchStatus'] = '';
            $conditions1['returnType']    = 'count';
            $conditions1['returnDays']    = 'days';
            $rowsCount1 = $this->Jobpostadmin_Model->getRowsHiring($conditions1);
            $config1['base_url']    = base_url().'administrator/hiring_report/days';
            $config1['total_rows']  = $rowsCount1;
            $offset1 = 0;
            $config1['per_page']    = 10;
            $config1['show_link'] = $offset1;
            // Get rows
            $conditions1['returnType'] = '';
            $conditions1['start'] = $offset1;
            $conditions1['limit'] = $config1['per_page'];
            $data_result1 = $this->Jobpostadmin_Model->getRowsHiring($conditions1);
            $data['result1'] = $this->modify_hiring_data($data_result1);
            $data['resultTotal1'] = $rowsCount1;
            $data['pagination1'] = $config1;

            // Get rows count
            $conditions2['searchKeyword'] = '';
            $conditions2['searchStatus'] = '';
            $conditions2['returnType']    = 'count';
            $conditions2['returnDays']    = 'week';
            $rowsCount2 = $this->Jobpostadmin_Model->getRowsHiring($conditions2);
            $config2['base_url']    = base_url().'administrator/hiring_report/week';
            $config2['total_rows']  = $rowsCount2;
            $offset2 = 0;
            $config2['per_page'] = 10;
            $config2['show_link'] = $offset2;
            // Get rows
            $conditions2['returnType'] = '';
            $conditions2['start'] = $offset2;
            $conditions2['limit'] = $config2['per_page'];
            $data_result2 = $this->Jobpostadmin_Model->getRowsHiring($conditions2);
            $data['result2'] = $this->modify_hiring_data($data_result2);
            $data['resultTotal2'] = $rowsCount2;
            $data['pagination2'] = $config2;


            // Get rows count
            $conditions3['searchKeyword'] = '';
            $conditions3['searchStatus'] = '';
            $conditions3['returnType']    = 'count';
            $conditions3['returnDays']    = 'month';
            $rowsCount3 = $this->Jobpostadmin_Model->getRowsHiring($conditions3);

            $config3['base_url']    = base_url().'administrator/hiring_report/month';
            $config3['total_rows']  = $rowsCount3;
            $offset3 = 0;
            $config3['per_page'] = 10;
            $config3['show_link'] = $offset3;
            // // Get rows
            $conditions3['returnType'] = '';
            $conditions3['start'] = $offset3;
            $conditions3['limit'] = $config3['per_page'];
            $data_result3 = $this->Jobpostadmin_Model->getRowsHiring($conditions3);
            $data['result3'] = $this->modify_hiring_data($data_result3);
            $data['resultTotal3'] = $rowsCount3;
            $data['pagination3'] = $config3;


            // Get rows count
            $conditions4['searchKeyword'] = '';
            $conditions4['searchStatus'] = '';
            $conditions4['returnType']    = 'count';
            $conditions4['returnDays']    = 'year';
            $rowsCount4 = $this->Jobpostadmin_Model->getRowsHiring($conditions4);
            $config4['base_url']    = base_url().'administrator/hiring_report/year';
            $config4['total_rows']  = $rowsCount4;
            $offset4 = 0;
            $config4['per_page']    = 10;
            $config4['show_link'] = $offset4;
            // // Get rows
            $conditions4['returnType'] = '';
            $conditions4['start'] = $offset4;
            $conditions4['limit'] = $config4['per_page'];
            $data_result4 = $this->Jobpostadmin_Model->getRowsHiring($conditions4);
            $data['result4'] = $this->modify_hiring_data($data_result4);
            $data['resultTotal4'] = $rowsCount4;
            $data['pagination4'] = $config4;


            // Get rows count
            $conditions5['searchKeyword'] = $data['searchKeyword'];
            $conditions5['searchStatus'] = $data['searchStatus'];
            $conditions5['searchFrom'] = $data['searchFrom'];
            $conditions5['searchTo'] = $data['searchTo'];

            $conditions5['returnType']    = 'count';
            $conditions5['returnDays']    = 'all';
            $rowsCount5 = $this->Jobpostadmin_Model->getRowsHiring($conditions5);
            $config5['base_url']    = base_url().'administrator/hiring_report/datewise';
            $config5['total_rows']  = $rowsCount5;
            if($whichTab == "year") {
              if($this->uri->segment(4)) {
                $offset5 = $this->uri->segment(4);
              } else {
                $offset5 = 0;
              }
            } else {
              $offset5 = 0;
            }
            $config5['per_page']    = 10;
            $config5['show_link'] = $offset5;
            // // Get rows
            $conditions5['returnType'] = '';
            $conditions5['start'] = $offset5;
            $conditions5['limit'] = $config5['per_page'];
            $data_result5 = $this->Jobpostadmin_Model->getRowsHiring($conditions5);
            $data['result5'] = $this->modify_hiring_data($data_result5);
            $data['resultTotal5'] = $rowsCount5;
            $data['pagination5'] = $config5;

            $data['checkactive'] = "tab5";

        } else {

            if($this->uri->segment(3)) {
              $whichTab = $this->uri->segment(3);
            } else {
              $whichTab = "days";
            }

            // // Get rows count
            $conditions1['searchKeyword'] = $data['searchKeyword'];
            $conditions1['searchStatus'] = $data['searchStatus'];
            $conditions1['returnType']    = 'count';
            $conditions1['returnDays']    = 'days';
            $rowsCount1 = $this->Jobpostadmin_Model->getRowsHiring($conditions1);
            $config1['base_url']    = base_url().'administrator/hiring_report/days';
            $config1['total_rows']  = $rowsCount1;
            if($whichTab == "days") {
              
              if($this->uri->segment(4)) {
                $offset1 = $this->uri->segment(4);
              } else {
                $offset1 = 0;
              }
            } else {
              $offset1 = 0;
            }
            $config1['per_page']    = 10;
            $config1['show_link'] = $offset1;
            // Get rows
            $conditions1['returnType'] = '';
            $conditions1['start'] = (int)$offset1;
            $conditions1['limit'] = $config1['per_page'];
            //var_dump($conditions1);die;
            $data_result1 = $this->Jobpostadmin_Model->getRowsHiring($conditions1);
            $data['result1'] = $this->modify_hiring_data($data_result1);
            $data['resultTotal1'] = $rowsCount1;
            $data['pagination1'] = $config1;


            // Get rows count
            $conditions2['searchKeyword'] = $data['searchKeyword'];
            $conditions2['searchStatus'] = $data['searchStatus'];
            $conditions2['returnType']    = 'count';
            $conditions2['returnDays']    = 'week';
            $rowsCount2 = $this->Jobpostadmin_Model->getRowsHiring($conditions2);
            $config2['base_url']    = base_url().'administrator/hiring_report/week';
            $config2['total_rows']  = $rowsCount2;
            if($whichTab == "week") {
              
              if($this->uri->segment(4)) {
                $offset2 = $this->uri->segment(4);
              } else {
                $offset2 = 0;
              }

            } else {
              $offset2 = 0;
            }
            $config2['per_page'] = 10;
            $config2['show_link'] = $offset2;
            // Get rows
            $conditions2['returnType'] = '';
            $conditions2['start'] = $offset2;
            $conditions2['limit'] = $config2['per_page'];
            $data_result2 = $this->Jobpostadmin_Model->getRowsHiring($conditions2);
            $data['result2'] = $this->modify_hiring_data($data_result2);
            $data['resultTotal2'] = $rowsCount2;
            $data['pagination2'] = $config2;


            // Get rows count
            $conditions3['searchKeyword'] = $data['searchKeyword'];
            $conditions3['searchStatus'] = $data['searchStatus'];
            $conditions3['returnType']    = 'count';
            $conditions3['returnDays']    = 'month';
            $rowsCount3 = $this->Jobpostadmin_Model->getRowsHiring($conditions3);

            $config3['base_url']    = base_url().'administrator/hiring_report/month';
            $config3['total_rows']  = $rowsCount3;
            if($whichTab == "month") {
              if($this->uri->segment(4)) {
                $offset3 = $this->uri->segment(4);
              } else {
                $offset3 = 0;
              }
            } else {
              $offset3 = 0;
            }
            $config3['per_page'] = 10;
            $config3['show_link'] = $offset3;
            // // Get rows
            $conditions3['returnType'] = '';
            $conditions3['start'] = $offset3;
            $conditions3['limit'] = $config3['per_page'];
            $data_result3 = $this->Jobpostadmin_Model->getRowsHiring($conditions3);
            $data['result3'] = $this->modify_hiring_data($data_result3);
            $data['resultTotal3'] = $rowsCount3;
            $data['pagination3'] = $config3;


            // Get rows count
            $conditions4['searchKeyword'] = $data['searchKeyword'];
            $conditions4['searchStatus'] = $data['searchStatus'];
            $conditions4['returnType']    = 'count';
            $conditions4['returnDays']    = 'year';
            $rowsCount4 = $this->Jobpostadmin_Model->getRowsHiring($conditions4);
            $config4['base_url']    = base_url().'administrator/hiring_report/year';
            $config4['total_rows']  = $rowsCount4;
            if($whichTab == "year") {
              if($this->uri->segment(4)) {
                $offset4 = $this->uri->segment(4);
              } else {
                $offset4 = 0;
              }
            } else {
              $offset4 = 0;
            }
            $config4['per_page']    = 10;
            $config4['show_link'] = $offset4;
            // // Get rows
            $conditions4['returnType'] = '';
            $conditions4['start'] = $offset4;
            $conditions4['limit'] = $config4['per_page'];
            $data_result4 = $this->Jobpostadmin_Model->getRowsHiring($conditions4);
            $data['result4'] = $this->modify_hiring_data($data_result4);
            $data['resultTotal4'] = $rowsCount4;
            $data['pagination4'] = $config4;


            // Get rows count
            $conditions5['searchKeyword'] = $data['searchKeyword'];
            $conditions5['searchStatus'] = $data['searchStatus'];
            $conditions5['returnType']    = 'count';
            $conditions5['returnDays']    = 'all';
            $rowsCount5 = $this->Jobpostadmin_Model->getRowsHiring($conditions5);
            $config5['base_url']    = base_url().'administrator/hiring_report/datewise';
            $config5['total_rows']  = $rowsCount5;
            if($whichTab == "year") {
              if($this->uri->segment(4)) {
                $offset5 = $this->uri->segment(4);
              } else {
                $offset5 = 0;
              }
            } else {
              $offset5 = 0;
            }
            $config5['per_page']    = 10;
            $config5['show_link'] = $offset5;
            // // Get rows
            $conditions5['returnType'] = '';
            $conditions5['start'] = $offset5;
            $conditions5['limit'] = $config5['per_page'];
            $data_result5 = $this->Jobpostadmin_Model->getRowsHiring($conditions5);
            $data['result5'] = $this->modify_hiring_data($data_result5);
            $data['resultTotal5'] = $rowsCount5;
            $data['pagination5'] = $config5;

            if($this->uri->segment(3)) {

              $whichTabing = $this->uri->segment(3);
              if($whichTabing == "days") {
                  $data['checkactive'] = "tab1"; 
              } else if($whichTabing == "week") {
                  $data['checkactive'] = "tab2";
              } else if($whichTabing == "month") {
                  $data['checkactive'] = "tab3";
              } else if($whichTabing == "year") {
                  $data['checkactive'] = "tab4";
              } else if($whichTabing == "datewise") {
                  $data['checkactive'] = "tab5";
              }
            } else {
              $data['checkactive'] = "tab1";  
            }
        }

        //var_dump($data);die;
        //---------------------------------------------------------------------------------------

        $this->load->view('administrator/hiring_reports',$data);
    }


    public function export_csv_report_hiring() {

        $filename = 'users_'.date('Ymd').'.csv'; 
        header("Content-Description: File Transfer"); 
        header("Content-Disposition: attachment; filename=$filename"); 
        header("Content-Type: application/csv; ");

        $tabType = $_GET['tabtype'];

        if($tabType == "daywise") {

            if($this->session->userdata('searchKeyword') || $this->session->userdata('searchStatus')) {

                $inputKeywords = $this->session->userdata('searchKeyword');
                $searchKeyword = strip_tags($inputKeywords);

                $inputStatus = $this->session->userdata('searchStatus');
                $searchStatus = strip_tags($inputStatus);

                $conditions['searchKeyword'] = $searchKeyword;
                $conditions['searchStatus'] = $searchStatus;
                $conditions['returnDays']    = 'days';
                $usersData = $this->Jobpostadmin_Model->getRowsHiring($conditions);

            } else {
                $conditions['searchKeyword'] = "";
                $conditions['searchStatus'] = "";
                $conditions['returnDays']    = 'days';
                $usersData = $this->Jobpostadmin_Model->getRowsHiring($conditions);
            }

        } elseif($tabType == "weekwise") {

            if($this->session->userdata('searchKeyword') || $this->session->userdata('searchStatus')) {

                $inputKeywords = $this->session->userdata('searchKeyword');
                $searchKeyword = strip_tags($inputKeywords);

                $inputStatus = $this->session->userdata('searchStatus');
                $searchStatus = strip_tags($inputStatus);

                $conditions['searchKeyword'] = $searchKeyword;
                $conditions['searchStatus'] = $searchStatus;
                $conditions['returnDays']    = 'week';
                $usersData = $this->Jobpostadmin_Model->getRowsHiring($conditions);

            } else {
                $conditions['searchKeyword'] = "";
                $conditions['searchStatus'] = "";
                $conditions['returnDays']    = 'week';
                $usersData = $this->Jobpostadmin_Model->getRowsHiring($conditions);
            }

        } elseif($tabType == "monthwise") {

            if($this->session->userdata('searchKeyword') || $this->session->userdata('searchStatus')) {

                $inputKeywords = $this->session->userdata('searchKeyword');
                $searchKeyword = strip_tags($inputKeywords);

                $inputStatus = $this->session->userdata('searchStatus');
                $searchStatus = strip_tags($inputStatus);

                $conditions['searchKeyword'] = $searchKeyword;
                $conditions['searchStatus'] = $searchStatus;
                $conditions['returnDays']    = 'month';
                $usersData = $this->Jobpostadmin_Model->getRowsHiring($conditions);

            } else {
                $conditions['searchKeyword'] = "";
                $conditions['searchStatus'] = "";
                $conditions['returnDays']    = 'month';
                $usersData = $this->Jobpostadmin_Model->getRowsHiring($conditions);
            }

        } elseif($tabType == "yearwise") {

            if($this->session->userdata('searchKeyword') || $this->session->userdata('searchStatus')) {

                $inputKeywords = $this->session->userdata('searchKeyword');
                $searchKeyword = strip_tags($inputKeywords);

                $inputStatus = $this->session->userdata('searchStatus');
                $searchStatus = strip_tags($inputStatus);

                $conditions['searchKeyword'] = $searchKeyword;
                $conditions['searchStatus'] = $searchStatus;
                $conditions['returnDays']    = 'year';
                $usersData = $this->Jobpostadmin_Model->getRowsHiring($conditions);

            } else {
                $conditions['searchKeyword'] = "";
                $conditions['searchStatus'] = "";
                $conditions['returnDays']    = 'year';
                $usersData = $this->Jobpostadmin_Model->getRowsHiring($conditions);
            }

        } else {

            if($this->session->userdata('searchKeyword') || $this->session->userdata('searchStatus') || $this->session->userdata('searchFrom') || $this->session->userdata('searchTo')) {

                $inputKeywords = $this->session->userdata('searchKeyword');
                $searchKeyword = strip_tags($inputKeywords);

                $inputStatus = $this->session->userdata('searchStatus');
                $searchStatus = strip_tags($inputStatus);

                $inputFrom = $this->session->userdata('searchFrom');
                $searchFrom = strip_tags($inputFrom);

                $inputTo = $this->session->userdata('searchTo');
                $searchTo = strip_tags($inputTo);

                $conditions['searchKeyword'] = $searchKeyword;
                $conditions['searchStatus'] = $searchStatus;
                $conditions['searchFrom'] = $searchFrom;
                $conditions['searchTo'] = $searchTo;
                $conditions['returnDays']    = 'all';
                $usersData = $this->Jobpostadmin_Model->getRowsHiring($conditions);

            } else {
              // get data
                $conditions['searchKeyword'] = "";
                $conditions['searchStatus'] = "";
                $conditions['searchFrom'] = "";
                $conditions['searchTo'] = "";
                $conditions['returnDays']    = 'all';
                $usersData = $this->Jobpostadmin_Model->getRowsHiring($conditions);
            }

        }

        // file creation 
        $file = fopen('php://output','w');
        $header = array("SNo","Company Name","Site name","Job id","Job posted by","job title","Interview Mode","Job Category","Job Sub-Category","Candidate Id","Candidate Name","Candidate Email","Candidate Phone","Experience (Year)","Experience (Month)","Education","Job Level","Industry","Superpower","Specialization","Sub-Specialization","Work Mode","Vaccination","Relocate","Platform","Job Search status","Last company","App Version", "internetspeed","Applied Date","Salary Offer", "Total Guaranteed Allowance", "Status", "Last update", "Fall out reasion", "Hired date", "candidate Last managed by", "chatbot", "chatbot result", "Applied By"); 
        fputcsv($file, $header);

        $x1 =1;
        $dataArray = array();

        $dataArray = $this->modify_hiring_data($usersData);
          
        foreach ($dataArray as $key=>$line) { 
          fputcsv($file,$line); 
        }
        fclose($file); 
        exit; 
    }

    public function modify_hiring_data($data) {

        $dataArray = array();
        $x1=1;
          foreach($data as $jobList) {
            $category = $this->Jobpost_Model->categorybyid($jobList['category']);
            
            $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
            if($subcategory) {
                $subcategoryy = $subcategory[0]['subcategory'];
            } else {
                $subcategoryy = '';
            }

            $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
            $expmonth = $jobList['exp_month'];
            $expyear = $jobList['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";    
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
            } else if($expyear >= 7) { 
                $expfilter = "10";
            }
            $salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);
            if($jobList['status']=='1'){
              $jobList['status'] = 'New Application';
            }else if($jobList['status']=='2'){
              $jobList['status']='No Show';
            }else if($jobList['status']=='3'){
              $jobList['status']='Fall Out';
            }else if($jobList['status']=='4'){
              $jobList['status']='Refer';
            }else if($jobList['status']=='5'){
              $jobList['status']='On Going Application';
            }else if($jobList['status']=='6'){
              $jobList['status']='Accepted JO';
            }else if($jobList['status']=='7'){
              $jobList['status']='Hired';
            }else if($jobList['status']=='8'){
              $jobList['status']='reschedule';
            } else {
              $jobList['status']='';
            }
            if($jobList['status']=='Hired'){
              $hiredDate= date('Y-m-d',strtotime($jobList['updated_at']));
            }else{
              $hiredDate = '';
            }
            if($jobList['chatbot'] == 0) {
              $chatboot = "No";
            } else {
              $chatboot = "Yes";
            }
            
            if($jobList['chatbot'] == 1) {
                $chatResult = base_url()."administrator/questionnaire_user_view/".base64_encode($jobList['id']).'/'.base64_encode($jobList['uid']);
                $chatResult = "<a href='".$chatResult."'>view</a>";
            } else {
                $chatResult = "----";
            }

            if(strlen($jobList['exp_month']) <=0) {
              $jobList['exp_month'] = 0;
            }
            if(strlen($jobList['exp_year']) <=0) {
              $jobList['exp_year'] = 0;
            }

            if(!empty($jobList['platform'])) { 
                                                               
                if($jobList['platform']=='ios') {  
                    $jobList['platform'] = "iOS";
                } elseif ($jobList['platform']=='android') {
                    $jobList['platform'] = "Android"; 
                } elseif ($jobList['platform']=='web') {
                    $jobList['platform'] = "Web"; 
                }
            } else {

                if(!empty($jobList['type']) && $jobList['type']=='apple') {
                    $jobList['platform'] = "iOS";
                } else {
                    $jobList['platform'] = "Web";
                }
            }

            if($jobList['jobsearch_status'] == 1) {
               $jobList['jobsearch_status'] = "Actively seeking";
            } else if($jobList['jobsearch_status'] == 2) {
               $jobList['jobsearch_status'] = "Open to offers";
            }  else if($jobList['jobsearch_status'] == 3) {
               $jobList['jobsearch_status'] = "Exploring";
            }

            if(strlen($jobList['topbpo']) > 0) {
                $lastCompany =  $jobList['topbpo'];
            } else {
                $lastCompany = "";
            }

            $adminapply = $this->Jobpost_Model->adminapplied_fetchhhh($jobList['uid'], $jobList['id']);
            if($adminapply) {
                $appliedBy  = "Admin";
            } else {
                $appliedBy  = "User";                
            }

              $dataArray[] = [
                  "srno"=> $x,
                  "recruiter_cname"=>$jobList['cname'],
                  "company_name"=>$companydetail[0]['cname'],
                  "job_id"=>$jobList['id'],
                  "posted_by"=>$jobList['fname'].' '.$jobList['lname'],
                  "job_title"=>$jobList['jobtitle'],
                  "mode"=>$jobList['mode'],
                  "category"=>$category[0]['category'],
                  "subcategory"=>$subcategoryy,
                  "user_id"=>$jobList['uid'],
                  "user_name"=>$jobList['name'],
                  "user_email"=>$jobList['email'],
                  "user_phone"=>$jobList['phone'],
                  "user_exp_year"=>$jobList['exp_year'].' Year',
                  "user_exp_month"=>$jobList['exp_month'].' Month',
                  "education"=>$jobList['education'],
                  "joblevel"=>$jobList['jobLevel'],
                  "industry"=>$jobList['industry'],
                  "superpower"=>$jobList['superpower'],
                  "specialization"=>$jobList['specialization'],
                  "sub_specialization"=>$jobList['sub_specialization'],
                  "work_mode"=>$jobList['work_mode'],
                  "vaccination"=>$jobList['vaccination'],
                  "relocate"=>$jobList['relocate'],

                  "platform"=>$jobList['platform'],
                  "jobsearch_status"=>$jobList['jobsearch_status'],
                  "last_company"=>$lastCompany,

                  "app_version"=>$jobList['app_version'],
                  "internetspeed"=>$jobList['internetspeed'],
                  "created_at"=>date('Y-m-d', strtotime($jobList['created_at'])),
                  "basic_salary"=>$salary[0]['basicsalary'],
                  "allowances"=>$jobList['allowance'],
                  "status"=>$jobList['status'],
                  "apply_date"=>date('Y-m-d',strtotime($jobList['updated_at'])),
                  "fallout"=>$jobList['fallout_reason'],
                  "hired_date"=>$hiredDate,
                  "managed_by"=>$jobList['fname'].' '.$jobList['lname'],
                  "chatbot"=>$chatboot,
                  "chat_result"=>$chatResult,
                  "appliedBy" => $appliedBy
              ];
              $x1++;
          }

          return $dataArray;
    } 


    public function jobmatch_report() {

        $this->session->unset_userdata('search');

        $data = array();
        $data["jobLists"] = [];
        //---------------------------------------------------------------------------------------
        
        $perPage1 = 10;

        $this->load->helper('url');
        $this->load->library('pagination');
        $this->load->library('session');

        // // If search request submitted

        if($this->input->post('submitSearch')) {

            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if(!empty($searchKeyword)){
                $this->session->set_userdata('searchKeyword',$searchKeyword);
            }else{
                $this->session->unset_userdata('searchKeyword');
            }

            $inputStatus = $this->input->post('status_dropdown');
            $searchStatus = strip_tags($inputStatus);

            if(!empty($searchStatus)) {
                $this->session->set_userdata('searchStatus',$searchStatus);
            }else{
                $this->session->unset_userdata('searchStatus');
            }

            $inputSort = $this->input->post('sort_dropdown');
            $searchSort = strip_tags($inputSort);
            if(!empty($searchSort)) {
                $this->session->set_userdata('searchSort',$searchSort);
            }else{
                $this->session->unset_userdata('searchSort');
            }

            if($this->input->post('from_date') || $this->input->post('to_date')) {

                $inputFrom = $this->input->post('from_date');
                $searchFrom = strip_tags($inputFrom);
                if(!empty($searchFrom)) {
                    $this->session->set_userdata('searchFrom',$searchFrom);
                }else{
                    $this->session->unset_userdata('searchFrom');
                }

                $inputTo = $this->input->post('to_date');
                $searchTo = strip_tags($inputTo);
                if(!empty($searchTo)) {
                    $this->session->set_userdata('searchTo',$searchTo);
                }else{
                    $this->session->unset_userdata('searchTo');
                }
            }
            
        } elseif($this->input->post('submitSearchReset')) {

            $this->session->unset_userdata('searchKeyword');
            $this->session->unset_userdata('searchStatus');
            $this->session->unset_userdata('searchFrom');
            $this->session->unset_userdata('searchTo');
            $this->session->unset_userdata('searchSort');
        
        } else {

            // $inputKeywords = $this->input->post('searchKeyword');
            // $searchKeyword = strip_tags($inputKeywords);
            // if(!empty($searchKeyword)){
            //     $this->session->set_userdata('searchKeyword',$searchKeyword);
            // }else{
            //     if($this->uri->segment(3) == 0) {
            //       $this->session->unset_userdata('searchKeyword');
            //     }
            // }

            // $inputStatus = $this->input->post('status_dropdown');
            // $searchStatus = strip_tags($inputStatus);
            // if(!empty($searchStatus)) {
            //     $this->session->set_userdata('searchStatus',$searchStatus);
            // }else{
            //     if($this->uri->segment(3) == 0) {
            //       $this->session->unset_userdata('searchStatus');
            //     }
            // }

            // $inputSort = $this->input->post('sort_dropdown');
            // $searchSort = strip_tags($inputSort);
            // if(!empty($searchSort)) {
            //     $this->session->set_userdata('searchSort',$searchSort);
            // }else{
            //     $this->session->unset_userdata('searchSort');
            // }

            // if($this->input->post('from_date') || $this->input->post('to_date')) {

            //     $inputFrom = $this->input->post('from_date');
            //     $searchFrom = strip_tags($inputFrom);
            //     if(!empty($searchFrom)) {
            //         $this->session->set_userdata('searchFrom',$searchFrom);
            //     }else{
            //         if($this->uri->segment(3) == 0) {
            //           $this->session->unset_userdata('searchFrom');
            //         }
            //     }

            //     $inputTo = $this->input->post('to_date');
            //     $searchTo = strip_tags($inputTo);
            //     if(!empty($searchTo)) {
            //         $this->session->set_userdata('searchTo',$searchTo);
            //     }else{
            //         if($this->uri->segment(3) == 0) {
            //           $this->session->unset_userdata('searchTo');
            //         }
            //     }
            // }
        }

        if($this->session->userdata('searchKeyword')) {
            $data['searchKeyword'] = $this->session->userdata('searchKeyword');
        } else {
            $data['searchKeyword'] = "";
        }
        if(isset($_GET['status'])) {
          $data['searchStatus'] = 1;
          $data['searchStatus'] = "";
        } else {
            if($this->session->userdata('searchStatus')) {
                $data['searchStatus'] = $this->session->userdata('searchStatus');
            } else {
                $data['searchStatus'] = "";
            }
            if($this->session->userdata('searchSort')) {
                $data['searchSort'] = $this->session->userdata('searchSort');
            } else {
                $data['searchSort'] = "";
            }
        }
        if($this->session->userdata('searchFrom')) {
            $data['searchFrom'] = $this->session->userdata('searchFrom');
        } else {
            $data['searchFrom'] = "";
        }
        if($this->session->userdata('searchTo')) {
            $data['searchTo'] = $this->session->userdata('searchTo');
        } else {
            $data['searchTo'] = "";
        }

        if($this->input->post('tabtype')) {
          $tabType = $this->input->post('tabtype');
        } else {

            if($this->uri->segment(3)) {
              $tabType = $this->uri->segment(3);
            } else {
                $tabType = "";
            }
            //$tabType = "";
        }

        //var_dump($tabType);die;
        if($tabType == "daywise") {

            if($this->uri->segment(3)) {
              $whichTab = $this->uri->segment(3);
            } else {
              $whichTab = "days";
            }

            // // Get rows count
            $conditions1['searchKeyword'] = $data['searchKeyword'];
            $conditions1['searchStatus'] = $data['searchStatus'];
            $conditions1['searchSort'] = $data['searchSort'];
            $conditions1['returnType']    = 'count';
            $conditions1['returnDays']    = 'days';
            $rowsCount1 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions1);
            $config1['base_url']    = base_url().'administrator/job_match/days';
            $config1['total_rows']  = $rowsCount1;
            if($whichTab == "days") {
              
              if($this->uri->segment(4)) {
                $offset1 = $this->uri->segment(4);
              } else {
                $offset1 = 0;
              }
            } else {
              $offset1 = 0;
            }
            $config1['per_page']    = 10;
            $config1['show_link'] = $offset1;
            // Get rows
            $conditions1['returnType'] = '';
            $conditions1['start'] = $offset1;
            $conditions1['limit'] = $config1['per_page'];

            $data_result1 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions1);
            $data['result1'] = $this->modify_jobmatch_data($data_result1, $conditions1['searchSort']);
            $data['resultTotal1'] = $rowsCount1;
            $data['pagination1'] = $config1;


            // Get rows count
            $conditions2['searchKeyword'] = '';
            $conditions2['searchStatus'] = '';
            $conditions2['searchSort'] = '';
            $conditions2['returnType']    = 'count';
            $conditions2['returnDays']    = 'week';
            $rowsCount2 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions2);
            $config2['base_url']    = base_url().'administrator/job_match/week';
            $config2['total_rows']  = $rowsCount2;
              $offset2 = 0;
            $config2['per_page'] = 10;
            $config2['show_link'] = $offset2;
            // Get rows
            $conditions2['returnType'] = '';
            $conditions2['start'] = $offset2;
            $conditions2['limit'] = $config2['per_page'];
            $data_result2 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions2);
            $data['result2'] = $this->modify_jobmatch_data($data_result2, $conditions2['searchSort']);
            $data['resultTotal2'] = $rowsCount2;
            $data['pagination2'] = $config2;


            // Get rows count
            $conditions3['searchKeyword'] = '';
            $conditions3['searchStatus'] = '';
            $conditions3['searchSort'] = '';
            $conditions3['returnType']    = 'count';
            $conditions3['returnDays']    = 'month';
            $rowsCount3 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions3);
            
            $config3['base_url']    = base_url().'administrator/job_match/month';
            $config3['total_rows']  = $rowsCount3;
              $offset3 = 0;
            $config3['per_page'] = 10;
            $config3['show_link'] = $offset3;
            // // Get rows
            $conditions3['returnType'] = '';
            $conditions3['start'] = $offset3;
            $conditions3['limit'] = $config3['per_page'];
            $data_result3 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions3);
            $data['result3'] = $this->modify_jobmatch_data($data_result3, $conditions3['searchSort']);
            $data['resultTotal3'] = $rowsCount3;
            $data['pagination3'] = $config3;


            // Get rows count
            $conditions4['searchKeyword'] = '';
            $conditions4['searchStatus'] = '';
            $conditions4['searchSort'] = '';
            $conditions4['returnType']    = 'count';
            $conditions4['returnDays']    = 'year';
            $rowsCount4 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions4);
            $config4['base_url']    = base_url().'administrator/job_match/year';
            $config4['total_rows']  = $rowsCount4;
              $offset4 = 0;
            $config4['per_page']    = 10;
            $config4['show_link'] = $offset4;
            // // Get rows
            $conditions4['returnType'] = '';
            $conditions4['start'] = $offset4;
            $conditions4['limit'] = $config4['per_page'];
            $data_result4 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions4);
            $data['result4'] = $this->modify_jobmatch_data($data_result4, $conditions4['searchSort']);
            $data['resultTotal4'] = $rowsCount4;
            $data['pagination4'] = $config4;

            // Get rows count
            $conditions5['searchKeyword'] = '';
            $conditions5['searchStatus'] = '';
            $conditions5['searchSort'] = '';
            $conditions5['returnType']    = 'count';
            $conditions5['returnDays']    = 'all';
            $rowsCount5 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions5);
            $config5['base_url']    = base_url().'administrator/job_match/datewise';
            $config5['total_rows']  = $rowsCount5;
              $offset5 = 0;
            $config5['per_page']    = 10;
            $config5['show_link'] = $offset5;
            // // Get rows
            $conditions5['returnType'] = '';
            $conditions5['start'] = $offset5;
            $conditions5['limit'] = $config5['per_page'];
            $data_result5 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions5);
            $data['result5'] = $this->modify_jobmatch_data($data_result5, $conditions5['searchSort']);
            $data['resultTotal5'] = $rowsCount5;
            $data['pagination5'] = $config5;

            $data['checkactive'] = "tab1"; 

        } else if($tabType == "weekwise") {

            if($this->uri->segment(3)) {
              $whichTab = $this->uri->segment(3);
            } else {
              $whichTab = "days";
            }

            // // Get rows count
            $conditions1['searchKeyword'] = '';
            $conditions1['searchStatus'] = '';
            $conditions1['searchSort'] = '';
            $conditions1['returnType']    = 'count';
            $conditions1['returnDays']    = 'days';
            $rowsCount1 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions1);
            $config1['base_url']    = base_url().'administrator/job_match/days';
            $config1['total_rows']  = $rowsCount1;
              $offset1 = 0;
            $config1['per_page']    = 10;
            $config1['show_link'] = $offset1;
            // Get rows
            $conditions1['returnType'] = '';
            $conditions1['start'] = $offset1;
            $conditions1['limit'] = $config1['per_page'];
            $data_result1 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions1);
            $data['result1'] = $this->modify_jobmatch_data($data_result1, $conditions1['searchSort']);
            $data['resultTotal1'] = $rowsCount1;
            $data['pagination1'] = $config1;

            // Get rows count
            $conditions2['searchKeyword'] = $data['searchKeyword'];
            $conditions2['searchStatus'] = $data['searchStatus'];
            $conditions2['searchSort'] = $data['searchSort'];
            $conditions2['returnType']    = 'count';
            $conditions2['returnDays']    = 'week';
            $rowsCount2 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions2);
            $config2['base_url']    = base_url().'administrator/job_match/week';
            $config2['total_rows']  = $rowsCount2;
            if($whichTab == "week") {
              
              if($this->uri->segment(4)) {
                $offset2 = $this->uri->segment(4);
              } else {
                $offset2 = 0;
              }

            } else {
              $offset2 = 0;
            }
            $config2['per_page'] = 10;
            $config2['show_link'] = $offset2;
            // Get rows
            $conditions2['returnType'] = '';
            $conditions2['start'] = $offset2;
            $conditions2['limit'] = $config2['per_page'];
            $data_result2 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions2);
            $data['result2'] = $this->modify_jobmatch_data($data_result2, $conditions2['searchSort']);
            $data['resultTotal2'] = $rowsCount2;
            $data['pagination2'] = $config2;


            // Get rows count
            $conditions3['searchKeyword'] = '';
            $conditions3['searchStatus'] = '';
            $conditions3['searchSort'] = '';
            $conditions3['returnType']    = 'count';
            $conditions3['returnDays']    = 'month';
            $rowsCount3 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions3);
            $config3['base_url']    = base_url().'administrator/job_match/month';
            $config3['total_rows']  = $rowsCount3;
              $offset3 = 0;
            $config3['per_page'] = 10;
            $config3['show_link'] = $offset3;
            // // Get rows
            $conditions3['returnType'] = '';
            $conditions3['start'] = $offset3;
            $conditions3['limit'] = $config3['per_page'];
            $data_result3 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions3);
            $data['result3'] = $this->modify_jobmatch_data($data_result3, $conditions3['searchSort']);
            $data['resultTotal3'] = $rowsCount3;
            $data['pagination3'] = $config3;


            // Get rows count
            $conditions4['searchKeyword'] = '';
            $conditions4['searchStatus'] = '';
            $conditions4['searchSort'] = '';
            $conditions4['returnType']    = 'count';
            $conditions4['returnDays']    = 'year';
            $rowsCount4 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions4);
            $config4['base_url']    = base_url().'administrator/job_match/year';
            $config4['total_rows']  = $rowsCount4;
              $offset4 = 0;
            $config4['per_page']    = 10;
            $config4['show_link'] = $offset4;
            // // Get rows
            $conditions4['returnType'] = '';
            $conditions4['start'] = $offset4;
            $conditions4['limit'] = $config4['per_page'];
            $data_result4 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions4);
            $data['result4'] = $this->modify_jobmatch_data($data_result4, $conditions4['searchSort']);
            $data['resultTotal4'] = $rowsCount4;
            $data['pagination4'] = $config4;


            // Get rows count
            $conditions5['searchKeyword'] = '';
            $conditions5['searchStatus'] = '';
            $conditions5['searchSort'] = '';
            $conditions5['returnType']    = 'count';
            $conditions5['returnDays']    = 'all';
            $rowsCount5 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions5);
            $config5['base_url']    = base_url().'administrator/job_match/datewise';
            $config5['total_rows']  = $rowsCount5;
              $offset5 = 0;
            $config5['per_page']    = 10;
            $config5['show_link'] = $offset5;
            // // Get rows
            $conditions5['returnType'] = '';
            $conditions5['start'] = $offset5;
            $conditions5['limit'] = $config5['per_page'];
            $data_result5 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions5);
            $data['result5'] = $this->modify_jobmatch_data($data_result5, $conditions5['searchSort']);
            $data['resultTotal5'] = $rowsCount5;
            $data['pagination5'] = $config5;

            $data['checkactive'] = "tab2";

        } else if($tabType == "monthwise") {

            if($this->uri->segment(3)) {
              $whichTab = $this->uri->segment(3);
            } else {
              $whichTab = "days";
            }

            // // Get rows count
            $conditions1['searchKeyword'] = '';
            $conditions1['searchStatus'] = '';
            $conditions1['searchSort'] = '';
            $conditions1['returnType']    = 'count';
            $conditions1['returnDays']    = 'days';
            $rowsCount1 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions1);
            $config1['base_url']    = base_url().'administrator/job_match/days';
            $config1['total_rows']  = $rowsCount1;
              $offset1 = 0;
            $config1['per_page']    = 10;
            $config1['show_link'] = $offset1;
            // Get rows
            $conditions1['returnType'] = '';
            $conditions1['start'] = $offset1;
            $conditions1['limit'] = $config1['per_page'];
            $data_result1 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions1);
            $data['result1'] = $this->modify_jobmatch_data($data_result1, $conditions1['searchSort']);
            $data['resultTotal1'] = $rowsCount1;
            $data['pagination1'] = $config1;

            // Get rows count
            $conditions2['searchKeyword'] = '';
            $conditions2['searchStatus'] = '';
            $conditions2['searchSort'] = '';
            $conditions2['returnType']    = 'count';
            $conditions2['returnDays']    = 'week';
            $rowsCount2 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions2);
            $config2['base_url']    = base_url().'administrator/job_match/week';
            $config2['total_rows']  = $rowsCount2;
              $offset2 = 0;
            $config2['per_page'] = 10;
            $config2['show_link'] = $offset2;
            // Get rows
            $conditions2['returnType'] = '';
            $conditions2['start'] = $offset2;
            $conditions2['limit'] = $config2['per_page'];
            $data_result2 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions2);
            $data['result2'] = $this->modify_jobmatch_data($data_result2, $conditions2['searchSort']);
            $data['resultTotal2'] = $rowsCount2;
            $data['pagination2'] = $config2;


            // Get rows count
            $conditions3['searchKeyword'] = $data['searchKeyword'];
            $conditions3['searchStatus'] = $data['searchStatus'];
            $conditions3['searchSort'] = $data['searchSort'];
            $conditions3['returnType']    = 'count';
            $conditions3['returnDays']    = 'month';
            $rowsCount3 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions3);
            
            $config3['base_url']    = base_url().'administrator/job_match/month';
            $config3['total_rows']  = $rowsCount3;
            if($whichTab == "month") {
              if($this->uri->segment(4)) {
                $offset3 = $this->uri->segment(4);
              } else {
                $offset3 = 0;
              }
            } else {
              $offset3 = 0;
            }
            $config3['per_page'] = 10;
            $config3['show_link'] = $offset3;
            // // Get rows
            $conditions3['returnType'] = '';
            $conditions3['start'] = $offset3;
            $conditions3['limit'] = $config3['per_page'];
            $data_result3 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions3);
            $data['result3'] = $this->modify_jobmatch_data($data_result3, $conditions3['searchSort']);
            $data['resultTotal3'] = $rowsCount3;
            $data['pagination3'] = $config3;

            // Get rows count
            $conditions4['searchKeyword'] = '';
            $conditions4['searchStatus'] = '';
            $conditions4['searchSort'] = '';
            $conditions4['returnType']    = 'count';
            $conditions4['returnDays']    = 'year';
            $rowsCount4 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions4);
            $config4['base_url']    = base_url().'administrator/job_match/year';
            $config4['total_rows']  = $rowsCount4;
              $offset4 = 0;
            $config4['per_page']    = 10;
            $config4['show_link'] = $offset4;
            // // Get rows
            $conditions4['returnType'] = '';
            $conditions4['start'] = $offset4;
            $conditions4['limit'] = $config4['per_page'];
            $data_result4 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions4);
            $data['result4'] = $this->modify_jobmatch_data($data_result4, $conditions4['searchSort']);
            $data['resultTotal4'] = $rowsCount4;
            $data['pagination4'] = $config4;


            // Get rows count
            $conditions5['searchKeyword'] = '';
            $conditions5['searchStatus'] = '';
            $conditions5['searchSort'] = '';
            $conditions5['returnType']    = 'count';
            $conditions5['returnDays']    = 'all';
            $rowsCount5 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions5);
            $config5['base_url']    = base_url().'administrator/job_match/datewise';
            $config5['total_rows']  = $rowsCount5;
              $offset5 = 0;
            $config5['per_page']    = 10;
            $config5['show_link'] = $offset5;
            // // Get rows
            $conditions5['returnType'] = '';
            $conditions5['start'] = $offset5;
            $conditions5['limit'] = $config5['per_page'];
            $data_result5 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions5);
            $data['result5'] = $this->modify_jobmatch_data($data_result5, $conditions5['searchSort']);
            $data['resultTotal5'] = $rowsCount5;
            $data['pagination5'] = $config5;

            $data['checkactive'] = "tab3";
          
        } else if($tabType == "yearwise") {

            if($this->uri->segment(3)) {
              $whichTab = $this->uri->segment(3);
            } else {
              $whichTab = "days";
            }

            // // Get rows count
            $conditions1['searchKeyword'] = '';
            $conditions1['searchStatus'] = '';
            $conditions1['searchSort'] = '';
            $conditions1['returnType']    = 'count';
            $conditions1['returnDays']    = 'days';
            $rowsCount1 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions1);
            $config1['base_url']    = base_url().'administrator/job_match/days';
            $config1['total_rows']  = $rowsCount1;
              $offset1 = 0;
            $config1['per_page']    = 10;
            $config1['show_link'] = $offset1;
            // Get rows
            $conditions1['returnType'] = '';
            $conditions1['start'] = $offset1;
            $conditions1['limit'] = $config1['per_page'];
            $data_result1 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions1);
            $data['result1'] = $this->modify_jobmatch_data($data_result1, $conditions1['searchSort']);
            $data['resultTotal1'] = $rowsCount1;
            $data['pagination1'] = $config1;

            // Get rows count
            $conditions2['searchKeyword'] = '';
            $conditions2['searchStatus'] = '';
            $conditions2['searchSort'] = '';
            $conditions2['returnType']    = 'count';
            $conditions2['returnDays']    = 'week';
            $rowsCount2 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions2);
            $config2['base_url']    = base_url().'administrator/job_match/week';
            $config2['total_rows']  = $rowsCount2;
              $offset2 = 0;
            $config2['per_page'] = 10;
            $config2['show_link'] = $offset2;
            // Get rows
            $conditions2['returnType'] = '';
            $conditions2['start'] = $offset2;
            $conditions2['limit'] = $config2['per_page'];
            $data_result2 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions2);
            $data['result2'] = $this->modify_jobmatch_data($data_result2, $conditions2['searchSort']);
            $data['resultTotal2'] = $rowsCount2;
            $data['pagination2'] = $config2;

            // Get rows count
            $conditions3['searchKeyword'] = '';
            $conditions3['searchStatus'] = '';
            $conditions3['searchSort'] = '';
            $conditions3['returnType']    = 'count';
            $conditions3['returnDays']    = 'month';
            $rowsCount3 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions3);
            $config3['base_url']    = base_url().'administrator/job_match/month';
            $config3['total_rows']  = $rowsCount3;
              $offset3 = 0;
            $config3['per_page'] = 10;
            $config3['show_link'] = $offset3;
            // // Get rows
            $conditions3['returnType'] = '';
            $conditions3['start'] = $offset3;
            $conditions3['limit'] = $config3['per_page'];
            $data_result3 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions3);
            $data['result3'] = $this->modify_jobmatch_data($data_result3, $conditions3['searchSort']);
            $data['resultTotal3'] = $rowsCount3;
            $data['pagination3'] = $config3;

            // Get rows count
            $conditions4['searchKeyword'] = $data['searchKeyword'];
            $conditions4['searchStatus'] = $data['searchStatus'];
            $conditions4['searchSort'] = $data['searchSort'];
            $conditions4['returnType']    = 'count';
            $conditions4['returnDays']    = 'year';
            $rowsCount4 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions4);
            $config4['base_url']    = base_url().'administrator/job_match/year';
            $config4['total_rows']  = $rowsCount4;
            if($whichTab == "year") {
              if($this->uri->segment(4)) {
                $offset4 = $this->uri->segment(4);
              } else {
                $offset4 = 0;
              }
            } else {
              $offset4 = 0;
            }
            $config4['per_page']    = 10;
            $config4['show_link'] = $offset4;
            // // Get rows
            $conditions4['returnType'] = '';
            $conditions4['start'] = $offset4;
            $conditions4['limit'] = $config4['per_page'];
            $data_result4 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions4);
            $data['result4'] = $this->modify_jobmatch_data($data_result4, $conditions4['searchSort']);
            $data['resultTotal4'] = $rowsCount4;
            $data['pagination4'] = $config4;


            // Get rows count
            $conditions5['searchKeyword'] = '';
            $conditions5['searchStatus'] = '';
            $conditions5['searchSort'] = '';
            $conditions5['returnType']    = 'count';
            $conditions5['returnDays']    = 'all';
            $rowsCount5 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions5);
            $config5['base_url']    = base_url().'administrator/job_match/datewise';
            $config5['total_rows']  = $rowsCount5;
              $offset5 = 0;
            $config5['per_page']    = 10;
            $config5['show_link'] = $offset5;
            // // Get rows
            $conditions5['returnType'] = '';
            $conditions5['start'] = $offset5;
            $conditions5['limit'] = $config5['per_page'];
            $data_result5 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions5);
            $data['result5'] = $this->modify_jobmatch_data($data_result5, $conditions5['searchSort']);
            $data['resultTotal5'] = $rowsCount5;
            $data['pagination5'] = $config5;

            $data['checkactive'] = "tab4";
            
        } else if($tabType == "datewise") {

            if($this->uri->segment(3)) {
              $whichTab = $this->uri->segment(3);
            } else {
              $whichTab = "days";
            }

            // // Get rows count
            $conditions1['searchKeyword'] = '';
            $conditions1['searchStatus'] = '';
            $conditions1['searchSort'] = '';
            $conditions1['returnType']    = 'count';
            $conditions1['returnDays']    = 'days';
            $rowsCount1 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions1);
            $config1['base_url']    = base_url().'administrator/job_match/days';
            $config1['total_rows']  = $rowsCount1;
            $offset1 = 0;
            $config1['per_page']    = 10;
            $config1['show_link'] = $offset1;
            // Get rows
            $conditions1['returnType'] = '';
            $conditions1['start'] = $offset1;
            $conditions1['limit'] = $config1['per_page'];
            $data_result1 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions1);
            $data['result1'] = $this->modify_jobmatch_data($data_result1, $conditions1['searchSort']);
            $data['resultTotal1'] = $rowsCount1;
            $data['pagination1'] = $config1;

            // Get rows count
            $conditions2['searchKeyword'] = '';
            $conditions2['searchStatus'] = '';
            $conditions2['searchSort'] = '';
            $conditions2['returnType']    = 'count';
            $conditions2['returnDays']    = 'week';
            $rowsCount2 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions2);
            $config2['base_url']    = base_url().'administrator/job_match/week';
            $config2['total_rows']  = $rowsCount2;
            $offset2 = 0;
            $config2['per_page'] = 10;
            $config2['show_link'] = $offset2;
            // Get rows
            $conditions2['returnType'] = '';
            $conditions2['start'] = $offset2;
            $conditions2['limit'] = $config2['per_page'];
            $data_result2 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions2);
            $data['result2'] = $this->modify_jobmatch_data($data_result2, $conditions2['searchSort']);
            $data['resultTotal2'] = $rowsCount2;
            $data['pagination2'] = $config2;


            // Get rows count
            $conditions3['searchKeyword'] = '';
            $conditions3['searchStatus'] = '';
            $conditions3['searchSort'] = '';
            $conditions3['returnType']    = 'count';
            $conditions3['returnDays']    = 'month';
            $rowsCount3 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions3);

            $config3['base_url']    = base_url().'administrator/job_match/month';
            $config3['total_rows']  = $rowsCount3;
            $offset3 = 0;
            $config3['per_page'] = 10;
            $config3['show_link'] = $offset3;
            // // Get rows
            $conditions3['returnType'] = '';
            $conditions3['start'] = $offset3;
            $conditions3['limit'] = $config3['per_page'];
            $data_result3 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions3);
            $data['result3'] = $this->modify_jobmatch_data($data_result3, $conditions3['searchSort']);
            $data['resultTotal3'] = $rowsCount3;
            $data['pagination3'] = $config3;


            // Get rows count
            $conditions4['searchKeyword'] = '';
            $conditions4['searchStatus'] = '';
            $conditions4['searchSort'] = '';
            $conditions4['returnType']    = 'count';
            $conditions4['returnDays']    = 'year';
            $rowsCount4 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions4);
            $config4['base_url']    = base_url().'administrator/job_match/year';
            $config4['total_rows']  = $rowsCount4;
            $offset4 = 0;
            $config4['per_page']    = 10;
            $config4['show_link'] = $offset4;
            // // Get rows
            $conditions4['returnType'] = '';
            $conditions4['start'] = $offset4;
            $conditions4['limit'] = $config4['per_page'];
            $data_result4 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions4);
            $data['result4'] = $this->modify_jobmatch_data($data_result4, $conditions4['searchSort']);
            $data['resultTotal4'] = $rowsCount4;
            $data['pagination4'] = $config4;


            // Get rows count
            $conditions5['searchKeyword'] = $data['searchKeyword'];
            $conditions5['searchStatus'] = $data['searchStatus'];
            $conditions5['searchSort'] = $data['searchSort'];
            $conditions5['searchFrom'] = $data['searchFrom'];
            $conditions5['searchTo'] = $data['searchTo'];

            $conditions5['returnType']    = 'count';
            $conditions5['returnDays']    = 'all';
            $rowsCount5 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions5);
            $config5['base_url']    = base_url().'administrator/job_match/datewise';
            $config5['total_rows']  = $rowsCount5;
            if($whichTab == "datewise") {
              if($this->uri->segment(4)) {
                $offset5 = $this->uri->segment(4);
              } else {
                $offset5 = 0;
              }
            } else {
              $offset5 = 0;
            }
            $config5['per_page']    = 10;
            $config5['show_link'] = $offset5;
            //var_dump($config5);die;
            // // Get rows
            $conditions5['returnType'] = '';
            $conditions5['start'] = $offset5;
            $conditions5['limit'] = $config5['per_page'];
            $data_result5 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions5);
            $data['result5'] = $this->modify_jobmatch_data($data_result5, $conditions5['searchSort']);

            $data['resultTotal5'] = $rowsCount5;
            $data['pagination5'] = $config5;

            $data['checkactive'] = "tab5";

        } else {

            if($this->uri->segment(3)) {
              $whichTab = $this->uri->segment(3);
            } else {
              $whichTab = "days";
            }

            // // Get rows count
            $conditions1['searchKeyword'] = $data['searchKeyword'];
            $conditions1['searchStatus'] = $data['searchStatus'];
            $conditions1['searchSort'] = $data['searchSort'];
            $conditions1['returnType']    = 'count';
            $conditions1['returnDays']    = 'days';
            $rowsCount1 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions1);
            $config1['base_url']    = base_url().'administrator/job_match/days';
            $config1['total_rows']  = $rowsCount1;
            if($whichTab == "days") {
              
              if($this->uri->segment(4)) {
                $offset1 = $this->uri->segment(4);
              } else {
                $offset1 = 0;
              }
            } else {
              $offset1 = 0;
            }
            $config1['per_page']    = 10;
            $config1['show_link'] = $offset1;
            // Get rows
            $conditions1['returnType'] = '';
            $conditions1['start'] = (int)$offset1;
            $conditions1['limit'] = $config1['per_page'];
            //var_dump($conditions1);die;
            $data_result1 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions1);
            $data['result1'] = $this->modify_jobmatch_data($data_result1, $conditions1['searchSort']);
            $data['resultTotal1'] = $rowsCount1;
            $data['pagination1'] = $config1;


            // Get rows count
            $conditions2['searchKeyword'] = $data['searchKeyword'];
            $conditions2['searchStatus'] = $data['searchStatus'];
            $conditions2['searchSort'] = $data['searchSort'];
            $conditions2['returnType']    = 'count';
            $conditions2['returnDays']    = 'week';
            $rowsCount2 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions2);
            $config2['base_url']    = base_url().'administrator/job_match/week';
            $config2['total_rows']  = $rowsCount2;
            if($whichTab == "week") {
              
              if($this->uri->segment(4)) {
                $offset2 = $this->uri->segment(4);
              } else {
                $offset2 = 0;
              }

            } else {
              $offset2 = 0;
            }
            $config2['per_page'] = 10;
            $config2['show_link'] = $offset2;
            // Get rows
            $conditions2['returnType'] = '';
            $conditions2['start'] = $offset2;
            $conditions2['limit'] = $config2['per_page'];
            $data_result2 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions2);
            $data['result2'] = $this->modify_jobmatch_data($data_result2, $conditions2['searchSort']);
            $data['resultTotal2'] = $rowsCount2;
            $data['pagination2'] = $config2;


            // Get rows count
            $conditions3['searchKeyword'] = $data['searchKeyword'];
            $conditions3['searchStatus'] = $data['searchStatus'];
            $conditions3['searchSort'] = $data['searchSort'];
            $conditions3['returnType']    = 'count';
            $conditions3['returnDays']    = 'month';
            $rowsCount3 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions3);

            $config3['base_url']    = base_url().'administrator/job_match/month';
            $config3['total_rows']  = $rowsCount3;
            if($whichTab == "month") {
              if($this->uri->segment(4)) {
                $offset3 = $this->uri->segment(4);
              } else {
                $offset3 = 0;
              }
            } else {
              $offset3 = 0;
            }
            $config3['per_page'] = 10;
            $config3['show_link'] = $offset3;
            // // Get rows
            $conditions3['returnType'] = '';
            $conditions3['start'] = $offset3;
            $conditions3['limit'] = $config3['per_page'];
            $data_result3 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions3);
            $data['result3'] = $this->modify_jobmatch_data($data_result3, $conditions3['searchSort']);
            $data['resultTotal3'] = $rowsCount3;
            $data['pagination3'] = $config3;


            // Get rows count
            $conditions4['searchKeyword'] = $data['searchKeyword'];
            $conditions4['searchStatus'] = $data['searchStatus'];
            $conditions4['searchSort'] = $data['searchSort'];
            $conditions4['returnType']    = 'count';
            $conditions4['returnDays']    = 'year';
            $rowsCount4 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions4);
            $config4['base_url']    = base_url().'administrator/job_match/year';
            $config4['total_rows']  = $rowsCount4;
            if($whichTab == "year") {
              if($this->uri->segment(4)) {
                $offset4 = $this->uri->segment(4);
              } else {
                $offset4 = 0;
              }
            } else {
              $offset4 = 0;
            }
            $config4['per_page']    = 10;
            $config4['show_link'] = $offset4;
            // // Get rows
            $conditions4['returnType'] = '';
            $conditions4['start'] = $offset4;
            $conditions4['limit'] = $config4['per_page'];
            $data_result4 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions4);
            $data['result4'] = $this->modify_jobmatch_data($data_result4, $conditions4['searchSort']);
            $data['resultTotal4'] = $rowsCount4;
            $data['pagination4'] = $config4;


            // Get rows count
            $conditions5['searchKeyword'] = $data['searchKeyword'];
            $conditions5['searchStatus'] = $data['searchStatus'];
            $conditions5['searchSort'] = $data['searchSort'];
            $conditions5['returnType']    = 'count';
            $conditions5['returnDays']    = 'all';
            $rowsCount5 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions5);
            $config5['base_url']    = base_url().'administrator/job_match/datewise';
            $config5['total_rows']  = $rowsCount5;
            if($whichTab == "year") {
              if($this->uri->segment(4)) {
                $offset5 = $this->uri->segment(4);
              } else {
                $offset5 = 0;
              }
            } else {
              $offset5 = 0;
            }
            $config5['per_page']    = 10;
            $config5['show_link'] = $offset5;
            // // Get rows
            $conditions5['returnType'] = '';
            $conditions5['start'] = $offset5;
            $conditions5['limit'] = $config5['per_page'];
            $data_result5 = $this->Jobpostadmin_Model->getRowsJobMatch($conditions5);
            $data['result5'] = $this->modify_jobmatch_data($data_result5, $conditions5['searchSort']);
            $data['resultTotal5'] = $rowsCount5;
            $data['pagination5'] = $config5;

            if($this->uri->segment(3)) {

              $whichTabing = $this->uri->segment(3);
              if($whichTabing == "days") {
                  $data['checkactive'] = "tab1"; 
              } else if($whichTabing == "week") {
                  $data['checkactive'] = "tab2";
              } else if($whichTabing == "month") {
                  $data['checkactive'] = "tab3";
              } else if($whichTabing == "year") {
                  $data['checkactive'] = "tab4";
              } else if($whichTabing == "datewise") {
                  $data['checkactive'] = "tab5";
              }
            } else {
              $data['checkactive'] = "tab1";
            }
        }

        //var_dump($data);die;
        //---------------------------------------------------------------------------------------

        $this->load->view('administrator/jobmatch_reports',$data);
    }


    public function export_csv_report_jobmatch() {

        $filename = 'users_'.date('Ymd').'.csv'; 
        header("Content-Description: File Transfer"); 
        header("Content-Disposition: attachment; filename=$filename"); 
        header("Content-Type: application/csv; ");

        $tabType = $_GET['tabtype'];

        if($tabType == "daywise") {

            if($this->session->userdata('searchKeyword') || $this->session->userdata('searchStatus')) {

                $inputKeywords = $this->session->userdata('searchKeyword');
                $searchKeyword = strip_tags($inputKeywords);

                $inputStatus = $this->session->userdata('searchStatus');
                $searchStatus = strip_tags($inputStatus);

                $inputSort = $this->input->post('sort_dropdown');
                $searchSort = strip_tags($inputSort);

                $conditions['searchKeyword'] = $searchKeyword;
                $conditions['searchStatus'] = $searchStatus;
                $conditions['searchSort'] = $searchSort;
                $conditions['returnDays']    = 'days';
                $usersData = $this->Jobpostadmin_Model->getRowsJobMatch($conditions);

            } else {
                $conditions['searchKeyword'] = "";
                $conditions['searchStatus'] = "";
                $conditions['searchSort'] = "";
                $conditions['returnDays']    = 'days';
                $usersData = $this->Jobpostadmin_Model->getRowsJobMatch($conditions);
            }

        } elseif($tabType == "weekwise") {

            if($this->session->userdata('searchKeyword') || $this->session->userdata('searchStatus')) {

                $inputKeywords = $this->session->userdata('searchKeyword');
                $searchKeyword = strip_tags($inputKeywords);

                $inputStatus = $this->session->userdata('searchStatus');
                $searchStatus = strip_tags($inputStatus);

                $inputSort = $this->input->post('sort_dropdown');
                $searchSort = strip_tags($inputSort);

                $conditions['searchKeyword'] = $searchKeyword;
                $conditions['searchStatus'] = $searchStatus;
                $conditions['searchSort'] = $searchSort;
                $conditions['returnDays']    = 'week';
                $usersData = $this->Jobpostadmin_Model->getRowsJobMatch($conditions);

            } else {
                $conditions['searchKeyword'] = "";
                $conditions['searchStatus'] = "";
                $conditions['searchSort'] = "";
                $conditions['returnDays']    = 'week';
                $usersData = $this->Jobpostadmin_Model->getRowsJobMatch($conditions);
            }

        } elseif($tabType == "monthwise") {

            if($this->session->userdata('searchKeyword') || $this->session->userdata('searchStatus')) {

                $inputKeywords = $this->session->userdata('searchKeyword');
                $searchKeyword = strip_tags($inputKeywords);

                $inputStatus = $this->session->userdata('searchStatus');
                $searchStatus = strip_tags($inputStatus);

                $inputSort = $this->input->post('sort_dropdown');
                $searchSort = strip_tags($inputSort);

                $conditions['searchKeyword'] = $searchKeyword;
                $conditions['searchStatus'] = $searchStatus;
                $conditions['searchSort'] = $searchSort;
                $conditions['returnDays']    = 'month';
                $usersData = $this->Jobpostadmin_Model->getRowsJobMatch($conditions);

            } else {
                $conditions['searchKeyword'] = "";
                $conditions['searchStatus'] = "";
                $conditions['searchSort'] = "";
                $conditions['returnDays']    = 'month';
                $usersData = $this->Jobpostadmin_Model->getRowsJobMatch($conditions);
            }

        } elseif($tabType == "yearwise") {

            if($this->session->userdata('searchKeyword') || $this->session->userdata('searchStatus')) {

                $inputKeywords = $this->session->userdata('searchKeyword');
                $searchKeyword = strip_tags($inputKeywords);

                $inputStatus = $this->session->userdata('searchStatus');
                $searchStatus = strip_tags($inputStatus);

                $inputSort = $this->input->post('sort_dropdown');
                $searchSort = strip_tags($inputSort);

                $conditions['searchKeyword'] = $searchKeyword;
                $conditions['searchStatus'] = $searchStatus;
                $conditions['searchSort'] = $searchSort;
                $conditions['returnDays']    = 'year';
                $usersData = $this->Jobpostadmin_Model->getRowsJobMatch($conditions);

            } else {
                $conditions['searchKeyword'] = "";
                $conditions['searchStatus'] = "";
                $conditions['searchSort'] = "";
                $conditions['returnDays']    = 'year';
                $usersData = $this->Jobpostadmin_Model->getRowsJobMatch($conditions);
            }

        } else {

            if($this->session->userdata('searchKeyword') || $this->session->userdata('searchStatus') || $this->session->userdata('searchFrom') || $this->session->userdata('searchTo')) {

                $inputKeywords = $this->session->userdata('searchKeyword');
                $searchKeyword = strip_tags($inputKeywords);

                $inputStatus = $this->session->userdata('searchStatus');
                $searchStatus = strip_tags($inputStatus);

                $inputSort = $this->input->post('sort_dropdown');
                $searchSort = strip_tags($inputSort);

                $inputFrom = $this->session->userdata('searchFrom');
                $searchFrom = strip_tags($inputFrom);

                $inputTo = $this->session->userdata('searchTo');
                $searchTo = strip_tags($inputTo);

                $conditions['searchKeyword'] = $searchKeyword;
                $conditions['searchStatus'] = $searchStatus;
                $conditions['searchSort'] = $searchSort;
                $conditions['searchFrom'] = $searchFrom;
                $conditions['searchTo'] = $searchTo;
                $conditions['returnDays']    = 'all';
                $usersData = $this->Jobpostadmin_Model->getRowsJobMatch($conditions);

            } else {
              // get data
                $conditions['searchKeyword'] = "";
                $conditions['searchStatus'] = "";
                $conditions['searchSort'] = "";
                $conditions['searchFrom'] = "";
                $conditions['searchTo'] = "";
                $conditions['returnDays']    = 'all';
                $usersData = $this->Jobpostadmin_Model->getRowsJobMatch($conditions);
            }

        }

        // file creation 
        $file = fopen('php://output','w');
        $header = array("SNo","Company Name","Site name","Job id","Job posted by","job title","Interview Mode","Job Category","Job Sub-Category","Candidate Id","Candidate Name","Candidate Email","Candidate Phone","Experience (Year)","Experience (Month)","Education","Job Level","Industry","Superpower","Specialization","Sub-Specialization","Work Mode","Vaccination","Relocate","Platform","Job Search status","Last company","App Version", "internetspeed","Applied Date","Salary Offer", "Total Guaranteed Allowance", "Status", "Last update", "Fall out reasion", "Hired date", "candidate Last managed by", "chatbot", "chatbot result"); 
        fputcsv($file, $header);

        $x1 =1;
        $dataArray = array();

        $dataArray = $this->modify_jobmatch_data($usersData, $conditions['searchSort']);
          
        foreach ($dataArray as $key=>$line) { 
          fputcsv($file,$line); 
        }
        fclose($file); 
        exit; 
    }

    public function modify_jobmatch_data($data, $sort) {

        $dataArray = array();
        $x1=1;
          foreach($data as $jobList) {
            $category = $this->Jobpost_Model->categorybyid($jobList['category']);
            
            $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
            if($subcategory) {
                $subcategoryy = $subcategory[0]['subcategory'];
            } else {
                $subcategoryy = '';
            }

            $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
            $expmonth = $jobList['exp_month'];
            $expyear = $jobList['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";    
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
            } else if($expyear >= 7) { 
                $expfilter = "10";
            }
            $salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);
            if($jobList['status']=='1'){
              $jobList['status'] = 'New Application';
            }else if($jobList['status']=='2'){
              $jobList['status']='No Show';
            }else if($jobList['status']=='3'){
              $jobList['status']='Fall Out';
            }else if($jobList['status']=='4'){
              $jobList['status']='Refer';
            }else if($jobList['status']=='5'){
              $jobList['status']='On Going Application';
            }else if($jobList['status']=='6'){
              $jobList['status']='Accepted JO';
            }else if($jobList['status']=='7'){
              $jobList['status']='Hired';
            }else if($jobList['status']=='8'){
              $jobList['status']='reschedule';
            } else {
              $jobList['status']='';
            }
            if($jobList['status']=='Hired'){
              $hiredDate= date('Y-m-d',strtotime($jobList['updated_at']));
            }else{
              $hiredDate = '';
            }
            if($jobList['chatbot'] == 0) {
              $chatboot = "No";
            } else {
              $chatboot = "Yes";
            }
            
            if($jobList['chatbot'] == 1) {
                $chatResult = base_url()."administrator/questionnaire_user_view/".base64_encode($jobList['id']).'/'.base64_encode($jobList['uid']);
                $chatResult = "<a href='".$chatResult."'>view</a>";
            } else {
                $chatResult = "----";
            }

            if(strlen($jobList['exp_month']) <=0) {
              $jobList['exp_month'] = 0;
            }
            if(strlen($jobList['exp_year']) <=0) {
              $jobList['exp_year'] = 0;
            }

            if(!empty($jobList['platform'])) { 
                                                               
                if($jobList['platform']=='ios') {  
                    $jobList['platform'] = "iOS";
                } elseif ($jobList['platform']=='android') {
                    $jobList['platform'] = "Android"; 
                } elseif ($jobList['platform']=='web') {
                    $jobList['platform'] = "Web"; 
                }
            } else {

                if(!empty($jobList['type']) && $jobList['type']=='apple') {
                    $jobList['platform'] = "iOS";
                } else {
                    $jobList['platform'] = "Web";
                }
            }

            if($jobList['jobsearch_status'] == 1) {
               $jobList['jobsearch_status'] = "Actively seeking";
            } else if($jobList['jobsearch_status'] == 2) {
               $jobList['jobsearch_status'] = "Open to offers";
            }  else if($jobList['jobsearch_status'] == 3) {
               $jobList['jobsearch_status'] = "Exploring";
            }

            if(strlen($jobList['topbpo']) > 0) {
                $lastCompany =  $jobList['topbpo'];
            } else {
                $lastCompany = "";
            }

            $adminapply = $this->Jobpost_Model->adminapplied_fetch($jobList['uid']);

            $dataArray[] = [
                  "srno"=> $x,
                  "recruiter_cname"=>$jobList['cname'],
                  "company_name"=>$companydetail[0]['cname'],
                  "job_id"=>$jobList['id'],
                  "posted_by"=>$jobList['fname'].' '.$jobList['lname'],
                  "job_title"=>$jobList['jobtitle'],
                  "mode"=>$jobList['mode'],
                  "category"=>$category[0]['category'],
                  "subcategory"=>$subcategoryy,
                  "user_id"=>$jobList['uid'],
                  "user_name"=>$jobList['name'],
                  "user_email"=>$jobList['email'],
                  "user_phone"=>$jobList['phone'],
                  "user_exp_year"=>$jobList['exp_year'].' Year',
                  "user_exp_month"=>$jobList['exp_month'].' Month',
                  "education"=>$jobList['education'],
                  "joblevel"=>$jobList['jobLevel'],
                  "industry"=>$jobList['industry'],
                  "superpower"=>$jobList['superpower'],
                  "specialization"=>$jobList['specialization'],
                  "sub_specialization"=>$jobList['sub_specialization'],
                  "work_mode"=>$jobList['work_mode'],
                  "vaccination"=>$jobList['vaccination'],
                  "relocate"=>$jobList['relocate'],

                  "platform"=>$jobList['platform'],
                  "jobsearch_status"=>$jobList['jobsearch_status'],
                  "last_company"=>$lastCompany,

                  "app_version"=>$jobList['app_version'],
                  "internetspeed"=>$jobList['internetspeed'],
                  "created_at"=>date('Y-m-d', strtotime($jobList['created_at'])),
                  "basic_salary"=>$salary[0]['basicsalary'],
                  "allowances"=>$jobList['allowance'],
                  "status"=>$jobList['status'],
                  "apply_date"=>date('Y-m-d',strtotime($jobList['updated_at'])),
                  "fallout"=>$jobList['fallout_reason'],
                  "hired_date"=>$hiredDate,
                  "managed_by"=>$jobList['fname'].' '.$jobList['lname'],
                  "chatbot"=>$chatboot,
                  "chat_result"=>$chatResult,
                  "adminjobcount" => count($adminapply),
                  "adminjobcountt" => $jobList['adminAppliedCount'],
              ];
              $x1++;
          }

          if(strlen($sort) > 0) {

                if($sort == "high_to_low") {
                    usort($dataArray, function($a, $b) {
                        return $a['adminjobcount'] < $b['adminjobcount'];
                    });

                    // array_multisort(array_map(function($element) {
                    //     return $dataArray['adminjobcount'];
                    // }, $array), SORT_DESC, $array);

                } else {
                    usort($dataArray, function($a, $b) {
                        return $a['adminjobcount'] > $b['adminjobcount'];
                    });
                }
          }

          return $dataArray;
    }


    public function jobmatching() {

        $jobid = $this->uri->segment(3);
        $userid = $this->uri->segment(4);

        if((strlen($jobid)>0) && strlen($userid)>0) {

            $checkjob = $this->Jobpostadmin_Model->getjobmatch_job($jobid);
            if(!$checkjob) {
                redirect("administrator/job_match");
                die;
            }
            $checkuser = $this->Jobpostadmin_Model->getjobmatch_user($userid);
            if(!$checkuser) {
                redirect("administrator/job_match");
                die;
            }

            if(strlen($checkuser[0]['latitude']) > 0) {
                $latitude = $checkuser[0]['latitude'];
            } else {
                $latitude = '14.5676285';
            }
            if(strlen($checkuser[0]['longitude']) > 0) {
                $longitude = $checkuser[0]['longitude'];
            } else {
                $longitude = '120.7397551';
            }

            $jobArray = array();
            $fetchjobs = $this->Jobpostadmin_Model->getjobmatch_fetchjob($userid, $latitude, $longitude);
            if($fetchjobs) {

                foreach($fetchjobs as $fetchjob) {

                    $category = $this->Jobpost_Model->categorybyid($fetchjob['category']);
                    if($category) {
                        $categoryy = $category[0]['category'];
                    } else {
                        $categoryy = "";
                    }
                    $subcategory = $this->Jobpost_Model->subcategorybyid($fetchjob['subcategory']);
                    if($subcategory) {
                        $subcategoryy = $subcategory[0]['subcategory'];
                    } else {
                        $subcategoryy = '';
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($fetchjob['company_id']);
                    $companydetailMore = $this->Jobpost_Model->job_detailLocation_fetch($fetchjob['company_id']);

                    $expmonth = $checkuser[0]['exp_month'];
                    $expyear = $checkuser[0]['exp_year'];
                    if($expyear == 0 && $expmonth == 0) {
                        $expfilter = "1";    
                    }else if($expyear == 0 && $expmonth < 6) {
                        $expfilter = "2";
                    } else if($expyear < 1 && $expmonth >= 6) {
                        $expfilter = "3";
                    } else if($expyear < 2 && $expyear >= 1) {
                        $expfilter = "4";
                    } else if($expyear < 3 && $expyear >= 2) {
                        $expfilter = "5";
                    }else if($expyear < 4 && $expyear >= 3) {
                        $expfilter = "6";
                    }else if($expyear < 5 && $expyear >= 4) {
                        $expfilter = "7";
                    }else if($expyear < 6 && $expyear >= 5) {
                        $expfilter = "8";
                    }else if($expyear < 7 && $expyear >= 6) {
                        $expfilter = "9";
                    } else if($expyear >= 7) { 
                        $expfilter = "10";
                    }
                    $salary = $this->Jobpost_Model->getsalaryexp($fetchjob['id'],$expfilter);

                    $jobArray[] = [
                        "id" => $fetchjob['id'],
                        "jobTitle" => $fetchjob['jobtitle'],
                        "category" => $categoryy,
                        "subcategory" => $subcategoryy,
                        "company_name" => $fetchjob['cname'],
                        "site_name" => $companydetail[0]['cname'],
                        "address" => $companydetailMore[0]['address'],
                        "opening" => $fetchjob['opening'],
                        "salary" => $salary[0]['basicsalary'],
                        "experience" => $fetchjob['experience'],
                    ];
                }

                $data['jobArray'] = $jobArray;
                $data['userid'] = $userid;
                $this->load->view('administrator/jobs_for_jobmatch',$data);
                die;

            } else {

                $data['jobArray'] = [];
                $this->load->view('administrator/jobs_for_jobmatch',$data);
                die;
            }

        } else {

            redirect("administrator/job_match");
            die;
        }
    }

    public function applyjob() {
        
        $jobids = explode(',', $this->input->post('jobids'));
        $userid = $this->input->post('userid');
        if(count($jobids)>0) {

            foreach($jobids as $jobid) {

                $job_detail = $this->Jobpost_Model->job_detail_fetch($jobid);
                $user = $this->Jobpost_Model->getuser_email($userid);
                $companyname = $this->Candidate_Model->fetchcomp($job_detail[0]['recruiter_id']);

                $companydetail = $this->Jobpost_Model->job_detailLocation_fetch($job_detail[0]['company_id']);
                if(!empty($companydetail[0]['site_name'])) {
                    $sitenamee = $companydetail[0]['site_name'];
                } else {
                    $companydetail = $this->Candidate_Model->fetchcomp($job_detail[0]['company_id']);
                    $sitenamee = $companydetail[0]['cname'];
                }

                $jobexpire = date("Y-m-d", strtotime($job_detail[0]['jobexpire']));
                $schedule = date("Y-m-d");
                $interviewdate = date("Y-m-d");
                $interviewtime = date("H:i:s");
                $date = date('Y-m-d H:i:s');
                $messageee = "Thank You for applying through JobYoDA! Want a FREE Venti Coffee from us? Make sure to declare JobYoDA as your source of Application and send us an email with details of the company and site where you got hired at help@jobyoda.com. Please keep your lines open, the Recruiter will call you if you meet the job requirements! Good Luck!";

                $jobapply = [
                      "jobpost_id" => $jobid,
                      "user_id" => $userid,
                      "interviewdate" => $interviewdate,
                      "interviewtime" => $interviewtime,
                      "status" => 1,
                      "created_at" => $date,
                    ];
                $adminjobapply = [
                      "job_id" => $jobid,
                      "user_id" => $userid,
                    ];

                $this->Jobpost_Model->appliedjob_insert($jobapply);
                $this->Jobpost_Model->admin_appliedjob_insert($adminjobapply);
                    
                // $data2 = ['message'=>'You have got a new application from candidate'];
                // $this->Recruit_Model->Notificationmessageinsert($data2);

                $this->load->library('email');
                $this->email->initialize([
                          'protocol' => 'smtp',
                          'smtp_host' => 'smtpout.asia.secureserver.net',
                          'smtp_user' => 'jobrecommendation@jobyoda.com',
                          'smtp_pass' => 'Jobyoda@2k21',
                          'smtp_port' => 465,
                          'smtp_crypto' => 'ssl',
                          'charset'=>'utf-8',
                          'mailtype' => 'html',
                          'crlf' => "\r\n",
                          'newline' => "\r\n"
                ]);

                $data['cname'] = $companyname[0]['cname'];
                $data['rname'] = $companyname[0]['fname'].' '.$companyname[0]['lname'];
                $data['jobtitle'] = $job_detail[0]['jobtitle'];
                $data['name'] = $user[0]['name'];
                $data['phone'] =$user[0]['country_code'].$user[0]['phone'];
                $data['email'] =$user[0]['email'];
                $data['uid'] =$user[0]['id'];
                $msg = $this->load->view('administrator/jobapply_email_template',$data,TRUE);
            
                $this->email->from('help@jobyoda.com', "JobYoDA");
                $this->email->to($user[0]['email']);
                $this->email->subject('JobYoDA New Interview Alert - '.$companyname[0]['cname'].' - Date '.date('Y-m-d'));
                $this->email->message($msg);
                $this->email->send();

                redirect("administrator/job_match");
                die;
            }

        } else {

            redirect("administrator/job_match");
            die;
        }
    }


    public function adminjobapplied() {

        $userid = $this->uri->segment(3);
        $jobArray = array();

            $fetchjobs = $this->Jobpostadmin_Model->getjobmatch_adminapply($userid);
            if($fetchjobs) {

                foreach($fetchjobs as $fetchjob) {

                    $category = $this->Jobpost_Model->categorybyid($fetchjob['category']);
                    if($category) {
                        $categoryy = $category[0]['category'];
                    } else {
                        $categoryy = "";
                    }
                    $subcategory = $this->Jobpost_Model->subcategorybyid($fetchjob['subcategory']);
                    if($subcategory) {
                        $subcategoryy = $subcategory[0]['subcategory'];
                    } else {
                        $subcategoryy = '';
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($fetchjob['company_id']);
                    $companydetailMore = $this->Jobpost_Model->job_detailLocation_fetch($fetchjob['company_id']);

                    $expmonth = $checkuser[0]['exp_month'];
                    $expyear = $checkuser[0]['exp_year'];
                    if($expyear == 0 && $expmonth == 0) {
                        $expfilter = "1";    
                    }else if($expyear == 0 && $expmonth < 6) {
                        $expfilter = "2";
                    } else if($expyear < 1 && $expmonth >= 6) {
                        $expfilter = "3";
                    } else if($expyear < 2 && $expyear >= 1) {
                        $expfilter = "4";
                    } else if($expyear < 3 && $expyear >= 2) {
                        $expfilter = "5";
                    }else if($expyear < 4 && $expyear >= 3) {
                        $expfilter = "6";
                    }else if($expyear < 5 && $expyear >= 4) {
                        $expfilter = "7";
                    }else if($expyear < 6 && $expyear >= 5) {
                        $expfilter = "8";
                    }else if($expyear < 7 && $expyear >= 6) {
                        $expfilter = "9";
                    } else if($expyear >= 7) { 
                        $expfilter = "10";
                    }
                    $salary = $this->Jobpost_Model->getsalaryexp($fetchjob['id'],$expfilter);

                    $jobArray[] = [
                        "id" => $fetchjob['id'],
                        "jobTitle" => $fetchjob['jobtitle'],
                        "category" => $categoryy,
                        "subcategory" => $subcategoryy,
                        "company_name" => $fetchjob['cname'],
                        "site_name" => $companydetail[0]['cname'],
                        "address" => $companydetailMore[0]['address'],
                        "opening" => $fetchjob['opening'],
                        "salary" => $salary[0]['basicsalary'],
                        "experience" => $fetchjob['experience'],
                    ];
                }

                $data['jobArray'] = $jobArray;
                $data['userid'] = $userid;
                $this->load->view('administrator/job_match_adminapply',$data);
                die;

            } else {

                $data['jobArray'] = [];
                $this->load->view('administrator/job_match_adminapply',$data);
                die;
            }
    }


    // public function screening_reports(){
    //     $this->load->view('administrator/screening_reports');
    // }

    public function screening_reports() {

        $this->session->unset_userdata('search');

        $data = array();
        $data["jobLists"] = [];
        //---------------------------------------------------------------------------------------
        
        $perPage1 = 10;

        $this->load->helper('url');
        $this->load->library('pagination');
        $this->load->library('session');

        // // If search request submitted

        if($this->input->post('submitSearch')) {

            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if(!empty($searchKeyword)){
                $this->session->set_userdata('searchKeyword',$searchKeyword);
            }else{
                $this->session->unset_userdata('searchKeyword');
            }

            if($this->input->post('from_date') || $this->input->post('to_date')) {

                $inputFrom = $this->input->post('from_date');
                $searchFrom = strip_tags($inputFrom);
                if(!empty($searchFrom)) {
                    $this->session->set_userdata('searchFrom',$searchFrom);
                }else{
                    $this->session->unset_userdata('searchFrom');
                }

                $inputTo = $this->input->post('to_date');
                $searchTo = strip_tags($inputTo);
                if(!empty($searchTo)) {
                    $this->session->set_userdata('searchTo',$searchTo);
                }else{
                    $this->session->unset_userdata('searchTo');
                }
            }
            
        } elseif($this->input->post('submitSearchReset')) {

            $this->session->unset_userdata('searchKeyword');
            $this->session->unset_userdata('searchFrom');
            $this->session->unset_userdata('searchTo');
        
        } else {

            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if(!empty($searchKeyword)){
                $this->session->set_userdata('searchKeyword',$searchKeyword);
            }else{
                if($this->uri->segment(3) == 0) {
                  $this->session->unset_userdata('searchKeyword');
                }
            }

            if($this->input->post('from_date') || $this->input->post('to_date')) {

                $inputFrom = $this->input->post('from_date');
                $searchFrom = strip_tags($inputFrom);
                if(!empty($searchFrom)) {
                    $this->session->set_userdata('searchFrom',$searchFrom);
                }else{
                    if($this->uri->segment(3) == 0) {
                      $this->session->unset_userdata('searchFrom');
                    }
                }

                $inputTo = $this->input->post('to_date');
                $searchTo = strip_tags($inputTo);
                if(!empty($searchTo)) {
                    $this->session->set_userdata('searchTo',$searchTo);
                }else{
                    if($this->uri->segment(3) == 0) {
                      $this->session->unset_userdata('searchTo');
                    }
                }
            }
        }

        $data['searchKeyword'] = $this->session->userdata('searchKeyword');
        $data['searchFrom'] = $this->session->userdata('searchFrom');
        $data['searchTo'] = $this->session->userdata('searchTo');

        if($this->input->post('tabtype')) {
          $tabType = $this->input->post('tabtype');
        } else {
          $tabType = "";
        }

        if($tabType == "daywise") {

            if($this->uri->segment(3)) {
              $whichTab = $this->uri->segment(3);
            } else {
              $whichTab = "days";
            }

            // // Get rows count
            $conditions1['searchKeyword'] = $data['searchKeyword'];
            $conditions1['returnType']    = 'count';
            $conditions1['returnDays']    = 'days';
            $rowsCount1 = $this->Jobpostadmin_Model->getRowsScreening($conditions1);
            $config1['base_url']    = base_url().'administrator/screening_report/days';
            $config1['total_rows']  = $rowsCount1;
            if($whichTab == "days") {
              
                if($this->uri->segment(4)) {
                    $offset1 = $this->uri->segment(4);
                } else {
                    $offset1 = 0;
                }
            } else {
                $offset1 = 0;
            }
            $config1['per_page']    = 10;
            $config1['show_link'] = $offset1;
            // Get rows
            $conditions1['returnType'] = '';
            $conditions1['start'] = $offset1;
            $conditions1['limit'] = $config1['per_page'];

            $data_result1 = $this->Jobpostadmin_Model->getRowsScreening($conditions1);
            $data['result1'] = $this->modify_screen_data($data_result1);
            $data['resultTotal1'] = $rowsCount1;
            $data['pagination1'] = $config1;


            // Get rows count
            $conditions2['searchKeyword'] = '';
            $conditions2['returnType']    = 'count';
            $conditions2['returnDays']    = 'week';
            $rowsCount2 = $this->Jobpostadmin_Model->getRowsScreening($conditions2);
            $config2['base_url']    = base_url().'administrator/screening_report/week';
            $config2['total_rows']  = $rowsCount2;
              $offset2 = 0;
            $config2['per_page'] = 10;
            $config2['show_link'] = $offset2;
            // Get rows
            $conditions2['returnType'] = '';
            $conditions2['start'] = $offset2;
            $conditions2['limit'] = $config2['per_page'];
            $data_result2 = $this->Jobpostadmin_Model->getRowsScreening($conditions2);
            $data['result2'] = $this->modify_screen_data($data_result2);
            $data['resultTotal2'] = $rowsCount2;
            $data['pagination2'] = $config2;


            // Get rows count
            $conditions3['searchKeyword'] = '';
            $conditions3['returnType']    = 'count';
            $conditions3['returnDays']    = 'month';
            $rowsCount3 = $this->Jobpostadmin_Model->getRowsScreening($conditions3);

            $config3['base_url']    = base_url().'administrator/screening_report/month';
            $config3['total_rows']  = $rowsCount3;
              $offset3 = 0;
            $config3['per_page'] = 10;
            $config3['show_link'] = $offset3;
            // // Get rows
            $conditions3['returnType'] = '';
            $conditions3['start'] = $offset3;
            $conditions3['limit'] = $config3['per_page'];
            $data_result3 = $this->Jobpostadmin_Model->getRowsScreening($conditions3);
            $data['result3'] = $this->modify_screen_data($data_result3);
            $data['resultTotal3'] = $rowsCount3;
            $data['pagination3'] = $config3;


            // Get rows count
            $conditions4['searchKeyword'] = '';
            $conditions4['returnType']    = 'count';
            $conditions4['returnDays']    = 'year';
            $rowsCount4 = $this->Jobpostadmin_Model->getRowsScreening($conditions4);
            $config4['base_url']    = base_url().'administrator/screening_report/year';
            $config4['total_rows']  = $rowsCount4;
              $offset4 = 0;
            $config4['per_page']    = 10;
            $config4['show_link'] = $offset4;
            // // Get rows
            $conditions4['returnType'] = '';
            $conditions4['start'] = $offset4;
            $conditions4['limit'] = $config4['per_page'];
            $data_result4 = $this->Jobpostadmin_Model->getRowsScreening($conditions4);
            $data['result4'] = $this->modify_screen_data($data_result4);
            $data['resultTotal4'] = $rowsCount4;
            $data['pagination4'] = $config4;

            // Get rows count
            $conditions5['searchKeyword'] = '';
            $conditions5['returnType']    = 'count';
            $conditions5['returnDays']    = 'all';
            $rowsCount5 = $this->Jobpostadmin_Model->getRowsScreening($conditions5);
            $config5['base_url']    = base_url().'administrator/screening_report/datewise';
            $config5['total_rows']  = $rowsCount5;
              $offset5 = 0;
            $config5['per_page']    = 10;
            $config5['show_link'] = $offset5;
            // // Get rows
            $conditions5['returnType'] = '';
            $conditions5['start'] = $offset5;
            $conditions5['limit'] = $config5['per_page'];
            $data_result5 = $this->Jobpostadmin_Model->getRowsScreening($conditions5);
            $data['result5'] = $this->modify_screen_data($data_result5);
            $data['resultTotal5'] = $rowsCount5;
            $data['pagination5'] = $config5;

            $data['checkactive'] = "tab1"; 

        } else if($tabType == "weekwise") {

            if($this->uri->segment(3)) {
              $whichTab = $this->uri->segment(3);
            } else {
              $whichTab = "days";
            }

            // // Get rows count
            $conditions1['searchKeyword'] = '';
            $conditions1['returnType']    = 'count';
            $conditions1['returnDays']    = 'days';
            $rowsCount1 = $this->Jobpostadmin_Model->getRowsScreening($conditions1);
            $config1['base_url']    = base_url().'administrator/screening_report/days';
            $config1['total_rows']  = $rowsCount1;
              $offset1 = 0;
            $config1['per_page']    = 10;
            $config1['show_link'] = $offset1;
            // Get rows
            $conditions1['returnType'] = '';
            $conditions1['start'] = $offset1;
            $conditions1['limit'] = $config1['per_page'];
            $data_result1 = $this->Jobpostadmin_Model->getRowsScreening($conditions1);
            $data['result1'] = $this->modify_screen_data($data_result1);
            $data['resultTotal1'] = $rowsCount1;
            $data['pagination1'] = $config1;

            // Get rows count
            $conditions2['searchKeyword'] = $data['searchKeyword'];
            $conditions2['returnType']    = 'count';
            $conditions2['returnDays']    = 'week';
            $rowsCount2 = $this->Jobpostadmin_Model->getRowsScreening($conditions2);
            $config2['base_url']    = base_url().'administrator/screening_report/week';
            $config2['total_rows']  = $rowsCount2;
            if($whichTab == "week") {
              
              if($this->uri->segment(4)) {
                $offset2 = $this->uri->segment(4);
              } else {
                $offset2 = 0;
              }

            } else {
              $offset2 = 0;
            }
            $config2['per_page'] = 10;
            $config2['show_link'] = $offset2;
            // Get rows
            $conditions2['returnType'] = '';
            $conditions2['start'] = $offset2;
            $conditions2['limit'] = $config2['per_page'];
            $data_result2 = $this->Jobpostadmin_Model->getRowsScreening($conditions2);
            $data['result2'] = $this->modify_screen_data($data_result2);
            $data['resultTotal2'] = $rowsCount2;
            $data['pagination2'] = $config2;


            // Get rows count
            $conditions3['searchKeyword'] = '';
            $conditions3['returnType']    = 'count';
            $conditions3['returnDays']    = 'month';
            $rowsCount3 = $this->Jobpostadmin_Model->getRowsScreening($conditions3);
            $config3['base_url']    = base_url().'administrator/screening_report/month';
            $config3['total_rows']  = $rowsCount3;
              $offset3 = 0;
            $config3['per_page'] = 10;
            $config3['show_link'] = $offset3;
            // // Get rows
            $conditions3['returnType'] = '';
            $conditions3['start'] = $offset3;
            $conditions3['limit'] = $config3['per_page'];
            $data_result3 = $this->Jobpostadmin_Model->getRowsScreening($conditions3);
            $data['result3'] = $this->modify_screen_data($data_result3);
            $data['resultTotal3'] = $rowsCount3;
            $data['pagination3'] = $config3;


            // Get rows count
            $conditions4['searchKeyword'] = '';
            $conditions4['returnType']    = 'count';
            $conditions4['returnDays']    = 'year';
            $rowsCount4 = $this->Jobpostadmin_Model->getRowsScreening($conditions4);
            $config4['base_url']    = base_url().'administrator/screening_report/year';
            $config4['total_rows']  = $rowsCount4;
              $offset4 = 0;
            $config4['per_page']    = 10;
            $config4['show_link'] = $offset4;
            // // Get rows
            $conditions4['returnType'] = '';
            $conditions4['start'] = $offset4;
            $conditions4['limit'] = $config4['per_page'];
            $data_result4 = $this->Jobpostadmin_Model->getRowsScreening($conditions4);
            $data['result4'] = $this->modify_screen_data($data_result4);
            $data['resultTotal4'] = $rowsCount4;
            $data['pagination4'] = $config4;


            // Get rows count
            $conditions5['searchKeyword'] = '';
            $conditions5['returnType']    = 'count';
            $conditions5['returnDays']    = 'all';
            $rowsCount5 = $this->Jobpostadmin_Model->getRowsScreening($conditions5);
            $config5['base_url']    = base_url().'administrator/screening_report/datewise';
            $config5['total_rows']  = $rowsCount5;
              $offset5 = 0;
            $config5['per_page']    = 10;
            $config5['show_link'] = $offset5;
            // // Get rows
            $conditions5['returnType'] = '';
            $conditions5['start'] = $offset5;
            $conditions5['limit'] = $config5['per_page'];
            $data_result5 = $this->Jobpostadmin_Model->getRowsScreening($conditions5);
            $data['result5'] = $this->modify_screen_data($data_result5);
            $data['resultTotal5'] = $rowsCount5;
            $data['pagination5'] = $config5;

            $data['checkactive'] = "tab2";

        } else if($tabType == "monthwise") {

            if($this->uri->segment(3)) {
              $whichTab = $this->uri->segment(3);
            } else {
              $whichTab = "days";
            }

            // // Get rows count
            $conditions1['searchKeyword'] = '';
            $conditions1['returnType']    = 'count';
            $conditions1['returnDays']    = 'days';
            $rowsCount1 = $this->Jobpostadmin_Model->getRowsScreening($conditions1);
            $config1['base_url']    = base_url().'administrator/screening_report/days';
            $config1['total_rows']  = $rowsCount1;
              $offset1 = 0;
            $config1['per_page']    = 10;
            $config1['show_link'] = $offset1;
            // Get rows
            $conditions1['returnType'] = '';
            $conditions1['start'] = $offset1;
            $conditions1['limit'] = $config1['per_page'];
            $data_result1 = $this->Jobpostadmin_Model->getRowsScreening($conditions1);
            $data['result1'] = $this->modify_screen_data($data_result1);
            $data['resultTotal1'] = $rowsCount1;
            $data['pagination1'] = $config1;

            // Get rows count
            $conditions2['searchKeyword'] = '';
            $conditions2['returnType']    = 'count';
            $conditions2['returnDays']    = 'week';
            $rowsCount2 = $this->Jobpostadmin_Model->getRowsScreening($conditions2);
            $config2['base_url']    = base_url().'administrator/screening_report/week';
            $config2['total_rows']  = $rowsCount2;
              $offset2 = 0;
            $config2['per_page'] = 10;
            $config2['show_link'] = $offset2;
            // Get rows
            $conditions2['returnType'] = '';
            $conditions2['start'] = $offset2;
            $conditions2['limit'] = $config2['per_page'];
            $data_result2 = $this->Jobpostadmin_Model->getRowsScreening($conditions2);
            $data['result2'] = $this->modify_screen_data($data_result2);
            $data['resultTotal2'] = $rowsCount2;
            $data['pagination2'] = $config2;


            // Get rows count
            $conditions3['searchKeyword'] = $data['searchKeyword'];
            $conditions3['returnType']    = 'count';
            $conditions3['returnDays']    = 'month';
            $rowsCount3 = $this->Jobpostadmin_Model->getRowsScreening($conditions3);

            $config3['base_url']    = base_url().'administrator/screening_report/month';
            $config3['total_rows']  = $rowsCount3;
            if($whichTab == "month") {
              if($this->uri->segment(4)) {
                $offset3 = $this->uri->segment(4);
              } else {
                $offset3 = 0;
              }
            } else {
              $offset3 = 0;
            }
            $config3['per_page'] = 10;
            $config3['show_link'] = $offset3;
            // // Get rows
            $conditions3['returnType'] = '';
            $conditions3['start'] = $offset3;
            $conditions3['limit'] = $config3['per_page'];
            $data_result3 = $this->Jobpostadmin_Model->getRowsScreening($conditions3);
            $data['result3'] = $this->modify_screen_data($data_result3);
            $data['resultTotal3'] = $rowsCount3;
            $data['pagination3'] = $config3;

            // Get rows count
            $conditions4['searchKeyword'] = '';
            $conditions4['returnType']    = 'count';
            $conditions4['returnDays']    = 'year';
            $rowsCount4 = $this->Jobpostadmin_Model->getRowsScreening($conditions4);
            $config4['base_url']    = base_url().'administrator/screening_report/year';
            $config4['total_rows']  = $rowsCount4;
              $offset4 = 0;
            $config4['per_page']    = 10;
            $config4['show_link'] = $offset4;
            // // Get rows
            $conditions4['returnType'] = '';
            $conditions4['start'] = $offset4;
            $conditions4['limit'] = $config4['per_page'];
            $data_result4 = $this->Jobpostadmin_Model->getRowsScreening($conditions4);
            $data['result4'] = $this->modify_screen_data($data_result4);
            $data['resultTotal4'] = $rowsCount4;
            $data['pagination4'] = $config4;


            // Get rows count
            $conditions5['searchKeyword'] = '';
            $conditions5['returnType']    = 'count';
            $conditions5['returnDays']    = 'all';
            $rowsCount5 = $this->Jobpostadmin_Model->getRowsScreening($conditions5);
            $config5['base_url']    = base_url().'administrator/screening_report/datewise';
            $config5['total_rows']  = $rowsCount5;
              $offset5 = 0;
            $config5['per_page']    = 10;
            $config5['show_link'] = $offset5;
            // // Get rows
            $conditions5['returnType'] = '';
            $conditions5['start'] = $offset5;
            $conditions5['limit'] = $config5['per_page'];
            $data_result5 = $this->Jobpostadmin_Model->getRowsScreening($conditions5);
            $data['result5'] = $this->modify_screen_data($data_result5);
            $data['resultTotal5'] = $rowsCount5;
            $data['pagination5'] = $config5;

            $data['checkactive'] = "tab3";
          
        } else if($tabType == "yearwise") {

            if($this->uri->segment(3)) {
              $whichTab = $this->uri->segment(3);
            } else {
              $whichTab = "days";
            }

            // // Get rows count
            $conditions1['searchKeyword'] = '';
            $conditions1['returnType']    = 'count';
            $conditions1['returnDays']    = 'days';
            $rowsCount1 = $this->Jobpostadmin_Model->getRowsScreening($conditions1);
            $config1['base_url']    = base_url().'administrator/screening_report/days';
            $config1['total_rows']  = $rowsCount1;
              $offset1 = 0;
            $config1['per_page']    = 10;
            $config1['show_link'] = $offset1;
            // Get rows
            $conditions1['returnType'] = '';
            $conditions1['start'] = $offset1;
            $conditions1['limit'] = $config1['per_page'];
            $data_result1 = $this->Jobpostadmin_Model->getRowsScreening($conditions1);
            $data['result1'] = $this->modify_screen_data($data_result1);
            $data['resultTotal1'] = $rowsCount1;
            $data['pagination1'] = $config1;

            // Get rows count
            $conditions2['searchKeyword'] = '';
            $conditions2['returnType']    = 'count';
            $conditions2['returnDays']    = 'week';
            $rowsCount2 = $this->Jobpostadmin_Model->getRowsScreening($conditions2);
            $config2['base_url']    = base_url().'administrator/screening_report/week';
            $config2['total_rows']  = $rowsCount2;
              $offset2 = 0;
            $config2['per_page'] = 10;
            $config2['show_link'] = $offset2;
            // Get rows
            $conditions2['returnType'] = '';
            $conditions2['start'] = $offset2;
            $conditions2['limit'] = $config2['per_page'];
            $data_result2 = $this->Jobpostadmin_Model->getRowsScreening($conditions2);
            $data['result2'] = $this->modify_screen_data($data_result2);
            $data['resultTotal2'] = $rowsCount2;
            $data['pagination2'] = $config2;

            // Get rows count
            $conditions3['searchKeyword'] = '';
            $conditions3['returnType']    = 'count';
            $conditions3['returnDays']    = 'month';
            $rowsCount3 = $this->Jobpostadmin_Model->getRowsScreening($conditions3);
            $config3['base_url']    = base_url().'administrator/screening_report/month';
            $config3['total_rows']  = $rowsCount3;
              $offset3 = 0;
            $config3['per_page'] = 10;
            $config3['show_link'] = $offset3;
            // // Get rows
            $conditions3['returnType'] = '';
            $conditions3['start'] = $offset3;
            $conditions3['limit'] = $config3['per_page'];
            $data_result3 = $this->Jobpostadmin_Model->getRowsScreening($conditions3);
            $data['result3'] = $this->modify_screen_data($data_result3);
            $data['resultTotal3'] = $rowsCount3;
            $data['pagination3'] = $config3;

            // Get rows count
            $conditions4['searchKeyword'] = $data['searchKeyword'];
            $conditions4['returnType']    = 'count';
            $conditions4['returnDays']    = 'year';
            $rowsCount4 = $this->Jobpostadmin_Model->getRowsScreening($conditions4);
            $config4['base_url']    = base_url().'administrator/screening_report/year';
            $config4['total_rows']  = $rowsCount4;
            if($whichTab == "year") {
              if($this->uri->segment(4)) {
                $offset4 = $this->uri->segment(4);
              } else {
                $offset4 = 0;
              }
            } else {
              $offset4 = 0;
            }
            $config4['per_page']    = 10;
            $config4['show_link'] = $offset4;
            // // Get rows
            $conditions4['returnType'] = '';
            $conditions4['start'] = $offset4;
            $conditions4['limit'] = $config4['per_page'];
            $data_result4 = $this->Jobpostadmin_Model->getRowsScreening($conditions4);
            $data['result4'] = $this->modify_screen_data($data_result4);
            $data['resultTotal4'] = $rowsCount4;
            $data['pagination4'] = $config4;


            // Get rows count
            $conditions5['searchKeyword'] = '';
            $conditions5['returnType']    = 'count';
            $conditions5['returnDays']    = 'all';
            $rowsCount5 = $this->Jobpostadmin_Model->getRowsScreening($conditions5);
            $config5['base_url']    = base_url().'administrator/screening_report/datewise';
            $config5['total_rows']  = $rowsCount5;
              $offset5 = 0;
            $config5['per_page']    = 10;
            $config5['show_link'] = $offset5;
            // // Get rows
            $conditions5['returnType'] = '';
            $conditions5['start'] = $offset5;
            $conditions5['limit'] = $config5['per_page'];
            $data_result5 = $this->Jobpostadmin_Model->getRowsScreening($conditions5);
            $data['result5'] = $this->modify_screen_data($data_result5);
            $data['resultTotal5'] = $rowsCount5;
            $data['pagination5'] = $config5;

            $data['checkactive'] = "tab4";
            
        } else if($tabType == "datewise") {

            if($this->uri->segment(3)) {
              $whichTab = $this->uri->segment(3);
            } else {
              $whichTab = "days";
            }

            // // Get rows count
            $conditions1['searchKeyword'] = '';
            $conditions1['returnType']    = 'count';
            $conditions1['returnDays']    = 'days';
            $rowsCount1 = $this->Jobpostadmin_Model->getRowsScreening($conditions1);
            $config1['base_url']    = base_url().'administrator/screening_report/days';
            $config1['total_rows']  = $rowsCount1;
            $offset1 = 0;
            $config1['per_page']    = 10;
            $config1['show_link'] = $offset1;
            // Get rows
            $conditions1['returnType'] = '';
            $conditions1['start'] = $offset1;
            $conditions1['limit'] = $config1['per_page'];
            $data_result1 = $this->Jobpostadmin_Model->getRowsScreening($conditions1);
            $data['result1'] = $this->modify_screen_data($data_result1);
            $data['resultTotal1'] = $rowsCount1;
            $data['pagination1'] = $config1;

            // Get rows count
            $conditions2['searchKeyword'] = '';
            $conditions2['returnType']    = 'count';
            $conditions2['returnDays']    = 'week';
            $rowsCount2 = $this->Jobpostadmin_Model->getRowsScreening($conditions2);
            $config2['base_url']    = base_url().'administrator/screening_report/week';
            $config2['total_rows']  = $rowsCount2;
            $offset2 = 0;
            $config2['per_page'] = 10;
            $config2['show_link'] = $offset2;
            // Get rows
            $conditions2['returnType'] = '';
            $conditions2['start'] = $offset2;
            $conditions2['limit'] = $config2['per_page'];
            $data_result2 = $this->Jobpostadmin_Model->getRowsScreening($conditions2);
            $data['result2'] = $this->modify_screen_data($data_result2);
            $data['resultTotal2'] = $rowsCount2;
            $data['pagination2'] = $config2;


            // Get rows count
            $conditions3['searchKeyword'] = '';
            $conditions3['returnType']    = 'count';
            $conditions3['returnDays']    = 'month';
            $rowsCount3 = $this->Jobpostadmin_Model->getRowsScreening($conditions3);

            $config3['base_url']    = base_url().'administrator/screening_report/month';
            $config3['total_rows']  = $rowsCount3;
            $offset3 = 0;
            $config3['per_page'] = 10;
            $config3['show_link'] = $offset3;
            // // Get rows
            $conditions3['returnType'] = '';
            $conditions3['start'] = $offset3;
            $conditions3['limit'] = $config3['per_page'];
            $data_result3 = $this->Jobpostadmin_Model->getRowsScreening($conditions3);
            $data['result3'] = $this->modify_screen_data($data_result3);
            $data['resultTotal3'] = $rowsCount3;
            $data['pagination3'] = $config3;


            // Get rows count
            $conditions4['searchKeyword'] = '';
            $conditions4['returnType']    = 'count';
            $conditions4['returnDays']    = 'year';
            $rowsCount4 = $this->Jobpostadmin_Model->getRowsScreening($conditions4);
            $config4['base_url']    = base_url().'administrator/screening_report/year';
            $config4['total_rows']  = $rowsCount4;
            $offset4 = 0;
            $config4['per_page']    = 10;
            $config4['show_link'] = $offset4;
            // // Get rows
            $conditions4['returnType'] = '';
            $conditions4['start'] = $offset4;
            $conditions4['limit'] = $config4['per_page'];
            $data_result4 = $this->Jobpostadmin_Model->getRowsScreening($conditions4);
            $data['result4'] = $this->modify_screen_data($data_result4);
            $data['resultTotal4'] = $rowsCount4;
            $data['pagination4'] = $config4;


            // Get rows count
            $conditions5['searchKeyword'] = $data['searchKeyword'];
            $conditions5['searchFrom'] = $data['searchFrom'];
            $conditions5['searchTo'] = $data['searchTo'];

            $conditions5['returnType']    = 'count';
            $conditions5['returnDays']    = 'all';
            $rowsCount5 = $this->Jobpostadmin_Model->getRowsScreening($conditions5);
            $config5['base_url']    = base_url().'administrator/screening_report/datewise';
            $config5['total_rows']  = $rowsCount5;
            if($whichTab == "year") {
              if($this->uri->segment(4)) {
                $offset5 = $this->uri->segment(4);
              } else {
                $offset5 = 0;
              }
            } else {
              $offset5 = 0;
            }
            $config5['per_page']    = 10;
            $config5['show_link'] = $offset5;
            // // Get rows
            $conditions5['returnType'] = '';
            $conditions5['start'] = $offset5;
            $conditions5['limit'] = $config5['per_page'];
            $data_result5 = $this->Jobpostadmin_Model->getRowsHiring($conditions5);
            $data['result5'] = $this->modify_screen_data($data_result5);
            $data['resultTotal5'] = $rowsCount5;
            $data['pagination5'] = $config5;

            $data['checkactive'] = "tab5";

        } else {

            if($this->uri->segment(3)) {
              $whichTab = $this->uri->segment(3);
            } else {
              $whichTab = "days";
            }

            // // Get rows count
            $conditions1['searchKeyword'] = $data['searchKeyword'];
            $conditions1['returnType']    = 'count';
            $conditions1['returnDays']    = 'days';
            $rowsCount1 = $this->Jobpostadmin_Model->getRowsScreening($conditions1);
            $config1['base_url']    = base_url().'administrator/screening_report/days';
            $config1['total_rows']  = $rowsCount1;
            if($whichTab == "days") {
              
              if($this->uri->segment(4)) {
                $offset1 = $this->uri->segment(4);
              } else {
                $offset1 = 0;
              }
            } else {
              $offset1 = 0;
            }

            $config1['per_page']    = 10;
            $config1['show_link'] = $offset1;
            // Get rows
            $conditions1['returnType'] = '';
            $conditions1['start'] = (int)$offset1;
            $conditions1['limit'] = $config1['per_page'];
            //var_dump($conditions1);die;
            $data_result1 = $this->Jobpostadmin_Model->getRowsScreening($conditions1);
            $data['result1'] = $this->modify_screen_data($data_result1);
            $data['resultTotal1'] = $rowsCount1;
            $data['pagination1'] = $config1;


            // Get rows count
            $conditions2['searchKeyword'] = $data['searchKeyword'];
            $conditions2['returnType']    = 'count';
            $conditions2['returnDays']    = 'week';
            $rowsCount2 = $this->Jobpostadmin_Model->getRowsScreening($conditions2);
            $config2['base_url']    = base_url().'administrator/screening_report/week';
            $config2['total_rows']  = $rowsCount2;
            if($whichTab == "week") {
              
              if($this->uri->segment(4)) {
                $offset2 = $this->uri->segment(4);
              } else {
                $offset2 = 0;
              }

            } else {
              $offset2 = 0;
            }
            $config2['per_page'] = 10;
            $config2['show_link'] = $offset2;
            // Get rows
            $conditions2['returnType'] = '';
            $conditions2['start'] = $offset2;
            $conditions2['limit'] = $config2['per_page'];

            $data_result2 = $this->Jobpostadmin_Model->getRowsScreening($conditions2);
            $data['result2'] = $this->modify_screen_data($data_result2);
            $data['resultTotal2'] = $rowsCount2;
            $data['pagination2'] = $config2;


            // Get rows count
            $conditions3['searchKeyword'] = $data['searchKeyword'];
            $conditions3['returnType']    = 'count';
            $conditions3['returnDays']    = 'month';
            $rowsCount3 = $this->Jobpostadmin_Model->getRowsScreening($conditions3);

            $config3['base_url']    = base_url().'administrator/screening_report/month';
            $config3['total_rows']  = $rowsCount3;
            if($whichTab == "month") {
              if($this->uri->segment(4)) {
                $offset3 = $this->uri->segment(4);
              } else {
                $offset3 = 0;
              }
            } else {
              $offset3 = 0;
            }
            $config3['per_page'] = 10;
            $config3['show_link'] = $offset3;
            // // Get rows
            $conditions3['returnType'] = '';
            $conditions3['start'] = $offset3;
            $conditions3['limit'] = $config3['per_page'];
            $data_result3 = $this->Jobpostadmin_Model->getRowsScreening($conditions3);
            $data['result3'] = $this->modify_screen_data($data_result3);
            $data['resultTotal3'] = $rowsCount3;
            $data['pagination3'] = $config3;


            // Get rows count
            $conditions4['searchKeyword'] = $data['searchKeyword'];
            $conditions4['returnType']    = 'count';
            $conditions4['returnDays']    = 'year';
            $rowsCount4 = $this->Jobpostadmin_Model->getRowsScreening($conditions4);
            $config4['base_url']    = base_url().'administrator/screening_report/year';
            $config4['total_rows']  = $rowsCount4;
            if($whichTab == "year") {
              if($this->uri->segment(4)) {
                $offset4 = $this->uri->segment(4);
              } else {
                $offset4 = 0;
              }
            } else {
              $offset4 = 0;
            }
            $config4['per_page']    = 10;
            $config4['show_link'] = $offset4;
            // // Get rows
            $conditions4['returnType'] = '';
            $conditions4['start'] = $offset4;
            $conditions4['limit'] = $config4['per_page'];
            $data_result4 = $this->Jobpostadmin_Model->getRowsScreening($conditions4);
            $data['result4'] = $this->modify_screen_data($data_result4);
            $data['resultTotal4'] = $rowsCount4;
            $data['pagination4'] = $config4;


            // Get rows count
            $conditions5['searchKeyword'] = $data['searchKeyword'];
            $conditions5['returnType']    = 'count';
            $conditions5['returnDays']    = 'all';
            $rowsCount5 = $this->Jobpostadmin_Model->getRowsScreening($conditions5);
            $config5['base_url']    = base_url().'administrator/screening_report/datewise';
            $config5['total_rows']  = $rowsCount5;
            if($whichTab == "year") {
              if($this->uri->segment(4)) {
                $offset5 = $this->uri->segment(4);
              } else {
                $offset5 = 0;
              }
            } else {
              $offset5 = 0;
            }
            $config5['per_page']    = 10;
            $config5['show_link'] = $offset5;
            // // Get rows
            $conditions5['returnType'] = '';
            $conditions5['start'] = $offset5;
            $conditions5['limit'] = $config5['per_page'];
            $data_result5 = $this->Jobpostadmin_Model->getRowsScreening($conditions5);
            $data['result5'] = $this->modify_screen_data($data_result5);
            $data['resultTotal5'] = $rowsCount5;
            $data['pagination5'] = $config5;

            if($this->uri->segment(3)) {

              $whichTabing = $this->uri->segment(3);
              if($whichTabing == "days") {
                  $data['checkactive'] = "tab1"; 
              } else if($whichTabing == "week") {
                  $data['checkactive'] = "tab2";
              } else if($whichTabing == "month") {
                  $data['checkactive'] = "tab3";
              } else if($whichTabing == "year") {
                  $data['checkactive'] = "tab4";
              } else if($whichTabing == "datewise") {
                  $data['checkactive'] = "tab5";
              }
            } else {
              $data['checkactive'] = "tab1";  
            }
        }

        //var_dump($data);die;
        //---------------------------------------------------------------------------------------

        $this->load->view('administrator/screening_reports',$data);
    }

    public function export_csv_report_screening() {

        $filename = 'users_'.date('Ymd').'.csv'; 
        header("Content-Description: File Transfer"); 
        header("Content-Disposition: attachment; filename=$filename"); 
        header("Content-Type: application/csv; ");

        $tabType = $_GET['tabtype'];

        if($tabType == "daywise") {

            if($this->session->userdata('searchKeyword') || $this->session->userdata('searchStatus')) {

                $inputKeywords = $this->session->userdata('searchKeyword');
                $searchKeyword = strip_tags($inputKeywords);

                $conditions['searchKeyword'] = $searchKeyword;
                $conditions['returnDays']    = 'days';
                $usersData = $this->Jobpostadmin_Model->getRowsScreening($conditions);

            } else {
                $conditions['searchKeyword'] = "";
                $conditions['returnDays']    = 'days';
                $usersData = $this->Jobpostadmin_Model->getRowsScreening($conditions);
            }

        } elseif($tabType == "weekwise") {

            if($this->session->userdata('searchKeyword') || $this->session->userdata('searchStatus')) {

                $inputKeywords = $this->session->userdata('searchKeyword');
                $searchKeyword = strip_tags($inputKeywords);

                $conditions['searchKeyword'] = $searchKeyword;
                $conditions['returnDays']    = 'week';
                $usersData = $this->Jobpostadmin_Model->getRowsScreening($conditions);

            } else {
                $conditions['searchKeyword'] = "";
                $conditions['returnDays']    = 'week';
                $usersData = $this->Jobpostadmin_Model->getRowsScreening($conditions);
            }

        } elseif($tabType == "monthwise") {

            if($this->session->userdata('searchKeyword') || $this->session->userdata('searchStatus')) {

                $inputKeywords = $this->session->userdata('searchKeyword');
                $searchKeyword = strip_tags($inputKeywords);

                $conditions['searchKeyword'] = $searchKeyword;
                $conditions['returnDays']    = 'month';
                $usersData = $this->Jobpostadmin_Model->getRowsScreening($conditions);

            } else {
                $conditions['searchKeyword'] = "";
                $conditions['returnDays']    = 'month';
                $usersData = $this->Jobpostadmin_Model->getRowsScreening($conditions);
            }

        } elseif($tabType == "yearwise") {

            if($this->session->userdata('searchKeyword') || $this->session->userdata('searchStatus')) {

                $inputKeywords = $this->session->userdata('searchKeyword');
                $searchKeyword = strip_tags($inputKeywords);

                $conditions['searchKeyword'] = $searchKeyword;
                $conditions['returnDays']    = 'year';
                $usersData = $this->Jobpostadmin_Model->getRowsScreening($conditions);

            } else {
                $conditions['searchKeyword'] = "";
                $conditions['returnDays']    = 'year';
                $usersData = $this->Jobpostadmin_Model->getRowsScreening($conditions);
            }

        } else {

            if($this->session->userdata('searchKeyword') || $this->session->userdata('searchStatus') || $this->session->userdata('searchFrom') || $this->session->userdata('searchTo')) {

                $inputKeywords = $this->session->userdata('searchKeyword');
                $searchKeyword = strip_tags($inputKeywords);

                $inputFrom = $this->session->userdata('searchFrom');
                $searchFrom = strip_tags($inputFrom);

                $inputTo = $this->session->userdata('searchTo');
                $searchTo = strip_tags($inputTo);

                $conditions['searchKeyword'] = $searchKeyword;
                $conditions['searchFrom'] = $searchFrom;
                $conditions['searchTo'] = $searchTo;
                $conditions['returnDays']    = 'all';
                $usersData = $this->Jobpostadmin_Model->getRowsScreening($conditions);

            } else {
              // get data
                $conditions['searchKeyword'] = "";
                $conditions['searchFrom'] = "";
                $conditions['searchTo'] = "";
                $conditions['returnDays']    = 'all';
                $usersData = $this->Jobpostadmin_Model->getRowsScreening($conditions);
            }

        }

        // file creation 
        $file = fopen('php://output','w');
        $header = array("SNo","Company Name","Site name","Job id","Job posted by","job title","Interview mode","Job Category","Job Sub-Category","Candidate Id","Candidate Name","Candidate Email","Candidate Phone","Experience (Year)","Experience (Month)","Education", "Job Level","Industry","Superpower","Specialization","Sub-Specialization","Work Mode","Vaccination","Relocate","Platform","Job Search status","Last company","App Version","internetspeed","Screening date","Salary Offer", "Total Guaranteed Allowance", "Status"); 
        fputcsv($file, $header);

        $x1 =1;
        $dataArray = array();

        $dataArray = $this->modify_screen_data($usersData);
          
        foreach ($dataArray as $key=>$line) { 
          fputcsv($file,$line); 
        }
        fclose($file); 
        exit; 
    }

    public function modify_screen_data($data) {

        $dataArray = array();
        $x1=1;
          foreach($data as $jobList) {
            $category = $this->Jobpost_Model->categorybyid($jobList['category']);
            
            $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
            if($subcategory) {
                $subcategoryy = $subcategory[0]['subcategory'];
            } else {
                $subcategoryy = '';
            }

            $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
            $expmonth = $jobList['exp_month'];
            $expyear = $jobList['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";    
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
            } else if($expyear >= 7) { 
                $expfilter = "10";
            }
            $salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);
            
            if($jobList['status'] == 1) {
                $screenStatus = "Pending";
            } else {
                $screenStatus = "Completed";
            }

            if(strlen($jobList['exp_month']) <=0) {
              $jobList['exp_month'] = 0;
            }
            if(strlen($jobList['exp_year']) <=0) {
              $jobList['exp_year'] = 0;
            }

            if(!empty($jobList['platform'])) { 
                                                               
                if($jobList['platform']=='ios') {  
                     $jobList['platform'] = "iOS";
                } elseif ($jobList['platform']=='android') {
                     $jobList['platform'] = "Android"; 
                } elseif ($jobList['platform']=='web') {
                    $jobList['platform'] = "Web"; 
                } 
            } else {

                if(!empty($jobList['type']) && $jobList['type']=='apple') {
                    $jobList['platform'] = "iOS";
                } else {
                    $jobList['platform'] = "Web";
                }
            }

            if($jobList['jobsearch_status'] == 1) {
               $jobList['jobsearch_status'] = "Actively seeking";
            } else if($jobList['jobsearch_status'] == 2) {
               $jobList['jobsearch_status'] = "Open to offers";
            }  else if($jobList['jobsearch_status'] == 3) {
               $jobList['jobsearch_status'] = "Exploring";
            }

            if(strlen($jobList['topbpo']) > 0) {
                $lastCompany =  $jobList['topbpo'];
            } else {
                $lastCompany = "";
            }

              $dataArray[] = [
                  "srno"=> $x,
                  "recruiter_cname"=>$jobList['cname'],
                  "company_name"=>$companydetail[0]['cname'],
                  "job_id"=>$jobList['id'],
                  "posted_by"=>$jobList['fname'].' '.$jobList['lname'],
                  "job_title"=>$jobList['jobtitle'],
                  "interview_mode"=>"Instant Screening",
                  "category"=>$category[0]['category'],
                  "subcategory"=>$subcategoryy,
                  "user_id"=>$jobList['uid'],
                  "user_name"=>$jobList['name'],
                  "user_email"=>$jobList['email'],
                  "user_phone"=>$jobList['phone'],
                  "user_exp_year"=>$jobList['exp_year'].' Year',
                  "user_exp_month"=>$jobList['exp_month'].' Month',
                  "education"=>$jobList['education'],
                  "joblevel"=>$jobList['jobLevel'],
                  "industry"=>$jobList['industry'],
                  "superpower"=>$jobList['superpower'],
                  "specialization"=>$jobList['specialization'],
                  "sub_specialization"=>$jobList['sub_specialization'],
                  "work_mode"=>$jobList['work_mode'],
                  "vaccination"=>$jobList['vaccination'],
                  "relocate"=>$jobList['relocate'],
                  "platform"=>$jobList['platform'],
                  "jobsearch_status"=>$jobList['jobsearch_status'],
                  "last_company"=>$lastCompany,
                  "app_version"=>$jobList['app_version'],
                  "internetspeed"=>$jobList['internetspeed'],
                  "apply_date"=>date('Y-m-d',strtotime($jobList['created_at'])),
                  "basic_salary"=>$salary[0]['basicsalary'],
                  "allowances"=>$jobList['allowance'],
                  "status"=>$screenStatus,
              ];
              $x1++;
          }

          return $dataArray;
    } 

    public function hirecost() {

        $this->session->unset_userdata('search');

        $listings = $this->Recruiteradmin_Model->recruitercost_lists(); 
        $listget = array();

        if($listings) {
            foreach($listings as $listing) {
               $Listcount = $this->Recruiteradmin_Model->hiredcandidates_lists($listing['id']);
               //echo "<pre>";
               //echo count($Listcount);
               $invoices = $this->Jobpostadmin_Model->get_invoices_data($listing['id']);
               if(count($Listcount) >0){
                  $cc= 1;
                  $countt = count($Listcount);
               } else{
                  $cc=0;
                  $countt = 0;
               }

               if(count($invoices) >0){
                  $ic= 1;
               } else{
                  $ic=0;
               }
               $listget[] = ["cname"=> $listing['cname'], "cost"=>$listing['cost'], "effective_date"=> $listing['effective_date'], "billing_date"=> $listing['billing_date'],"id"=>$listing['id'], "haveList"=> $cc,'countt'=>$countt , "iclist"=>$ic];
            }
        }
        $data["Lists"] = $listget;
      
        if(!empty($data["Lists"])){
            $this->load->view('administrator/hirecost', $data);
        }
        else{
            $this->session->set_flashdata('invoice_error','Invoice Not available');
            redirect("administrator/Jobpost/hirecost");
        }
    }

    public function notappliedjob_reports(){
        $data["jobLists"] = $this->Jobpostadmin_Model->notappliedjob_fetchAll();

        $this->load->view('administrator/notappliedjob_reports',$data);
    }

    public function user_reports() {

        $this->session->unset_userdata('search');

        $data = array();
        $data["jobLists"] = [];
        //---------------------------------------------------------------------------------------
        
        $perPage1 = 10;

        $this->load->helper('url');
        $this->load->library('pagination');
        $this->load->library('session');

        // // If search request submitted

        if($this->input->post('submitSearch')) {

            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if(!empty($searchKeyword)){
                $this->session->set_userdata('searchKeyword',$searchKeyword);
            }else{
                $this->session->unset_userdata('searchKeyword');
            }

            $inputStatus = $this->input->post('status_dropdown');
            $searchStatus = strip_tags($inputStatus);

            if(!empty($searchStatus)) {
                $this->session->set_userdata('searchStatus',$searchStatus);
            }else{
                $this->session->unset_userdata('searchStatus');
            }

            if($this->input->post('from_date') || $this->input->post('to_date')) {

                $inputFrom = $this->input->post('from_date');
                $searchFrom = strip_tags($inputFrom);
                if(!empty($searchFrom)) {
                    $this->session->set_userdata('searchFrom',$searchFrom);
                }else{
                    $this->session->unset_userdata('searchFrom');
                }

                $inputTo = $this->input->post('to_date');
                $searchTo = strip_tags($inputTo);
                if(!empty($searchTo)) {
                    $this->session->set_userdata('searchTo',$searchTo);
                }else{
                    $this->session->unset_userdata('searchTo');
                }
            }
            
        } elseif($this->input->post('submitSearchReset')) {

            $this->session->unset_userdata('searchKeyword');
            $this->session->unset_userdata('searchStatus');
            $this->session->unset_userdata('searchFrom');
            $this->session->unset_userdata('searchTo');
        
        } else {

            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if(!empty($searchKeyword)){
                $this->session->set_userdata('searchKeyword',$searchKeyword);
            }else{
                if($this->uri->segment(3) == 0) {
                  $this->session->unset_userdata('searchKeyword');
                }
            }

            $inputStatus = $this->input->post('status_dropdown');
            $searchStatus = strip_tags($inputStatus);
            if(!empty($searchStatus)) {
                $this->session->set_userdata('searchStatus',$searchStatus);
            }else{
                if($this->uri->segment(3) == 0) {
                  $this->session->unset_userdata('searchStatus');
                }
            }

            if($this->input->post('from_date') || $this->input->post('to_date')) {

                $inputFrom = $this->input->post('from_date');
                $searchFrom = strip_tags($inputFrom);
                if(!empty($searchFrom)) {
                    $this->session->set_userdata('searchFrom',$searchFrom);
                }else{
                    if($this->uri->segment(3) == 0) {
                      $this->session->unset_userdata('searchFrom');
                    }
                }

                $inputTo = $this->input->post('to_date');
                $searchTo = strip_tags($inputTo);
                if(!empty($searchTo)) {
                    $this->session->set_userdata('searchTo',$searchTo);
                }else{
                    if($this->uri->segment(3) == 0) {
                      $this->session->unset_userdata('searchTo');
                    }
                }
            }
        }

        $data['searchKeyword'] = $this->session->userdata('searchKeyword');
        if(isset($_GET['status'])) {
          $data['searchStatus'] = 1;
        } else {
          $data['searchStatus'] = $this->session->userdata('searchStatus');
        }
        $data['searchFrom'] = $this->session->userdata('searchFrom');
        $data['searchTo'] = $this->session->userdata('searchTo');

        if($this->input->post('tabtype')) {
          $tabType = $this->input->post('tabtype');
        } else {
          $tabType = "";
        }

        if($tabType == "daywise") {

            if($this->uri->segment(3)) {
              $whichTab = $this->uri->segment(3);
            } else {
              $whichTab = "days";
            }

            // // Get rows count
            $conditions1['searchKeyword'] = $data['searchKeyword'];
            $conditions1['searchStatus'] = $data['searchStatus'];
            $conditions1['returnType']    = 'count';
            $conditions1['returnDays']    = 'days';
            $rowsCount1 = $this->Jobpostadmin_Model->getRowsUsers($conditions1);
            $config1['base_url']    = base_url().'administrator/userreport/days';
            $config1['total_rows']  = $rowsCount1;
            if($whichTab == "days") {
              
              if($this->uri->segment(4)) {
                $offset1 = $this->uri->segment(4);
              } else {
                $offset1 = 0;
              }
            } else {
              $offset1 = 0;
            }
            $config1['per_page']    = 10;
            $config1['show_link'] = $offset1;
            // Get rows
            $conditions1['returnType'] = '';
            $conditions1['start'] = $offset1;
            $conditions1['limit'] = $config1['per_page'];

            $data_result1 = $this->Jobpostadmin_Model->getRowsUsers($conditions1);
            $data['result1'] = $data_result1;
            $data['resultTotal1'] = $rowsCount1;
            $data['pagination1'] = $config1;


            // Get rows count
            $conditions2['searchKeyword'] = '';
            $conditions2['searchStatus'] = '';
            $conditions2['returnType']    = 'count';
            $conditions2['returnDays']    = 'week';
            $rowsCount2 = $this->Jobpostadmin_Model->getRowsUsers($conditions2);
            $config2['base_url']    = base_url().'administrator/userreport/week';
            $config2['total_rows']  = $rowsCount2;
              $offset2 = 0;
            $config2['per_page'] = 10;
            $config2['show_link'] = $offset2;
            // Get rows
            $conditions2['returnType'] = '';
            $conditions2['start'] = $offset2;
            $conditions2['limit'] = $config2['per_page'];
            $data_result2 = $this->Jobpostadmin_Model->getRowsUsers($conditions2);
            $data['result2'] = $data_result2;
            $data['resultTotal2'] = $rowsCount2;
            $data['pagination2'] = $config2;


            // Get rows count
            $conditions3['searchKeyword'] = '';
            $conditions3['searchStatus'] = '';
            $conditions3['returnType']    = 'count';
            $conditions3['returnDays']    = 'month';
            $rowsCount3 = $this->Jobpostadmin_Model->getRowsUsers($conditions3);

            $config3['base_url']    = base_url().'administrator/userreport/month';
            $config3['total_rows']  = $rowsCount3;
              $offset3 = 0;
            $config3['per_page'] = 10;
            $config3['show_link'] = $offset3;
            // // Get rows
            $conditions3['returnType'] = '';
            $conditions3['start'] = $offset3;
            $conditions3['limit'] = $config3['per_page'];
            $data_result3 = $this->Jobpostadmin_Model->getRowsUsers($conditions3);
            $data['result3'] = $data_result3;
            $data['resultTotal3'] = $rowsCount3;
            $data['pagination3'] = $config3;


            // Get rows count
            $conditions4['searchKeyword'] = '';
            $conditions4['searchStatus'] = '';
            $conditions4['returnType']    = 'count';
            $conditions4['returnDays']    = 'year';
            $rowsCount4 = $this->Jobpostadmin_Model->getRowsUsers($conditions4);
            $config4['base_url']    = base_url().'administrator/userreport/year';
            $config4['total_rows']  = $rowsCount4;
              $offset4 = 0;
            $config4['per_page']    = 10;
            $config4['show_link'] = $offset4;
            // // Get rows
            $conditions4['returnType'] = '';
            $conditions4['start'] = $offset4;
            $conditions4['limit'] = $config4['per_page'];
            $data_result4 = $this->Jobpostadmin_Model->getRowsUsers($conditions4);
            $data['result4'] = $data_result4;
            $data['resultTotal4'] = $rowsCount4;
            $data['pagination4'] = $config4;

            // Get rows count
            $conditions5['searchKeyword'] = '';
            $conditions5['searchStatus'] = '';
            $conditions5['returnType']    = 'count';
            $conditions5['returnDays']    = 'all';
            $rowsCount5 = $this->Jobpostadmin_Model->getRowsUsers($conditions5);
            $config5['base_url']    = base_url().'administrator/userreport/datewise';
            $config5['total_rows']  = $rowsCount5;
              $offset5 = 0;
            $config5['per_page']    = 10;
            $config5['show_link'] = $offset5;
            // // Get rows
            $conditions5['returnType'] = '';
            $conditions5['start'] = $offset5;
            $conditions5['limit'] = $config5['per_page'];
            $data_result5 = $this->Jobpostadmin_Model->getRowsUsers($conditions5);
            $data['result5'] = $data_result5;
            $data['resultTotal5'] = $rowsCount5;
            $data['pagination5'] = $config5;

            $data['checkactive'] = "tab1"; 

        } else if($tabType == "weekwise") {

            if($this->uri->segment(3)) {
              $whichTab = $this->uri->segment(3);
            } else {
              $whichTab = "days";
            }

            // // Get rows count
            $conditions1['searchKeyword'] = '';
            $conditions1['searchStatus'] = '';
            $conditions1['returnType']    = 'count';
            $conditions1['returnDays']    = 'days';
            $rowsCount1 = $this->Jobpostadmin_Model->getRowsUsers($conditions1);
            $config1['base_url']    = base_url().'administrator/userreport/days';
            $config1['total_rows']  = $rowsCount1;
              $offset1 = 0;
            $config1['per_page']    = 10;
            $config1['show_link'] = $offset1;
            // Get rows
            $conditions1['returnType'] = '';
            $conditions1['start'] = $offset1;
            $conditions1['limit'] = $config1['per_page'];
            $data_result1 = $this->Jobpostadmin_Model->getRowsUsers($conditions1);
            $data['result1'] = $data_result1;
            $data['resultTotal1'] = $rowsCount1;
            $data['pagination1'] = $config1;

            // Get rows count
            $conditions2['searchKeyword'] = $data['searchKeyword'];
            $conditions2['searchStatus'] = $data['searchStatus'];
            $conditions2['returnType']    = 'count';
            $conditions2['returnDays']    = 'week';
            $rowsCount2 = $this->Jobpostadmin_Model->getRowsUsers($conditions2);
            $config2['base_url']    = base_url().'administrator/userreport/week';
            $config2['total_rows']  = $rowsCount2;
            if($whichTab == "week") {
              
              if($this->uri->segment(4)) {
                $offset2 = $this->uri->segment(4);
              } else {
                $offset2 = 0;
              }

            } else {
              $offset2 = 0;
            }
            $config2['per_page'] = 10;
            $config2['show_link'] = $offset2;
            // Get rows
            $conditions2['returnType'] = '';
            $conditions2['start'] = $offset2;
            $conditions2['limit'] = $config2['per_page'];
            $data_result2 = $this->Jobpostadmin_Model->getRowsUsers($conditions2);
            $data['result2'] = $data_result2;
            $data['resultTotal2'] = $rowsCount2;
            $data['pagination2'] = $config2;


            // Get rows count
            $conditions3['searchKeyword'] = '';
            $conditions3['searchStatus'] = '';
            $conditions3['returnType']    = 'count';
            $conditions3['returnDays']    = 'month';
            $rowsCount3 = $this->Jobpostadmin_Model->getRowsUsers($conditions3);
            $config3['base_url']    = base_url().'administrator/userreport/month';
            $config3['total_rows']  = $rowsCount3;
              $offset3 = 0;
            $config3['per_page'] = 10;
            $config3['show_link'] = $offset3;
            // // Get rows
            $conditions3['returnType'] = '';
            $conditions3['start'] = $offset3;
            $conditions3['limit'] = $config3['per_page'];
            $data_result3 = $this->Jobpostadmin_Model->getRowsUsers($conditions3);
            $data['result3'] = $data_result3;
            $data['resultTotal3'] = $rowsCount3;
            $data['pagination3'] = $config3;


            // Get rows count
            $conditions4['searchKeyword'] = '';
            $conditions4['searchStatus'] = '';
            $conditions4['returnType']    = 'count';
            $conditions4['returnDays']    = 'year';
            $rowsCount4 = $this->Jobpostadmin_Model->getRowsUsers($conditions4);
            $config4['base_url']    = base_url().'administrator/userreport/year';
            $config4['total_rows']  = $rowsCount4;
              $offset4 = 0;
            $config4['per_page']    = 10;
            $config4['show_link'] = $offset4;
            // // Get rows
            $conditions4['returnType'] = '';
            $conditions4['start'] = $offset4;
            $conditions4['limit'] = $config4['per_page'];
            $data_result4 = $this->Jobpostadmin_Model->getRowsUsers($conditions4);
            $data['result4'] = $data_result4;
            $data['resultTotal4'] = $rowsCount4;
            $data['pagination4'] = $config4;


            // Get rows count
            $conditions5['searchKeyword'] = '';
            $conditions5['searchStatus'] = '';
            $conditions5['returnType']    = 'count';
            $conditions5['returnDays']    = 'all';
            $rowsCount5 = $this->Jobpostadmin_Model->getRowsUsers($conditions5);
            $config5['base_url']    = base_url().'administrator/userreport/datewise';
            $config5['total_rows']  = $rowsCount5;
              $offset5 = 0;
            $config5['per_page']    = 10;
            $config5['show_link'] = $offset5;
            // // Get rows
            $conditions5['returnType'] = '';
            $conditions5['start'] = $offset5;
            $conditions5['limit'] = $config5['per_page'];
            $data_result5 = $this->Jobpostadmin_Model->getRowsUsers($conditions5);
            $data['result5'] = $data_result5;
            $data['resultTotal5'] = $rowsCount5;
            $data['pagination5'] = $config5;

            $data['checkactive'] = "tab2";

        } else if($tabType == "monthwise") {

            if($this->uri->segment(3)) {
              $whichTab = $this->uri->segment(3);
            } else {
              $whichTab = "days";
            }

            // // Get rows count
            $conditions1['searchKeyword'] = '';
            $conditions1['searchStatus'] = '';
            $conditions1['returnType']    = 'count';
            $conditions1['returnDays']    = 'days';
            $rowsCount1 = $this->Jobpostadmin_Model->getRowsUsers($conditions1);
            $config1['base_url']    = base_url().'administrator/userreport/days';
            $config1['total_rows']  = $rowsCount1;
              $offset1 = 0;
            $config1['per_page']    = 10;
            $config1['show_link'] = $offset1;
            // Get rows
            $conditions1['returnType'] = '';
            $conditions1['start'] = $offset1;
            $conditions1['limit'] = $config1['per_page'];
            $data_result1 = $this->Jobpostadmin_Model->getRowsUsers($conditions1);
            $data['result1'] = $data_result1;
            $data['resultTotal1'] = $rowsCount1;
            $data['pagination1'] = $config1;

            // Get rows count
            $conditions2['searchKeyword'] = '';
            $conditions2['searchStatus'] = '';
            $conditions2['returnType']    = 'count';
            $conditions2['returnDays']    = 'week';
            $rowsCount2 = $this->Jobpostadmin_Model->getRowsUsers($conditions2);
            $config2['base_url']    = base_url().'administrator/userreport/week';
            $config2['total_rows']  = $rowsCount2;
              $offset2 = 0;
            $config2['per_page'] = 10;
            $config2['show_link'] = $offset2;
            // Get rows
            $conditions2['returnType'] = '';
            $conditions2['start'] = $offset2;
            $conditions2['limit'] = $config2['per_page'];
            $data_result2 = $this->Jobpostadmin_Model->getRowsUsers($conditions2);
            $data['result2'] = $data_result2;
            $data['resultTotal2'] = $rowsCount2;
            $data['pagination2'] = $config2;


            // Get rows count
            $conditions3['searchKeyword'] = $data['searchKeyword'];
            $conditions3['searchStatus'] = $data['searchStatus'];
            $conditions3['returnType']    = 'count';
            $conditions3['returnDays']    = 'month';
            $rowsCount3 = $this->Jobpostadmin_Model->getRowsUsers($conditions3);

            $config3['base_url']    = base_url().'administrator/userreport/month';
            $config3['total_rows']  = $rowsCount3;
            if($whichTab == "month") {
              if($this->uri->segment(4)) {
                $offset3 = $this->uri->segment(4);
              } else {
                $offset3 = 0;
              }
            } else {
              $offset3 = 0;
            }
            $config3['per_page'] = 10;
            $config3['show_link'] = $offset3;
            // // Get rows
            $conditions3['returnType'] = '';
            $conditions3['start'] = $offset3;
            $conditions3['limit'] = $config3['per_page'];
            $data_result3 = $this->Jobpostadmin_Model->getRowsUsers($conditions3);
            $data['result3'] = $data_result3;
            $data['resultTotal3'] = $rowsCount3;
            $data['pagination3'] = $config3;

            // Get rows count
            $conditions4['searchKeyword'] = '';
            $conditions4['searchStatus'] = '';
            $conditions4['returnType']    = 'count';
            $conditions4['returnDays']    = 'year';
            $rowsCount4 = $this->Jobpostadmin_Model->getRowsUsers($conditions4);
            $config4['base_url']    = base_url().'administrator/userreport/year';
            $config4['total_rows']  = $rowsCount4;
              $offset4 = 0;
            $config4['per_page']    = 10;
            $config4['show_link'] = $offset4;
            // // Get rows
            $conditions4['returnType'] = '';
            $conditions4['start'] = $offset4;
            $conditions4['limit'] = $config4['per_page'];
            $data_result4 = $this->Jobpostadmin_Model->getRowsUsers($conditions4);
            $data['result4'] = $data_result4;
            $data['resultTotal4'] = $rowsCount4;
            $data['pagination4'] = $config4;


            // Get rows count
            $conditions5['searchKeyword'] = '';
            $conditions5['searchStatus'] = '';
            $conditions5['returnType']    = 'count';
            $conditions5['returnDays']    = 'all';
            $rowsCount5 = $this->Jobpostadmin_Model->getRowsUsers($conditions5);
            $config5['base_url']    = base_url().'administrator/userreport/datewise';
            $config5['total_rows']  = $rowsCount5;
              $offset5 = 0;
            $config5['per_page']    = 10;
            $config5['show_link'] = $offset5;
            // // Get rows
            $conditions5['returnType'] = '';
            $conditions5['start'] = $offset5;
            $conditions5['limit'] = $config5['per_page'];
            $data_result5 = $this->Jobpostadmin_Model->getRowsUsers($conditions5);
            $data['result5'] = $data_result5;
            $data['resultTotal5'] = $rowsCount5;
            $data['pagination5'] = $config5;

            $data['checkactive'] = "tab3";
          
        } else if($tabType == "yearwise") {

            if($this->uri->segment(3)) {
              $whichTab = $this->uri->segment(3);
            } else {
              $whichTab = "days";
            }

            // // Get rows count
            $conditions1['searchKeyword'] = '';
            $conditions1['searchStatus'] = '';
            $conditions1['returnType']    = 'count';
            $conditions1['returnDays']    = 'days';
            $rowsCount1 = $this->Jobpostadmin_Model->getRowsUsers($conditions1);
            $config1['base_url']    = base_url().'administrator/userreport/days';
            $config1['total_rows']  = $rowsCount1;
              $offset1 = 0;
            $config1['per_page']    = 10;
            $config1['show_link'] = $offset1;
            // Get rows
            $conditions1['returnType'] = '';
            $conditions1['start'] = $offset1;
            $conditions1['limit'] = $config1['per_page'];
            $data_result1 = $this->Jobpostadmin_Model->getRowsUsers($conditions1);
            $data['result1'] = $data_result1;
            $data['resultTotal1'] = $rowsCount1;
            $data['pagination1'] = $config1;

            // Get rows count
            $conditions2['searchKeyword'] = '';
            $conditions2['searchStatus'] = '';
            $conditions2['returnType']    = 'count';
            $conditions2['returnDays']    = 'week';
            $rowsCount2 = $this->Jobpostadmin_Model->getRowsUsers($conditions2);
            $config2['base_url']    = base_url().'administrator/userreport/week';
            $config2['total_rows']  = $rowsCount2;
              $offset2 = 0;
            $config2['per_page'] = 10;
            $config2['show_link'] = $offset2;
            // Get rows
            $conditions2['returnType'] = '';
            $conditions2['start'] = $offset2;
            $conditions2['limit'] = $config2['per_page'];
            $data_result2 = $this->Jobpostadmin_Model->getRowsUsers($conditions2);
            $data['result2'] = $data_result2;
            $data['resultTotal2'] = $rowsCount2;
            $data['pagination2'] = $config2;

            // Get rows count
            $conditions3['searchKeyword'] = '';
            $conditions3['searchStatus'] = '';
            $conditions3['returnType']    = 'count';
            $conditions3['returnDays']    = 'month';
            $rowsCount3 = $this->Jobpostadmin_Model->getRowsUsers($conditions3);
            $config3['base_url']    = base_url().'administrator/userreport/month';
            $config3['total_rows']  = $rowsCount3;
              $offset3 = 0;
            $config3['per_page'] = 10;
            $config3['show_link'] = $offset3;
            // // Get rows
            $conditions3['returnType'] = '';
            $conditions3['start'] = $offset3;
            $conditions3['limit'] = $config3['per_page'];
            $data_result3 = $this->Jobpostadmin_Model->getRowsUsers($conditions3);
            $data['result3'] = $data_result3;
            $data['resultTotal3'] = $rowsCount3;
            $data['pagination3'] = $config3;

            // Get rows count
            $conditions4['searchKeyword'] = $data['searchKeyword'];
            $conditions4['searchStatus'] = $data['searchStatus'];
            $conditions4['returnType']    = 'count';
            $conditions4['returnDays']    = 'year';
            $rowsCount4 = $this->Jobpostadmin_Model->getRowsUsers($conditions4);
            $config4['base_url']    = base_url().'administrator/userreport/year';
            $config4['total_rows']  = $rowsCount4;
            if($whichTab == "year") {
              if($this->uri->segment(4)) {
                $offset4 = $this->uri->segment(4);
              } else {
                $offset4 = 0;
              }
            } else {
              $offset4 = 0;
            }
            $config4['per_page']    = 10;
            $config4['show_link'] = $offset4;
            // // Get rows
            $conditions4['returnType'] = '';
            $conditions4['start'] = $offset4;
            $conditions4['limit'] = $config4['per_page'];
            $data_result4 = $this->Jobpostadmin_Model->getRowsUsers($conditions4);
            $data['result4'] = $data_result4;
            $data['resultTotal4'] = $rowsCount4;
            $data['pagination4'] = $config4;


            // Get rows count
            $conditions5['searchKeyword'] = '';
            $conditions5['searchStatus'] = '';
            $conditions5['returnType']    = 'count';
            $conditions5['returnDays']    = 'all';
            $rowsCount5 = $this->Jobpostadmin_Model->getRowsUsers($conditions5);
            $config5['base_url']    = base_url().'administrator/userreport/datewise';
            $config5['total_rows']  = $rowsCount5;
              $offset5 = 0;
            $config5['per_page']    = 10;
            $config5['show_link'] = $offset5;
            // // Get rows
            $conditions5['returnType'] = '';
            $conditions5['start'] = $offset5;
            $conditions5['limit'] = $config5['per_page'];
            $data_result5 = $this->Jobpostadmin_Model->getRowsUsers($conditions5);
            $data['result5'] = $data_result5;
            $data['resultTotal5'] = $rowsCount5;
            $data['pagination5'] = $config5;

            $data['checkactive'] = "tab4";
            
        } else if($tabType == "datewise") {

            if($this->uri->segment(3)) {
              $whichTab = $this->uri->segment(3);
            } else {
              $whichTab = "days";
            }

            // // Get rows count
            $conditions1['searchKeyword'] = '';
            $conditions1['searchStatus'] = '';
            $conditions1['returnType']    = 'count';
            $conditions1['returnDays']    = 'days';
            $rowsCount1 = $this->Jobpostadmin_Model->getRowsUsers($conditions1);
            $config1['base_url']    = base_url().'administrator/userreport/days';
            $config1['total_rows']  = $rowsCount1;
            $offset1 = 0;
            $config1['per_page']    = 10;
            $config1['show_link'] = $offset1;
            // Get rows
            $conditions1['returnType'] = '';
            $conditions1['start'] = $offset1;
            $conditions1['limit'] = $config1['per_page'];
            $data_result1 = $this->Jobpostadmin_Model->getRowsUsers($conditions1);
            $data['result1'] = $data_result1;
            $data['resultTotal1'] = $rowsCount1;
            $data['pagination1'] = $config1;

            // Get rows count
            $conditions2['searchKeyword'] = '';
            $conditions2['searchStatus'] = '';
            $conditions2['returnType']    = 'count';
            $conditions2['returnDays']    = 'week';
            $rowsCount2 = $this->Jobpostadmin_Model->getRowsUsers($conditions2);
            $config2['base_url']    = base_url().'administrator/userreport/week';
            $config2['total_rows']  = $rowsCount2;
            $offset2 = 0;
            $config2['per_page'] = 10;
            $config2['show_link'] = $offset2;
            // Get rows
            $conditions2['returnType'] = '';
            $conditions2['start'] = $offset2;
            $conditions2['limit'] = $config2['per_page'];
            $data_result2 = $this->Jobpostadmin_Model->getRowsUsers($conditions2);
            $data['result2'] = $data_result2;
            $data['resultTotal2'] = $rowsCount2;
            $data['pagination2'] = $config2;


            // Get rows count
            $conditions3['searchKeyword'] = '';
            $conditions3['searchStatus'] = '';
            $conditions3['returnType']    = 'count';
            $conditions3['returnDays']    = 'month';
            $rowsCount3 = $this->Jobpostadmin_Model->getRowsUsers($conditions3);

            $config3['base_url']    = base_url().'administrator/userreport/month';
            $config3['total_rows']  = $rowsCount3;
            $offset3 = 0;
            $config3['per_page'] = 10;
            $config3['show_link'] = $offset3;
            // // Get rows
            $conditions3['returnType'] = '';
            $conditions3['start'] = $offset3;
            $conditions3['limit'] = $config3['per_page'];
            $data_result3 = $this->Jobpostadmin_Model->getRowsUsers($conditions3);
            $data['result3'] = $data_result3;
            $data['resultTotal3'] = $rowsCount3;
            $data['pagination3'] = $config3;


            // Get rows count
            $conditions4['searchKeyword'] = '';
            $conditions4['searchStatus'] = '';
            $conditions4['returnType']    = 'count';
            $conditions4['returnDays']    = 'year';
            $rowsCount4 = $this->Jobpostadmin_Model->getRowsUsers($conditions4);
            $config4['base_url']    = base_url().'administrator/userreport/year';
            $config4['total_rows']  = $rowsCount4;
            $offset4 = 0;
            $config4['per_page']    = 10;
            $config4['show_link'] = $offset4;
            // // Get rows
            $conditions4['returnType'] = '';
            $conditions4['start'] = $offset4;
            $conditions4['limit'] = $config4['per_page'];
            $data_result4 = $this->Jobpostadmin_Model->getRowsUsers($conditions4);
            $data['result4'] = $data_result4;
            $data['resultTotal4'] = $rowsCount4;
            $data['pagination4'] = $config4;


            // Get rows count
            $conditions5['searchKeyword'] = $data['searchKeyword'];
            $conditions5['searchStatus'] = $data['searchStatus'];
            $conditions5['searchFrom'] = $data['searchFrom'];
            $conditions5['searchTo'] = $data['searchTo'];

            $conditions5['returnType']    = 'count';
            $conditions5['returnDays']    = 'all';
            $rowsCount5 = $this->Jobpostadmin_Model->getRowsUsers($conditions5);
            $config5['base_url']    = base_url().'administrator/userreport/datewise';
            $config5['total_rows']  = $rowsCount5;
            if($whichTab == "year") {
              if($this->uri->segment(4)) {
                $offset5 = $this->uri->segment(4);
              } else {
                $offset5 = 0;
              }
            } else {
              $offset5 = 0;
            }
            $config5['per_page']    = 10;
            $config5['show_link'] = $offset5;
            // // Get rows
            $conditions5['returnType'] = '';
            $conditions5['start'] = $offset5;
            $conditions5['limit'] = $config5['per_page'];
            $data_result5 = $this->Jobpostadmin_Model->getRowsUsers($conditions5);
            $data['result5'] = $data_result5;
            $data['resultTotal5'] = $rowsCount5;
            $data['pagination5'] = $config5;

            $data['checkactive'] = "tab5";

        } else {

            if($this->uri->segment(3)) {
              $whichTab = $this->uri->segment(3);
            } else {
              $whichTab = "days";
            }

            // // Get rows count
            $conditions1['searchKeyword'] = $data['searchKeyword'];
            $conditions1['searchStatus'] = $data['searchStatus'];
            $conditions1['returnType']    = 'count';
            $conditions1['returnDays']    = 'days';
            $rowsCount1 = $this->Jobpostadmin_Model->getRowsUsers($conditions1);
            $config1['base_url']    = base_url().'administrator/userreport/days';
            $config1['total_rows']  = $rowsCount1;
            if($whichTab == "days") {
              
              if($this->uri->segment(4)) {
                $offset1 = $this->uri->segment(4);
              } else {
                $offset1 = 0;
              }
            } else {
              $offset1 = 0;
            }
            $config1['per_page']    = 10;
            $config1['show_link'] = $offset1;
            // Get rows
            $conditions1['returnType'] = '';
            $conditions1['start'] = (int)$offset1;
            $conditions1['limit'] = $config1['per_page'];
            //var_dump($conditions1);die;
            $data_result1 = $this->Jobpostadmin_Model->getRowsUsers($conditions1);
            $data['result1'] = $data_result1;
            $data['resultTotal1'] = $rowsCount1;
            $data['pagination1'] = $config1;


            // Get rows count
            $conditions2['searchKeyword'] = $data['searchKeyword'];
            $conditions2['searchStatus'] = $data['searchStatus'];
            $conditions2['returnType']    = 'count';
            $conditions2['returnDays']    = 'week';
            $rowsCount2 = $this->Jobpostadmin_Model->getRowsUsers($conditions2);
            $config2['base_url']    = base_url().'administrator/userreport/week';
            $config2['total_rows']  = $rowsCount2;
            if($whichTab == "week") {
              
              if($this->uri->segment(4)) {
                $offset2 = $this->uri->segment(4);
              } else {
                $offset2 = 0;
              }

            } else {
              $offset2 = 0;
            }
            $config2['per_page'] = 10;
            $config2['show_link'] = $offset2;
            // Get rows
            $conditions2['returnType'] = '';
            $conditions2['start'] = $offset2;
            $conditions2['limit'] = $config2['per_page'];
            $data_result2 = $this->Jobpostadmin_Model->getRowsUsers($conditions2);
            $data['result2'] = $data_result2;
            $data['resultTotal2'] = $rowsCount2;
            $data['pagination2'] = $config2;


            // Get rows count
            $conditions3['searchKeyword'] = $data['searchKeyword'];
            $conditions3['searchStatus'] = $data['searchStatus'];
            $conditions3['returnType']    = 'count';
            $conditions3['returnDays']    = 'month';
            $rowsCount3 = $this->Jobpostadmin_Model->getRowsUsers($conditions3);

            $config3['base_url']    = base_url().'administrator/userreport/month';
            $config3['total_rows']  = $rowsCount3;
            if($whichTab == "month") {
              if($this->uri->segment(4)) {
                $offset3 = $this->uri->segment(4);
              } else {
                $offset3 = 0;
              }
            } else {
              $offset3 = 0;
            }
            $config3['per_page'] = 10;
            $config3['show_link'] = $offset3;
            // // Get rows
            $conditions3['returnType'] = '';
            $conditions3['start'] = $offset3;
            $conditions3['limit'] = $config3['per_page'];
            $data_result3 = $this->Jobpostadmin_Model->getRowsUsers($conditions3);
            $data['result3'] = $data_result3;
            $data['resultTotal3'] = $rowsCount3;
            $data['pagination3'] = $config3;


            // Get rows count
            $conditions4['searchKeyword'] = $data['searchKeyword'];
            $conditions4['searchStatus'] = $data['searchStatus'];
            $conditions4['returnType']    = 'count';
            $conditions4['returnDays']    = 'year';
            $rowsCount4 = $this->Jobpostadmin_Model->getRowsUsers($conditions4);
            $config4['base_url']    = base_url().'administrator/userreport/year';
            $config4['total_rows']  = $rowsCount4;
            if($whichTab == "year") {
              if($this->uri->segment(4)) {
                $offset4 = $this->uri->segment(4);
              } else {
                $offset4 = 0;
              }
            } else {
              $offset4 = 0;
            }
            $config4['per_page']    = 10;
            $config4['show_link'] = $offset4;
            // // Get rows
            $conditions4['returnType'] = '';
            $conditions4['start'] = $offset4;
            $conditions4['limit'] = $config4['per_page'];
            $data_result4 = $this->Jobpostadmin_Model->getRowsUsers($conditions4);
            $data['result4'] = $data_result4;
            $data['resultTotal4'] = $rowsCount4;
            $data['pagination4'] = $config4;


            // Get rows count
            $conditions5['searchKeyword'] = $data['searchKeyword'];
            $conditions5['searchStatus'] = $data['searchStatus'];
            $conditions5['returnType']    = 'count';
            $conditions5['returnDays']    = 'all';
            $rowsCount5 = $this->Jobpostadmin_Model->getRowsUsers($conditions5);
            $config5['base_url']    = base_url().'administrator/userreport/datewise';
            $config5['total_rows']  = $rowsCount5;
            if($whichTab == "year") {
              if($this->uri->segment(4)) {
                $offset5 = $this->uri->segment(4);
              } else {
                $offset5 = 0;
              }
            } else {
              $offset5 = 0;
            }
            $config5['per_page']    = 10;
            $config5['show_link'] = $offset5;
            // // Get rows
            $conditions5['returnType'] = '';
            $conditions5['start'] = $offset5;
            $conditions5['limit'] = $config5['per_page'];
            $data_result5 = $this->Jobpostadmin_Model->getRowsUsers($conditions5);
            $data['result5'] = $data_result5;
            $data['resultTotal5'] = $rowsCount5;
            $data['pagination5'] = $config5;

            if($this->uri->segment(3)) {

              $whichTabing = $this->uri->segment(3);
              if($whichTabing == "days") {
                  $data['checkactive'] = "tab1"; 
              } else if($whichTabing == "week") {
                  $data['checkactive'] = "tab2";
              } else if($whichTabing == "month") {
                  $data['checkactive'] = "tab3";
              } else if($whichTabing == "year") {
                  $data['checkactive'] = "tab4";
              } else if($whichTabing == "datewise") {
                  $data['checkactive'] = "tab5";
              }
            } else {
              $data['checkactive'] = "tab1";  
            }
        }

        //var_dump($data);die;
        //---------------------------------------------------------------------------------------

        $this->load->view('administrator/user_reports',$data);
    }

    public function export_csv_report() {

        $filename = 'users_'.date('Ymd').'.csv'; 
        header("Content-Description: File Transfer"); 
        header("Content-Disposition: attachment; filename=$filename"); 
        header("Content-Type: application/csv; ");

        $tabType = $_GET['tabtype'];

        if($tabType == "daywise") {

            if($this->session->userdata('searchKeyword') || $this->session->userdata('searchStatus')) {

                $inputKeywords = $this->session->userdata('searchKeyword');
                $searchKeyword = strip_tags($inputKeywords);

                $inputStatus = $this->session->userdata('searchStatus');
                $searchStatus = strip_tags($inputStatus);

                $conditions['searchKeyword'] = $searchKeyword;
                $conditions['searchStatus'] = $searchStatus;
                $conditions['returnDays']    = 'days';
                $usersData = $this->Jobpostadmin_Model->getRowsUsers($conditions);

            } else {
                $conditions['searchKeyword'] = "";
                $conditions['searchStatus'] = "";
                $conditions['returnDays']    = 'days';
                $usersData = $this->Jobpostadmin_Model->getRowsUsers($conditions);
            }

        } elseif($tabType == "weekwise") {

            if($this->session->userdata('searchKeyword') || $this->session->userdata('searchStatus')) {

                $inputKeywords = $this->session->userdata('searchKeyword');
                $searchKeyword = strip_tags($inputKeywords);

                $inputStatus = $this->session->userdata('searchStatus');
                $searchStatus = strip_tags($inputStatus);

                $conditions['searchKeyword'] = $searchKeyword;
                $conditions['searchStatus'] = $searchStatus;
                $conditions['returnDays']    = 'week';
                $usersData = $this->Jobpostadmin_Model->getRowsUsers($conditions);

            } else {
                $conditions['searchKeyword'] = "";
                $conditions['searchStatus'] = "";
                $conditions['returnDays']    = 'week';
                $usersData = $this->Jobpostadmin_Model->getRowsUsers($conditions);
            }

        } elseif($tabType == "monthwise") {

            if($this->session->userdata('searchKeyword') || $this->session->userdata('searchStatus')) {

                $inputKeywords = $this->session->userdata('searchKeyword');
                $searchKeyword = strip_tags($inputKeywords);

                $inputStatus = $this->session->userdata('searchStatus');
                $searchStatus = strip_tags($inputStatus);

                $conditions['searchKeyword'] = $searchKeyword;
                $conditions['searchStatus'] = $searchStatus;
                $conditions['returnDays']    = 'month';
                $usersData = $this->Jobpostadmin_Model->getRowsUsers($conditions);

            } else {
                $conditions['searchKeyword'] = "";
                $conditions['searchStatus'] = "";
                $conditions['returnDays']    = 'month';
                $usersData = $this->Jobpostadmin_Model->getRowsUsers($conditions);
            }

        } elseif($tabType == "yearwise") {

            if($this->session->userdata('searchKeyword') || $this->session->userdata('searchStatus')) {

                $inputKeywords = $this->session->userdata('searchKeyword');
                $searchKeyword = strip_tags($inputKeywords);

                $inputStatus = $this->session->userdata('searchStatus');
                $searchStatus = strip_tags($inputStatus);

                $conditions['searchKeyword'] = $searchKeyword;
                $conditions['searchStatus'] = $searchStatus;
                $conditions['returnDays']    = 'year';
                $usersData = $this->Jobpostadmin_Model->getRowsUsers($conditions);

            } else {
                $conditions['searchKeyword'] = "";
                $conditions['searchStatus'] = "";
                $conditions['returnDays']    = 'year';
                $usersData = $this->Jobpostadmin_Model->getRowsUsers($conditions);
            }

        } else {

            if($this->session->userdata('searchKeyword') || $this->session->userdata('searchStatus') || $this->session->userdata('searchFrom') || $this->session->userdata('searchTo')) {

                $inputKeywords = $this->session->userdata('searchKeyword');
                $searchKeyword = strip_tags($inputKeywords);

                $inputStatus = $this->session->userdata('searchStatus');
                $searchStatus = strip_tags($inputStatus);

                $inputFrom = $this->session->userdata('searchFrom');
                $searchFrom = strip_tags($inputFrom);

                $inputTo = $this->session->userdata('searchTo');
                $searchTo = strip_tags($inputTo);

                $conditions['searchKeyword'] = $searchKeyword;
                $conditions['searchStatus'] = $searchStatus;
                $conditions['searchFrom'] = $searchFrom;
                $conditions['searchTo'] = $searchTo;
              
                //$conditions['returnDays']    = 'all';
                $usersData = $this->Jobpostadmin_Model->getRowsUsers($conditions);
            } else {
              // get data
                $conditions['searchKeyword'] = "";
                $conditions['searchStatus'] = "";
                $conditions['searchFrom'] = "";
                $conditions['searchTo'] = "";
                $conditions['returnDays']    = 'all';
                $usersData = $this->Jobpostadmin_Model->getRowsUsers($conditions);
            }
        }

        // file creation 
        $file = fopen('php://output','w');
        $header = array("SNo","Name","Email","Phone","Registration Date","Experience (Year)","Experience (Month)","Location","State","City","Intrested In","Benefits", "Salary", "Newsletter","Education","Job Level","Industry", "Superpower", "Specialization","Sub specialization","25 MBPS Internet","Work Mode","Vaccination","Relocate", "Platform","Job Search status","Last company","App Version","Last Used","Signup Via"); 
        fputcsv($file, $header);

        $x1 =1;
        $dataArray = array();

        //$dataArray = $this->modify_hiring_data($usersData);
          
        foreach($usersData as $jobList) {

            //if(date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-365 days'))  ) {

                if($jobList['phone']==0){
                  $jobList['phone']='';
                }else{
                  $jobList['phone']=$jobList['phone'];
                }
                
                if($jobList['exp_year']>1){
                  $exp_year = $jobList['exp_year'].' Years'; 
                }else{
                  $exp_year = $jobList['exp_year'].' Year';
                }
                
                if($jobList['exp_month']>1){
                  $exp_month = $jobList['exp_month'].' Months'; 
                }else{
                  $exp_month = $jobList['exp_month'].' Month';
                }
                if(!empty($jobList['platform'])){
                  $jobList['device_type']=$jobList['platform'];
                }else{
                  $jobList['device_type']='web';
                }

                if(!empty($jobList['state'])){
                  $jobList['state']=$jobList['state'];
                }else{
                  $jobList['state']=" ";
                }

                if(!empty($jobList['city'])){
                  $jobList['city']=$jobList['city'];
                }else{
                  $jobList['city']=" ";
                }

                if(!empty($jobList['jobsInterested'])){
                  $jobList['jobsInterested']=$jobList['jobsInterested'];
                }else{
                  $jobList['jobsInterested']=" ";
                }

                if(!empty($jobList['app_version'])){
                  $jobList['app_version']=$jobList['app_version'];
                }else{
                  $jobList['app_version']=" ";
                }

                if(!empty($jobList['last_used'])){
                  $jobList['last_used']=$jobList['last_used'];
                }else{
                  $jobList['last_used']=" ";
                }

                if($jobList['newsletters'] == 1) {
                  $jobList['newsletters'] = "Yes";
                } else {
                  $jobList['newsletters'] = "No";
                }

                if(!empty($jobList['platform'])) { 
                                                                   
                    if($jobList['platform']=='ios') {  
                         $jobList['platform'] = "iOS";
                    } elseif ($jobList['platform']=='android') {
                         $jobList['platform'] = "Android"; 
                    } elseif ($jobList['platform']=='web') {
                      $jobList['platform'] = "Web"; 
                    } 
                } else {

                    if(!empty($jobList['type']) && $jobList['type']=='apple') {
                      $jobList['platform'] = "iOS";
                    } else {
                      $jobList['platform'] = "Web";
                    }
                }

                if($jobList['jobsearch_status'] == 1) {
                   $jobList['jobsearch_status'] = "Actively seeking";
                } else if($jobList['jobsearch_status'] == 2) {
                   $jobList['jobsearch_status'] = "Open to offers";
                }  else if($jobList['jobsearch_status'] == 3) {
                   $jobList['jobsearch_status'] = "Exploring";
                }

                if(strlen($jobList['topbpo']) > 0) {
                    $lastCompany =  $jobList['topbpo'];
                } else {
                    $lastCompany = "";
                }
                //$lastCompany = "";

                $dataArray[] = ["srno"=>$x1, 
                               "name"=>$jobList['name'], 
                               "email"=>$jobList['email'],
                               "phone"=>$jobList['phone'],
                               "created_at"=>date('Y-m-d', strtotime($jobList['created_at'])),
                               "experience_year"=>$exp_year,
                               "experience_month"=>$exp_month,
                               "location"=>$jobList['location'],
                               "state"=>$jobList['state'],
                               "city"=>$jobList['city'],
                               "jobsInterested"=>$jobList['jobsInterested'],
                               "jobsbenefits"=>$jobList['jobsbenefits'],
                               "current_salary"=>$jobList['current_salary'],
                               "newsletters"=>$jobList['newsletters'],
                               "education"=>$jobList['education'],
                               "joblevel"=>$jobList['jobLevel'],
                               "industry"=>$jobList['industry'],
                               "superpower"=>$jobList['superpower'],
                               "specialization"=>$jobList['specialization'],
                               "sub_specialization"=>$jobList['sub_specialization'],
                               "internetspeed"=>$jobList['internetspeed'],
                               "work_mode"=>$jobList['work_mode'],
                               "vaccination"=>$jobList['vaccination'],
                               "relocate"=>$jobList['relocate'],
                               "platform"=>$jobList['platform'],
                               "jobsearch_status"=>$jobList['jobsearch_status'],
                               "lastCompany"=>$lastCompany,
                               "app_version"=>$jobList['app_version'],
                               "last_used"=>$jobList['last_used'],
                               "type"=>$jobList['type'],
                            ];
              $x1++;
            //}
        }
        
        foreach ($dataArray as $key=>$line) { 
          fputcsv($file,$line); 
        }
        fclose($file); 
        exit; 
    }

    public function non_user_reports() {
        //$data["jobLists"] = $this->Jobpostadmin_Model->notappliedjob_fetchAll();

        $this->session->unset_userdata('search');
        
        $this->load->view('administrator/non_user_reports');
    }

    public function invoice(){
        $company_id = base64_decode($this->input->get('id'));
        //echo $company_id;die;
        $data["invoices"] = $this->Jobpostadmin_Model->get_invoices_data($company_id);

        $this->load->view('administrator/invoice',$data);
    }

    public function getuserDatabyDay(){
        $status = $this->input->post('status');
        $date =  date('Y-m-d');
        //echo $date;die;
        $data = $this->Jobpostadmin_Model->getuserDatabyDay($status,$date);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">SNo</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Email</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Registration Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Experience</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Location</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">State</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">City</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Intrested In</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Education</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Nationality</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Superpower</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Platform</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">App Version</span>
             </div>
          </th>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Last Login</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Signup Via</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {

             if( date('Y-m-d', strtotime($jobList['created_at'])) == $date  ){
              if($jobList['phone']==0){
                $jobList['phone']='';
              }else{
                $jobList['phone']=$jobList['phone'];
              }
              
              if($jobList['exp_year']>1){
                $exp_year = $jobList['exp_year'].' Years'; 
              }else{
                $exp_year = $jobList['exp_year'].' Year';
              }

              
              if($jobList['exp_month']>1){
                $exp_month = $jobList['exp_month'].' Months'; 
              }else{
                $exp_month = $jobList['exp_month'].' Month';
              }

              if(!empty($jobList['device_type'])){
                if($jobList['device_type']=='android'){
                  $jobList['device_type']='Android';
                }
                else{
                  $jobList['device_type']='iOS';
                }
              }else{
                $jobList['device_type']='Web';
              }

              if($jobList['type']=='normal'){
                $jobList['type'] = 'Normal';
              }elseif($jobList['type']=='gmail'){
                $jobList['type'] = 'Gmail';
              }elseif($jobList['type']=='apple'){
                $jobList['type'] = 'Apple';
              }else{
                $jobList['type'] = 'Facebook';
              }

              $output.=' <tr>
              <td style="width:40px;">'. $x1.'</td>
              <td style="width:100px;">'. $jobList['name'].'</td>
              <td style="width:100px;">'. $jobList['email'].'</td>
              <td style="width:70px;">'.$jobList['phone'].'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.$exp_year.' '.$exp_month. '</td>
              <td style="width:70px;">'.$jobList['location'].'</td>
              <td style="width:70px;">'.$jobList['state'].'</td>
              <td style="width:70px;">'.$jobList['city'].'</td>
              <td style="width:70px;">'.$jobList['jobsInterested'].'</td>
              <td style="width:70px;">'.$jobList['education'].'</td>
              <td style="width:70px;">'.$jobList['nationality'].'</td>
              <td style="width:70px;">'.$jobList['superpower'].'</td>
              <td style="width:70px;">'.$jobList['device_type'].'</td>
              <td style="width:70px;">'.$jobList['app_version'].'</td>
              <td style="width:70px;">'.$jobList['last_used'].'</td>
              <td style="width:70px;">'.$jobList['type'].'</td>
           </tr>';
           $x1++;
             }
          
          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }

    public function getnonuserDatabyDay(){
        $status = $this->input->post('status');
        $date =  date('Y-m-d');
        //echo $date;die;
        $data = $this->Jobpostadmin_Model->getnonuserDatabyDay($status,$date);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">SNo</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Email</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Registration Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Platform</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Signup Via</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {

             if( date('Y-m-d', strtotime($jobList['created_at'])) == $date  ){
              if($jobList['phone']==0){
                $jobList['phone']='';
              }else{
                $jobList['phone']=$jobList['phone'];
              }
              
              if($jobList['exp_year']>1){
                $exp_year = $jobList['exp_year'].' Years'; 
              }else{
                $exp_year = $jobList['exp_year'].' Year';
              }

              
              if($jobList['exp_month']>1){
                $exp_month = $jobList['exp_month'].' Months'; 
              }else{
                $exp_month = $jobList['exp_month'].' Month';
              }

              if(!empty($jobList['device_type'])){
                if($jobList['device_type']=='android'){
                  $jobList['device_type']='Android';
                }
                else{
                  $jobList['device_type']='iOS';
                }
              }else{
                $jobList['device_type']='Web';
              }

              if($jobList['type']=='normal'){
                $jobList['type'] = 'Normal';
              }elseif($jobList['type']=='gmail'){
                $jobList['type'] = 'Gmail';
              }elseif($jobList['type']=='apple'){
                $jobList['type'] = 'Apple';
              }else{
                $jobList['type'] = 'Facebook';
              }

              $output.=' <tr>
              <td style="width:40px;">'. $x1.'</td>
              <td style="width:100px;">'. $jobList['name'].'</td>
              <td style="width:100px;">'. $jobList['email'].'</td>
              <td style="width:70px;">'.$jobList['phone'].'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.$jobList['device_type'].'</td>
              <td style="width:70px;">'.$jobList['type'].'</td>
           </tr>';
           $x1++;
             }
          
          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }

    public function getuserDatabyWeek(){
        $status = $this->input->post('status');
        $date =  date('Y-m-d');
        //echo $date;die;
        $data = $this->Jobpostadmin_Model->getuserDatabyDay($status,$date);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">SNo</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Email</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Registration Date</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Experience</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Location</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">State</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">City</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Intrested In</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Education</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Nationality</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Superpower</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Platform</span>
             </div>
          </th><th class="secondary-text">
 <div class="table-header">
    <span class="column-title">App Version</span>
 </div>
</th>

<th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Last Used</span>
             </div>
          </th>

<th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Signup Via</span>
             </div>
          </th>

       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {

             
             //echo date('Y-m-d', strtotime('-7 days'));die;
             if(date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-7 days'))  ){
              if($jobList['phone']==0){
                $jobList['phone']='';
              }else{
                $jobList['phone']=$jobList['phone'];
              }
              
              if($jobList['exp_year']>1){
                $exp_year = $jobList['exp_year'].' Years'; 
              }else{
                $exp_year = $jobList['exp_year'].' Year';
              }

              
              if($jobList['exp_month']>1){
                $exp_month = $jobList['exp_month'].' Months'; 
              }else{
                $exp_month = $jobList['exp_month'].' Month';
              }
              if(!empty($jobList['device_type'])){
                $jobList['device_type']=$jobList['device_type'];
              }else{
                $jobList['device_type']='web';
              }
                $output.=' <tr>
              <td style="width:40px;">'. $x1.'</td>
              <td style="width:100px;">'. $jobList['name'].'</td>
              <td style="width:100px;">'. $jobList['email'].'</td>
              <td style="width:70px;">'.$jobList['phone'].'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.$exp_year.' '.$exp_month. '</td>
              <td style="width:70px;">'.$jobList['location'].'</td>
              <td style="width:70px;">'.$jobList['state'].'</td>
              <td style="width:70px;">'.$jobList['city'].'</td>
              <td style="width:70px;">'.$jobList['jobsInterested'].'</td>
              <td style="width:70px;">'.$jobList['education'].'</td>
              <td style="width:70px;">'.$jobList['nationality'].'</td>
              <td style="width:70px;">'.$jobList['superpower'].'</td>
              <td style="width:70px;">'.$jobList['device_type'].'</td>
              <td style="width:70px;">'.$jobList['app_version'].'</td>
              <td style="width:70px;">'.$jobList['last_used'].'</td>
              <td style="width:70px;">'.$jobList['type'].'</td>
           </tr>';
           $x1++;
             }

          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }

    public function getnonuserDatabyWeek(){
        $status = $this->input->post('status');
        $date =  date('Y-m-d');
        //echo $date;die;
        $data = $this->Jobpostadmin_Model->getnonuserDatabyDay($status,$date);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">SNo</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Email</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Registration Date</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Platform</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Signup Via</span>
             </div>
          </th>
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {

             
             //echo date('Y-m-d', strtotime('-7 days'));die;
             if(date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-7 days'))  ){
              if($jobList['phone']==0){
                $jobList['phone']='';
              }else{
                $jobList['phone']=$jobList['phone'];
              }
              
              if($jobList['exp_year']>1){
                $exp_year = $jobList['exp_year'].' Years'; 
              }else{
                $exp_year = $jobList['exp_year'].' Year';
              }

              
              if($jobList['exp_month']>1){
                $exp_month = $jobList['exp_month'].' Months'; 
              }else{
                $exp_month = $jobList['exp_month'].' Month';
              }
              if(!empty($jobList['device_type'])){
                $jobList['device_type']=$jobList['device_type'];
              }else{
                $jobList['device_type']='web';
              }
                $output.=' <tr>
              <td style="width:40px;">'. $x1.'</td>
              <td style="width:100px;">'. $jobList['name'].'</td>
              <td style="width:100px;">'. $jobList['email'].'</td>
              <td style="width:70px;">'.$jobList['phone'].'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.$jobList['device_type'].'</td>
              <td style="width:70px;">'.$jobList['type'].'</td>
           </tr>';
           $x1++;
             }

          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }

    public function getuserDatabyMonth(){
        $status = $this->input->post('status');
        $date =  date('Y-m-d');
        //echo $date;die;
        $data = $this->Jobpostadmin_Model->getuserDatabyDay($status,$date);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">SNo</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Email</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Registration Date</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Experience</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Location</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">State</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">City</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Intrested In</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Education</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Nationality</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Superpower</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Platform</span>
             </div>
          </th>
          <th class="secondary-text">
 <div class="table-header">
    <span class="column-title">App Version</span>
 </div>
</th>

          <th class="secondary-text">
 <div class="table-header">
    <span class="column-title">Last Used</span>
 </div>
</th>

<th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Signup Via</span>
             </div>
          </th>
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {

             
             //echo date('Y-m-d', strtotime('-7 days'));die;
             if(date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-30 days'))  ){
              if($jobList['phone']==0){
                $jobList['phone']='';
              }else{
                $jobList['phone']=$jobList['phone'];
              }
              
              if($jobList['exp_year']>1){
                $exp_year = $jobList['exp_year'].' Years'; 
              }else{
                $exp_year = $jobList['exp_year'].' Year';
              }

              
              if($jobList['exp_month']>1){
                $exp_month = $jobList['exp_month'].' Months'; 
              }else{
                $exp_month = $jobList['exp_month'].' Month';
              }
              if(!empty($jobList['device_type'])){
                $jobList['device_type']=$jobList['device_type'];
              }else{
                $jobList['device_type']='web';
              }
                $output.=' <tr>
              <td style="width:40px;">'. $x1.'</td>
              <td style="width:100px;">'. $jobList['name'].'</td>
              <td style="width:100px;">'. $jobList['email'].'</td>
              <td style="width:70px;">'.$jobList['phone'].'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.$exp_year.' '.$exp_month. '</td>
              <td style="width:70px;">'.$jobList['location'].'</td>
              <td style="width:70px;">'.$jobList['state'].'</td>
              <td style="width:70px;">'.$jobList['city'].'</td>
              <td style="width:70px;">'.$jobList['jobsInterested'].'</td>
              <td style="width:70px;">'.$jobList['education'].'</td>
              <td style="width:70px;">'.$jobList['nationality'].'</td>
              <td style="width:70px;">'.$jobList['superpower'].'</td>
              <td style="width:70px;">'.$jobList['device_type'].'</td>
              <td style="width:70px;">'.$jobList['app_version'].'</td>
              <td style="width:70px;">'.$jobList['last_used'].'</td>
              <td style="width:70px;">'.$jobList['type'].'</td>
           </tr>';
           $x1++;
             }

          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }

    public function getnonuserDatabyMonth(){
        $status = $this->input->post('status');
        $date =  date('Y-m-d');
        //echo $date;die;
        $data = $this->Jobpostadmin_Model->getnonuserDatabyDay($status,$date);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">SNo</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Email</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Registration Date</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Platform</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Signup Via</span>
             </div>
          </th>
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {

             
             //echo date('Y-m-d', strtotime('-7 days'));die;
             if(date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-30 days'))  ){
              if($jobList['phone']==0){
                $jobList['phone']='';
              }else{
                $jobList['phone']=$jobList['phone'];
              }
              
              if($jobList['exp_year']>1){
                $exp_year = $jobList['exp_year'].' Years'; 
              }else{
                $exp_year = $jobList['exp_year'].' Year';
              }

              
              if($jobList['exp_month']>1){
                $exp_month = $jobList['exp_month'].' Months'; 
              }else{
                $exp_month = $jobList['exp_month'].' Month';
              }
              if(!empty($jobList['device_type'])){
                $jobList['device_type']=$jobList['device_type'];
              }else{
                $jobList['device_type']='web';
              }
                $output.=' <tr>
              <td style="width:40px;">'. $x1.'</td>
              <td style="width:100px;">'. $jobList['name'].'</td>
              <td style="width:100px;">'. $jobList['email'].'</td>
              <td style="width:70px;">'.$jobList['phone'].'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.$jobList['device_type'].'</td>
              <td style="width:70px;">'.$jobList['type'].'</td>
           </tr>';
           $x1++;
             }

          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }

    public function getuserDatabyYear(){
        $status = $this->input->post('status');
        $date =  date('Y-m-d');
        //echo $date;die;
        $data = $this->Jobpostadmin_Model->getuserDatabyDay($status,$date);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">SNo</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Email</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Registration Date</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Experience</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Location</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">State</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">City</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Intrested In</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Education</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Nationality</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Superpower</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Platform</span>
             </div>
          </th>
          <th class="secondary-text">
 <div class="table-header">
    <span class="column-title">App Version</span>
 </div>
</th>
          <th class="secondary-text">
 <div class="table-header">
    <span class="column-title">Last Used</span>
 </div>
</th>
<th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Signup Via</span>
             </div>
          </th>
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {

             
             //echo date('Y-m-d', strtotime('-7 days'));die;
              if(date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-365 days'))  ){
              if($jobList['phone']==0){
                $jobList['phone']='';
              }else{
                $jobList['phone']=$jobList['phone'];
              }
              
              if($jobList['exp_year']>1){
                $exp_year = $jobList['exp_year'].' Years'; 
              }else{
                $exp_year = $jobList['exp_year'].' Year';
              }
              
              if($jobList['exp_month']>1){
                $exp_month = $jobList['exp_month'].' Months'; 
              }else{
                $exp_month = $jobList['exp_month'].' Month';
              }
              if(!empty($jobList['device_type'])){
                $jobList['device_type']=$jobList['device_type'];
              }else{
                $jobList['device_type']='web';
              }
                $output.=' <tr>
              <td style="width:40px;">'. $x1.'</td>
              <td style="width:100px;">'. $jobList['name'].'</td>
              <td style="width:100px;">'. $jobList['email'].'</td>
              <td style="width:70px;">'.$jobList['phone'].'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.$exp_year.' '.$exp_month. '</td>
              <td style="width:70px;">'.$jobList['location'].'</td>
              <td style="width:70px;">'.$jobList['state'].'</td>
              <td style="width:70px;">'.$jobList['city'].'</td>
              <td style="width:70px;">'.$jobList['jobsInterested'].'</td>
              <td style="width:70px;">'.$jobList['education'].'</td>
              <td style="width:70px;">'.$jobList['nationality'].'</td>
              <td style="width:70px;">'.$jobList['superpower'].'</td>
              <td style="width:70px;">'.$jobList['device_type'].'</td>
              <td style="width:70px;">'.$jobList['app_version'].'</td>
              <td style="width:70px;">'.$jobList['last_used'].'</td>
              <td style="width:70px;">'.$jobList['type'].'</td>
           </tr>';
           $x1++;
             }

          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }


    public function getnonuserDatabyYear(){
        $status = $this->input->post('status');
        $date =  date('Y-m-d');
        //echo $date;die;
        $data = $this->Jobpostadmin_Model->getnonuserDatabyDay($status,$date);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">SNo</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Email</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Registration Date</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Platform</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Signup Via</span>
             </div>
          </th>
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {

             
             //echo date('Y-m-d', strtotime('-7 days'));die;
             if(date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-365 days'))  ){
              if($jobList['phone']==0){
                $jobList['phone']='';
              }else{
                $jobList['phone']=$jobList['phone'];
              }
              
              if($jobList['exp_year']>1){
                $exp_year = $jobList['exp_year'].' Years'; 
              }else{
                $exp_year = $jobList['exp_year'].' Year';
              }

              
              if($jobList['exp_month']>1){
                $exp_month = $jobList['exp_month'].' Months'; 
              }else{
                $exp_month = $jobList['exp_month'].' Month';
              }
              if(!empty($jobList['device_type'])){
                $jobList['device_type']=$jobList['device_type'];
              }else{
                $jobList['device_type']='web';
              }
                $output.=' <tr>
              <td style="width:40px;">'. $x1.'</td>
              <td style="width:100px;">'. $jobList['name'].'</td>
              <td style="width:100px;">'. $jobList['email'].'</td>
              <td style="width:70px;">'.$jobList['phone'].'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.$jobList['device_type'].'</td>
              <td style="width:70px;">'.$jobList['type'].'</td>
           </tr>';
           $x1++;
             }

          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }

    public function getDatabyStatus(){
        $status = $this->input->post('status');
        $date =  date('Y-m-d');
        //echo $date;die;
        $data = $this->Jobpostadmin_Model->getDatabyStatus($status,$date);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Mode</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Chatbot</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Sub-Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate ID</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Email</span>
             </div>
          </th>
      
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Super Power</span>
             </div>
          </th>
      
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Applied Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Salary Offer</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Guaranteed Allowance</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Latest Status</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Last Update</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Fall Out Reason</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Hired Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Last Managed By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Chatbot</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {
            $category = $this->Jobpost_Model->categorybyid($jobList['category']);
            
            $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
            if($subcategory) {
                $subcategoryy = $subcategory[0]['subcategory'];
            } else {
                $subcategoryy = '';
            }

            $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
            $expmonth = $jobList['exp_month'];
            $expyear = $jobList['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
            $salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);
            if($jobList['status']=='1'){
              $jobList['status'] = 'New Application';
            }else if($jobList['status']=='2'){
              $jobList['status']='No Show';
            }else if($jobList['status']=='3'){
              $jobList['status']='Fall Out';
            }else if($jobList['status']=='4'){
              $jobList['status']='Refer';
            }else if($jobList['status']=='5'){
              $jobList['status']='On Going Application';
            }else if($jobList['status']=='6'){
              $jobList['status']='Accepted JO';
            }else if($jobList['status']=='7'){
              $jobList['status']='Hired';
            }else if($jobList['status']=='8'){
              $jobList['status']='reschedule';
            } else {
              $jobList['status']='';
            }
            if($jobList['status']=='Hired'){
              $hiredDate= date('Y-m-d',strtotime($jobList['updated_at']));
            }else{
              $hiredDate = '';
            }
            if($jobList['chatbot'] == 0) {
              $chatboot = "No";
            } else {
              $chatboot = "Yes";
            }
             if( date('Y-m-d', strtotime($jobList['created_at'])) == $date  ){

                if($jobList['chatbot'] == 1) {
                      $chatResult = base_url()."administrator/questionnaire_user_view/".base64_encode($jobList['id']).'/'.base64_encode($jobList['uid']);
                      $chatResult = "<a href='".$chatResult."'>view</a>";
                  } else {
                      $chatResult = "----";
                  }

                $output.=' <tr>
              <td style="width:40px;">'. $jobList['cname'].'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$jobList['mode'].'</td>
              <td style="width:70px;">'.$chatboot.'</td>
              <td style="width:70px;">'.$category[0]['category'].'</td>
              <td style="width:70px;">'.$subcategoryy.'</td>
            <td style="width:70px;">'.$jobList['uid'].'</td>
            <td style="width:70px;">'.$jobList['name'].'</td>
            <td style="width:70px;">'.$jobList['email'].'</td>
            <td style="width:70px;">'.$jobList['phone'].'</td>
            <td style="width:70px;">'.$jobList['superpower'].'</td>
            <td style="width:70px;">'.$jobList['interviewdate'].'</td>
            <td style="width:70px;">'.$jobList['created_at'].'</td>
            <td style="width:70px;">'.$salary[0]['basicsalary'].'</td>
            <td style="width:70px;">'.$jobList['allowance'].'</td>
              
              <td style="width:70px;">'.$jobList['status'].'</td>
            <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['updated_at'])).'</td>
            <td style="width:70px;">'.$jobList['fallout_reason'].'</td>
            <td style="width:70px;">'.$hiredDate.'</td>
            <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
            <td style="width:70px;">'.$chatResult.'</td>
           </tr>';
           $x1++;
             }
          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }


    public function getuserDatabyDate(){
        $status = $this->input->post('status');
        $from_date = $this->input->post('from_date');
        $to_date = $this->input->post('to_date');
        $date =  date('Y-m-d');
        //echo $date;die;
        $data = $this->Jobpostadmin_Model->getuserDatabyDay($status,$date);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">SNo</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Email</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Registration Date</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Experience</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Location</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">State</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">City</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Intrested In</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Education</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Nationality</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Superpower</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Platform</span>
             </div>
          </th>
          <th class="secondary-text">
 <div class="table-header">
    <span class="column-title">App Version</span>
 </div>
</th>
          <th class="secondary-text">
 <div class="table-header">
    <span class="column-title">Last Used</span>
 </div>
</th>
<th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Signup Via</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {

             
             //echo date('Y-m-d', strtotime('-7 days'));die;
             if(date('Y-m-d', strtotime($jobList['created_at'])) >= $from_date && date('Y-m-d', strtotime($jobList['created_at'])) <= $to_date) {
              
              if($jobList['phone']==0){
                $jobList['phone']='';
              }else{
                $jobList['phone']=$jobList['phone'];
              }
              
              if($jobList['exp_year']>1){
                $exp_year = $jobList['exp_year'].' Years'; 
              }else{
                $exp_year = $jobList['exp_year'].' Year';
              }

              
              if($jobList['exp_month']>1){
                $exp_month = $jobList['exp_month'].' Months'; 
              }else{
                $exp_month = $jobList['exp_month'].' Month';
              }
              if(!empty($jobList['device_type'])){
                $jobList['device_type']=$jobList['device_type'];
              }else{
                $jobList['device_type']='web';
              }
              
                $output.=' <tr>
              <td style="width:40px;">'. $x1.'</td>
              <td style="width:100px;">'. $jobList['name'].'</td>
              <td style="width:100px;">'. $jobList['email'].'</td>
              <td style="width:70px;">'.$jobList['phone'].'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.$exp_year.' '.$exp_month. '</td>
              <td style="width:70px;">'.$jobList['location'].'</td>
              <td style="width:70px;">'.$jobList['state'].'</td>
              <td style="width:70px;">'.$jobList['city'].'</td>
              <td style="width:70px;">'.$jobList['jobsInterested'].'</td>
              <td style="width:70px;">'.$jobList['education'].'</td>
              <td style="width:70px;">'.$jobList['nationality'].'</td>
              <td style="width:70px;">'.$jobList['superpower'].'</td>
              <td style="width:70px;">'.$jobList['device_type'].'</td>
              <td style="width:70px;">'.$jobList['app_version'].'</td>
              <td style="width:70px;">'.$jobList['last_used'].'</td>
              <td style="width:70px;">'.$jobList['type'].'</td>
              
           </tr>';
           $x1++;
             }

          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }

    public function getnonuserDatabyDate(){
        $status = $this->input->post('status');
        $from_date = $this->input->post('from_date');
        $to_date = $this->input->post('to_date');
        $date =  date('Y-m-d');
        //echo $date;die;
        $data = $this->Jobpostadmin_Model->getnonuserDatabyDay($status,$date);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">SNo</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Email</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Phone</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Registration Date</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Platform</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Signup Via</span>
             </div>
          </th>
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {

             
             //echo date('Y-m-d', strtotime('-7 days'));die;
             if(date('Y-m-d', strtotime($jobList['created_at'])) >= $from_date && date('Y-m-d', strtotime($jobList['created_at'])) <= $to_date){
              if($jobList['phone']==0){
                $jobList['phone']='';
              }else{
                $jobList['phone']=$jobList['phone'];
              }
                $output.=' <tr>
              <td style="width:40px;">'. $x1.'</td>
              <td style="width:100px;">'. $jobList['name'].'</td>
              <td style="width:100px;">'. $jobList['email'].'</td>
              <td style="width:70px;">'.$jobList['phone'].'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.$jobList['device_type'].'</td>
              <td style="width:70px;">'.$jobList['type'].'</td>
           </tr>';
           $x1++;
             }

          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }


    public function getDatabyStatusweek(){
        $status = $this->input->post('status');
        $date =  date('Y-m-d');
        //echo $status;die;
        $data = $this->Jobpostadmin_Model->getDatabyStatus($status,$date);
        //echo "<pre>";
        //print_r($data);die;
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Mode</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Chatbot</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Sub-Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate ID</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Email</span>
             </div>
          </th>
      
      <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Phone</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Super Power</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Applied Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Salary Offer</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Guaranteed Allowance</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Latest Status</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Last Update</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Fall Out Reason</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Hired Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Last Managed By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Chatbot</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {
            $category = $this->Jobpost_Model->categorybyid($jobList['category']);
            $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
            if($subcategory) {
                $getsubcategory =  $subcategory[0]['subcategory'];
            } else {
                $getsubcategory = "";
            }
              
              $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
        $expmonth = $jobList['exp_month'];
        $expyear = $jobList['exp_year'];

        if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
        $salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);
        if($jobList['status']=='1'){
          $jobList['status'] = 'New Application';
        }else if($jobList['status']=='2'){
          $jobList['status']='No Show';
        }else if($jobList['status']=='3'){
          $jobList['status']='Fall Out';
        }else if($jobList['status']=='4'){
          $jobList['status']='Refer';
        }else if($jobList['status']=='5'){
          $jobList['status']='On Going Application';
        }else if($jobList['status']=='6'){
          $jobList['status']='Accepted JO';
        }else if($jobList['status']=='7'){
          $jobList['status']='Hired';
        }else if($jobList['status']=='8'){
          $jobList['status']='reschedule';
        } else {
          $jobList['status']='';
        }
        if($jobList['status']=='Hired'){
          $hiredDate= date('Y-m-d',strtotime($jobList['updated_at']));
        }else{
          $hiredDate = '';
        }
          if($jobList['chatbot'] == 0) {
            $chatboot = "No";
          } else {
            $chatboot = "Yes";
          }
             //echo date('Y-m-d', strtotime('-7 days'));die;
             if(date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-7 days'))  ){

                if($jobList['chatbot'] == 1) {
                      $chatResult = base_url()."administrator/questionnaire_user_view/".base64_encode($jobList['id']).'/'.base64_encode($jobList['uid']);
                      $chatResult = "<a href='".$chatResult."'>view</a>";
                  } else {
                      $chatResult = "----";
                  }

                $output.=' <tr>
              <td style="width:40px;">'. $jobList['cname'].'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$jobList['mode'].'</td>
              <td style="width:70px;">'.$chatboot.'</td>
              <td style="width:70px;">'.$category[0]['category'].'</td>
              <td style="width:70px;">'.$getsubcategory.'</td>
              <td style="width:70px;">'.$jobList['uid'].'</td>
              <td style="width:70px;">'.$jobList['name'].'</td>
              <td style="width:70px;">'.$jobList['email'].'</td>
        <td style="width:70px;">'.$jobList['phone'].'</td>
        <td style="width:70px;">'.$jobList['superpower'].'</td>
              <td style="width:70px;">'.$jobList['interviewdate'].'</td>
              <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.$salary[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$jobList['allowance'].'</td>
              
              <td style="width:70px;">'.$jobList['status'].'</td>
              <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['updated_at'])).'</td>
              <td style="width:70px;">'.$jobList['fallout_reason'].'</td>
              <td style="width:70px;">'.$hiredDate.'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              <td style="width:70px;">'.$chatResult.'</td>
              
           </tr>';
           $x1++;
             }

          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }

    public function getDatabyStatusmonth(){
        $status = $this->input->post('status');
        $date =  date('Y-m-d');
        //echo $date;die;
        $data = $this->Jobpostadmin_Model->getDatabyStatus($status,$date);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Mode</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">chatbot</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Sub-Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate ID</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Email</span>
             </div>
          </th>
      
      <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Phone</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Super Power</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Date</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Applied Date</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Salary Offer</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Guaranteed Allowance</span>
             </div>
          </th>

          

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Latest Status</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Last Update</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Fall Out Reason</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Hired Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Last Managed By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Chatbot</span>
             </div>
          </th>

       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {
            $category = $this->Jobpost_Model->categorybyid($jobList['category']);
            $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
            if($subcategory) {
                $getsubcategory = $subcategory[0]['subcategory'];
            } else {
                $getsubcategory = "";
            }

              $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
        $expmonth = $jobList['exp_month'];
        $expyear = $jobList['exp_year'];

        if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
        $salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);
        if($jobList['status']=='1'){
          $jobList['status'] = 'New Application';
        }else if($jobList['status']=='2'){
          $jobList['status']='No Show';
        }else if($jobList['status']=='3'){
          $jobList['status']='Fall Out';
        }else if($jobList['status']=='4'){
          $jobList['status']='Refer';
        }else if($jobList['status']=='5'){
          $jobList['status']='On Going Application';
        }else if($jobList['status']=='6'){
          $jobList['status']='Accepted JO';
        }else if($jobList['status']=='7'){
          $jobList['status']='Hired';
        }else if($jobList['status']=='8'){
          $jobList['status']='reschedule';
        } else {
          $jobList['status']='';
        }

        if($jobList['status']=='Hired'){
          $hiredDate= date('Y-m-d',strtotime($jobList['updated_at']));
        }else{
          $hiredDate = '';
        }
          if($jobList['chatbot'] == 0) {
            $chatboot = "No";
          } else {
            $chatboot = "Yes";
          }
             if( date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-30 days'))  ){

                if($jobList['chatbot'] == 1) {
                      $chatResult = base_url()."administrator/questionnaire_user_view/".base64_encode($jobList['id']).'/'.base64_encode($jobList['uid']);
                      $chatResult = "<a href='".$chatResult."'>view</a>";
                  } else {
                      $chatResult = "----";
                  }

                $output.=' <tr>
              <td style="width:40px;">'. $jobList['cname'].'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$jobList['mode'].'</td>
              <td style="width:70px;">'.$chatboot.'</td>
              <td style="width:70px;">'.$category[0]['category'].'</td>
              <td style="width:70px;">'.$getsubcategory.'</td>
        <td style="width:70px;">'.$jobList['uid'].'</td>
        <td style="width:70px;">'.$jobList['name'].'</td>
        <td style="width:70px;">'.$jobList['email'].'</td>
    <td style="width:70px;">'.$jobList['phone'].'</td>
    <td style="width:70px;">'.$jobList['superpower'].'</td>
        <td style="width:70px;">'.$jobList['interviewdate'].'</td>
        <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['created_at'])).'</td>
        <td style="width:70px;">'.$salary[0]['basicsalary'].'</td>
        <td style="width:70px;">'.$jobList['allowance'].'</td>
              
              <td style="width:70px;">'.$jobList['status'].'</td>
        <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['updated_at'])).'</td>
        <td style="width:70px;">'.$jobList['fallout_reason'].'</td>
        <td style="width:70px;">'.$hiredDate.'</td>
        <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
        <td style="width:70px;">'.$chatResult.'</td>
              
           </tr>';
           $x1++;
             }
       
          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }

    public function getDatabyStatusyear(){
        $status = $this->input->post('status');
        $date =  date('Y-m-d');
        //echo $date;die;
        $data = $this->Jobpostadmin_Model->getDatabyStatus($status,$date);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Mode</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Chatbot</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Sub-Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate ID</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Email</span>
             </div>
          </th>
     
      <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Phone</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Super Power</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Applied Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Salary Offer</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Guaranteed Allowance</span>
             </div>
          </th>


          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Latest Status</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Last Update</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Fall Out Reason</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Hired Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Last Managed By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Chatbot</span>
             </div>
          </th>

       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {
              $category = $this->Jobpost_Model->categorybyid($jobList['category']);

              $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);

              if($subcategory) {
                  $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
                  
                  $expmonth = $jobList['exp_month'];
                  $expyear = $jobList['exp_year'];

                  if($expyear == 0 && $expmonth == 0) {
                      $expfilter = "1";
                  }else if($expyear == 0 && $expmonth < 6) {
                      $expfilter = "2";
                  } else if($expyear < 1 && $expmonth >= 6) {
                      $expfilter = "3";
                  } else if($expyear < 2 && $expyear >= 1) {
                      $expfilter = "4";
                  } else if($expyear < 3 && $expyear >= 2) {
                      $expfilter = "5";
                  }else if($expyear < 4 && $expyear >= 3) {
                      $expfilter = "6";
                  }else if($expyear < 5 && $expyear >= 4) {
                      $expfilter = "7";
                  }else if($expyear < 6 && $expyear >= 5) {
                      $expfilter = "8";
                  }else if($expyear < 7 && $expyear >= 6) {
                      $expfilter = "9";    
                  } else if($expyear >= 7) { 
                      $expfilter = "10";
                  }

                  $salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);
                  if($jobList['status']=='1'){
                    $jobList['status'] = 'New Application';
                  }else if($jobList['status']=='2'){
                    $jobList['status']='No Show';
                  }else if($jobList['status']=='3'){
                    $jobList['status']='Fall Out';
                  }else if($jobList['status']=='4'){
                    $jobList['status']='Refer';
                  }else if($jobList['status']=='5'){
                    $jobList['status']='On Going Application';
                  }else if($jobList['status']=='6'){
                    $jobList['status']='Accepted JO';
                  }else if($jobList['status']=='7'){
                    $jobList['status']='Hired';
                  }else if($jobList['status']=='8'){
                    $jobList['status']='reschedule';
                  } else {
                    $jobList['status']='';
                  }
                  
                  if($jobList['status']=='Hired'){
                    $hiredDate= date('Y-m-d',strtotime($jobList['updated_at']));
                  }else{
                    $hiredDate = '';
                  } 
                 if($jobList['chatbot'] == 0) {
                    $chatboot = "No";
                  } else {
                    $chatboot = "Yes";
                  }
                 if( date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-365 days'))  ){

                    if($jobList['chatbot'] == 1) {
                        $chatResult = base_url()."administrator/questionnaire_user_view/".base64_encode($jobList['id']).'/'.base64_encode($jobList['uid']);
                        $chatResult = "<a href='".$chatResult."'>view</a>";
                    } else {
                        $chatResult = "----";
                    }

                    $output.=' <tr>
                    <td style="width:40px;">'. $jobList['cname'].'</td>
                    <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
                    <td style="width:100px;">'. $jobList['id'].'</td>
                    <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
                    <td style="width:70px;">'.$jobList['jobtitle'].'</td>
                    <td style="width:70px;">'.$jobList['mode'].'</td>
                    <td style="width:70px;">'.$chatboot.'</td>
                    <td style="width:70px;">'.$category[0]['category'].'</td>
                    <td style="width:70px;">'.$subcategory[0]['subcategory'].'</td>
                    <td style="width:70px;">'.$jobList['uid'].'</td>
                    <td style="width:70px;">'.$jobList['name'].'</td>
                    <td style="width:70px;">'.$jobList['email'].'</td>
                    <td style="width:70px;">'.$jobList['phone'].'</td>
                    <td style="width:70px;">'.$jobList['superpower'].'</td>
                    <td style="width:70px;">'.$jobList['interviewdate'].'</td>
                    <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['created_at'])).'</td>
                    <td style="width:70px;">'.$salary[0]['basicsalary'].'</td>
                    <td style="width:70px;">'.$jobList['allowance'].'</td>
                         
                          <td style="width:70px;">'.$jobList['status'].'</td>
                    <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['updated_at'])).'</td>
                    <td style="width:70px;">'.$jobList['fallout_reason'].'</td>
                    <td style="width:70px;">'.$hiredDate.'</td>
                    <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
                    <td style="width:70px;">'.$chatResult.'</td>
                    
                    </tr>';
                    $x1++;
                 }
              }
          }
          
          $output.= '</tbody>';

        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }

    public function getDatabyStatusDate(){
        $status = $this->input->post('status');
        $from_date = $this->input->post('from_date');
        $to_date = $this->input->post('to_date');
        $date =  date('Y-m-d');
        //echo $date;die;
        $data = $this->Jobpostadmin_Model->getDatabyStatus($status,$date);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Mode</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Chatbot</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Sub-Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate ID</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Email</span>
             </div>
          </th>
      
      <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Phone</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Super Power</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Applied Date</span>
             </div>
          </th>
          
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Salary Offer</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Guaranteed Allowance</span>
             </div>
          </th>

          

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Latest Status</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Last Update</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Fall Out Reason</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Hired Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Last Managed By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Chatbot</span>
             </div>
          </th>

       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {
            $category = $this->Jobpost_Model->categorybyid($jobList['category']);
            $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
            if($subcategory) {
                $getsubcategory = $subcategory[0]['subcategory'];
            } else {
                $getsubcategory = "";
            }

              $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
        $expmonth = $jobList['exp_month'];
        $expyear = $jobList['exp_year'];

        if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
        $salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);
        if($jobList['status']=='1'){
          $jobList['status'] = 'New Application';
        }else if($jobList['status']=='2'){
          $jobList['status']='No Show';
        }else if($jobList['status']=='3'){
          $jobList['status']='Fall Out';
        }else if($jobList['status']=='4'){
          $jobList['status']='Refer';
        }else if($jobList['status']=='5'){
          $jobList['status']='On Going Application';
        }else if($jobList['status']=='6'){
          $jobList['status']='Accepted JO';
        }else if($jobList['status']=='7'){
          $jobList['status']='Hired';
        }else if($jobList['status']=='8'){
          $jobList['status']='reschedule';
        } else {
          $jobList['status']='';
        }
        if($jobList['status']=='Hired'){
          $hiredDate= date('Y-m-d',strtotime($jobList['updated_at']));
        }else{
          $hiredDate = '';
        }
          if($jobList['chatbot'] == 0) {
            $chatboot = "No";
          } else {
            $chatboot = "Yes";
          }
             if( date('Y-m-d', strtotime($jobList['created_at'])) >= $from_date &&  date('Y-m-d', strtotime($jobList['created_at'])) <= $to_date ){

                if($jobList['chatbot'] == 1) {
                      $chatResult = base_url()."administrator/questionnaire_user_view/".base64_encode($jobList['id']).'/'.base64_encode($jobList['uid']);
                      $chatResult = "<a href='".$chatResult."'>view</a>";
                  } else {
                      $chatResult = "----";
                  }

                $output.=' <tr>
              <td style="width:40px;">'. $jobList['cname'].'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$jobList['mode'].'</td>
              <td style="width:70px;">'.$chatboot.'</td>
              <td style="width:70px;">'.$category[0]['category'].'</td>
              <td style="width:70px;">'.$getsubcategory.'</td>
        <td style="width:70px;">'.$jobList['uid'].'</td>
        <td style="width:70px;">'.$jobList['name'].'</td>
        <td style="width:70px;">'.$jobList['email'].'</td>
    <td style="width:70px;">'.$jobList['phone'].'</td>
    <td style="width:70px;">'.$jobList['superpower'].'</td>
        <td style="width:70px;">'.$jobList['interviewdate'].'</td>
        <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['created_at'])).'</td>
        <td style="width:70px;">'.$salary[0]['basicsalary'].'</td>
        <td style="width:70px;">'.$jobList['allowance'].'</td>
              
              <td style="width:70px;">'.$jobList['status'].'</td>
        <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['updated_at'])).'</td>
        <td style="width:70px;">'.$jobList['fallout_reason'].'</td>
        <td style="width:70px;">'.$hiredDate.'</td>
        <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
        <td style="width:70px;">'.$chatResult.'</td>
              
           </tr>';
           $x1++;
             }

          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }

    public function invoicebyDate(){
        $from_date = $this->input->post('from_date');
        $to_date = $this->input->post('to_date');
        $data = $this->Recruiteradmin_Model->invoice_lists();
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">SNo</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Company</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Invoice</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Date</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {

             
             //echo date('Y-m-d', strtotime('-7 days'));die;
             if(date('Y-m-d', strtotime($jobList['date'])) >= $from_date && date('Y-m-d', strtotime($jobList['date'])) <= $to_date){
                $invoice_path = base_url().$jobList['invoice'];
                $invoice_date = date("Y-m-d",strtotime($jobList['date']));
                $output.=' <tr>
              <td style="width:40px;">'. $x1.'</td>
              <td style="width:100px;">'. $jobList['cname'].'</td>
              <td style="width:100px;"><a href="'.$invoice_path.'">Download</a></td>
              <td style="width:70px;">'.$invoice_date.'</td>
              
              
           </tr>';
           $x1++;
             }

          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }


    public function boostjobs() {
        $data["jobLists"] = $this->Jobpostadmin_Model->boostjob_fetch();
        //echo $this->db->last_query();die;
        $data["boost_amount"] = $this->Jobpostadmin_Model->boost_amount();
        $this->load->view('administrator/boostjobs', $data);
    }

    public function activelyjobs() {
        $data["jobLists"] = $this->Jobpostadmin_Model->activelyjob_fetch();
        //echo $this->db->last_query();die;
        $data["actively_amount"] = $this->Jobpostadmin_Model->actively_amount();
        $this->load->view('administrator/activelyjobs', $data);
    }

    public function generatepdf() {

        $jid = $this->input->get('job_id');
        $amount = $this->input->get('amount');
        $hiredLists = $this->Jobpostadmin_Model->boostjob_fetchbyjobid($jid); 
        //print_r($hiredLists);die;
        $invoice_number = "JobYoDa".rand(10000,99999);
        $output = '<div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body p-0">
                                        
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td style="width:300px;">
                                                        <img src="https://jobyoda.com/recruiterfiles/images/jobyoda.png" width="200px;">
                                                    </td>
                                                    <td style="width:400px;">
                                                        <div style="float:right;">
                                                            <p class="font-weight-bold mb-1">Invoice #'.$invoice_number.'</p>
                                                            <p class="text-muted">Invoice Generated Date: '.date("d M, Y").'</p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <hr class="my-5">

                                        <div class="row pb-5 p-5">
                                            <div class="col-md-6" style="width:300px; margin-bottom:30px; margin-top:30px;">
                                                <p class="font-weight-bold mb-4" style="font-weight: bold !important; margin:0;"> Company Information</p>
                                                <p class="mb-1" style="margin:0;">'.$hiredLists[0]['cname'].'</p>
                                                <p class="mb-1" style="margin:0;">'.$hiredLists[0]['address'].'</p>
                                            </div>
                                        </div>

                                        <div class="row p-5">
                                            <div class="col-md-12" style="width:800px;">
                                                <table class="table" style="border:1px solid #ddd;">
                                                    <thead>
                                                        <tr>
                                                            <th class="border-0 text-uppercase small font-weight-bold">S No</th>
                                                            <th class="border-0 text-uppercase small font-weight-bold">Job Title</th>
                                                            
                                                            <th class="border-0 text-uppercase small font-weight-bold">Company Name</th>
                                                            
                                                            
                                                            <th class="border-0 text-uppercase small font-weight-bold">Date</th>
                                                            <th class="border-0 text-uppercase small font-weight-bold">Amount</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>';
                                                    $x2=1;
                                                    $total_amount=0;
                                                    foreach ($hiredLists as $hiredList) {
                                                        $data = ["payment_status" => 1];
                                                        $this->Recruiteradmin_Model->job_payment_update($data,$hiredList['rid'],$hiredList['id']);
                                                        $hiredDate = date("d-m-Y", strtotime($hiredList['created_at']));
                                                        $output.= '<tr>
                                                            <td style="width:50px;">'.$x2.'</td>
                                                            <td style="width:140px;">'.$hiredList['jobtitle'].'</td>
                                                            <td style="width:140px;">'.$hiredList['cname'].'</td>
                                                            
                                                            <td style="width:140px;">'.$hiredDate.'</td>
                                                            <td style="width:140px;">'.$amount.'</td>
                                                            
                                                        </tr>';
                                                        $x2++;
                                                        $total_amount+= $amount;
                                                    }
                                                        
                                                   $output.= '</tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <div class="d-flex flex-row-reverse bg-dark text-white p-4">
                                            <div class="py-3 px-5 text-right" style="margin-top:30px; background:#000; padding:5px;">
                                                <div class="mb-2" style="text-align:right; font-size:30px;color:#fff;">Grand Total</div>
                                                <div class="h2 font-weight-light" style="text-align:right;color:#fff; margin:0;">'.$total_amount.'</div>
                                            </div>

                                            <div class="py-3 px-5 text-right">
                                                
                                            </div>

                                            <div class="py-3 px-5 text-right">
                                                
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                   </div>';

        $file_name = "./recruiterupload/".md5(rand()) . '.pdf';
        $this->pdf->loadHtml($output);
        $this->pdf->render();
       
        $file = $this->pdf->output();
        file_put_contents($file_name,  $file );
       
        $this->load->library('email');
        $this->email->initialize([
            'protocol' => 'smtp',
            'smtp_host' => 'smtpout.asia.secureserver.net',
            'smtp_user' => 'noreply@jobyoda.com',
            'smtp_pass' => 'Jobyoda@2k21',
            'smtp_port' => 465,
            'smtp_crypto' => 'ssl',
            'charset'=>'utf-8',
            'mailtype' => 'html',
            'crlf' => "\r\n",
            'newline' => "\r\n"
        ]);
        $this->email->from("help@jobyoda.com", "JobYoDa");
        $this->email->to($hiredLists[0]['email']);
        $this->email->subject('JobYoda - Invoice');
        $msg = "Dear ".$hiredLists[0]['cname'] ;
        $msg .= ", Please check the invoice as below: ";
        $this->email->message($msg);
        $this->email->attach($file_name);
        
        if($this->email->send()) {

            $this->pdf->stream($file_name, array("Attachment"=>0));
            $data = ["invoice_id" => $invoice_number, "total_amount" => $total_amount, "company_id" => $hiredLists[0]['rid'] , "invoice" => $file_name , "jobpost_id" => $jid, 'type'=>2];
            $this->Recruiteradmin_Model->invoice_insert($data);
        }else{
            echo $hiredLists[0]['email'];
        }
      
    }


    public function activelygeneratepdf() {

        $jid = $this->input->get('job_id');
        $amount = $this->input->get('amount');
        $hiredLists = $this->Jobpostadmin_Model->activelyjob_fetchbyjobid($jid); 
        //print_r($hiredLists);die;
        $invoice_number = "JobYoDa".rand(10000,99999);
        $output = '<div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body p-0">
                                        
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td style="width:300px;">
                                                        <img src="https://jobyoda.com/recruiterfiles/images/jobyoda.png" width="200px;">
                                                    </td>
                                                    <td style="width:400px;">
                                                        <div style="float:right;">
                                                            <p class="font-weight-bold mb-1">Invoice #'.$invoice_number.'</p>
                                                            <p class="text-muted">Invoice Generated Date: '.date("d M, Y").'</p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <hr class="my-5">

                                        <div class="row pb-5 p-5">
                                            <div class="col-md-6" style="width:300px; margin-bottom:30px; margin-top:30px;">
                                                <p class="font-weight-bold mb-4" style="font-weight: bold !important; margin:0;"> Company Information</p>
                                                <p class="mb-1" style="margin:0;">'.$hiredLists[0]['cname'].'</p>
                                                <p class="mb-1" style="margin:0;">'.$hiredLists[0]['address'].'</p>
                                            </div>
                                        </div>

                                        <div class="row p-5">
                                            <div class="col-md-12" style="width:800px;">
                                                <table class="table" style="border:1px solid #ddd;">
                                                    <thead>
                                                        <tr>
                                                            <th class="border-0 text-uppercase small font-weight-bold">S No</th>
                                                            <th class="border-0 text-uppercase small font-weight-bold">Job Title</th>
                                                            <th class="border-0 text-uppercase small font-weight-bold">Company Name</th>
                                                            <th class="border-0 text-uppercase small font-weight-bold">Date</th>
                                                            <th class="border-0 text-uppercase small font-weight-bold">Amount</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>';
                                                    $x2=1;
                                                    $total_amount=0;
                                                    foreach ($hiredLists as $hiredList) {
                                                        $data = ["actively_payment_status" => 1];
                                                        $this->Recruiteradmin_Model->job_payment_update($data,$hiredList['rid'],$hiredList['id']);
                                                        $hiredDate = date("d-m-Y", strtotime($hiredList['created_at']));
                                                        $output.= '<tr>
                                                            <td style="width:50px;">'.$x2.'</td>
                                                            <td style="width:140px;">'.$hiredList['jobtitle'].'</td>
                                                            <td style="width:140px;">'.$hiredList['cname'].'</td>
                                                            
                                                            <td style="width:140px;">'.$hiredDate.'</td>
                                                            <td style="width:140px;">'.$amount.'</td>
                                                            
                                                        </tr>';
                                                        $x2++;
                                                        $total_amount+= $amount;
                                                    }
                                                        
                                                   $output.= '</tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <div class="d-flex flex-row-reverse bg-dark text-white p-4">
                                            <div class="py-3 px-5 text-right" style="margin-top:30px; background:#000; padding:5px;">
                                                <div class="mb-2" style="text-align:right; font-size:30px;color:#fff;">Grand Total</div>
                                                <div class="h2 font-weight-light" style="text-align:right;color:#fff; margin:0;">'.$total_amount.'</div>
                                            </div>

                                            <div class="py-3 px-5 text-right">
                                                
                                            </div>

                                            <div class="py-3 px-5 text-right">
                                                
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                   </div>';

        $file_name = "./recruiterupload/".md5(rand()) . '.pdf';
        $this->pdf->loadHtml($output);
        $this->pdf->render();
       
        $file = $this->pdf->output();
        file_put_contents($file_name,  $file );
       
        $this->load->library('email');
        $this->email->initialize([
            'protocol' => 'smtp',
            'smtp_host' => 'smtpout.asia.secureserver.net',
            'smtp_user' => 'noreply@jobyoda.com',
            'smtp_pass' => 'Jobyoda@2k21',
            'smtp_port' => 465,
            'smtp_crypto' => 'ssl',
            'charset'=>'utf-8',
            'mailtype' => 'html',
            'crlf' => "\r\n",
            'newline' => "\r\n"
        ]);
        $this->email->from("help@jobyoda.com", "JobYoDa");
        //$this->email->to($hiredLists[0]['email']);
        $this->email->to("ankit.mittal@mobulous.com");
        $this->email->subject('JobYoda - Actively Hiring Invoice');
        $msg = "Dear ".$hiredLists[0]['cname'] ;
        $msg .= ", Please check the invoice as below: ";
        $this->email->message($msg);
        $this->email->attach($file_name);
        
        if($this->email->send()) {

            $this->pdf->stream($file_name, array("Attachment"=>0));
            $data = ["invoice_id" => $invoice_number, "total_amount" => $total_amount, "company_id" => $hiredLists[0]['rid'] , "invoice" => $file_name , "jobpost_id" => $jid, 'type'=>6];
            $this->Recruiteradmin_Model->invoice_insert($data);
        } else {
            
            echo $this->email->print_debugger();
            die;
            //echo $hiredLists[0]['email'];

        }
      
    }
    
    public function jobpostviewpage() {
        $jobId = base64_decode($_GET['id']);
        $role = base64_decode($_GET['role']); 
        $data['getJobs'] = $this->Jobpostadmin_Model->jobupdate_detail_fetch($jobId);
        $data['getJobloc'] = $this->Jobpostadmin_Model->jobupdate_location_fetch($role);
        $data['jobExps'] = $this->Jobpostadmin_Model->jobupdate_detailexp_fetch($jobId);
        //echo $this->db->last_query();die;
        $data['recruiterExps'] = $this->Jobpostadmin_Model->jobupdate_recruiterexp_fetch($role);
        $data['industryLists'] = $this->Jobpostadmin_Model->industry_lists($data['getJobs'][0]['industry']);
        $data['levels'] = $this->Jobpostadmin_Model->level_lists($data['getJobs'][0]['level']);
        $data['langs'] = $this->Jobpostadmin_Model->language_lists($data['getJobs'][0]['language']);
        $jobskills = $this->Jobpost_Model->job_skills($jobId);
        
        if(empty($jobskills)){
                $jobskills[]= ['skill'=>$data['getJobs'][0]['skills']];
            }
        $data['jobskills'] = $jobskills;
        $this->load->view('administrator/jobpostview', $data);
    }
    
    public function candidatelistpage() {
        $jobId = base64_decode($_GET['id']);
        $data["jobLists"] = $this->Jobpostadmin_Model->job_fetchsingle($jobId);
        //echo $this->db->last_query();die;
        $this->load->view('administrator/candidatestatuslists', $data);
    }
    
    public function jobpostdelete() {
        $jid = $_GET['id'];
        $this->Jobpostadmin_Model->delete_jobpost($jid);
        $this->Jobpostadmin_Model->delete_jobpostlocation($jid);
        redirect("administrator/jobpost/index");
    }

    public function updateJobpost() {
        $jobId = base64_decode($_GET['id']);
        $role = base64_decode($_GET['role']);
        $data['getJobs'] = $this->Jobpostadmin_Model->jobupdate_detail_fetch($jobId);
        $data['addresses'] = $this->Common_Model->companysite_lists();
        $data['levels'] = $this->Jobpost_Model->level_lists();
        $data['category'] = $this->Jobpost_Model->category_lists1();
        $data['subcategory'] = $this->Jobpost_Model->subcategory_lists();
        $skills = $this->Jobpostadmin_Model->jobs_skills_single($jobId);
        
        if($skills) { 
            $x1=0;
            foreach ($skills as $skill) {
              $skills1[$x1] = $skill['skill_id'];
              $x1++;
            }
            $data['skills2'] = $skills1;
        } else{
            $data['skills2'] = [];
        }
        $data['skills'] = $this->Jobpostadmin_Model->skill_lists();
        
        $data['getJobloc'] = $this->Jobpostadmin_Model->jobupdate_location_fetch($data['getJobs'][0]['recruiter_id']);
        $data['jobExps'] = $this->Jobpostadmin_Model->jobupdate_detailexp_fetch($jobId);
        //$data['getJobexps'] = $this->Jobpost_Model->jobupdate_detailexp_fetch($jobId);
        $data['recruiterExps'] = $this->Jobpostadmin_Model->jobupdate_recruiterexp_fetch($role);
        $data['industryLists'] = $this->Common_Model->industry_lists();
        $data['channels'] = $this->Common_Model->channel_lists();
        $data['langs'] = $this->Common_Model->language_lists();
        $this->load->view('administrator/updatejob', $data);
    }
    
    public function jobPostUpdate() {
        $jobData = $this->input->post();
        $this->form_validation->set_rules('jobTitle', 'Job Title', 'trim|required');
        $this->form_validation->set_rules('opening', 'Opening', 'trim|required');
        $this->form_validation->set_rules('experience', 'Experience', 'trim|required');
        $this->form_validation->set_rules('level', 'Level', 'trim|required');
        $this->form_validation->set_rules('modecheck', 'Interview Mode', 'trim|required');
        $this->form_validation->set_rules('jobLocation', 'Job Location', 'trim|required');
        $this->form_validation->set_rules('jobDesc', 'Job Description', 'trim|required');
        $this->form_validation->set_rules('category', 'Category', 'trim|required');
        $this->form_validation->set_rules('education', 'Education', 'trim|required');
        $this->form_validation->set_rules('jobExpire', 'Job Expire', 'trim|required');
//        $this->form_validation->set_rules('compDetail', 'Company Detail', 'trim|required');
        $rid = $jobData['rid'];
        //echo $rid;
        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            //print_r($data['errors']);die;
            $data['getJobs'] = $this->Jobpostadmin_Model->jobupdate_detail_fetch($rid);
            $data['getJobloc'] = $this->Jobpostadmin_Model->jobupdate_location_fetch($rid);
            $data['jobExps'] = $this->Jobpostadmin_Model->jobupdate_detailexp_fetch($rid);
            //$data['recruiterExps'] = $this->Jobpostadmin_Model->jobupdate_recruiterexp_fetch($role);
            $data['industryLists'] = $this->Common_Model->industry_lists();
            $data['channels'] = $this->Common_Model->channel_lists();
            $data['langs'] = $this->Common_Model->language_lists();
            
            $this->load->view('administrator/updatejob',$data);
        } else {

            if($jobData['level_status'] == "on") {
                $jobData['level_status'] = 1;
            } else {
                $jobData['level_status'] = 0;
            }
            if($jobData['education_status'] == "on") {
                $jobData['education_status'] = 1;
            } else {
                $jobData['education_status'] = 0;
            }
            if($jobData['experience_status'] == "on") {
                $jobData['experience_status'] = 1;
            } else {
                $jobData['experience_status'] = 0;
            }

            $data1 = ["jobtitle"=>$jobData['jobTitle'],"company_id"=>$jobData['jobLocation'], "opening"=> $jobData['opening'], "experience"=> $jobData['experience'], "category" => $jobData['category'],
                        "subcategory" => $jobData['subcategory'],
                        "language" => $jobData['lang'],
                        "other_language" => $jobData['otherlanguage'],
                        "jobDesc" => $jobData['jobDesc'],
                        "jobPitch" => $jobData['jobPitch'],
                        "skills" => $jobData['skill'],
                        "education" => $jobData['education'],
                        
                        "level_status" => $jobData['level_status'],
                        "education_status" => $jobData['education_status'],
                        "experience_status" => $jobData['experience_status'],

                        "jobexpire" => $jobData['jobExpire'],
                        "certification" => $jobData['certification'],
                        "mode" => $jobData['modecheck'],
                        "modeurl" => $jobData['modeurl']??'',
                        ];
            $jobInserted = $this->Jobpostadmin_Model->job_update($data1, $jobData['rid']);

            /*$addLatLong = $this->getLatLong($jobData['jobLocation']);
            $dataLocation = [
                        "jobLocation" => $jobData['jobLocation'],
                        "latitude" => $addLatLong['latitude'],
                        "longitude" => $addLatLong['longitude'],
                    ];*/
            //$this->Jobpostadmin_Model->jobLocation_update($dataLocation, $jobData['rid']);

            if(isset($jobData['expRange'])) {
                $jobExpRanges[] = $jobData['expRange'];
                $jobExpBasicSalarys[] = $jobData['expBasicSalary'];
                $count = count($jobData['expRange']);
                if($count > 0) {
                    for($i=0;$i<$count;$i++) {
                        $data2[$i] = ["grade_id" => $jobData['expRange'][$i],"jobpost_id"=>$jobData["rid"], "basicsalary" => $jobData['expBasicSalary'][$i]];
                    }
                    $this->Jobpostadmin_Model->job_basicsalary_delete($jobData['rid']);
                    $expsalInserted = $this->Jobpostadmin_Model->basic_salary_insert($data2);
                    //$expsalInserted = $this->Jobpostadmin_Model->basic_salary_update($data1, $jobData['rid']);
                }
            }

            if($jobInserted) {
                 $this->session->set_tempdata('inserted','Job Post Successfully Updated',5);
                 $dataId = $jobData['rid'];
                 $role_id= $jobData['role'];
                 redirect("administrator/jobpost/");
            } else {
                $this->session->set_flashdata('postError','Job Post not able to Updated',5);
                $dataId = $jobData['rid'];
                redirect("administrator/jobpost/updateJobpost?id=base64_encode($dataId)&role=base64_encode($role_id)");
            }
        }
    }

    public function manageJobView() {
        $userSession = $this->session->userdata('userSession');
        $getJobs = $this->Jobpost_Model->job_fetch($userSession['id']);
        foreach($getJobs as $getJob) {
            $getappliedCount = $this->Jobpost_Model->jobappliedCount_fetch($getJob['id']);
            $data['jobFetch'][] = [
                                    "id"=>$getJob['id'],
                                    "jobtitle"=>$getJob['jobtitle'],
                                    "jobLocation"=>$getJob['jobLocation'],
                                    "jobDesc"=>$getJob['jobDesc'],
                                    "companydetail"=>$getJob['companydetail'],
                                    "qualification"=>$getJob['qualification'],
                                    "skills"=>$getJob['skills'],
                                    "salary"=>$getJob['salary'],
                                    "totalApply"=>$getappliedCount[0]['appliedCount'],
                                    ];
        }

        $data['jobApplicationFetch'] = $this->Jobpost_Model->jobApplication_fetch($userSession['id']);
        $data['activejobFetch'] = $this->Jobpost_Model->activeJob_fetch($userSession['id']);
        $this->load->view('recruiter/manage_jobs', $data);
    }

    public function jobpostView() {
        $jobId = $_GET['type'];
        $data['getJobs'] = $this->Jobpost_Model->jobupdate_detail_fetch($jobId);
        $data['getJobexps'] = $this->Jobpost_Model->jobupdate_detailexp_fetch($data['getJobs'][0]['company_id']);
        $data['industryLists'] = $this->Common_Model->industry_lists();
        $data['channels'] = $this->Common_Model->channel_lists();
        $data['langs'] = $this->Common_Model->language_lists();
        $data["getExps"] = $this->Recruit_Model->recruiter_exp_fetch($data['getJobs'][0]['company_id']);
        $this->load->view('recruiter/updatejob', $data);
    }

    public function getLatLong($address){
        if(!empty($address)){
            //Formatted address
            $formattedAddr = str_replace(' ','+',$address);
            //Send request and receive json data by address
            $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddr.'&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk'); 
            $output = json_decode($geocodeFromAddr);
            //Get latitude and longitute from json data
            $data['latitude']  = $output->results[0]->geometry->location->lat; 
            $data['longitude'] = $output->results[0]->geometry->location->lng;
            //Return latitude and longitude of the given address
            if(!empty($data)) {
                return $data;
            }else{
                return false;
            }
        }else{
            return false;   
        }
    }

    function distance($lat1, $lon1, $lat2, $lon2, $unit) {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
          return ($miles * 1.609344);
        } else if ($unit == "N") {
          return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }



        public function getScreenDatabyStatus(){
        $status = $this->input->post('status');
        $date =  date('Y-m-d');

        $data = $this->Jobpostadmin_Model->getScreenDatabyStatus($status,$date);

        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Sub-Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate ID</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Email</span>
             </div>
          </th>
      
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Salary Offer</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Guaranteed Allowance</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Screening Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title"> Status </span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {
            $category = $this->Jobpost_Model->categorybyid($jobList['category']);
            
            $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
            if($subcategory) {
                $subcategoryy = $subcategory[0]['subcategory'];
            } else {
                $subcategoryy = '';
            }

            $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
            $expmonth = $jobList['exp_month'];
            $expyear = $jobList['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }

            if($jobList['status'] == 1) {
                $screenStatus = "Pending";
            } else {
                $screenStatus = "Completed";
            }

        $salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);
        
             if( date('Y-m-d', strtotime($jobList['created_at'])) == $date  ){
                $output.=' <tr>
              <td style="width:40px;">'. $jobList['cname'].'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$category[0]['category'].'</td>
              <td style="width:70px;">'.$subcategoryy.'</td>
              <td style="width:70px;">'.$jobList['uid'].'</td>
              <td style="width:70px;">'.$jobList['name'].'</td>
              <td style="width:70px;">'.$jobList['email'].'</td>
              <td style="width:70px;">'.$jobList['phone'].'</td>
              <td style="width:70px;">'.$salary[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$jobList['allowance'].'</td>      
              <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['updated_at'])).'</td>
              <td style="width:70px;"> '. $screenStatus .' </td>
           </tr>';
           $x1++;
             }
          }
        $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }



    public function getScreenDatabyStatusweek(){
        $status = $this->input->post('status');
        $date =  date('Y-m-d');
        $data = $this->Jobpostadmin_Model->getScreenDatabyStatus($status,$date);

        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Sub-Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate ID</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Email</span>
             </div>
          </th>
      
      <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Phone</span>
             </div>
          </th>
          
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Salary Offer</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Guaranteed Allowance</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Screening Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Status</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {
            $category = $this->Jobpost_Model->categorybyid($jobList['category']);
            $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
            if($subcategory) {
                $getsubcategory =  $subcategory[0]['subcategory'];
            } else {
                $getsubcategory = "";
            }
              
              $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
        $expmonth = $jobList['exp_month'];
        $expyear = $jobList['exp_year'];

        if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }

            if($jobList['status'] == 1) {
                  $screenStatus = "Pending";
              } else {
                  $screenStatus = "Completed";
              }

        $salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);
             
            
             if(date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-7 days'))  ){
                $output.=' <tr>
              <td style="width:40px;">'. $jobList['cname'].'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$category[0]['category'].'</td>
              <td style="width:70px;">'.$getsubcategory.'</td>
              <td style="width:70px;">'.$jobList['uid'].'</td>
              <td style="width:70px;">'.$jobList['name'].'</td>
              <td style="width:70px;">'.$jobList['email'].'</td>
              <td style="width:70px;">'.$jobList['phone'].'</td>
              <td style="width:70px;">'.$salary[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$jobList['allowance'].'</td>
              <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['updated_at'])).'</td>
              <td style="width:70px;"> '. $screenStatus .' </td>
              
              
           </tr>';
           $x1++;
             }

          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }


        public function getScreenDatabyStatusmonth(){
        $status = $this->input->post('status');
        $date =  date('Y-m-d');
        //echo $date;die;
        $data = $this->Jobpostadmin_Model->getScreenDatabyStatus($status,$date);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Sub-Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate ID</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Email</span>
             </div>
          </th>
      
      <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Salary Offer</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Guaranteed Allowance</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Screening Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Status</span>
             </div>
          </th>
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {
            $category = $this->Jobpost_Model->categorybyid($jobList['category']);
            $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
            if($subcategory) {
                $getsubcategory = $subcategory[0]['subcategory'];
            } else {
                $getsubcategory = "";
            }

              $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
        $expmonth = $jobList['exp_month'];
        $expyear = $jobList['exp_year'];

        if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }

            if($jobList['status'] == 1) {
                  $screenStatus = "Pending";
              } else {
                  $screenStatus = "Completed";
              }

        $salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);

             if( date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-30 days'))  ){
                $output.=' <tr>
              <td style="width:40px;">'. $jobList['cname'].'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$category[0]['category'].'</td>
              <td style="width:70px;">'.$getsubcategory.'</td>
            <td style="width:70px;">'.$jobList['uid'].'</td>
            <td style="width:70px;">'.$jobList['name'].'</td>
            <td style="width:70px;">'.$jobList['email'].'</td>
              <td style="width:70px;">'.$jobList['phone'].'</td>
            <td style="width:70px;">'.$salary[0]['basicsalary'].'</td>
        <td style="width:70px;">'.$jobList['allowance'].'</td>
              
        <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['updated_at'])).'</td>
        <td style="width:70px;"> '. $screenStatus .' </td>
              
           </tr>';
           $x1++;
             }
       
          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }

      public function getScreenDatabyStatusyear() {
        $status = $this->input->post('status');
        $date =  date('Y-m-d');

        $data = $this->Jobpostadmin_Model->getScreenDatabyStatus($status,$date);

        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Sub-Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate ID</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Email</span>
             </div>
          </th>
     
      <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Salary Offer</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Guaranteed Allowance</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Screening Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title"> Status </span>
             </div>
          </th>
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {
              $category = $this->Jobpost_Model->categorybyid($jobList['category']);

              $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);

              if($subcategory) {
                  $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
                  
                  $expmonth = $jobList['exp_month'];
                  $expyear = $jobList['exp_year'];

                  if($expyear == 0 && $expmonth == 0) {
                      $expfilter = "1";
                  }else if($expyear == 0 && $expmonth < 6) {
                      $expfilter = "2";
                  } else if($expyear < 1 && $expmonth >= 6) {
                      $expfilter = "3";
                  } else if($expyear < 2 && $expyear >= 1) {
                      $expfilter = "4";
                  } else if($expyear < 3 && $expyear >= 2) {
                      $expfilter = "5";
                  }else if($expyear < 4 && $expyear >= 3) {
                      $expfilter = "6";
                  }else if($expyear < 5 && $expyear >= 4) {
                      $expfilter = "7";
                  }else if($expyear < 6 && $expyear >= 5) {
                      $expfilter = "8";
                  }else if($expyear < 7 && $expyear >= 6) {
                      $expfilter = "9";    
                  } else if($expyear >= 7) { 
                      $expfilter = "10";
                  }

                  if($jobList['status'] == 1) {
                  $screenStatus = "Pending";
              } else {
                  $screenStatus = "Completed";
              }

                  $salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);
                 
                 if( date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-365 days'))  ){
                    $output.=' <tr>
                    <td style="width:40px;">'. $jobList['cname'].'</td>
                    <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
                    <td style="width:100px;">'. $jobList['id'].'</td>
                    <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
                    <td style="width:70px;">'.$jobList['jobtitle'].'</td>
                    <td style="width:70px;">'.$category[0]['category'].'</td>
                    <td style="width:70px;">'.$subcategory[0]['subcategory'].'</td>
                    <td style="width:70px;">'.$jobList['uid'].'</td>
                    <td style="width:70px;">'.$jobList['name'].'</td>
                    <td style="width:70px;">'.$jobList['email'].'</td>
                    <td style="width:70px;">'.$jobList['phone'].'</td>
                    <td style="width:70px;">'.$salary[0]['basicsalary'].'</td>
                    <td style="width:70px;">'.$jobList['allowance'].'</td>
                    <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['updated_at'])).'</td>
                    <td style="width:70px;"> '. $screenStatus .' </td>
                    
                    </tr>';
                    $x1++;
                 }
              }
          }
          
          $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }   
    echo $output;
    }


    public function getScreenDatabyStatusDate(){
        $status = $this->input->post('status');
        $from_date = $this->input->post('from_date');
        $to_date = $this->input->post('to_date');
        $date =  date('Y-m-d');
        
        $data = $this->Jobpostadmin_Model->getScreenDatabyStatus($status,$date);
        
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Sub-Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate ID</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Email</span>
             </div>
          </th>
      
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Phone</span>
             </div>
          </th>
          
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Salary Offer</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Guaranteed Allowance</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Screening Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title"> Status </span>
             </div>
          </th>
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {
            $category = $this->Jobpost_Model->categorybyid($jobList['category']);
            $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
            if($subcategory) {
                $getsubcategory = $subcategory[0]['subcategory'];
            } else {
                $getsubcategory = "";
            }

              $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
        $expmonth = $jobList['exp_month'];
        $expyear = $jobList['exp_year'];

        if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }

            if($jobList['status'] == 1) {
                  $screenStatus = "Pending";
              } else {
                  $screenStatus = "Completed";
              }

        $salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);
             
             if( date('Y-m-d', strtotime($jobList['created_at'])) >= $from_date &&  date('Y-m-d', strtotime($jobList['created_at'])) <= $to_date ){
                $output.=' <tr>
              <td style="width:40px;">'. $jobList['cname'].'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$category[0]['category'].'</td>
              <td style="width:70px;">'.$getsubcategory.'</td>
        <td style="width:70px;">'.$jobList['uid'].'</td>
        <td style="width:70px;">'.$jobList['name'].'</td>
        <td style="width:70px;">'.$jobList['email'].'</td>
    <td style="width:70px;">'.$jobList['phone'].'</td>
        <td style="width:70px;">'.$salary[0]['basicsalary'].'</td>
        <td style="width:70px;">'.$jobList['allowance'].'</td>
        <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['updated_at'])).'</td>
        <td style="width:70px;"> '. $screenStatus .' </td>
              
           </tr>';
           $x1++;
             }
          } 
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        } 
    echo $output;
    }
}
?>