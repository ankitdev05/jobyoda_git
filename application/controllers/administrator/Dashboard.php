<?php
ob_start();
ini_set('memory_limit', '-1');
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */
class Dashboard extends CI_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('newmodels/recruiter/Recruit_Model');
        $this->load->model('newmodels/recruiter/Jobpost_Model');
        $this->load->model('newmodels/Admin_Model');
        $this->load->model('newmodels/Subscription_Model');
        $this->load->model('newmodels/Jobseekeradmin_Model');
        $this->load->model('newmodels/Jobpostadmin_Model');
        $this->load->model('newmodels/Recruiteradmin_Model');
        $this->load->model('newmodels/Common_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');

        $this->session->unset_userdata('search');

        if($this->session->userdata('adminSession')) {
            
        } else {
            redirect("administrator/admin/index");
        }

        if($this->session->userdata('searchhh')) {
            $this->session->unset_userdata('searchhh');
        }
    }
    
    public function index() {
        $data["adminLists"] = $this->Recruiteradmin_Model->admin_listing();
        $data["boostamount"] = $this->Recruiteradmin_Model->boost_amount();
        $data["activelyamount"] = $this->Recruiteradmin_Model->actively_amount();
        $data["appLists"] = $this->Recruiteradmin_Model->recruiter_lists_approved();
        $data["pendingLists"] = $this->Recruiteradmin_Model->pending_recruiters();
        $data["jobLists"] = $this->Jobpostadmin_Model->job_fetchAll();
        $data["appliedJobs"] = $this->Jobpostadmin_Model->appliedJobs();
        $data["openings"] = $this->Jobpost_Model->job_openings();
        $data["candidates"] = $this->Recruiteradmin_Model->candidatesLists();
        $data['compListing'] = $this->Recruiteradmin_Model->company_lists();
        $data['storyListing'] = $this->Recruiteradmin_Model->all_stories();
        $data["invoiceLists"] = $this->Recruiteradmin_Model->invoice_lists(); 
        $data["contactLists"] = $this->Jobseekeradmin_Model->contact_lists1();
        $data["adLists"] = $this->Jobseekeradmin_Model->advertise_lists();

        $data["transferRequest"] = $this->Recruiteradmin_Model->recruiter_transferfetch();
        $data["testimonialRequest"] = $this->Recruit_Model->testimonial_all();
        $data["jobRadius"] = $this->Recruiteradmin_Model->getradius();
        $subscriptionRequest = $this->Subscription_Model->get_subscribe_request_unseen();
        $data["subscriptionRequest"] = count($subscriptionRequest);
        $this->load->view('administrator/index', $data);
    }

    public function data_metrics() {

        if(isset($_GET["type"])) {
            $showType = $_GET["type"];
        } else {
            $showType = "all";
        }

        $totalRegistration = array();
        $totalBPO = array();
        $totalNonBPO = array();
        $totalApp = array();
        $totalCompany = array();
        $totalCompanyArray = array();

        $totalYear = array();
        $currentYear = date('Y');
        $startingYear = 2019;
        for($x=$startingYear; $x<=$currentYear; $x++) {

            $totalYear[] = $x;

            for($y=1;$y<=12;$y++) {

                if($y>=1 && $y<=9) {
                    $monthFormat = '0'.$y;
                } else {
                    $monthFormat = $y;
                }
                $getMonth = $x.'-'.$monthFormat;

                $getUserCount = $this->Jobseekeradmin_Model->get_count_metricrecord($getMonth);
                

                $totalRegistration[$x][] = ["year"=>$x, "month"=>$monthFormat, "by"=>$getMonth, "record"=>$getUserCount];
            }
        }
        // BPO
        for($x=$startingYear; $x<=$currentYear; $x++) {

            for($y=1;$y<=12;$y++) {

                if($y>=1 && $y<=9) {
                    $monthFormat = '0'.$y;
                } else {
                    $monthFormat = $y;
                }
                $getMonth = $x.'-'.$monthFormat;

                $getUserCount = $this->Jobseekeradmin_Model->get_count_metricrecordBPO($getMonth);
                

                $totalBPO[$x][] = ["year"=>$x, "month"=>$monthFormat, "by"=>$getMonth, "record"=>$getUserCount];
            }
        }
        // Non BPO
        for($x=$startingYear; $x<=$currentYear; $x++) {

            for($y=1;$y<=12;$y++) {

                if($y>=1 && $y<=9) {
                    $monthFormat = '0'.$y;
                } else {
                    $monthFormat = $y;
                }
                $getMonth = $x.'-'.$monthFormat;

                $getUserCount = $this->Jobseekeradmin_Model->get_count_metricrecordNonBPO($getMonth);
                

                $totalNonBPO[$x][] = ["year"=>$x, "month"=>$monthFormat, "by"=>$getMonth, "record"=>$getUserCount];
            }
        }

        // Application
        // for($x=$startingYear; $x<=$currentYear; $x++) {

        //     for($y=1;$y<=12;$y++) {

        //         if($y>=1 && $y<=9) {
        //             $monthFormat = '0'.$y;
        //         } else {
        //             $monthFormat = $y;
        //         }
        //         $getMonth = $x.'-'.$monthFormat;

        //         $getUserCount = $this->Jobseekeradmin_Model->get_count_metricrecordApplication($getMonth);
                
        //         $totalApp[$x][] = ["year"=>$x, "month"=>$monthFormat, "by"=>$getMonth, "record"=>$getUserCount];
        //     }
        // }

        $totalSumApp = array();

        // Instant Screening
        for($x=$startingYear; $x<=$currentYear; $x++) {

            for($y=1;$y<=12;$y++) {

                if($y>=1 && $y<=9) {
                    $monthFormat = '0'.$y;
                } else {
                    $monthFormat = $y;
                }
                $getMonth = $x.'-'.$monthFormat;

                $getUserCount = $this->Jobseekeradmin_Model->get_count_metricrecordInstant($getMonth);
                $totalInstant[$x][] = ["year"=>$x, "month"=>$monthFormat, "by"=>$getMonth, "record"=>$getUserCount];
                $getUserCountCall = $this->Jobseekeradmin_Model->get_count_metricrecordCall($getMonth);
                $getUserCounting = $getUserCount + $getUserCountCall;
                $totalSumApp[$x][] = ["year"=>$x, "month"=>$monthFormat, "by"=>$getMonth, "record"=>$getUserCounting];
            }
        }

        // Call
        for($x=$startingYear; $x<=$currentYear; $x++) {

            for($y=1;$y<=12;$y++) {

                if($y>=1 && $y<=9) {
                    $monthFormat = '0'.$y;
                } else {
                    $monthFormat = $y;
                }
                $getMonth = $x.'-'.$monthFormat;
                $getUserCount = $this->Jobseekeradmin_Model->get_count_metricrecordCall($getMonth);
                $totalCall[$x][] = ["year"=>$x, "month"=>$monthFormat, "by"=>$getMonth, "record"=>$getUserCount];
            }
        }

        // Visit
        // for($x=$startingYear; $x<=$currentYear; $x++) {

        //     for($y=1;$y<=12;$y++) {

        //         if($y>=1 && $y<=9) {
        //             $monthFormat = '0'.$y;
        //         } else {
        //             $monthFormat = $y;
        //         }
        //         $getMonth = $x.'-'.$monthFormat;

        //         $getUserCount = $this->Jobseekeradmin_Model->get_count_metricrecordVisit($getMonth);
                
        //         $totalVisit[$x][] = ["year"=>$x, "month"=>$monthFormat, "by"=>$getMonth, "record"=>$getUserCount];
        //     }
        // }


        $getMainRecruiter = $this->Jobseekeradmin_Model->get_main_recruiter();
        foreach($getMainRecruiter as $getMainRecruit) {

            $getHaveJobs = $this->Jobseekeradmin_Model->get_count_havejobs($getMainRecruit['id'], $showType);

            if($getHaveJobs > 0) {

                $totalCompany[] = $getMainRecruit['cname'];

                $getAdditionActiveJobs = $this->Jobseekeradmin_Model->get_count_activejobs($getMainRecruit['id'], "daily");
                $getactivejobs = $this->Jobseekeradmin_Model->get_count_activejobs($getMainRecruit['id'], $showType);

                $getAdditionUpcomingExpireJobs = $this->Jobseekeradmin_Model->get_count_upcomingexpirejobs($getMainRecruit['id'], "daily");
                $getUpcomingExpireJobs = $this->Jobseekeradmin_Model->get_count_upcomingexpirejobs($getMainRecruit['id'], $showType);

                $sumInstant = 0;
                $getAllInstantJobs = $this->Jobseekeradmin_Model->get_all_instantjobs($getMainRecruit['id']);
                foreach($getAllInstantJobs as $getAllInstantJob) {

                    $getInstantApplyJobs = $this->Jobseekeradmin_Model->get_count_instantapplyjobs($getAllInstantJob['id'], $showType);
                    $sumInstant = $sumInstant + $getInstantApplyJobs;
                }

                $sumPhone = 0;
                $getAllPhoneJobs = $this->Jobseekeradmin_Model->get_all_Phonejobs($getMainRecruit['id']);
                foreach($getAllPhoneJobs as $getAllPhoneJob) {

                    $getPhoneApplyJobs = $this->Jobseekeradmin_Model->get_count_phoneapplyjobs($getAllPhoneJob['id'], $showType);
                    $sumPhone = $sumPhone + $getPhoneApplyJobs;
                }

                $sumWalk = 0;
                $getAllWalkJobs = $this->Jobseekeradmin_Model->get_all_Walkjobs($getMainRecruit['id']);
                foreach($getAllWalkJobs as $getAllWalkJob) {

                    $getWalkApplyJobs = $this->Jobseekeradmin_Model->get_count_walkapplyjobs($getAllWalkJob['id'], $showType);
                    $sumWalk = $sumWalk + $getWalkApplyJobs;
                }

                $sumNotify = 0;
                $getAllNotifyJobs = $this->Jobseekeradmin_Model->get_all_notifyjobs($getMainRecruit['id']);
                foreach($getAllNotifyJobs as $getAllNotifyJob) {

                    $getNotifySendJobs = $this->Jobseekeradmin_Model->get_count_notifysendjobs($getAllNotifyJob['id'], $showType);
                    $sumNotify = $sumNotify + $getNotifySendJobs;
                }

                if($showType == "daily") {
                    $modifyActiveJobs = $getactivejobs;
                } else {
                    $modifyActiveJobs = $getactivejobs + $getAdditionActiveJobs;
                }

                $totalCompanyArray[] = [
                            "cname"=>$getMainRecruit['cname'],
                            "have_jobs"=>$getHaveJobs,
                            "active_jobs"=>$modifyActiveJobs,
                            "upcoming_expire_jobs"=>$getUpcomingExpireJobs,
                            "total_apply"=>$sumInstant + $sumPhone + $sumWalk,
                            "instant_apply"=>$sumInstant,
                            "phone_apply"=>$sumPhone,
                            "walk_apply"=>$sumWalk,
                            "notification_send"=>$sumNotify,
                ];
            }
        }
        $data = ["totalYear"=>$totalYear, "totalBPO"=>$totalBPO, "totalNonBPO"=>$totalNonBPO, "totalApp"=>$totalSumApp, "totalInstant"=>$totalInstant, "totalCall"=>$totalCall, "totalVisit"=>$totalVisit, "totalRegistration"=>$totalRegistration, "totalCompany"=>$totalCompany, "totalCompanyArray" => $totalCompanyArray];
        $this->load->view('administrator/datametric', $data);

    }

    public function company_wise_data_metrics() {

        $totalRegistration = array();
        $totalBPO = array();
        $totalNonBPO = array();
        $totalCompany = array();
        $totalCompanyArray = array();
        $totalYear = array();

        $getMainRecruiter = $this->Jobseekeradmin_Model->get_comapnywisemain_recruiter();
        foreach($getMainRecruiter as $getMainRecruit) {

            $getHaveJobs = $this->Jobseekeradmin_Model->get_companywisecount_havejobs($getMainRecruit['id'], $showType);
            if($getHaveJobs > 0) {
                $totalCompany[] = ["company_id"=>$getMainRecruit['id'], "company_name"=>$getMainRecruit['cname']];

            }
        }

        if(isset($_GET["company"])) {
            $companyID = $_GET["company"];
        } else {
            $companyID = $totalCompany[0]['company_id'];
        }

        if(strlen($companyID) > 0) {
        
            $currentYear = date('Y');
            $startingYear = 2019;
            
            // Jobs
            for($x=$startingYear; $x<=$currentYear; $x++) {

                $totalYear[] = $x;

                for($y=1;$y<=12;$y++) {

                    if($y>=1 && $y<=9) {
                        $monthFormat = '0'.$y;
                    } else {
                        $monthFormat = $y;
                    }
                    $getMonth = $x.'-'.$monthFormat;
                    $getHaveJobs = $this->Jobseekeradmin_Model->get_monthwisecount_havejobs($companyID, $getMonth);
                    //$getUserCount = $this->Jobseekeradmin_Model->get_count_metricrecord($getMonth);
                    $totalJobs[$x][] = ["year"=>$x, "month"=>$monthFormat, "by"=>$getMonth, "record"=>$getHaveJobs];
                }
            }
            
            // Active Jobs
            for($x=$startingYear; $x<=$currentYear; $x++) {

                for($y=1;$y<=12;$y++) {

                    if($y>=1 && $y<=9) {
                        $monthFormat = '0'.$y;
                    } else {
                        $monthFormat = $y;
                    }
                    $getMonth = $x.'-'.$monthFormat;
                    $getactivejobs = $this->Jobseekeradmin_Model->get_monthwisecount_activejobs($companyID, $getMonth);
                    //$getUserCount = $this->Jobseekeradmin_Model->get_count_metricrecordBPO($getMonth);
                    $totalActives[$x][] = ["year"=>$x, "month"=>$monthFormat, "by"=>$getMonth, "record"=>$getactivejobs];
                }
            }

            // Expire jobs
            for($x=$startingYear; $x<=$currentYear; $x++) {

                for($y=1;$y<=12;$y++) {

                    if($y>=1 && $y<=9) {
                        $monthFormat = '0'.$y;
                    } else {
                        $monthFormat = $y;
                    }
                    $getMonth = $x.'-'.$monthFormat;
                    $getUpcomingExpireJobs = $this->Jobseekeradmin_Model->get_monthwisecount_upcomingexpirejobs($companyID, $getMonth);
                    //$getUserCount = $this->Jobseekeradmin_Model->get_count_metricrecordNonBPO($getMonth);
                    $totalExpires[$x][] = ["year"=>$x, "month"=>$monthFormat, "by"=>$getMonth, "record"=>$getUpcomingExpireJobs];
                }
            }

            // Total Applications, Instant, Phone, walk-in

            $getInstantIDS = array();
            $getPhoneIDS = array();
            $getWalkIDS = array();
            $getNotifyIDS = array();
            $getAllJobs = $this->Jobseekeradmin_Model->get_all_commonjobs($companyID);
            foreach($getAllJobs as $getAllJob) {

                if($getAllJob["mode"] == "Instant screening") {

                    $getInstantIDS[] = ["id" => $getAllJob["id"]];

                } else if($getAllJob["mode"] == "Call") {

                    $getPhoneIDS[] = ["id" => $getAllJob["id"]];

                } else if($getAllJob["mode"] == "Walk-in") {

                    $getWalkIDS[] = ["id" => $getAllJob["id"]];
                }
                $getNotifyIDS[] = ["id" => $getAllJob["id"]];
            }

            for($x=$startingYear; $x<=$currentYear; $x++) {

                for($y=1;$y<=12;$y++) {

                    if($y>=1 && $y<=9) {
                        $monthFormat = '0'.$y;
                    } else {
                        $monthFormat = $y;
                    }
                    $getMonth = $x.'-'.$monthFormat;

                    $sumInstant = 0;
                    foreach($getInstantIDS as $getInstantID) {

                        $getInstantApplyJobs = $this->Jobseekeradmin_Model->get_monthwisecount_instantapplyjobs($getInstantID['id'], $getMonth);
                        $sumInstant = $sumInstant + $getInstantApplyJobs;
                    }
                    $totalInstantJobs[$x][] = ["year"=>$x, "month"=>$monthFormat, "by"=>$getMonth, "record"=>$sumInstant];

                    $sumPhone = 0;
                    foreach($getPhoneIDS as $getPhoneID) {

                        $getPhoneApplyJobs = $this->Jobseekeradmin_Model->get_monthwisecount_phoneapplyjobs($getPhoneID['id'], $getMonth);
                        $sumPhone = $sumPhone + $getPhoneApplyJobs;
                    }
                    $totalPhoneJobs[$x][] = ["year"=>$x, "month"=>$monthFormat, "by"=>$getMonth, "record"=>$sumPhone];

                    $sumWalk = 0;
                    foreach($getWalkIDS as $getWalkID) {

                        $getWalkApplyJobs = $this->Jobseekeradmin_Model->get_monthwisecount_walkapplyjobs($getWalkID['id'], $getMonth);
                        $sumWalk = $sumWalk + $getWalkApplyJobs;
                    }
                    $totalWalkInJobs[$x][] = ["year"=>$x, "month"=>$monthFormat, "by"=>$getMonth, "record"=>$sumWalk];

                    // All application
                    $totalApplicationInJobs[$x][] = ["year"=>$x, "month"=>$monthFormat, "by"=>$getMonth, "record"=>$sumInstant + $sumPhone + $sumWalk];


                    $sumNotify = 0;
                    foreach($getNotifyIDS as $getNotifyID) {

                        $getNotifySendJobs = $this->Jobseekeradmin_Model->get_monthwisecount_notifysendjobs($getNotifyID['id'], $getMonth);
                        $sumNotify = $sumNotify + $getNotifySendJobs;
                    }
                    $totalNotifyInJobs[$x][] = ["year"=>$x, "month"=>$monthFormat, "by"=>$getMonth, "record"=>$sumNotify];

                }
            }
        
        }
        $data = ["totalCompany"=>$totalCompany, "totalYear"=>$totalYear, "totalJobs"=>$totalJobs, "totalActives"=>$totalActives, "totalExpires"=>$totalExpires, "totalInstantJobs"=>$totalInstantJobs, "totalPhoneJobs"=>$totalPhoneJobs, "totalWalkInJobs" => $totalWalkInJobs, "totalApplicationInJobs" => $totalApplicationInJobs, "totalNotifyInJobs"=>$totalNotifyInJobs];
        $this->load->view('administrator/datametric_company', $data);

    }
        
    public function radiusUpdate() {
        
        $radius_km = $this->input->post('radius_km');
        $data = ["km" => $radius_km];

        $this->Recruiteradmin_Model->radiuskmupdate($data, 1);
        redirect("administrator/dashboard");
    }

    public function logout() {
        //unset($_SESSION['userSession']);
        $this->session->unset_userdata('adminSession');
        redirect("administrator/admin/index");
    }
}
?>