<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittall
 * @license         Mobulous
 */
class Slider extends CI_Controller {
    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('User_Model');
        $this->load->model('recruiter/Jobpost_Model');
        $this->load->model('recruiter/Newpoints_Model');
        $this->load->model('recruiter/Homejob_Model');
        $this->load->model('Common_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');
        $this->load->library('google');
        $this->load->library('Facebook');
    }

  public function index(){
   // echo "hi";die;
    $this->load->view('slider');
  }
   } 