<?php
ob_start();
ini_set("allow_url_fopen", 1);
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */
class Report extends CI_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('recruiter/Jobpost_Model');
        $this->load->model('recruiter/Recruit_Model');
        $this->load->model('Common_Model');
        $this->load->library('form_validation');
        $this->load->model('Jobpostrecruiter_Model');
        $this->load->library('encryption');

        if($this->session->userdata('userSession')) {
            
        } else {
            redirect("recruiter/recruiter/index");
        }
    }
    public function index() {
          $data["jobLists"] = $this->Jobpostrecruiter_Model->boostjob_fetch();
        $this->load->view('recruiter/report-list');
    }

    public function getDatabyStatus(){
              $status = $this->input->post('status');
        $date =  date('Y-m-d');
        $userSession = $this->session->userdata('userSession');
        //echo $date;die;
        $data = $this->Jobpostrecruiter_Model->getDatabyStatus($status,$date,$userSession['id']);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">SNo</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Recruiter Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {

             if( date('Y-m-d', strtotime($jobList['created_at'])) == $date  ){
                $output.=' <tr>
              <td style="width:40px;">'. $x1.'</td>
              <td style="width:100px;">'. $jobList['name'].'</td>
              <td style="width:100px;">'. $jobList['cname'].'</td>
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              
              
           </tr>';
           $x1++;
             }
             
          }
          
    $output.= '</tbody>';
        }else{
            $output='<tbody>
            <td>No Data Found<td>
            </tbody>';
        }
        
    echo $output;
       }

   public function getDatabyStatusweek(){
         $status = $this->input->post('status');
        $date =  date('Y-m-d');
        $userSession = $this->session->userdata('userSession');
        //echo $date;die;
        $data = $this->Jobpostrecruiter_Model->getDatabyStatus($status,$date,$userSession['id']);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">SNo</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Recruiter Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {

             
             //echo date('Y-m-d', strtotime('-7 days'));die;
             if( date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-7 days'))  ){
                $output.=' <tr>
              <td style="width:40px;">'. $x1.'</td>
              <td style="width:100px;">'. $jobList['name'].'</td>
              <td style="width:100px;">'. $jobList['cname'].'</td>
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              
              
           </tr>';
           $x1++;
             }
          
          }
          
    $output.= '</tbody>';
        }else{
            $output='<tbody>
            <td>No Data Found<td>
            </tbody>';
        }
        
    echo $output;
    
    }    


  public function getDatabyStatusmonth(){
        $date =  date('Y-m-d');
        $userSession = $this->session->userdata('userSession');
        $status = $this->input->post('status');
        //echo $date;die;
        $data = $this->Jobpostrecruiter_Model->getDatabyStatus($status,$date,$userSession['id']);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">SNo</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Recruiter Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {


             if( date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-30 days'))  ){
                $output.=' <tr>
              <td style="width:40px;">'. $x1.'</td>
              <td style="width:100px;">'. $jobList['name'].'</td>
              <td style="width:100px;">'. $jobList['cname'].'</td>
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              
              
           </tr>';
           $x1++;
             }
       
          
          }
          
    $output.= '</tbody>';
        }else{
            $output='<tbody>
            <td>No Data Found<td>
            </tbody>';
        }
        
    echo $output;
    }    

  public function getDatabyStatusyear(){
        $status = $this->input->post('status');
        $userSession = $this->session->userdata('userSession');
        $date =  date('Y-m-d');
        //echo $date;die;
        $data = $this->Jobpostrecruiter_Model->getDatabyStatus($status,$date,$userSession['id']);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">SNo</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Recruiter Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {

             
             if( date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-365 days'))  ){
                $output.=' <tr>
              <td style="width:40px;">'. $x1.'</td>
              <td style="width:100px;">'. $jobList['name'].'</td>
              <td style="width:100px;">'. $jobList['cname'].'</td>
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              
              
           </tr>';
           $x1++;
             }
          
          }
          
    $output.= '</tbody>';
        }else{
            $output='<tbody>
            <td>No Data Found<td>
            </tbody>';
        }
        
    echo $output;

    }

    public function getDatabyStatusDate(){
        $status = $this->input->post('status');
        $from_date = $this->input->post('from_date');
        $to_date = $this->input->post('to_date');
        $date =  date('Y-m-d');
        $userSession = $this->session->userdata('userSession');
        //echo $date;die;
        $data = $this->Jobpostrecruiter_Model->getDatabyStatus($status,$date,$userSession['id']);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">SNo</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Recruiter Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {
            //echo date('Y-m-d', strtotime($jobList['created_at']));
             if( date('Y-m-d', strtotime($jobList['created_at'])) >= $from_date &&   date('Y-m-d', strtotime($jobList['created_at'])) <= $to_date){
                $output.=' <tr>
              <td style="width:40px;">'. $x1.'</td>
              <td style="width:100px;">'. $jobList['name'].'</td>
              <td style="width:100px;">'. $jobList['cname'].'</td>
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              
              
           </tr>';
           $x1++;
             }
          
          }
          
    $output.= '</tbody>';
        }else{
            $output='<tbody>
            <td>No Data Found<td>
            </tbody>';
        }
        
    echo $output;

    }


}
?>
