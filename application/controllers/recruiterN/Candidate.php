<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */
class Candidate extends CI_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('recruiter/Jobpost_Model');
        $this->load->model('recruiter/Candidate_Model');
        $this->load->model('recruiter/Recruit_Model');
        $this->load->model('Common_Model');
        $this->load->model('User_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');

        if($this->session->userdata('userSession')) {
            
        } else {
            redirect("recruiter/recruiter/index");
        }
    }

    public function index() {
        $userSession = $this->session->userdata('userSession');
        $jobtype = $_GET['type'];
        $data['hiredlist'] = $this->Candidate_Model->hired_list($jobtype);
        $data['rejectedlist'] = $this->Candidate_Model->rejected_list($jobtype);
        $data['referedlist'] = $this->Candidate_Model->refered_list($jobtype);
        $data['jobdetail'] = $this->Candidate_Model->job_detail($jobtype);
        
        $newApps = $this->Candidate_Model->newApplication_list($jobtype);
       $newApp1 = array();
        foreach($newApps as $newApp) {
            $getRate = $this->calculateRating($newApp['user_id']);
            $newApp1[] = ['jobpost_id'=>$newApp['jobpost_id'], 'user_id'=>$newApp['user_id'], 'interviewdate'=>$newApp['interviewdate'], 'interviewtime'=>$newApp['interviewtime'], 'name'=>$newApp['name'], 'email'=>$newApp['email'], 'location'=>$newApp['location'], 'phone'=>$newApp['phone'], 'profilePic'=>$newApp['profilePic'], "rating"=>$getRate];
        }
        $data['newApp'] = $newApp1;

        $goingApps = $this->Candidate_Model->goingApplication_list($jobtype);
        $goingApp1 = array();
        foreach($goingApps as $goingApp) {
            $getRate = $this->calculateRating($goingApp['user_id']);
            $goingApp1[] = ['jobpost_id'=>$newApp['jobpost_id'], 'user_id'=>$newApp['user_id'], 'interviewdate'=>$newApp['interviewdate'], 'interviewtime'=>$newApp['interviewtime'], 'name'=>$newApp['name'], 'email'=>$newApp['email'], 'location'=>$newApp['location'], 'phone'=>$newApp['phone'], 'profilePic'=>$newApp['profilePic'], "rating"=>$getRate];
        }
        $data['goingApp'] = $goingApp1;

        $acceptedApps = $this->Candidate_Model->acceptedApplication_list($jobtype);
        $acceptedApp1 = array();
        foreach($acceptedApps as $acceptedApp) {
            $getRate = $this->calculateRating($acceptedApp['user_id']);
            $acceptedApp1[] = ['jobpost_id'=>$newApp['jobpost_id'], 'user_id'=>$newApp['user_id'], 'interviewdate'=>$newApp['interviewdate'], 'interviewtime'=>$newApp['interviewtime'], 'name'=>$newApp['name'], 'email'=>$newApp['email'], 'location'=>$newApp['location'], 'phone'=>$newApp['phone'], 'profilePic'=>$newApp['profilePic'], "rating"=>$getRate];
        }
        $data['acceptedApp'] = $acceptedApp1;
        
        $this->load->view('recruiter/candidate_profile_list', $data);
    }

    public function candidate_lists() {
        $jobtype = $_GET['type'];
        $jobstatus = trim($_GET['status']);
        $data['hiredlist'] = $this->Candidate_Model->hired_list($jobtype);
        $data['rejectedlist'] = $this->Candidate_Model->rejected_list($jobtype);
        $data['jobdetail'] = $this->Candidate_Model->job_detail($jobtype);
        $data['goingApp'] = $this->Candidate_Model->goingApplication_list($jobtype);
        $userSession = $this->session->userdata('userSession');
        

        if($jobstatus == 1) {
            $data['alllists'] = $this->Candidate_Model->hiredapplication_list($jobtype);
            
        } else if($jobstatus == 2) {
            $data['alllists'] = $this->Candidate_Model->referapplication_list($jobtype);
            
        } else if(strcmp($jobstatus, 3)) {
            $data['alllists'] = $this->Candidate_Model->falloutapplication_list($jobtype);
        }
        //echo $this->db->last_query();die;
        $this->load->view('recruiter/candidate_profile_list_status', $data);
    }
    
    public function candidate_suggested() {
        $userSession = $this->session->userdata('userSession');
        $jobtype = $_GET['type'];
        $data['jobdetail'] = $this->Candidate_Model->job_detail($jobtype);
        //print_r($data['jobdetail']);exit();

        $skills=$data['jobdetail'][0]['skills'];
        $experience=$data['jobdetail'][0]['experience'];
         $designation=$data['jobdetail'][0]['jobtitle'];
       //echo $designation;die;
        $skill = explode(', ',$skills);

        $return_array = array();

         
       
        for($i=0;$i<count($skill);$i++){
           $suggest= $this->Candidate_Model->suggestedcandidate($skill[$i]);
           //print_r($suggest);
        
           
           foreach($suggest as $value){
           if(!empty($value)) {
            $return_array[]= $value['user_id'];
            
            }
           
           }
           
           
        }

        $user_desgexp = $this->Candidate_Model->user_list();
        //print_r($user_desgexp);die;

        foreach($user_desgexp as $key){
          if($key['exp_year'] == $experience || $key['designation'] == $designation){
              
               $return_array[] =  $key['id'];
          }                
        }
        
       // print_r($return_array);die;
        $return_array = array_unique($return_array);
        //print_r($return_array);die;
        foreach ($return_array as $key =>  $value) {
             $fetchcandidatedetails = $this->Candidate_Model->fetchcandidate($value,$jobtype);

             if(!empty($fetchcandidatedetails)){
                unset($return_array[$key]);
             }
        }
       
        
        foreach($return_array as $userid){
           $candiade = $this->Candidate_Model->fetchsuggested($userid);
            //echo $this->db->last_query();
        }

        if(!empty($candiade)){

            $data['candidatedetails']= $candiade;
        }
 
        $this->load->view('recruiter/candidate_suggested', $data);
    }

    public function manageCandidates() {
        $userSession = $this->session->userdata('userSession');
        if(!empty($_GET['status'])){
            $status = $_GET['status'];
        }
        
        if(!empty($status)){
            $data['candidatesApplied'] = $this->Candidate_Model->candidateApplied_list1($userSession['id'],$status);
        }else{
           $data['candidatesApplied'] = $this->Candidate_Model->candidateApplied_list($userSession['id']); 
        }
        
        $data["languages"] = $this->Common_Model->language_lists();
        $data["industries"] = $this->Common_Model->industry_lists();
        $data['skillData'] = $this->Candidate_Model->skills_fetch();
        $data['clients'] = $this->Recruit_Model->user_clientSupported();
        $data['levels'] = $this->Jobpost_Model->level_lists();
        $data['category'] = $this->Jobpost_Model->category_lists();
        $data['subcategory'] = $this->Jobpost_Model->subcategory_lists();
        $this->load->view('recruiter/manage_candidate', $data);
    }
    
    public function filtermanagecandidate() {
        $userSession = $this->session->userdata('userSession');
        $filterData = $this->input->post();
        $data['candidatesApplied'] = $this->Candidate_Model->Applied_listfilter($userSession['id'], $filterData);
        $data["languages"] = $this->Common_Model->language_lists();
        $data["industries"] = $this->Common_Model->industry_lists();
        $data['skillData'] = $this->Candidate_Model->skills_fetch();
        $data['clients'] = $this->Recruit_Model->user_clientSupported();
        $data['levels'] = $this->Jobpost_Model->level_lists();
        $data['category'] = $this->Jobpost_Model->category_lists();
        $data['subcategory'] = $this->Jobpost_Model->subcategory_lists();
	$data['filterdata'] = $filterData;
        $this->load->view('recruiter/manage_candidate', $data);
    }

    public function searchCandidates() {
        $userSession = $this->session->userdata('userSession');
        $keyword = $this->input->post();
        if(!empty($keyword['keyword'])){
          $key = $keyword['keyword'];
        }
        else{
          $key= '';
        }
        $data['candidatesApplied'] = $this->Candidate_Model->candidateApplied_listsearch($userSession['id'], $key);
        $data["languages"] = $this->Common_Model->language_lists();
        $data["industries"] = $this->Common_Model->industry_lists();
        $data['skillData'] = $this->Candidate_Model->skills_fetch();
        $data['clients'] = $this->Recruit_Model->user_clientSupported();
        $data['keyword'] = $key;
        $this->load->view('recruiter/search_candidate', $data);
    }

    public function sendNotification(){
      $uid = $this->input->post('uid');
      $jid = $this->input->post('job_id');
      $user_data = $this->User_Model->user_single($uid);
      $fetchnotifications = $this->Candidate_Model->fetchnotifications($uid);
      if($user_data[0]['device_type']=="android") {
          $res1 = $this->push_notification_suggested($user_data[0]['device_token'],'New Job', 'There is a job suggested for you','suggest job',$jid);

      }else if($user_data[0]['device_type']=="ios") {
          //echo $recruiter_id;die;
           $this->pushiphon(array($user_data[0]['device_token'],'There is a job suggested for you','suggest job','New Job',$jid,count($fetchnotifications)));
      }
    }

    public function jobstatuschange() {
        $jobData = $this->input->post();
        $jobtitle = $jobData['jobtitle'];
        $recruiter_id = $jobData['recruiter_id'];
        $companyname = $this->Candidate_Model->fetchcomp($recruiter_id);
        
        if($jobData['falloutreason']){
            if($jobData['falloutreason'] == 1) {
               $reason = "No show";
            } else if($jobData['falloutreason'] == 2) {
               $reason = "Did not meet requirement";
            } else if($jobData['falloutreason'] == 3) {
               $reason = "Did not Accept Job Offers";
            } else if($jobData['falloutreason'] == 4) {
               $reason = "Refer to another job listing";
            } else if($jobData['falloutreason'] == 5) {
               $reason = "Day 1 No Show";
            }
        }

        if($jobData['statusradio'] == 2) {
           $data = ["status" => 3, "fallout_status" => 1, "fallout_reason" => "No Show"];
           $this->Candidate_Model->candidateApplied_statuschange($jobData['appid'], $jobData['uid'], $data);
        
        } else if($jobData['statusradio'] == 3) {
          if(empty($jobData['falloutreason'])){
            $this->session->set_tempdata('fallerr','Fallout reason is required');
          }
            $data = ["status" => $jobData['statusradio'], "fallout_status" => $jobData['falloutreason'], "fallout_reason" => $reason];
            $this->Candidate_Model->candidateApplied_statuschange($jobData['appid'], $jobData['uid'], $data);
        
        } else if($jobData['statusradio'] == 4) {
          if(empty($jobData['jobid'])){
            $this->session->set_tempdata('jobiderr','Job Id is required');
          }
            $data = ["status" => 1,  'jobpost_id' => $jobData['jobid']];
            $this->Candidate_Model->candidateApplied_statuschange($jobData['appid'], $jobData['uid'], $data);
        
        } else if($jobData['statusradio'] == 5) {

            if($jobData['goingreason'] == 1) {
               $data = ["status" => 5, "fallout_status" => 0, "fallout_reason" => "Open"];
               $this->Candidate_Model->candidateApplied_statuschange($jobData['appid'], $jobData['uid'], $data);
            
            } else if($jobData['goingreason'] == 2) {
               $data = ["status" => 5, "fallout_status" => 0, "fallout_reason" => "Hiring"];
               $this->Candidate_Model->candidateApplied_statuschange($jobData['appid'], $jobData['uid'], $data);
            }
        
        } else if($jobData['statusradio'] == 6) {

            if($jobData['acceptedreason'] == 1) {
               $data = ["status" => 6, "fallout_status" => 0, "fallout_reason" => "ACCEPTED JO"];
               $this->Candidate_Model->candidateApplied_statuschange($jobData['appid'], $jobData['uid'], $data);
            
            } else if($jobData['acceptedreason'] == 2) {
               $data = ["status" => 6, "fallout_status" => 0, "fallout_reason" => "ACCEPTED JO Rechedule"];
               $this->Candidate_Model->candidateApplied_statuschange($jobData['appid'], $jobData['uid'], $data);
            }
        
        } else {
            $data = ["status" => $jobData['statusradio'], "fallout_status" =>  0,"fallout_reason" => " "];
            $this->Candidate_Model->candidateApplied_statuschange($jobData['appid'], $jobData['uid'], $data);
        }

        if($jobData['statusradio']=="1") {
            $msg="Your Job application for "." " . $jobtitle ." "."with "." " .$companyname[0]['cname'] ." is "."New Application";
        } if($jobData['statusradio']=="2") {
            $msg="Your Job application for "." ". $jobtitle ." "."with "." ". $companyname[0]['cname'] ." is changed to "."No Show";
        
        } if($jobData['statusradio']=="3") {
            $msg="Your Job application for "." ". $jobtitle ." "."with "." ". $companyname[0]['cname'] ." is changed to "."Fall Out";
        
        } if($jobData['statusradio']=="4") {
            $msg="Your Job application for "." ". $jobtitle ." "."with "." ". $companyname[0]['cname'] ." is changed to "."Refer";
        
        } if($jobData['statusradio'] == 5) {
            $msg="Your Job application for "." ". $jobtitle ." "."with "." ". $companyname[0]['cname'] ." is chnaged to "."On Going Application";
        
        } if($jobData['statusradio'] == 6) {
            $msg="Your Job application for "." ". $jobtitle ." "."with "." ". $companyname[0]['cname'] ." is changed to "."Accepted JO";
        
        } if($jobData['statusradio'] == 7) {
            $msg1="Your Job application for "." ". $jobtitle ." "."with "." ". $companyname[0]['cname'] ." is changed to "."Hired. Please share your success story";
            $msg2="Your Job application for "." ". $jobtitle ." "."with "." ". $companyname[0]['cname'] ." is changed to "."Hired. Please share your review";
        }
        
        $data['candidatesApplied'] = $this->Candidate_Model->fetchnotification($jobData['uid'], $jobData['appid'], $jobData['statusradio']);
        
        $user_data = $this->User_Model->user_single($jobData['uid']);
        
        if($user_data[0]['notification_status']=="1") {

            if(empty($data['candidatesApplied'])) {
                
               if($jobData['statusradio'] == 7) {
                  ///var_dump("2");die;
                  $data1 = ["user_id"=>$jobData['uid'],"jobapp_id"=>$jobData['appid'], "recruiter_id" =>$jobData['recruiter_id'], "notification"=>$msg1, "status_changed"=>$jobData['statusradio'], "job_status"=>'1'];
                  
                  $notificationInserted = $this->Candidate_Model->notification_insert($data1);
                  
                  $data2 = ["user_id"=>$jobData['uid'],"jobapp_id"=>$jobData['appid'], "recruiter_id" =>$jobData['recruiter_id'], "notification"=>$msg2,"status_changed"=>$jobData['statusradio'], "job_status"=>'2'];
                  //var_dump($data2);die;
                  $notificationInserted1 = $this->Candidate_Model->notification_insert($data2);
                  $fetchnotifications = $this->Candidate_Model->fetchnotifications($jobData['uid']);
                  if($user_data[0]['device_type']=="android") {
                      $res1 = $this->push_notification_android_customer($user_data[0]['device_token'],$jobtitle, $msg1,'share story',$recruiter_id,$companyname[0]['cname'],$notificationInserted);
                      //print_r($res1);die;
                  $res2 = $this->push_notification_android_customer($user_data[0]['device_token'],$jobtitle, $msg2,'share review',$recruiter_id,$companyname[0]['cname'],$notificationInserted);

                  }else if($user_data[0]['device_type']=="ios") {
                      //echo $recruiter_id;die;
                       $this->pushiphon(array($user_data[0]['device_token'],$msg1,'share story',$jobtitle,$recruiter_id,count($fetchnotifications),$companyname[0]['cname'],$notificationInserted));
                      //print_r($respos);die;
                      $this->pushiphon(array($user_data[0]['device_token'],$msg2,'share review',$jobtitle,$recruiter_id,count($fetchnotifications),$companyname[0]['cname'],$notificationInserted1));
                  }
           
               } else {
                  $data1 = ["user_id"=>$jobData['uid'],"jobapp_id"=>$jobData['appid'], "recruiter_id" =>$jobData['recruiter_id'], "notification"=>$msg, "status_changed"=>$jobData['statusradio'], "job_status"=>'0'];

                  $notificationInserted = $this->Candidate_Model->notification_insert($data1);
                  $fetchnotifications = $this->Candidate_Model->fetchnotifications($jobData['uid']);
                  if($user_data[0]['device_type']=="android") {
                      $res = $this->push_notification_android_customer($user_data[0]['device_token'], $jobtitle, $msg, 'applied', $jobData['jid'],$companyname[0]['cname'],$notificationInserted);
                  
                  }  else if($user_data[0]['device_type']=="ios"){
                      $this->pushiphon(array($user_data[0]['device_token'], $msg, 'applied',$jobtitle, $jobData['jid'],count($fetchnotifications),$companyname[0]['cname'],$notificationInserted));
                      
                  } 
               } 
            } else {
               if($jobData['statusradio'] == 7) {
                   $data1 = ["notification"=>$msg, "job_status"=>'1'];
                   $notificationInserted = $this->Candidate_Model->notification_update($jobData['uid'],$jobData['statusradio'],$jobData['appid'],$data1);
                   $fetchnotifications = $this->Candidate_Model->fetchnotifications($jobData['uid']);
                   if($user_data[0]['device_type']=="android") {
                       $res1 = $this->push_notification_android_customer($user_data[0]['device_token'],$jobtitle, $msg1,'share story',$recruiter_id,$companyname[0]['cname']);
                       $res2 = $this->push_notification_android_customer($user_data[0]['device_token'],$jobtitle, $msg2,'share review',$recruiter_id,$companyname[0]['cname']);
                   
                   } else if($user_data[0]['device_type']=="ios") {
                   $this->pushiphon(array($user_data[0]['device_token'],$msg1,'share story',$jobtitle,$recruiter_id),count($fetchnotifications),$companyname[0]['cname']);
                   $this->pushiphon(array($user_data[0]['device_token'],$msg2,'share review',$jobtitle,$recruiter_id),count($fetchnotifications),$companyname[0]['cname']);
               }
                   
                   
               
               }else{
               
                  $data1 = ["notification"=>$msg, "job_status"=>'0'];
                  $notificationInserted = $this->Candidate_Model->notification_update($jobData['uid'],$jobData['statusradio'],$jobData['appid'],$data1);
                  $fetchnotifications = $this->Candidate_Model->fetchnotifications($jobData['uid']);
                  if($user_data[0]['device_type']=="android") {

                     $res = $this->push_notification_android_customer($user_data[0]['device_token'], $jobtitle, $msg, 'applied' ,$jobData['jid'],$companyname[0]['cname'],$notificationInserted); 

                  } else if($user_data[0]['device_type']=="ios"){
                     $this->pushiphon(array($user_data[0]['device_token'],$msg,'applied',$jobtitle,$jobData['jid'],count($fetchnotifications),$companyname[0]['cname'],$notificationInserted)); 
                  } 
               }
            }
           
        }else{
            
            if(empty($data['candidatesApplied'])) {
            
               if($jobData['statusradio']=="7") {
                  
                  $data1 = ["user_id"=>$jobData['uid'],"jobapp_id"=>$jobData['appid'], "recruiter_id" =>$jobData['recruiter_id'], "notification"=>$msg1, "status_changed"=>$jobData['statusradio'], "job_status"=>'1'];
                  $notificationInserted = $this->Candidate_Model->notification_insert($data1);
                  
                  $data2 = ["user_id"=>$jobData['uid'],"jobapp_id"=>$jobData['appid'], "recruiter_id" =>$jobData['recruiter_id'], "notification"=>$msg2, "status_changed"=>$jobData['statusradio'], "job_status"=>'2'];
                  $notificationInserted1 = $this->Candidate_Model->notification_insert($data2);
           
               } else {
                  $data1 = ["user_id"=>$jobData['uid'],"jobapp_id"=>$jobData['appid'], "recruiter_id" =>$jobData['recruiter_id'], "notification"=>$msg, "status_changed"=>$jobData['statusradio'], "job_status"=>'0'];

                  $notificationInserted = $this->Candidate_Model->notification_insert($data1);
               }
                  
            } else {

               if($jobData['statusradio']=="7") {
                   
                   $data1 = ["notification"=>$msg, "job_status"=>'1'];
                   $notificationInserted = $this->Candidate_Model->notification_update($jobData['uid'],$jobData['statusradio'],$jobData['appid'],$data1);
               
               } else{
               
                  $data1 = ["notification"=>$msg, "job_status"=>'0'];
                  $notificationInserted = $this->Candidate_Model->notification_update($jobData['uid'],$jobData['statusradio'],$jobData['appid'],$data1);
                  
               }
            }
            
         }
       
      redirect("recruiter/candidate/manageCandidates");
   }

    public function filtercandidate() {
        $userSession = $this->session->userdata('userSession');
        $filterData = $this->input->post();
        $data['candidatesApplied'] = $this->Candidate_Model->search_listfilter($userSession['id'], $filterData);
        $data["languages"] = $this->Common_Model->language_lists();
        $data["industries"] = $this->Common_Model->industry_lists();
        $data['skillData'] = $this->Candidate_Model->skills_fetch();
        $data['clients'] = $this->Recruit_Model->user_clientSupported();
        $data['keyword'] = $filterData['keyword'];
        $this->load->view('recruiter/search_candidate', $data);
    }

    public function singleCandidates() {
        $id = $this->input->get('id');
        $userSession = $this->session->userdata('userSession');
        $data['candidatesApplied'] = $this->Candidate_Model->candidateApplied_single($id);
        $data['candidateEducation'] = $this->Candidate_Model->candidateEducation_single($id);
        $data['candidateExp'] = $this->Candidate_Model->candidateExp_single($id);
        $data['candidateSkills'] = $this->Candidate_Model->candidateSkills_single($id);
        $data['candidateClients'] = $this->Candidate_Model->candidateClients_single($id);
        $data['candidateLang'] = $this->Candidate_Model->candidateLanguages_single($id);
        $data['candidateAssessment'] = $this->Candidate_Model->candidateAssessment_single($id);
        $data['candidateExpert'] = $this->Candidate_Model->candidateExpert_single($id);
        $this->load->view('recruiter/candidate_single', $data);
    }
    
    public function contact(){
        $contactData = $this->input->post();
        
        $data1 = ["name"=>$contactData['name'], "email"=>$contactData['email'], "phone"=> $contactData['phone'], "message"=> $contactData['message']];

        $contactInserted = $this->Candidate_Model->contact_insert($data1);
        $userId = $contactData['user_id'];
        if($contactInserted) {
            
            $this->load->library('email');
            $this->email->from($contactData['email'], "JobYodha");
            $this->email->to($contactData['user_email']);
            $this->email->subject('Contact from JobYodha');
            $msg = "Dear " . $contactData['name'];
            $msg .= ". Jobyodha Recruited contacted you. Please check the message below. ";
            $msg .= ". Name ". $contactData['name'];
            $msg .= ". and Message ". $contactData['message'];
            $this->email->message($msg);
            $this->email->send();

            $this->session->set_tempdata('contactSuccess','Contacted Successfully');
            
            redirect("recruiter/candidate/singleCandidates?id=$userId");
        } else {
            redirect("recruiter/candidate/singleCandidates?id=$userId");
        }
    
    }

    function push_notification_android_customer($regid,$title,$body,$type,$recruiter_id,$companyname,$notification_id){
      date_default_timezone_set('Asia/Kolkata');
        define('API_ACCESS_KEY','AIzaSyCTzjJxETlJxp18hCwYHLFETLZRkbGFiGw');
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $notification = [
            'title' =>$title,
            'body' => $body,
            'type' => $type,
            'recruiter_id' => $recruiter_id,
            'story_id' => '13',
            'companyname' => $companyname,
            'notification_id' => $notification_id,
            'timestamp' => date('H:i:s'),
            'suggestid' => '2'
        ];
        $extraNotificationData = $notification;
        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to'        => $regid,
            //'notification' => $notification,
            'data' => $extraNotificationData
        ];
        $headers1 = [
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
        }

        function push_notification_suggested($regid,$title,$body,$type,$recruiter_id){
      date_default_timezone_set('Asia/Kolkata');
        define('API_ACCESS_KEY','AIzaSyCTzjJxETlJxp18hCwYHLFETLZRkbGFiGw');
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $notification = [
            'title' =>$title,
            'body' => $body,
            'type' => $type,
            'recruiter_id' => $recruiter_id,
            'story_id' => '13',
            'companyname' => '22',
            'notification_id' => '32',
            'timestamp' => date('H:i:s'),
            'suggestid' => '1'
        ];
        $extraNotificationData = $notification;
        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to'        => $regid,
            //'notification' => $notification,
            'data' => $extraNotificationData
        ];
        $headers1 = [
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
        }

        function pushiphon($array)       
         {      //print_r($array);die;                           
           $deviceToken = $array[0];           
           $passphrase = '1234';  
           $this->autoRender = false;
           $this->layout = false; 
           //$basePath = WWW_ROOT."QualititionSolution.pem";                 
           $basePath = "p12ForPem.pem";                 
             //echo file_exists($basePath);  die;          
           if(file_exists($basePath))             
           {  
           //echo $basePath." exists";die;     
           $ctx = stream_context_create(); 
           stream_context_set_option($ctx, 'ssl', 'local_cert', $basePath);
           stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
           $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,  
           $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx); 
           if (!$fp)     
           exit("Failed to connect: $err $errstr" . PHP_EOL);
          $body['aps'] = array('alert' => array('title' => $array[3],"body"=>$array[1]),'sound' =>'default','badge'=>$array[5]);
          $body['body'] = $array[1];
          $body['type'] = $array[2];
          $body['recruiter_id'] = $array[4];
          $body['companyname'] = $array[6];
          $body['notification_id'] = $array[7];
          //echo"<pre>";print_r($body);die; 
          $payload = json_encode($body);
           
           $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
          
          $result = fwrite($fp, $msg, strlen($msg));
          //print_r($result);die;
           if (!$result)
           {           
                    
           }     
            else  
           {   
            //return $result; 
            //echo "<pre>";print_r($body['aps']); die;   
              
           }       
             fclose($fp); 
           }  
      }

    /*function android_push($deviceToken = null, $message = null, $badge = null)    
    {    
      
        $url              = 'https://fcm.googleapis.com/fcm/send';   
        $message          = array("message" => $message,'sound' => 'default','badge'=>$badge);  
         
        $registatoin_ids  = array($deviceToken);
        $fields           = array('registration_ids' => $registatoin_ids, 'data' => $message);
        
        $GOOGLE_API_KEY   = "AIzaSyCTzjJxETlJxp18hCwYHLFETLZRkbGFiGw";   
        $headers          = array(
            'Authorization: key=' . $GOOGLE_API_KEY,
            'Content-Type: application/json',   
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);    
        
        
        curl_close($ch);
        return $result;

    }*/

  public function calculateRating($id) {
      
      $attainments = $this->fetchEduSingleData($id);
      if($attainments) {
         $rate1 = array();
         foreach($attainments as $attainment) {
            if($attainment['attainment'] == "Post Graduate") {
               $rate1[] = 5;
            } else if($attainment['attainment'] == "College Graduate") {
               $rate1[] = 5;
            } else if($attainment['attainment'] == "Associate Degree") {
               $rate1[] = 4;
            } else if($attainment['attainment'] == "Undergraduate") {
               $rate1[] = 3;
            } else if($attainment['attainment'] == "Vocational") {
               $rate1[] = 3;
            } else if($attainment['attainment'] == "High School") {
               $rate1[] = 2;
            } else{
               $rate1[] = 0;
            }
         }
         $maxVal = max($rate1);
         $star = ($maxVal * 20)/100;  
      
      } else{
         $star =0;
      }

      $experiences = $this->fetchWorkSingleData($id);
      //print_r($experiences);
      if(!empty($experiences)) {
        //print_r($experiences);
         $totalDays = 0;
         foreach($experiences as $experience) {
            $from = $experience['from'];
            //$from = strtotime($from);
            //echo $from;die;
            $to = $experience['to'];
            $datetime1 = new DateTime($from);
            //$datetime1->setTimestamp(1372622987);
            $datetime2 = new DateTime($to);
            $interval = $datetime1->diff($datetime2);
            $totalDays = $totalDays + $interval->days;
         }

         if($totalDays == 0) {
            $rate2 = 1;
         } else if(($totalDays < 360) && ($totalDays > 0)) {
            $rate2 = 2;
         } else if(($totalDays < 720) && ($totalDays > 300)) {
            $rate2 = 3;
         } else if(($totalDays < 1080) && ($totalDays > 720)) {
            $rate2 = 4;
         } else if($totalDays > 1080) {
            $rate2 = 5;
         } else {
            $rate2 = 0;
         }
         $star1 = $rate2 * 30 / 100;
      } else {
         $star1 = 0;
      }

      $tenure = $this->fetchWorkuniqueData($id);
      
      if($tenure) {
         $totalDays1 = 0;
         $countComp = 0;
         foreach($experiences as $experience) {
            $from = $experience['from'];
            //$from = strtotime($from);
            $to = $experience['to'];
            $datetime1 = new DateTime($from);
            $datetime2 = new DateTime($to);
            $interval = $datetime1->diff($datetime2);
            $totalDays1 = $totalDays + $interval->days;
            $countComp++;
         }

         $avgWork = $totalDays1 / $countComp;
         if($avgWork == 0) {
            $rate3 = 1;
         } else if($avgWork < 360 && $avgWork > 0) {
            $rate3 = 2;
         } else if($avgWork < 720 && $avgWork > 300) {
            $rate3 = 3;
         } else if($avgWork < 1080 && $avgWork > 720) {
            $rate3 = 4;
         } else if($avgWork >= 1080) {
            $rate3 = 5;
         } else{
            $rate3 = 0;
         }
         $star2 = ($rate3 * 30)/100;
      } else{
         $star2 =0;
      }
      
      $self = $this->User_Model->user_assessment($id);
      
      if($self) {
         $totalSelf = $self[0]['verbal'] + $self[0]['written'] + $self[0]['listening'] + $self[0]['problem'];
         $avgSelf = $totalSelf / 5;
         $star3 = ($avgSelf * 20)/100;
      } else{
         $star3 = 0;
      }

      $totalStar = $star + $star1 + $star2 + $star3;
      return $totalStar = number_format((float)$totalStar, 1, '.', '');
   }

   public function fetchEduSingleData($id) {
        $eduDatas = $this->User_Model->edu_single($id);
        if($eduDatas) {
            foreach ($eduDatas as $eduData) {
                $data[] = ["id"=> $eduData['id'], "attainment" => $eduData['attainment'], "degree" => $eduData['degree'], "university" => $eduData['university'], "from" => $eduData['degreeFrom'], "to" => $eduData['degreeTo']];
            }
            return $data;
        } else{
            $data = [];
            return $data;
        }
    }

    public function fetchWorkSingleData($id) {
        $workDatas = $this->User_Model->work_single($id);
        if($workDatas) {
            foreach ($workDatas as $workData) {
                $data[] = ["id"=> $workData['id'], "title" => $workData['title'], "company" => $workData['company'], "desc" => $workData['jobDesc'], "from" => $workData['workFrom'], "to" => $workData['workTo'], "category"=>$workData['category'], "categoryid"=>$workData['categoryid'], "subcategory"=> $workData['subcategory'],  "subcategoryid"=> $workData['subcatid'], "level"=> $workData['level'], "levelid"=> $workData['levelid'], "currently_working"=> $workData['currently_working']];
            }
            return $data;
        } else{
            $data = [];
            return $data;
        }
    }

    public function fetchWorkuniqueData($id) {
      $workDatas = $this->User_Model->work_unique($id);
        if($workDatas) {
            foreach ($workDatas as $workData) {
                $data[] = ["id"=> $workData['id'], "title" => $workData['title'], "company" => $workData['company'], "desc" => $workData['jobDesc'], "from" => $workData['workFrom'], "to" => $workData['workTo'], "category"=>$workData['category'], "categoryid"=>$workData['categoryid'], "subcategory"=> $workData['subcategory'],  "subcategoryid"=> $workData['subcatid'], "level"=> $workData['level'], "levelid"=> $workData['levelid'], "currently_working"=> $workData['currently_working']];
            }
            return $data;
        } else{
            $data = [];
            return $data;
        }
    }



}
?>
