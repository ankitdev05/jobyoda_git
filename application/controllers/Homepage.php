<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittall
 * @license         Mobulous
 */
class Homepage extends CI_Controller {
    
    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('newmodels/User_Model');
        $this->load->model('newmodels/recruiter/Jobpost_Model');
        $this->load->model('newmodels/recruiter/Newpoints_Model');
        $this->load->model('newmodels/recruiter/Homepage_Model');
        $this->load->model('newmodels/recruiter/Explorejobs_Model');
        $this->load->model('newmodels/Recruiteradmin_Model');
        $this->load->model('newmodels/Common_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');
        $this->load->library('google');
        $this->load->library('Facebook');
    }

    public function fetch_location(){
        $location =$this->input->post();
        $lat = (float)$location['currentLatitude'];
        $long = (float)$location['currentLongitude'];
        $locationdata = array(
                'current_lat'  => $lat,
                'current_long'     => $long
        );
        $this->session->set_userdata('locationdata',$locationdata);
        echo json_encode($location);
    }

    public function fetchcompany() {
        $searchTerm = $this->input->post('keyword');
        $search_array = array();
        $search_array1 = array();
        $search_array2 = array();
        $search_array3 = array();
        $search_array4 = array();

        if(!empty($searchTerm)) {

            if ($this->session->userdata('usersess')) {
                $userSess = $this->session->userdata('usersess');  
                $tokendata = ['id'=>$userSess['id']];
                $userID = $userSess['id'];

                $userData = $this->User_Model->token_match($tokendata);
                $expmonth = $userData[0]['exp_month'];
                $expyear = $userData[0]['exp_year'];

                $expmonth = $exp_month;
                $expyear = $exp_year;

                if($expyear == 0 && $expmonth == 0) {
                    $expfilter = "1";        
                } else if($expyear == 0 && $expmonth < 6) {
                    $expfilter = "2";
                } else if($expyear < 1 && $expmonth >= 6) {
                    $expfilter = "3";  
                } else if($expyear < 2 && $expyear >= 1) {
                    $expfilter = "4";
                } else if($expyear < 3 && $expyear >= 2) {
                    $expfilter = "5";
                } else if($expyear < 4 && $expyear >= 3) {
                    $expfilter = "6";  
                } else if($expyear < 5 && $expyear >= 4) {
                    $expfilter = "7";  
                } else if($expyear < 6 && $expyear >= 5) {
                    $expfilter = "8";  
                } else if($expyear < 7 && $expyear >= 6) {
                    $expfilter = "9";  
                } else if($expyear >= 7) { 
                    $expfilter = "10";
                } else {
                    $expfilter = "";
                }
            } else {
                $expfilter = "";
                $userID = 0;
            }
            $job_list = $this->Common_Model->home_search_job_recruiter($searchTerm);
            if (!empty($job_list)) {
                $search_array1 = $job_list;            
            }
            $job_list = $this->Common_Model->home_search_job_company($searchTerm);
            if (!empty($job_list)) {
                $search_array2 = $job_list;            
            }
            $job_list1 = $this->Common_Model->home_search_job_title($searchTerm, $expfilter, $userID);
            if (!empty($job_list1)) {
                $search_array3 = $job_list1;
            }
            $search_array4 = array_merge($search_array1, $search_array2);
            $search_array4 = array_merge($search_array3, $search_array4);
        
            if(count($search_array4) > 0) {
                foreach($search_array4 as $search_arr4) {

                  if(strlen($search_arr4["search_title"]) > 45) {
                    $serachTitle = substr($search_arr4["search_title"],0,45).'...';
                  } else {
                    $serachTitle = $search_arr4["search_title"];
                  }

                  $data['value'] = $search_arr4["search_title"];
                  $data['label'] = $serachTitle;
                  $data['label1'] = $search_arr4["jobcount"];
                  array_push($search_array, $data); 
                }
                echo json_encode($search_array);
                exit;
            
            } else {

                $break_string = explode(' ',$searchTerm);
                if(count($break_string) >= 3) {
                    $searchTerm1 = $break_string[0].' '.$break_string[1];
                
                } else if(count($break_string) == 2) {
                    $searchTerm1 = $break_string[0];
                } else {
                    $searchTerm1 = $searchTerm;
                }

                $job_list = $this->Common_Model->home_search_job_recruiter($searchTerm1);
                if (!empty($job_list)) {
                    $search_array1 = $job_list;            
                }
                $job_list = $this->Common_Model->home_search_job_company($searchTerm1);
                if (!empty($job_list)) {
                    $search_array2 = $job_list;            
                }
                $job_list1 = $this->Common_Model->home_search_job_title($searchTerm1, $expfilter, $userID);
                if (!empty($job_list1)) {
                    $search_array3 = $job_list1;
                }

                $search_array4 = array_merge($search_array1, $search_array2);
                $search_array4 = array_merge($search_array3, $search_array4);

                if(count($search_array4) > 0) {
                    $search_array['company_lists'] = $search_array4;
                    $response = $this->load->view('ajax_search_html', $search_array, TRUE);
                    echo $response;
                    exit;
                
                } else {

                    $break_string = explode(' ',$searchTerm);
                    if(count($break_string) >= 3) {
                        $searchTerm2 = $break_string[0];
                
                    } else if(count($break_string) == 2) {
                        $searchTerm2 = $break_string[0];
                    } else {
                        $searchTerm2 = $searchTerm;
                    }

                    $job_list = $this->Common_Model->home_search_job_recruiter($searchTerm2);
                    if (!empty($job_list)) {
                        $search_array1 = $job_list;            
                    }
                    $job_list = $this->Common_Model->home_search_job_company($searchTerm2);
                    if (!empty($job_list)) {
                        $search_array2 = $job_list;            
                    }
                    $job_list1 = $this->Common_Model->home_search_job_title($searchTerm2, $expfilter, $userID);
                    if (!empty($job_list1)) {
                        $search_array3 = $job_list1;
                    }
                    $search_array4 = array_merge($search_array1, $search_array2);
                    $search_array4 = array_merge($search_array3, $search_array4);
                    
                    if(count($search_array4) > 0) {
                        $search_array['company_lists'] = $search_array4;
                        $response = $this->load->view('ajax_search_html', $search_array, TRUE);
                        echo $response;
                        exit;
                    } else {
                        $response = $this->load->view('ajax_search_html', $search_array, TRUE);
                        echo $response;
                        exit;
                    }
                }
            }
        }

        $response = $this->load->view('ajax_search_html', $search_array, TRUE);
        echo $response;
        exit;
    }

    // Our Partners
    public function ourpartners() {

        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat = '14.5676285';
            $current_long = '120.7397551';
        }

        $ad_list = $this->Jobpost_Model->advertise_lists($current_lat,$current_long);
        if(!empty($ad_list)) {
            $x=0;
            foreach ($ad_list as $ad_lists) {
                    $ad_lists1[$x] = [
                          "ad_id" => $ad_lists['id'],
                          "banner" => $ad_lists['banner'],
                          "description" => $ad_lists['description'],
                          "recruiter_id" => $ad_lists['site_id'],
                          "profilePic" => $ad_lists['companyPic'],             
                          "companyName" =>   $ad_lists['cname']             

                      ];
            $x++;
            }
        }else{
            $ad_lists1 = [];
        }
        $data['ad_list'] = $ad_lists1;
        
        if ($this->session->userdata('usersess')) {
            $userSess = $this->session->userdata('usersess');  
            $tokendata = ['id'=>$userSess['id']];
            $userID = $userSess['id'];

            $userData = $this->User_Model->token_match($tokendata);
            $expmonth = $userData[0]['exp_month'];
            $expyear = $userData[0]['exp_year'];


            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";        
            } else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";  
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
            } else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";  
            } else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";  
            } else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";  
            } else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";  
            } else if($expyear >= 7) { 
                $expfilter = "10";
            } else {
                $expfilter = "";
            }
        } else {
            $expfilter = "";
            $userID = 0;
        }

        $data['meta_title'] = "Top BPO Companies Philippines | Jobyoda Partner Company Near You";
        $data['meta_description'] = "Be a part of the top BPO companies in the Philippines with JobyoDa, helping jobseekers find the best BPO jobs available today. Apply now!";

        $data["ourPartners"] = $this->getourpartners($userID, $expfilter, $current_lat, $current_long);
        $this->load->view('partners', $data);
    }

    // Top Partners list
    public function toppartners() {

        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat = '14.5676285';
            $current_long = '120.7397551';
        }

        $ad_list = $this->Jobpost_Model->advertise_lists($current_lat,$current_long);
        if(!empty($ad_list)) {
            $x=0;
            foreach ($ad_list as $ad_lists) {
                    $ad_lists1[$x] = [
                          "ad_id" => $ad_lists['id'],
                          "banner" => $ad_lists['banner'],
                          "description" => $ad_lists['description'],
                          "recruiter_id" => $ad_lists['site_id'],
                          "profilePic" => $ad_lists['companyPic'],             
                          "companyName" =>   $ad_lists['cname']             

                      ];
            $x++;
            }
        }else{
            $ad_lists1 = [];
        }
        $data['ad_list'] = $ad_lists1;
        
        if ($this->session->userdata('usersess')) {
            $userSess = $this->session->userdata('usersess');  
            $tokendata = ['id'=>$userSess['id']];
            $userID = $userSess['id'];

            $userData = $this->User_Model->token_match($tokendata);
            $expmonth = $userData[0]['exp_month'];
            $expyear = $userData[0]['exp_year'];


            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";        
            } else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";  
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
            } else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";  
            } else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";  
            } else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";  
            } else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";  
            } else if($expyear >= 7) { 
                $expfilter = "10";
            } else {
                $expfilter = "";
            }
        } else {
            $expfilter = "";
            $userID = 0;
        }

        $data['meta_title'] = "Top BPO Companies Philippines | Jobyoda Partner Company Near You";
        $data['meta_description'] = "Be a part of the top BPO companies in the Philippines with JobyoDa, helping jobseekers find the best BPO jobs available today. Apply now!";

        $data["ourPartners"] = $this->gettoppartners($userID, $expfilter, $current_lat, $current_long);
        $this->load->view('top_employer', $data);
    }

    // City listing
    public function citysearch() {

        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat = '14.5676285';
            $current_long = '120.7397551';
        }

        if ($this->session->userdata('usersess')) {
            $userSess = $this->session->userdata('usersess');  
            $tokendata = ['id'=>$userSess['id']];
            $userID = $userSess['id'];

            $userData = $this->User_Model->token_match($tokendata);
            $expmonth = (int)$userData[0]['exp_month'];
            $expyear = (int)$userData[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";        
            } else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";  
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
            } else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";  
            } else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";  
            } else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";  
            } else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";  
            } else if($expyear >= 7) { 
                $expfilter = "10";
            } else {
                $expfilter = "";
            }
        } else {
            $expfilter = "";
            $userID = 0;
        }        
        $data["citySearchs"] = $this->getcitysearch($userID, $expfilter, $current_lat, $current_long);
        $data['meta_title'] = "City Search BPO Jobs | Call Center Jobs near me | Jobyoda Location";
        $data['meta_description'] = "Find jobs based on location with JobYoDa ,helping jobseekers find the best BPO jobs available today. Apply now!";
        
        $this->load->view('citysearch', $data);
    }

    // Expertise listing (category)
    public function yourexpertise() {

        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat = '14.5676285';
            $current_long = '120.7397551';
        }

        if ($this->session->userdata('usersess')) {
            $userSess = $this->session->userdata('usersess');  
            $tokendata = ['id'=>$userSess['id']];
            $userID = $userSess['id'];

            $userData = $this->User_Model->token_match($tokendata);
            $expmonth = $userData[0]['exp_month'];
            $expyear = $userData[0]['exp_year'];

            $expmonth = $exp_month;
            $expyear = $exp_year;

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";        
            } else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";  
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
            } else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";  
            } else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";  
            } else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";  
            } else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";  
            } else if($expyear >= 7) { 
                $expfilter = "10";
            } else {
                $expfilter = "";
            }
        } else {
            $expfilter = "";
            $userID = 0;
        }        
        $data["expertises"] = $this->getexpertise($userID, $expfilter, $current_lat, $current_long);
        $data['meta_title'] = "Jobyoda Expertise | BPO Job Recruitment Expert | Jobyoda Philippines";
        $data['meta_description'] = "Find jobs based on your skills with JobYoDa ,helping jobseekers find the best BPO jobs available today. Apply now!";

        $this->load->view('expertise', $data);
    }

    // Secondary index page
    public function index() {
        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat = '14.5676285';
            $current_long = '120.7397551';
        }

        $company_list = $this->Common_Model->company_lists();
        if ($company_list) {
            $x = 0;
            foreach ($company_list as $company_lists) {
                $checkjob = $this->Common_Model->check_jobs($company_lists['id']);
                if ($checkjob) {
                    $company_lists1[$x] = ["id" => $company_lists['id'], "cname" => $company_lists['cname']];
                    $x++;
                }
            }
        } else {
            $company_lists1 = [];
        }
        $data['companynamelist'] = $company_lists1;
        $data['companylists'] = $company_list;

        $ad_list = $this->Jobpost_Model->advertise_lists($current_lat,$current_long);
        if(!empty($ad_list)) {
            $x=0;
            foreach ($ad_list as $ad_lists) {
                    $ad_lists1[$x] = [
                          "ad_id" => $ad_lists['id'],
                          "banner" => $ad_lists['banner'],
                          "description" => $ad_lists['description'],
                          "recruiter_id" => $ad_lists['site_id'],
                          "profilePic" => $ad_lists['companyPic'],             
                          "companyName" =>   $ad_lists['cname']
                      ];
            $x++;
            }
        }else{
            $ad_lists1 = [];
        }
        $data['ad_list'] = $ad_lists1;
        $data['level_list'] = $this->Common_Model->level_lists();
        $data['category_list'] = $this->Common_Model->category_lists();
        $data['jobtitle'] = $this->Common_Model->job_title_list();
        $data['userlist'] = $this->Common_Model->user_list();
        $data['popcat'] = $this->Common_Model->popular_category();
        $data['openings'] = $this->Jobpost_Model->job_openings();

        if ($this->session->userdata('usersess')) {
            $userSess = $this->session->userdata('usersess');  
            $tokendata = ['id'=>$userSess['id']];
            $userID = $userSess['id'];

            $userData = $this->User_Model->token_match($tokendata);
            
            $expmonth = (int)$userData[0]['exp_month'];
            $expyear = (int)$userData[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                //var_dump($expmonth, $expyear);die;    
                $expfilter = "1";        
            } else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";  
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
            } else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";  
            } else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";  
            } else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";  
            } else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";  
            } else if($expyear >= 7) { 
                $expfilter = "10";
            } else {
                $expfilter = "";
            }
        } else {
            $expfilter = "";
            $userID = 0;
        }


        $data["ourPartners"] = $this->getourpartnersLimit($userID, $expfilter, $current_lat, $current_long);
        $companyidsarray = array();
        foreach($data["ourPartners"] as $ourPartners) {
            $companyidsarray[] = $ourPartners['parent_id'];
        }
        $companyidsarray = array_unique($companyidsarray);

        $data["ourSubPartners"] = $this->getourpartnersSub($userID, $expfilter, $current_lat, $current_long);

        $data["citySearchs"] = $this->getcitysearch($userID, $expfilter, $current_lat, $current_long);
        $data["allcities"] = $this->getourpartners_city($userID, $expfilter, $current_lat, $current_long);

        $data["expertises"] = $this->getexpertise($userID, $expfilter, $current_lat, $current_long);
        
        $data["nearByJobs"] = $this->getnearByJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);
        $data["hotJobs"] = $this->gethotJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);
        $data['itjobs'] = $this->getitJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);
        $data['leadershipJobs'] = $this->getleadershipJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);
        $data['instant'] = $this->getinstantJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);

        $featuredTopPicks = $this->getFeaturedTopPicks($userID, $current_lat, $current_long, $expfilter, $companyidsarray);
        $data["bonusJobs"] = $featuredTopPicks['bonus'];
        $data["shiftJobs"] = $featuredTopPicks['shiftjobs'];
        $data["monthpayJobs"] = $featuredTopPicks['monthpay'];
        $data["bonusJobs"] = $featuredTopPicks['bonus'];
        $data["workfromhome"] = $featuredTopPicks['workfromhome'];
        $data["freefood"] = $featuredTopPicks['freefood'];
        $data["day1hmo"] = $featuredTopPicks['day1hmo'];
        
        $data['homemeta'] = $this->Recruiteradmin_Model->fetchCondRow('home_setting',array('id'=>1));

        $data['meta_title'] = $data['homemeta']->meta_tag;
        $data['meta_description'] = $data['homemeta']->meta_description;

        $this->load->view('homepage', $data);
    }

    // index page ajax for get category level etc on hide show popup
    public function fetchfilterhtml() {

        if($this->session->userdata('locationdata')) {
            $locationdata = $this->session->userdata('locationdata');
            
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat = '14.5676285';
            $current_long = '120.7397551';
        }

        if ($this->session->userdata('usersess')) {
            $userSess = $this->session->userdata('usersess');  
            $tokendata = ['id'=>$userSess['id']];
            $userID = $userSess['id'];

            $userData = $this->User_Model->token_match($tokendata);
            
            $expmonth = (int)$userData[0]['exp_month'];
            $expyear = (int)$userData[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                //var_dump($expmonth, $expyear);die;    
                $expfilter = "1";        
            } else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";  
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
            } else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";  
            } else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";  
            } else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";  
            } else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";  
            } else if($expyear >= 7) { 
                $expfilter = "10";
            } else {
                $expfilter = "";
            }
        } else {
            $expfilter = "";
            $userID = 0;
        }

        $data['category_list'] = $this->Common_Model->category_lists();
        $data['level_list'] = $this->Common_Model->level_lists();
        $data["ourSubPartners"] = $this->getourpartnersSub($userID, $expfilter, $current_lat, $current_long);
        $response = $this->load->view('homepage_filter', $data, TRUE);
        echo $response;
        exit;
    }

    // index page ajax for get city on hide show popup
    public function fetchfilterlocationhtml() {

        if($this->session->userdata('locationdata')) {
            $locationdata = $this->session->userdata('locationdata');
            
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat = '14.5676285';
            $current_long = '120.7397551';
        }

        if ($this->session->userdata('usersess')) {
            $userSess = $this->session->userdata('usersess');  
            $tokendata = ['id'=>$userSess['id']];
            $userID = $userSess['id'];

            $userData = $this->User_Model->token_match($tokendata);
            
            $expmonth = (int)$userData[0]['exp_month'];
            $expyear = (int)$userData[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                //var_dump($expmonth, $expyear);die;    
                $expfilter = "1";        
            } else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";  
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
            } else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";  
            } else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";  
            } else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";  
            } else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";  
            } else if($expyear >= 7) { 
                $expfilter = "10";
            } else {
                $expfilter = "";
            }
        } else {
            $expfilter = "";
            $userID = 0;
        }

        $data["citySearchs"] = $this->getcitysearch($userID, $expfilter, $current_lat, $current_long);
        $data["allcities"] = $this->getourpartners_city($userID, $expfilter, $current_lat, $current_long);
        $response = $this->load->view('homepage_location_filter', $data, TRUE);
        echo $response;
        exit;
    }

    // Main index page
    public function home_demo() {
        if($this->session->userdata('locationdata')) {
            $locationdata = $this->session->userdata('locationdata');
            
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat = '14.5676285';
            $current_long = '120.7397551';
        }

        $company_list = $this->Common_Model->company_lists();
        if ($company_list) {
            $x = 0;
            foreach ($company_list as $company_lists) {
                $checkjob = $this->Common_Model->check_jobs($company_lists['id']);
                if ($checkjob) {
                    $company_lists1[$x] = ["id" => $company_lists['id'], "cname" => $company_lists['cname']];
                    $x++;
                }
            }
        } else {
            $company_lists1 = [];
        }
        $data['companynamelist'] = $company_lists1;
        $data['companylists'] = $company_list;

        $ad_list = $this->Jobpost_Model->advertise_lists($current_lat,$current_long);
        if(!empty($ad_list)) {
            $x=0;
            foreach ($ad_list as $ad_lists) {
                    $ad_lists1[$x] = [
                          "ad_id" => $ad_lists['id'],
                          "banner" => $ad_lists['banner'],
                          "description" => $ad_lists['description'],
                          "recruiter_id" => $ad_lists['site_id'],
                          "profilePic" => $ad_lists['companyPic'],             
                          "companyName" =>   $ad_lists['cname']
                      ];
            $x++;
            }
        }else{
            $ad_lists1 = [];
        }
        $data['ad_list'] = $ad_lists1;
        $data['level_list'] = $this->Common_Model->level_lists();
        $data['category_list'] = $this->Common_Model->category_lists();
        $data['jobtitle'] = $this->Common_Model->job_title_list();
        $data['userlist'] = $this->Common_Model->user_list();
        $data['popcat'] = $this->Common_Model->popular_category();
        $data['openings'] = $this->Jobpost_Model->job_openings();

        if ($this->session->userdata('usersess')) {
            $userSess = $this->session->userdata('usersess');  
            $tokendata = ['id'=>$userSess['id']];
            $userID = $userSess['id'];

            $userData = $this->User_Model->token_match($tokendata);
            
            $expmonth = (int)$userData[0]['exp_month'];
            $expyear = (int)$userData[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                //var_dump($expmonth, $expyear);die;    
                $expfilter = "1";        
            } else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";  
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
            } else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";  
            } else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";  
            } else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";  
            } else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";  
            } else if($expyear >= 7) { 
                $expfilter = "10";
            } else {
                $expfilter = "";
            }
        } else {
            $expfilter = "";
            $userID = 0;
        }


        $data["ourPartners"] = $this->getourpartners($userID, $expfilter, $current_lat, $current_long);
        $companyidsarray = array();
        foreach($data["ourPartners"] as $ourPartners) {
            $companyidsarray[] = $ourPartners['parent_id'];
        }
        $companyidsarray = array_unique($companyidsarray);

        $data["ourSubPartners"] = $this->getourpartnersSub($userID, $expfilter, $current_lat, $current_long);
        $data["allcities"] = $this->getourpartners_city($userID, $expfilter, $current_lat, $current_long);

        $data["topPartners"] = $this->gettoppartners($userID, $expfilter, $current_lat, $current_long);
        //var_dump($data["topPartners"]);die;

        $data["citySearchs"] = $this->getcitysearch($userID, $expfilter, $current_lat, $current_long);
        $data["expertises"] = $this->getexpertise($userID, $expfilter, $current_lat, $current_long);
        
        $data["nearByJobs"] = $this->getnearByJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);
        $data["hotJobs"] = $this->gethotJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);
        $data['itjobs'] = $this->getitJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);
        $data['leadershipJobs'] = $this->getleadershipJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);
        $data['instant'] = $this->getinstantJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);

        $data['noexp'] = $this->getnoexpJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);
        $data['actively'] = $this->getactivelyJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);
        $data['nursing'] = $this->getnursingcountJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);

        $featuredTopPicks = $this->getFeaturedTopPicks($userID, $current_lat, $current_long, $expfilter, $companyidsarray);
        $data["bonusJobs"] = $featuredTopPicks['bonus'];
        $data["shiftJobs"] = $featuredTopPicks['shiftjobs'];
        $data["monthpayJobs"] = $featuredTopPicks['monthpay'];
        $data["bonusJobs"] = $featuredTopPicks['bonus'];
        $data["workfromhome"] = $featuredTopPicks['workfromhome'];
        $data["freefood"] = $featuredTopPicks['freefood'];
        $data["day1hmo"] = $featuredTopPicks['day1hmo'];
        
        $data['homemeta'] = $this->Recruiteradmin_Model->fetchCondRow('home_setting',array('id'=>1));

        $data['meta_title'] = $data['homemeta']->meta_tag;
        $data['meta_description'] = $data['homemeta']->meta_description;

        $this->load->view('homepage_demo', $data);
    }

    // Dummy
    public function index11() {
        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat = '14.5676285';
            $current_long = '120.7397551';
        }

        $company_list = $this->Common_Model->company_lists();
        if ($company_list) {
            $x = 0;
            foreach ($company_list as $company_lists) {
                $checkjob = $this->Common_Model->check_jobs($company_lists['id']);
                if ($checkjob) {
                    $company_lists1[$x] = ["id" => $company_lists['id'], "cname" => $company_lists['cname']];
                    $x++;
                }
            }
        } else {
            $company_lists1 = [];
        }
        $data['companynamelist'] = $company_lists1;
        $data['companylists'] = $company_list;

        $ad_list = $this->Jobpost_Model->advertise_lists($current_lat,$current_long);
        if(!empty($ad_list)) {
            $x=0;
            foreach ($ad_list as $ad_lists) {
                    $ad_lists1[$x] = [
                          "ad_id" => $ad_lists['id'],
                          "banner" => $ad_lists['banner'],
                          "description" => $ad_lists['description'],
                          "recruiter_id" => $ad_lists['site_id'],
                          "profilePic" => $ad_lists['companyPic'],             
                          "companyName" =>   $ad_lists['cname']
                      ];
            $x++;
            }
        }else{
            $ad_lists1 = [];
        }
        $data['ad_list'] = $ad_lists1;
        $data['level_list'] = $this->Common_Model->level_lists();
        $data['category_list'] = $this->Common_Model->category_lists();
        $data['jobtitle'] = $this->Common_Model->job_title_list();
        $data['userlist'] = $this->Common_Model->user_list();
        $data['popcat'] = $this->Common_Model->popular_category();
        $data['openings'] = $this->Jobpost_Model->job_openings();

        if ($this->session->userdata('usersess')) {
            $userSess = $this->session->userdata('usersess');  
            $tokendata = ['id'=>$userSess['id']];
            $userID = $userSess['id'];

            $userData = $this->User_Model->token_match($tokendata);
            
            $expmonth = (int)$userData[0]['exp_month'];
            $expyear = (int)$userData[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                //var_dump($expmonth, $expyear);die;    
                $expfilter = "1";        
            } else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";  
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
            } else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";  
            } else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";  
            } else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";  
            } else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";  
            } else if($expyear >= 7) { 
                $expfilter = "10";
            } else {
                $expfilter = "";
            }
        } else {
            $expfilter = "";
            $userID = 0;
        }


        $data["ourPartners"] = $this->getourpartners($userID, $expfilter, $current_lat, $current_long);
        $companyidsarray = array();
        foreach($data["ourPartners"] as $ourPartners) {
            $companyidsarray[] = $ourPartners['parent_id'];
        }
        $companyidsarray = array_unique($companyidsarray);

        $data["ourSubPartners"] = $this->getourpartnersSub($userID, $expfilter, $current_lat, $current_long);
        $data["allcities"] = $this->getourpartners_city($userID, $expfilter, $current_lat, $current_long);

        $data["topPartners"] = $this->gettoppartners($userID, $expfilter, $current_lat, $current_long);

        $data["citySearchs"] = $this->getcitysearch($userID, $expfilter, $current_lat, $current_long);
        $data["expertises"] = $this->getexpertise($userID, $expfilter, $current_lat, $current_long);
        
        $data["nearByJobs"] = $this->getnearByJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);
        $data["hotJobs"] = $this->gethotJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);
        $data['itjobs'] = $this->getitJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);
        $data['leadershipJobs'] = $this->getleadershipJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);
        $data['instant'] = $this->getinstantJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);

        $data['noexp'] = $this->noexp_count($userID, $expfilter, $current_lat, $current_long, $companyidsarray);
        $data['actively'] = $this->actively_count($userID, $expfilter, $current_lat, $current_long, $companyidsarray);
        $data['nursing'] = $this->nursing_count($userID, $expfilter, $current_lat, $current_long, $companyidsarray);

        $featuredTopPicks = $this->getFeaturedTopPicks($userID, $current_lat, $current_long, $expfilter, $companyidsarray);
        $data["bonusJobs"] = $featuredTopPicks['bonus'];
        $data["shiftJobs"] = $featuredTopPicks['shiftjobs'];
        $data["monthpayJobs"] = $featuredTopPicks['monthpay'];
        $data["bonusJobs"] = $featuredTopPicks['bonus'];
        $data["workfromhome"] = $featuredTopPicks['workfromhome'];
        $data["freefood"] = $featuredTopPicks['freefood'];
        $data["day1hmo"] = $featuredTopPicks['day1hmo'];
        
        $data['homemeta'] = $this->Recruiteradmin_Model->fetchCondRow('home_setting',array('id'=>1));

        $data['meta_title'] = $data['homemeta']->meta_tag;
        $data['meta_description'] = $data['homemeta']->meta_description;

        $this->load->view('homepage', $data);
    }

    // explore more jobs
    public function explorejobs() {
        if($this->session->userdata('locationdata')) {
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        } else {
            $current_lat = '14.5676285';
            $current_long = '120.7397551';
        }

        $data['category_list'] = $this->Common_Model->category_lists();
        $data['popcat'] = $this->Common_Model->popular_category();

        if ($this->session->userdata('usersess')) {
            $userSess = $this->session->userdata('usersess');  
            $tokendata = ['id'=>$userSess['id']];
            $userID = $userSess['id'];

            $userData = $this->User_Model->token_match($tokendata);
            
            $expmonth = (int)$userData[0]['exp_month'];
            $expyear = (int)$userData[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                //var_dump($expmonth, $expyear);die;    
                $expfilter = "1";        
            } else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";  
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
            } else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";  
            } else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";  
            } else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";  
            } else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";  
            } else if($expyear >= 7) { 
                $expfilter = "10";
            } else {
                $expfilter = "";
            }
        } else {
            $expfilter = "";
            $userID = 0;
        }
        $data["ourPartners"] = $this->getourpartners($userID, $expfilter, $current_lat, $current_long);
        $companyidsarray = array();
        foreach($data["ourPartners"] as $ourPartners) {
            $companyidsarray[] = $ourPartners['parent_id'];
        }
        $companyidsarray = array_unique($companyidsarray);

        $data["hotJobs"] = $this->getactivelyhiringJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);
        $data['meta_title'] = "BPO Jobs Philippines | Call Center Jobs | Jobyoda Dream Jobs Site";
        $data['meta_description'] = "Find work from home, part time jobs online, and full time BPO and call center positions at JobYoDa. See jobs near you and apply for your dream job today!";

        $this->load->view('explore_jobs', $data);
    }

    // video listing
    public function videos() {

        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat = '14.5676285';
            $current_long = '120.7397551';
        }
        //Fetch videos
        $videosArray = array();
        $fetchvideos = $this->Newpoints_Model->fetchvideos();
        foreach($fetchvideos as $fetchvideo) {
            $videosArray[] = ["id"=>$fetchvideo['id'],"video"=>$fetchvideo['video'],"banner"=>$fetchvideo['thumbnail'],"title"=>$fetchvideo['title'],"desc"=>$fetchvideo['description'],"posted"=>date("d F Y", strtotime($fetchvideo['updated_at']))];
        }
        $data["videoListings"] = $videosArray;
        $data['videometa'] = $this->Recruiteradmin_Model->fetchCondRow('setting',array('id'=>1));
        $data['meta_title'] = $data['videometa']->video_meta_tag;
        $data['meta_description'] = $data['videometa']->video_meta_description;

        $this->load->view('videos_jobyoda', $data);
    }

    // Fetch explore more tabs ajax
    public function fetchpopularsearch() {

        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat = '14.5676285';
            $current_long = '120.7397551';
        }

        $data = $this->input->post();
        $result = array();

        if ($this->session->userdata('usersess')) {
            $userSess = $this->session->userdata('usersess');  
            $tokendata = ['id'=>$userSess['id']];
            $userID = $userSess['id'];

            $userData = $this->User_Model->token_match($tokendata);
            
            $expmonth = (int)$userData[0]['exp_month'];
            $expyear = (int)$userData[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                //var_dump($expmonth, $expyear);die;    
                $expfilter = "1";        
            } else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";  
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
            } else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";  
            } else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";  
            } else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";  
            } else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";  
            } else if($expyear >= 7) { 
                $expfilter = "10";
            } else {
                $expfilter = "";
            }
        } else {
            $expfilter = "";
            $userID = 0;
        }

        $data["ourPartners"] = $this->getourpartners($userID, $expfilter, $current_lat, $current_long);
        $companyidsarray = array();
        foreach($data["ourPartners"] as $ourPartners) {
            $companyidsarray[] = $ourPartners['parent_id'];
        }
        $companyidsarray = array_unique($companyidsarray);

        $featuredTopPicks = $this->getExploreJobsTopPicks($userID, $current_lat, $current_long, $expfilter, $companyidsarray);

        if($data['keyword'] == 2) {
            
            $result['popular'] = $featuredTopPicks['workfromhome'];

        } else if($data['keyword'] == 3) {

            $result['popular'] = $this->getexloreinstantJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);
        
        } else if($data['keyword'] == 4) {

            $result['popular'] = $featuredTopPicks['day1hmo'];
        
        } else if($data['keyword'] == 5) {

            $result['popular'] = $featuredTopPicks['freefood'];
        
        } else if($data['keyword'] == 6) {

            $result['popular'] = $this->getexploreitJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);
        
        } else if($data['keyword'] == 7) {

            $result['popular'] = $this->getexploreleadershipJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);
        
        } else if($data['keyword'] == 8) {

            $result['popular'] = $featuredTopPicks['monthpay'];
        
        } else if($data['keyword'] == 1) {

            $result['popular'] = $this->getexplorehotJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);
        
        } else if($data['keyword'] == 9) {

            $result['popular'] = $this->getexplorenearByJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);
        
        } else if($data['keyword'] == 10) {

            $result['popular'] = $featuredTopPicks['bonus'];
        
        } else if($data['keyword'] == 11) {

            $result['popular'] = $featuredTopPicks['shiftjobs'];
        
        } else if($data['keyword'] == 12) {

            $result['popular'] = $featuredTopPicks['day1hmodependent'];
        
        } else if($data['keyword'] == 13) {

            $result['popular'] = $this->getexploreNoExpJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);
        
        } else if($data['keyword'] == 14) {

            $result['popular'] = $this->getnursingJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);
        
        } else if($data['keyword'] == 15) {

            $result['popular'] = $this->getactivelyhiringJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);
        }

        
        $result['extra'] = ["keyword"=>$data['keyword']]; 
        
        $response = $this->load->view('homepage_popular', $result, TRUE);
        
        echo $response;
        exit;
    }

    // Common Functions
    public function getourpartners_city($userID, $expfilter, $current_lat, $current_long) {

        $company_lists = $this->Newpoints_Model->companysite_groupby_city();
        $companylist = array();

        if(!empty($company_lists)) {
            $x=0;

            foreach ($company_lists as $company_list) {

                $jobcountfetch = $this->Homepage_Model->companysite_jobcount($userID, $company_list['id'], $expfilter, $current_lat, $current_long);
                if($jobcountfetch) {
                    $jobcount = count($jobcountfetch);
                    if($jobcount > 0) {
                        $companylist[] = $company_list['city'];
                    }
                    $x++;
                }
            }
            $companylist = array_unique($companylist);
        } else {
            $companylist = [];
        }

        return $companylist;
    }
    public function getourpartnersLimit($userID, $expfilter, $current_lat, $current_long) {

        $company_lists = $this->Newpoints_Model->companysite_lists($current_lat, $current_long);

        if(!empty($company_lists)) {
            $x=0;

            foreach ($company_lists as $company_list) {

                $parent = $this->Newpoints_Model->company_parent($company_list['parent_id']);

                $jobcountfetch = $this->Homepage_Model->companysite_jobcount($userID, $company_list['id'], $expfilter, $current_lat, $current_long);
                if($jobcountfetch) {

                    if($x <=10) {

                        $jobcount = count($jobcountfetch);
                        $total_oppurnity=0;
                        foreach($jobcountfetch as $jobcountf) {
                            $total_oppurnity = $total_oppurnity + $jobcountf['opening'];
                        }
                        
                        if($jobcount > 0) {

                            if(strlen($company_list['companyPic']) > 0) {
                                $comapnyPic = $company_list['companyPic'];
                            } else {
                                $parentPic = $this->Jobpost_Model->fetch_companyPic($company_list['parent_id']);

                                $comapnyPic = $parentPic[0]['companyPic'];
                            }
                            
                            $companylist[$x] = [
                                  "id" => $company_list['id'],
                                  "image" => $comapnyPic,
                                  "cname" => $parent[0]['cname'].', '.$company_list['cname'],
                                  "jobcount" => $jobcount,
                                  "openingcount" => $total_oppurnity,
                                  "parent_id"=>$company_list['parent_id']
                            ];
                        
                        }
                    }
                    $x++;
                }
            }
        } else {
            $companylist = [];
        }

        $jobcount = array();
        foreach ($companylist as $key => $row)
        {
            $jobcount[$key] = $row['jobcount'];
        }
        array_multisort($jobcount, SORT_DESC, $companylist);

        return $companylist;
    }
    public function getourpartners($userID, $expfilter, $current_lat, $current_long) {

        $company_lists = $this->Newpoints_Model->companysite_lists($current_lat, $current_long);

        if(!empty($company_lists)) {
            $x=0;

            foreach ($company_lists as $company_list) {
                $parent = $this->Newpoints_Model->company_parent($company_list['parent_id']);

                $jobcountfetch = $this->Homepage_Model->companysite_jobcount($userID, $company_list['id'], $expfilter, $current_lat, $current_long);
                if($jobcountfetch) {

                    $jobcount = count($jobcountfetch);
                    $total_oppurnity=0;
                    foreach($jobcountfetch as $jobcountf) {
                        $total_oppurnity = $total_oppurnity + $jobcountf['opening'];
                    }
                    
                    if($jobcount > 0) {

                        if(strlen($company_list['companyPic']) > 0) {
                            $comapnyPic = $company_list['companyPic'];
                        } else {
                            $parentPic = $this->Jobpost_Model->fetch_companyPic($company_list['parent_id']);

                            $comapnyPic = $parentPic[0]['companyPic'];
                        }

                        $companylist[$x] = [
                              "id" => $company_list['id'],
                              "image" => $comapnyPic,
                              "cname" => $parent[0]['cname'].', '.$company_list['cname'],
                              "jobcount" => $jobcount,
                              "openingcount" => $total_oppurnity,
                              "parent_id"=>$company_list['parent_id'],
                        ];
                    
                    }
                    $x++;
                }
            }
        } else {
            $companylist = [];
        }

        $jobcount = array();
        foreach ($companylist as $key => $row)
        {
            $jobcount[$key] = $row['jobcount'];
        }
        array_multisort($jobcount, SORT_DESC, $companylist);

        return $companylist;
    }
    public function gettoppartners($userID, $expfilter, $current_lat, $current_long) {

        $company_lists = $this->Newpoints_Model->mainrecruiter_lists($current_lat, $current_long);

        if(!empty($company_lists)) {
            $x=0;

            foreach ($company_lists as $company_list) {

                if($company_list['top_recruiter'] > 0) {

                    if(strlen($company_list['companyPic']) > 0) {
                        $comapnyPic = $company_list['companyPic'];
                    }  else {
                        $comapnyPic = "";
                    }

                    $getTopPicks = $this->Newpoints_Model->company_parent_toppicks($company_list['id']);
                    if($getTopPicks) {
                        $toppics_first = $getTopPicks[0]['picks_id'];
                        $toppics_second = $getTopPicks[1]['picks_id'];
                    } else {
                        $toppics_first = 0;
                        $toppics_second = 0;
                    }
                    if(strlen($company_list['headquarter']) > 0) {
                        $parent_headquater = $company_list['headquarter'];
                    } else {
                        $parent_headquater = "";
                    }
                    if(strlen($company_list['founded']) > 0) {
                        $parent_founded = $company_list['founded'];
                    } else {
                        $parent_founded = "";
                    }
                    if(strlen($company_list['size']) > 0) {
                        $parent_size = $company_list['size'];
                    } else {
                        $parent_size = "";
                    }
                    if(strlen($company_list['num_sites']) > 0) {
                        $parent_num_sites = $company_list['num_sites'];
                    } else {
                        $parent_num_sites = "";
                    }
                    
                    $companylist[$x] = [
                                          "id" => $company_list['id'],
                                          "image" => $comapnyPic,
                                          "cname" => $company_list['cname'],
                                          "top_Recruiter" => $company_list['top_recruiter'],
                                          "headquater" => $parent_headquater,
                                          "founded" => $parent_founded,
                                          "size" => $parent_size,
                                          "num_sites" => $parent_num_sites,
                                          "toppics_first" => $toppics_first,
                                          "toppics_second" => $toppics_second,
                                    ];
                    $x++;
                }

            }
        } else {
            $companylist = [];
        }

        $jobcount = array();
        foreach ($companylist as $key => $row) {

            $jobcount[$key] = $row['top_recruiter'];
        }
        array_multisort($jobcount, SORT_DESC, $companylist);


        return $companylist;
    }
    public function getourpartnersSub($userID, $expfilter, $current_lat, $current_long) {

        $company_lists = $this->Newpoints_Model->companysite_lists($current_lat, $current_long);

        if(!empty($company_lists)) {
            $x=0;

            foreach ($company_lists as $company_list) {
                $parent = $this->Newpoints_Model->company_parent($company_list['parent_id']);

                $jobcountfetch = $this->Newpoints_Model->companysite_jobcount($userID, $company_list['id'], $expfilter, $current_lat, $current_long);
                if($jobcountfetch > 0) {
                    
                    if(strlen($company_list['companyPic']) > 0) {
                        
                        $companylist[$x] = [
                              "id" => $company_list['id'],
                              "image" => $company_list['companyPic'],
                              "cname" => $company_list['cname'],
                              "jobcount" => $jobcountfetch,
                              "parent_id"=>$company_list['parent_id']
                        ];
                    
                    } else {

                        $siteimgs = $this->Jobpost_Model->fetch_companyPic($company_list['parent_id']);
                        if($siteimgs) {
                            $companylist[$x] = [
                                  "id" => $company_list['id'],
                                  "image" => $siteimgs[0]['companyPic'],
                                  "cname" => $company_list['cname'],
                                  "jobcount" => $jobcountfetch,
                                  "parent_id"=>$company_list['parent_id']
                            ];
                        }
                    }
                    $x++;
                }
            }
        } else {
            $companylist = [];
        }

        $jobcount = array();
        foreach ($companylist as $key => $row)
        {
            $jobcount[$key] = $row['jobcount'];
        }
        array_multisort($jobcount, SORT_DESC, $companylist);

        return $companylist;
    }
    public function getcitysearch($userID, $expfilter, $current_lat, $current_long) {

        $city_lists = $this->Jobpost_Model->homecity_lists();
        if(!empty($city_lists)) {

            $c=0;
            $citylist = array();

            if ($this->session->userdata('usersess')) {
                $userSess = $this->session->userdata('usersess');  
                $tokendata = ['id'=>$userSess['id']];
                $userData = $this->User_Model->token_match($tokendata);
                if($userData) {
                    $usercity = $userData[0]['city'];
                } else {
                    $usercity = "";
                }
            } else {
                $usercity = "";
            }
            
            foreach ($city_lists as $city_list) {
                
                if(strpos($usercity, $city_list['name']) !== false && strlen($usercity) > 0) {
                    if($city_list['name'] == "Metro Manila") {
                        $city_list['name'] = "Manila";
                    }

                    $jobcountfetch = $this->Homepage_Model->city_jobcount($userID, $city_list['name'], $expfilter, $current_lat, $current_long);
                    if($jobcountfetch) {

                        $jobcount = count($jobcountfetch);
                        $total_oppurnity=0;
                        foreach($jobcountfetch as $jobcountf) {
                            $total_oppurnity = $total_oppurnity + $jobcountf['opening'];
                        }

                        if($city_list['name'] == "Manila") {
                            $city_list['name'] = "Metro Manila";
                        }
                        $citylist[$c] = [
                              "id" => $city_list['id'],
                              "cityname" => $city_list['name'],
                              "cityslug" => $city_list['slug'],
                              "image" => base_url().$city_list['image'],
                              "thumbnail" => base_url().$city_list['thumbnail'],
                              "jobcount" => $jobcount,
                              "openingcount" => $total_oppurnity,
                        ];
                        $c++;
                    }
                }
            }
            if(count($citylist) > 0) {
                $cc = $c;
            } else {
                $cc = 0;
            }
            foreach ($city_lists as $city_list) {

                if(strpos($usercity, $city_list['name']) !== false && strlen($usercity) > 0) {
                } else {
                    if($city_list['name'] == "Metro Manila") {
                        $city_list['name'] = "Manila";
                    }
                    $jobcountfetch = $this->Homepage_Model->city_jobcount($userID, $city_list['name'], $expfilter, $current_lat, $current_long);
                    if($jobcountfetch) {

                        $jobcount = count($jobcountfetch);
                        $total_oppurnity=0;
                        foreach($jobcountfetch as $jobcountf) {
                            $total_oppurnity = $total_oppurnity + $jobcountf['opening'];
                        }

                        if($city_list['name'] == "Manila") {
                            $city_list['name'] = "Metro Manila";
                        }
                        $citylist[$cc] = [
                              "id" => $city_list['id'],
                              "cityname" => $city_list['name'],
                              "cityslug" => $city_list['slug'],
                              "image" => base_url().$city_list['image'],
                              "thumbnail" => base_url().$city_list['thumbnail'],
                              "jobcount" => $jobcount,
                              "openingcount" => $total_oppurnity,
                        ];
                        $cc++;
                    }
                }
            }
        } else {
            $citylist = [];
        }

        return $citylist;
    }
    public function getexpertise($userID, $expfilter, $current_lat, $current_long) {

        $cat_lists = $this->Jobpost_Model->category_lists();

        if(!empty($cat_lists)) {
            $x=0;

            foreach ($cat_lists as $cat_list) {
                $jobcountfetch = $this->Homepage_Model->category_jobcount($userID, $cat_list['id'], $expfilter, $current_lat, $current_long);
                
                $jobcount = count($jobcountfetch);
                $total_oppurnity=0;
                foreach($jobcountfetch as $jobcountf) {
                    $total_oppurnity = $total_oppurnity + $jobcountf['opening'];
                }
                
                $categorylist[$x] = [
                      "id" => $cat_list['id'],
                      "catname" => $cat_list['category'],
                      "jobcount" => $jobcount,
                      "openingcount" => $total_oppurnity,
                ];
                $x++;
            }
        } else {
            $categorylist = [];
        }
        return $categorylist;
    }
    public function getnearByJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray) {
        $data1 = array();
        $jobfetchs = $this->Homepage_Model->home_nearby_count($userID, $current_lat, $current_long, $expfilter);
        //var_dump($jobfetchs);die;
        if ($jobfetchs) {

            $cDate = date("Y-m-d");
            $jobCount=1;
            $openingCount=0;
            foreach($jobfetchs as $jobfetch) {

                if(strtotime($jobfetch['jobexpire']) >= strtotime($cDate)) {

                    $jobCount++;
                    $openingCount = $openingCount + $jobfetch['opening'];
                }
            }
            return $data1 = ["jobcount"=>$jobCount-1, "openingcount"=>$openingCount];

        } else{
            $data1=["jobcount"=>0, "openingcount"=>0];
            return $data1;
        }
    }
    public function gethotJobs($userID, $expfilter, $userLat, $userLong, $companyidsarray) {

        $cDate = date("Y-m-d");
        $jobCount=1;
        $openingCount=0;
        $neglectarr = 0;
        $filter_values = [
            "cname" => "",
            "location" => "",
            "lat" => "",
            "long" => "",
            "getcity" => "",
            "jobcategory" => "",
            "joblevel" => "",
            "sorting" => "",
            "basicSalary" => "",
            "toppicks" => "",
            "allowances" => "",
            "medical" => "",
            "workshift" => "",
            "leaves" => "",
        ];

        $hotjobData = $this->Homepage_Model->hotjob_count_groupby($userID, $expfilter, $userLat, $userLong, $filter_values);
        foreach ($hotjobData as $hotjobsData) {
            
            if(strtotime($hotjobsData['jobexpire']) >= strtotime($cDate)) {
                $neglectarr[] = $hotjobsData['id'];
                $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                if(!empty($jobTop) && count($jobTop)>=1) {

                    $jobCount++;
                    $openingCount = $openingCount + $hotjobsData['opening'];
                }
            }
        }

        return $data1 = ["jobcount"=>$jobCount, "openingcount"=>$openingCount];
    }
    public function getitJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray) {

        $itcatjobDatas = $this->Homepage_Model->information_technology_count($userID, $expfilter, $current_lat, $current_long);
        if ($itcatjobDatas) {
            $cDate = date("Y-m-d");
            $jobCount=1;
            $openingCount=0;
            foreach($itcatjobDatas as $itcatjobData) {

                if(strtotime($itcatjobData['jobexpire']) >= strtotime($cDate)) {

                    $jobCount++;
                    $openingCount = $openingCount + $itcatjobData['opening'];
                }
            }
            //var_dump($jobCount);die;
            return $data1 = ["jobcount"=>$jobCount, "openingcount"=>$openingCount];

        } else{
            $data1=["jobcount"=>0, "openingcount"=>0];
            return $data1;
        }
    }
    public function getleadershipJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray) {

        $catjobDatas = $this->Homepage_Model->leadership_count($userID, $expfilter, $current_lat, $current_long);
        if ($catjobDatas) {
            $cDate = date("Y-m-d");
            $jobCount=1;
            $openingCount=0;
            foreach($catjobDatas as $catjobData) {

                if(strtotime($catjobData['jobexpire']) >= strtotime($cDate)) {

                    $jobCount++;
                    $openingCount = $openingCount + $catjobData['opening'];
                }
            }
            return $data1 = ["jobcount"=>$jobCount, "openingcount"=>$openingCount];

        } else {

            $data1=["jobcount"=>0, "openingcount"=>0];
            return $data1;
        }
    }

    public function getinstantJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray) {

        $catjobDatas = $this->Homepage_Model->instantscreening_count($userID, $expfilter, $current_lat, $current_long);
        if ($catjobDatas) {

            $cDate = date("Y-m-d");
            $jobCount=1;
            $openingCount=0;
            foreach($catjobDatas as $catjobData) {

                if(strtotime($catjobData['jobexpire']) >= strtotime($cDate)) {

                    $jobCount++;
                    $openingCount = $openingCount + $catjobData['opening'];
                }
            }
            $data1 = ["jobcount"=>$jobCount, "openingcount"=>$openingCount];
            return $data1;
        } else{
            $data1=["jobcount"=>0, "openingcount"=>0];
            return $data1;
        }
    }

    public function getnoexpJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray) {

        $catjobDatas = $this->Homepage_Model->noexp_count($userID, $expfilter, $current_lat, $current_long);
        if ($catjobDatas) {

            $cDate = date("Y-m-d");
            $jobCount=0;
            $openingCount=0;
            foreach($catjobDatas as $catjobData) {

                if(strtotime($catjobData['jobexpire']) >= strtotime($cDate)) {

                    $jobCount++;
                    $openingCount = $openingCount + $catjobData['opening'];
                }
            }
            $data1 = ["jobcount"=>$jobCount, "openingcount"=>$openingCount];
            return $data1;
        } else{
            $data1=["jobcount"=>0, "openingcount"=>0];
            return $data1;
        }
    }

    public function getactivelyJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray) {

        $catjobDatas = $this->Homepage_Model->actively_count($userID, $expfilter, $current_lat, $current_long);
        if ($catjobDatas) {

            $cDate = date("Y-m-d");
            $jobCount=0;
            $openingCount=0;
            foreach($catjobDatas as $catjobData) {

                if(strtotime($catjobData['jobexpire']) >= strtotime($cDate)) {
                    $jobCount++;
                    $openingCount = $openingCount + $catjobData['opening'];
                }
            }
            $data1 = ["jobcount"=>$jobCount, "openingcount"=>$openingCount];
            return $data1;
        } else{
            $data1=["jobcount"=>0, "openingcount"=>0];
            return $data1;
        }
    }

    public function getnursingcountJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray) {

        $catjobDatas = $this->Homepage_Model->nursing_count($userID, $expfilter, $current_lat, $current_long);
        
        if ($catjobDatas) {

            $cDate = date("Y-m-d");
            $jobCount=0;
            $openingCount=0;
            foreach($catjobDatas as $catjobData) {

                if(strtotime($catjobData['jobexpire']) >= strtotime($cDate)) {

                    $jobCount++;
                    $openingCount = $openingCount + $catjobData['opening'];
                }
            }
            $data1 = ["jobcount"=>$jobCount, "openingcount"=>$openingCount];
            return $data1;
        } else{
            $data1=["jobcount"=>0, "openingcount"=>0];
            return $data1;
        }
    }

    public function getFeaturedTopPicks($uid, $current_lat, $current_long, $expfilter, $companyidsarray) {

        $dataTopicks["monthpay"] = array();
        $dataTopicks["shiftjobs"] = array();
        $dataTopicks["retirement"] = array();
        $dataTopicks["day1hmo"] = array();
        $dataTopicks["freefood"] = array();
        $dataTopicks["bonus"] = array();
        $dataTopicks["workfromhome"] = array();

        $hotjobDataone = $this->Homepage_Model->feturedtoppicks_bonus_count($uid, $expfilter, $current_lat, $current_long);
        if ($hotjobDataone) {
            $cDate = date("Y-m-d");
            $jobCount=1;
            $openingCount=0;
            foreach($hotjobDataone as $jobfetch) {

                if(strtotime($jobfetch['jobexpire']) >= strtotime($cDate)) {

                    $jobCount++;
                    $openingCount = $openingCount + $jobfetch['opening'];
                }
            }
            $dataTopicks["bonus"] = ["jobcount"=>$jobCount, "openingcount"=>$openingCount];
        } else {
            $dataTopicks["bonus"] = ["jobcount"=>0, "openingcount"=>0];
        }

        $hotjobDataone = $this->Homepage_Model->feturedtoppicks_freefood_count($uid, $expfilter, $current_lat, $current_long);
        if ($hotjobDataone) {

            $cDate = date("Y-m-d");
            $jobCount=1;
            $openingCount=0;
            foreach($hotjobDataone as $jobfetch) {

                if(strtotime($jobfetch['jobexpire']) >= strtotime($cDate)) {

                    $jobCount++;
                    $openingCount = $openingCount + $jobfetch['opening'];
                }
            }
            $dataTopicks["freefood"] = ["jobcount"=>$jobCount-1, "openingcount"=>$openingCount];

        } else {
            $dataTopicks["freefood"] = ["jobcount"=>0, "openingcount"=>0];
        }

        $hotjobDataone = $this->Homepage_Model->feturedtoppicks_dayihmo_count($uid, $expfilter, $current_lat, $current_long);
        if ($hotjobDataone) {

            $cDate = date("Y-m-d");
            $jobCount=1;
            $openingCount=0;
            foreach($hotjobDataone as $jobfetch) {

                if(strtotime($jobfetch['jobexpire']) >= strtotime($cDate)) {

                    $jobCount++;
                    $openingCount = $openingCount + $jobfetch['opening'];
                }
            }
            $dataTopicks["day1hmo"] = ["jobcount"=>$jobCount-1, "openingcount"=>$openingCount];
        } else {
            $dataTopicks["day1hmo"] = ["jobcount"=>0, "openingcount"=>0];
        }

        $hotjobDataone = $this->Homepage_Model->feturedtoppicks_shift_count($uid, $expfilter, $current_lat, $current_long);
        if ($hotjobDataone) {

            $cDate = date("Y-m-d");
            $jobCount=1;
            $openingCount=0;
            foreach($hotjobDataone as $jobfetch) {

                if(strtotime($jobfetch['jobexpire']) >= strtotime($cDate)) {

                    $jobCount++;
                    $openingCount = $openingCount + $jobfetch['opening'];
                }
            }
            $dataTopicks["shiftjobs"] = ["jobcount"=>$jobCount-1, "openingcount"=>$openingCount];

        } else {
            $dataTopicks["shiftjobs"] = ["jobcount"=>0, "openingcount"=>0];
        }

        $hotjobDataone = $this->Homepage_Model->feturedtoppicks_monthpay_count($uid, $expfilter, $current_lat, $current_long);
        if ($hotjobDataone) {

            $cDate = date("Y-m-d");
            $jobCount=1;
            $openingCount=0;
            foreach($hotjobDataone as $jobfetch) {

                if(strtotime($jobfetch['jobexpire']) >= strtotime($cDate)) {

                    $jobCount++;
                    $openingCount = $openingCount + $jobfetch['opening'];
                }
            }
            $dataTopicks["monthpay"] = ["jobcount"=>$jobCount-1, "openingcount"=>$openingCount];
        } else {
            $dataTopicks["monthpay"] = ["jobcount"=>0, "openingcount"=>0];
        }

        $hotjobDataone = $this->Homepage_Model->feturedtoppicks_workfromhome_count($uid, $expfilter, $current_lat, $current_long);
        if ($hotjobDataone) {

            $cDate = date("Y-m-d");
            $jobCount=1;
            $openingCount=0;
            foreach($hotjobDataone as $jobfetch) {

                if(strtotime($jobfetch['jobexpire']) >= strtotime($cDate)) {

                    $jobCount++;
                    $openingCount = $openingCount + $jobfetch['opening'];
                }
            }
            $dataTopicks["workfromhome"] = ["jobcount"=>$jobCount-1, "openingcount"=>$openingCount];
        } else {
            $dataTopicks["workfromhome"] = ["jobcount"=>0, "openingcount"=>0];
        }

        $hotjobDataone = $this->Homepage_Model->feturedtoppicks_allowances_count($uid, $expfilter, $current_lat, $current_long);
        if ($hotjobDataone) {

            $cDate = date("Y-m-d");
            $jobCount=1;
            $openingCount=0;
            foreach($hotjobDataone as $jobfetch) {

                if(strtotime($jobfetch['jobexpire']) >= strtotime($cDate)) {

                    $jobCount++;
                    $openingCount = $openingCount + $jobfetch['opening'];
                }
            }
            $dataTopicks["retirement"] = ["jobcount"=>$jobCount-1, "openingcount"=>$openingCount];

        } else {
            $dataTopicks["retirement"] = ["jobcount"=>0, "openingcount"=>0];
        }

        return $dataTopicks;
    }

    public function getExploreJobsTopPicks($uid, $current_lat, $current_long, $expfilter, $companyidsarray) {

        $dataTopicks["bonus"] = array();
        $dataTopicks["shiftjobs"] = array();
        $dataTopicks["monthpay"] = array();
        $dataTopicks["workfromhome"] = array();
        $dataTopicks["retirement"] = array();
        $dataTopicks["day1hmo"] = array();
        $dataTopicks["day1hmodependent"] = array();
        $dataTopicks["freefood"] = array();

        $hotjobDataone = $this->Explorejobs_Model->feturedtoppicks_fetch_latlong_bonus($uid, $expfilter, $current_lat, $current_long);
        if ($hotjobDataone) {
            $jobarray = array();
            $neglectarr = array();

            $hotjobDataonegroup = $this->Explorejobs_Model->feturedtoppicks_fetch_latlong_bonus_groupby($uid, $expfilter, $current_lat, $current_long);
            if ($hotjobDataonegroup) {
                foreach ($hotjobDataonegroup as $hotjobsData) {
                    $neglectarr[] = $hotjobsData['id'];
                    $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 1);
                    $datatoppicks1 = "1"; 
                    
                    if(isset($jobTopseleted[0]['picks_id'])) {
                        $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTopseleted[1]['picks_id'])) {
                        $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = $comPic;
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = '1';
                    } else {
                        $jobsstatus = '0';
                    }
                    
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    //$distance = '';
                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    $dataTopicks["bonus"][] = ["jobpost_id" => $hotjobsData['id'], 
                              "comapnyId" => $hotjobsData['compId'],
                              "recruiter_id" => $hotjobsData['recruiter_id'],
                              "job_title" => $hotjobsData['jobtitle'], 
                              "jobDesc" => $hotjobsData['jobDesc'], 
                              "salary" => number_format($basicsalary), 
                              "companyName" => $companydetail[0]["cname"], 
                              "companyAddress" => $companyaddress,
                              "job_image" => $jobImageData,
                              "jobPitch"=>$hotjobsData['jobPitch'], 
                              "cname"=>$cname, 
                              "distance" =>$distance, 
                              'job_image' =>$jobImageData,
                              "savedjob"=> $jobsstatus,
                              "toppicks1" => $datatoppicks1,
                              "toppicks2" => $datatoppicks2,
                              "toppicks3" => $datatoppicks3,
                              "mode"=>$hotjobsData['mode'], 
                    ];

                }
            }

            foreach ($hotjobDataone as $hotjobsData) {
                if(is_array($hotjobsData['id'], $neglectarr)) { } else {

                    $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 1);
                    $datatoppicks1 = "1"; 
                    
                    if(isset($jobTopseleted[0]['picks_id'])) {
                        $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTopseleted[1]['picks_id'])) {
                        $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = $comPic;
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = '1';
                    } else {
                        $jobsstatus = '0';
                    }
                    
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    //$distance = '';
                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    $dataTopicks["bonus"][] = ["jobpost_id" => $hotjobsData['id'], 
                              "comapnyId" => $hotjobsData['compId'],
                              "recruiter_id" => $hotjobsData['recruiter_id'],
                              "job_title" => $hotjobsData['jobtitle'], 
                              "jobDesc" => $hotjobsData['jobDesc'], 
                              "salary" => number_format($basicsalary), 
                              "companyName" => $companydetail[0]["cname"], 
                              "companyAddress" => $companyaddress,
                              "job_image" => $jobImageData,
                              "jobPitch"=>$hotjobsData['jobPitch'], 
                              "cname"=>$cname, 
                              "distance" =>$distance, 
                              'job_image' =>$jobImageData,
                              "savedjob"=> $jobsstatus,
                              "toppicks1" => $datatoppicks1,
                              "toppicks2" => $datatoppicks2,
                              "toppicks3" => $datatoppicks3,
                              "mode"=>$hotjobsData['mode'], 
                    ];
                }
            }
        }

        $hotjobDataone = $this->Explorejobs_Model->feturedtoppicks_fetch_latlong_bonus_freefood($uid, $expfilter, $current_lat, $current_long);
        if ($hotjobDataone) {
            $neglectarr1 = array();

            $hotjobDataonegroup = $this->Explorejobs_Model->feturedtoppicks_fetch_latlong_freefood_groupby($uid, $expfilter, $current_lat, $current_long);

            foreach ($hotjobDataonegroup as $hotjobsData) {
                $neglectarr1[] = $hotjobsData['id'];
                $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 2);
                $datatoppicks1 = "2"; 
                
                if(isset($jobTopseleted[0]['picks_id'])) {
                    $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                } else{
                    $datatoppicks2 = "";
                }
                if(isset($jobTopseleted[1]['picks_id'])) {
                    $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                } else{
                    $datatoppicks3 = "";
                }

                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                if(!empty($companyPic[0]['companyPic'])) {
                    $comPic = trim($companyPic[0]['companyPic']);
                } else{
                    $comPic = "";
                }

                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                if (isset($jobImage[0]['pic'])) {
                    $jobImageData = $jobImage[0]['pic'];
                } else {
                    $jobImageData = $comPic;
                }
                
                $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                if (isset($jobsavedStatus[0]['status'])) {
                    $jobsstatus = '1';
                } else {
                    $jobsstatus = '0';
                }
                
                $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                if ($jobsal[0]['basicsalary']) {
                    $basicsalary = $jobsal[0]['basicsalary'];
                } else {
                    $basicsalary = "0";
                }
                
                $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                if (isset($companydetail1[0]['address'])) {
                    $companyaddress = $companydetail1[0]['address'];
                } else {
                    $companyaddress = '';
                }
                
                $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                if(!empty($companyname[0]["cname"])){
                    $cname = $companyname[0]["cname"];
                }else{
                    $cname = '';
                }

                $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                $distance = number_format((float)$distance, 2, '.', '');
                //$distance = '';
                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                if (isset($jobImage[0]['pic'])) {
                    $jobImageData = $jobImage[0]['pic'];
                } else {
                    $jobImageData = '';
                }
                $dataTopicks["freefood"][] = ["jobpost_id" => $hotjobsData['id'], 
                          "comapnyId" => $hotjobsData['compId'],
                          "recruiter_id" => $hotjobsData['recruiter_id'],
                          "job_title" => $hotjobsData['jobtitle'], 
                          "jobDesc" => $hotjobsData['jobDesc'], 
                          "salary" => number_format($basicsalary), 
                          "companyName" => $companydetail[0]["cname"], 
                          "companyAddress" => $companyaddress,
                          "job_image" => $jobImageData,
                          "jobPitch"=>$hotjobsData['jobPitch'], 
                          "cname"=>$cname, 
                          "distance" =>$distance, 
                          'job_image' =>$jobImageData,
                          "savedjob"=> $jobsstatus,
                          "toppicks1" => $datatoppicks1,
                          "toppicks2" => $datatoppicks2,
                          "toppicks3" => $datatoppicks3,
                          "mode"=>$hotjobsData['mode'], 
                ];

            }

            foreach ($hotjobDataone as $hotjobsData) {

                if(is_array($hotjobsData['id'], $neglectarr1)) { } else {

                    $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 2);
                    $datatoppicks1 = "2"; 
                    
                    if(isset($jobTopseleted[0]['picks_id'])) {
                        $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTopseleted[1]['picks_id'])) {
                        $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = $comPic;
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = '1';
                    } else {
                        $jobsstatus = '0';
                    }
                    
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    //$distance = '';
                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    $dataTopicks["freefood"][] = ["jobpost_id" => $hotjobsData['id'], 
                              "comapnyId" => $hotjobsData['compId'],
                              "recruiter_id" => $hotjobsData['recruiter_id'],
                              "job_title" => $hotjobsData['jobtitle'], 
                              "jobDesc" => $hotjobsData['jobDesc'], 
                              "salary" => number_format($basicsalary), 
                              "companyName" => $companydetail[0]["cname"], 
                              "companyAddress" => $companyaddress,
                              "job_image" => $jobImageData,
                              "jobPitch"=>$hotjobsData['jobPitch'], 
                              "cname"=>$cname, 
                              "distance" =>$distance, 
                              'job_image' =>$jobImageData,
                              "savedjob"=> $jobsstatus,
                              "toppicks1" => $datatoppicks1,
                              "toppicks2" => $datatoppicks2,
                              "toppicks3" => $datatoppicks3,
                              "mode"=>$hotjobsData['mode'], 
                    ];
                }
            }
        }


        $hotjobDataone = $this->Explorejobs_Model->feturedtoppicks_fetch_latlong_bonus_dayihmo($uid, $expfilter, $current_lat, $current_long);
        if ($hotjobDataone) {
            $jobarray = array();
            $neglectarr2 = array();

            $hotjobDataonegroup = $this->Explorejobs_Model->feturedtoppicks_fetch_latlong_dayihmo_groupby($uid, $expfilter, $current_lat, $current_long);
            foreach ($hotjobDataonegroup as $hotjobsData) {
                $neglectarr2[] = $hotjobsData['id'];
                $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 4);
                $datatoppicks1 = "4"; 
                
                if(isset($jobTopseleted[0]['picks_id'])) {
                    $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                } else{
                    $datatoppicks2 = "";
                }
                if(isset($jobTopseleted[1]['picks_id'])) {
                    $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                } else{
                    $datatoppicks3 = "";
                }

                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                if(!empty($companyPic[0]['companyPic'])) {
                    $comPic = trim($companyPic[0]['companyPic']);
                } else{
                    $comPic = "";
                }

                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                if (isset($jobImage[0]['pic'])) {
                    $jobImageData = $jobImage[0]['pic'];
                } else {
                    $jobImageData = $comPic;
                }
                
                $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                if (isset($jobsavedStatus[0]['status'])) {
                    $jobsstatus = '1';
                } else {
                    $jobsstatus = '0';
                }
                
                $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                if ($jobsal[0]['basicsalary']) {
                    $basicsalary = $jobsal[0]['basicsalary'];
                } else {
                    $basicsalary = "0";
                }
                
                $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                if (isset($companydetail1[0]['address'])) {
                    $companyaddress = $companydetail1[0]['address'];
                } else {
                    $companyaddress = '';
                }
                
                $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                if(!empty($companyname[0]["cname"])){
                    $cname = $companyname[0]["cname"];
                }else{
                    $cname = '';
                }

                $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                $distance = number_format((float)$distance, 2, '.', '');
                //$distance = '';
                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                if (isset($jobImage[0]['pic'])) {
                    $jobImageData = $jobImage[0]['pic'];
                } else {
                    $jobImageData = '';
                }
                $dataTopicks["day1hmo"][] = ["jobpost_id" => $hotjobsData['id'], 
                          "comapnyId" => $hotjobsData['compId'],
                          "recruiter_id" => $hotjobsData['recruiter_id'],
                          "job_title" => $hotjobsData['jobtitle'], 
                          "jobDesc" => $hotjobsData['jobDesc'], 
                          "salary" => number_format($basicsalary), 
                          "companyName" => $companydetail[0]["cname"], 
                          "companyAddress" => $companyaddress,
                          "job_image" => $jobImageData,
                          "jobPitch"=>$hotjobsData['jobPitch'], 
                          "cname"=>$cname, 
                          "distance" =>$distance, 
                          'job_image' =>$jobImageData,
                          "savedjob"=> $jobsstatus,
                          "toppicks1" => $datatoppicks1,
                          "toppicks2" => $datatoppicks2,
                          "toppicks3" => $datatoppicks3,
                          "mode"=>$hotjobsData['mode'], 
                ];

            }

            foreach ($hotjobDataone as $hotjobsData) {

                if(is_array($hotjobsData['id'], $neglectarr2)) { } else {
                    $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 4);
                    $datatoppicks1 = "4"; 
                    
                    if(isset($jobTopseleted[0]['picks_id'])) {
                        $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTopseleted[1]['picks_id'])) {
                        $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = $comPic;
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = '1';
                    } else {
                        $jobsstatus = '0';
                    }
                    
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    //$distance = '';
                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    $dataTopicks["day1hmo"][] = ["jobpost_id" => $hotjobsData['id'], 
                              "comapnyId" => $hotjobsData['compId'],
                              "recruiter_id" => $hotjobsData['recruiter_id'],
                              "job_title" => $hotjobsData['jobtitle'], 
                              "jobDesc" => $hotjobsData['jobDesc'], 
                              "salary" => number_format($basicsalary), 
                              "companyName" => $companydetail[0]["cname"], 
                              "companyAddress" => $companyaddress,
                              "job_image" => $jobImageData,
                              "jobPitch"=>$hotjobsData['jobPitch'], 
                              "cname"=>$cname, 
                              "distance" =>$distance, 
                              'job_image' =>$jobImageData,
                              "savedjob"=> $jobsstatus,
                              "toppicks1" => $datatoppicks1,
                              "toppicks2" => $datatoppicks2,
                              "toppicks3" => $datatoppicks3,
                              "mode"=>$hotjobsData['mode'], 
                    ];
                }
            }
        }


        $hotjobDataone = $this->Explorejobs_Model->feturedtoppicks_fetch_latlong_bonus_shift($uid, $expfilter, $current_lat, $current_long);
        if ($hotjobDataone) {
            $jobarray = array();
            $neglectarr3 = array();

            $hotjobDataonegroup = $this->Explorejobs_Model->feturedtoppicks_fetch_latlong_shift_groupby($uid, $expfilter, $current_lat, $current_long);
            foreach ($hotjobDataonegroup as $hotjobsData) {
                $neglectarr3[] = $hotjobsData['id'];
                $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 5);
                $datatoppicks1 = "5"; 
                
                if(isset($jobTopseleted[0]['picks_id'])) {
                    $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                } else{
                    $datatoppicks2 = "";
                }
                if(isset($jobTopseleted[1]['picks_id'])) {
                    $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                } else{
                    $datatoppicks3 = "";
                }

                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                if(!empty($companyPic[0]['companyPic'])) {
                    $comPic = trim($companyPic[0]['companyPic']);
                } else{
                    $comPic = "";
                }

                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                if (isset($jobImage[0]['pic'])) {
                    $jobImageData = $jobImage[0]['pic'];
                } else {
                    $jobImageData = $comPic;
                }
                
                $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                if (isset($jobsavedStatus[0]['status'])) {
                    $jobsstatus = '1';
                } else {
                    $jobsstatus = '0';
                }
                
                $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                if ($jobsal[0]['basicsalary']) {
                    $basicsalary = $jobsal[0]['basicsalary'];
                } else {
                    $basicsalary = "0";
                }
                
                $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                if (isset($companydetail1[0]['address'])) {
                    $companyaddress = $companydetail1[0]['address'];
                } else {
                    $companyaddress = '';
                }
                
                $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                if(!empty($companyname[0]["cname"])){
                    $cname = $companyname[0]["cname"];
                }else{
                    $cname = '';
                }

                $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                $distance = number_format((float)$distance, 2, '.', '');
                //$distance = '';
                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                if (isset($jobImage[0]['pic'])) {
                    $jobImageData = $jobImage[0]['pic'];
                } else {
                    $jobImageData = '';
                }
                $dataTopicks["shiftjobs"][] = ["jobpost_id" => $hotjobsData['id'], 
                          "comapnyId" => $hotjobsData['compId'],
                          "recruiter_id" => $hotjobsData['recruiter_id'],
                          "job_title" => $hotjobsData['jobtitle'], 
                          "jobDesc" => $hotjobsData['jobDesc'], 
                          "salary" => number_format($basicsalary), 
                          "companyName" => $companydetail[0]["cname"], 
                          "companyAddress" => $companyaddress,
                          "job_image" => $jobImageData,
                          "jobPitch"=>$hotjobsData['jobPitch'], 
                          "cname"=>$cname, 
                          "distance" =>$distance, 
                          'job_image' =>$jobImageData,
                          "savedjob"=> $jobsstatus,
                          "toppicks1" => $datatoppicks1,
                          "toppicks2" => $datatoppicks2,
                          "toppicks3" => $datatoppicks3,
                          "mode"=>$hotjobsData['mode'], 
                ];

            }

            foreach ($hotjobDataone as $hotjobsData) {
                if(is_array($hotjobsData['id'], $neglectarr3)) { } else {
                    $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 5);
                    $datatoppicks1 = "5"; 
                    
                    if(isset($jobTopseleted[0]['picks_id'])) {
                        $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTopseleted[1]['picks_id'])) {
                        $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = $comPic;
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = '1';
                    } else {
                        $jobsstatus = '0';
                    }
                    
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    //$distance = '';
                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    $dataTopicks["shiftjobs"][] = ["jobpost_id" => $hotjobsData['id'], 
                              "comapnyId" => $hotjobsData['compId'],
                              "recruiter_id" => $hotjobsData['recruiter_id'],
                              "job_title" => $hotjobsData['jobtitle'], 
                              "jobDesc" => $hotjobsData['jobDesc'], 
                              "salary" => number_format($basicsalary), 
                              "companyName" => $companydetail[0]["cname"], 
                              "companyAddress" => $companyaddress,
                              "job_image" => $jobImageData,
                              "jobPitch"=>$hotjobsData['jobPitch'], 
                              "cname"=>$cname, 
                              "distance" =>$distance, 
                              'job_image' =>$jobImageData,
                              "savedjob"=> $jobsstatus,
                              "toppicks1" => $datatoppicks1,
                              "toppicks2" => $datatoppicks2,
                              "toppicks3" => $datatoppicks3,
                              "mode"=>$hotjobsData['mode'], 
                    ];
                }
            }
        }

        $hotjobDataone = $this->Explorejobs_Model->feturedtoppicks_fetch_latlong_bonus_monthpay($uid, $expfilter, $current_lat, $current_long);
        if ($hotjobDataone) {
            $jobarray = array();
            $neglectarr4 = array();

            $hotjobDataonegroup = $this->Explorejobs_Model->feturedtoppicks_fetch_latlong_monthpay_groupby($uid, $expfilter, $current_lat, $current_long);
            foreach ($hotjobDataonegroup as $hotjobsData) {
                $neglectarr4[] = $hotjobsData['id'];
                $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 6);
                $datatoppicks1 = "6"; 
                
                if(isset($jobTopseleted[0]['picks_id'])) {
                    $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                } else{
                    $datatoppicks2 = "";
                }
                if(isset($jobTopseleted[1]['picks_id'])) {
                    $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                } else{
                    $datatoppicks3 = "";
                }

                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                if(!empty($companyPic[0]['companyPic'])) {
                    $comPic = trim($companyPic[0]['companyPic']);
                } else{
                    $comPic = "";
                }

                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                if (isset($jobImage[0]['pic'])) {
                    $jobImageData = $jobImage[0]['pic'];
                } else {
                    $jobImageData = $comPic;
                }
                
                $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                if (isset($jobsavedStatus[0]['status'])) {
                    $jobsstatus = '1';
                } else {
                    $jobsstatus = '0';
                }
                
                $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                if ($jobsal[0]['basicsalary']) {
                    $basicsalary = $jobsal[0]['basicsalary'];
                } else {
                    $basicsalary = "0";
                }
                
                $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                if (isset($companydetail1[0]['address'])) {
                    $companyaddress = $companydetail1[0]['address'];
                } else {
                    $companyaddress = '';
                }
                
                $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                if(!empty($companyname[0]["cname"])){
                    $cname = $companyname[0]["cname"];
                }else{
                    $cname = '';
                }

                $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                $distance = number_format((float)$distance, 2, '.', '');
                //$distance = '';
                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                if (isset($jobImage[0]['pic'])) {
                    $jobImageData = $jobImage[0]['pic'];
                } else {
                    $jobImageData = '';
                }
                $dataTopicks["monthpay"][] = ["jobpost_id" => $hotjobsData['id'], 
                          "comapnyId" => $hotjobsData['compId'],
                          "recruiter_id" => $hotjobsData['recruiter_id'],
                          "job_title" => $hotjobsData['jobtitle'], 
                          "jobDesc" => $hotjobsData['jobDesc'], 
                          "salary" => number_format($basicsalary), 
                          "companyName" => $companydetail[0]["cname"], 
                          "companyAddress" => $companyaddress,
                          "job_image" => $jobImageData,
                          "jobPitch"=>$hotjobsData['jobPitch'], 
                          "cname"=>$cname, 
                          "distance" =>$distance, 
                          'job_image' =>$jobImageData,
                          "savedjob"=> $jobsstatus,
                          "toppicks1" => $datatoppicks1,
                          "toppicks2" => $datatoppicks2,
                          "toppicks3" => $datatoppicks3,
                          "mode"=>$hotjobsData['mode'], 
                ];

            }

            foreach ($hotjobDataone as $hotjobsData) {

                if(is_array($hotjobsData['id'], $neglectarr4)) { } else {

                    $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 6);
                    $datatoppicks1 = "6"; 
                    
                    if(isset($jobTopseleted[0]['picks_id'])) {
                        $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTopseleted[1]['picks_id'])) {
                        $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = $comPic;
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = '1';
                    } else {
                        $jobsstatus = '0';
                    }
                    
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    //$distance = '';
                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    $dataTopicks["monthpay"][] = ["jobpost_id" => $hotjobsData['id'], 
                              "comapnyId" => $hotjobsData['compId'],
                              "recruiter_id" => $hotjobsData['recruiter_id'],
                              "job_title" => $hotjobsData['jobtitle'], 
                              "jobDesc" => $hotjobsData['jobDesc'], 
                              "salary" => number_format($basicsalary), 
                              "companyName" => $companydetail[0]["cname"], 
                              "companyAddress" => $companyaddress,
                              "job_image" => $jobImageData,
                              "jobPitch"=>$hotjobsData['jobPitch'], 
                              "cname"=>$cname, 
                              "distance" =>$distance, 
                              'job_image' =>$jobImageData,
                              "savedjob"=> $jobsstatus,
                              "toppicks1" => $datatoppicks1,
                              "toppicks2" => $datatoppicks2,
                              "toppicks3" => $datatoppicks3,
                              "mode"=>$hotjobsData['mode'], 
                    ];
                }
            }
        }

        $hotjobDataone = $this->Explorejobs_Model->feturedtoppicks_fetch_latlong_bonus_workfromhome($uid, $expfilter, $current_lat, $current_long);
        if ($hotjobDataone) {
            $jobarray = array();
            $neglectarr5 = array();

            $hotjobDataonegroup = $this->Explorejobs_Model->feturedtoppicks_fetch_latlong_workfromhome_groupby($uid, $expfilter, $current_lat, $current_long);
            foreach ($hotjobDataonegroup as $hotjobsData) {
                $neglectarr5[] = $hotjobsData['id'];
                $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 7);
                $datatoppicks1 = "7"; 
                
                if(isset($jobTopseleted[0]['picks_id'])) {
                    $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                } else{
                    $datatoppicks2 = "";
                }
                if(isset($jobTopseleted[1]['picks_id'])) {
                    $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                } else{
                    $datatoppicks3 = "";
                }

                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                if(!empty($companyPic[0]['companyPic'])) {
                    $comPic = trim($companyPic[0]['companyPic']);
                } else{
                    $comPic = "";
                }

                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                if (isset($jobImage[0]['pic'])) {
                    $jobImageData = $jobImage[0]['pic'];
                } else {
                    $jobImageData = $comPic;
                }
                
                $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                if (isset($jobsavedStatus[0]['status'])) {
                    $jobsstatus = '1';
                } else {
                    $jobsstatus = '0';
                }
                
                $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                if ($jobsal[0]['basicsalary']) {
                    $basicsalary = $jobsal[0]['basicsalary'];
                } else {
                    $basicsalary = "0";
                }
                
                $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                if (isset($companydetail1[0]['address'])) {
                    $companyaddress = $companydetail1[0]['address'];
                } else {
                    $companyaddress = '';
                }
                
                $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                if(!empty($companyname[0]["cname"])){
                    $cname = $companyname[0]["cname"];
                }else{
                    $cname = '';
                }

                $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                $distance = number_format((float)$distance, 2, '.', '');
                //$distance = '';
                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                if (isset($jobImage[0]['pic'])) {
                    $jobImageData = $jobImage[0]['pic'];
                } else {
                    $jobImageData = '';
                }
                $dataTopicks["workfromhome"][] = ["jobpost_id" => $hotjobsData['id'], 
                          "comapnyId" => $hotjobsData['compId'],
                          "recruiter_id" => $hotjobsData['recruiter_id'],
                          "job_title" => $hotjobsData['jobtitle'], 
                          "jobDesc" => $hotjobsData['jobDesc'], 
                          "salary" => number_format($basicsalary), 
                          "companyName" => $companydetail[0]["cname"], 
                          "companyAddress" => $companyaddress,
                          "job_image" => $jobImageData,
                          "jobPitch"=>$hotjobsData['jobPitch'], 
                          "cname"=>$cname, 
                          "distance" =>$distance, 
                          'job_image' =>$jobImageData,
                          "savedjob"=> $jobsstatus,
                          "toppicks1" => $datatoppicks1,
                          "toppicks2" => $datatoppicks2,
                          "toppicks3" => $datatoppicks3,
                          "mode"=>$hotjobsData['mode'], 
                ];

            }

            foreach ($hotjobDataone as $hotjobsData) {

                if(is_array($hotjobsData['id'], $neglectarr5)) { } else {
                    $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 7);
                    $datatoppicks1 = "7"; 
                    
                    if(isset($jobTopseleted[0]['picks_id'])) {
                        $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTopseleted[1]['picks_id'])) {
                        $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = $comPic;
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = '1';
                    } else {
                        $jobsstatus = '0';
                    }
                    
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    //$distance = '';
                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    $dataTopicks["workfromhome"][] = ["jobpost_id" => $hotjobsData['id'], 
                              "comapnyId" => $hotjobsData['compId'],
                              "recruiter_id" => $hotjobsData['recruiter_id'],
                              "job_title" => $hotjobsData['jobtitle'], 
                              "jobDesc" => $hotjobsData['jobDesc'], 
                              "salary" => number_format($basicsalary), 
                              "companyName" => $companydetail[0]["cname"], 
                              "companyAddress" => $companyaddress,
                              "job_image" => $jobImageData,
                              "jobPitch"=>$hotjobsData['jobPitch'], 
                              "cname"=>$cname, 
                              "distance" =>$distance, 
                              'job_image' =>$jobImageData,
                              "savedjob"=> $jobsstatus,
                              "toppicks1" => $datatoppicks1,
                              "toppicks2" => $datatoppicks2,
                              "toppicks3" => $datatoppicks3,
                              "mode"=>$hotjobsData['mode'], 
                    ];
                }
            }
        }


        $hotjobDataone = $this->Explorejobs_Model->feturedtoppicks_fetch_latlong_bonus_allowances($uid, $expfilter, $current_lat, $current_long);
        if ($hotjobDataone) {
            $jobarray = array();
            $neglectarr6 = array();

            $hotjobDataonegroup = $this->Explorejobs_Model->feturedtoppicks_fetch_latlong_allowances_groupby($uid, $expfilter, $current_lat, $current_long);
            foreach ($hotjobDataonegroup as $hotjobsData) {
                $neglectarr6[] = $hotjobsData['id'];
                $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 5);
                $datatoppicks1 = "5"; 
                
                if(isset($jobTopseleted[0]['picks_id'])) {
                    $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                } else{
                    $datatoppicks2 = "";
                }
                if(isset($jobTopseleted[1]['picks_id'])) {
                    $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                } else{
                    $datatoppicks3 = "";
                }

                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                if(!empty($companyPic[0]['companyPic'])) {
                    $comPic = trim($companyPic[0]['companyPic']);
                } else{
                    $comPic = "";
                }

                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                if (isset($jobImage[0]['pic'])) {
                    $jobImageData = $jobImage[0]['pic'];
                } else {
                    $jobImageData = $comPic;
                }
                
                $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                if (isset($jobsavedStatus[0]['status'])) {
                    $jobsstatus = '1';
                } else {
                    $jobsstatus = '0';
                }
                
                $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                if ($jobsal[0]['basicsalary']) {
                    $basicsalary = $jobsal[0]['basicsalary'];
                } else {
                    $basicsalary = "0";
                }
                
                $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                if (isset($companydetail1[0]['address'])) {
                    $companyaddress = $companydetail1[0]['address'];
                } else {
                    $companyaddress = '';
                }
                
                $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                if(!empty($companyname[0]["cname"])){
                    $cname = $companyname[0]["cname"];
                }else{
                    $cname = '';
                }

                $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                $distance = number_format((float)$distance, 2, '.', '');
                //$distance = '';
                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                if (isset($jobImage[0]['pic'])) {
                    $jobImageData = $jobImage[0]['pic'];
                } else {
                    $jobImageData = '';
                }
                $dataTopicks["retirement"][] = ["jobpost_id" => $hotjobsData['id'], 
                          "comapnyId" => $hotjobsData['compId'],
                          "recruiter_id" => $hotjobsData['recruiter_id'],
                          "job_title" => $hotjobsData['jobtitle'], 
                          "jobDesc" => $hotjobsData['jobDesc'], 
                          "salary" => number_format($basicsalary), 
                          "companyName" => $companydetail[0]["cname"], 
                          "companyAddress" => $companyaddress,
                          "job_image" => $jobImageData,
                          "jobPitch"=>$hotjobsData['jobPitch'], 
                          "cname"=>$cname, 
                          "distance" =>$distance, 
                          'job_image' =>$jobImageData,
                          "savedjob"=> $jobsstatus,
                          "toppicks1" => $datatoppicks1,
                          "toppicks2" => $datatoppicks2,
                          "toppicks3" => $datatoppicks3,
                          "mode"=>$hotjobsData['mode'], 
                ];

            }

            foreach ($hotjobDataone as $hotjobsData) {

                if(is_array($hotjobsData['id'], $neglectarr6)) { } else {
                    $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 5);
                    $datatoppicks1 = "5"; 
                    
                    if(isset($jobTopseleted[0]['picks_id'])) {
                        $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTopseleted[1]['picks_id'])) {
                        $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = $comPic;
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = '1';
                    } else {
                        $jobsstatus = '0';
                    }
                    
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    //$distance = '';
                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    $dataTopicks["retirement"][] = ["jobpost_id" => $hotjobsData['id'], 
                              "comapnyId" => $hotjobsData['compId'],
                              "recruiter_id" => $hotjobsData['recruiter_id'],
                              "job_title" => $hotjobsData['jobtitle'], 
                              "jobDesc" => $hotjobsData['jobDesc'], 
                              "salary" => number_format($basicsalary), 
                              "companyName" => $companydetail[0]["cname"], 
                              "companyAddress" => $companyaddress,
                              "job_image" => $jobImageData,
                              "jobPitch"=>$hotjobsData['jobPitch'], 
                              "cname"=>$cname, 
                              "distance" =>$distance, 
                              'job_image' =>$jobImageData,
                              "savedjob"=> $jobsstatus,
                              "toppicks1" => $datatoppicks1,
                              "toppicks2" => $datatoppicks2,
                              "toppicks3" => $datatoppicks3,
                              "mode"=>$hotjobsData['mode'], 
                    ];
                }
            }
        }
        

        $hotjobDataone = $this->Explorejobs_Model->feturedtoppicks_fetch_latlong_dayihmodepend($uid, $expfilter, $current_lat, $current_long);
        if ($hotjobDataone) {
            $jobarray = array();
            $neglectarr2 = array();

            $hotjobDataonegroup = $this->Explorejobs_Model->feturedtoppicks_fetch_latlong_dayihmodepend_groupby($uid, $expfilter, $current_lat, $current_long);
            foreach ($hotjobDataonegroup as $hotjobsData) {
                $neglectarr2[] = $hotjobsData['id'];
                $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 3);
                $datatoppicks1 = "3"; 
                
                if(isset($jobTopseleted[0]['picks_id'])) {
                    $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                } else{
                    $datatoppicks2 = "";
                }
                if(isset($jobTopseleted[1]['picks_id'])) {
                    $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                } else{
                    $datatoppicks3 = "";
                }

                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                if(!empty($companyPic[0]['companyPic'])) {
                    $comPic = trim($companyPic[0]['companyPic']);
                } else{
                    $comPic = "";
                }

                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                if (isset($jobImage[0]['pic'])) {
                    $jobImageData = $jobImage[0]['pic'];
                } else {
                    $jobImageData = $comPic;
                }
                
                $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                if (isset($jobsavedStatus[0]['status'])) {
                    $jobsstatus = '1';
                } else {
                    $jobsstatus = '0';
                }
                
                $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                if ($jobsal[0]['basicsalary']) {
                    $basicsalary = $jobsal[0]['basicsalary'];
                } else {
                    $basicsalary = "0";
                }
                
                $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                if (isset($companydetail1[0]['address'])) {
                    $companyaddress = $companydetail1[0]['address'];
                } else {
                    $companyaddress = '';
                }
                
                $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                if(!empty($companyname[0]["cname"])){
                    $cname = $companyname[0]["cname"];
                }else{
                    $cname = '';
                }

                $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                $distance = number_format((float)$distance, 2, '.', '');
                //$distance = '';
                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                if (isset($jobImage[0]['pic'])) {
                    $jobImageData = $jobImage[0]['pic'];
                } else {
                    $jobImageData = '';
                }
                $dataTopicks["day1hmodependent"][] = ["jobpost_id" => $hotjobsData['id'], 
                          "comapnyId" => $hotjobsData['compId'],
                          "recruiter_id" => $hotjobsData['recruiter_id'],
                          "job_title" => $hotjobsData['jobtitle'], 
                          "jobDesc" => $hotjobsData['jobDesc'], 
                          "salary" => number_format($basicsalary), 
                          "companyName" => $companydetail[0]["cname"], 
                          "companyAddress" => $companyaddress,
                          "job_image" => $jobImageData,
                          "jobPitch"=>$hotjobsData['jobPitch'], 
                          "cname"=>$cname, 
                          "distance" =>$distance, 
                          'job_image' =>$jobImageData,
                          "savedjob"=> $jobsstatus,
                          "toppicks1" => $datatoppicks1,
                          "toppicks2" => $datatoppicks2,
                          "toppicks3" => $datatoppicks3,
                          "mode"=>$hotjobsData['mode'], 
                ];

            }

            foreach ($hotjobDataone as $hotjobsData) {

                if(is_array($hotjobsData['id'], $neglectarr2)) { } else {
                    $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 3);
                    $datatoppicks1 = "3"; 
                    
                    if(isset($jobTopseleted[0]['picks_id'])) {
                        $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTopseleted[1]['picks_id'])) {
                        $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = $comPic;
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = '1';
                    } else {
                        $jobsstatus = '0';
                    }
                    
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    //$distance = '';
                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    $dataTopicks["day1hmodependent"][] = ["jobpost_id" => $hotjobsData['id'], 
                              "comapnyId" => $hotjobsData['compId'],
                              "recruiter_id" => $hotjobsData['recruiter_id'],
                              "job_title" => $hotjobsData['jobtitle'], 
                              "jobDesc" => $hotjobsData['jobDesc'], 
                              "salary" => number_format($basicsalary), 
                              "companyName" => $companydetail[0]["cname"], 
                              "companyAddress" => $companyaddress,
                              "job_image" => $jobImageData,
                              "jobPitch"=>$hotjobsData['jobPitch'], 
                              "cname"=>$cname, 
                              "distance" =>$distance, 
                              'job_image' =>$jobImageData,
                              "savedjob"=> $jobsstatus,
                              "toppicks1" => $datatoppicks1,
                              "toppicks2" => $datatoppicks2,
                              "toppicks3" => $datatoppicks3,
                              "mode"=>$hotjobsData['mode'], 
                    ];
                }
            }
        }

        return $dataTopicks;
    }

    public function getexplorehotJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray) {

        $hotjobss = array();
        $bostArr = array();
        $neglectArr = array();
        $hotjobData = $this->Explorejobs_Model->hotjob_fetch_latlongbylimit_groupby($userID, $expfilter, $current_lat, $current_long,[]);
        foreach ($hotjobData as $hotjobsData) {
            $neglectArr[] = $hotjobsData['id'];

            $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
            if(!empty($jobTop) && count($jobTop)>=1) {
                  
                if(isset($jobTop[0]['picks_id'])) {
                    $datatoppicks1 = $jobTop[0]['picks_id']; 
                } else{
                    $datatoppicks1 = "";
                }
                if(isset($jobTop[1]['picks_id'])) {
                    $datatoppicks2 = $jobTop[1]['picks_id']; 
                } else{
                    $datatoppicks2 = "";
                }
                if(isset($jobTop[2]['picks_id'])) {
                    $datatoppicks3 = $jobTop[2]['picks_id']; 
                } else{
                    $datatoppicks3 = "";
                }
                $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['user_id']);
                if(!empty($savedjob[0]['id'])) {
                    $savedjob = '1';
                } else{
                    $savedjob = "0";
                }
                $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                if($jobsal[0]['basicsalary']) {
                    $basicsalary = $jobsal[0]['basicsalary'];
                } else{
                    $basicsalary = 0;
                }
                $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                if(isset($companydetail1[0]['address'])){
                    $companyaddress=$companydetail1[0]['address'];
                }
                else{
                    $companyaddress='';
                }

                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                if(!empty($companyPic[0]['companyPic'])) {
                    $comPic = trim($companyPic[0]['companyPic']);
                } else{
                    $comPic = "";
                }

                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                if(isset($jobImage[0]['pic'])){
                    $jobImageData=$jobImage[0]['pic'];
                }
                else{
                    $jobImageData=$comPic;
                }

                $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                if(!empty($companyname[0]["cname"])){
                    $cname = $companyname[0]["cname"];
                }else{
                    $cname = '';
                }
                $distance = $this->distance($current_lat, $current_long, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                $distance = number_format((float)$distance, 2, '.', ''); 

                $hotjobss[] = [
                        "jobpost_id" => $hotjobsData['id'],
                        "comapnyId" =>$hotjobsData['compId'],
                        "recruiter_id" =>$hotjobsData['recruiter_id'],
                        "job_title" => $hotjobsData['jobtitle'],
                        "jobDesc" => $hotjobsData['jobDesc'],
                        "jobPitch"=>$hotjobsData['jobPitch'],
                        "salary" => number_format($basicsalary),
                        "companyName"=> $companydetail[0]["cname"],
                        "cname"=> $cname,
                        "companyAddress"=>$companyaddress,
                        "toppicks1" => $datatoppicks1,
                        "toppicks2" => $datatoppicks2,
                        "toppicks3" => $datatoppicks3,
                        "job_image" =>$jobImageData,
                        "latitude"=> $hotjobsData['latitude'], 
                        "longitude"=>$hotjobsData['longitude'],
                        "jobexpire"=> $hotjobsData['jobexpire'],
                        "companyPic"=> $comPic,
                        "savedjob" =>$savedjob,
                        "boostjob" =>$hotjobsData['boost_status'],
                        "distance" => $distance,
                        "mode" => $hotjobsData['mode'],
                ];
            }
        }

        $boostjobData = $this->Explorejobs_Model->boostjob_fetch_latlongbylimit($userID,$expfilter, $current_lat,$current_long);
        foreach ($boostjobData as $boostjobsData) {

            if(is_array($boostjobsData['id'], $neglectArr)) { } else {
                $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
                if(!empty($jobTop) && count($jobTop)>=1) {
                    if(isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }                
                    $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userID);
                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }
                    $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = 0;
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                    if(isset($companydetail1[0]['address'])){
                        $companyaddress=$companydetail1[0]['address'];
                    }
                    else{
                        $companyaddress='';
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                    if(isset($jobImage[0]['pic'])){
                        $jobImageData=$jobImage[0]['pic'];
                    }
                    else{
                        $jobImageData=$comPic;
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', ''); 

                    $bostArr[] = $boostjobsData['id'];

                    $hotjobss[] = [
                            "jobpost_id" => $boostjobsData['id'],
                            "comapnyId" =>$boostjobsData['compId'],
                            "job_title" => $boostjobsData['jobtitle'],
                            "jobDesc" => $boostjobsData['jobDesc'],
                            "jobPitch"=>$boostjobsData['jobPitch'],
                            "salary" => number_format($boostjobsData['salary']),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $boostjobsData['latitude'], 
                            "longitude"=>$boostjobsData['longitude'],
                            "jobexpire"=> $boostjobsData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$boostjobsData['boost_status'],
                            "distance" => $distance
                    ];
                }
            }
        }

        $hotjobData = $this->Explorejobs_Model->hotjob_fetch_latlongbylimit($userID, $expfilter, $current_lat, $current_long);
        foreach ($hotjobData as $hotjobsData) {

            if(in_array($hotjobsData['id'], $bostArr)) {
            } else {

                $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                if(!empty($jobTop) && count($jobTop)>=1) {
                      //echo "</pre>";    
                    if(isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }
                    $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['user_id']);
                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = 0;
                    }
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if(isset($companydetail1[0]['address'])){
                        $companyaddress=$companydetail1[0]['address'];
                    }
                    else{
                        $companyaddress='';
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if(isset($jobImage[0]['pic'])){
                        $jobImageData=$jobImage[0]['pic'];
                    }
                    else{
                        $jobImageData=$comPic;
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }
                    $distance = $this->distance($current_lat, $current_long, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', ''); 

                    $hotjobss[] = [
                            "jobpost_id" => $hotjobsData['id'],
                            "comapnyId" =>$hotjobsData['compId'],
                            "recruiter_id" =>$hotjobsData['recruiter_id'],
                            "job_title" => $hotjobsData['jobtitle'],
                            "jobDesc" => $hotjobsData['jobDesc'],
                            "jobPitch"=>$hotjobsData['jobPitch'],
                            "salary" => number_format($basicsalary),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $hotjobsData['latitude'], 
                            "longitude"=>$hotjobsData['longitude'],
                            "jobexpire"=> $hotjobsData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$hotjobsData['boost_status'],
                            "distance" => $distance,
                            "mode" => $hotjobsData['mode'],
                    ];
                }
            }
        }

        return $hotjobss;
    }

    public function getexploreitJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray) {

        $itcatjobs = array();
        $negletarr = array();

        foreach($companyidsarray as $companyidsarr) {
            $itcatjobDatas = $this->Explorejobs_Model->catjob_fetch_information_technology_groupby($userID, $expfilter, $current_lat, $current_long);
            foreach ($itcatjobDatas as $itcatjobData) {
                $negletarr[] = $itcatjobData['id'];

                $jobTop = $this->Jobpost_Model->job_toppicks($itcatjobData['id']);
          
                if(!empty($jobTop) && count($jobTop)>=0) {
                      //echo "</pre>";    
                    if(isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }
                    
                    $savedjob = $this->Jobpost_Model->job_saved($itcatjobData['id'], $userTokenCheck[0]['user_id']);
                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }

                    $jobsal = $this->Jobpost_Model->getExpJob($itcatjobData['id']);
                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = 0;
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($itcatjobData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($itcatjobData['compId']);
                    if(isset($companydetail1[0]['address'])){
                        $companyaddress=$companydetail1[0]['address'];
                    }
                    else{
                        $companyaddress='';
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($itcatjobData['compId']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($itcatjobData['id']);
                    if(isset($jobImage[0]['pic'])){
                        $jobImageData=$jobImage[0]['pic'];
                    }
                    else{
                        $jobImageData=$comPic;
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($itcatjobData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', ''); 

                    $itcatjobs[] = [
                            "jobpost_id" => $itcatjobData['id'],
                            "comapnyId" =>$itcatjobData['compId'],
                            "recruiter_id" =>$itcatjobData['recruiter_id'],
                            "job_title" => $itcatjobData['jobtitle'],
                            "jobDesc" => $itcatjobData['jobDesc'],
                            "jobPitch"=>$itcatjobData['jobPitch'],
                            "salary" => number_format($itcatjobData['salary']),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $itcatjobData['latitude'], 
                            "longitude"=>$itcatjobData['longitude'],
                            "jobexpire"=> $itcatjobData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$itcatjobData['boost_status'],
                            "distance" => $distance,
                            "mode"=>$itcatjobData['mode'],
                    ];
                }
            }
        }

        $itcatjobDatas = $this->Explorejobs_Model->catjob_fetch_information_technology($userID, $expfilter, $current_lat, $current_long);
        foreach ($itcatjobDatas as $itcatjobData) {

            if(is_array($itcatjobData['id'], $negletarr)) { } else {
                $jobTop = $this->Jobpost_Model->job_toppicks($itcatjobData['id']);
          
                if(!empty($jobTop) && count($jobTop)>=0) {
                      //echo "</pre>";    
                    if(isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }
                    
                    $savedjob = $this->Jobpost_Model->job_saved($itcatjobData['id'], $userTokenCheck[0]['user_id']);
                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }

                    $jobsal = $this->Jobpost_Model->getExpJob($itcatjobData['id']);
                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = 0;
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($itcatjobData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($itcatjobData['compId']);
                    if(isset($companydetail1[0]['address'])){
                        $companyaddress=$companydetail1[0]['address'];
                    }
                    else{
                        $companyaddress='';
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($itcatjobData['compId']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($itcatjobData['id']);
                    if(isset($jobImage[0]['pic'])){
                        $jobImageData=$jobImage[0]['pic'];
                    }
                    else{
                        $jobImageData=$comPic;
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($itcatjobData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', ''); 

                    $itcatjobs[] = [
                            "jobpost_id" => $itcatjobData['id'],
                            "comapnyId" =>$itcatjobData['compId'],
                            "recruiter_id" =>$itcatjobData['recruiter_id'],
                            "job_title" => $itcatjobData['jobtitle'],
                            "jobDesc" => $itcatjobData['jobDesc'],
                            "jobPitch"=>$itcatjobData['jobPitch'],
                            "salary" => number_format($itcatjobData['salary']),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $itcatjobData['latitude'], 
                            "longitude"=>$itcatjobData['longitude'],
                            "jobexpire"=> $itcatjobData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$itcatjobData['boost_status'],
                            "distance" => $distance,
                            "mode"=>$itcatjobData['mode'],
                    ];
                }

            }
        }
        return $itcatjobs;
    }

    public function getexploreleadershipJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray) {

        $leadercatjobs = array();
        $negletarr = array();

        foreach($companyidsarray as $companyidsarr) {
            $catjobDatas = $this->Explorejobs_Model->catjob_fetch_leadership_groupby($userID, $expfilter, $current_lat, $current_long);
            foreach ($catjobDatas as $catjobData) {
                $negletarr[] = $catjobData['id'];
                $jobTop = $this->Jobpost_Model->job_toppicks($catjobData['id']);
          
                if(!empty($jobTop) && count($jobTop)>=0) {
                      //echo "</pre>";    
                    if(isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }
                    
                    $savedjob = $this->Jobpost_Model->job_saved($catjobData['id'], $userTokenCheck[0]['user_id']);
                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }

                    $jobsal = $this->Jobpost_Model->getExpJob($catjobData['id']);
                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = 0;
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($catjobData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($catjobData['compId']);
                    if(isset($companydetail1[0]['address'])){
                        $companyaddress=$companydetail1[0]['address'];
                    }
                    else{
                        $companyaddress='';
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($catjobData['compId']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($catjobData['id']);
                    if(isset($jobImage[0]['pic'])){
                        $jobImageData=$jobImage[0]['pic'];
                    }
                    else{
                        $jobImageData=$comPic;
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($catjobData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', ''); 

                    $leadercatjobs[] = [
                            "jobpost_id" => $catjobData['id'],
                            "comapnyId" =>$catjobData['compId'],
                            "recruiter_id" =>$catjobData['recruiter_id'],
                            "job_title" => $catjobData['jobtitle'],
                            "jobDesc" => $catjobData['jobDesc'],
                            "jobPitch"=>$catjobData['jobPitch'],
                            "salary" => number_format($catjobData['salary']),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $catjobData['latitude'], 
                            "longitude"=>$catjobData['longitude'],
                            "jobexpire"=> $catjobData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$catjobData['boost_status'],
                            "distance" => $distance,
                            "mode"=> $catjobData['mode'], 
                    ];
                }
            }
        }

        $catjobDatas = $this->Explorejobs_Model->catjob_fetch_leadership($userID, $expfilter, $current_lat, $current_long);
        foreach ($catjobDatas as $catjobData) {

            if(is_array($catjobData['id'], $negletarr)) { } else {

                $jobTop = $this->Jobpost_Model->job_toppicks($catjobData['id']);
          
                    if(isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }
                    
                    $savedjob = $this->Jobpost_Model->job_saved($catjobData['id'], $userTokenCheck[0]['user_id']);
                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }

                    $jobsal = $this->Jobpost_Model->getExpJob($catjobData['id']);
                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = 0;
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($catjobData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($catjobData['compId']);
                    if(isset($companydetail1[0]['address'])){
                        $companyaddress=$companydetail1[0]['address'];
                    }
                    else{
                        $companyaddress='';
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($catjobData['compId']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($catjobData['id']);
                    if(isset($jobImage[0]['pic'])){
                        $jobImageData=$jobImage[0]['pic'];
                    }
                    else{
                        $jobImageData=$comPic;
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($catjobData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', ''); 

                    $leadercatjobs[] = [
                            "jobpost_id" => $catjobData['id'],
                            "comapnyId" =>$catjobData['compId'],
                            "recruiter_id" =>$catjobData['recruiter_id'],
                            "job_title" => $catjobData['jobtitle'],
                            "jobDesc" => $catjobData['jobDesc'],
                            "jobPitch"=>$catjobData['jobPitch'],
                            "salary" => number_format($catjobData['salary']),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $catjobData['latitude'], 
                            "longitude"=>$catjobData['longitude'],
                            "jobexpire"=> $catjobData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$catjobData['boost_status'],
                            "distance" => $distance,
                            "mode"=> $catjobData['mode'], 
                    ];
                //}
            }
        }
        return $leadercatjobs;
    }

    public function getexplorenearByJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray) {
        $data1 = array();
        $neglectarr = array();
        
        $jobfetchs = $this->Explorejobs_Model->job_fetch_homeforappnearby_groupby($userID, $current_lat, $current_long, $expfilter);
        if ($jobfetchs) {
            foreach ($jobfetchs as $jobfetch) {
                $neglectarr[] = $jobfetch['id'];
                $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);

                if ($jobsal[0]['basicsalary']) {
                    $basicsalary = $jobsal[0]['basicsalary'];
                } else {
                    $basicsalary = "0";
                }
                
                $condtionssavedjob = array('jobpost_id' => $jobfetch['id'], 'user_id' => $userID);
                $jobstatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                if (isset($jobstatus[0]['status'])) {
                    $jobstatus = '1';
                } else {
                    $jobstatus = '0';
                }

                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobfetch['compId']);
                if (isset($companydetail1[0]['address'])) {
                    $companyaddress = $companydetail1[0]['address'];
                } else {
                    $companyaddress = '';
                }
                
                $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                if ($jobTop) {
                    if (isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id'];
                    } else {
                        $datatoppicks1 = "";
                    }
                    if (isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id'];
                    } else {
                        $datatoppicks2 = "";
                    }
                    if (isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id'];
                    } else {
                        $datatoppicks3 = "";
                    }
                } else {
                    $datatoppicks1 = "";
                    $datatoppicks2 = "";
                    $datatoppicks3 = "";
                }

                $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['compId']);
                if(!empty($companyname[0]["cname"])){
                    $cname = $companyname[0]["cname"];
                }else{
                    $cname = '';
                }

                $distance = $this->distance($current_lat, $current_long, $jobfetch['latitude'], $jobfetch['longitude'], "K");
                $distance = number_format((float)$distance, 2, '.', '');
                $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['recruiter_id']);       
                if(!empty($companyPic[0]['companyPic'])) {
                    $comPic = trim($companyPic[0]['companyPic']);
                } else{
                    $comPic = "";
                }

                $jobImage = $this->Jobpost_Model->job_image($jobfetch['id']);
                if (isset($jobImage[0]['pic'])) {
                    $jobImageData = $jobImage[0]['pic'];
                } else {
                    $jobImageData = $comPic;
                }
                       
                $data1[] = ["jobpost_id" => $jobfetch['id'], "recruiter_id" => $jobfetch['recruiter_id'], "comapnyId" => $jobfetch['compId'], "job_title" => $jobfetch['jobtitle'], "jobDesc" => $jobfetch['jobDesc'],"distance"=>$distance, "salary" => number_format($basicsalary), "companyName" => $cname, "cname"=> $cname,"jobPitch"=>$jobfetch['jobPitch'], "companyId" => $jobfetch['compId'], "jobstatus" => $jobstatus, "company_address" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image"=>$jobImageData, "cname"=>$jobfetch['cname'],"distance"=>$distance, "savedjob" => $jobstatus, "mode" => $jobfetch['mode']];
            }
            $data1;
        } else{
            $data1=[];
        }

            $jobfetchs = $this->Explorejobs_Model->job_fetch_homeforappnearby($userID, $current_lat, $current_long, $expfilter);
            if ($jobfetchs) {
                foreach ($jobfetchs as $jobfetch) {

                    if(is_array($jobfetch['id'], $neglectarr)) { } else {

                        $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);
                        if ($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else {
                            $basicsalary = "0";
                        }
                        
                        $condtionssavedjob = array('jobpost_id' => $jobfetch['id'], 'user_id' => $userID);
                        $jobstatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                        if (isset($jobstatus[0]['status'])) {
                            $jobstatus = '1';
                        } else {
                            $jobstatus = '0';
                        }

                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobfetch['compId']);
                        if (isset($companydetail1[0]['address'])) {
                            $companyaddress = $companydetail1[0]['address'];
                        } else {
                            $companyaddress = '';
                        }
                        
                        $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                        if ($jobTop) {
                            if (isset($jobTop[0]['picks_id'])) {
                                $datatoppicks1 = $jobTop[0]['picks_id'];
                            } else {
                                $datatoppicks1 = "";
                            }
                            if (isset($jobTop[1]['picks_id'])) {
                                $datatoppicks2 = $jobTop[1]['picks_id'];
                            } else {
                                $datatoppicks2 = "";
                            }
                            if (isset($jobTop[2]['picks_id'])) {
                                $datatoppicks3 = $jobTop[2]['picks_id'];
                            } else {
                                $datatoppicks3 = "";
                            }
                        } else {
                            $datatoppicks1 = "";
                            $datatoppicks2 = "";
                            $datatoppicks3 = "";
                        }

                        $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['compId']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($current_lat, $current_long, $jobfetch['latitude'], $jobfetch['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', '');
                        $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['recruiter_id']);       
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }

                        $jobImage = $this->Jobpost_Model->job_image($jobfetch['id']);
                        if (isset($jobImage[0]['pic'])) {
                            $jobImageData = $jobImage[0]['pic'];
                        } else {
                            $jobImageData = $comPic;
                        }
                               
                        $data1[] = ["jobpost_id" => $jobfetch['id'], "recruiter_id" => $jobfetch['recruiter_id'], "comapnyId" => $jobfetch['compId'], "job_title" => $jobfetch['jobtitle'], "jobDesc" => $jobfetch['jobDesc'],"distance"=>$distance, "salary" => number_format($basicsalary), "companyName" => $cname, "cname"=> $cname,"jobPitch"=>$jobfetch['jobPitch'], "companyId" => $jobfetch['compId'], "jobstatus" => $jobstatus, "company_address" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image"=>$jobImageData, "cname"=>$jobfetch['cname'],"distance"=>$distance, "savedjob" => $jobstatus, "mode" => $jobfetch['mode']];
                    }
                }
                return $data1;
            } else{
                $data1=[];
                return $data1;
            }
    }

    public function getexploreNoExpJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray) {
        $data1 = array();
        $neglectarr = array();
        
        $jobfetchs = $this->Explorejobs_Model->job_fetch_homeforappNoExp_groupby($userID, $current_lat, $current_long, $expfilter);
        if ($jobfetchs) {
            foreach ($jobfetchs as $jobfetch) {
                $neglectarr[] = $jobfetch['id'];
                $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);

                if ($jobsal[0]['basicsalary']) {
                    $basicsalary = $jobsal[0]['basicsalary'];
                } else {
                    $basicsalary = "0";
                }
                
                $condtionssavedjob = array('jobpost_id' => $jobfetch['id'], 'user_id' => $userID);
                $jobstatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                if (isset($jobstatus[0]['status'])) {
                    $jobstatus = '1';
                } else {
                    $jobstatus = '0';
                }

                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobfetch['compId']);
                if (isset($companydetail1[0]['address'])) {
                    $companyaddress = $companydetail1[0]['address'];
                } else {
                    $companyaddress = '';
                }
                
                $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                if ($jobTop) {
                    if (isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id'];
                    } else {
                        $datatoppicks1 = "";
                    }
                    if (isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id'];
                    } else {
                        $datatoppicks2 = "";
                    }
                    if (isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id'];
                    } else {
                        $datatoppicks3 = "";
                    }
                } else {
                    $datatoppicks1 = "";
                    $datatoppicks2 = "";
                    $datatoppicks3 = "";
                }

                $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['compId']);
                if(!empty($companyname[0]["cname"])){
                    $cname = $companyname[0]["cname"];
                }else{
                    $cname = '';
                }

                $distance = $this->distance($current_lat, $current_long, $jobfetch['latitude'], $jobfetch['longitude'], "K");
                $distance = number_format((float)$distance, 2, '.', '');
                $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['recruiter_id']);       
                if(!empty($companyPic[0]['companyPic'])) {
                    $comPic = trim($companyPic[0]['companyPic']);
                } else{
                    $comPic = "";
                }

                $jobImage = $this->Jobpost_Model->job_image($jobfetch['id']);
                if (isset($jobImage[0]['pic'])) {
                    $jobImageData = $jobImage[0]['pic'];
                } else {
                    $jobImageData = $comPic;
                }
                       
                $data1[] = ["jobpost_id" => $jobfetch['id'], "recruiter_id" => $jobfetch['recruiter_id'], "comapnyId" => $jobfetch['compId'], "job_title" => $jobfetch['jobtitle'], "jobDesc" => $jobfetch['jobDesc'],"distance"=>$distance, "salary" => number_format($basicsalary), "companyName" => $cname, "cname"=> $cname,"jobPitch"=>$jobfetch['jobPitch'], "companyId" => $jobfetch['compId'], "jobstatus" => $jobstatus, "company_address" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image"=>$jobImageData, "cname"=>$jobfetch['cname'],"distance"=>$distance, "savedjob" => $jobstatus, "mode" => $jobfetch['mode']];
            }
            $data1;
        } else{
            $data1=[];
        }

            $jobfetchs = $this->Explorejobs_Model->job_fetch_homeforappNoExp($userID, $current_lat, $current_long, $expfilter);
            if ($jobfetchs) {
                foreach ($jobfetchs as $jobfetch) {

                    if(is_array($jobfetch['id'], $neglectarr)) { } else {

                        $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);
                        if ($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else {
                            $basicsalary = "0";
                        }
                        
                        $condtionssavedjob = array('jobpost_id' => $jobfetch['id'], 'user_id' => $userID);
                        $jobstatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                        if (isset($jobstatus[0]['status'])) {
                            $jobstatus = '1';
                        } else {
                            $jobstatus = '0';
                        }

                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobfetch['compId']);
                        if (isset($companydetail1[0]['address'])) {
                            $companyaddress = $companydetail1[0]['address'];
                        } else {
                            $companyaddress = '';
                        }
                        
                        $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                        if ($jobTop) {
                            if (isset($jobTop[0]['picks_id'])) {
                                $datatoppicks1 = $jobTop[0]['picks_id'];
                            } else {
                                $datatoppicks1 = "";
                            }
                            if (isset($jobTop[1]['picks_id'])) {
                                $datatoppicks2 = $jobTop[1]['picks_id'];
                            } else {
                                $datatoppicks2 = "";
                            }
                            if (isset($jobTop[2]['picks_id'])) {
                                $datatoppicks3 = $jobTop[2]['picks_id'];
                            } else {
                                $datatoppicks3 = "";
                            }
                        } else {
                            $datatoppicks1 = "";
                            $datatoppicks2 = "";
                            $datatoppicks3 = "";
                        }

                        $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['compId']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($current_lat, $current_long, $jobfetch['latitude'], $jobfetch['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', '');
                        $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['recruiter_id']);       
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }

                        $jobImage = $this->Jobpost_Model->job_image($jobfetch['id']);
                        if (isset($jobImage[0]['pic'])) {
                            $jobImageData = $jobImage[0]['pic'];
                        } else {
                            $jobImageData = $comPic;
                        }
                               
                        $data1[] = ["jobpost_id" => $jobfetch['id'], "recruiter_id" => $jobfetch['recruiter_id'], "comapnyId" => $jobfetch['compId'], "job_title" => $jobfetch['jobtitle'], "jobDesc" => $jobfetch['jobDesc'],"distance"=>$distance, "salary" => number_format($basicsalary), "companyName" => $cname, "cname"=> $cname,"jobPitch"=>$jobfetch['jobPitch'], "companyId" => $jobfetch['compId'], "jobstatus" => $jobstatus, "company_address" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image"=>$jobImageData, "cname"=>$jobfetch['cname'],"distance"=>$distance, "savedjob" => $jobstatus, "mode" => $jobfetch['mode']];
                    }
                }
                return $data1;
            } else{
                $data1=[];
                return $data1;
            }
    }

    public function getnursingJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray) {
        $data1 = array();
        $neglectarr = array();
        
        $jobfetchs = $this->Explorejobs_Model->job_fetch_homefornursing_groupby($userID, $current_lat, $current_long, $expfilter);
        if ($jobfetchs) {
            foreach ($jobfetchs as $jobfetch) {
                $neglectarr[] = $jobfetch['id'];
                $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);

                if ($jobsal[0]['basicsalary']) {
                    $basicsalary = $jobsal[0]['basicsalary'];
                } else {
                    $basicsalary = "0";
                }
                
                $condtionssavedjob = array('jobpost_id' => $jobfetch['id'], 'user_id' => $userID);
                $jobstatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                if (isset($jobstatus[0]['status'])) {
                    $jobstatus = '1';
                } else {
                    $jobstatus = '0';
                }

                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobfetch['compId']);
                if (isset($companydetail1[0]['address'])) {
                    $companyaddress = $companydetail1[0]['address'];
                } else {
                    $companyaddress = '';
                }
                
                $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                if ($jobTop) {
                    if (isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id'];
                    } else {
                        $datatoppicks1 = "";
                    }
                    if (isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id'];
                    } else {
                        $datatoppicks2 = "";
                    }
                    if (isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id'];
                    } else {
                        $datatoppicks3 = "";
                    }
                } else {
                    $datatoppicks1 = "";
                    $datatoppicks2 = "";
                    $datatoppicks3 = "";
                }

                $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['compId']);
                if(!empty($companyname[0]["cname"])){
                    $cname = $companyname[0]["cname"];
                }else{
                    $cname = '';
                }

                $distance = $this->distance($current_lat, $current_long, $jobfetch['latitude'], $jobfetch['longitude'], "K");
                $distance = number_format((float)$distance, 2, '.', '');
                $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['recruiter_id']);       
                if(!empty($companyPic[0]['companyPic'])) {
                    $comPic = trim($companyPic[0]['companyPic']);
                } else{
                    $comPic = "";
                }

                $jobImage = $this->Jobpost_Model->job_image($jobfetch['id']);
                if (isset($jobImage[0]['pic'])) {
                    $jobImageData = $jobImage[0]['pic'];
                } else {
                    $jobImageData = $comPic;
                }
                       
                $data1[] = ["jobpost_id" => $jobfetch['id'], "recruiter_id" => $jobfetch['recruiter_id'], "comapnyId" => $jobfetch['compId'], "job_title" => $jobfetch['jobtitle'], "jobDesc" => $jobfetch['jobDesc'],"distance"=>$distance, "salary" => number_format($basicsalary), "companyName" => $cname, "cname"=> $cname,"jobPitch"=>$jobfetch['jobPitch'], "companyId" => $jobfetch['compId'], "jobstatus" => $jobstatus, "company_address" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image"=>$jobImageData, "cname"=>$jobfetch['cname'],"distance"=>$distance, "savedjob" => $jobstatus, "mode" => $jobfetch['mode']];
            }
            $data1;
        } else{
            $data1=[];
        }

            $jobfetchs = $this->Explorejobs_Model->job_fetch_homefornursing($userID, $current_lat, $current_long, $expfilter);
            if ($jobfetchs) {
                foreach ($jobfetchs as $jobfetch) {

                    if(is_array($jobfetch['id'], $neglectarr)) { } else {

                        $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);
                        if ($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else {
                            $basicsalary = "0";
                        }
                        
                        $condtionssavedjob = array('jobpost_id' => $jobfetch['id'], 'user_id' => $userID);
                        $jobstatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                        if (isset($jobstatus[0]['status'])) {
                            $jobstatus = '1';
                        } else {
                            $jobstatus = '0';
                        }

                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobfetch['compId']);
                        if (isset($companydetail1[0]['address'])) {
                            $companyaddress = $companydetail1[0]['address'];
                        } else {
                            $companyaddress = '';
                        }
                        
                        $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                        if ($jobTop) {
                            if (isset($jobTop[0]['picks_id'])) {
                                $datatoppicks1 = $jobTop[0]['picks_id'];
                            } else {
                                $datatoppicks1 = "";
                            }
                            if (isset($jobTop[1]['picks_id'])) {
                                $datatoppicks2 = $jobTop[1]['picks_id'];
                            } else {
                                $datatoppicks2 = "";
                            }
                            if (isset($jobTop[2]['picks_id'])) {
                                $datatoppicks3 = $jobTop[2]['picks_id'];
                            } else {
                                $datatoppicks3 = "";
                            }
                        } else {
                            $datatoppicks1 = "";
                            $datatoppicks2 = "";
                            $datatoppicks3 = "";
                        }

                        $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['compId']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($current_lat, $current_long, $jobfetch['latitude'], $jobfetch['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', '');
                        $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['recruiter_id']);       
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }

                        $jobImage = $this->Jobpost_Model->job_image($jobfetch['id']);
                        if (isset($jobImage[0]['pic'])) {
                            $jobImageData = $jobImage[0]['pic'];
                        } else {
                            $jobImageData = $comPic;
                        }
                               
                        $data1[] = ["jobpost_id" => $jobfetch['id'], "recruiter_id" => $jobfetch['recruiter_id'], "comapnyId" => $jobfetch['compId'], "job_title" => $jobfetch['jobtitle'], "jobDesc" => $jobfetch['jobDesc'],"distance"=>$distance, "salary" => number_format($basicsalary), "companyName" => $cname, "cname"=> $cname,"jobPitch"=>$jobfetch['jobPitch'], "companyId" => $jobfetch['compId'], "jobstatus" => $jobstatus, "company_address" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image"=>$jobImageData, "cname"=>$jobfetch['cname'],"distance"=>$distance, "savedjob" => $jobstatus, "mode" => $jobfetch['mode']];
                    }
                }
                return $data1;
            } else{
                $data1=[];
                return $data1;
            }
    }

    public function getactivelyhiringJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray) {

        $data1 = array();
        $neglectarr = array();
        
        $jobfetchs = $this->Explorejobs_Model->job_fetch_homeforactively_groupby($userID, $current_lat, $current_long, $expfilter);
        if ($jobfetchs) {
            foreach ($jobfetchs as $jobfetch) {
                $neglectarr[] = $jobfetch['id'];
                $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);

                if ($jobsal[0]['basicsalary']) {
                    $basicsalary = $jobsal[0]['basicsalary'];
                } else {
                    $basicsalary = "0";
                }
                
                $condtionssavedjob = array('jobpost_id' => $jobfetch['id'], 'user_id' => $userID);
                $jobstatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                if (isset($jobstatus[0]['status'])) {
                    $jobstatus = '1';
                } else {
                    $jobstatus = '0';
                }

                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobfetch['compId']);
                if (isset($companydetail1[0]['address'])) {
                    $companyaddress = $companydetail1[0]['address'];
                } else {
                    $companyaddress = '';
                }
                
                $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                if ($jobTop) {
                    if (isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id'];
                    } else {
                        $datatoppicks1 = "";
                    }
                    if (isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id'];
                    } else {
                        $datatoppicks2 = "";
                    }
                    if (isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id'];
                    } else {
                        $datatoppicks3 = "";
                    }
                } else {
                    $datatoppicks1 = "";
                    $datatoppicks2 = "";
                    $datatoppicks3 = "";
                }

                $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['compId']);
                if(!empty($companyname[0]["cname"])){
                    $cname = $companyname[0]["cname"];
                }else{
                    $cname = '';
                }

                $distance = $this->distance($current_lat, $current_long, $jobfetch['latitude'], $jobfetch['longitude'], "K");
                $distance = number_format((float)$distance, 2, '.', '');
                $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['recruiter_id']);       
                if(!empty($companyPic[0]['companyPic'])) {
                    $comPic = trim($companyPic[0]['companyPic']);
                } else{
                    $comPic = "";
                }

                $jobImage = $this->Jobpost_Model->job_image($jobfetch['id']);
                if (isset($jobImage[0]['pic'])) {
                    $jobImageData = $jobImage[0]['pic'];
                } else {
                    $jobImageData = $comPic;
                }
                       
                $data1[] = ["jobpost_id" => $jobfetch['id'], "recruiter_id" => $jobfetch['recruiter_id'], "comapnyId" => $jobfetch['compId'], "job_title" => $jobfetch['jobtitle'], "jobDesc" => $jobfetch['jobDesc'],"distance"=>$distance, "salary" => number_format($basicsalary), "companyName" => $cname, "cname"=> $cname,"jobPitch"=>$jobfetch['jobPitch'], "companyId" => $jobfetch['compId'], "jobstatus" => $jobstatus, "company_address" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image"=>$jobImageData, "cname"=>$jobfetch['cname'],"distance"=>$distance, "savedjob" => $jobstatus, "mode" => $jobfetch['mode']];
            }
            $data1;
        } else{
            $data1=[];
        }

            $jobfetchs = $this->Explorejobs_Model->job_fetch_homeforactively($userID, $current_lat, $current_long, $expfilter);
            if ($jobfetchs) {
                foreach ($jobfetchs as $jobfetch) {

                    if(is_array($jobfetch['id'], $neglectarr)) { } else {

                        $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);
                        if ($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else {
                            $basicsalary = "0";
                        }
                        
                        $condtionssavedjob = array('jobpost_id' => $jobfetch['id'], 'user_id' => $userID);
                        $jobstatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                        if (isset($jobstatus[0]['status'])) {
                            $jobstatus = '1';
                        } else {
                            $jobstatus = '0';
                        }

                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobfetch['compId']);
                        if (isset($companydetail1[0]['address'])) {
                            $companyaddress = $companydetail1[0]['address'];
                        } else {
                            $companyaddress = '';
                        }
                        
                        $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                        if ($jobTop) {
                            if (isset($jobTop[0]['picks_id'])) {
                                $datatoppicks1 = $jobTop[0]['picks_id'];
                            } else {
                                $datatoppicks1 = "";
                            }
                            if (isset($jobTop[1]['picks_id'])) {
                                $datatoppicks2 = $jobTop[1]['picks_id'];
                            } else {
                                $datatoppicks2 = "";
                            }
                            if (isset($jobTop[2]['picks_id'])) {
                                $datatoppicks3 = $jobTop[2]['picks_id'];
                            } else {
                                $datatoppicks3 = "";
                            }
                        } else {
                            $datatoppicks1 = "";
                            $datatoppicks2 = "";
                            $datatoppicks3 = "";
                        }

                        $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['compId']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($current_lat, $current_long, $jobfetch['latitude'], $jobfetch['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', '');
                        $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['recruiter_id']);       
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }

                        $jobImage = $this->Jobpost_Model->job_image($jobfetch['id']);
                        if (isset($jobImage[0]['pic'])) {
                            $jobImageData = $jobImage[0]['pic'];
                        } else {
                            $jobImageData = $comPic;
                        }
                               
                        $data1[] = ["jobpost_id" => $jobfetch['id'], "recruiter_id" => $jobfetch['recruiter_id'], "comapnyId" => $jobfetch['compId'], "job_title" => $jobfetch['jobtitle'], "jobDesc" => $jobfetch['jobDesc'],"distance"=>$distance, "salary" => number_format($basicsalary), "companyName" => $cname, "cname"=> $cname,"jobPitch"=>$jobfetch['jobPitch'], "companyId" => $jobfetch['compId'], "jobstatus" => $jobstatus, "company_address" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image"=>$jobImageData, "cname"=>$jobfetch['cname'],"distance"=>$distance, "savedjob" => $jobstatus, "mode" => $jobfetch['mode']];
                    }
                }
                return $data1;
            } else{
                $data1=[];
                return $data1;
            }
    }

    public function getexloreinstantJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray) {

        $instantjobs = array();
        $neglectarr = array();

        $catjobDatas = $this->Explorejobs_Model->instantscreening_fetch_groupby($userID, $expfilter, $current_lat, $current_long);
        foreach ($catjobDatas as $catjobData) {

            $neglectarr[] = $catjobData['id'];

            $jobTop = $this->Jobpost_Model->job_toppicks($catjobData['id']);
            
                if(isset($jobTop[0]['picks_id'])) {
                    $datatoppicks1 = $jobTop[0]['picks_id']; 
                } else{
                    $datatoppicks1 = "";
                }
                if(isset($jobTop[1]['picks_id'])) {
                    $datatoppicks2 = $jobTop[1]['picks_id']; 
                } else{
                    $datatoppicks2 = "";
                }
                if(isset($jobTop[2]['picks_id'])) {
                    $datatoppicks3 = $jobTop[2]['picks_id']; 
                } else{
                    $datatoppicks3 = "";
                }
                
                $savedjob = $this->Jobpost_Model->job_saved($catjobData['id'], $userTokenCheck[0]['user_id']);
                if(!empty($savedjob[0]['id'])) {
                    $savedjob = '1';
                } else{
                    $savedjob = "0";
                }

                $jobsal = $this->Jobpost_Model->getExpJob($catjobData['id']);
                if($jobsal[0]['basicsalary']) {
                    $basicsalary = $jobsal[0]['basicsalary'];
                } else{
                    $basicsalary = 0;
                }

                $companydetail = $this->Jobpost_Model->company_detail_fetch($catjobData['compId']);
                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($catjobData['compId']);
                if(isset($companydetail1[0]['address'])){
                    $companyaddress=$companydetail1[0]['address'];
                }
                else{
                    $companyaddress='';
                }

                $companyPic = $this->Jobpost_Model->fetch_companyPic($catjobData['compId']);
                if(!empty($companyPic[0]['companyPic'])) {
                    $comPic = trim($companyPic[0]['companyPic']);
                } else{
                    $comPic = "";
                }

                $jobImage = $this->Jobpost_Model->job_image($catjobData['id']);
                if(isset($jobImage[0]['pic'])){
                    $jobImageData=$jobImage[0]['pic'];
                }
                else{
                    $jobImageData=$comPic;
                }

                $companyname = $this->Jobpost_Model->company_name_fetch($catjobData['recruiter_id']);
                if(!empty($companyname[0]["cname"])){
                    $cname = $companyname[0]["cname"];
                }else{
                    $cname = '';
                }

                $distance = $this->distance($current_lat, $current_long, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                $distance = number_format((float)$distance, 2, '.', ''); 

                $instantjobs[] = [
                        "jobpost_id" => $catjobData['id'],
                        "comapnyId" =>$catjobData['compId'],
                        "job_title" => $catjobData['jobtitle'],
                        "jobDesc" => $catjobData['jobDesc'],
                        "jobPitch"=>$catjobData['jobPitch'],
                        "salary" => number_format($catjobData['salary']),
                        "companyName"=> $companydetail[0]["cname"],
                        "cname"=> $cname,
                        "companyAddress"=>$companyaddress,
                        "toppicks1" => $datatoppicks1,
                        "toppicks2" => $datatoppicks2,
                        "toppicks3" => $datatoppicks3,
                        "job_image" =>$jobImageData,
                        "latitude"=> $catjobData['latitude'], 
                        "longitude"=>$catjobData['longitude'],
                        "jobexpire"=> $catjobData['jobexpire'],
                        "companyPic"=> $comPic,
                        "savedjob" =>$savedjob,
                        "boostjob" =>$catjobData['boost_status'],
                        "distance" => $distance,
                        "mode"=> $catjobData['mode'],
                ];
            //}
        }

        $catjobDatas = $this->Explorejobs_Model->instantscreening_fetch($userID, $expfilter, $current_lat, $current_long);
        foreach ($catjobDatas as $catjobData) {

            if(in_array($catjobData['id'], $neglectarr)) { } else {
                $jobTop = $this->Jobpost_Model->job_toppicks($catjobData['id']);
          
                if(!empty($jobTop) && count($jobTop)>=0) {
                      //echo "</pre>";    
                    if(isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }
                    
                    $savedjob = $this->Jobpost_Model->job_saved($catjobData['id'], $userTokenCheck[0]['user_id']);
                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }

                    $jobsal = $this->Jobpost_Model->getExpJob($catjobData['id']);
                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = 0;
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($catjobData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($catjobData['compId']);
                    if(isset($companydetail1[0]['address'])){
                        $companyaddress=$companydetail1[0]['address'];
                    }
                    else{
                        $companyaddress='';
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($catjobData['compId']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($catjobData['id']);
                    if(isset($jobImage[0]['pic'])){
                        $jobImageData=$jobImage[0]['pic'];
                    }
                    else{
                        $jobImageData=$comPic;
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($catjobData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', ''); 

                    $instantjobs[] = [
                            "jobpost_id" => $catjobData['id'],
                            "comapnyId" =>$catjobData['compId'],
                            "job_title" => $catjobData['jobtitle'],
                            "jobDesc" => $catjobData['jobDesc'],
                            "jobPitch"=>$catjobData['jobPitch'],
                            "salary" => number_format($catjobData['salary']),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $catjobData['latitude'], 
                            "longitude"=>$catjobData['longitude'],
                            "jobexpire"=> $catjobData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$catjobData['boost_status'],
                            "distance" => $distance,
                            "mode"=> $catjobData['mode'],
                    ];
                }
            }
        }
        return $instantjobs;
    }

    public function distance($lat1, $lon1, $lat2, $lon2, $unit) {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) *sin(deg2rad($lat2)) + cos(deg2rad($lat1))* cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist* 60 *1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }
}
?>

