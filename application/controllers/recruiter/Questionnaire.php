<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */
class Questionnaire extends CI_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('newmodels/recruiter/Recruit_Model');
        $this->load->model('newmodels/Common_Model');
        $this->load->model('newmodels/recruiter/Jobpost_Model');
        $this->load->model('newmodels/Subscription_Model');
        $this->load->model('newmodels/Recruiteradmin_Model');
        $this->load->model('newmodels/recruiter/Questionnaire_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');
        $this->load->helper('cookie');
    }

    public function questionnaire_add() {

        $in = base64_decode($this->uri->segment(3));
        $data['jobid'] = $in;
        $this->load->view('recruiter/questionnaire_add', $data);
    }

    public function questionnairesave() {
        $data = $this->input->post();
        
        if(count($data['question']) <= 0 || strlen($data['question'][0]) < 1) {
            redirect("recruiter/questionnaire");
        }
        $saveData = array();
        for($x=0; $x < count($data['question']); $x++) {
            $y = $x + 1;
            if(strlen($data['question'][$x]) > 0) {
                $reOptions = array();
                foreach($data["options$y"] as $getoptions) {
                    if(strlen($getoptions) > 0) {
                        $reOptions[] = $getoptions;
                    }
                }
                $saveData = ["jobpost_id"=>$data['jobid'], "question"=>$data['question'][$x], "type"=>$data['type'][$x], "options"=> implode(',',$reOptions), "answer"=> $data["answer"][$x], "haverequired"=>$data['haverequired'][$x]??0 ];

                $this->Questionnaire_Model->questionnaire_save($saveData);
            }
        }
        
        $this->session->set_tempdata('inserted', 'Job Posted Successfully',5);
        redirect("recruiter/jobpost/manageJobView");
    }

    public function questionnaireedit() {
        $in = base64_decode($this->uri->segment(3));
        $data['jobid'] = $in;

        $quesDatas = $this->Questionnaire_Model->questionnaire_fetch($data['jobid']);
        if($quesDatas) {
            foreach($quesDatas as $quesData) {
                $data['questionData'][] = ["question"=>$quesData['question'], "type"=>$quesData['type'], "options"=>explode(',', $quesData['options']), "answers"=>$quesData['answer'], "haverequired"=>$quesData['haverequired']];
            }
            $this->load->view('recruiter/questionnaire_edit', $data);
        } else {
            redirect("recruiter/questionnaire/". base64_encode($data['jobid']));
        }
    }

    public function questionnaireupdate() {
        $data = $this->input->post();
        
        if(count($data['question']) <= 0 || strlen($data['question'][0]) < 1) {
            redirect("recruiter/questionnaire");
        }
        
        $this->Questionnaire_Model->questionnaire_delete($data['jobid']);

        $saveData = array();
        for($x=0; $x < count($data['question']); $x++) {
            $y = $x + 1;
            if(strlen($data['question'][$x]) > 0) {

                $reOptions = array();
                foreach($data["options$y"] as $getoptions) {
                    if(strlen($getoptions) > 0) {
                        $reOptions[] = $getoptions;
                    }
                }

                $saveData = ["jobpost_id"=>$data['jobid'], "question"=>$data['question'][$x], "type"=>$data['type'][$x], "options"=> implode(',',$reOptions), "answer"=> $data["answer"][$x], "haverequired"=>$data['haverequired'][$x]??0 ];

                $this->Questionnaire_Model->questionnaire_save($saveData);
            }
        }
        //var_dump($saveData);die;
        
        $this->session->set_tempdata('inserted', 'Job Posted Successfully',5);
        redirect("recruiter/jobpost/manageJobView");
    }

    public function questionnaire_view() {

        $in = base64_decode($this->uri->segment(3));
        $data['jobid'] = $in;

        $quesDatas = $this->Questionnaire_Model->questionnaire_fetch($data['jobid']);
        foreach($quesDatas as $quesData) {
            $data['questionData'][] = ["question"=>$quesData['question'], "type"=>$quesData['type'], "options"=>explode(',', $quesData['options']), "answers"=>$quesData['answer'], "haverequired"=>$quesData['haverequired']];
        }
        $this->load->view('recruiter/questionnaire_view', $data);
    }

    public function questionnaire_user_view() {

        $in = base64_decode($this->uri->segment(3));
        $user_id = base64_decode($this->uri->segment(4));
        $data['jobid'] = $in;

        $quesDatas = $this->Questionnaire_Model->questionnaire_fetch($data['jobid']);
        $data["score"] =  0;

        foreach($quesDatas as $quesData) {

            $userDatas = $this->Questionnaire_Model->questionnaire_user_fetch($data['jobid'], $quesData['id'], $user_id);
            if($userDatas) {
                $data['questionData'][] = ["question"=>$quesData['question'], "type"=>$quesData['type'], "options"=>explode(',', $quesData['options']), "answers"=>$quesData['answer'], "useranswer"=>$userDatas[0]['answer'], "haverequired"=>$quesData['haverequired']];

                $data["score"] =  $userDatas[0]['percentage'];
                $data["scoreResult"] =  $userDatas[0]['result'];
            }
        }
        $this->load->view('recruiter/questionnaire_user_view', $data);
    }
}

   