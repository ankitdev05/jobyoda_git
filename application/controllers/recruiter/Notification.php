<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */
class Notification extends CI_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('newmodels/recruiter/Recruit_Model');
        $this->load->model('newmodels/Common_Model');
        $this->load->model('newmodels/recruiter/Jobpost_Model');
        $this->load->model('newmodels/recruiter/Candidate_Model');
        $this->load->model('newmodels/Recruiteradmin_Model');
        $this->load->model('newmodels/Subscription_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');
        $this->load->helper('cookie');
        if($this->session->userdata('userSession')) {
            
        } else {
            redirect("recruiter/recruiter/index");
        }
        
    }

    public function index() {
        $userSession = $this->session->userdata('userSession');
         if(!empty($userSession['label']) && $userSession['label']=='3'){
              $subrecruiter_id = $userSession['id'];
              $userSession['id'] = $userSession['parent_id'];
          }else{
              $userSession['id'] = $userSession['id'];
              $subrecruiter_id = 0;
          }
        
        $data['candidatesApplied'] = $this->fetchpending($userSession['id'],$subrecruiter_id); 
        $data["languages"] = $this->Common_Model->language_lists();
        $data["industries"] = $this->Common_Model->industry_lists();
        $data['skillData'] = $this->Candidate_Model->skills_fetch();
        $data['clients'] = $this->Recruit_Model->user_clientSupported();
        $data['levels'] = $this->Jobpost_Model->level_lists();
        $data['category'] = $this->Jobpost_Model->category_lists();
        $data['subcategory'] = $this->Jobpost_Model->subcategory_lists();
        $this->load->view('recruiter/notification', $data);
    }

    public function fetchpending($userSession,$subrecruiter_id) {
        $data["pendingData"] = [];
        
        $pending = $this->Candidate_Model->candidateduenotification_list($userSession,$subrecruiter_id); 
        
        if (!empty($pending)) {
            foreach ($pending as $pendings) {
                $enddate =  date('Y-m-d', strtotime($pendings['updated_at']. ' + 3 days'));
                $enddate = strtotime($enddate);
                $updated_date=  strtotime($pendings['updated_at']);
                $now = time();
                $datediff =  $now - $updated_date;

                $daydiff = floor($datediff / (60*60*24));
                $data["pendingData"][] =["profilePic"=>$pendings['profilePic'],"user_id"=>$pendings['user_id'], "name"=>$pendings['name'], "email"=>$pendings['email'], 'phone'=>$pendings['phone'], "location"=>$pendings['location'], "status"=>$pendings['status'], "interviewdate" => $pendings['interviewdate'], 'interviewtime' => $pendings['interviewtime'] , "date_day1" => $pendings['date_day1'], 'jobDesc' => $pendings['jobDesc'], "jobtitle"=>$pendings['jobtitle'],"mode"=>$pendings['mode'], "compid"=>$pendings['compid'], "id"=>$pendings['id'], 'user_id'=>$pendings['user_id'],'jobpost_id'=>$pendings['jobpost_id'], 'updated_at'=> $pendings['updated_at'], 'fallout_reason' => $pendings['fallout_reason']];

            }
        }
        return $data["pendingData"];
    }
   
}
?>  
