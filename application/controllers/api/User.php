<?php
ob_start();
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */
class User extends REST_Controller {
    
    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('newmodels/User_Model');
        $this->load->model('newmodels/Common_Model');
        $this->load->model('newmodels/recruiter/Jobpost_Model');
        $this->load->model('newmodels/recruiter/Candidate_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');
        $key = $this->encryption->create_key(10);
    }
    // http://localhost/jobyodha/api/example/users/id/1
    
    public function phoneVerify_post() {
        $userData = $this->input->post();   
        $this->form_validation->set_rules('phone', 'phone', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|required');

        if(!empty($userData['email'])) {
            $phoneMatch = $this->User_Model->checkemail($userData['email']);    
            if(count($phoneMatch) > 0) {
                if($phoneMatch[0]['type']=='normal') {
                    $type='You have already registered with this email. Please login with your email address.';
                }else if($phoneMatch[0]['type']=='gmail'){
                    $type = "You have already registered with this email # with your Google ID.Please login with your Google ID";
                }else if($phoneMatch[0]['type']=='facebook'){
                    $type = "You have already registered with this email # with your Facebook ID.Please login with your Facebook ID";
                }else if($phoneMatch[0]['type']=='google'){
                    $type = "You have already registered with this email # with your Google ID.Please login with your Google ID";
                }

                $response = ['message' => $type, 'status' => "FAILURE"]; 
                return $this->set_response($response, REST_Controller::HTTP_CREATED);
                die;
            }
        }

        if(!empty($userData['phone'])) {
            $phoneMatch = $this->User_Model->checkphone($userData['phone']);    
            if(count($phoneMatch) > 0) {
                if($phoneMatch[0]['type']=='normal') {
	                $type='You have already registered with this phone number using your email. Please login with your email address.';
	            }else if($phoneMatch[0]['type']=='gmail'){
	                $type = "You have already registered with this Phone # with your Google ID.Please login with your Google ID";
	            }else if($phoneMatch[0]['type']=='facebook'){
	                $type = "You have already registered with this Phone # with your Facebook ID.Please login with your Facebook ID";
	            }else if($phoneMatch[0]['type']=='google'){
	                $type = "You have already registered with this Phone # with your Google ID.Please login with your Google ID";
	            }

                $response = ['message' => $type, 'status' => "FAILURE"]; 
                return $this->set_response($response, REST_Controller::HTTP_CREATED);
                die;
	        }
        }
        
        $response = ['message' => "Phone number and Email id does not exists", 'status' => "SUCCESS"];
        return $this->set_response($response, REST_Controller::HTTP_CREATED);
        die;
    }

    // normal Signup

    public function signup_post() {

        $userData = $this->input->post();
        
        if($userData['signupType'] == "normal") {
            
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
            $this->form_validation->set_rules('country_code', 'country_code', 'trim|required');
            $this->form_validation->set_rules('phone', 'phone', 'trim|required');
            $this->form_validation->set_rules('password', 'password', 'trim|required|matches[password]');
            $this->form_validation->set_rules('location', 'Location', 'trim|required');
            $this->form_validation->set_rules('state', 'State', 'trim');
            $this->form_validation->set_rules('city', 'City', 'trim');
            $this->form_validation->set_rules('education', 'Education', 'trim|required');
            $this->form_validation->set_rules('industry', 'Industry', 'trim|required');
            $this->form_validation->set_rules('specialization', 'Specialization', 'trim|required');
            $this->form_validation->set_rules('sub_specialization', 'Sub-Specialization', 'trim|required');
            $this->form_validation->set_rules('joblevel', 'Job Level', 'trim|required');
            $this->form_validation->set_rules('exp_month', 'Exp Month', 'trim|required');
            $this->form_validation->set_rules('exp_year', 'Exp Year', 'trim|required');
            $this->form_validation->set_rules('jobsInterested', 'Job intrested In', 'trim|required');
            $this->form_validation->set_rules('internetspeed', 'Internet Speed', 'trim');

            $this->form_validation->set_rules('device_type', 'device type', 'trim|required');
            $this->form_validation->set_rules('device_token', 'device token', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $errors = $this->form_validation->error_array();
                $error = current($errors);
                $response = ['message' => $error, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);

            } else {
                $hash = $this->encryption->encrypt($userData['password']);
                $token = $this->User_Model->randomstring();

                $data = [
                    'name' => ucwords(strtolower($userData['name'])),
                    'email' => $userData['email'],
                    'country_code' => $userData['country_code'],
                    'phone' => $userData['phone'],
                    'password' => $hash,
                    'type' => "normal",
                    'token' => $token,
                    'status' => 1,
                    'active' => 1,
                    'verify_status' => 1,
                    'device_type' => $userData['device_type'],
                    'device_token' => $userData['device_token'],
                    'location' => $userData['location'],
                    'state' => $userData['state'],
                    'city' => $userData['city'],
                    'education' => $userData['education'],
                    'industry' => $userData['industry'],
                    'specialization' => $userData['specialization'],
                    'sub_specialization' => $userData['sub_specialization'],
                    'superpower' => $userData['specialization'],
                    'joblevel' => $userData['joblevel'],
                    'exp_year' => $userData['exp_year'],
                    'exp_month' => $userData['exp_month'],
                    'jobsInterested' => $userData['jobsInterested'],
                    'internetspeed' => $userData['internetspeed'],
                ];

                $userInserted = $this->User_Model->user_insert($data);
                
                if($userInserted) {
                    $tokenData = ['user_id'=> $userInserted,
                                'token' => $token,
                                'device_type' => $userData['device_type'],
                                'device_token' => $userData['device_token'],
                    ];
                    $tokenInserted = $this->User_Model->usertoken_insert($tokenData);

                    if(!empty($userData['resume'])) {
                        $this->User_Model->user_resume_insert($resumePath, $userInserted);
                        $resume_percent = 10;
                    } else {
                        $resume_percent = 0;
                    }
                    
                    $comProfile = ["user_id"=>$userInserted, "personal"=>10,"proffesional"=>10, "other"=>10, "resume" => $resume_percent];
                    $this->User_Model->insert_profileComplete($comProfile);
                    if(!empty($userData['resume'])) {
                        $this->User_Model->user_resume_insert($userResume, $userInserted);
                    }

                    $getData = $this->fetchUserSingleDataByToken($userInserted, $userData['device_token'], $userData['device_type']);
                    $getCompletenes = $this->fetchUserCompleteData($userInserted);
                    $getData['completeness'] = $getCompletenes['comp'];

                    $rating = $this->calculateRating($userInserted);
                    $response = ['signup' => $getData, 'status' => "SUCCESS"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                
                } else {
                    $errors = "Data not inserted";
                    $response = ['message' => $errors, 'status' => "FAILURE"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
            }
        
        } else {
        
            $this->form_validation->set_rules('socialToken', 'Social Token', 'trim|required');
            $this->form_validation->set_rules('signupType', 'Social Type', 'trim|required');
            $this->form_validation->set_rules('device_token', 'Device Token', 'trim|required');
            $this->form_validation->set_rules('device_type', 'Device Type', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $errors = $this->form_validation->error_array();
                $error = current($errors);
                $response = ['message' => $error, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);

            } else {

                $token = $this->User_Model->randomstring();
                
                $socialCheck = $this->User_Model->user_socialagain($userData['socialToken'], $userData['signupType']);
                
                if(!$socialCheck) {
                    
                    $data = [
                        'name' => ucwords(strtolower($userData['name'])),
                        'email' => '',
                        'type' => $userData['signupType'],
                        'token' => $token,
                        'socialToken' => $userData['socialToken'],
                        'profilePic' => $userData['profilePic'],
                        'status' => 0,
                        'device_type' => $userData['device_type'],
                        'device_token' => $userData['device_token'],
                    ];
                    $userInserted = $this->User_Model->user_insert($data);
                    
                    if($userInserted) {
                        $tokenData = ['user_id'=> $userInserted,
                                    'token' => $token,
                                    'device_type' => $userData['device_type'],
                                    'device_token' => $userData['device_token'],
                        ];

                        $tokenInserted = $this->User_Model->usertoken_insert($tokenData);
                        $getData = $this->fetchUserSingleDataByToken($userInserted, $userData['device_token'], $userData['device_type']);
                        
                        $rating = $this->calculateRating($userInserted);
                        $response = ['signup' => $getData, 'rating' => $rating, 'status' => "ALREADY"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    } else{
                        $errors = "Data not inserted";
                        $response = ['message' => $errors, 'status' => "FAILURE"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }

                } else {

                    if($socialCheck[0]['status'] == 0) {

                        $response = ['signup' => $userData, 'status' => "ALREADY"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);

                    } else {

                        $token = $this->User_Model->randomstring();
                        
                        if($socialCheck[0]['verify_status'] == 1) {

                            $dataCheck = ['user_id'=>$socialCheck[0]['id'], 'device_token'=>$userData['device_token'],'device_type'=>$userData['device_type']];
                            $checkToken = $this->User_Model->user_token_check($dataCheck);

                            if($checkToken) {
                                $data = ["token" => $token];
                                $this->User_Model->update_tokenagain($data, $socialCheck[0]['id'], $userData['device_token']);
                            } else {
                                $tokenData = ['user_id'=> $socialCheck[0]['id'],
                                        'token' => $token,
                                        'device_type' => $userData['device_type'],
                                        'device_token' => $userData['device_token'],
                                    ];
                                $tokenInserted = $this->User_Model->usertoken_insert($tokenData);
                            }

                            $getData = $this->fetchUserSingleDataByToken($socialCheck[0]['id'], $userData['device_token'], $userData['device_type']);


                            $rating = $this->calculateRating($socialCheck[0]['id']);
                            $response = ['signup' => $getData, 'rating' => $rating, 'status' => "SUCCESS"];
                            $this->set_response($response, REST_Controller::HTTP_CREATED);

                        } else {

                            $response = ['data'=>["user_id"=>$socialCheck[0]['id'],"phone"=>$socialCheck[0]['phone'],"country_code"=>$socialCheck[0]['country_code']], 'message' => "User is not verified", 'status' => "FAILURE"];
                            $this->set_response($response, REST_Controller::HTTP_CREATED);
                        }
                    }
                }
            }
        }
    }

    // Social signup first Step
    public function signup_post() {

        $userData = $this->input->post();
        
        if($userData['signupType'] == "normal") {
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|is_unique[user.email]');
            $this->form_validation->set_rules('country_code', 'country_code', 'trim|required');
            $this->form_validation->set_rules('phone', 'phone', 'trim|required');
            $this->form_validation->set_rules('password', 'password', 'trim|required');
            $this->form_validation->set_rules('confirmPassword', 'confirm password', 'trim|required|matches[password]');
            $this->form_validation->set_rules('device_type', 'device type', 'trim|required');
            $this->form_validation->set_rules('device_token', 'device token', 'trim|required');

            $this->form_validation->set_rules('location', 'Location', 'trim|required');
            $this->form_validation->set_rules('exp_month', 'Exp Month', 'trim|required');
            $this->form_validation->set_rules('exp_year', 'Exp Year', 'trim|required');
            $this->form_validation->set_rules('education', 'Education', 'trim|required');
            $this->form_validation->set_rules('superpower', 'superpower', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $errors = $this->form_validation->error_array();
                $error = current($errors);
                $response = ['message' => $error, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);

            } else {
                $hash = $this->encryption->encrypt($userData['password']);
                
                $token = $this->User_Model->randomstring();
                $data = [
                    'name' => ucwords(strtolower($userData['name'])),
                    'email' => $userData['email'],
                    'country_code' => $userData['country_code'],
                    'phone' => $userData['phone'],
                    'password' => $hash,
                    'type' => "normal",
                    'token' => $token,
                    'status' => 1,
                    'device_type' => $userData['device_type'],
                    'device_token' => $userData['device_token'],
                    'referral_used' => $userData['referral_code'],
                    'exp_month' => $userData['exp_month'],
                    'exp_year' => $userData['exp_year'],
                    'education' => $userData['education'],
                    'superpower' => $userData['superpower'],
                    'location' => $userData['location'],
                    'nationality' => $userData['nationality']
                ];
                
                $userInserted = $this->User_Model->user_insert($data);
                
                if($userInserted) {
                    $tokenData = ['user_id'=> $userInserted,
                                'token' => $token,
                                'device_type' => $userData['device_type'],
                                'device_token' => $userData['device_token'],
                    ];
                    $tokenInserted = $this->User_Model->usertoken_insert($tokenData);
                    $comProfile = ["user_id"=>$userInserted, "personal"=>10,"proffesional"=>10, "other"=>10, "resume" => 0];
                    $this->User_Model->insert_profileComplete($comProfile);

                    $getData = $this->fetchUserSingleDataByToken($userInserted, $userData['device_token'], $userData['device_type']);

                    $getCompletenes = $this->fetchUserCompleteData($userInserted);
                    $getData['completeness'] = $getCompletenes['comp'];
                    $rating = $this->calculateRating($userInserted);
                    $response = ['signup' => $getData, 'rating'=>$rating, 'status' => "SUCCESS"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                } else{
                    $errors = "Data not inserted";
                    $response = ['message' => $errors, 'status' => "FAILURE"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
            }
        } else {
            $token = $this->User_Model->randomstring();

            $socialCheck = $this->User_Model->user_socialagain($userData['socialToken'], $userData['signupType']);
            if(!$socialCheck) {
                
                $data = [
                    'name' => ucwords(strtolower($userData['name'])),
                    'email' => '',
                    'type' => $userData['signupType'],
                    'token' => $token,
                    'socialToken' => $userData['socialToken'],
                    'profilePic' => $userData['profilePic'],
                    'status' => 0,
                    'device_type' => $userData['device_type'],
                    'device_token' => $userData['device_token'],
                ];
                $userInserted = $this->User_Model->user_insert($data);
                
                if($userInserted) {
                    $tokenData = ['user_id'=> $userInserted,
                                'token' => $token,
                                'device_type' => $userData['device_type'],
                                'device_token' => $userData['device_token'],
                    ];

                    $tokenInserted = $this->User_Model->usertoken_insert($tokenData);
                    $getData = $this->fetchUserSingleDataByToken($userInserted, $userData['device_token'], $userData['device_type']);
                    $rating = $this->calculateRating($userInserted);
                    $response = ['signup' => $getData, 'rating' => $rating, 'status' => "ALREADY"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                } else{
                    $errors = "Data not inserted";
                    $response = ['message' => $errors, 'status' => "FAILURE"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
            } else {

                if($socialCheck[0]['status'] == 0) {

                    $getData = $this->fetchUserSingleData($socialCheck[0]['id']);

                    $dataCheck = ['user_id'=>$socialCheck[0]['id'],'device_token'=>$userData['device_token'],'device_type'=>$userData['device_type']];
                    $checkToken = $this->User_Model->user_token_check($dataCheck);

                    if($checkToken) {
                        $data = ["token" => $token];
                        $this->User_Model->update_tokenagain($data, $socialCheck[0]['id'], $userData['device_token']);
                    } else {
                        $tokenData = ['user_id'=> $socialCheck[0]['id'],
                                'token' => $token,
                                'device_type' => $userData['device_type'],
                                'device_token' => $userData['device_token'],
                            ];
                        $tokenInserted = $this->User_Model->usertoken_insert($tokenData);
                    }
                    $getData = $this->fetchUserSingleDataByToken($socialCheck[0]['id'], $userData['device_token'], $userData['device_type']);
                    $response = ['signup' => $getData, 'status' => "ALREADY"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);

                } else {
                    $dataCheck = ['user_id'=>$socialCheck[0]['id'],'device_token'=>$userData['device_token'],'device_type'=>$userData['device_type']];
                    $checkToken = $this->User_Model->user_token_check($dataCheck);

                    if($checkToken) {
                        $data = ["token" => $token];
                        $this->User_Model->update_tokenagain($data, $socialCheck[0]['id'], $userData['device_token']);
                    } else {
                        $tokenData = ['user_id'=> $socialCheck[0]['id'],
                                'token' => $token,
                                'device_type' => $userData['device_type'],
                                'device_token' => $userData['device_token'],
                            ];
                        $tokenInserted = $this->User_Model->usertoken_insert($tokenData);
                    }
                    $getData = $this->fetchUserSingleDataByToken($socialCheck[0]['id'], $userData['device_token'], $userData['device_type']);
                    $rating = $this->calculateRating($socialCheck[0]['id']);
                    $response = ['signup' => $getData, 'rating' => $rating , 'status' => "SUCCESS"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
            }
        }
    }

    // Social Signup
    public function socialSignup_post() {
        $userData = $this->input->post();
        $data = [
                'name' => ucwords(strtolower($userData['name'])),
                'socialToken' => $userData['socialToken'],
                'type' => $userData['signupType']
        ];

        $token = $this->User_Model->randomstring();
        $socialCheck = $this->User_Model->user_social($data['socialToken'], $data['type']);
        if($socialCheck) {
            
            $this->form_validation->set_rules('phone', 'phone', 'trim|required');
            $this->form_validation->set_rules('country_code', 'country_code', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                $errors = $this->form_validation->error_array();
                $error = current($errors);
                $response = ['message' => $error, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            
            } else {
                
                $hash = "12weqw";
                $moreData = ["name"=> $userData['name'], "email"=> $userData['email'],  "country_code"=>$userData['country_code'], "phone"=> $userData['phone'], "password"=> $hash, "status" => 1, 'device_type' => $userData['device_type'],'device_token' => $userData['device_token'], 'referral_used' => $userData['referral_code']];
                $getCompleteness = $this->User_Model->check_profileComplete($socialCheck[0]['id']);
                if($getCompleteness) {
                    if($getCompleteness[0]['signup'] == 0) {
                        $profileComplete = 10;
                        $comProfile = ["signup" => $profileComplete];
                        $this->User_Model->update_profileComplete($comProfile, $socialCheck[0]['id']);
                    }
                } else{
                    $profileComplete = 10;
                    $comProfile = ["user_id"=>$socialCheck[0]['id'], "signup" => $profileComplete];
                    $this->User_Model->insert_profileComplete($comProfile);
                }

                $getData = $this->fetchUserSingleData($socialCheck[0]['id']);
                $getCompletenes = $this->fetchUserCompleteData($socialCheck[0]['id']);
                $rating = $this->calculateRating($socialCheck[0]['id']);
                $getData['completeness'] = $getCompletenes['comp'];
				$userUpdated = $this->User_Model->user_update($moreData, $socialCheck[0]['id']);
                if(strlen($getData['relocate']) > 0 || strlen($getData['vaccination']) > 0 || strlen($getData['work_mode']) > 0) {
                    $allow = "not allow";
                } else {
                    $allow = "allow";
                }
                $response = ['socialSignup' => $getData, 'checkscreen'=>$allow, 'rating' => $rating , 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
        }
    }

    // Login
    public function login_post() {

        $userData = $this->input->post();
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
        $this->form_validation->set_rules('device_token', 'device token', 'trim|required');
        $this->form_validation->set_rules('device_type', 'device type', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {
            $password = $userData['password'];
            $data = ["email" => $userData['email'], "type"=>"normal"];
            $userCheck = $this->User_Model->user_login($data);
            if($userCheck) {

                $getPass = $userCheck[0]['password'];
                $vPass = $this->encryption->decrypt($getPass);
                
                if($password == $vPass) {
                    
                    if($userCheck[0]['active'] == 1) {

                        $token = $this->User_Model->randomstring();
                        $tokenData = ['user_id'=> $userCheck[0]['id'],
                            'token' => $token,
                            'device_type' => $userData['device_type'],
                            'device_token' => $userData['device_token'],
                        ];

                        $tokens = ["device_token" => $userData['device_token'], "user_id" => $userCheck[0]['id']];                        
                        $userTokenCheck1 = $this->User_Model->token_match1($tokens);
                        if(empty($userTokenCheck1)) {
                            
                            $this->User_Model->usertoken_delete($userCheck[0]['id'], $tokenData['device_token']);

                            $tokenInserted = $this->User_Model->usertoken_insert($tokenData);
                        } else {
                            $tokenData = ['token' => $token];
                            $tokenUpdated = $this->User_Model->update_usertoken($tokenData, $userData['device_token'], $userCheck[0]['id']);
                        }
                        
                        $getData = $this->fetchUserSingleDataByToken($userCheck[0]['id'], $userData['device_token'], $userData['device_type']);

                        $getCompletenes = $this->fetchUserCompleteData($userCheck[0]['id']);
                        $getData['completeness'] = $getCompletenes['comp'];
                        $rating = $this->calculateRating($userCheck[0]['id']);
                        $response = ['login' => $getData, 'rating' => $rating , 'status' => "SUCCESS"];
                    } else{
                        $response = ['message' => "User is blocked", 'status' => "FAILURE"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                } else {
                    $response = ['message' => "Password is incorrect", 'status' => "FAILURE"];
                }
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            } else{
                $response = ['message' => "Email is not yet registered", 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    // Change Password
    public function changePassword_post() {
        $userData = $this->input->post();
        $this->form_validation->set_rules('token', 'token', 'trim|required');
        $this->form_validation->set_rules('oldPassword', 'oldPassword', 'trim|required');
        $this->form_validation->set_rules('newPassword', 'newPassword', 'trim|required');
        $this->form_validation->set_rules('confirmPassword', 'confirmPassword', 'trim|required|matches[newPassword]');
        
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {
            $data = ["token" => $userData['token']];
            $userTokenCheck1 = $this->User_Model->token_match1($data);
            if($userTokenCheck1){
                $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
                $userTokenCheck = $this->User_Model->token_match($tokendata);
                if($userTokenCheck) {
                    $userPass = $this->encryption->decrypt($userTokenCheck[0]['password']);
                    
                    if($userPass == $userData['oldPassword']) {
                        $hash = $this->encryption->encrypt($userData['newPassword']);
                        $data = ["password" => $hash];
                        $this->User_Model->password_update($data, $userData['token']);

                        $success = "Password changed successfully";
                        $response = ['changePassword' => $success, 'status' => "SUCCESS"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    } else {
                        $errors = "Old Password does not match";
                        $response = ['message' => $errors, 'status' => "FAILURE"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                }
            } else {
                    $errors = "Bad Request";
                    $response = ['message' => $errors, 'status' => "FAILURE"];
            }
        }
    }

    // Indusrty listing
    public function industryList_get() {
        $data = $this->Common_Model->industry_lists();
        $response = ['industryList' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
    }

    // Channel Listing
    public function channelList_get() {
        $data = $this->Common_Model->channel_lists();
        $response = ['channelList' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
    }

    // Signup BPO List
    public function signupbpolist_get() {
        $bpo_lists = $this->Jobpost_Model->bpo_lists();
        if($bpo_lists) {
            $x=0;
            foreach ($bpo_lists as $bpo_list) {
                $bpo_list1[$x] = [
                      "id" => $bpo_list['id'],
                      "name" => $bpo_list['name']                    

                  ];
                $x++;
            }  
        }else{
            $bpo_list1=[];
        }
        
        $response = ['bpolist' => $bpo_list1, 'status' => "SUCCESS"];
        $this->set_response($response, REST_Controller::HTTP_CREATED);
    }

    // Language List
    public function languageList_get() {
        $data = $this->Common_Model->language_lists();
        $response = ['languageList' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
    }

    public function specialization_post() {
        $data = $this->input->post();
        $specialization_array = array();
 
        $specialization_lists = $this->Common_Model->category_list_byindustry($data['industry']);
        if($specialization_lists) {
            
            foreach ($specialization_lists as $specialization_list) {
                
                $specialization_array[] = [
                      "id" => $specialization_list['id'],
                      "name" => $specialization_list['category']
                  ];
            }
        }

        $response = ['specialization' => $specialization_array, 'status' => "SUCCESS"];
        $this->set_response($response, REST_Controller::HTTP_CREATED);
    }

    public function subspecialization_post() {
        $data = $this->input->post();
        $sub_specialization_array = array();
 
        $sub_specialization_lists = $this->Common_Model->subcategory_list_byspecialization($data['specialization']);
        if($sub_specialization_lists) {
            
            foreach ($sub_specialization_lists as $sub_specialization_list) {
                
                $sub_specialization_array[] = [
                      "id" => $sub_specialization_list['id'],
                      "name" => $sub_specialization_list['subcategory']
                  ];
            }
        }

        $response = ['sub_specialization' => $sub_specialization_array, 'status' => "SUCCESS"];
        $this->set_response($response, REST_Controller::HTTP_CREATED);
    }

    public function emailsendaftersignup_post() {

        $userData = $this->input->post();

        $data = ["token" => $userData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);

        if($userTokenCheck1) {
            
            $userid = $userTokenCheck1[0]['user_id'];
            
            $this->successsignupone($userid);

            $success = "Email send successfully";
            $response = ['emailsendaftersignup' => $success, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } else {

            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function jobintrestedin_post() {
        $data = $this->input->post();
        $intestedin_array = array();


        if(strlen($data['token']) > 0) {
            $data = ["token" => $data['token']];
            $userTokenCheck = $this->User_Model->token_match1($data);
            if($userTokenCheck) {

                $data = ["id" => $userTokenCheck[0]['user_id']];
                $userDataGet = $this->User_Model->token_match($data);
                if($userDataGet) {

                    if(strlen($userDataGet[0]['industry'])>0) {

                        $data_industry = $userDataGet[0]['industry'];
                    } else {
                        $data_industry = "BPO";
                    }
                } else {
                    $data_industry = "BPO";
                }

            } else {
                $data_industry = "BPO";
            }

        } else {
            $data_industry = $data['industry'];
        }

        $intestedin_lists = $this->Common_Model->jobintrested_list_byindustry($data_industry);
        if($intestedin_lists) {
            
            foreach ($intestedin_lists as $intestedin_list) {
                
                $intestedin_array[] = [
                      "id" => $intestedin_list['id'],
                      "name" => $intestedin_list['name']
                  ];
            }
        }

        $response = ['jobintrestedin' => $intestedin_array, 'status' => "SUCCESS"];
        $this->set_response($response, REST_Controller::HTTP_CREATED);
    }

    public function getownstate_post() {
        $data = $this->input->post();

        if(empty($data['state'])) {
            $result = $this->User_Model->getownstate();
            $response = ['getownstate' => $result, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        } else {
            $result = $this->User_Model->getowncity($data['state']);
            $response = ['getowncity' => $result, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }   
    }

    // Work Exp save
    public function workExperience_post() {
        $workData = $this->input->post();
        $this->form_validation->set_rules('title', 'title', 'trim|required');
        $this->form_validation->set_rules('company', 'company', 'trim|required');
        $this->form_validation->set_rules('desc', 'desc', 'trim');
        $this->form_validation->set_rules('from', 'from', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {
            $data = ["token" => $workData['token']];
            $userTokenCheck1 = $this->User_Model->token_match1($data);
            if($userTokenCheck1){
                $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
                $userTokenCheck = $this->User_Model->token_match($tokendata);
                if($userTokenCheck) {
                    $jobFrom = $workData['from'];
                    $jobTo = $workData['to'];

                    $data = ["user_id"=>$userTokenCheck[0]['id'], "title"=>$workData['title'], "company"=>$workData['company'], "jobDesc"=>$workData['desc'], "workFrom"=>$jobFrom, "workTo"=>$jobTo,"currently_working"=>$workData['currently_working'],"level"=>$workData['level'], "category"=>$workData['category'], "subcategory"=>$workData['subcategory']];

                    if(empty($workData['id'])) {

                        $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                        if($getCompleteness) {
                            if($getCompleteness[0]['experience'] == 0) {
                                $profileComplete = 10;
                                $comProfile = ["experience" => $profileComplete];
                                $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                            }
                        }
                        $workInserted = $this->User_Model->work_insert($data);
                    } else {
                        $workInserted = $this->User_Model->work_update($data, $workData['id']);
                    }

                    if($workInserted) {
                        $this->successsignupone($userTokenCheck[0]['id']);
                        $userId = $userTokenCheck[0]['id'];
                        $workData = $this->fetchWorkSingleData($userId);
                        $response = ['workExperience' => $workData, 'status' => "SUCCESS"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    } else{
                        $errors = "Data not inserted";
                        $response = ['message' => $errors, 'status' => "FAILURE"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                } 
            }else{
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    // Education save
    public function education_post() {
        $eduData = $this->input->post();
        $this->form_validation->set_rules('attainment', 'title', 'trim|required');
        $this->form_validation->set_rules('degree', 'company', 'trim|required');
        $this->form_validation->set_rules('university', 'desc', 'trim|required');
        $this->form_validation->set_rules('from', 'from', 'trim|required');
        $this->form_validation->set_rules('to', 'to', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {
            $data = ["token" => $eduData['token']];
            $userTokenCheck1 = $this->User_Model->token_match1($data);
            if($userTokenCheck1){
                $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
                $userTokenCheck = $this->User_Model->token_match($tokendata);
                if($userTokenCheck) {
                    $jobFrom = $eduData['from'];
                    $jobTo = $eduData['to'];

                    $data = ["user_id"=>$userTokenCheck[0]['id'], "attainment"=>$eduData['attainment'], "degree"=>$eduData['degree'], "university"=>$eduData['university'], "degreeFrom"=>$jobFrom, "degreeTo"=>$jobTo];

                    if(empty($eduData['id'])) {
                        $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                        if($getCompleteness) {
                            if($getCompleteness[0]['education'] == 0) {
                                $profileComplete = 10;
                                $comProfile = ["education" => $profileComplete];
                                $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                            }
                        }
                        $eduInserted = $this->User_Model->edu_insert($data);
                    } else {
                        $eduInserted = $this->User_Model->edu_update($data, $eduData['id']);
                    }

                    if($eduInserted) {

                        $this->successsignupone($userTokenCheck[0]['id']);

                        $userId = $userTokenCheck[0]['id'];
                        $eduData = $this->fetchEduSingleData($userId);
                        $response = ['education' => $eduData, 'status' => "SUCCESS"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    } else{
                        $errors = "Data not inserted";
                        $response = ['message' => $errors, 'status' => "FAILURE"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                }
            }else{
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    // Profile Update
    public function profileUpdate_post() {
        $profileComplete = 0;
        $profileData = $this->input->post();
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('location', 'Location', 'trim|required');
        $this->form_validation->set_rules('education', 'Education', 'trim|required');
        if($profileData['profilePic'] != ""){
            $this->form_validation->set_rules('profilePic', 'profilePic', 'required');
            $this->form_validation->set_rules('type', 'type', 'required');
        }

        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {
            $data = ["token" => $profileData['token']];
            $userTokenCheck1 = $this->User_Model->token_match1($data);
            if($userTokenCheck1){
                $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
                $userTokenCheck = $this->User_Model->token_match($tokendata);
                if($userTokenCheck) {

                    if(isset($profileData['work_mode'])) {
                        $profile_work_mode = $profileData['work_mode'];
                    } else {
                        $profile_work_mode = "";
                    }
                    if(isset($profileData['vaccination'])) {
                        $profile_vaccination = $profileData['vaccination'];
                    } else {
                        $profile_vaccination = "";
                    }
                    if(isset($profileData['relocate'])) {
                        $profile_relocate = $profileData['relocate'];
                    } else {
                        $profile_relocate = "";
                    }

                    $data1 = ["name"=> $profileData['name'], 
                            "country_code"=>$profileData['country_code'], 
                            "phone"=> $profileData['phone'],
                            "dob"=>$profileData['dob'], 
                            "location"=> $profileData['location'], 
                            "state"=> $profileData['state'],
                            "city"=> $profileData['city'],
                            "designation"=> $profileData['designation'], 
                            "current_salary"=> $profileData['current_salary'], 
                            "exp_month"=> $profileData['exp_month'], 
                            "exp_year"=> $profileData['exp_year'], 
                            "education"=> $profileData['education'], 
                            "superpower"=> $profileData['specialization'], 
                            "nationality"=> $profileData['nationality'], 
                            "jobsearch_status"=>$profileData['jobsearch_status'],
                            "bpoyear"=>$profileData['bpoyear'],
                            "bpomonth"=>$profileData['bpomonth'],
                            "timeforcall"=>$profileData['timeforcall'],
                            "industry"=>$profileData['industry'],
                            "specialization"=>$profileData['specialization'],
                            "sub_specialization"=>$profileData['sub_specialization'],
                            "haveinternet"=>$profileData['haveinternet'],
                            "internetspeed"=>$profileData['internetspeed'],
                            "topbpo"=>$profileData['lastbpo'], 
                            "jobLevel"=>$profileData['jobLevel'],
                            "jobsInterested"=>$profileData['jobsInterested'],
                            "jobsbenefits"=>$profileData['benefits'],
                            "work_mode"=>$profile_work_mode,
                            "vaccination"=>$profile_vaccination,
                            "relocate"=>$profile_relocate,
                        ];

                    $userUpdated = $this->User_Model->user_update($data1, $userTokenCheck[0]['id']);

                    if($profileData['profilePic'] != "" && !empty($profileData['profilePic'])) {
                        $userPic = $this->Common_Model->image_upload($profileData['profilePic'], $profileData['type']);

                        if($userPic) {
                            $userPicUpdate = $this->User_Model->user_pic_update($userPic, $userTokenCheck[0]['id']);
                        }
                    }

                    $userData = $this->User_Model->user_single($userTokenCheck[0]['id']);
                    if($userData[0]['profilePic'] != "") {
                        $userProfilePic = $userData[0]['profilePic'];
                    } else{
                        $userProfilePic = "";
                    }
                    $assessData = $this->User_Model->user_assessment($userTokenCheck[0]['id']);
                    $expertData = $this->User_Model->user_expert($userTokenCheck[0]['id']);
                    $workData = $this->fetchWorkSingleData($userTokenCheck[0]['id']);
                    $eduData = $this->fetchEduSingleData($userTokenCheck[0]['id']);
                    $langData = $this->fetchLangSingleData($userTokenCheck[0]['id']);
                    $getCompletenes = $this->fetchUserCompleteData($userTokenCheck[0]['id']);    
                    $profile['completeness'] = $getCompletenes['comp'];

                    $profile['user'] = ["name"=>$userData[0]['name'], "dob"=>$userData[0]['dob'], "email"=>$userData[0]['email'],"country_code"=>$userData[0]['country_code'], "phone"=>$userData[0]['phone'], "location"=>$userData[0]['location'],"designation"=>$userData[0]['designation'],"current_salary"=>$userData[0]['current_salary'],"exp_month"=>$userData[0]['exp_month'],"exp_year"=>$userData[0]['exp_year'],"education"=>$userData[0]['education'],"superpower"=>$userData[0]['superpower'],"nationality"=>$userData[0]['nationality'], "profilePic" => $userProfilePic, "jobsearch_status"=>$userData[0]['jobsearch_status'],"bpoyear"=>$userData[0]['bpoyear'],"bpomonth"=>$userData[0]['bpomonth'],"timeforcall"=>$userData[0]['timeforcall'],"education"=>$userData[0]['education'],"industry"=>$userData[0]['industry'],"specialization"=>$userData[0]['specialization'],"sub_specialization"=>$userData[0]['sub_specialization'],"haveinternet"=>$userData[0]['haveinternet'],"internetspeed"=>$userData[0]['internetspeed'],"lastbpo"=>$userData[0]['topbpo'],"benefits"=>$userData[0]['jobsbenefits'],"work_mode"=>$userData[0]['work_mode'],"vaccination"=>$userData[0]['vaccination'],"relocate"=>$userData[0]['relocate']];

                    if($assessData) {
                        $profile['assessment'] = ["verbal"=>$assessData[0]['verbal'], "written"=>$assessData[0]['written'], "listening"=>$assessData[0]['listening'], "problem"=>$assessData[0]['problem']];
                    } else{
                        $profile['assessment'] = [];
                    }
                    if($workData) {
                        $profile['experience'] = $workData;    
                    } else {
                        $profile['experience'] = [];
                    }
                    if($eduData) {
                        $profile['education'] = $eduData;    
                    } else{
                        $profile['education'] = [];
                    }
                    if($langData){
                        $profile['language'] = $langData;    
                    } else{
                        $profile['language'] = [];
                    }
                    if($expertData) {
                        $expert = $expertData[0]['expert'];
                        $extractExport = explode(',', $expert); 
                        $profile['expert'] = $extractExport;
                    } else{
                        $profile['expert'] = [];
                    }
                    
                    $clientData = $this->fetchClientSingleData($userTokenCheck[0]['id']);
                    if($clientData) {
                        $profile['clients'] = $clientData;
                    } else{
                        $profile['clients'][] = [
                                    "id" =>"",
                                    "clients"=>""
                                ];
                    }
                    $getResume = $this->User_Model->resume_single($userTokenCheck[0]['id']);

                    if($getResume) {
                        $profile["resume"] = $getResume[0]['resume'];
                    } else{
                        $profile["resume"] = "";
                    }
                    $profile['rating'] = $this->calculateRating($userTokenCheck[0]['id']);
                    
                    $this->successsignupone($userTokenCheck[0]['id']);

                    $response = ['profileupdate' => $profile, 'status' => "SUCCESS"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
            } else {
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    public function profileUpdateMore_post() {

        $profileData = $this->input->post();
        $this->form_validation->set_rules('current_salary', 'Current Salary', 'trim|required');
        $this->form_validation->set_rules('newsletters', 'Newsletter', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {
            $data = ["token" => $profileData['token']];
            $userTokenCheck1 = $this->User_Model->token_match1($data);
        
            if($userTokenCheck1) {
                $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
                $userTokenCheck = $this->User_Model->token_match($tokendata);

                if($userTokenCheck) {

                    if(isset($profileData['work_mode'])) {
                        $profile_work_mode = $profileData['work_mode'];
                    } else {
                        $profile_work_mode = "";
                    }
                    if(isset($profileData['vaccination'])) {
                        $profile_vaccination = $profileData['vaccination'];
                    } else {
                        $profile_vaccination = "";
                    }
                    if(isset($profileData['relocate'])) {
                        $profile_relocate = $profileData['relocate'];
                    } else {
                        $profile_relocate = "";
                    }

                    $data1 = [
                          "current_salary"=> $profileData['current_salary'], 
                          "newsletters"=> $profileData['newsletters'],
                          "jobsbenefits"=>$profileData['benefits'],
                          "work_mode"=>$profile_work_mode,
                          "vaccination"=>$profile_vaccination,
                          "relocate"=>$profile_relocate,
                    ];

                    $userUpdated = $this->User_Model->user_update($data1, $userTokenCheck[0]['id']);

                    $comProfile = ["other_more"=>10,"dynamic_resume"=>10];
                    $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);

                    $this->successsignupone($userTokenCheck[0]['id']);

                    $response = ['profileupdateMore' => $userUpdated, 'status' => "SUCCESS"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                
                } else {
                    $errors = "Bad Request";
                    $response = ['message' => $errors, 'status' => "FAILURE"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
            } else {
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }
    
    // Get profile data
    public function profileView_post() {
        $profileData = $this->input->post();
        $data = ["token" => $profileData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);  
            if($userTokenCheck) {

                $userData = $this->User_Model->user_single($userTokenCheck[0]['id']);
                if($userData[0]['profilePic'] != "") {
                    $userProfilePic = $userData[0]['profilePic'];
                } else{
                    $userProfilePic = "";
                }
                $assessData = $this->User_Model->user_assessment($userTokenCheck[0]['id']);
                $expertData = $this->User_Model->user_expert($userTokenCheck[0]['id']);
                

                $dynamicResumeData = $this->User_Model->user_dynamic_resume_single($userTokenCheck[0]['id']);
                if($dynamicResumeData) {
                    $dynamicresume = base_url().$dynamicResumeData[0]['resume'];
                } else {

                    $this->successsignupone($userTokenCheck[0]['id']);

                    $dynamicResumeData = $this->User_Model->user_dynamic_resume_single($userTokenCheck[0]['id']);
                    if($dynamicResumeData) {
                        $dynamicresume = base_url().$dynamicResumeData[0]['resume'];
                    } else {
                        $dynamicresume = "";
                    }
                }
                
                $workData = $this->fetchWorkSingleData($userTokenCheck[0]['id']);
                $eduData = $this->fetchEduSingleData($userTokenCheck[0]['id']);
                $langData = $this->fetchLangSingleData($userTokenCheck[0]['id']);
                
                $getCompletenes = $this->fetchUserCompleteData($userTokenCheck[0]['id']);    
                $profile['completeness'] = $getCompletenes['comp'];

                if($userData[0]['designation']){
                    $userData[0]['designation'] = $userData[0]['designation'];
                }  else{
                $userData[0]['designation'] = ""; 
                }
                if($userData[0]['location']){
                    $userData[0]['location'] = $userData[0]['location'];
                }  else{
                $userData[0]['location'] = ""; 
                }if($userData[0]['education']){
                    $userData[0]['education'] = $userData[0]['education'];
                }  else{
                $userData[0]['education'] = ""; 
                }if($userData[0]['superpower']){
                    $userData[0]['superpower'] = $userData[0]['superpower'];
                }  else{
                $userData[0]['superpower'] = ""; 
                }
                if($userData[0]['current_salary']){
                    $userData[0]['current_salary'] = $userData[0]['current_salary'];
                }  else{
                $userData[0]['current_salary'] = ""; 
                }
                if($userData[0]['exp_month']>="0"){
                    $userData[0]['exp_month'] = $userData[0]['exp_month'];
                }  else{
                $userData[0]['exp_month'] = ""; 
                }
                if($userData[0]['exp_year']>="0"){
                    $userData[0]['exp_year'] = $userData[0]['exp_year'];
                }  else{
                $userData[0]['exp_year'] = ""; 
                }
                if($userData[0]['nationality']){
                    $userData[0]['nationality'] = $userData[0]['nationality'];
                }  else{
                $userData[0]['nationality'] = ""; 
                }
                if($userData[0]['country_code']){
                    $userData[0]['country_code'] = $userData[0]['country_code'];
                }  else{
                $userData[0]['country_code'] = ""; 
                }

                if($userData[0]['jobsearch_status']){
                    $userData[0]['jobsearch_status'] = $userData[0]['jobsearch_status'];
                }  else{
                $userData[0]['jobsearch_status'] = ""; 
                }
                if($userData[0]['bpoyear']){
                    $userData[0]['bpoyear'] = $userData[0]['bpoyear'];
                }  else{
                $userData[0]['bpoyear'] = ""; 
                }
                if($userData[0]['bpomonth']){
                    $userData[0]['bpomonth'] = $userData[0]['bpomonth'];
                }  else{
                $userData[0]['bpomonth'] = ""; 
                }
                if($userData[0]['timeforcall']){
                    $userData[0]['timeforcall'] = $userData[0]['timeforcall'];
                }  else{
                $userData[0]['timeforcall'] = ""; 
                }
                if($userData[0]['education']){
                    $userData[0]['education'] = $userData[0]['education'];
                }  else{
                $userData[0]['education'] = ""; 
                }
                if($userData[0]['industry']){
                    $userData[0]['industry'] = $userData[0]['industry'];
                }  else{
                $userData[0]['industry'] = ""; 
                }
                if($userData[0]['specialization']){
                    $userData[0]['specialization'] = $userData[0]['specialization'];
                }  else{
                $userData[0]['specialization'] = ""; 
                }
                if($userData[0]['sub_specialization']){
                    $userData[0]['sub_specialization'] = $userData[0]['sub_specialization'];
                }  else{
                $userData[0]['sub_specialization'] = ""; 
                }
                if($userData[0]['haveinternet']){
                    $userData[0]['haveinternet'] = $userData[0]['haveinternet'];
                }  else{
                $userData[0]['haveinternet'] = ""; 
                }
                if($userData[0]['internetspeed']){
                    $userData[0]['internetspeed'] = $userData[0]['internetspeed'];
                }  else{
                $userData[0]['internetspeed'] = ""; 
                }
                if($userData[0]['topbpo']){
                    $userData[0]['topbpo'] = $userData[0]['topbpo'];
                }  else{
                $userData[0]['topbpo'] = ""; 
                }

                if($userData[0]['jobLevel']){
                    $userData[0]['jobLevel'] = $userData[0]['jobLevel'];
                }  else{
                $userData[0]['jobLevel'] = ""; 
                }

                if($userData[0]['jobsInterested']){
                    $userData[0]['jobsInterested'] = explode(',', $userData[0]['jobsInterested']);
                }  else{
                $userData[0]['jobsInterested'] = ""; 
                }

                if($userData[0]['work_mode']){
                    $userData[0]['work_mode'] = $userData[0]['work_mode'];
                }  else{
                $userData[0]['work_mode'] = ""; 
                }

                if($userData[0]['vaccination']){
                    $userData[0]['vaccination'] = $userData[0]['vaccination'];
                }  else{
                $userData[0]['vaccination'] = ""; 
                }

                if($userData[0]['relocate']){
                    $userData[0]['relocate'] = $userData[0]['relocate'];
                }  else{
                $userData[0]['relocate'] = ""; 
                }

                $profile['user'] = ["name"=>$userData[0]['name'], "dob"=>$userData[0]['dob'], "email"=>$userData[0]['email'],"country_code"=>$userData[0]['country_code'], "phone"=>$userData[0]['phone'], "location"=>$userData[0]['location'],"designation"=>$userData[0]['designation'],"current_salary"=>$userData[0]['current_salary'],"exp_month"=>$userData[0]['exp_month'],"exp_year"=>$userData[0]['exp_year'],"location"=>$userData[0]['location'],"education"=>$userData[0]['education'],"superpower"=>$userData[0]['specialization'],"nationality"=>$userData[0]['nationality'], "jobsearch_status"=>$userData[0]['jobsearch_status'],"bpoyear"=>$userData[0]['bpoyear'],"bpomonth"=>$userData[0]['bpomonth'],"timeforcall"=>$userData[0]['timeforcall'],"education"=>$userData[0]['education'],"industry"=>$userData[0]['industry'],"specialization"=>$userData[0]['specialization'],"sub_specialization"=>$userData[0]['sub_specialization'],"haveinternet"=>$userData[0]['haveinternet'],"internetspeed"=>$userData[0]['internetspeed'],"lastbpo"=>$userData[0]['topbpo'], "profilePic" => $userProfilePic, "state"=>$userData[0]['state']??'',"city"=>$userData[0]['city']??'', "dynamicresume"=>$dynamicresume, "jobsInterested"=>$userData[0]['jobsInterested'], "jobLevel"=>$userData[0]['jobLevel'], "benefits"=>$userData[0]['jobsbenefits'], "work_mode"=>$userData[0]['work_mode'], "vaccination"=>$userData[0]['vaccination'], "relocate"=>$userData[0]['relocate']];
                
                if($assessData) {
                    $profile['assessment'] = ["verbal"=>$assessData[0]['verbal'], "written"=>$assessData[0]['written'], "listening"=>$assessData[0]['listening'], "problem"=>$assessData[0]['problem']];
                } else{
                    $profile['assessment'] = [
                                    "verbal"=> "",
                                    "written"=> "",
                                    "listening"=> "",
                                    "problem"=> ""
                                ];
                }
                if($workData) {
                    $profile['experience'] = $workData;
                } else{
                    $profile['experience'][] = [
                                "id" => "",
                                "title" => "",
                                "company" => "",
                                "desc" => "",
                                "from" => "",
                                "to" => "",
                                "industry" => "",
                                "channel" => ""
                            ];
                }
                if($eduData) {
                    $profile['education'] = $eduData;
                } else{
                    $profile['education'][] = [
                                        "id" => "",
                                        "attainment" => "",
                                        "degree" => "",
                                        "university" => "",
                                        "from" => "",
                                        "to" => ""
                                    ];
                }
                if($langData) {
                    $profile['language'] = $langData;
                } else{
                    $profile['language'][] = [
                            "id"=> "",
                            "lang_name"=> ""
                        ];
                }
                if($expertData) {
                    if(empty($expertData[0]['expert'])){
                        $profile['expert'] =[];
                    } else {
                        $expert = $expertData[0]['expert'];
                        $extractExport = explode(',', $expert); 
                        $profile['expert'] = $extractExport;
                    }
                    
                } else {
                    $profile['expert'] = [];
                }

                $clientData = $this->fetchClientSingleData($userTokenCheck[0]['id']);
                if($clientData) {
                    $profile['clients'] = $clientData;
                } else{
                    $profile['clients'][] = [
                                    "id" =>"",
                                    "clients"=>""
                                    ];
                }
                $getResume = $this->User_Model->resume_single($userTokenCheck[0]['id']);

                if($getResume) {
                    $profile["resume"] = $getResume[0]['resume'];
                } else{
                    $profile["resume"] = "";
                }

                $profile['rating'] = $this->calculateRating($userTokenCheck[0]['id']);
                $response = ['profileview' => $profile, 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        } else {
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    // Profile view more
    public function profileMoreView_post() {
        $profileData = $this->input->post();
        $data = ["token" => $profileData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);  
            if($userTokenCheck) {
                $skillsData = $this->fetchSkillsSingleData($userTokenCheck[0]['id']);

                if($skillsData) {
                    $profile['skills'] = $skillsData;
                } else{
                    $profile['skills'][] = [
                                "id" =>"",
                                "skills"=>""
                            ];
                }
                $moreData = $this->fetchMoreSingleData($userTokenCheck[0]['id']);
                if($moreData) {
                    $profile['more'] = $moreData;
                } else{
                    $profile['more'] = [
                                "id" =>"",
                                "strength"=>"",
                                "weakness"=>"",
                                "achievements"=>""
                            ];
                }
                $response = ['profileMoreView' => $profile, 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    // Profile more
    public function profileMore_post() {
        $profileData = $this->input->post();
        
        $data = ["token" => $profileData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck) {

                $data = ["user_id"=> $userTokenCheck[0]['id'], "strength"=>$profileData['strength'], "weakness"=> $profileData['weakness'], "achievements"=> $profileData['achievements']];

                $checkMore = $this->User_Model->user_more($userTokenCheck[0]['id']);
                if($checkMore) {
                    $checkMoreUpdate = $this->User_Model->user_more_update($data, $userTokenCheck[0]['id']);
                } else{
                    $checkMoreInsert = $this->User_Model->user_more_insert($data);
                }
                $moreData = $this->fetchMoreSingleData($userTokenCheck[0]['id']);
                if($moreData) {
                    $profile['more'] = $moreData;
                } else{
                    $profile['more'] = [
                                "id" =>"",
                                "strength"=>"",
                                "weakness"=>"",
                                "achievements"=>""
                            ];
                }

                $response = ['profileMore' => $profile, 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function check_profile_post() {
        
        $userData = $this->input->post();
        $this->form_validation->set_rules('token', 'token', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {

            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } else {

            $data = ["token" => $userData['token']];
            $userTokenCheck1 = $this->User_Model->token_match1($data);

            if($userTokenCheck1) {

                $getData = $this->fetchUserSingleData($userTokenCheck1[0]['user_id']);
                if($getData) {
                    if(empty($getData['bpoyear']) && empty($getData['bpomonth'])) {
                        $success = "step-1";
                        $response = ['profile' => $success, 'status' => "SUCCESS"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    
                    } elseif(empty($getData['current_salary'])) {
                        $success = "step-2";
                        $response = ['profile' => $success, 'status' => "SUCCESS"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    
                    } else {

                        $success = "completed";
                        $response = ['profile' => $success, 'status' => "SUCCESS"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }   

                } else {

                    $errors = "Bad Request";
                    $response = ['message' => $errors, 'status' => "FAILURE"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);    
                }

            } else {
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    public function stepzero_profile_post() {

        $userData = $this->input->post();
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
            $this->form_validation->set_rules('country_code', 'country_code', 'trim|required');
            $this->form_validation->set_rules('phone', 'phone', 'trim|required');
            $this->form_validation->set_rules('nationality', 'Nationality', 'trim');
            $this->form_validation->set_rules('exp_month', 'Exp Month', 'trim|required');
            $this->form_validation->set_rules('exp_year', 'Exp Year', 'trim|required');

            $this->form_validation->set_rules('location', 'Location', 'trim|required');
            $this->form_validation->set_rules('state', 'State', 'trim');
            $this->form_validation->set_rules('city', 'City', 'trim');
            $this->form_validation->set_rules('jobsInterested', 'Job intrested In', 'trim|required');

            $this->form_validation->set_rules('education', 'Education', 'trim|required');
            $this->form_validation->set_rules('specialization', 'Specialization', 'trim|required');
            $this->form_validation->set_rules('sub_specialization', 'Sub-Specialization', 'trim|required');

        if ($this->form_validation->run() == FALSE) {

            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } else {

            $data = ["token" => $userData['token']];
            $userTokenCheck1 = $this->User_Model->token_match1($data);

            if($userTokenCheck1) {

    
                $data = [
                    'name' => ucwords(strtolower($userData['name'])),
                    'email' => $userData['email'],
                    'country_code' => $userData['country_code'],
                    'phone' => $userData['phone'],
                    'device_type' => $userData['device_type'],
                    'device_token' => $userData['device_token'],

                    'location' => $userData['location'],
                    'state' => $userData['state'],
                    'city' => $userData['city'],
                    'nationality' => $userData['nationality'],
                    'exp_year' => $userData['exp_year'],
                    'exp_month' => $userData['exp_month'],
                    'jobsInterested' => $userData['jobsInterested'],
                    'education' => $userData['education'], 
                    'superpower' => $userData['specialization'], 
                    'specialization' => $userData['specialization'], 
                    'sub_specialization' => $userData['sub_specialization']
                ];

                $this->User_Model->user_update($data, $userTokenCheck1[0]['user_id']);
                
                $success = "user details saved successfully";
                $response = ['otherdetail' => $success, 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);

            } else {
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }

    }

    public function stepone_profile_post() {

        $userData = $this->input->post();
        $this->form_validation->set_rules('jobsearch', 'Job Search', 'trim|required');
        $this->form_validation->set_rules('bpoyear', 'Experience in year', 'trim|required');
        $this->form_validation->set_rules('bpomonth', 'Experience in month', 'trim|required');
        $this->form_validation->set_rules('timeforcall', 'Best time for call', 'trim');
        $this->form_validation->set_rules('industry', 'Industry', 'trim|required');
        $this->form_validation->set_rules('lastbpo', 'Last bpo', 'trim|required');
        $this->form_validation->set_rules('jobLevel', 'Jo Level', 'trim|required');

        if(!empty($userData['resume'])) {
            $this->form_validation->set_rules('resume', '', 'required');
            $this->form_validation->set_rules('type', '', 'required');
        }

        if ($this->form_validation->run() == FALSE) {

            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } else {

            $data = ["token" => $userData['token']];
            $userTokenCheck1 = $this->User_Model->token_match1($data);

            if($userTokenCheck1) {

                if(!empty($userData['resume'])) {
                    $userResume = $this->Common_Model->file_upload($resumeData['resume'], $resumeData['type']);
                } else {
                    $userResume = "";
                }
                if(!empty($userData['resume'])) {
                    $this->User_Model->user_resume_insert($userResume, $userTokenCheck1[0]['user_id']);
                }

                $data = ['topbpo' => $userData['lastbpo'],'jobsearch_status' => $userData['jobsearch'], 'bpoyear' => $userData['bpoyear'], 'bpomonth' => $userData['bpomonth'], 'timeforcall' => $userData['timeforcall'], 'industry' => $userData['industry'], 'jobLevel' => $userData['jobLevel']];

                $this->User_Model->user_update($data, $userTokenCheck1[0]['user_id']);
                
                $success = "user details saved successfully";
                $response = ['otherdetail' => $success, 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);

            } else {
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }

    }

    public function steptwo_profile_post() {

        $userData = $this->input->post();
        $this->form_validation->set_rules('haveinternet', 'Have fixed internet connection', 'trim');
        $this->form_validation->set_rules('internetspeed', 'Speed', 'trim');
        $this->form_validation->set_rules('current_salary', 'Current Salary', 'trim|required');
        $this->form_validation->set_rules('howhear', 'How Did you hear', 'trim');
        $this->form_validation->set_rules('expert', 'Expert', 'trim|required');

        if ($this->form_validation->run() == FALSE) {

            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } else {

            $data = ["token" => $userData['token']];
            $userTokenCheck1 = $this->User_Model->token_match1($data);

            if($userTokenCheck1) {

                $data = ['haveinternet' => $userData['haveinternet'], 'internetspeed' => $userData['internetspeed'], 'current_salary' => $userData['current_salary'], 'howhear' => $userData['howhear'],'referral_used' => $userData['referral_code']];

                $this->User_Model->user_update($data, $userTokenCheck1[0]['user_id']);
                
                $data3 = ["user_id"=>$userTokenCheck1[0]['user_id'], "expert" => $userData['expert']];
                $checkExpert = $this->User_Model->user_expert($userTokenCheck1[0]['user_id']);
                if($checkExpert) {
                    if(!empty($userData['expert'])) {
                        $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck1[0]['user_id']);
                        if($getCompleteness) {
                            if($getCompleteness[0]['expert'] == 0) {
                                $profileComplete = 10;
                                $comProfile = ["expert" => $profileComplete];
                                $this->User_Model->update_profileComplete($comProfile, $userTokenCheck1[0]['user_id']);
                            }
                        }
                    }
                    $checkExpertUpdate = $this->User_Model->user_expert_update($data3, $userTokenCheck1[0]['user_id']);
                } else{

                    if(!empty($userData['expert'])) {
                        $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck1[0]['user_id']);
                        if($getCompleteness) {
                            if($getCompleteness[0]['expert'] == 0) {
                                $profileComplete = 10;
                                $comProfile = ["expert" => $profileComplete];
                                $this->User_Model->update_profileComplete($comProfile, $userTokenCheck1[0]['user_id']);
                            }
                        }
                    }
                    $checkExpertInsert = $this->User_Model->user_expert_insert($data3);
                }
                $success = "user details saved successfully";
                $response = ['otherdetail' => $success, 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);

            } else {
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    // Upload Resume
    public function userResume_post() {
        $resumeData = $this->input->post();
        $this->form_validation->set_rules('resume', '', 'required');
        $this->form_validation->set_rules('type', '', 'required');
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        } else {
            $data = ["token" => $resumeData['token']];
            $userTokenCheck1 = $this->User_Model->token_match1($data);

            if($userTokenCheck1){
                $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
                $userTokenCheck = $this->User_Model->token_match($tokendata);
                if($userTokenCheck) {
                    $checkResume = $this->User_Model->resume_single($userTokenCheck[0]['id']);

                    if($checkResume) {
                        $userResume = $this->Common_Model->file_upload($resumeData['resume'], $resumeData['type']);
                        $userResumeUpdate = $this->User_Model->user_resume_update($userResume, $userTokenCheck[0]['id']);
                    } else {
                        $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                        if($getCompleteness) {
                            if($getCompleteness[0]['resume'] == 0) {
                                $profileComplete = 10;
                                $comProfile = ["resume" => $profileComplete];
                                $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                            }
                        }
                        $userResume = $this->Common_Model->file_upload($resumeData['resume'], $resumeData['type']);
                        $userResumeInsert = $this->User_Model->user_resume_insert($userResume, $userTokenCheck[0]['id']);
                    }
                    $getResume = $this->User_Model->resume_single($userTokenCheck[0]['id']);
                    $data = ["resume" => $getResume[0]['resume']];
                    $response = ['userresume' => $data, 'status' => "SUCCESS"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
            } else {
                    $errors = "Bad Request";
                    $response = ['message' => $errors, 'status' => "FAILURE"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    // SKills Save
    public function addSkills_post() {
        $skillsData = $this->input->post();
        
        $data = ["token" => $skillsData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {            
            
            $data = ["user_id"=>$userTokenCheck[0]['id'], "skills"=>$skillsData['skills']];
            $skillsCheck = $this->User_Model->skills_beforeinsertcheck($data);

            if($skillsCheck) {
                $errors = "Skills already added";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            } else {

                if(empty($skillsData['id'])) {
                    $skillsCount = $this->User_Model->skills_beforeInsertCount($userTokenCheck[0]['id']);
                  //print_r($skillsCount);die;
                    //echo count($skillsCount);die;
                    if(count($skillsCount)<5) {         
                      $data = ["user_id"=>$userTokenCheck[0]['id'], "skills"=>$skillsData['skills']];
                      $skillsInserted = $this->User_Model->topSkills_insert($data);

                      if($skillsInserted) {
                          $userId = $userTokenCheck[0]['id'];
                          $skillsData = $this->fetchSkillsSingleData($userId);
                          //var_dump($getClientData);die;
                          $response = ['addSkills' => $skillsData, 'status' => "SUCCESS"];
                          $this->set_response($response, REST_Controller::HTTP_CREATED);
                      } else{
                          $errors = "Data not inserted";
                          $response = ['message' => $errors, 'status' => "FAILURE"];
                          $this->set_response($response, REST_Controller::HTTP_CREATED);
                      }
                    } else{
                        $errors = "Skills not accepted more than 5";
                        $response = ['message' => $errors, 'status' => "FAILURE"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                } else {
                    $data = ["skills"=>$skillsData['skills']];
                    $skillsInserted = $this->User_Model->topSkills_update($data, $skillsData['id']);

                    if($skillsInserted) {
                        $userId = $userTokenCheck[0]['id'];
                        $skillsData = $this->fetchSkillsSingleData($userId);
                        //var_dump($getClientData);die;
                        $response = ['addSkills' => $skillsData, 'status' => "SUCCESS"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    } else{
                        $errors = "Data not inserted";
                        $response = ['message' => $errors, 'status' => "FAILURE"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                }
            }
        }} else{
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    // Language add
    public function languageAdd_post() {
        $langData = $this->input->post();
        $this->form_validation->set_rules('lang_id', 'Language Id', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {
            $data = ["token" => $langData['token']];
            $userTokenCheck1 = $this->User_Model->token_match1($data);
        
            if($userTokenCheck1){
                $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
                $userTokenCheck = $this->User_Model->token_match($tokendata);
                if($userTokenCheck) {
                    $langids = $langData['lang_id'];
                    $langids = explode(',', $langids);
                    
                    if(empty($langData['id'])) {
                        $deleteData = $this->User_Model->delete_user1($userTokenCheck[0]['id'], 'user_language');
                        foreach($langids as $langid) {  
                            $data = ["user_id"=>$userTokenCheck[0]['id'], "lang_id"=>$langid];
                            $langCount = $this->User_Model->lang_beforeInsertCount($userTokenCheck[0]['id']);
                            if(count($langCount) < 5) {
                                
                                $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                                
                                if($getCompleteness) {
                                    if($getCompleteness[0]['language'] == 0) {
                                        $profileComplete = 10;
                                        $comProfile = ["language" => $profileComplete];
                                        $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                                    }
                                }
                                $langInserted = $this->User_Model->lang_insert($data);

                                if($langInserted) {

                                    $userId = $userTokenCheck[0]['id'];
                                    $langData = $this->fetchLangSingleData($userId);
                                    $response = ['languageAdd' => $langData, 'langCount'=>count($langData), 'status' => "SUCCESS"];
                                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                                } else {
                                    $errors = "Data not inserted";
                                    $response = ['message' => $errors, 'status' => "FAILURE"];
                                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                                }
                            } else {
                                $errors = "Language not accepted more than 5";
                                $response = ['message' => $errors, 'status' => "FAILURE"];
                                $this->set_response($response, REST_Controller::HTTP_CREATED);
                            }
                        }

                        $this->successsignupone($userTokenCheck[0]['id']);

                    } else {
                        $data = ["user_id"=>$userTokenCheck[0]['id'], "lang_id"=>$langData['lang_id']];
                        $langInserted = $this->User_Model->lang_update($data, $langData['id']);
                        //echo $this->db->last_query();
                        if($langInserted) {

                            $this->successsignupone($userTokenCheck[0]['id']);

                            $userId = $userTokenCheck[0]['id'];
                            $langData = $this->fetchLangSingleData($userId);
                            $response = ['languageAdd' => $langData, 'status' => "SUCCESS"];
                            $this->set_response($response, REST_Controller::HTTP_CREATED);
                        } else{
                            $errors = "Data not inserted";
                            $response = ['message' => $errors, 'status' => "FAILURE"];
                            $this->set_response($response, REST_Controller::HTTP_CREATED);
                        }
                    }
                
                }
            } else{
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    // Forgot PAssword
    public function forgotPassword_post() {
        $userData = $this->input->post();
        $data = ["email" => $userData['email'], "type"=>"normal"];
        $userTokenCheck = $this->User_Model->email_match($data);
        if($userTokenCheck) {
            $email = $userTokenCheck[0]['email'];
            $name = $userTokenCheck[0]['name'];
            $token = $this->getToken();
            $data['full_name'] = ucfirst($name);
            $data['token'] = $token;
            $data['user_id'] = $userTokenCheck[0]['id'];
            $msg = $this->load->view('passwordemail',$data,TRUE);
            $config=array(
                  'protocol' => 'smtp',
                  'smtp_host' => 'smtpout.asia.secureserver.net',
                  'smtp_user' => 'noreply@jobyoda.com',
                  'smtp_pass' => 'Jobyoda@2k21',
                  'smtp_port' => 465,
                  'smtp_crypto' => 'ssl',
                  'charset'=>'utf-8',
                  'mailtype' => 'html',
                  'crlf' => "\r\n",
                  'newline' => "\r\n"
            );

            $this->email->initialize($config);
            $this->email->from("help@jobyoda.com", "JobYoDA");
            $this->email->to($email);
            $this->email->subject('JobYoDA - Forgot Password Link');
            $this->email->message($msg);
            
            if($this->email->send()) {
               $forgotPassCheck = $this->User_Model->forgotPass_check($userTokenCheck[0]['id']);     
               //echo $this->db->last_query();die;
               if($forgotPassCheck) {
                  $vCode = ["verifyCode"=>$token];
                  $this->User_Model->forgotPass_update($vCode, $userTokenCheck[0]['id']);
               } else {
                  $vCode = ["user_id"=>$userTokenCheck[0]['id'], "verifyCode"=>$token];
                  $this->User_Model->forgotPass_insert($vCode);
               }

                $success = "Check your Email for change password";
                $response = ['message' => $success, 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            } else {
                $errors = "Email is not send";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        } else {

            $data = ["email" => $userData['email']];
            $userTokenCheck = $this->User_Model->email_match($data);
            if($userTokenCheck) {

                if($userTokenCheck[0]['type'] == "gmail" || $userTokenCheck[0]['type'] == "facebook" || $userTokenCheck[0]['type'] == "apple") {

                    $errors = "This acount is linked with your social account, you can not reset this password.";
                    $response = ['message' => $errors, 'status' => "FAILURE"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);    
                }
                
            
            } else {
                $errors = "Email is not yet registered";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    // Logout
    public function logout_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        $userTokenCheck = $this->User_Model->token_match1($data);
        if($userTokenCheck) {
            $userLogout = $this->User_Model->user_logout1($userTokenCheck[0]['user_id'],$userData['token']);
            if($userLogout) {
                $response = ['message' => "Logout successfully", 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        } else{
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    // Delete user data
    public function deleteuserData_post(){
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck){
                if($userData['type'] == "clients") {
                    $tableName = "user_topclients";
                } else if($userData['type'] == "language") {
                    $tableName = "user_language";
                } else if($userData['type'] == "work_experience") {
                    $tableName = "user_work_experience";
                } else if($userData['type'] == "education") {
                    $tableName = "user_education";
                } else if($userData['type'] == "skills") {
                    $tableName = "user_skills";
                }
                $deleteData = $this->User_Model->delete_user($userData['id'], $tableName);
                $workData = $this->fetchWorkSingleData($userTokenCheck[0]['id']);
                $eduData = $this->fetchEduSingleData($userTokenCheck[0]['id']);
                $langData = $this->fetchLangSingleData($userTokenCheck[0]['id']);
                $clientData = $this->fetchClientSingleData($userTokenCheck[0]['id']); 
                if(empty($clientData)){
                    $comProfile = ["topClient" => 0];
                }
                if(empty($workData)){
                    $comProfile = ["experience" => 0];
                } 
                if(empty($eduData)){
                    $comProfile = ["education" => 0];
                } 
                if(empty($langData)){
                    $comProfile = ["language" => 0];
                }   
                if(!empty($comProfile)){
                    $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                }
                
                if($deleteData=="true"){

                    $this->successsignupone($userTokenCheck[0]['id']);

                    $response = ['message' => "Successfully deleted", 'status' => "SUCCESS"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }else{
                    $response = ['message' => "Unable to Delete", 'status' => 'SUCCESS'];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
            } 
        }else{
            $errors ="Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    // Job search status change
    public function jobsearchstatus_post() {
        $jobData = $this->input->post();
        $this->form_validation->set_rules('jobsearchstatus', 'jobsearchstatus', 'required');
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        } else {
            $data = ["token" => $jobData['token']];
            $userTokenCheck1 = $this->User_Model->token_match1($data);
            if($userTokenCheck1){
                $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
                $userTokenCheck = $this->User_Model->token_match($tokendata);
                if($userTokenCheck) {
                    $statusdata = $jobData['jobsearchstatus'];
                    $statusUpdate = $this->User_Model->jobsearchstatus_update($statusdata, $userTokenCheck[0]['id']);
                
                    $response = ['jobsearchstatus' => "Status changed Successfully", 'status' => "SUCCESS"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
            }else {
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    // Get job search status
    public function getjobsearchstatus_post() {
        $job = $this->input->post();
        $data = ["token" => $job['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1) {
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck) {
                $checkStatus = $this->User_Model->user_single($userTokenCheck[0]['id']);
                $jobstatus = ['jobsearchstatus'=>$checkStatus[0]['jobsearch_status']];
                $response = ['jobsearch_status' => $jobstatus, 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED); 
            } 
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    // notification status change
    public function notificationstatus_post() {
        $jobData = $this->input->post();
        $this->form_validation->set_rules('notificationstatus', 'notification status', 'required');
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        } else {
            $data = ["token" => $jobData['token']];
            $userTokenCheck1 = $this->User_Model->token_match1($data);
            if($userTokenCheck1){
                $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
                $userTokenCheck = $this->User_Model->token_match($tokendata);
                if($userTokenCheck) {
                    $notificationstatusdata = $jobData['notificationstatus'];
                    $statusUpdate = $this->User_Model->notificationstatus_update($notificationstatusdata, $userTokenCheck[0]['id']);
                
                    $response = ['notificationstatus' => "Status changed Successfully", 'status' => "SUCCESS"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
            } else {
                    $errors = "Bad Request";
                    $response = ['message' => $errors, 'status' => "FAILURE"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    // get email
    public function getuseremail_post() {
        $job = $this->input->post();
        $data = ["token" => $job['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1) {
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck) {
                $checkStatus = $this->User_Model->user_single($userTokenCheck[0]['id']);
                $useremail = $checkStatus[0]['email'];
                $response = ['user_email' => $useremail, 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
                
            } 
        }else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    // user contact
    public function usercontact_post() {
        $userData = $this->input->post();
        $this->form_validation->set_rules('token', 'token', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|required');
        $this->form_validation->set_rules('message', 'description', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {
            $data = ["token" => $userData['token']];
            $userTokenCheck1 = $this->User_Model->token_match1($data);
            if($userTokenCheck1){
                $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
                $userTokenCheck = $this->User_Model->token_match($tokendata);
                if($userTokenCheck) {
                        $data1 = ["user_id" => $userTokenCheck[0]['id'],"email"=> $userData['email'], "message"=>$userData['message'] ];
                        $this->load->library('email');
                        $data['name'] = ucfirst($userTokenCheck[0]['name']);
                        $data['message'] = $userData['message'];
                        $caseid = rand(10000,99999);
                        $data['caseId'] = 'JobYoDA-Jobseeker'.$caseid;
                        $msg = $this->load->view('contactappemail',$data,TRUE);
                        $config=array(
                        'charset'=>'utf-8',
                        'wordwrap'=> TRUE,
                        'mailtype' => 'html'
                        );
                        $this->email->initialize($config);
                        $this->email->from($userTokenCheck[0]['email'], "JobYoDA");
                        $this->email->to('help@jobyoda.com');
                        $this->email->subject('Ticket ID:'.$caseid);
                        $this->email->message($msg);
                        $this->email->send();
                    $contact_inserted =  $this->User_Model->insert_contact($data1);
                    if($contact_inserted){
                        $response = ['message' => "Submitted Successfully", 'status' => "SUCCESS"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                        
                    
                }
            } else {
                    $errors = "Bad Request";
                    $response = ['message' => $errors, 'status' => "FAILURE"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    // get Quotation
    public function quotation_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck) {
                date_default_timezone_set("Asia/Manila");
                if(isset($userData['last_used']) && !empty($userData['last_used']) && !empty($userData['app_version']) && isset($userData['app_version'])){
                    $userData['last_used'] = date('Y-m-d H:i:s',strtotime($userData['last_used']));
                    $datadevicetoken = ["app_version"=>$userData['app_version'], "last_used"=>$userData['last_used'], "platform"=>$userData['platform']];
                    $this->User_Model->update_token($datadevicetoken, $userTokenCheck[0]['id']);
                    //echo $this->db->last_query();die;
                }
                
                $fetchnotifications = $this->Candidate_Model->fetchnotifications($userTokenCheck[0]['id']);
                $data1 = $this->User_Model->quote_single();
                $response = ['quote' => $data1,'notification_count'=>count($fetchnotifications), 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        } else {
              $errors = "Bad Request";
              $response = ['message' => $errors, 'status' => "FAILURE"];
              $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    // Sign in bonus
    public function signinbonus_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck) {
                $bonusamount =  $this->User_Model->fetch_bonusamount($userTokenCheck[0]['id']);
                $data['bonus_amount'] = $bonusamount[0]['sign_in_bonus'];

                $bonushistorys =  $this->User_Model->fetch_bonushistory($userTokenCheck[0]['id']);
                $x=0;
                foreach($bonushistorys as $bonushistory) {

                $data['bonus_history'][$x] = ["id"=>$bonushistory['id'], "bonus"=>$bonushistory['bonus'], "notification"=>$bonushistory['notification'], "added_at"=>date("d-M-Y h:i:s",strtotime($bonushistory['added_at']))];
                $x++;
                }

                if($bonushistorys){
                    $response = ['signinbonus' => $data, 'status' => "SUCCESS"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                } else{
                    $data['bonus_amount'] = "0";
                    $data['bonus_history'] = []; 
                    $response = ['signinbonus' => $data, 'status' => "SUCCESS"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }           
            }
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    // Calculate Rating
    public function calculateRating_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck) {
                $this->load->helper('date');
                
                $attainments = $this->fetchEduSingleData($userTokenCheck[0]['id']);
                if($attainments) {
                $rate1 = array();
                foreach($attainments as $attainment) {
                    if($attainment['attainment'] == "Post Graduate") {
                        $rate1[] = 5;
                    } else if($attainment['attainment'] == "College Graduate") {
                        $rate1[] = 5;
                    } else if($attainment['attainment'] == "Associate Degree") {
                        $rate1[] = 4;
                    } else if($attainment['attainment'] == "Undergraduate") {
                        $rate1[] = 3;
                    } else if($attainment['attainment'] == "Vocational") {
                        $rate1[] = 3;
                    } else if($attainment['attainment'] == "High School") {
                        $rate1[] = 2;
                    } else{
                        $rate1[] = 0;
                    }
                }
                $maxVal = max($rate1);
                $star = ($maxVal * 20)/100;  
                
                } else{
                $star =0;
                }
    
                $experiences = $this->fetchWorkSingleData($userTokenCheck[0]['id']);
                if($experiences) {
                $totalDays = 0;
                foreach($experiences as $experience) {
                    $from = $experience['from'];
                    $to = $experience['to'];
                    $datetime1 = strtotime($from);
                    $datetime2 = strtotime($to);
                    $datediff = $datetime1 - $datetime2;
                    $totalDays = round($datediff / (60 * 60 * 24));
                }
    
                if($totalDays == 0) {
                    $rate2 = 1;
                } else if(($totalDays < 360) && ($totalDays > 0)) {
                    $rate2 = 2;
                } else if(($totalDays < 720) && ($totalDays > 300)) {
                    $rate2 = 3;
                } else if(($totalDays < 1080) && ($totalDays > 720)) {
                    $rate2 = 4;
                } else if($totalDays > 1080) {
                    $rate2 = 5;
                } else {
                    $rate2 = 0;
                }
                $star1 = $rate2 * 30 / 100;
                } else {
                $star1 = 0;
                }
    
                $tenure = $this->fetchWorkuniqueData($userTokenCheck[0]['id']);
                if($tenure) {
                    $totalDays1 = 0;
                    $countComp = 0;
                    foreach($experiences as $experience) {
                        $from = $experience['from'];
                        $to = $experience['to'];
                        $datetime1 = strtotime($from);
                        $datetime2 = strtotime($to);
                        $datediff = $datetime1 - $datetime2;
                        $totalDays1 = round($datediff / (60 * 60 * 24));
                        $countComp++;
                    }
    
                $avgWork = $totalDays1 / $countComp;
                if($avgWork == 0) {
                    $rate3 = 1;
                } else if($avgWork < 360 && $avgWork > 0) {
                    $rate3 = 2;
                } else if($avgWork < 720 && $avgWork > 300) {
                    $rate3 = 3;
                } else if($avgWork < 1080 && $avgWork > 720) {
                    $rate3 = 4;
                } else if($avgWork >= 1080) {
                    $rate3 = 5;
                } else{
                    $rate3 = 0;
                }
                $star2 = ($rate3 * 30)/100;
                } else{
                $star2 =0;
                }
                
                $self = $this->User_Model->user_assessment($userTokenCheck[0]['id']);
                if($self) {
                $totalSelf = $self[0]['verbal'] + $self[0]['written'] + $self[0]['listening'] + $self[0]['problem'];
                $avgSelf = $totalSelf / 5;
                $star3 = ($avgSelf * 20)/100;
                } else{
                $star3 = 0;
                }
    
                $totalStar = $star + $star1 + $star2 + $star3;
                $totalStar = number_format((float)$totalStar, 1, '.', '');
                $userData = $this->User_Model->user_single($userTokenCheck[0]['id']);
                if($userData[0]['profilePic'] != "") {
                    $userProfilePic = $userData[0]['profilePic'];
                } else{
                    $userProfilePic = "";
                }
    
                $data = ["rating"=> $totalStar, "profilePic"=> $userProfilePic, "name"=>$userData[0]['name']];
                $response = ["calculateRating"=>$data, "status"=>"SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            
            }
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }
  
    public function calculateRating($id) {
  
        $this->load->helper('date');
        $attainments = $this->fetchEduSingleData($id);
        if($attainments) {
            $rate1 = array();
            foreach($attainments as $attainment) {
                if($attainment['attainment'] == "Post Graduate") {
                    $rate1[] = 5;
                } else if($attainment['attainment'] == "College Graduate") {
                    $rate1[] = 5;
                } else if($attainment['attainment'] == "Associate Degree") {
                    $rate1[] = 4;
                } else if($attainment['attainment'] == "Undergraduate") {
                    $rate1[] = 3;
                } else if($attainment['attainment'] == "Vocational") {
                    $rate1[] = 3;
                } else if($attainment['attainment'] == "High School") {
                    $rate1[] = 2;
                } else{
                    $rate1[] = 0;
                }
            }
            $maxVal = max($rate1);
            $star = ($maxVal * 20)/100;  
        
        } else{
            $star =0;
        }
  
        $experiences = $this->fetchWorkSingleData($id);
        if($experiences) {
            $totalDays = 0;
            foreach($experiences as $experience) {
                $from = $experience['from'];
                $to = $experience['to'];
                $datetime1 = strtotime($from);
                $datetime2 = strtotime($to);
                $datediff = $datetime1 - $datetime2;
                $totalDays = round($datediff / (60 * 60 * 24));
            }

            if($totalDays == 0) {
                $rate2 = 1;
            } else if(($totalDays < 360) && ($totalDays > 0)) {
                $rate2 = 2;
            } else if(($totalDays < 720) && ($totalDays > 300)) {
                $rate2 = 3;
            } else if(($totalDays < 1080) && ($totalDays > 720)) {
                $rate2 = 4;
            } else if($totalDays > 1080) {
                $rate2 = 5;
            } else {
                $rate2 = 0;
            }
            $star1 = $rate2 * 30 / 100;
        } else {
            $star1 = 0;
        }
  
        $tenure = $this->fetchWorkuniqueData($id);
        if($tenure) {
            $totalDays1 = 0;
            $countComp = 0;
            foreach($experiences as $experience) {
                $from = $experience['from'];
                $to = $experience['to'];
                $datetime1 = strtotime($from);
                $datetime2 = strtotime($to);
                $datediff = $datetime1 - $datetime2;
                $totalDays1 = round($datediff / (60 * 60 * 24));
                $countComp++;
            }

            $avgWork = $totalDays1 / $countComp;
            if($avgWork == 0) {
                $rate3 = 1;
            } else if($avgWork < 360 && $avgWork > 0) {
                $rate3 = 2;
            } else if($avgWork < 720 && $avgWork > 300) {
                $rate3 = 3;
            } else if($avgWork < 1080 && $avgWork > 720) {
                $rate3 = 4;
            } else if($avgWork >= 1080) {
                $rate3 = 5;
            } else{
                $rate3 = 0;
            }
            $star2 = ($rate3 * 30)/100;
        } else{
            $star2 =0;
        }
            
        $self = $this->User_Model->user_assessment($id);
        if($self) {
            $totalSelf = $self[0]['verbal'] + $self[0]['written'] + $self[0]['listening'] + $self[0]['problem'];
            $avgSelf = $totalSelf / 5;
            $star3 = ($avgSelf * 20)/100;
        } else{
            $star3 = 0;
        }
        $totalStar = $star + $star1 + $star2 + $star3;
        $totalStar = number_format((float)$totalStar, 1, '.', '');
        return $totalStar;  
    }

    // Save Top clients
    public function topclients_post() {
        $clientData = $this->input->post();
        
        $data = ["token" => $clientData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck) {
                
                $data = ["user_id"=>$userTokenCheck[0]['id'], "clients"=>$clientData['clients']];
                $clientCheck = $this->User_Model->client_beforeinsertcheck($data);

                if($clientCheck) {
                    $errors = "Client already added";
                    $response = ['message' => $errors, 'status' => "FAILURE"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                } else {

                    if(empty($clientData['id'])) {
                        $clientCount = $this->User_Model->client_beforeInsertCount($userTokenCheck[0]['id']);

                        if(count($clientCount) < 5) {
                        $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                        if($getCompleteness) {
                            if($getCompleteness[0]['topClient'] == 0) {
                                $profileComplete = 10;
                                $comProfile = ["topClient" => $profileComplete];
                                $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                            }
                        }
                        $data = ["user_id"=>$userTokenCheck[0]['id'], "clients"=>$clientData['clients']];
                        $clientInserted = $this->User_Model->topClient_insert($data);

                        if($clientInserted) {

                            $this->successsignupone($userTokenCheck[0]['id']);

                            $userId = $userTokenCheck[0]['id'];
                            $clientData = $this->fetchClientSingleData($userId);
                            $response = ['topclients' => $clientData, 'status' => "SUCCESS"];
                            $this->set_response($response, REST_Controller::HTTP_CREATED);
                        } else{
                            $errors = "Data not inserted";
                            $response = ['message' => $errors, 'status' => "FAILURE"];
                            $this->set_response($response, REST_Controller::HTTP_CREATED);
                        }
                        } else{
                            $errors = "Clients not accepted more than 5";
                            $response = ['message' => $errors, 'status' => "FAILURE"];
                            $this->set_response($response, REST_Controller::HTTP_CREATED);
                        }

                    } else {
                        $data = ["clients"=>$clientData['clients']];
                        $clientInserted = $this->User_Model->topClient_update($data, $clientData['id']);

                        if($clientInserted) {

                            $this->successsignupone($userTokenCheck[0]['id']);

                            $userId = $userTokenCheck[0]['id'];
                            $clientData = $this->fetchClientSingleData($userId);
                            $response = ['topclients' => $clientData, 'status' => "SUCCESS"];
                            $this->set_response($response, REST_Controller::HTTP_CREATED);
                        } else{
                            $errors = "Data not inserted";
                            $response = ['message' => $errors, 'status' => "FAILURE"];
                            $this->set_response($response, REST_Controller::HTTP_CREATED);
                        }
                    }
                }
            }
        } else{
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    // Check Resume
    public function checkResume_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1) {
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck) {
        
                $job_detail = $this->Jobpost_Model->job_detail_fetch($userData['jobpost_id']);
                if($job_detail[0]['education_status']=='1'){
                    $user_education_match = $this->User_Model->user_single_educationbyuserid1($userTokenCheck[0]['id']);

                    if($user_education_match){
                        $education_match = "True";
                    } else{
                        $education_match = "False";
                    }

                } else {
                    $education_match = "True";
                }
                if($job_detail[0]['experience_status']=='1'){
                    $expmonth = $userTokenCheck[0]['exp_month'];
                    $expyear = $userTokenCheck[0]['exp_year'];

                    if($userTokenCheck[0]['exp_month']>=0   && $userTokenCheck[0]['exp_year']>=0){
                        $experience_match = "True";
                    }  else{
                        $experience_match = 'False';
                    }

                }else{
                    $experience_match = "True";
                }
                if($education_match == "True" && $experience_match== "True") {
                    $checkResume = $this->User_Model->resume_single($userTokenCheck[0]['id']);
                    if($checkResume) {
                        $response = ['message' => "Resume found", 'status' => "SUCCESS"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    } else{
                        $response = ['message' => "Resume not found", 'status' => "SUCCESS"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                }else{
                    $errors = "Please update your Total Years of Experience and Highest Education Level to qualify for this job.";
                    $response = ['message' => $errors, 'status' => "FAILURE"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
            }
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    // Superpower listing
    public function superpowerlist_get() {
        $jobData = $this->input->post();

        $superpower_list = $this->Jobpost_Model->category_lists1();
        if($superpower_list){
            $x=0;
            foreach ($superpower_list as $superpower_lists) {
            $superpower_list1[$x] = [
                "id" => $superpower_lists['id'],
                "category" => $superpower_lists['category']
            ];
        $x++;
        }  
        }else{
            $superpower_list1=[];
        }
        $response = ['superpowerlist' => $superpower_list1, 'status' => "SUCCESS"];
        $this->set_response($response, REST_Controller::HTTP_CREATED);
    }

    // Signup category list
    public function signupcategorylist_get() {
        $category_list = $this->Jobpost_Model->category_lists();
        if($category_list) {
            $x=0;
            foreach ($category_list as $category_lists) {
                $category_list1[$x] = [
                      "id" => $category_lists['id'],
                      "category" => $category_lists['category']                    

                  ];
                $x++;
            }  
        }else{
            $category_list1=[];
        }
        
        $response = ['categorylist' => $category_list1, 'status' => "SUCCESS"];
        $this->set_response($response, REST_Controller::HTTP_CREATED);
    }   

    // User Intrested in
    public function userintrestedin_post() {
        
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);

        if($userTokenCheck1) {

            $userData = $this->User_Model->user_single($userTokenCheck1[0]['user_id']);
            $subcategoryArray = array();
            $subcategoryArray1 = array();

            if($userData[0]['industry'] == "Hotels" || $userData[0]['industry'] == "Restaurants") {

            	$userData[0]['industry'] = "Non BPO";
            
            } else if($userData[0]['industry'] == "Hotels") {

            	$userData[0]['industry'] = "BPO - IT";
            }

            $subcategory_list = $this->Common_Model->jobintrested_list_byindustry($userData[0]['industry']);
            if($subcategory_list) {
                
                foreach ($subcategory_list as $subcategory_lists) {
                    
                    if (strpos($userData[0]['jobsInterested'], $subcategory_lists['name']) !== false) {
                        
                        $subcategoryArray[] = [
                              "id" => $subcategory_lists['id'],           
                              "subcategory" => $subcategory_lists['name']
                        ];
                        $subcategoryArray1[] = [
                          "id" => $subcategory_lists['id'],
                          "subcategory" => $subcategory_lists['name'],
                          "selected"=>1
                        ];

                    } else {
                        $subcategoryArray1[] = [
                          "id" => $subcategory_lists['id'],
                          "subcategory" => $subcategory_lists['name'],
                          "selected"=>0
                        ];
                    }
                }  
            } else {
                $subcategoryArray=[];
            }

            $data = ["token" => $userData[0]['token'], "subcategorylist"=>$subcategoryArray, "subcategorylist1"=>$subcategoryArray1];

            $response = ['msg' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {

            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function getLocation_post() {
        $data = $this->input->post();
        $lat = $data['latitude'];
        $long = $data['longitude'];
        $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$lat.','.$long.'&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk');
        $output = json_decode($geocodeFromAddr);
        var_dump($output);die;
        $address = array(
            'country' => google_getCountry($output),
            'province' => google_getProvince($output),
            'city' => google_getCity($output),
            'street' => google_getStreet($output),
            'postal_code' => google_getPostalCode($output),
            'country_code' => google_getCountryCode($output),
            'formatted_address' => google_getAddress($output),
        );
        $response = ['getLocation' => $address, 'status' => "SUCCESS"];
    }

    public function resume_delete_post() {
        
        $userData = $this->input->post();
        $this->form_validation->set_rules('token', 'token', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {

            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } else {

            $data = ["token" => $userData['token']];
            $userTokenCheck1 = $this->User_Model->token_match1($data);

            if($userTokenCheck1) {

                $checkResume = $this->User_Model->resume_single($userTokenCheck1[0]['user_id']);
                if($checkResume) {
                    unlink($checkResume[0]['resume']);
                    $this->User_Model->resume_delete($userTokenCheck1[0]['user_id']);
                
                    $response = ['resume_delete' => "Resume deleted successfully", 'status' => "SUCCESS"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                
                } else {
                    $errors = "Resume not found";
                    $response = ['message' => $errors, 'status' => "FAILURE"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }

            } else {
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    public function file_check($str){
        $allowed_mime_type_arr = array('application/pdf', 'application/doc', 'application/docx');
        $mime = get_mime_by_extension($_FILES['resumeFile']['name']);
        if(isset($_FILES['resumeFile']['name']) && $_FILES['resumeFile']['name']!=""){
            if(in_array($mime, $allowed_mime_type_arr)) {
                return true;
            }else{
                $this->form_validation->set_message('file_check', 'Please select only pdf/Doc file.');
                return false;
            }
        }else{
            $this->form_validation->set_message('file_check', 'Please choose a file to upload.');
            return false;
        }
    }

    public function getdynamicresume_post() {

        $profileData = $this->input->post();
        $data = ["token" => $profileData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);  
            
            if($userTokenCheck) {

                $this->successsignupone($userTokenCheck[0]['id']);
            
                $dynamicResumeData = $this->User_Model->user_dynamic_resume_single($userTokenCheck[0]['id']);
                if($dynamicResumeData) {
                    $dynamicresume = base_url().$dynamicResumeData[0]['resume'];
                } else {

                    $dynamicresume = "";
                }

                $response = ['getdynamicresume' => $dynamicresume, 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);

            } else {
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }

        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }

    }

    public function fetchUserSingleDataByToken($id, $deviceToken, $deviceType) {
        $userData = $this->User_Model->user_single($id);
        if($userData[0]['designation']){
            $userData[0]['designation']=$userData[0]['designation'];
        }else{
            $userData[0]['designation']="";
        }
        if($userData[0]['current_salary']){
            $userData[0]['current_salary']=$userData[0]['current_salary'];
        }else{
            $userData[0]['current_salary']="";
        }
        if($userData[0]['exp_month']){
            $userData[0]['exp_month']=$userData[0]['exp_month'];
        }else{
            $userData[0]['exp_month']="";
        }
        if($userData[0]['exp_year']){
            $userData[0]['exp_year']=$userData[0]['exp_year'];
        }else{
            $userData[0]['exp_year']="";
        }

        if($userData[0]['jobsbenefits']){
            $userData[0]['jobsbenefits']=$userData[0]['jobsbenefits'];
        }else{
            $userData[0]['jobsbenefits']="";
        }
        if($userData[0]['work_mode']){
            $userData[0]['work_mode']=$userData[0]['work_mode'];
        }else{
            $userData[0]['work_mode']="";
        }
        if($userData[0]['vaccination']){
            $userData[0]['vaccination']=$userData[0]['vaccination'];
        }else{
            $userData[0]['vaccination']="";
        }
        if($userData[0]['relocate']){
            $userData[0]['relocate']=$userData[0]['relocate'];
        }else{
            $userData[0]['relocate']="";
        }

        $data = ["user_id"=>$id, "device_token"=>$deviceToken, "device_type"=>$deviceType];
        $userToken = $this->User_Model->user_token_check($data);


        $data = ["token" => $userToken[0]['token'], "id" => $userData[0]['id'], "name" => $userData[0]['name'], "email" => $userData[0]['email'], "country_code" => $userData[0]['country_code'], "phone" => $userData[0]['phone'], "designation" => $userData[0]['designation'], "current_salary" => $userData[0]['current_salary'], "exp_month" => $userData[0]['exp_month'], "exp_year" => $userData[0]['exp_year'], "type" => $userData[0]['type'], "profilePic" => $userData[0]['profilePic'], "completeness" => $userData[0]['completeness'], "jobsbenefits" => $userData[0]['jobsbenefits'], "work_mode" => $userData[0]['work_mode'], "vaccination" => $userData[0]['vaccination'], "relocate" => $userData[0]['relocate']];
        return $data;
    }

    public function fetchUserSingleData($id) {
        $userData = $this->User_Model->user_single($id);
        if($userData[0]['designation']){
            $userData[0]['designation']=$userData[0]['designation'];
        }else{
            $userData[0]['designation']="";
        }
        if($userData[0]['education']){
            $userData[0]['education']=$userData[0]['education'];
        }else{
            $userData[0]['education']="";
        }
        if($userData[0]['location']){
            $userData[0]['location']=$userData[0]['location'];
        }else{
            $userData[0]['location']="";
        }
        if($userData[0]['superpower']){
            $userData[0]['superpower']=$userData[0]['superpower'];
        }else{
            $userData[0]['superpower']="";
        }
        if($userData[0]['current_salary']){
            $userData[0]['current_salary']=$userData[0]['current_salary'];
        }else{
            $userData[0]['current_salary']="";
        }
        if($userData[0]['exp_month']){
            $userData[0]['exp_month']=$userData[0]['exp_month'];
        }else{
            $userData[0]['exp_month']="";
        }
        if($userData[0]['exp_year']){
            $userData[0]['exp_year']=$userData[0]['exp_year'];
        }else{
            $userData[0]['exp_year']="";
        }
        if($userData[0]['nationality']){
            $userData[0]['nationality']=$userData[0]['nationality'];
        }else{
            $userData[0]['nationality']="";
        }

        if($userData[0]['jobsbenefits']){
            $userData[0]['jobsbenefits']=$userData[0]['jobsbenefits'];
        }else{
            $userData[0]['jobsbenefits']="";
        }
        if($userData[0]['work_mode']){
            $userData[0]['work_mode']=$userData[0]['work_mode'];
        }else{
            $userData[0]['work_mode']="";
        }
        if($userData[0]['vaccination']){
            $userData[0]['vaccination']=$userData[0]['vaccination'];
        }else{
            $userData[0]['vaccination']="";
        }
        if($userData[0]['relocate']){
            $userData[0]['relocate']=$userData[0]['relocate'];
        }else{
            $userData[0]['relocate']="";
        }
        
        $intrestedGET = explode(',',$userData[0]['jobsInterested']);
        $intrestedGETNew = array();
        foreach($intrestedGET as $intrestedG) {
            $intrestedGETNew[] = $intrestedG;
        }
        $data = ["token" => $userData[0]['token'], "id" => $userData[0]['id'], "name" => $userData[0]['name'], "email" => $userData[0]['email'], "type" => $userData[0]['type'], "profilePic" => $userData[0]['profilePic'], "phone" => $userData[0]['phone'], "country_code" => $userData[0]['country_code'], "dob" => $userData[0]['dob'], "location" => $userData[0]['location'], "designation" => $userData[0]['designation'], "current_salary" => $userData[0]['current_salary'], "exp_month" => $userData[0]['exp_month'], "exp_year" => $userData[0]['exp_year'],"education" => $userData[0]['education'],"nationality" => $userData[0]['nationality'],"superpower" => $userData[0]['superpower'],"jobsInterested" => $intrestedGETNew,"state" => $userData[0]['state'],"city" => $userData[0]['city'], "timeforcall" => $userData[0]['timeforcall'], "bpoyear" => $userData[0]['bpoyear'], "bpomonth" => $userData[0]['bpomonth'], "jobsearch_status" => $userData[0]['jobsearch_status'], "topbpo" => $userData[0]['topbpo'], "industry" => $userData[0]['industry'], "specialization" => $userData[0]['specialization'], "sub_specialization" => $userData[0]['sub_specialization'], "haveinternet" => $userData[0]['haveinternet'], "internetspeed" => $userData[0]['internetspeed'], "howhear" => $userData[0]['howhear'], "jobsbenefits" => $userData[0]['jobsbenefits'], "work_mode" => $userData[0]['work_mode'], "vaccination" => $userData[0]['vaccination'], "relocate" => $userData[0]['relocate']];
        return $data;
    }

    public function fetchUserCompleteData($id) {
        $userData = $this->User_Model->userComplete_single($id);
        
        if($userData) {
          $data['comp'] = $userData[0]['personal'] + $userData[0]['proffesional'] + $userData[0]['other'] + $userData[0]['other_more'] + $userData[0]['resume'] + $userData[0]['dynamic_resume'] + $userData[0]['experience'] + $userData[0]['education'] + $userData[0]['language'] + $userData[0]['topClient'];
        } else{
          $data['comp'] = 0;
        }
        return $data;
    }

    public function fetchWorkSingleData($id) {
        $workDatas = $this->User_Model->work_single($id);
        if($workDatas) {
            foreach ($workDatas as $workData) {
                $data[] = ["id"=> $workData['id'], "title" => $workData['title'], "company" => $workData['company'], "desc" => $workData['jobDesc'], "from" => $workData['workFrom'], "to" => $workData['workTo'], "category"=>$workData['category'], "categoryid"=>$workData['categoryid'], "subcategory"=> $workData['subcategory'],  "subcategoryid"=> $workData['subcatid'], "level"=> $workData['level'], "levelid"=> $workData['levelid'], "currently_working"=> $workData['currently_working']];
            }
            return $data;
        } else{
            $data = [];
            return $data;
        }
    }

    public function fetchWorkuniqueData($id) {
        $workDatas = $this->User_Model->work_unique($id);
        if($workDatas) {
            foreach ($workDatas as $workData) {
                $data[] = ["id"=> $workData['id'], "title" => $workData['title'], "company" => $workData['company'], "desc" => $workData['jobDesc'], "from" => $workData['workFrom'], "to" => $workData['workTo'], "category"=>$workData['category'], "categoryid"=>$workData['categoryid'], "subcategory"=> $workData['subcategory'],  "subcategoryid"=> $workData['subcatid'], "level"=> $workData['level'], "levelid"=> $workData['levelid'], "currently_working"=> $workData['currently_working']];
            }
            return $data;
        } else{
            $data = [];
            return $data;
        }
    }

    public function fetchEduSingleData($id) {
        $eduDatas = $this->User_Model->edu_single($id);
        if($eduDatas) {
            foreach ($eduDatas as $eduData) {
                $data[] = ["id"=> $eduData['id'], "attainment" => $eduData['attainment'], "degree" => $eduData['degree'], "university" => $eduData['university'], "from" => $eduData['degreeFrom'], "to" => $eduData['degreeTo']];
            }
            return $data;
        } else{
            $data = [];
            return $data;
        }
    }

    public function fetchLangSingleData($id) {
        $langDatas = $this->User_Model->lang_single($id);
        if($langDatas) {
            foreach ($langDatas as $langData) {
                $data[] = ["id"=> $langData['id'],"lang_id"=> $langData['lang_id'], "lang_name" => $langData['name']];
            }
            return $data;
        } else {
            $data = [];
            return $data;
        }
    }

    public function fetchClientSingleData($id) {
        $clientDatas = $this->User_Model->client_single($id);
        if($clientDatas) {
            return $clientDatas;
        } else {
            $data = [];
            return $data;
        }
    }

    public function fetchSkillsSingleData($id) {
        $skillsDatas = $this->User_Model->skills_single($id);
        if($skillsDatas) {
            return $skillsDatas;
        } else {
            $data = [];
            return $data;
        }
    }

    public function fetchMoreSingleData($id) {
        $moreDatas = $this->User_Model->user_more($id);
        if($moreDatas) {
            $dataMore = ["id"=>$moreDatas[0]['id'], "strength"=>$moreDatas[0]['strength'], "weakness"=> $moreDatas[0]['weakness'], "achievements"=>$moreDatas[0]['achievements']];
            return $dataMore;
        } else {
            $data = [];
            return $data;
        }
    }

    public function jobbenefits_get() {
        //$data = $this->input->post();
        $benefits_array = array();

        $intestedin_lists = $this->Common_Model->jobbenefits_list();
        if($intestedin_lists) {
            
            foreach ($intestedin_lists as $intestedin_list) {
                
                $benefits_array[] = [
                      "id" => $intestedin_list['id'],
                      "name" => $intestedin_list['sub_benefits']
                  ];
            }
        }

        $response = ['jobbenefits' => $benefits_array, 'status' => "SUCCESS"];
        $this->set_response($response, REST_Controller::HTTP_CREATED);
    }

    /*
    * file value and type check during validation
    */
    public function img_check($str){
        $allowed_mime_type_arr = array('jpeg','jpg','png','x-png');
        $image_parts = explode(";base64,", $image);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];

        if(in_array($image_type, $allowed_mime_type_arr)){
            return true;
        }else{
            $this->form_validation->set_message('file_check', 'Please select only jpg/jpeg/png file.');
            return false;
        }
    }

    function compareDate() {
        $startDate = strtotime($_POST['from']);
        $endDate = strtotime($_POST['to']);
        if ($endDate <= $startDate)
          return True;
        else {
          $this->form_validation->set_message('compareDate', '%s should be greater than Contract Start Date.');
          return False;
        }
    }
  
    function getToken(){
        $string = "";
        $chars = "0123456789";
        for($i=0;$i<4;$i++)
        $string.=substr($chars,(rand()%(strlen($chars))), 1);
        return $string;
    }

    public function valid_password($password = '') {
        $password = trim($password);
        $regex_lowercase = '/[a-z]/';
        $regex_uppercase = '/[A-Z]/';
        $regex_number = '/[0-9]/';
        $regex_special = '/[!@#$%^&*()\-_=+{};:,<.>§~]/';

        if (empty($password)) {
            $this->form_validation->set_message('valid_password', 'Password is required.');

            return FALSE;
        }
        if (preg_match_all($regex_lowercase, $password) < 1) {
            $this->form_validation->set_message('valid_password', 'Password must have at least one lowercase letter.');

            return FALSE;
        }
        if (preg_match_all($regex_uppercase, $password) < 1) {
            $this->form_validation->set_message('valid_password', 'Password must have at least one uppercase letter.');

            return FALSE;
        }
        if (preg_match_all($regex_number, $password) < 1) {
            $this->form_validation->set_message('valid_password', 'Password must have at least one number.');

            return FALSE;
        }
        if (preg_match_all($regex_special, $password) < 1) {
            $this->form_validation->set_message('valid_password', 'Password must have at least one special character.' . ' ' . htmlentities('!@#$%^&*()\-_=+{};:,<.>§~'));

            return FALSE;
        }

        if (strlen($password) < 5) {
            $this->form_validation->set_message('valid_password', 'Password must be at least 5 characters in length.');

            return FALSE;
        }
        if (strlen($password) > 32) {
            $this->form_validation->set_message('valid_password', 'Password cannot exceed 32 characters in length.');

            return FALSE;
        }
        return TRUE;
    }

    // Create Dynamic Resume
    public function successsignupone($userid) {

        $data['profileDetail'] = $this->fetchUserSingleData($userid);
        $data['strengthdata'] = $this->User_Model->user_more($userid);
        $data["phonecodes"] = $this->Common_Model->phonecode_lists();
        $data['languageData'] = $this->User_Model->lang_single($userid);
        $data['clientData'] = $this->User_Model->client_single($userid);
        $data['skillData'] = $this->User_Model->skills_single($userid);
        $data['assessData'] = $this->User_Model->user_assessment($userid);
        $data['expertData'] = $this->User_Model->user_expert($userid);
        $data['industrylists'] = $this->Common_Model->industry_lists();
        $data['levellist'] = $this->Common_Model->level_lists();
        $data['categorylist'] = $this->Common_Model->category_lists();
        $data['subcategorylist'] = $this->Common_Model->subcategory_lists();
        $data['subcategorylist1'] = $this->Common_Model->subcategory_lists1();
        $data['channellists'] = $this->Common_Model->channel_lists();
        $data['channellistss'] = $this->Common_Model->channel_lists();
        $data["nations"] = $this->Common_Model->nation_lists();
        $data["powers"] = $this->Common_Model->power_lists();
        $data['workData'] = $this->fetchWorkSingleData($userid);

        $data['industries'] = $this->Common_Model->industry_lists();
        $data['topbpos'] = $this->Common_Model->bpo_lists();

        $data['getCompletenes'] = $this->fetchUserCompleteData($userid);
        $data['languagelists'] = $this->Common_Model->language_lists();
        $data['eduData'] = $this->fetchEduSingleData($userid);
        $data['checkResume'] = $this->User_Model->resume_single($userid);

        $data['checkStatus'] = $this->User_Model->user_single($userid);
        $data["intrestedin"] = $this->Jobpost_Model->filtersubcategory_listsbycatid(9);
        $data["states"] = $this->User_Model->getownstate();

        $this->load->library('pdf');
        $html = $this->load->view('dynamic_resume', $data, true);

        $filename = md5(rand()). '.pdf';
        $file_name = "./dynamic_resume/".$filename;
        $file_name_one = "dynamic_resume/".$filename;
        $this->pdf->loadHtml($html);
        $this->pdf->setPaper('A4', 'portrait');
        $this->pdf->render();
        $file = $this->pdf->output();

        file_put_contents($file_name,  $file);
        
        $dynamicResumeCheck = $this->User_Model->user_dynamic_resume_single($userid);
        if($dynamicResumeCheck) {
            $oldURL = $dynamicResumeCheck[0]['resume'];
            unlink($oldURL);
            $dataResume = ["resume"=>$file_name_one];
            $this->User_Model->user_dynamic_resume_update($dataResume, $userid);

        } else {

            $dataResume = ["user_id"=>$userid, "resume"=>$file_name_one];
            $this->User_Model->user_dynamic_resume($dataResume);
        }

        return true;
    }

}
?>
