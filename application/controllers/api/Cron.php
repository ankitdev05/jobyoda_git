<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');
define('API_ACCESS_KEY', 'AIzaSyCTzjJxETlJxp18hCwYHLFETLZRkbGFiGw');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittall
 * @license         Mobulous
 */
class Cron extends CI_Controller {
    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('newmodels/Cron_Model');
        $this->load->model('newmodels/User_Model');
        $this->load->model('newmodels/recruiter/Candidate_Model');
        $this->load->model('newmodels/Jobseekeradmin_Model');
        $this->load->model('newmodels/Recruiteradmin_Model');
        $this->load->library('aws3');
    }

    public function sendReminder() {
        $appliedJobs = $this->Cron_Model->jobappliedupcoming_fetch();
        $date = date('Y-m-d H:i:s');
        foreach ($appliedJobs as $jobs) {
            if ($hourdiff == 0) {
                $mobileNumber = $jobs['country_code'] . $jobs['phone'];

                if($jobs['mode'] == "Walk-in") {
     
                    $msgege = "Hi, you have an upcoming interview tomorrow for " . $jobs['jobtitle'] . " at " . $jobs['cname'] . ". We'll be expecting you at " . $jobs['interviewtime'] . ". Please look for " . $jobs['fname'] . " " . $jobs['lname'] . ".Make sure to show up on time as this is your slot. Best of luck from JobYoDA team.";
                } else {

                    $msgege = "Hi, you have an upcoming interview tomorrow for " . $jobs['jobtitle'] . " with " . $jobs['cname'] . ". Please keep your phone line open as the recruiter will call you. Best of luck from JobYoDA team.";
                }

                $data1 = ["user_id" => $jobs['user_id'], "jobapp_id" => $jobs['id'], "recruiter_id" => $jobs['recruiter_id'], "notification" => $msgege, "status_changed" => 3, "job_status" => '0', "badge_count" => 1, "createdon" => $date];
                $notificationInserted = $this->Candidate_Model->notification_insert($data1);
                if ($notificationInserted) {
                    $fetchnotifications = $this->Candidate_Model->fetchnotifications($jobs['user_id']);
                }
                if ($jobs['notification_status'] == '1') {
                    $tokendata = ['user_id' => $jobs['user_id']];
                    $userTokenCheck = $this->User_Model->user_token_check($tokendata);
                    foreach ($userTokenCheck as $usertoken) {
                        if ($usertoken['device_type'] == "android") {
                            echo $res1 = $this->push_notification_android_customer($usertoken['device_token'], $jobs['jobtitle'], $msgege, 'applied', $jobs['id'], $jobs['cname'], count($fetchnotifications));
                        } else {
                            $this->pushiphon(array($usertoken['device_token'], $msgege, 'applied', $jobs['jobtitle'], $jobs['id'], count($fetchnotifications), $jobs['cname']));
                        }
                    }
                }
            }
        }
    }
    public function send_email() {
        $appliedJobs = $this->Cron_Model->jobappliedupcoming_fetch1();
        $now = time();
        foreach ($appliedJobs as $jobs) {
            $phone = $this->Cron_Model->getPhone($jobs['recruiter_id']);
            echo $mobileNumber = '+' . $phone[0]['phonecode'] . $phone[0]['phone'];
            $msgege = "Good morning " . $jobs['fname'] . " " . $jobs['lname'] . "! You have " . $jobs['candCount'] . " candidates lined up for an interview today at " . $jobs['cname'] . ". Head on to http://bit.ly/32gUJrW to view all applicants.";
            $this->load->library('email');
            $data['candCount'] = $jobs['candCount'];
            $data['cname'] = $jobs['cname'];
            $data['name'] = $jobs['fname'] . ' ' . $jobs['lname'];
            $msg = $this->load->view('recruiter/recruiteremail', $data, TRUE);
            $config = array('charset' => 'utf-8', 'wordwrap' => TRUE, 'mailtype' => 'html');
            $this->email->initialize(['protocol' => 'smtp', 'smtp_host' => 'smtpout.asia.secureserver.net', 'smtp_user' => 'help@jobyoda.com', 'smtp_pass' => 'Success@2021', 'smtp_port' => 465, 'smtp_crypto' => 'ssl', 'charset' => 'utf-8', 'mailtype' => 'html', 'crlf' => "\r\n", 'newline' => "\r\n"]);
            $this->email->from('help@jobyoda.com', "JobYoDA");
            $this->email->to($jobs['email']);
            $this->email->subject('JobYoDA Upcoming Interview - ' . $jobs["cname"] . ' - Date ' . date('Y-m-d'));
            $this->email->message($msg);
            $this->email->send();
            echo "<br>";
        }
        $this->send_subemail();
    }

    public function send_subemail() {
        $appliedJobs = $this->Cron_Model->jobappliedupcomingsub_fetch1();
        $now = time();
        foreach ($appliedJobs as $jobs) {
            $phone = $this->Cron_Model->getPhone($jobs['subrecruiter_id']);
            echo $mobileNumber = '+' . $phone[0]['phonecode'] . $phone[0]['phone'];
            $msgege = "Good morning " . $jobs['fname'] . " " . $jobs['lname'] . "! You have " . $jobs['candCount'] . " candidates lined up for an interview today at " . $jobs['cname'] . ". Head on to http://bit.ly/32gUJrW to view all applicants.";
            $this->load->library('email');
            $data['candCount'] = $jobs['candCount'];
            $data['cname'] = $jobs['cname'];
            $data['name'] = $jobs['fname'] . ' ' . $jobs['lname'];
            $msg = $this->load->view('recruiter/recruiteremail', $data, TRUE);
            $config = array('charset' => 'utf-8', 'wordwrap' => TRUE, 'mailtype' => 'html');
            $this->email->initialize(['protocol' => 'smtp', 'smtp_host' => 'smtpout.asia.secureserver.net', 'smtp_user' => 'help@jobyoda.com', 'smtp_pass' => 'Success@2021', 'smtp_port' => 465, 'smtp_crypto' => 'ssl', 'charset' => 'utf-8', 'mailtype' => 'html', 'crlf' => "\r\n", 'newline' => "\r\n"]);
            $this->email->from('help@jobyoda.com', "JobYoDA");
            $this->email->to($jobs['email']);
            $this->email->subject('JobYoDA Upcoming Interview - ' . $jobs["cname"] . ' - Date ' . date('Y-m-d'));
            $this->email->message($msg);
            $this->email->send();
            echo "<br>";
        }
    }

    public function reminder_email() {
        $pending = $this->Cron_Model->candidateduenotification_list();
    
        if (!empty($pending)) {
            foreach ($pending as $pendings) {
                $this->load->library('email');
                $data['candCount'] = $pendings['candCount'];
                $data['name'] = $pendings['fname'] . ' ' . $pendings['lname'];
                $msg = $this->load->view('recruiter/reminderemail', $data, TRUE);
                $config = array('charset' => 'utf-8', 'wordwrap' => TRUE, 'mailtype' => 'html');
                $this->email->initialize(['protocol' => 'smtp', 'smtp_host' => 'smtpout.asia.secureserver.net', 'smtp_user' => 'help@jobyoda.com', 'smtp_pass' => 'Success@2021', 'smtp_port' => 465, 'smtp_crypto' => 'ssl', 'charset' => 'utf-8', 'mailtype' => 'html', 'crlf' => "\r\n", 'newline' => "\r\n"]);
                $this->email->from('help@jobyoda.com', "JobYoDA");
                $this->email->to($pendings['email']);
                $this->email->subject('JobYoDA Overdue Application Status - Date ' . date('Y-m-d'));
                $this->email->message($msg);
                $this->email->send();
            }
        }
        $this->remindersub_email();
    }
    public function remindersub_email() {
        $pending = $this->Cron_Model->candidatesubnotification_list();
        if (!empty($pending)) {
            foreach ($pending as $pendings) {
                $this->load->library('email');
                $data['candCount'] = $pendings['candCount'];
                $data['name'] = $pendings['fname'] . ' ' . $pendings['lname'];
                $msg = $this->load->view('recruiter/reminderemail', $data, TRUE);
                $config = array('charset' => 'utf-8', 'wordwrap' => TRUE, 'mailtype' => 'html');
                $this->email->initialize(['protocol' => 'smtp', 'smtp_host' => 'smtpout.asia.secureserver.net', 'smtp_user' => 'help@jobyoda.com', 'smtp_pass' => 'Success@2021', 'smtp_port' => 465, 'smtp_crypto' => 'ssl', 'charset' => 'utf-8', 'mailtype' => 'html', 'crlf' => "\r\n", 'newline' => "\r\n"]);
                $this->email->from('help@jobyoda.com', "JobYoDA");
                $this->email->to($pendings['email']);
                $this->email->subject('JobYoDA Overdue Application Status - Date ' . date('Y-m-d'));
                $this->email->message($msg);
                $this->email->send();
            }
        }
    }
    public function sendpromo() {
        $this->sendReminder();
    }

    public function push_promo() {
        $userdatas = $this->Jobseekeradmin_Model->fetchPromo();

        if ($userdatas) {

            $senderTokens = array();
            $senderIds = array();

            for ($j = 0; $j < sizeof($userdatas); $j++) {

                $getUsers = $this->Jobseekeradmin_Model->fetchUser($userdatas[$j]['user_id']);

                if ($getUsers) {
                    
                    foreach ($getUsers as $getUser) {

                        $checkNotifyOn = $this->Jobseekeradmin_Model->user_single($getUser['user_id']);

                        if ($checkNotifyOn) {

                            if ($checkNotifyOn[0]['notification_status'] == 1) {

                                $senderTokens[] = $getUser['device_token'];
                            }
                            
                        }
                    }
                    $senderIds[] = $userdatas[$j]['user_id'];

                    if(count($senderTokens) > 0) {
                        
                        
                        $testingDataSave = ["senderTokens"=>implode(',',$senderTokens), "title"=>$userdatas[$j]['title'], "message"=>$userdatas[$j]['message'],"promoid"=>$userdatas[$j]['id'],"userid"=>implode(',',$senderIds)];
                        $this->Jobseekeradmin_Model->promo_inserted($testingDataSave);


                        $senAndroid = $this->push_notification_android_customer3($senderTokens, urldecode($userdatas[$j]['title']), urldecode($userdatas[$j]['message']), 'promo', 0, $userdatas[$j]['id']);
                    }
                }

                $this->Jobseekeradmin_Model->batchUpdatePromo($userdatas[$j]['id']);
            }
        }
    }

    public function push_promo_limited() {
        $userdatas = $this->Jobseekeradmin_Model->fetchPromoLimit();
        $xx=1;
        if ($userdatas) {

            for ($j = 0;$j < sizeof($userdatas);$j++) {
        
                $getUsers = $this->Jobseekeradmin_Model->fetchUser($userdatas[$j]['user_id']);
                if ($getUsers) {
                    
                    foreach ($getUsers as $getUser) {

                        $checkNotifyOn = $this->Jobseekeradmin_Model->user_single($getUser['user_id']);
                        if ($checkNotifyOn) {
        
                            if ($checkNotifyOn[0]['notification_status'] == 1) {
        
                                if ($getUser['device_type'] == "ios") {
        
                                    $senAndroid = $this->pushiphon1(array($getUser['device_token'], urldecode($userdatas[$j]['message']), 'promo', urldecode($userdatas[$j]['title']), 0, $userdatas[$j]['id']));
                                
                                } else {
        
                                    $senAndroid = $this->push_notification_android_customer1($getUser['device_token'], urldecode($userdatas[$j]['title']), urldecode($userdatas[$j]['message']), 'promo', 0, $userdatas[$j]['id']);
                                }
                                $xx++;
                            }
                        }
                    }
                }
                $this->Jobseekeradmin_Model->batchUpdatePromo($userdatas[$j]['id']);
            }
        }
        echo $xx;
    }

    public function jobupdationpush_notify() {

        $maindatas = $this->Jobseekeradmin_Model->fetchMainJobUpdationNotify();        
        if($maindatas) {

            $userdatas = $this->Jobseekeradmin_Model->fetchJobNotify($maindata[0]['id']);
            if ($userdatas) {

                $senderTokens = array();
                $senderIds = array();

                foreach($userdatas as $userdata) {

                    $senderTokens[] = $userdata['device_token'];
                    $senderIds[] = $userdata['id'];
                }

                $this->Jobseekeradmin_Model->jobpushUpdate($senderIds);
                $senAndroid = $this->push_notification_android_customer2($senderTokens, urldecode($maindata[0]['title']), urldecode($maindata[0]['notification']), 'jobnotify', 0, $maindata[0]['job_id']);
            }

            $checkMain = $this->Jobseekeradmin_Model->checkMainJobNotify($maindata[0]['id']);
            if(count($checkMain) > 0) { } else {
                $this->Jobseekeradmin_Model->updateMainJobNotify($maindata[0]['id']);
            }
        }
    }


    public function jobpush_notify() {

        $maindatas = $this->Jobseekeradmin_Model->fetchMainJobNotify();        

        if($maindatas) {

            $userdatas = $this->Jobseekeradmin_Model->fetchJobNotify($maindatas[0]['id']);
            if ($userdatas) {

                $senderTokens = array();
                $senderIds = array();
                foreach($userdatas as $userdata) {

                    $senderTokens[] = $userdata['device_token'];
                    $senderIds[] = $userdata['id'];
                }

                $this->Jobseekeradmin_Model->jobpushUpdate($senderIds);
                $senAndroid = $this->push_notification_android_customer2($senderTokens, urldecode($maindatas[0]['title']), urldecode($maindatas[0]['notification']), 'jobnotify', 0, $maindatas[0]['job_id']);
            }

            $checkMain = $this->Jobseekeradmin_Model->checkMainJobNotify($maindatas[0]['id']);
            if(count($checkMain) > 0) { } else {
                $this->Jobseekeradmin_Model->updateMainJobNotify($maindatas[0]['id']);
            }
            var_dump($maindatas);
        }
    }
    public function jobpush_notify_two() {

        $maindatas = $this->Jobseekeradmin_Model->fetchMainJobNotifyy();        

        if($maindatas) {

            $userdatas = $this->Jobseekeradmin_Model->fetchJobNotify($maindatas[0]['id']);
            if ($userdatas) {

                $senderTokens = array();
                $senderIds = array();
                foreach($userdatas as $userdata) {

                    $senderTokens[] = $userdata['device_token'];
                    $senderIds[] = $userdata['id'];
                }

                $this->Jobseekeradmin_Model->jobpushUpdate($senderIds);
                $senAndroid = $this->push_notification_android_customer2($senderTokens, urldecode($maindatas[0]['title']), urldecode($maindatas[0]['notification']), 'jobnotify', 0, $maindatas[0]['job_id']);
            }

            $checkMain = $this->Jobseekeradmin_Model->checkMainJobNotify($maindatas[0]['id']);
            if(count($checkMain) > 0) { } else {
                $this->Jobseekeradmin_Model->updateMainJobNotify($maindatas[0]['id']);
            }
        }
    }

    public function jobpush_notify_three() {

        $maindatas = $this->Jobseekeradmin_Model->fetchMainJobNotifyy();        

        if($maindatas) {

            $userdatas = $this->Jobseekeradmin_Model->fetchJobNotify_one($maindatas[0]['id']);
            if ($userdatas) {

                $senderTokens = array();
                $senderIds = array();
                foreach($userdatas as $userdata) {

                    $senderTokens[] = $userdata['device_token'];
                    $senderIds[] = $userdata['id'];
                }

                $this->Jobseekeradmin_Model->jobpushUpdate($senderIds);
                $senAndroid = $this->push_notification_android_customer2($senderTokens, urldecode($maindatas[0]['title']), urldecode($maindatas[0]['notification']), 'jobnotify', 0, $maindatas[0]['job_id']);
            }

            $checkMain = $this->Jobseekeradmin_Model->checkMainJobNotify($maindatas[0]['id']);
            if(count($checkMain) > 0) { } else {
                $this->Jobseekeradmin_Model->updateMainJobNotify($maindatas[0]['id']);
            }
        }
    }

    public function jobpush_notify_limited() {
        $userdatas = $this->Jobseekeradmin_Model->fetchJobNotifyLimited();
        if ($userdatas) {

            foreach($userdatas as $userdata) {

                if($userdata['status'] == 1) {

                    $deviceToken = $userdata['device_token'];
            
                    $checkNotifyOn = $this->Jobseekeradmin_Model->user_single($userdata['user_id']);
                    if ($checkNotifyOn) {

                        if ($checkNotifyOn[0]['notification_status'] == 1) {

                            $mainData = $this->Jobseekeradmin_Model->fetchJobNotifyLimitedMainData($userdata['notify_id']);
                            
                            if ($userdata['device_type'] == "ios") {

                                $senAndroid = $this->pushiphon1(array($deviceToken, urldecode($mainData[0]['title']), 'jobnotify', urldecode($mainData[0]['notification']), 0, $mainData[0]['job_id']));
                            
                            } else {

                                $senAndroid = $this->push_notification_android_customer1($deviceToken, urldecode($mainData[0]['title']), urldecode($mainData[0]['notification']), 'jobnotify', 0, $mainData[0]['job_id']);
                            }
                        }
                        $this->Jobseekeradmin_Model->jobpushUpdate($userdata['id']);
                    }
                }
            }
        }
    }


    public function notification_delete() {
        $date = date('Y-m-d H:i:s', strtotime('-3 days'));
        
        $this->Jobseekeradmin_Model->delete_jobnotification($date);
        $this->Jobseekeradmin_Model->delete_jobnotificationrecord($date);
        $this->Jobseekeradmin_Model->delete_jobnotificationpromo($date);
    }

    public function sendInstanceReminder() {
        $screenJobs = $this->Cron_Model->getScreening();
        $date = date('Y-m-d H:i:s');

        if($screenJobs) {

            $this->load->library('email');

            foreach ($screenJobs as $jobs) {

                $formatDate = date('Y-m-d H:i:s', strtotime($jobs['created_at']));

                $date1 = strtotime($formatDate);  
                $date2 = strtotime($date);
                $diff = $date2 - $date1;
                $daysdiff = abs(round($diff / 86400));

                $hourdiff = round(abs($date2 - $date1)/(60*60));
                $minsdiff = round(abs($date2 - $date1)/60);

                $this->email->initialize([
                        'protocol' => 'smtp',
                        'smtp_host' => 'smtpout.asia.secureserver.net',
                        'smtp_user' => 'instantscreening@jobyoda.com',
                        'smtp_pass' => 'Jobyoda@2k21',
                        'smtp_port' => 465,
                        'smtp_crypto' => 'ssl',
                        'charset'=>'utf-8',
                        'mailtype' => 'html',
                        'crlf' => "\r\n",
                        'newline' => "\r\n"
                ]);

                //if($jobs['user_id'] == 261803 || $jobs['user_id'] == 278476 || $jobs['user_id'] == 181971) {

                    if($hourdiff >= 2 || $hourdiff <= 3) {
                    //if($minsdiff == 15) {

                        if($jobs['notify_status'] == 2) { } else {

                            $user = $this->User_Model->user_single($jobs['user_id']);
                            if($user) {
                                $jobdata = $this->Cron_Model->getjobdata($jobs['jobpost_id']);
                                if($jobdata) {
                                    $compdata = $this->Cron_Model->getcompdata($jobdata[0]['company_id']);
                                    if($compdata) {

                                        $sitename = $compdata[0]['site_name'];
                                        $jobURL = $jobdata[0]['modeurl'];
                                        
                                        $mobileNumber = $user[0]['country_code'] . $user[0]['phone'];
                                        
                                        //"Thank you for using JobYoDA to apply at ".$sitename.". We are here to help you find your Dream BPO job! ".$sitename." requires you to complete the pre-assessment in order to move to the next step. Please click here ".$jobURL." to proceed. Please ignore this message if you have completed your application successfully. Wishing good luck from the JobYoDA team!";

                                        $msgText = "Thank you for using JobYoDA to apply at ".$sitename.". We are here to help you find a great job fit with so much less hassle! IMPORTANT: ".$sitename." requires you to complete the pre-assessment in order to move to the next step. In fact, they may not even see your name until you finish it 🙂 . Please click here ".$jobURL." to proceed. Of course please ignore this message if you have already completed your application successfully. The JobYoDA team wishes you the best!";

                                        //$msgText = "Thank you for choosing JobYoDA to apply at ".$sitename;

                                        $subject = "Update on Your ".$sitename." Application";
                                        $data = ["sitename"=>$sitename, "jobURL"=>$jobURL];
                                        $msgEmailText = $this->load->view('recruiter/instantemail', $data, TRUE);
                                        
                                        $this->email->from('help@jobyoda.com', "JobYoDA");
                                        $this->email->to($user[0]['email']);
                                        $this->email->subject($subject);
                                        $this->email->message($msgEmailText);
                                        $this->email->send();

                                        $this->Cron_Model->instantupdate($jobs['id'], 2);

                                        if(strlen($mobileNumber) > 5) {
                                            //$this->SendAwsSms($mobileNumber, $msgText);
                                        }
                                    }
                                }
                            }
                        }
                    
                    } else if($hourdiff >=12  || $hourdiff <= 13) {
                    //} else if($minsdiff == 20) {

                        if($jobs['notify_status'] == 12) { } else {

                            $user = $this->User_Model->user_single($jobs['user_id']);
                            if($user) {
                                $jobdata = $this->Cron_Model->getjobdata($jobs['jobpost_id']);
                                if($jobdata) {
                                    $compdata = $this->Cron_Model->getcompdata($jobdata[0]['company_id']);
                                    if($compdata) {
                                        $sitename = $compdata[0]['site_name'];
                                        $jobURL = $jobdata[0]['modeurl'];

                                        $subject = "Attention! ".$sitename." is looking for you!";
                                        $data = ["sitename"=>$sitename, "jobURL"=>$jobURL];
                                        $msgEmailText = $this->load->view('recruiter/instantemailtwo', $data, TRUE);
                                        
                                        $this->email->from('help@jobyoda.com', "JobYoDA");
                                        $this->email->to($user[0]['email']);
                                        $this->email->subject($subject);
                                        $this->email->message($msgEmailText);
                                        $this->email->send();

                                        $this->Cron_Model->instantupdate($jobs['id'], 12);
                                    }
                                }
                            }
                        }
                    
                    } else if($daysdiff == 2) {
                    //} else if($minsdiff == 30) {

                        if($jobs['notify_status'] == 48) { } else {
                            $user = $this->User_Model->user_single($jobs['user_id']);
                            if($user) {
                                $jobdata = $this->Cron_Model->getjobdata($jobs['jobpost_id']);
                                if($jobdata) {
                                    $compdata = $this->Cron_Model->getcompdata($jobdata[0]['company_id']);
                                    if($compdata) {
                                        $sitename = $compdata[0]['site_name'];
                                        $jobURL = $jobdata[0]['modeurl'];

                                        $subject = "Did you get Hired at ".$sitename." ? Claim your Venti Coffee voucher NOW!";
                                        $data = ["sitename"=>$sitename, "jobURL"=>$jobURL];
                                        $msgEmailText = $this->load->view('recruiter/instantemailthree', $data, TRUE);
                                        
                                        $this->email->from('help@jobyoda.com', "JobYoDA");
                                        $this->email->to($user[0]['email']);
                                        $this->email->subject($subject);
                                        $this->email->message($msgEmailText);
                                        $this->email->send();
                                        
                                        $this->Cron_Model->instantupdate($jobs['id'], 48);
                                    }
                                }
                            }
                        }
                    }
                //}
            }
        }
        echo "success";
        die;
    }

    public function dummysms() {

        echo $this->SendAwsSms('+919911818192', "Hello how are you");
        die;
    }

    public function dummymail() {

        $subject = "Testing jobyoda!";

        $msgEmailText = "This is a test mail";
        $this->load->library('encryption');
        $this->load->library('email');
        $this->email->initialize([
                'protocol' => 'smtp',
                'smtp_host' => 'smtpout.asia.secureserver.net',
                'smtp_user' => 'otp@jobyoda.com',
                'smtp_pass' => 'Jobyoda@2k21',
                'smtp_port' => 465,
                'smtp_crypto' => 'ssl',
                'charset'=>'utf-8',
                'mailtype' => 'html',
                'crlf' => "\r\n",
                'newline' => "\r\n"
        ]);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");
        $this->email->from('help@jobyoda.com', "JobYoDA");
        $this->email->to('ankit.mittal@mobulous.com');
        $this->email->subject($subject);
        $this->email->message($msgEmailText);
        //$this->email->send();
        if (! $this->email->send()) {

            echo $this->email->print_debugger();
            die;
        } else {
            echo "success";
        }
        
        die;
    }

    public function resumepathchange() {
        $resumes = $this->Cron_Model->getUserResume();
        if($resumes) {  
            $xx=1;
            foreach($resumes as $resume) {

                $newpath = str_replace("resumeuploads","files", $resume['resume']);

                $this->Cron_Model->updateResume($resume['user_id'], $newpath);

                $xx++;
            }
        }

        echo $xx;
        die;
    }

    public function custom_push_Send() {

        $getUsers = $this->Jobseekeradmin_Model->fetchUser(54);
        if ($getUsers) {
            foreach ($getUsers as $getUser) {
                $checkNotifyOn = $this->Jobseekeradmin_Model->user_single($getUser['user_id']);
                if ($checkNotifyOn) {
                        
                    $senAndroid = $this->push_notification_android_customer1($getUser['device_token'], urldecode("Test notification"), urldecode("Test notification"), 'promo', 0, $getUser['user_id']);
                }
            }
        }
        echo "success";die;
    }

    public function getallresumes() {

        $resumesArr = array();
        $resumesFolderArr = array();
        $diffFolderArr = array();
        $existsFolderArr = array();

        $imagesDirectory = "dynamic_resume/";
        if(is_dir($imagesDirectory))
        {
            $opendirectory = opendir($imagesDirectory);
            while (($image = readdir($opendirectory)) !== false)
            {
                if(($image == '.') || ($image == '..'))
                {
                    continue;
                }
                
                $imgFileType = pathinfo($image,PATHINFO_EXTENSION);
                
                $resumesFolderArr[] = $image;
            }
            
            closedir($opendirectory);
        }

        foreach($resumesFolderArr as $resumesFolder) {

            $getSelectedresume = $this->Cron_Model->getselectedUserResume($resumesFolder);

            if($getSelectedresume) {}else {
                echo $resumesFolder;
                $diffFolderArr[] = $resumesFolder;
            } 
        }
        die;
    }

    public function create_resume() {
    }

    // http://18.191.12.133/api2/cron/dbexport
    // http://18.191.12.133/api2/cron/dbexportone
    // http://18.191.12.133/api2/cron/dbexporttwo
    // http://18.191.12.133/api2/cron/dbexportthree

    public function dbexport() {

        $path = $_SERVER['DOCUMENT_ROOT'].'/mysql_backup/';
        $files = glob($path.'*'); // get all file names
        
        foreach($files as $file) { // iterate files
          
          if(is_file($file))
            unlink($file); // delete file
            //echo $file.'file deleted';
        }   

        $prefs = array(
        	'tables'=> array('admin', 'admin_forgot', 'advertise_amount', 'advertise_list', 'api_version', 'applied_jobs', 'boost_amount', 'channel_lists', 'cities_listing', 'comapny_rating', 'company_sites', 'contact_us', 'counnotification', 'country', 'exp_with_salary', 'faq', 'hire_cost', 'how_it_works', 'industry_lists', 'invoice', 'jobposting_address', 'jobposting_location', 'jobseeker_forgot', 'jobs_notify', 'jobs_notify_records', 'jobtitlecache', 'job_allowances', 'job_category', 'job_images', 'job_leaves', 'job_levels', 'job_medical',  'job_posting', 'job_screening', 'job_skills', 'job_subcategory', 'job_titles','job_toppicks', 'job_workshift', 'keywordcache', 'languages', 'motivational_quotes', 'nationality', 'news_list', 'notificationmessage', 'notifications', 'owncountrydata', 'profile_completeness'),
            'format' => 'txt',
            'filename' => 'my_db_backup_one.sql'
        );

        $this->load->dbutil();
        $backup = $this->dbutil->backup($prefs);
        $db_name = 'backup-first.sql';
        $save = 'mysql_backup/'.$db_name;
        $this->load->helper('file');
        write_file($save, $backup);

        $serverURL = base_url() . 'mysql_backup/';

        echo $serverURL;
        exit;
    }

    public function dbexportone() {

        $prefones = array(
	        	'tables'=> array('promo_notifications', 'reconciliation','recruiter', 'recruiter_addexp', 'recruiter_allowances', 'recruiter_details', 'recruiter_leaves', 'recruiter_medical', 'recruiter_subscribed', 'recruiter_toppicks', 'recruiter_workshift', 'recruite_testimonials', 'referral_codes', 'region', 'reviews', 'saved_jobs', 'save_filter_keys', 'sign_in_bonus', 'site_images', 'skills', 'special_profile','static_content', 'story_comments', 'story_likes', 'subrecruiter_permission', 'success_stories', 'super_power', 'testimonials', 'top_bpos', 'transfers', 'user_assessment', 'user_contact', 'user_education', 'user_expert','user_forgot','user_language', 'user_more_details','user_resume', 'user_skills'),
	            'format' => 'txt',
	            'filename' => 'my_db_backup_two.sql'
	        );

        $this->load->dbutil();
        $backupone = $this->dbutil->backup($prefones);
        $db_name_one = 'backup-second.sql';
        $save_one = 'mysql_backup/'.$db_name_one;
        $this->load->helper('file');
        write_file($save_one, $backupone);

        $serverURL_one = base_url() . $save_one;

        echo $serverURL_one;
        exit;
    }

    public function dbexporttwo() {

        $preftwos = array(
	        	'tables'=> array('user'),
	            'format' => 'txt',
	            'filename' => 'my_db_backup_three.sql'
	        );

        $this->load->dbutil();
        $backuptwo = $this->dbutil->backup($preftwos);
        $db_name_two = 'backup-third.sql';
        $save_two = 'mysql_backup/'.$db_name_two;
        $this->load->helper('file');
        write_file($save_two, $backuptwo);

        $serverURL_two = base_url() . $save_two;

        echo $serverURL_two;
        exit;
    }

    public function dbexportthree() {

        $prefthrees = array(
                'tables'=> array('user_token', 'user_topclients', 'user_work_experience', 'verification_email', 'videoadvertise_list', 'view_applicant_block'),
                'format' => 'txt',
                'filename' => 'my_db_backup_four.sql'
            );

        $this->load->dbutil();
        $backupthree = $this->dbutil->backup($prefthrees);
        $db_name_three = 'backup-fourth.sql';
        $save_three = 'mysql_backup/'.$db_name_three;
        $this->load->helper('file');
        write_file($save_three, $backupthree);

        $serverURL_three = base_url() . $save_three;

        echo $serverURL_three;
        exit;
    }

    public function checksubscriptionplan() {

        $dataLists = $this->Recruiteradmin_Model->recruiter_lists(); 
        foreach($dataLists as $dataList) {

            $userDataPlan = $this->Subscription_Model->view_existing($dataList['id']);
            if($userDataPlan) {
                
                $to_date = strtotime($userDataPlan[0]['start_date']);
                $from_date = strtotime($userDataPlan[0]['expiry_date']);
                $current_date = date("d-m-Y");
                $current_date = strtotime($current_date);
                if($current_date > $from_date) {
                    $record = ["status"=>2];
                    $this->Subscription_Model->recruiter_subscribe_update($record, $dataList['id']);
                }
            }
        }
    }

    public function checkactivelyhiring() {

        $dataLists = $this->Recruiteradmin_Model->job_lists_actively(); 
        if($dataLists) {
            foreach($dataLists as $dataList) {

                $activelyDate = date("Y-m-d", strtotime($dataList['actively_date']));
                $activelyDateSTR = strtotime($activelyDate);
                $activelyDateWeek = date("Y-m-d", strtotime($dataList['actively_date']. ' + 7 days'));
                //$activelyDateWeek = date("Y-m-d", strtotime($dataList['actively_date']));
                $activelyDateWeekSTR = strtotime($activelyDateWeek);
                $currentDate = date("Y-m-d");

                if($activelyDateSTR == $activelyDateWeekSTR) {

                    $this->Recruiteradmin_Model->job_lists_actively_update($dataList['id']);
                }
            }
        }
    }

    public function SendAwsSms($to, $sms_message) {
        require 'vendor/autoload.php';
        //echo 'hi';die;
        $params = array('credentials' => array('key' => 'AKIA2PEGBF6OC7RXCBWM', 'secret' => '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',), 'region' => 'us-west-2', 'version' => 'latest');
        $sns = new \Aws\Sns\SnsClient($params);
        $args = array("SenderID" => "JobYoDA", "SMSType" => "Transactional", "Message" => $sms_message, "PhoneNumber" => $to,);
        //print_r($args);die;
        $result = $sns->publish($args);
        return true;
        //print_r($result);
    }

    function pushiphon1($array) {
        $access_key = API_ACCESS_KEY;
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $notification = ['sound' => 'default', 'apns-push-type' => 'alert', 'title' => $array[3], 'body' => $array[1], 'type' => $array[2], 'recruiter_id' => $array[5], 'story_id' => '13', 'companyname' => '21', 'notification_id' => $array[5], 'timestamp' => date('H:i:s'), 'suggestid' => '2', 'badgecount' => $array[4], 'promo_id' => $array[5]];

        $fcmNotification = [
        'to' => $array[0],
        'notification' => $notification,
        'data' => $notification];

        $headers1 = ['Authorization: key=' . $access_key, 'Content-Type: application/json'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    function push_notification_android_customer1($regid, $title, $body, $type, $badgecount, $promoId) {
        //  echo $regid;die;
        $access_key = API_ACCESS_KEY;
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        
        $notification = ['sound' => 'default', 'apns-push-type' => 'alert', 'title' => $title, 'body' => $body, 'type' => $type, 'recruiter_id' => $promoId, 'story_id' => '13', 'companyname' => '21', 'notification_id' => '52', 'timestamp' => date('H:i:s'), 'suggestid' => '2', 'badgecount' => $badgecount, 'promo_id' => $promoId];

        $notification1 = ['title' => $title, 'body' => $body, 'type' => $type, 'recruiter_id' => $promoId, 'story_id' => '13', 'companyname' => '21', 'notification_id' => '52', 'timestamp' => date('H:i:s'), 'suggestid' => '2', 'badgecount' => $badgecount, 'promo_id' => $promoId];

        $fcmNotification = [
        'to' => $regid,
        'data' => $notification1];
        $headers1 = ['Authorization: key=' . $access_key, 'Content-Type: application/json'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);
        
        return $result;
    }

    public function jobpush_notify_testing() {


        $senderTokens[] = "eOiIp737Bpo:APA91bEBjsC39_65JAZzCBuHhuM_qQZ_jfdnQN11FuwmQNraszFglca_vvfQkjmeHGvjvPKxr1FVe_7ijPUnqPRNBds3sIOxmME9oY33y2xTZ0wFOIZZ2-7_Dyr8K-A_5EAgvQkNGcEC";

        $senAndroid = $this->push_notification_android_customer2($senderTokens, urldecode("Test notification get"), urldecode("check test notification"), 'jobnotify', 0, "1197");

    }

    function push_notification_android_customer2($regid, $title, $body, $type, $badgecount, $promoId) {
        
        $access_key = API_ACCESS_KEY;
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        
        $notification = ['sound' => 'default', 'apns-push-type' => 'alert', 'title' => $title, 'body' => $body, 'type' => $type, 'recruiter_id' => $promoId, 'story_id' => '13', 'companyname' => '21', 'notification_id' => '52', 'timestamp' => date('H:i:s'), 'suggestid' => '2', 'badgecount' => $badgecount, 'promo_id' => $promoId];

        $notification1 = ['title' => $title, 'body' => $body, 'type' => $type, 'recruiter_id' => $promoId, 'story_id' => '13', 'companyname' => '21', 'notification_id' => '52', 'timestamp' => date('H:i:s'), 'suggestid' => '2', 'badgecount' => $badgecount, 'promo_id' => $promoId];

        $fcmNotification = [
        'registration_ids' => $regid,
        'data' => $notification1];
        $headers1 = ['Authorization: key=' . $access_key, 'Content-Type: application/json'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    function push_notification_android_customer3($regid, $title, $body, $type, $badgecount, $promoId) {
        $access_key = API_ACCESS_KEY;
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        
        $notification = ['sound' => 'default', 'apns-push-type' => 'alert', 'title' => $title, 'body' => $body, 'type' => $type, 'recruiter_id' => $promoId, 'story_id' => '13', 'companyname' => '21', 'notification_id' => '52', 'timestamp' => date('H:i:s'), 'suggestid' => '2', 'badgecount' => $badgecount, 'promo_id' => $promoId];

        $notification1 = ['title' => $title, 'body' => $body, 'type' => $type, 'recruiter_id' => $promoId, 'story_id' => '13', 'companyname' => '21', 'notification_id' => '52', 'timestamp' => date('H:i:s'), 'suggestid' => '2', 'badgecount' => $badgecount, 'promo_id' => $promoId];
        $fcmNotification = [
        'registration_ids' => $regid,
        'data' => $notification1];
        $headers1 = ['Authorization: key=' . $access_key, 'Content-Type: application/json'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    function push_notification_common($regid, $title, $body, $type, $badgecount, $promoId) {
        $access_key = API_ACCESS_KEY;
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $notification = ['title' => $title, 'body' => $body, 'type' => $type, 'recruiter_id' => '12', 'story_id' => '13', 'companyname' => '21', 'notification_id' => '52', 'timestamp' => date('H:i:s'), 'suggestid' => '2', 'badgecount' => $badgecount, 'promo_id' => $promoId];

        $fcmNotification = [
        'registration_ids' => $regid,
        'notification' => $notification,
        'data' => $notification];

        $headers1 = ['Authorization: key=' . $access_key, 'Content-Type: application/json'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    public function sendSms($to, $sms_message) {
        $this->load->library('twilio');
        $from = 'JobYoDA';
        $to = $to;
        $response = $this->twilio->sms($from, $to, $sms_message);
        if ($response->ResponseXml->Message->Status[0] == "failed") {
            return '2';
        } else {
            return '1';
        }
    }

    public function testmessage() {
        $this->load->library('twilio');
        $from = 'JobYoDA';
        $to = '+918218524174';
        $sms_message = 'test message';
        $callurl = 'https://jobyoda.com/api/cron/getstatus';
        $response = $this->twilio->sms($from, $to, $sms_message, $callurl);
        echo "<pre>";
        print_r($response);
        echo "</pre>";
        die;
        if ($response->ResponseXml->Message->Status[0] == "failed") {
            return '2';
        } else {
            return '1';
        }
    }
    
    public function getstatus() {
        $data = ['sms' => 'hii'];
        $this->Cron_Model->test_sms($data);
    }

    function push_notification_android_customer($regid, $title, $body, $type, $recruiter_id, $companyname, $badgecount) {
        date_default_timezone_set('Asia/Manila');
        $access_key = API_ACCESS_KEY;
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $notification = ['title' => $title, 'body' => $body, 'type' => $type, 'recruiter_id' => $recruiter_id, 'story_id' => '13', 'companyname' => $companyname, 'notification_id' => '1', 'timestamp' => date('H:i:s'), 'suggestid' => '2', 'badgecount' => $badgecount, 'promo_id' => '002'];
        $extraNotificationData = $notification;
        $fcmNotification = [
        'to' => $regid,
        'data' => $extraNotificationData];
        $headers1 = ['Authorization: key=' . $access_key, 'Content-Type: application/json'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    function pushiphon($array) {
        $deviceToken = $array[0];
        $passphrase = '1234';
        $this->autoRender = false;
        $this->layout = false;
        //$basePath = WWW_ROOT."QualititionSolution.pem";
        $basePath = "JobYoda.pem";
        //echo file_exists($basePath);  die;
        if (file_exists($basePath)) {
            //echo $basePath." exists";die;
            $ctx = stream_context_create();
            stream_context_set_option($ctx, 'ssl', 'local_cert', $basePath);
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
            $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
            if (!$fp) exit("Failed to connect: $err $errstr" . PHP_EOL);
            $body['aps'] = array('alert' => array('title' => $array[3], "body" => $array[1]), 'sound' => 'default', 'badge' => $array[5]);
            $body['body'] = $array[1];
            $body['type'] = $array[2];
            $body['recruiter_id'] = $array[4];
            $body['companyname'] = $array[6];
            $body['notification_id'] = '1';
            $payload = json_encode($body);
            $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
            $result = fwrite($fp, $msg, strlen($msg));
            
            if (!$result) {
                echo "err";
            } else {
                
                echo "<pre>";
                print_r($body['aps']);
            }
            fclose($fp);
        }
    }
}
