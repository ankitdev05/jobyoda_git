<?php
ob_start();
ini_set('display_errors', 1);
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */

class Jobpost extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('newmodels/User_Model');
        $this->load->model('newmodels/recruiter/Jobpost_Model');
        $this->load->model('newmodels/recruiter/Candidate_Model');
        $this->load->model('newmodels/recruiter/Recruit_Model');
        $this->load->model('newmodels/recruiter/Newpoints_Model');
        $this->load->model('newmodels/Common_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');
        $key = $this->encryption->create_key(10);
    }
    // http://localhost/jobyodha/api/example/users/id/1
    

    public function jobLocateHome_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1){

            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck) {
                
                $expmonth = $userTokenCheck[0]['exp_month'];
                $expyear = $userTokenCheck[0]['exp_year'];
                if($expyear == 0 && $expmonth == 0) {
                    $expfilter = "1";
                    
                }else if($expyear == 0 && $expmonth < 6) {
                    $expfilter = "2";
                    
                } else if($expyear < 1 && $expmonth >= 6) {
                    $expfilter = "3";
                    
                } else if($expyear < 2 && $expyear >= 1) {
                    $expfilter = "4";
                    
                } else if($expyear < 3 && $expyear >= 2) {
                    $expfilter = "5";
                    
                }else if($expyear < 4 && $expyear >= 3) {
                    $expfilter = "6";
                    
                }else if($expyear < 5 && $expyear >= 4) {
                    $expfilter = "7";
                    
                }else if($expyear < 6 && $expyear >= 5) {
                    $expfilter = "8";
                    
                }else if($expyear < 7 && $expyear >= 6) {
                    $expfilter = "9";
                    
                } else if($expyear >= 7) { 
                    $expfilter = "10";
                    
                }
                $jobLocates = $this->Jobpost_Model->group_locateHome($userTokenCheck[0]['id'],$expfilter);    
                $returnnewarray = array();
                $latlogcheck =array();
                foreach ($jobLocates as $value123) {
                        if(in_array($value123['latitude'],$latlogcheck)){
                            $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['totaljobs'] = $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['totaljobs'] + 1;
                        } else {
                            $latlogcheck[] = $value123['latitude'];
                            $returnnewarray[] = $value123;
                        }
                }    
                $x=0;
                foreach ($returnnewarray as $jobLocate) {
                    
                    if(strlen($jobLocate['allowance']) <= 0) {
                        $salary_f = $jobLocate['salary'];
                    } else {
                        $salary_f = $jobLocate['salary'] + $jobLocate['allowance'];
                    }
                    $returnnewarray[$x]= ["recruiter_id"=> $jobLocate['recruiter_id'], "latitude"=> $jobLocate['latitude'], "longitude"=>$jobLocate['longitude'], "totaljobs"=>(string)$jobLocate['totaljobs'], "salary"=> (int)$jobLocate['salary']];
                    
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobLocate['id']);
                    if($jobTop) {
                        if(isset($jobTop[0]['picks_id'])){
                            $one = (string)$jobTop[0]['picks_id'];
                            $returnnewarray[$x]['toppicks1'] = "$one";
                        }
                        if(isset($jobTop[1]['picks_id'])){
                            $two = (string)$jobTop[1]['picks_id'];
                            $returnnewarray[$x]['toppicks2'] =  "$two";
                        }
                        if(isset($jobTop[2]['picks_id'])){
                            $three = (string)$jobTop[2]['picks_id'];
                            $returnnewarray[$x]['toppicks3'] = "$three";
                        }
                    } else {
                        $returnnewarray[$x]['toppicks1'] = "";
                        $returnnewarray[$x]['toppicks2'] = "";
                        $returnnewarray[$x]['toppicks3'] = "";
                    }
                    $x++;
                }
                $getCompletenes = $this->fetchUserCompleteData($userTokenCheck[0]['id']);  
                if($userTokenCheck[0]['exp_month']>=0   && $userTokenCheck[0]['exp_year']>=0){
                    $exp_added = "No";
                }  else{
                    $exp_added = 'Yes';
                }
                $completeness = $getCompletenes['comp'];
                if($completeness<=14 || $exp_added== 'Yes'){
                    $completeness = "Yes";
                }else{
                    $completeness = "No";
                }
                $fetchnotifications = $this->Candidate_Model->fetchnotifications($userTokenCheck[0]['id']);
                $fetchpromonotifications = $this->Candidate_Model->fetchpromonotifications($userTokenCheck[0]['id']);
                
                $response = ['jobLocateHome' => $returnnewarray,'completeness'=>$completeness, 'notification_count' => count($fetchnotifications)+count($fetchpromonotifications), 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function jobListing_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck) {

                $expmonth = $userTokenCheck[0]['exp_month'];
                $expyear = $userTokenCheck[0]['exp_year'];
                if($expyear == 0 && $expmonth == 0) {
                    $expfilter = "1";
                    
                }else if($expyear == 0 && $expmonth < 6) {
                    $expfilter = "2";
                    
                } else if($expyear < 1 && $expmonth >= 6) {
                    $expfilter = "3";
                    
                } else if($expyear < 2 && $expyear >= 1) {
                    $expfilter = "4";
                    
                } else if($expyear < 3 && $expyear >= 2) {
                    $expfilter = "5";
                    
                }else if($expyear < 4 && $expyear >= 3) {
                    $expfilter = "6";
                    
                }else if($expyear < 5 && $expyear >= 4) {
                    $expfilter = "7";
                    
                }else if($expyear < 6 && $expyear >= 5) {
                    $expfilter = "8";
                    
                }else if($expyear < 7 && $expyear >= 6) {
                    $expfilter = "9";
                    
                } else if($expyear >= 7) { 
                    $expfilter = "10";
                    
                }

                $userLat = $userData['lat'];
                $userLong = $userData['long'];
                $jobpost_id = $userData['jobpost_id'];

                $jobfetchs = $this->Jobpost_Model->job_fetch_latlong($userLat, $userLong, $jobpost_id,$expfilter); 
                if($jobfetchs) {
                    $x=0;
                    foreach ($jobfetchs as $jobfetch) {
                        $jobApplyfetch = $this->Jobpost_Model->job_fetch_appliedbycompid($jobfetch['id'], $userTokenCheck[0]['id']);

                        $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                        if($jobTop) {

                        } else{
                            $jobTop = [];
                        }
                        if($jobApplyfetch) {

                        } else {
                            $jobsal = $this->Jobpost_Model->getExpJob1($jobfetch['id'],$expfilter);
                        
                            $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);

                            if($jobTop) {
                                if(isset($jobTop[0]['picks_id'])){
                                    $datatoppicks1 = $jobTop[0]['picks_id'];    
                                } else{
                                    $datatoppicks1 = "";
                                }
                                if(isset($jobTop[1]['picks_id'])){
                                    $datatoppicks2 =  $jobTop[1]['picks_id'];    
                                } else{
                                    $datatoppicks2 = "";
                                }
                                if(isset($jobTop[2]['picks_id'])){
                                    $datatoppicks3 = $jobTop[2]['picks_id'];    
                                } else{
                                    $datatoppicks3 = "";
                                }
                            } else {
                                $datatoppicks1 = "";
                                $datatoppicks2 = "";
                                $datatoppicks3 = "";
                            }

                            $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['recruiter_id']);
                            $image = $this->Jobpost_Model->job_image($jobfetch['id']);
                            $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['recruiter_id']);
                            $savedjob = $this->Jobpost_Model->job_saved($jobfetch['id'], $userTokenCheck[0]['id']);
                            if(!empty($companyPic[0]['companyPic'])) {
                                $comPic = trim($companyPic[0]['companyPic']);
                            } else{
                                $comPic = "";
                            }
                            
                            if(!empty($image[0]['pic'])) {
                                $jobpic = $image[0]['pic'];
                            } else{
                                $jobpic = $comPic;
                            }
                            
                            if(!empty($savedjob[0]['id'])) {
                                $savedjob = '1';
                            } else{
                                $savedjob = "0";
                            }
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            if($jobfetch['chatbot'] == 1) {
                                $jobfetch['mode'] = "chatbot";
                            }

                            $distance = $this->distance($userData['cur_lat'], $userData['cur_long'], $jobfetch['latitude'], $jobfetch['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', ''); 
                            $data["jobList"][] = [
                                    "jobpost_id" => $jobfetch['id'],
                                    "job_title" => $jobfetch['jobtitle'],
                                    "jobDesc" => $jobfetch['jobDesc'],
                                    "salary" => number_format($jobfetch['salary']),
                                    "jobexpire" => $jobfetch['jobexpire'],
                                    "companyName" => $jobfetch['cname'],
                                    "mode" => $jobfetch['mode'],
                                    "modeurl" => $jobfetch['modeurl'],
                                    "cname" => $cname,
                                    "companyId" => $jobfetch['compId'],
                                    "toppicks1" => "$datatoppicks1",
                                    "toppicks2" => "$datatoppicks2",
                                    "toppicks3" => "$datatoppicks3",
                                    "companyPic" => $comPic,
                                    "jobpic" => $jobpic,
                                    "savedjob" => $savedjob,
                                    "distance" => $distance 
                                ];
                        }
                        $x++;
                    }
                    
                    $response = ['jobListing' => $data, 'status' => "SUCCESS"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                } else{
                    $errors = "No data found";
                    $response = ['message' => $errors, 'status' => "FAILURE"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
            }
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function jobdetail_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck) {
                $expmonth = $userTokenCheck[0]['exp_month'];
                $expyear = $userTokenCheck[0]['exp_year'];
                
                if($expyear == 0 && $expmonth == 0) {
                    $expfilter = "1";    
                }else if($expyear == 0 && $expmonth < 6) {
                    $expfilter = "2";
                } else if($expyear < 1 && $expmonth >= 6) {
                    $expfilter = "3";
                } else if($expyear < 2 && $expyear >= 1) {
                    $expfilter = "4";
                } else if($expyear < 3 && $expyear >= 2) {
                    $expfilter = "5";
                }else if($expyear < 4 && $expyear >= 3) {
                    $expfilter = "6";
                }else if($expyear < 5 && $expyear >= 4) {
                    $expfilter = "7";
                }else if($expyear < 6 && $expyear >= 5) {
                    $expfilter = "8";
                }else if($expyear < 7 && $expyear >= 6) {
                    $expfilter = "9";
                } else if($expyear >= 7) { 
                    $expfilter = "10";
                }
                $jobpost_id = $userData['jobpost_id'];
                $jobDetails = $this->Jobpost_Model->job_detail_fetch($jobpost_id);
                $jobLocation = $this->Jobpost_Model->job_detailLocation_fetch($jobDetails[0]['company_id']);
                $jobSaved = $this->Jobpost_Model->applied_status_fetch($jobpost_id, $userTokenCheck[0]['id']);
                if($jobSaved) {
                    $savestatus = 1;
                } else{
                    $savestatus = 0;
                }

                $appliedStatus = $this->Jobpost_Model->applied_status_fetch($jobDetails[0]['id'], $userTokenCheck[0]['id']);
                if($appliedStatus) {
                    $jobstatus = $appliedStatus[0]['status'];
                    $jobapplieddate = date("Y-m-d",strtotime($appliedStatus[0]['interviewdate']));
                    $jobappliedtiming = date("H:i",strtotime($appliedStatus[0]['interviewtime']));
                    $jobappliedtime = $jobapplieddate.",".$jobappliedtiming;
                    $date_day1 = date("Y-m-d",strtotime($appliedStatus[0]['date_day1']));
                    //$jobappliedtime = $appliedStatus[0]['created_at'];
                } else{
                    $jobstatus = '0';
                    $jobappliedtime = '0';
                }

                for($itlku=$expfilter; $itlku >-1 ; $itlku--) { 
                    $jobsal = $this->Jobpost_Model->getExpJob1($jobDetails[0]['id'],$itlku);
                    if(!empty($jobsal)){
                        break;
                    }
                }
                if($jobsal[0]['basicsalary']) {
                    $basicsalary = $jobsal[0]['basicsalary'];
                } else{
                    $basicsalary = '0';
                }

                if($jobsal[0]['grade_id']) {
                    $grade = $jobsal[0]['grade_id'];
                    if($grade==0){
                        $basicexp="All Tenure";
                    }if($grade==1){
                        $basicexp="Minimum Experience";
                    }if($grade==2){
                        $basicexp="less than 6 months";
                    }if($grade==3){
                        $basicexp="6mo to 1 yr";
                    }if($grade==4){
                        $basicexp="1 yr to 2 yr";
                    }if($grade==5){
                        $basicexp="2 yr to 3 yr";
                    }if($grade==6){
                        $basicexp="3 yr to 4 yr";
                    }if($grade==7){
                        $basicexp="4 yr to 5 yr";
                    }if($grade==8){
                        $basicexp="5 yr to 6 yr";
                    }if($grade==9){
                        $basicexp="6 yr to 7 yr";
                    }if($grade==10){
                        $basicexp="7 yr & up";
                    }
                } else{
                    $basicexp = " ";
                }
                $jobImg = array();
                $jobTop = $this->Jobpost_Model->job_toppicks1($jobDetails[0]['id']);
                $jobAllowance = $this->Jobpost_Model->job_allowances($jobDetails[0]['id']);
                $jobMedical = $this->Jobpost_Model->job_medical($jobDetails[0]['id']);
                $jobShift = $this->Jobpost_Model->job_workshift($jobDetails[0]['id']);
                $jobLeaves = $this->Jobpost_Model->job_leaves($jobDetails[0]['id']);
                
                $savedjob = $this->Jobpost_Model->job_saved($jobpost_id, $userTokenCheck[0]['id']);
                if(!empty($savedjob[0]['id'])) {
                    $savedjob = '1';
                } else{
                    $savedjob = "0";
                }

                $jobarrImg = $this->Jobpost_Model->job_images_single($jobpost_id);
                if(count($jobarrImg) > 0) {
                    foreach($jobarrImg as $jobImgg) {
                        $jobImg[] = ['pic'=>$jobImgg['pic']];
                    }
                    
                    $companyPic = $this->Jobpost_Model->fetch_companyPic1($jobDetails[0]['company_id']);
                    if(!empty($companyPic)) {
                        $comPic = $companyPic;

                        foreach($comPic as $compImgg) {
                            $jobImg[] = ['pic'=>$compImgg['pic']];
                        }    
                    }

                } else {

                    $companyPic = $this->Jobpost_Model->fetch_companyPic1($jobDetails[0]['company_id']);
                    if(!empty($companyPic)) {
                        $comPic = $companyPic;

                        foreach($comPic as $compImgg) {
                            $jobImg[] = ['pic'=>$compImgg['pic']];
                        }    
                    } else {
                        $jobImg = [['pic'=>'']];
                    }
                }
                if($jobTop) {
                    $toppickss = $jobTop;
                } else {
                    $toppickss = [];
                }
                if($jobAllowance) {
                    $allowancess = $jobAllowance;
                } else {
                    $allowancess = [];
                }
                if($jobMedical) {
                    $medicals = $jobMedical;
                } else {
                    $medicals = [];
                }if($jobShift) {
                    $shifts = $jobShift;
                } else {
                    $shifts = [];
                }if($jobLeaves) {
                    $leavess = $jobLeaves;
                } else {
                    $leavess = [];
                }
                if(!empty($jobDetails[0]['jobPitch'])){
                    $jobPitch = $jobDetails[0]['jobPitch'];
                }else{
                    $jobPitch = '';
                }
                $jobskills = $this->Jobpost_Model->job_skills($jobDetails[0]['id']);
                if(empty($jobskills)){
                    $jobskills[]= ['skill'=>$jobDetails[0]['skills']];
                }
                
                if(!empty($jobDetails[0]['allowance'])){
                    $jobDetails[0]['allowance'] = $jobDetails[0]['allowance'];
                }else{
                    $jobDetails[0]['allowance'] = '';
                }
                if(!empty($jobDetails[0]['joining_bonus'])){
                    $jobDetails[0]['joining_bonus'] = $jobDetails[0]['joining_bonus'];
                }else{
                    $jobDetails[0]['joining_bonus'] = '';
                }
                if($jobDetails[0]['allowance']>0 && $basicsalary>0){
                    $total_salary = $basicsalary+$jobDetails[0]['allowance'];
                }else{
                    $total_salary = $basicsalary;
                }

                if($jobDetails[0]['subrecruiter_id']) {
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobDetails[0]['subrecruiter_id']);
                }else{
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobDetails[0]['recruiter_id']);
                }

                if(!empty($date_day1) && $jobstatus=='6'){
                    $date_day1 = $date_day1;
                }else{
                    $date_day1 = '';
                }
                if($jobDetails[0]['chatbot'] == 1) {
                    $jobDetails[0]['mode'] = "chatbot";
                }
                
                $distance = $this->distance($userData['cur_lat'], $userData['cur_long'], $jobLocation[0]['latitude'], $jobLocation[0]['longitude'], "K");
                $distance = number_format((float)$distance, 2, '.', '');

                $data["jobdetail"] = [
                            "jobpost_id" => $jobDetails[0]['id'],
                            "job_title" => $jobDetails[0]['jobtitle'],
                            "location" => $jobLocation[0]['address'],
                            "opening" => $jobDetails[0]['opening'],
                            "experience" => $basicexp,
                            "jobDesc" => $jobDetails[0]['jobDesc'],
                            "allowance" => $jobDetails[0]['allowance'],
                            "joining_bonus" => $jobDetails[0]['joining_bonus'],
                            "salary" => number_format($basicsalary),
                            "total_salary" => (string)$total_salary,
                            "jobexpire" => $jobDetails[0]['jobexpire'],
                            "skills" => $jobskills,
                            "qualification" => $jobDetails[0]['education'],
                            "jobSaved" => $savestatus,
                            "status" => $jobstatus,
                            "topPicks" => $toppickss,
                            "allowances" => $allowancess,
                            "medical" => $medicals,
                            "workshift" => array_merge($shifts,$leavess),
                            //"leaves" => $leavess,
                            "images" => $jobImg,
                            "savedjob" => $savedjob,
                            "jobPitch" => $jobPitch,
                            'jobappliedtime' => $jobappliedtime,
                            'distance' => $distance,
                            'date_day1' => $date_day1,
                            "mode" => $jobDetails[0]['mode'],
                            "modeurl" => $jobDetails[0]['modeurl'],
                            "sharecount"=>$this->getsharecount($jobDetails[0]['id'])
                        ];

                $companydetail = $this->Jobpost_Model->company_detail_fetch($jobDetails[0]['company_id']);
                $companyname = $this->Jobpost_Model->company_name_fetch($jobDetails[0]['recruiter_id']);
                if($jobTop) {
                } else{
                    $jobTop = [];
                }

                $companyPic = $this->Jobpost_Model->fetch_companyPic($companydetail[0]["id"]);            
                if(!empty($companyPic[0]['companyPic'])) {
                    $comPic = trim($companyPic[0]['companyPic']);
                } else{
                    $comPic = "";
                }
                if($companydetail1[0]["comm_channel"]){
                    $comm_channel = explode(',', $companydetail1[0]["comm_channel"]);
                }else{
                    $comm_channel = [];
                }
                if(!empty($companyname[0]["cname"])){
                    $cname = $companyname[0]["cname"];
                }else{
                    $cname = '';
                }

                if(!empty($companydetail1[0]["landline"])){
                    $landline = $companydetail1[0]["landline"];
                }else{
                    $landline = '';
                }
                $data["comapnyDetail"] = [
                                        "name"=> $companydetail[0]["cname"],
                                        "cname"=> $cname,
                                        "phonecode" => $companydetail1[0]["phonecode"],
                                        "phone" => $companydetail1[0]["phone"],
                                        "landline" => $landline,
                                        "comm_channel" => $comm_channel,
                                        "email" => $companydetail1[0]["email"],
                                        "company_id" => $companydetail[0]["id"],
                                        "companyPic" => $comPic
                                        ];

                $response = ['jobdetail' => $data, 'status' => "SUCCESS"];

                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function applyJob_post() {
        $job = $this->input->post();
        $date = date('Y-m-d H:i:s');
        $data = ["token" => $job['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck) {
                
                $job_detail = $this->Jobpost_Model->job_detail_fetch($job['jobpost_id']);
                $companyname = $this->Candidate_Model->fetchcomp($job_detail[0]['recruiter_id']);
                $companydetail = $this->Jobpost_Model->job_detailLocation_fetch($job_detail[0]['company_id']);
                if(!empty($companydetail[0]['site_name'])) {
                    $sitenamee = $companydetail[0]['site_name'];
                } else {
                    $companydetail = $this->Candidate_Model->fetchcomp($job_detail[0]['company_id']);
                    $sitenamee = $companydetail[0]['cname'];
                }

                $this->form_validation->set_rules('jobpost_id', 'job post id', 'trim|required');
                $this->form_validation->set_rules('interviewdate', 'Interview Date', 'trim');
                $this->form_validation->set_rules('interviewtime', 'Interview Time', 'trim');

                if ($this->form_validation->run() == FALSE) {
                    $errors = $this->form_validation->error_array();
                    $error = current($errors);
                    $response = ['message' => $error, 'status' => "FAILURE"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);

                } else {

                    $jobexpire = date("Y-m-d", strtotime($job_detail[0]['jobexpire']));
                    $schedule = date("Y-m-d");
                    $job['interviewdate'] = date("Y-m-d");
                    
                    $jobexpire1 = strtotime($jobexpire);
                    $schedule1 = strtotime($schedule);
                    $currentDate = strtotime(date('Y-m-d'));
                
                    if($job_detail[0]['education_status']=='1') {
                        
                        $user_education_match = $this->User_Model->user_single_educationbyuserid2($userTokenCheck[0]['id'], $job_detail[0]['education']);
                        if($user_education_match) {
                            $userEdu = $user_education_match[0]['education'];

                            if($job_detail[0]['education'] == "No Requirement") {
                                $education_match = "True";
                            
                            } else if($job_detail[0]['education'] == "Vocational") {

                                if($job_detail[0]['education'] == $userEdu) {
                                    $education_match = "True";

                                } else if($userEdu == "High School Graduate" || $userEdu == "Undergraduate" || $userEdu == "Associate Degree" || $userEdu == "College Graduate" || $userEdu == "Post Graduate") {

                                    $education_match = "True";
                                } else {
                                    $education_match = "False";                                
                                }
                            
                            } else if($job_detail[0]['education'] == "High School Graduate") {

                                if($job_detail[0]['education'] == $userEdu) {
                                    $education_match = "True";

                                } else if($userEdu == "Undergraduate" || $userEdu == "Associate Degree" || $userEdu == "College Graduate" || $userEdu == "Post Graduate") {

                                    $education_match = "True";
                                } else {
                                    $education_match = "False";                                
                                }
                            
                            } else if($job_detail[0]['education'] == "Undergraduate") {

                                if($job_detail[0]['education'] == $userEdu) {
                                    $education_match = "True";

                                } else if($userEdu == "Associate Degree" || $userEdu == "College Graduate" || $userEdu == "Post Graduate") {
                                    $education_match = "True";
                                } else {
                                    $education_match = "False";                                
                                }
                            
                            } else if($job_detail[0]['education'] == "Associate Degree") {

                                if($job_detail[0]['education'] == $userEdu) {
                                    $education_match = "True";

                                } else if($userEdu == "College Graduate" || $userEdu == "Post Graduate") {
                                    $education_match = "True";
                                } else {
                                    $education_match = "False";                                
                                }
                            
                            } else if($job_detail[0]['education'] == "College Graduate") {

                                if($job_detail[0]['education'] == $userEdu) {
                                    $education_match = "True";

                                } else if($userEdu == "Post Graduate") {
                                    $education_match = "True";
                                } else {
                                    $education_match = "False";                                
                                }
                            } else if($job_detail[0]['education'] == "College Graduate") {

                                if($job_detail[0]['education'] == $userEdu) {
                                    $education_match = "True";
                                } else {
                                    $education_match = "False";                                
                                }
                            } else {
                                $education_match = "False";
                            }

                        } else {
                            $education_match = "False";
                        }

                    }else{
                        $education_match = "True";
                    }

                    if($job_detail[0]['experience_status']=='1') {
                        
                        $user_experience_match = $this->User_Model->user_single_exp($userTokenCheck[0]['id'],$job_detail[0]['experience']);
                        if($user_experience_match){
                            $experience_match = "True";
                        } else{
                            $experience_match = "False";
                        }

                    }else{
                        $experience_match = "True";
                    }

                    if($jobexpire1 >= $currentDate) {
                        
                        if($currentDate >= $currentDate) {
                            
                            if($job_detail[0]['mode'] == "Walk-in") {
                                
                                if($job_detail[0]['chatbot'] == 0) {
                                    
                                    $messageee = "<span style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>All the best from the JobYoDA Team!</span> <span style='text-align:center;color:#fbaf31;line-height:28px;margin-bottom:0px;font-weight:500;'>Did you know JobYoda offers a FREE Venti Coffee to all hires?</span> <span style='text-align:center;color:#fbaf31;line-height:28px;margin-bottom:0px;font-weight:500;'>To claim, all you need to do is:</span> <span style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>1. Declare JobYoda as the source of your application</span> <span style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>2. Send us and email at Help@jobyoda.com to claim your voucher if you get hired</span> <span style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>3. We will validate with the recruiter and process</span> <span style='text-align:center;color:#00a94f;;line-height:28px;margin-bottom:0px;font-weight:500;'>Good Luck!</span>";
                                } else {

                                    $messageee = "Thank You for applying through JobYoDA! Want a FREE Venti Coffee from us? Make sure to declare JobYoDA as your source of Application and send us an email with details of the company and site where you got hired at help@jobyoda.com. Please keep your lines open, the Recruiter will call you if you meet the job requirements! Good Luck!";
                                }

                            } else if($job_detail[0]['mode'] == "Call") {
                                
                                if($job_detail[0]['chatbot'] == 0) {
                                    
                                    $messageee = "<span style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>All the best from the JobYoDA Team!</span> <span style='text-align:center;color:#fbaf31;line-height:28px;margin-bottom:0px;font-weight:500;'>Did you know JobYoda offers a FREE Venti Coffee to all hires?</span> <span style='text-align:center;color:#fbaf31;line-height:28px;margin-bottom:0px;font-weight:500;'>To claim, all you need to do is:</span> <span style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>1. Declare JobYoda as the source of your application</span> <span style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>2. Send us and email at Help@jobyoda.com to claim your voucher if you get hired</span> <span style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>3. We will validate with the recruiter and process</span> <span style='text-align:center;color:#00a94f;;line-height:28px;margin-bottom:0px;font-weight:500;'>Good Luck!</span>";
                                } else {

                                    $messageee = "Thank You for applying through JobYoDA! Want a FREE Venti Coffee from us? Make sure to declare JobYoDA as your source of Application and send us an email with details of the company and site where you got hired at help@jobyoda.com. Please keep your lines open, the Recruiter will call you if you meet the job requirements! Good Luck!";
                                }
                            
                            } else if($job_detail[0]['mode'] == "Instant screening") {

                                $messageee = "<span style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>All the best from the JobYoDA Team!</span> <span style='text-align:center;color:#fbaf31;line-height:28px;margin-bottom:0px;font-weight:500;'>Did you know JobYoda offers a FREE Venti Coffee to all hires?</span> <span style='text-align:center;color:#fbaf31;line-height:28px;margin-bottom:0px;font-weight:500;'>To claim, all you need to do is:</span> <span style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>1. Declare JobYoda as the source of your application</span> <span style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>2. Send us and email at Help@jobyoda.com to claim your voucher if you get hired</span> <span style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>3. We will validate with the recruiter and process</span> <span style='text-align:center;color:#00a94f;;line-height:28px;margin-bottom:0px;font-weight:500;'>Good Luck!</span>";
                            }

                            if($this->input->post('reschedule')) {
                                $jobapply = [
                                "jobpost_id" => $job['jobpost_id'],
                                "user_id" => $userTokenCheck[0]['id'],
                                "interviewdate" => date("y-m-d",strtotime($job['interviewdate'])),
                                "interviewtime" => $job['interviewtime'],
                                "status" => 8,
                                "created_at" => $date,
                                ];

                                $resjobapply = [
                                "jobpost_id" => $job['jobpost_id'],
                                "user_id" => $userTokenCheck[0]['id'],
                                "interviewdate" => date("y-m-d",strtotime($job['interviewdate'])),
                                "interviewtime" => $job['interviewtime'],
                                "status" => 8,
                                "created_at" => $date,
                                "message" => $messageee,
                                "chatbot"=>$job_detail[0]['chatbot'],
                                "cname"=>$sitenamee,
                                "jobtitle"=>$job_detail[0]['jobtitle'],
                                ];
                            } else {
                                $jobapply = [
                                "jobpost_id" => $job['jobpost_id'],
                                "user_id" => $userTokenCheck[0]['id'],
                                "interviewdate" => date("y-m-d",strtotime($job['interviewdate'])),
                                "interviewtime" => $job['interviewtime'],
                                "status" => 1,
                                "created_at" => $date,
                                ];

                                $resjobapply = [
                                "jobpost_id" => $job['jobpost_id'],
                                "user_id" => $userTokenCheck[0]['id'],
                                "interviewdate" => date("y-m-d",strtotime($job['interviewdate'])),
                                "interviewtime" => $job['interviewtime'],
                                "status" => 1,
                                "created_at" => $date,
                                "message" => $messageee,
                                "chatbot"=>$job_detail[0]['chatbot'],
                                "cname"=>$sitenamee,
                                "jobtitle"=>$job_detail[0]['jobtitle'],
                                ];
                            }
                            
                            
                            if($education_match == "True" && $experience_match== "True") {
                                
                                $appliedJobs = $this->Jobpost_Model->job_fetch_appliedbycompid($job['jobpost_id'],$userTokenCheck[0]['id']);
                                
                                if(!empty($appliedJobs)) {

                                    if($this->input->post('reschedule')) {
                                        $where = ["jobpost_id"=>$job['jobpost_id'],"user_id"=>$userTokenCheck[0]['id']];

                                        $this->Jobpost_Model->appliedjob_update($where, $jobapply);

                                        $data2 = ['message'=>'You have got a rechedule application from candidate'];
                                        $this->Recruit_Model->Notificationmessageinsert($data2);
                                        $response = ['jobdetail' => $resjobapply, 'status' => "SUCCESS"];
                                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                                        
                                        if($schedule1 == $currentDate){
                                            $this->load->library('email');
                                            $data['interviewtime'] = $job['interviewtime'];
                                            $data['cname'] = $companyname[0]['cname'];
                                            $data['rname'] = $companyname[0]['fname'].' '.$companyname[0]['lname'];
                                            $data['jobtitle'] = $job_detail[0]['jobtitle'];
                                            $data['name'] =$userTokenCheck[0]['name'];
                                            $data['phone'] =$userTokenCheck[0]['country_code'].$userTokenCheck[0]['phone'];
                                            $data['email'] =$userTokenCheck[0]['email'];
                                            $data['uid'] =$userTokenCheck[0]['id'];
                                            $msg = $this->load->view('recruiter/recruiteremail2',$data,TRUE);
                                            $config=array(
                                            'charset'=>'utf-8',
                                            'wordwrap'=> TRUE,
                                            'mailtype' => 'html'
                                            );

                                            $this->email->initialize(
                                                        [
                                                        'protocol' => 'smtp',
                                                        'smtp_host' => 'smtpout.asia.secureserver.net',
                                                        'smtp_user' => 'help@jobyoda.com',
                                                        'smtp_pass' => 'Success@2021',
                                                        'smtp_port' => 465,
                                                        'smtp_crypto' => 'ssl',
                                                        'charset'=>'utf-8',
                                                        'mailtype' => 'html',
                                                        'crlf' => "\r\n",
                                                        'newline' => "\r\n"
                                                        ]
                                                );
                                            $this->email->from('help@jobyoda.com', "JobYoDA");
                                            $this->email->to($companyname[0]['email']);
                                            $this->email->subject('JobYoDA New Interview Alert - '.$companyname[0]['cname'].' - Date '.date('Y-m-d'));

                                            $this->email->message($msg);
                                            $this->email->send(); 
                                            $mobileNumber = '+'.$companyname[0]['phonecode'].$companyname[0]['phone'];
                                            $msgege = "New Interview Alert: ".$userTokenCheck[0]['name']." just scheduled for an interview today at ".$job['interviewtime']." for ".$job_detail[0]['jobtitle']." in ".$companyname[0]['cname'].". Get in touch via ".$userTokenCheck[0]['country_code'].$userTokenCheck[0]['phone']." or ".$userTokenCheck[0]['email']."";

                                            $this->SendAwsSms($mobileNumber,utf8_encode($msgege));  
                                            
                                            if(!empty($job_detail[0]['subrecruiter_id'])){
                                                $subrecruiter_data = $this->Candidate_Model->fetchcomp($job_detail[0]['subrecruiter_id']);
                                                $subrecruiter_comp = $this->Candidate_Model->fetchcomp($subrecruiter_data[0]['parent_id']);

                                                $this->load->library('email');
                                                $data1['interviewtime'] = $job['interviewtime'];
                                                $data1['cname'] = $subrecruiter_comp[0]['cname'];
                                                $data1['rname'] = $subrecruiter_data[0]['fname'].' '.$subrecruiter_data[0]['lname'];
                                                $data1['jobtitle'] = $job_detail[0]['jobtitle'];
                                                $data1['name'] =$userTokenCheck[0]['name'];
                                                $data1['phone'] =$userTokenCheck[0]['country_code'].$userTokenCheck[0]['phone'];
                                                $data1['email'] =$userTokenCheck[0]['email'];
                                                $data1['uid'] =$userTokenCheck[0]['id'];
                                                $msg = $this->load->view('recruiter/recruiteremail2',$data1,TRUE);
                                                $config=array(
                                                'charset'=>'utf-8',
                                                'wordwrap'=> TRUE,
                                                'mailtype' => 'html'
                                                );

                                                $this->email->initialize(
                                                                [
                                                                'protocol' => 'smtp',
                                                                'smtp_host' => 'smtpout.asia.secureserver.net',
                                                                'smtp_user' => 'help@jobyoda.com',
                                                                'smtp_pass' => 'Success@2021',
                                                                'smtp_port' => 465,
                                                                'smtp_crypto' => 'ssl',
                                                                'charset'=>'utf-8',
                                                                'mailtype' => 'html',
                                                                'crlf' => "\r\n",
                                                                'newline' => "\r\n"
                                                                ]
                                                        );
                                                $this->email->from('help@jobyoda.com', "JobYoDA");
                                                $this->email->to($subrecruiter_data[0]['email']);
                                                $this->email->subject('JobYoDA New Interview Alert - '.$subrecruiter_data[0]['cname'].' - Date '.date('Y-m-d'));
                                                $this->email->message($msg);
                                                $this->email->send(); 
                                                $mobileNumber1 = '+'.$subrecruiter_data[0]['phonecode'].$subrecruiter_data[0]['phone'];
                                                $msgege1 = "Rechedule Interview Alert: ".$userTokenCheck[0]['name']." just scheduled for an interview today at ".$job['interviewtime']." for ".$job_detail[0]['jobtitle']." in ".$subrecruiter_comp[0]['cname'].". Get in touch via ".$userTokenCheck[0]['country_code'].$userTokenCheck[0]['phone']." or ".$userTokenCheck[0]['email']."";
                                                $this->SendAwsSms($mobileNumber1, utf8_encode($msgege1));
                                            }   
                                        }

                                    } else {

                                        $errors = "You have already applied for this job";
                                        $response = ['message' => $errors, 'status' => "FAILURE"];
                                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                                    }

                                } else {

                                    $this->Jobpost_Model->appliedjob_insert($jobapply);
                                    $data2 = ['message'=>'You have got a new application from candidate'];
                                    $this->Recruit_Model->Notificationmessageinsert($data2);
                                    $response = ['jobdetail' => $resjobapply, 'status' => "SUCCESS"];
                                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                                    if($schedule1 == $currentDate){
                                        $this->load->library('email');
                                        $data['interviewtime'] = $job['interviewtime'];
                                        $data['cname'] = $companyname[0]['cname'];
                                        $data['rname'] = $companyname[0]['fname'].' '.$companyname[0]['lname'];
                                        $data['jobtitle'] = $job_detail[0]['jobtitle'];
                                        $data['name'] =$userTokenCheck[0]['name'];
                                        $data['phone'] =$userTokenCheck[0]['country_code'].$userTokenCheck[0]['phone'];
                                        $data['email'] =$userTokenCheck[0]['email'];
                                        $data['uid'] =$userTokenCheck[0]['id'];
                                        $msg = $this->load->view('recruiter/recruiteremail2',$data,TRUE);
                                        $config=array(
                                            'charset'=>'utf-8',
                                            'wordwrap'=> TRUE,
                                            'mailtype' => 'html'
                                        );

                                        $this->email->initialize(
                                                    [
                                                    'protocol' => 'smtp',
                                                    'smtp_host' => 'smtpout.asia.secureserver.net',
                                                    'smtp_user' => 'help@jobyoda.com',
                                                    'smtp_pass' => 'Success@2021',
                                                    'smtp_port' => 465,
                                                    'smtp_crypto' => 'ssl',
                                                    'charset'=>'utf-8',
                                                    'mailtype' => 'html',
                                                    'crlf' => "\r\n",
                                                    'newline' => "\r\n"
                                                    ]
                                        );
                                        $this->email->from('help@jobyoda.com', "JobYoDA");
                                        $this->email->to($companyname[0]['email']);
                                        $this->email->subject('JobYoDA New Interview Alert - '.$companyname[0]['cname'].' - Date '.date('Y-m-d'));

                                        $this->email->message($msg);
                                        $this->email->send(); 
                                        $mobileNumber = '+'.$companyname[0]['phonecode'].$companyname[0]['phone'];
                                        $msgege = "New Interview Alert: ".$userTokenCheck[0]['name']." just scheduled for an interview today for ".$job_detail[0]['jobtitle']." in ".$companyname[0]['cname'].". Get in touch via ".$userTokenCheck[0]['country_code'].$userTokenCheck[0]['phone']." or ".$userTokenCheck[0]['email']."";
                                        $this->SendAwsSms($mobileNumber,utf8_encode($msgege));  

                                        if(!empty($job_detail[0]['subrecruiter_id'])){
                                            $subrecruiter_data = $this->Candidate_Model->fetchcomp($job_detail[0]['subrecruiter_id']);
                                            $subrecruiter_comp = $this->Candidate_Model->fetchcomp($subrecruiter_data[0]['parent_id']);
                                            //echo $this->db->last_query();die;
                                            $this->load->library('email');
                                            $data1['interviewtime'] = $job['interviewtime'];
                                            $data1['cname'] = $subrecruiter_comp[0]['cname'];
                                            $data1['rname'] = $subrecruiter_data[0]['fname'].' '.$subrecruiter_data[0]['lname'];
                                            $data1['jobtitle'] = $job_detail[0]['jobtitle'];
                                            $data1['name'] =$userTokenCheck[0]['name'];
                                            $data1['phone'] =$userTokenCheck[0]['country_code'].$userTokenCheck[0]['phone'];
                                            $data1['email'] =$userTokenCheck[0]['email'];
                                            $data1['uid'] =$userTokenCheck[0]['id'];
                                            $msg = $this->load->view('recruiter/recruiteremail2',$data1,TRUE);
                                            $config=array(
                                            'charset'=>'utf-8',
                                            'wordwrap'=> TRUE,
                                            'mailtype' => 'html'
                                            );

                                            $this->email->initialize(
                                                            [
                                                            'protocol' => 'smtp',
                                                            'smtp_host' => 'smtpout.asia.secureserver.net',
                                                            'smtp_user' => 'help@jobyoda.com',
                                                            'smtp_pass' => 'Success@2021',
                                                            'smtp_port' => 465,
                                                            'smtp_crypto' => 'ssl',
                                                            'charset'=>'utf-8',
                                                            'mailtype' => 'html',
                                                            'crlf' => "\r\n",
                                                            'newline' => "\r\n"
                                                            ]
                                            );
                                            $this->email->from('help@jobyoda.com', "JobYoDA");
                                            $this->email->to($subrecruiter_data[0]['email']);
                                            $this->email->subject('JobYoDA New Interview Alert - '.$subrecruiter_data[0]['cname'].' - Date '.date('Y-m-d'));
                                            $this->email->message($msg);
                                            $this->email->send(); 
                                            $mobileNumber1 = '+'.$subrecruiter_data[0]['phonecode'].$subrecruiter_data[0]['phone'];
                                            $msgege1 = "New Interview Alert: ".$userTokenCheck[0]['name']." just scheduled for an interview today at ".$job['interviewtime']." for ".$job_detail[0]['jobtitle']." in ".$subrecruiter_comp[0]['cname'].". Get in touch via ".$userTokenCheck[0]['country_code'].$userTokenCheck[0]['phone']." or ".$userTokenCheck[0]['email']."";
                                            $this->SendAwsSms($mobileNumber1, utf8_encode($msgege1));
                                        }   
                                    }
                                }
                                
                            } else {
                                $errors = "You are applying for a job for which your profile doesn’t match the requirements. Please update your profile.";
                                $response = ['message' => $errors, 'status' => "FAILURE"];
                                $this->set_response($response, REST_Controller::HTTP_CREATED);
                            }
                        
                        } else{
                            $errors = "Please select correct date";
                            $response = ['message' => $errors, 'status' => "FAILURE"];
                            $this->set_response($response, REST_Controller::HTTP_CREATED);
                        }
                    } else{
                        $errors = "Interview date expire on $jobexpire.";
                        $response = ['message' => $errors, 'status' => "FAILURE"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                }
            } 
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function jobAppliedListing_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck) {
                $jobAppliedpasts = $this->Jobpost_Model->jobappliedpast_fetch_byuserid($userTokenCheck[0]['id']);
                
                if($jobAppliedpasts) {

                    $data["jobPast"] = array();

                    foreach ($jobAppliedpasts as $jobAppliedpast) {
                        
                        $checkJobIsPosted = $this->Jobpost_Model->job_detail_fetch($jobAppliedpast['id']);

                        if($checkJobIsPosted) {

                            $jobsal = $this->Jobpost_Model->getExpJob($jobAppliedpast['id']);
                            if($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else{
                                $basicsalary = '0';
                            }
                            
                            $companydetail = $this->Jobpost_Model->company_detail_fetch($jobAppliedpast['company_id']);
                            $recruitdetail = $this->Jobpost_Model->company_detail_fetch($jobAppliedpast['recruiter_id']);
                            $recruitdetailMore = $this->Jobpost_Model->job_detailLocation_fetch($jobAppliedpast['company_id']);

                            //var_dump($jobAppliedpast['subrecruiter_id']);die;

                            if($jobAppliedpast['subrecruiter_id'] == 0) {
                                $recruitmorephone = $this->Jobpost_Model->job_detailLocation_fetch($recruitdetail[0]['id']);
                                $recruitmoreemail = $this->Jobpost_Model->company_detail_fetch($recruitdetail[0]['id']);

                                if(!empty($recruitmorephone[0]["landline"])) {
                                    $rephone = $recruitmorephone[0]["landline"];
                                }else{
                                    $rephone = $recruitmorephone[0]["phone"];
                                }

                                if($recruitmorephone[0]["phonecode"] != 0) {
                                    $rephonecode = $recruitmorephone[0]["phonecode"];
                                }else{
                                    $rephonecode = '';
                                }
                            } else {
                                $recruitmorephone = $this->Jobpost_Model->job_detailLocation_fetch($jobAppliedpast['subrecruiter_id']);

                                if(!empty($recruitmorephone[0]["landline"])) {
                                    $rephone = $recruitmorephone[0]["landline"];
                                }else{
                                    $rephone = $recruitmorephone[0]["phone"];
                                }

                                if($recruitmorephone[0]["phonecode"] != 0) {
                                    $rephonecode = $recruitmorephone[0]["phonecode"];
                                }else{
                                    $rephonecode = '';
                                }

                                $recruitmoreemail = $this->Jobpost_Model->company_detail_fetch($jobAppliedpast['subrecruiter_id']);
                            }

                            //if($recruitmoreemail) {

                                $jobTop = $this->Jobpost_Model->job_toppicks($jobAppliedpast['id']);

                                if($jobTop) {
                                    if(isset($jobTop[0]['picks_id'])){
                                        $datatoppicks1 = $jobTop[0]['picks_id'];
                                    } else{
                                        $datatoppicks1 = "";
                                    }
                                    if(isset($jobTop[1]['picks_id'])){
                                        $datatoppicks2 =  $jobTop[1]['picks_id'];    
                                    } else{
                                        $datatoppicks2 = "";
                                    }
                                    if(isset($jobTop[2]['picks_id'])){
                                        $datatoppicks3 = $jobTop[2]['picks_id'];    
                                    } else{
                                        $datatoppicks3 = "";
                                    }
                                } else {
                                    $datatoppicks1 = "";
                                    $datatoppicks2 = "";
                                    $datatoppicks3 = "";
                                }

                                $companyPic = $this->Jobpost_Model->fetch_companyPic($jobAppliedpast['company_id']);            
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }
                                
                                $image = $this->Jobpost_Model->job_image($jobAppliedpast['id']);
                                    
                                if(!empty($image[0]['pic'])) {
                                    $jobpic = $image[0]['pic'];
                                } else{
                                    $jobpic = $comPic;
                                }
                                $savedjob = $this->Jobpost_Model->job_saved($jobAppliedpast['id'], $userTokenCheck[0]['id']);
                                    if(!empty($savedjob[0]['id'])) {
                                        $savedjob = '1';
                                    } else{
                                        $savedjob = "0";
                                    }
                                    $companyname = $this->Jobpost_Model->company_name_fetch($jobAppliedpast["recruiter_id"]);
                                    if(!empty($companyname[0]["cname"])){
                                        $cname = $companyname[0]["cname"];
                                    }else{
                                        $cname = '';
                                    }

                                if($recruitdetailMore[0]["comm_channel"]){
                                    $comm_channel = explode(',', $recruitdetailMore[0]["comm_channel"]);
                                } else {
                                    $comm_channel = [];
                                }

                                $distance = $this->distance($userData['cur_lat'], $userData['cur_long'], $companyPic[0]['latitude'], $companyPic[0]['longitude'], "K");
                                if(!empty($jobAppliedpast['date_day1']) && $jobAppliedpast['status']=='6'){
                                    $jobAppliedpast['date_day1'] = $jobAppliedpast['date_day1'];
                                }else{
                                    $jobAppliedpast['date_day1'] = '';
                                }

                                $distance = number_format((float)$distance, 2, '.', '');
                                $data["jobPast"][] = [
                                        "jobpost_id" => $jobAppliedpast['id'],
                                        "job_title" => $jobAppliedpast['jobtitle'],
                                        "jobDesc" => $jobAppliedpast['jobDesc'],
                                        "interviewdate" => $jobAppliedpast['interviewdate'],
                                        "interviewtime" => date("H:i", strtotime($jobAppliedpast['interviewtime'])) ,
                                        "salary" => $basicsalary,
                                        "status" => $jobAppliedpast['status'],
                                        "companyName"=> $companydetail[0]["cname"],
                                        "cname"=> $cname,
                                        "companyId"=> $companydetail[0]["id"],
                                        "toppicks1" => "$datatoppicks1",
                                        "toppicks2" => "$datatoppicks2",
                                        "toppicks3" => "$datatoppicks3",
                                        "companyPic" => $comPic,
                                        "jobpic" => $jobpic,
                                        "savedjob" => $savedjob,
                                        "distance" => @$distance??'',
                                        "date_day1" => $jobAppliedpast['date_day1'],
                                        "recruitAddress"=>$companyPic[0]['address'],
                                        //"recruitPhone"=>'+'.$rephonecode.''.$rephone,
                                        //"recruitEmail"=> @$recruitmoreemail[0]['email']??'',
                                        //"recruitName"=> @$recruitmoreemail[0]['fname']." ". @$recruitmoreemail[0]['lname'],
                                        "comm_channel" => $comm_channel,
                                    ];
                            //}
                        }
                    }


                            if(count($data["jobPast"])  <= 0) {
                                $data["jobPast"][] = [
                                    "jobpost_id" => "",
                                    "job_title" => "",
                                    "jobDesc" => "",
                                    "interviewdate" => "",
                                    "interviewtime" => "",
                                    "salary" => "",
                                    "status" => "",
                                    "companyName"=> "",
                                    "cname"=> "",
                                    "companyPic" => "",
                                    "distance" => "",
                                    "date_day1" => '',
                                    "recruitAddress"=>"",
                                    "recruitPhone"=>"",
                                    "recruitEmail"=>"",
                                    "recruitName"=>"",
                                    "comm_channel" => [],
                                ];
                            }

                } else {
                    $data["jobPast"][] = [
                                "jobpost_id" => "",
                                "job_title" => "",
                                "jobDesc" => "",
                                "interviewdate" => "",
                                "interviewtime" => "",
                                "salary" => "",
                                "status" => "",
                                "companyName"=> "",
                                "cname"=> "",
                                "companyPic" => "",
                                "distance" => "",
                                "date_day1" => '',
                                "recruitAddress"=>"",
                                "recruitPhone"=>"",
                                "recruitEmail"=>"",
                                "recruitName"=>"",
                                "comm_channel" => [],
                            ];
                }

                $jobAppliedupcomings = $this->Jobpost_Model->jobappliedupcoming_fetch_byuserid($userTokenCheck[0]['id']);
                //echo $this->db->last_query();die;
                //print_r($jobAppliedupcomings);die;
                if($jobAppliedupcomings) {

                    foreach ($jobAppliedupcomings as $jobAppliedupcoming) {

                        $checkJobIsPosted = $this->Jobpost_Model->job_detail_fetch($jobAppliedupcoming['id']);

                        if($checkJobIsPosted) {

                                $jobsal1 = $this->Jobpost_Model->getExpJob($jobAppliedupcoming['id']);
                                if($jobsal1[0]['basicsalary']) {
                                    $basicsalary1 = $jobsal1[0]['basicsalary'];
                                } else{
                                    $basicsalary1 = '0';
                                }

                                $companydetail = $this->Jobpost_Model->company_detail_fetch($jobAppliedupcoming['company_id']);

                                $recruitdetail = $this->Jobpost_Model->company_detail_fetch($jobAppliedupcoming['recruiter_id']);
                                $recruitdetailMore = $this->Jobpost_Model->job_detailLocation_fetch($jobAppliedupcoming['company_id']);

                                if($jobAppliedupcoming['subrecruiter_id'] == 0) {
                                    $recruitmorephone = $this->Jobpost_Model->job_detailLocation_fetch($recruitdetail[0]['id']);
                                    $recruitmoreemail = $this->Jobpost_Model->company_detail_fetch($recruitdetail[0]['id']);
                                    if(!empty($recruitmorephone[0]["landline"])) {
                                        $rephone = $recruitmorephone[0]["landline"];
                                    }else{
                                        $rephone = $recruitmorephone[0]["phone"];
                                    }

                                    if($recruitmorephone[0]["phonecode"] != 0) {
                                        $rephonecode = $recruitmorephone[0]["phonecode"];
                                    }else{
                                        $rephonecode = '';
                                    }
                                } else {
                                    $recruitmorephone = $this->Jobpost_Model->job_detailLocation_fetch($jobAppliedupcoming['subrecruiter_id']);

                                    if(!empty($recruitmorephone[0]["landline"])) {
                                        $rephone = $recruitmorephone[0]["landline"];
                                    }else{
                                        $rephone = $recruitmorephone[0]["phone"];
                                    }

                                    if($recruitmorephone[0]["phonecode"] != 0) {
                                        $rephonecode = $recruitmorephone[0]["phonecode"];
                                    }else{
                                        $rephonecode = '';
                                    }

                                    $recruitmoreemail= $this->Jobpost_Model->company_detail_fetch($jobAppliedupcoming['subrecruiter_id']);
                                }

                                if($recruitmoreemail) {

                                        $jobTop = $this->Jobpost_Model->job_toppicks($jobAppliedupcoming['id']);

                                        if($jobTop) {
                                            if(isset($jobTop[0]['picks_id'])){
                                                $datatoppicks1 = $jobTop[0]['picks_id'];    
                                            } else{
                                                $datatoppicks1 = "";
                                            }
                                            if(isset($jobTop[1]['picks_id'])){
                                                $datatoppicks2 =  $jobTop[1]['picks_id'];    
                                            } else{
                                                $datatoppicks2 = "";
                                            }
                                            if(isset($jobTop[2]['picks_id'])){
                                                $datatoppicks3 = $jobTop[2]['picks_id'];    
                                            } else{
                                                $datatoppicks3 = "";
                                            }
                                        } else {
                                            $datatoppicks1 = "";
                                            $datatoppicks2 = "";
                                            $datatoppicks3 = "";
                                        }

                                        $companyPic = $this->Jobpost_Model->fetch_companyPic($jobAppliedupcoming['company_id']);            
                                        if(!empty($companyPic[0]['companyPic'])) {
                                            $comPic = trim($companyPic[0]['companyPic']);
                                        } else{
                                            $comPic = "";
                                        }
                                        
                                        $image = $this->Jobpost_Model->job_image($jobAppliedupcoming['id']);
                                            
                                            
                                        if(!empty($image[0]['pic'])) {
                                            $jobpic = $image[0]['pic'];
                                        } else{
                                            $jobpic = " ";
                                        }
                                        $savedjob = $this->Jobpost_Model->job_saved($jobAppliedupcoming['id'], $userTokenCheck[0]['id']);
                                            if(!empty($savedjob[0]['id'])) {
                                                $savedjob = '1';
                                            } else{
                                                $savedjob = "0";
                                            }
                                            
                                        $companyname = $this->Jobpost_Model->company_name_fetch($jobAppliedupcoming["recruiter_id"]);
                                        if(!empty($companyname[0]["cname"])){
                                            $cname = $companyname[0]["cname"];
                                        }else{
                                            $cname = '';
                                        }

                                        if($recruitdetailMore[0]["comm_channel"]){
                                            $comm_channel = explode(',', $recruitdetailMore[0]["comm_channel"]);
                                        } else {
                                            $comm_channel = [];
                                        }

                                        $distance = $this->distance($userData['cur_lat'], $userData['cur_long'], $companyPic[0]['latitude'], $companyPic[0]['longitude'], "K");
                                        $distance = number_format((float)$distance, 2, '.', '');
                                        $data["jobUpcoming"][] = [
                                                "jobpost_id" => $jobAppliedupcoming['id'],
                                                "job_title" => $jobAppliedupcoming['jobtitle'],
                                                "jobDesc" => $jobAppliedupcoming['jobDesc'],
                                                "interviewdate" => $jobAppliedupcoming['interviewdate'],
                                                "interviewtime" => date("H:i", strtotime($jobAppliedupcoming['interviewtime'])) ,
                                                "salary" => $basicsalary1,
                                                "status" => $jobAppliedupcoming['status'],
                                                "companyName"=> $companydetail[0]["cname"],
                                                "cname"=> $cname,
                                                "companyId"=> $companydetail[0]["id"],
                                                "toppicks1" => "$datatoppicks1",
                                                "toppicks2" => "$datatoppicks2",
                                                "toppicks3" => "$datatoppicks3",
                                                "companyPic" => $comPic,
                                                "jobpic" => $jobpic,
                                                "savedjob" => $savedjob,
                                                "distance" => $distance,
                                                "recruitAddress"=>$companyPic[0]['address'],
                                                "recruitPhone"=>'+'.$rephonecode.''.$rephone,
                                                "recruitEmail"=>$recruitmoreemail[0]['email'],
                                                "recruitName"=>$recruitmoreemail[0]['fname']." ".$recruitmoreemail[0]['lname'],
                                                "comm_channel" => $comm_channel,
                                            ];
                                }
                        }
                    
                    }

                    if(count($data["jobUpcoming"])  <= 0) {
                            $data["jobUpcoming"][] = [
                                "jobpost_id" => "",
                                "job_title" => "",
                                "jobDesc" => "",
                                "interviewdate" => "",
                                "interviewtime" => "",
                                "salary" => "",
                                "status" => "",
                                "companyName"=> "",
                                "cname"=> "",
                                "companyPic" => "",
                                "distance" => "",
                                "date_day1" => '',
                                "recruitAddress"=>"",
                                "recruitPhone"=>"",
                                "recruitEmail"=>"",
                                "recruitName"=>"",
                                "comm_channel" => [],
                            ];
                        }

                } else {
                    $data["jobUpcoming"][] = [
                                "jobpost_id" => "",
                                "job_title" => "",
                                "jobDesc" => "",
                                "salary" => "",
                                "status" => "",
                                "companyName"=> "",
                                "cname"=> "",
                                "companyPic" => "",
                                "interviewdate" => "",
                                "interviewtime" => "",
                                "distance" => "",
                                "recruitAddress"=>"",
                                "recruitPhone"=>"",
                                "recruitEmail"=>"",
                                "recruitName"=>"",
                                "comm_channel" => [],
                    ];
                }
                $fetchnotifications = $this->Candidate_Model->fetchnotifications($userTokenCheck[0]['id']);
                $fetchpromonotifications = $this->Candidate_Model->fetchpromonotifications($userTokenCheck[0]['id']);
                $response = ['jobAppliedListing' => $data,'notification_count'=>count($fetchnotifications)+count($fetchpromonotifications), 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function filters_post() {
        $filterData = $this->input->post();
        $data = ["token" => $filterData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        
        $data["jobList"] = [];
        
        if($userTokenCheck) {

            $expmonth = $userTokenCheck[0]['exp_month'];
            $expyear = $userTokenCheck[0]['exp_year'];
            
            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            } else {
                $expfilter ="";
            }

            // $updated_keyword = $this->check_company_keyword($filterData['search']);
            // $filterData['search'] = $updated_keyword;

            if(strlen($filterData['selectedSearch']) > 0) {
                $combination = [];

                if($filterData['search'] == $filterData['selectedSearch']) {
                    $filterData['search'] = $filterData['selectedSearch'];
                } else {
                    $filterData['selectedSearch'] = "";
                }
            } else {
                if(strlen($filterData['search']) > 0) {
                    $combination = $this->get_string_combinations($filterData['search']);
                } else {
                    $combination = [];
                }
            } 
            //var_dump($combination);die;
            $jobdata = array();
            $jobdatacount = array();

            if(count($combination) > 0) {

                $jobArray = array();
                //var_dump($this->default_distance);die;
                foreach($combination as $combina) {
                    $filterData['search'] = $combina;
                    $filterData['selectedSearch'] = $filterData['selectedSearch'];

                    $jobfetchs = $this->Jobpost_Model->job_fetch_all($filterData, $userTokenCheck[0]['id'], $expfilter, $this->default_distance);
                    $cDate = date('Y-m-d');
                    if($jobfetchs) {
                        foreach ($jobfetchs as $jobfetch) {

                            if(in_array($jobfetch['id'], $jobArray)) { } else {
                                if(strtotime($jobfetch['jobexpire']) >= strtotime($cDate)) {
                                    $jobArray[] = $jobfetch['id'];
                                    $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                                    if($jobTop) {
                                        if(isset($jobTop[0]['picks_id'])){
                                            $one = (string)$jobTop[0]['picks_id'];
                                            $dataJobList[0] = "$one";
                                        } else{
                                            $dataJobList[0] = "";
                                        }
                                        if(isset($jobTop[1]['picks_id'])) {
                                            $two = (string)$jobTop[1]['picks_id'];
                                            $dataJobList[1] = "$two";
                                        } else {
                                            $dataJobList[1] = "";
                                        }
                                        if(isset($jobTop[2]['picks_id'])){
                                            $three = (string)$jobTop[2]['picks_id'];
                                            $dataJobList[2] = "$three";
                                        } else{
                                            $dataJobList[2] = "";
                                        }
                                    } else {
                                        $dataJobList[0] = "";
                                        $dataJobList[1] = "";
                                        $dataJobList[2] = "";
                                    }
                                    //print_r($jobfetch);die;
                                    //$companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['company_id']);
                                    $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['recruiter_id']);
                                    $sitename = $this->Jobpost_Model->company_name_fetch($jobfetch['company_id']);
                                    if(!empty($companyname[0]["cname"])){
                                        $cname = $companyname[0]["cname"];
                                    }else{
                                        $cname = '';
                                    }
                                    if(!empty($sitename[0]["cname"])){
                                        $companyName = $sitename[0]["cname"];
                                    }else{
                                        $companyName = '';
                                    }
                                    $sal = $this->Jobpost_Model->fetch_highsalary123($jobfetch['id'],$expfilter);                                    

                                    // $image = $this->Jobpost_Model->job_image($jobfetch['id']);    
                                    // if(!empty($companyPic[0]['companyPic'])) {
                                    //     $comPic =trim($companyPic[0]['companyPic']);
                                    // } else{
                                    //     $comPic = "";
                                    // }
                                    // if(!empty($image[0]['pic'])) {
                                    //      $jobpic =$image[0]['pic'];
                                    // } else{
                                    //      $jobpic =$comPic;
                                    // }

                                    $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['company_id']);
                                    if(!empty($companyPic[0]['companyPic'])) {
                                        $comPic = trim($companyPic[0]['companyPic']);
                                    } else{
                                        $comPic = "";
                                    }
                                    $jobImage = $this->Jobpost_Model->job_image($jobfetch['id']);
                                    if(isset($jobImage[0]['pic'])) {
                                        $jobpic = $jobImage[0]['pic'];
                                    }else{
                                        $jobpic = $comPic;
                                    }

                                    $savedjob = $this->Jobpost_Model->job_saved($jobfetch['id'], $userTokenCheck[0]['id']);
                                    if(!empty($savedjob[0]['id'])) {
                                        $savedjob = '1';
                                    } else{
                                        $savedjob = "0";
                                    }
                                    if($jobfetch['jobPitch']){
                                        $jobfetch['jobPitch']=$jobfetch['jobPitch'];
                                    }else{
                                          $jobfetch['jobPitch']="";
                                    }

                                    if($jobfetch['chatbot'] == 1) {
                                        $jobfetch['mode'] = "chatbot";
                                    }

                                    $total_salary = (int)$sal[0]['salary'] + (int)$jobfetch['allowance'];
                                    $distance = $this->distance($filterData['cur_lat'], $filterData['cur_long'], $companyPic[0]['latitude'], $companyPic[0]['longitude'], "K");
                                    $distance = number_format((float)$distance, 2, '.', '');
                                    $jobdata[] = [
                                            "jobpost_id" => $jobfetch['id'],
                                            "job_title" => $jobfetch['jobtitle'],
                                            "jobDesc" => $jobfetch['jobPitch'],
                                            "salary" => (int)$jobfetch['salary'],
                                            "jobExpire" => $jobfetch['jobexpire'],
                                            "address" => $jobfetch['address'],
                                            "companyName" => $companyName,
                                            "cname" => $cname,
                                            "company_id" => $jobfetch['company_id'],
                                            "latitude" => $companyPic[0]['latitude'],
                                            "longitude" => $companyPic[0]['longitude'],
                                            "jobcount" => 1,
                                            "toppicks1" => $dataJobList[0],
                                            "toppicks2" => $dataJobList[1],
                                            "toppicks3" => $dataJobList[2],
                                            "companyPic" => $comPic,
                                            "jobpic" => $jobpic,
                                            "savedjob" => $savedjob,
                                            "distance" => $distance,
                                            "mode" => $jobfetch['mode'],
                                            "modeurl" => $jobfetch['modeurl'],
                                            "sharecount" => $this->getsharecount($jobfetch['id']),
                                            
                                    ];
                                }
                            }
                        }
                    }


                }
            } else {
                
                $jobfetchs = $this->Jobpost_Model->job_fetch_all($filterData, $userTokenCheck[0]['id'], $expfilter, $this->default_distance);
                $cDate = date('Y-m-d');
                if($jobfetchs) {
                    foreach ($jobfetchs as $jobfetch) {

                        if(strtotime($jobfetch['jobexpire']) >= strtotime($cDate)) {

                            $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                            if($jobTop) {
                                if(isset($jobTop[0]['picks_id'])){
                                    $one = (string)$jobTop[0]['picks_id'];
                                    $dataJobList[0] = "$one";
                                } else{
                                    $dataJobList[0] = "";
                                }
                                if(isset($jobTop[1]['picks_id'])) {
                                    $two = (string)$jobTop[1]['picks_id'];
                                    $dataJobList[1] = "$two";
                                } else {
                                    $dataJobList[1] = "";
                                }
                                if(isset($jobTop[2]['picks_id'])){
                                    $three = (string)$jobTop[2]['picks_id'];
                                    $dataJobList[2] = "$three";
                                } else{
                                    $dataJobList[2] = "";
                                }
                            } else {
                                $dataJobList[0] = "";
                                $dataJobList[1] = "";
                                $dataJobList[2] = "";
                            }
                            //print_r($jobfetch);die;
                            $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['company_id']);
                            $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['recruiter_id']);
                            $sitename = $this->Jobpost_Model->company_name_fetch($jobfetch['company_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            if(!empty($sitename[0]["cname"])){
                                $companyName = $sitename[0]["cname"];
                            }else{
                                $companyName = '';
                            }
                            $sal = $this->Jobpost_Model->fetch_highsalary123($jobfetch['id'],$expfilter);
                            $image = $this->Jobpost_Model->job_image($jobfetch['id']);    
                            if(!empty($companyPic[0]['companyPic'])) {
                                $comPic =trim($companyPic[0]['companyPic']);
                            } else{
                                $comPic = "";
                            }
                            if(!empty($image[0]['pic'])) {
                                 $jobpic =$image[0]['pic'];
                            } else{
                                 $jobpic =$comPic;
                            }
                            $savedjob = $this->Jobpost_Model->job_saved($jobfetch['id'], $userTokenCheck[0]['id']);
                            if(!empty($savedjob[0]['id'])) {
                                $savedjob = '1';
                            } else{
                                $savedjob = "0";
                            }
                            if($jobfetch['jobPitch']){
                                $jobfetch['jobPitch']=$jobfetch['jobPitch'];
                            }else{
                                  $jobfetch['jobPitch']="";
                            }

                            if($jobfetch['chatbot'] == 1) {
                                $jobfetch['mode'] = "chatbot";
                            }

                            $total_salary = (int)$sal[0]['salary'] + (int)$jobfetch['allowance'];
                            $distance = $this->distance($filterData['cur_lat'], $filterData['cur_long'], $companyPic[0]['latitude'], $companyPic[0]['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', '');
                            $jobdata[] = [
                                    "jobpost_id" => $jobfetch['id'],
                                    "job_title" => $jobfetch['jobtitle'],
                                    "jobDesc" => $jobfetch['jobPitch'],
                                    "salary" => (int)$jobfetch['salary'],
                                    "jobExpire" => $jobfetch['jobexpire'],
                                    "address" => $jobfetch['address'],
                                    "companyName" => $companyName,
                                    "cname" => $cname,
                                    "company_id" => $jobfetch['company_id'],
                                    "latitude" => $companyPic[0]['latitude'],
                                    "longitude" => $companyPic[0]['longitude'],
                                    "jobcount" => 1,
                                    "toppicks1" => $dataJobList[0],
                                    "toppicks2" => $dataJobList[1],
                                    "toppicks3" => $dataJobList[2],
                                    "companyPic" => $comPic,
                                    "jobpic" => $jobpic,
                                    "savedjob" => $savedjob,
                                    "distance" => $distance,
                                    "mode" => $jobfetch['mode'],
                                    "modeurl" => $jobfetch['modeurl'],
                                    "sharecount" => $this->getsharecount($jobfetch['id']),
                            ];
                        }
                    }
                }

            }


            if(count($combination) > 0) {

                $jobArray = array();

                foreach($combination as $combina) {
                    $filterData['search'] = $combina;

                    $jobcountfetchs = $this->Jobpost_Model->jobcount_fetch_all($filterData, $userTokenCheck[0]['id'], $expfilter, $this->default_distance);
                    $returnnewarray = array();
                    $latlogcheck =array();
                    foreach ($jobcountfetchs as $value123) {
                          
                          if(in_array($value123['latitude'],$latlogcheck)){
                          
                              $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['jobcount'] = $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['jobcount'] + 1;
                          
                          } else {
                          
                              $latlogcheck[] = $value123['latitude'];
                              $returnnewarray[] = $value123;
                          }
                    }
                    $jobcountfetchs = $returnnewarray;
                    if($jobcountfetchs) {
                        foreach ($jobcountfetchs as $jobcountfetch) {

                            if(strtotime($jobcountfetch['jobexpire']) >= strtotime($cDate)) {
                                $jobTop = $this->Jobpost_Model->job_toppicks($jobcountfetch['id']);
                                if($jobTop) {
                                    if(isset($jobTop[0]['picks_id'])){
                                        $one = (string)$jobTop[0]['picks_id'];
                                        $dataJobList[0] = "$one";
                                    } else{
                                        $dataJobList[0] = "";
                                    }
                                    if(isset($jobTop[1]['picks_id'])) {
                                        $two = (string)$jobTop[1]['picks_id'];
                                        $dataJobList[1] = "$two";
                                    } else {
                                        $dataJobList[1] = "";
                                    }
                                    if(isset($jobTop[2]['picks_id'])){
                                        $three = (string)$jobTop[2]['picks_id'];
                                        $dataJobList[2] = "$three";
                                    } else{
                                        $dataJobList[2] = "";
                                    }
                                } else {
                                    $dataJobList[0] = "";
                                    $dataJobList[1] = "";
                                    $dataJobList[2] = "";
                                }

                                $getlatlognids = $this->Jobpost_Model->jobfetchbylatlong($jobcountfetch['latitude'], $jobcountfetch['longitude'], $userTokenCheck[0]['id'],$expfilter);
                                $getmaxsal = array();
                                foreach($getlatlognids as $getlatlognid) {
                                    $salaryarrays = $this->Jobpost_Model->jobfetchbymaxsalary123($getlatlognid['id'],$expfilter);
                                    $getmaxsal[] = (int) $salaryarrays[0]['basesalary'];
                                }
                                
                                if($getmaxsal){
                                    $gotsalmax =  max($getmaxsal);
                                }else{
                                    $sal = $this->Jobpost_Model->fetch_highsalary123($jobcountfetch['id'],$expfilter);
                                    $gotsalmax = (int)$sal[0]['salary'];
                                }
                                
                                $total_salary = (int)$gotsalmax + (int)$jobcountfetch['allowance'];
                                $jobdatacount[] = [
                                        "company_id" => $jobcountfetch['company_id'],
                                        "salary" => (int)$jobcountfetch['salary'],
                                        //"salary" => (string)$total_salary,
                                        "latitude" => $jobcountfetch['latitude'],
                                        "longitude" => $jobcountfetch['longitude'],
                                        "address" => $jobcountfetch['address'],
                                        "jobcount" => (string)$jobcountfetch['jobcount'],
                                        "toppicks1" => $dataJobList[0],
                                        "toppicks2" => $dataJobList[1],
                                        "toppicks3" => $dataJobList[2]
                                    ];
                            }
                        }                 
                    }
                }
            } else {

                $jobcountfetchs = $this->Jobpost_Model->jobcount_fetch_all($filterData, $userTokenCheck[0]['id'], $expfilter, $this->default_distance);
                $returnnewarray = array();
                $latlogcheck =array();
                foreach ($jobcountfetchs as $value123) {
                      
                      if(in_array($value123['latitude'],$latlogcheck)){
                      
                          $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['jobcount'] = $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['jobcount'] + 1;
                      
                      } else {
                      
                          $latlogcheck[] = $value123['latitude'];
                          $returnnewarray[] = $value123;
                      }
                }
                $jobcountfetchs = $returnnewarray;
                if($jobcountfetchs) {
                    foreach ($jobcountfetchs as $jobcountfetch) {

                        if(strtotime($jobcountfetch['jobexpire']) >= strtotime($cDate)) {
                            $jobTop = $this->Jobpost_Model->job_toppicks($jobcountfetch['id']);
                            if($jobTop) {
                                if(isset($jobTop[0]['picks_id'])){
                                    $one = (string)$jobTop[0]['picks_id'];
                                    $dataJobList[0] = "$one";
                                } else{
                                    $dataJobList[0] = "";
                                }
                                if(isset($jobTop[1]['picks_id'])) {
                                    $two = (string)$jobTop[1]['picks_id'];
                                    $dataJobList[1] = "$two";
                                } else {
                                    $dataJobList[1] = "";
                                }
                                if(isset($jobTop[2]['picks_id'])){
                                    $three = (string)$jobTop[2]['picks_id'];
                                    $dataJobList[2] = "$three";
                                } else{
                                    $dataJobList[2] = "";
                                }
                            } else {
                                $dataJobList[0] = "";
                                $dataJobList[1] = "";
                                $dataJobList[2] = "";
                            }

                            $getlatlognids = $this->Jobpost_Model->jobfetchbylatlong($jobcountfetch['latitude'], $jobcountfetch['longitude'], $userTokenCheck[0]['id'],$expfilter);
                            $getmaxsal = array();
                            foreach($getlatlognids as $getlatlognid) {
                                $salaryarrays = $this->Jobpost_Model->jobfetchbymaxsalary123($getlatlognid['id'],$expfilter);
                                $getmaxsal[] = (int) $salaryarrays[0]['basesalary'];
                            }
                            
                            if($getmaxsal){
                                $gotsalmax =  max($getmaxsal);
                            }else{
                                $sal = $this->Jobpost_Model->fetch_highsalary123($jobcountfetch['id'],$expfilter);
                                $gotsalmax = (int)$sal[0]['salary'];
                            }
                            
                            $total_salary = (int)$gotsalmax + (int)$jobcountfetch['allowance'];
                            $jobdatacount[] = [
                                    "company_id" => $jobcountfetch['company_id'],
                                    "salary" => (int)$jobcountfetch['salary'],
                                    //"salary" => (string)$total_salary,
                                    "latitude" => $jobcountfetch['latitude'],
                                    "longitude" => $jobcountfetch['longitude'],
                                    "address" => $jobcountfetch['address'],
                                    "jobcount" => (string)$jobcountfetch['jobcount'],
                                    "toppicks1" => $dataJobList[0],
                                    "toppicks2" => $dataJobList[1],
                                    "toppicks3" => $dataJobList[2]
                                ];
                        }
                    }
                        
                }
            }
            if(count($jobdata) <= 0) {
                $data["jobList"] = [];
                $data['jobcount'] = [];

                $errors = "No Jobs available. Please update your experience to get more relevant Jobs.";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);

            } else {
                $data["jobList"] = $jobdata;
                $data['jobcount'] = $jobdatacount;

                $response = ['filters' => $data, 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }

        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function get_string_combinations($strng)
    {
        $words = explode(' ',$strng);
        $elements = pow(2, count($words))-1;
        $result = array();
        for ($i = 1; $i<=$elements; $i++)
       {
            $bin = decbin($i);
            $padded_bin = str_pad($bin, count($words), "0", STR_PAD_LEFT);
            $res = array();
            for ($k=0; $k<count($words); $k++)
           {
             
                if ($padded_bin[$k]==1){
                    $res[] = $words[$k];
                }
            }
            sort($res);
            $result[] = implode(" ", $res);
        }

        usort($result, function($a, $b) {
            return strlen($b) - strlen($a);
        });
        array_unshift($result, $strng);
        //sort($result);
        return $result;
    }

    public function check_company_keyword($keyword) {
        $searchTerm = $keyword;
        $search_array = array();

        if(!empty($searchTerm)) {

            if ($this->session->userdata('usersess')) {
                $userSess = $this->session->userdata('usersess');  
                $tokendata = ['id'=>$userSess['id']];
                $userID = $userSess['id'];

                $userData = $this->User_Model->token_match($tokendata);
                $expmonth = $userData[0]['exp_month'];
                $expyear = $userData[0]['exp_year'];

                $expmonth = $exp_month;
                $expyear = $exp_year;

                if($expyear == 0 && $expmonth == 0) {
                    $expfilter = "1";        
                } else if($expyear == 0 && $expmonth < 6) {
                    $expfilter = "2";
                } else if($expyear < 1 && $expmonth >= 6) {
                    $expfilter = "3";  
                } else if($expyear < 2 && $expyear >= 1) {
                    $expfilter = "4";
                } else if($expyear < 3 && $expyear >= 2) {
                    $expfilter = "5";
                } else if($expyear < 4 && $expyear >= 3) {
                    $expfilter = "6";  
                } else if($expyear < 5 && $expyear >= 4) {
                    $expfilter = "7";  
                } else if($expyear < 6 && $expyear >= 5) {
                    $expfilter = "8";  
                } else if($expyear < 7 && $expyear >= 6) {
                    $expfilter = "9";  
                } else if($expyear >= 7) { 
                    $expfilter = "10";
                } else {
                    $expfilter = "";
                }
            } else {
                $expfilter = "";
                $userID = 0;
            }

            $job_list = $this->Common_Model->home_search_job_recruiter($searchTerm);
            if (!empty($job_list)) {
                $search_array['company_lists'] = $job_list;            
            }
            $job_list = $this->Common_Model->home_search_job_company($searchTerm);
            if (!empty($job_list)) {
                $search_array['company_lists'] = $job_list;            
            }
            $job_list1 = $this->Common_Model->home_search_job_title($searchTerm, $expfilter, $userID);
            if (!empty($job_list1)) {
                $search_array['company_lists'] = $job_list1;
            }

            if(count($search_array['company_lists']) > 0) {
                return $searchTerm;
            
            } else {

                $break_string = explode(' ',$searchTerm);
                if(count($break_string) >= 3) {
                    $searchTerm1 = $break_string[0].' '.$break_string[1];
                
                } else if(count($break_string) == 2) {
                    $searchTerm1 = $break_string[0];
                }

                $job_list = $this->Common_Model->home_search_job_recruiter($searchTerm1);
                if (!empty($job_list)) {
                    $search_array['company_lists'] = $job_list;            
                }
                $job_list = $this->Common_Model->home_search_job_company($searchTerm1);
                if (!empty($job_list)) {
                    $search_array['company_lists'] = $job_list;            
                }
                $job_list1 = $this->Common_Model->home_search_job_title($searchTerm1, $expfilter, $userID);
                if (!empty($job_list1)) {
                    $search_array['company_lists'] = $job_list1;
                }

                if(count($search_array['company_lists']) > 0) {
                    return $searchTerm1;
                
                } else {

                    $break_string = explode(' ',$searchTerm);
                    if(count($break_string) >= 3) {
                        $searchTerm1 = $break_string[0];
                    
                    } else if(count($break_string) == 2) {
                        $searchTerm1 = $break_string[0];
                    }
                    $job_list = $this->Common_Model->home_search_job_recruiter($searchTerm2);
                    if (!empty($job_list)) {
                        $search_array['company_lists'] = $job_list;            
                    }
                    $job_list = $this->Common_Model->home_search_job_company($searchTerm2);
                    if (!empty($job_list)) {
                        $search_array['company_lists'] = $job_list;            
                    }
                    $job_list1 = $this->Common_Model->home_search_job_title($searchTerm2, $expfilter, $userID);
                    if (!empty($job_list1)) {
                        $search_array['company_lists'] = $job_list1;
                    }

                    if(count($search_array['company_lists']) > 0) {
                        return $searchTerm2;
                    } else {
                        return $searchTerm;
                    }
                }
            }
        }
        return $searchTerm;
    }

    public function jobFilters_post() {
        $filterData = $this->input->post();
        $data = ["token" => $filterData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        $data["jobList"] = [];
        $jobListing = [];

        if($userTokenCheck) {
            
            if(isset($filterData['exp_year']) && isset($filterData['exp_month'])){
                $expmonth = $filterData['exp_month'];
                $expyear = $filterData['exp_year'];
                
            }else{
                $expmonth = $userTokenCheck[0]['exp_month'];
                $expyear = $userTokenCheck[0]['exp_year'];
            }
            
            //echo $expyear;die;
            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }

            if($filterData['save_notify']=='1') {
                $save_keys_array=['user_id'=>$userTokenCheck[0]['id'],'salarySort'=>$filterData['salarySort'], 'locationSort'=>$filterData['locationSort'], 'basicSalary'=>$filterData['basicSalary'], 'joblevel'=>$filterData['joblevel'], 'industry'=>$filterData['industry'], 'jobcategory'=>$filterData['jobcategory'], 'location'=>$filterData['location'], 'jobsubcategory'=>$filterData['jobsubcategory'], 'language'=>$filterData['language'], 'toppicks'=>$filterData['toppicks'], 'allowances'=>$filterData['allowances'], 'medical'=>$filterData['medical'], 'workshift'=>$filterData['workshift'], 'lat'=>$filterData['lat'], 'long'=>$filterData['long']];
                $key_data = $this->Jobpost_Model->fetch_keys($userTokenCheck[0]['id']);
                if(empty($key_data)){
                    $this->Jobpost_Model->insert_keys($save_keys_array);
                }else{
                    $this->Jobpost_Model->update_keys($save_keys_array, $userTokenCheck[0]['id']);
                }
            }
            
            $jobfetchs = $this->Jobpost_Model->jobfilter_fetch_all($filterData, $userTokenCheck[0]['id'],$expfilter);


            if($jobfetchs) {
                $x=1;

                foreach ($jobfetchs as $jobfetch) {

                        
                  $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                  //print_r($jobTop);die;  

                   $array_first = array();

                   foreach ($jobTop as $cgvalue) {
                       $array_first[] = $cgvalue['picks_id'];
                   }

                   $array_second = explode(",",$filterData['toppicks']);

                   $jobfetch['countbytemp'] = sizeof(array_intersect($array_first,$array_second));

                  if($jobTop) {
                      if(isset($jobTop[0]['picks_id'])) {
                          $one = (string)$jobTop[0]['picks_id'];
                          $dataJobList[0] = "$one";
                      } else{
                          $dataJobList[0] = "";
                      }
                      if(isset($jobTop[1]['picks_id'])){
                          $two = (string)$jobTop[1]['picks_id'];
                          $dataJobList[1] = "$two";
                      } else{
                          $dataJobList[1] = "";
                      }
                      if(isset($jobTop[2]['picks_id'])){
                          $three = (string)$jobTop[2]['picks_id'];
                          $dataJobList[2] = "$three";
                      } else{
                          $dataJobList[2] = "";
                      }
                  } else {
                      $dataJobList[0] = "";
                      $dataJobList[1] = "";
                      $dataJobList[2] = "";
                  }


                  if($jobfetch['id']){
                      $jobfetch['id']=$jobfetch['id'];
                  }else{
                      $jobfetch['id']="";
                  }
                  if($jobfetch['jobtitle']){
                      $jobfetch['jobtitle']=$jobfetch['jobtitle'];
                  }else{
                      $jobfetch['jobtitle']="";
                  }
                  if($jobfetch['jobPitch']){
                      $jobfetch['jobPitch']=$jobfetch['jobPitch'];
                  }else{
                      $jobfetch['jobPitch']="";
                  }
                  
                  if($jobfetch['cname']){
                      $jobfetch['cname']=$jobfetch['cname'];
                  }else{
                      $jobfetch['cname']="";
                  }
                  if($jobfetch['latitude']){
                      $jobfetch['latitude']=$jobfetch['latitude'];
                  }else{
                      $jobfetch['latitude']="";
                  }
                  if($jobfetch['longitude']){
                      $jobfetch['longitude']=$jobfetch['longitude'];
                  }else{
                      $jobfetch['longitude']="";
                  }

                  $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['company_id']);
                  
                  if(!empty($companyPic[0]['companyPic'])) {
                      $comPic = trim($companyPic[0]['companyPic']);
                  } else{
                      $comPic = "";
                  }

                  $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['recruiter_id']);

                  if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }
                  if($filterData['basicSalary']) {
                     $sal = $this->Jobpost_Model->fetch_highsalary12($jobfetch['id'], $filterData['basicSalary'],$expfilter);
                  } else{
                     $sal = $this->Jobpost_Model->fetch_highsalary123($jobfetch['id'],$expfilter);
                    
                  }


                  $image = $this->Jobpost_Model->job_image($jobfetch['id']);
                  
                  
                      if(!empty($image[0]['pic'])) {
                          $jobpic = $image[0]['pic'];
                      } else{
                          $jobpic = $comPic;
                      }
                      $savedjob = $this->Jobpost_Model->job_saved($jobfetch['id'], $userTokenCheck[0]['id']);
                      if(!empty($savedjob[0]['id'])) {
                          $savedjob = '1';
                      } else{
                          $savedjob = "0";
                      }

                      if($jobfetch['chatbot'] == 1) {
                            $jobfetch['mode'] = "chatbot";
                        }

                    $total_salary = (int)$jobfetch['salary']+(int)$jobfetch['allowance'];
                    $distance = $this->distance($filterData['cur_lat'], $filterData['cur_long'], $companyPic[0]['latitude'], $companyPic[0]['longitude'], "K");

                    $distance = number_format((float)$distance, 2, '.', '');
                    $jobListing[] = [
                          "jobpost_id" => $jobfetch['id'],
                          "job_title" => $jobfetch['jobtitle'],
                          "jobDesc" => $jobfetch['jobPitch'],
                          "job_count"=>$jobfetch['countbytemp'],
                          "salary" => (int)$jobfetch['salary'],
                          "company_id" => $jobfetch['company_id'],
                          "jobExpire" => $jobfetch['jobexpire'],
                          "companyName" => $jobfetch['cname'],
                          "cname"=> $cname,
                          "latitude" => $jobfetch['latitude'],
                          "longitude" => $jobfetch['longitude'],
                          "address" => $jobfetch['address'],
                          "jobcount" =>1,
                          "toppicks1" => $dataJobList[0],
                          "toppicks2" => $dataJobList[1],
                          "toppicks3" => $dataJobList[2],
                          "companyPic" => $comPic,
                          "jobpic" => $jobpic,
                          "savedjob" => $savedjob,
                          "distance" => $distance,
                          "mode" => $jobfetch['mode'],
                          "modeurl" => $jobfetch['modeurl'],
                          "sharecount" => $this->getsharecount($jobfetch['id']),
                      ];
                    $x++;
                }
                if(count($data["jobList"]) <= 0) {
                    $data["jobList"][] = [];
                }
            } else{
                $data["jobList"] = [];
            }
            if($filterData['salarySort'] == 1) {
               $price = array();
               foreach ($jobListing as $key => $row) {
                    $price[$key] = $row['salary'];
               }
               array_multisort($price, SORT_DESC, $jobListing);
               $data["jobList"] = $jobListing;
            }

            else{

                 $price = array();
               foreach ($jobListing as $key => $row) {
                    $price[$key] = $row['job_count'];
               }
               array_multisort($price, SORT_DESC, $jobListing);
               
                $data["jobList"] = $jobListing;
            }
            
            $jobcountfetchs = $this->Jobpost_Model->jobfiltercount_fetch_all($filterData, $userTokenCheck[0]['id'],$expfilter);
                 $returnnewarray = array();
                 $latlogcheck =array();
                 foreach ($jobcountfetchs as $value123) {
                      if(in_array($value123['latitude'],$latlogcheck)){
                          $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['jobcount'] = $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['jobcount'] + 1;
                      } else {
                          $latlogcheck[] = $value123['latitude'];
                          $returnnewarray[] = $value123;
                      }
                 }

                  $jobcountfetchs = $returnnewarray;
            if($jobcountfetchs && $jobcountfetchs[0]['jobcount'] != 0) {
                
               foreach ($jobcountfetchs as $jobcountfetch) {
                        
                  $jobTop = $this->Jobpost_Model->job_toppicks($jobcountfetch['id']);
                  
                  if($jobTop) {
                      if(isset($jobTop[0]['picks_id'])){
                          $one = (string)$jobTop[0]['picks_id'];
                          $dataJobList[0] = "$one";
                      } else{
                          $dataJobList[0] = "";
                      }
                      if(isset($jobTop[1]['picks_id'])) {
                          $two = (string)$jobTop[1]['picks_id'];
                          $dataJobList[1] = "$two";
                      } else {
                          $dataJobList[1] = "";
                      }
                      if(isset($jobTop[2]['picks_id'])){
                          $three = (string)$jobTop[2]['picks_id'];
                          $dataJobList[2] = "$three";
                      } else{
                          $dataJobList[2] = "";
                      }
                  } else {
                      $dataJobList[0] = "";
                      $dataJobList[1] = "";
                      $dataJobList[2] = "";
                  }

                  $getlatlognids = $this->Jobpost_Model->jobfetchbylatlong($jobcountfetch['latitude'], $jobcountfetch['longitude'], $userTokenCheck[0]['id'],$expfilter);
                  $getmaxsal = array();

                  foreach($getlatlognids as $getlatlognid) {

                     if($filterData['basicSalary']) {
                        $salaryarrays = $this->Jobpost_Model->jobfetchbymaxsalary12($getlatlognid['id'],$filterData['basicSalary'],$expfilter);
                     } else{
                        $salaryarrays = $this->Jobpost_Model->jobfetchbymaxsalary123($getlatlognid['id'],$expfilter);
                     }
                     if(!empty($salaryarrays[0]['basesalary'])){
                        $getmaxsal[] = (int) $salaryarrays[0]['basesalary'];
                     }    
                  }
                  $data["jobcount"][] = [
                          "company_id" => $jobcountfetch['company_id'],
                          "salary" => (int)$jobcountfetch['salary'],
                          "latitude" => $jobcountfetch['latitude'],
                          "longitude" => $jobcountfetch['longitude'],
                          "address" => $jobcountfetch['address'],
                          "jobcount" => (string)($jobcountfetch['jobcount']),
                          "toppicks1" => $dataJobList[0],
                          "toppicks2" => $dataJobList[1],
                          "toppicks3" => $dataJobList[2]
                      ];
               }

            } else{
                
                $expfilter ="";
                $jobfetchs = $this->Jobpost_Model->jobfilter_fetch_all($filterData, $userTokenCheck[0]['id'],$expfilter);
           
                if($jobfetchs) {
                    $x=1;
                    foreach ($jobfetchs as $jobfetch) {

                      $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                       $array_first = array();
                       foreach ($jobTop as $cgvalue) {
                           $array_first[] = $cgvalue['picks_id'];
                       }

                       $array_second = explode(",",$filterData['toppicks']);

                       $jobfetch['countbytemp'] = sizeof(array_intersect($array_first,$array_second));

                      if($jobTop) {
                          if(isset($jobTop[0]['picks_id'])) {
                              $one = (string)$jobTop[0]['picks_id'];
                              $dataJobList[0] = "$one";
                          } else{
                              $dataJobList[0] = "";
                          }
                          if(isset($jobTop[1]['picks_id'])){
                              $two = (string)$jobTop[1]['picks_id'];
                              $dataJobList[1] = "$two";
                          } else{
                              $dataJobList[1] = "";
                          }
                          if(isset($jobTop[2]['picks_id'])){
                              $three = (string)$jobTop[2]['picks_id'];
                              $dataJobList[2] = "$three";
                          } else{
                              $dataJobList[2] = "";
                          }
                      } else {
                          $dataJobList[0] = "";
                          $dataJobList[1] = "";
                          $dataJobList[2] = "";
                      }


                      if($jobfetch['id']){
                          $jobfetch['id']=$jobfetch['id'];
                      }else{
                          $jobfetch['id']="";
                      }
                      if($jobfetch['jobtitle']){
                          $jobfetch['jobtitle']=$jobfetch['jobtitle'];
                      }else{
                          $jobfetch['jobtitle']="";
                      }
                      if($jobfetch['jobPitch']){
                          $jobfetch['jobPitch']=$jobfetch['jobPitch'];
                      }else{
                          $jobfetch['jobPitch']="";
                      }
                      
                      if($jobfetch['cname']){
                          $jobfetch['cname']=$jobfetch['cname'];
                      }else{
                          $jobfetch['cname']="";
                      }
                      if($jobfetch['latitude']){
                          $jobfetch['latitude']=$jobfetch['latitude'];
                      }else{
                          $jobfetch['latitude']="";
                      }
                      if($jobfetch['longitude']){
                          $jobfetch['longitude']=$jobfetch['longitude'];
                      }else{
                          $jobfetch['longitude']="";
                      }

                      $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['company_id']);
                      
                      if(!empty($companyPic[0]['companyPic'])) {
                          $comPic = trim($companyPic[0]['companyPic']);
                      } else{
                          $comPic = "";
                      }

                      $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['recruiter_id']);

                      if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }
                      if($filterData['basicSalary']) {
                         $sal = $this->Jobpost_Model->fetch_highsalary12($jobfetch['id'], $filterData['basicSalary']);
                      } else{
                         $sal = $this->Jobpost_Model->fetch_highsalary($jobfetch['id'],$expfilter);
                      }
                      $image = $this->Jobpost_Model->job_image($jobfetch['id']);
                      
                      
                          if(!empty($image[0]['pic'])) {
                              $jobpic = $image[0]['pic'];
                          } else{
                              $jobpic = $comPic;
                          }
                          $savedjob = $this->Jobpost_Model->job_saved($jobfetch['id'], $userTokenCheck[0]['id']);
                          if(!empty($savedjob[0]['id'])) {
                              $savedjob = '1';
                          } else{
                              $savedjob = "0";
                          }

                          if($jobfetch['chatbot'] == 1) {
                            $jobfetch['mode'] = "chatbot";
                        }

                          $total_salary = (int)$sal[0]['salary']+(int)$jobfetch['allowance'];
                         $jobListing[] = [
                              "jobpost_id" => $jobfetch['id'],
                              "job_title" => $jobfetch['jobtitle'],
                              "jobDesc" => $jobfetch['jobPitch'],
                              "job_count"=>$jobfetch['countbytemp'],
                              "salary" => $sal[0]['salary'],
                              //"salary" => (string)$total_salary,
                              "companyName" => $jobfetch['cname'],
                              "cname"=> $cname,
                              "latitude" => $jobfetch['latitude'],
                              "longitude" => $jobfetch['longitude'],
                              "address" => $jobfetch['address'],
                              "jobcount" =>1,
                              "toppicks1" => $dataJobList[0],
                              "toppicks2" => $dataJobList[1],
                              "toppicks3" => $dataJobList[2],
                              "companyPic" => $comPic,
                              "jobpic" => $jobpic,
                              "savedjob" => $savedjob,
                              "mode" => $jobfetch['mode'],
                              "modeurl" => $jobfetch['modeurl'],
                              "sharecount" => $this->getsharecount($jobfetch['id']),
                          ];
                        $x++;
                    }
                    if(count($data["jobList"]) <= 0) {
                        $data["jobList"][] = [];
                    }
                } else{
                    $data["jobList"] = [];
                }

                if($filterData['salarySort'] == 1) {
                   $price = array();
                   foreach ($jobListing as $key => $row) {
                        $price[$key] = $row['salary'];
                   }
                   array_multisort($price, SORT_DESC, $jobListing);
                   $data["jobList"] = $jobListing;
                } else{

                    $price = array();
                    foreach ($jobListing as $key => $row) {
                        $price[$key] = $row['job_count'];
                    }
                    array_multisort($price, SORT_DESC, $jobListing);
                    $data["jobList"] = $jobListing;
                }
                $jobcountfetchs = $this->Jobpost_Model->jobfiltercount_fetch_all($filterData, $userTokenCheck[0]['id'],$expfilter);
                if($jobcountfetchs && $jobcountfetchs[0]['jobcount'] != 0) {
                    
                   foreach ($jobcountfetchs as $jobcountfetch) {
                            
                      $jobTop = $this->Jobpost_Model->job_toppicks($jobcountfetch['id']);
                      
                      if($jobTop) {
                          if(isset($jobTop[0]['picks_id'])){
                              $one = (string)$jobTop[0]['picks_id'];
                              $dataJobList[0] = "$one";
                          } else{
                              $dataJobList[0] = "";
                          }
                          if(isset($jobTop[1]['picks_id'])) {
                              $two = (string)$jobTop[1]['picks_id'];
                              $dataJobList[1] = "$two";
                          } else {
                              $dataJobList[1] = "";
                          }
                          if(isset($jobTop[2]['picks_id'])){
                              $three = (string)$jobTop[2]['picks_id'];
                              $dataJobList[2] = "$three";
                          } else{
                              $dataJobList[2] = "";
                          }
                      } else {
                          $dataJobList[0] = "";
                          $dataJobList[1] = "";
                          $dataJobList[2] = "";
                      }

                      $getmaxsal = array();
                      $getlatlognids = $this->Jobpost_Model->jobfetchbylatlong($jobcountfetch['latitude'], $jobcountfetch['longitude'], $userTokenCheck[0]['id'],$expfilter);
                      foreach($getlatlognids as $getlatlognid) {

                         if($filterData['basicSalary']) {
                            $salaryarrays = $this->Jobpost_Model->jobfetchbymaxsalary1($getlatlognid['id'],$filterData['basicSalary']);
                         } else{
                            $salaryarrays = $this->Jobpost_Model->jobfetchbymaxsalary($getlatlognid['id'],$expfilter);
                         }
                          $getmaxsal[] = (int) $salaryarrays[0]['basesalary'];
                      }
                      $gotsalmax =  max($getmaxsal);
                      $total_salary = (int)$gotsalmax + (int)$jobcountfetch['allowance'];
                      $data["jobcount"][] = [
                              "company_id" => $jobcountfetch['company_id'],
                              "salary" => $gotsalmax,
                              "latitude" => $jobcountfetch['latitude'],
                              "longitude" => $jobcountfetch['longitude'],
                              "address" => $jobcountfetch['address'],
                              "jobcount" => $jobcountfetch['jobcount'],
                              "toppicks1" => $dataJobList[0],
                              "toppicks2" => $dataJobList[1],
                              "toppicks3" => $dataJobList[2]
                          ];
                   } 

                } else{
                    $data['jobcount'] = [];
                }
            }
            $response = ['jobFilters' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function companydetail_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {
            $expmonth = $userTokenCheck[0]['exp_month'];
            $expyear = $userTokenCheck[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
            $company_id = $userData['companyId'];

            $compfetchs = $this->Jobpost_Model->fetch_companydetails_bycompanyid($company_id);

            if($compfetchs) {
                $jobTop = $this->Jobpost_Model->job_fetch_TopPicks($compfetchs[0]['compId']);
                $compDesc = $this->Jobpost_Model->fetch_companyaddress($compfetchs[0]['parent_id']);
                //echo $this->db->last_query();die;

                if($jobTop) {
                    if(isset($jobTop[0]['picks_id'])){
                        $datatoppicks1 = $jobTop[0]['picks_id'];    
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])){
                        $datatoppicks2 =  $jobTop[1]['picks_id'];    
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])){
                        $datatoppicks3 = $jobTop[2]['picks_id'];    
                    } else{
                        $datatoppicks3 = "";
                    }
                    $topickss = $jobTop;
                } else {
                    $datatoppicks1 = "";
                    $datatoppicks2 = "";
                    $datatoppicks3 = "";
                    $topickss = [];
                }

                $companyReviews = $this->Jobpost_Model->fetch_companyRating($compfetchs[0]['compId']);
                if(!empty($companyReviews[0]['average'])){
                    $rating = $companyReviews[0]['average'];
                }else{
                    $rating="";
                }


                $companyPic = $this->Jobpost_Model->fetch_companyPic1($compfetchs[0]['compId']);        
                if(!empty($companyPic) && strlen($companyPic[0]['pic']) > 1 ) {
                    $comPic = $companyPic;
                } else {

                    $siteimgs = $this->Jobpost_Model->fetch_companyPic($compfetchs[0]['parent_id']);
                    if($siteimgs && strlen($siteimgs[0]['companyPic']) > 1 ) {

                        $comPic = [["pic"=>$siteimgs[0]['companyPic']]];
                    
                    } else {

                        $siteimgs = $this->Jobpost_Model->fetch_companyPic($company_id);
                        if($siteimgs  && strlen($siteimgs[0]['companyPic']) > 1 ) {

                            $comPic = [["pic"=>$siteimgs[0]['companyPic']]];
                        
                        } else {

                            $comPic = [["pic"=>'']];
                        }
                    }
                }

                $glassdoor = $this->Jobpost_Model->job_fetch_glass($compfetchs[0]['compId']);
                
                if($glassdoor) {
                    $glassrating = $glassdoor[0]['rating'];
                    $glassdate = $glassdoor[0]['created_at'];
                } else{
                  $glassrating = "0";
                  $glassdate = "0";
                }

                $reviewLast = $this->Jobpost_Model->job_fetch_lastrate($compfetchs[0]['compId']);
                if($reviewLast) { 
                  $reviewLast1 = $reviewLast[0]['feedback'];
                } else{
                  $reviewLast1 = "";
                }
                $data['companyDetail'] = [
                                "companyId" => $compfetchs[0]['compId'],
                                "address" => $compfetchs[0]['address'],
                                "companyName" => $compfetchs[0]['cname'],
                                "companyPic" => $compfetchs[0]['companyPic'],
                                "companyDesc" => $compDesc[0]['companyDesc'],
                                "toppicks1" => "$datatoppicks1",
                                "toppicks2" => "$datatoppicks2",
                                "toppicks3" => "$datatoppicks3",
                                "companyPic" => $comPic,
                                "topPicks" =>$topickss,
                                "rating" => $rating,
                                "review" => $reviewLast1,
                                "glassdoorRating" =>$glassrating,
                                "glassdoorDate" =>$glassdate
                            ];
            } else{
                $data['companyDetail'] = [];
            }
            $jobfetchs = $this->Jobpost_Model->job_fetch_bycompId($company_id,$expfilter);
            //echo $this->db->last_query();die;
            if($jobfetchs) {
                $data["jobList"] = [];
                $data["jobLists"] = [];
                foreach ($jobfetchs as $jobfetch) {
                    $jobApplyfetch = $this->Jobpost_Model->job_fetch_appliedbycompid($jobfetch['id'], $userTokenCheck[0]['id']);

                    if($jobApplyfetch) {

                    } else {
                        $jobTop1 = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                        if($jobTop1) {
                            if(isset($jobTop1[0]['picks_id'])){
                                $datatoppicks1 = $jobTop1[0]['picks_id'];    
                            } else{
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop1[1]['picks_id'])){
                                $datatoppicks2 =  $jobTop1[1]['picks_id'];    
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop1[2]['picks_id'])){
                                $datatoppicks3 = $jobTop1[2]['picks_id'];    
                            } else{
                                $datatoppicks3 = "";
                            }
                        } else {
                            $datatoppicks1 = "";
                            $datatoppicks2 = "";
                            $datatoppicks3 = "";
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['compId']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }
                        $image = $this->Jobpost_Model->job_image($jobfetch['id']);
                        
                        
                        if(!empty($image[0]['pic'])) {
                            $jobpic = $image[0]['pic'];
                        } else{
                            $jobpic = " ";
                        }
                         $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['recruiter_id']);
                         if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }
                        $savedjob = $this->Jobpost_Model->job_saved($jobfetch['id'], $userTokenCheck[0]['id']);
                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }

                        if($jobfetch['chatbot'] == 1) {
                            $jobfetch['mode'] = "chatbot";
                        }

                        $distance = $this->distance($userData['cur_lat'], $userData['cur_long'], $jobfetch['latitude'], $jobfetch['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', '');
                        $data["jobList"][] = [
                                "jobpost_id" => $jobfetch['id'],
                                "job_title" => $jobfetch['jobtitle'],
                                "jobDesc" => $jobfetch['jobDesc'],
                                "salary" => number_format($jobfetch['salary']),
                                "jobexpire" => $jobfetch['jobexpire'],
                                "companyName" => $jobfetch['cname'],
                                "cname" => $cname,
                                "companyId" => $jobfetch['compId'],
                                "toppicks1" => "$datatoppicks1",
                                "toppicks2" => "$datatoppicks2",
                                "toppicks3" => "$datatoppicks3",
                                "companyPic" => $comPic,
                                "jobpic" => $jobpic,
                                "savedjob" => $savedjob,
                                "distance" => $distance,
                                "mode" => $jobfetch['mode'],
                                "modeurl" => $jobfetch['modeurl'],
                                "sharecount"=> $this->getsharecount($jobfetch['id'])
                            ];
                    }
                }

                if(count($data["jobLists"]) < 1) {
                    $data["jobLists"] = $data["jobList"];
                    if(isset($data["jobList"][0])) {
                        $data["jobList"] = $data["jobList"][0];
                    } else{
                        $data["jobList"] = $data["jobList"];
                    }
                } else{
                    $data["jobList"] = [
                            "jobpost_id" => "",
                            "job_title" => "",
                            "jobDesc" => "",
                            "salary" => "",
                            "jobexpire" => "",
                            "companyName" => "",
                            "cname" => "",
                            "companyId" => "",
                            "toppicks1" => "",
                            "toppicks2" => "",
                            "toppicks3" => "",
                            "companyPic" => "",
                            "jobpic" => "",
                            "savedjob" => "",
                            "distance" => "",
                            "mode" => "",
                            "modeurl" => "",
                        ];
                    $data["jobLists"] =[
                        "jobpost_id" => "",
                            "job_title" => "",
                            "jobDesc" => "",
                            "salary" => "",
                            "jobexpire" => "",
                            "companyName" => "",
                            "cname" => "",
                            "companyId" => "",
                            "toppicks1" => "",
                            "toppicks2" => "",
                            "toppicks3" => "",
                            "companyPic" => "",
                            "jobpic" => "",
                            "savedjob" => "",
                            "distance" => "",
                            "mode" => "",
                            "modeurl" => "",
                    ];    
                }

            } else{
                $data["jobList"] = [
                            "jobpost_id" => "",
                            "job_title" => "",
                            "jobDesc" => "",
                            "salary" => "",
                            "jobexpire" => "",
                            "companyName" => "",
                            "cname" => "",
                            "companyId" => "",
                            "toppicks1" => "",
                            "toppicks2" => "",
                            "toppicks3" => "",
                            "companyPic" => "",
                            "jobpic" => "",
                            "savedjob" => "",
                            "distance" => "",
                            "mode" => "",
                            "modeurl" => "",
                        ];

                $data["jobLists"] = [];
            }
            
            $response = ['companydetail' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function mainrecruiterdetail_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {
            $expmonth = $userTokenCheck[0]['exp_month'];
            $expyear = $userTokenCheck[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
            $company_id = $userData['companyId'];

            $compfetchs = $this->Jobpost_Model->fetch_companydetails_bycompanyid($company_id);

            if($compfetchs) {
                $jobTop = $this->Jobpost_Model->job_fetch_TopPicks($compfetchs[0]['compId']);
                $compDesc = $this->Jobpost_Model->fetch_companyaddress($compfetchs[0]['compId']);
                if($jobTop) {
                    if(isset($jobTop[0]['picks_id'])){
                        $datatoppicks1 = $jobTop[0]['picks_id'];    
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])){
                        $datatoppicks2 =  $jobTop[1]['picks_id'];    
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])){
                        $datatoppicks3 = $jobTop[2]['picks_id'];    
                    } else{
                        $datatoppicks3 = "";
                    }
                    $topickss = $jobTop;
                } else {
                    $datatoppicks1 = "";
                    $datatoppicks2 = "";
                    $datatoppicks3 = "";
                    $topickss = [];
                }

                $companyReviews = $this->Jobpost_Model->fetch_companyRating($compfetchs[0]['compId']);
                if(!empty($companyReviews[0]['average'])){
                    $rating = $companyReviews[0]['average'];
                }else{
                    $rating="";
                }
                $companyPic = $this->Jobpost_Model->fetch_companyPic1($compfetchs[0]['compId']);        
                if(!empty($companyPic) && strlen($companyPic[0]['pic']) > 1 ) {
                    $comPic = $companyPic;
                } else {

                    $siteimgs = $this->Jobpost_Model->fetch_companyPic($compfetchs[0]['compId']);
                    if($siteimgs && strlen($siteimgs[0]['companyPic']) > 1 ) {

                        $comPic = [["pic"=>$siteimgs[0]['companyPic']]];
                    
                    } else {
                        $comPic = [["pic"=>'']];
                    }
                }

                $glassdoor = $this->Jobpost_Model->job_fetch_glass($compfetchs[0]['compId']);
                
                if($glassdoor) {
                    $glassrating = $glassdoor[0]['rating'];
                    $glassdate = $glassdoor[0]['created_at'];
                } else{
                  $glassrating = "0";
                  $glassdate = "0";
                }

                $reviewLast = $this->Jobpost_Model->job_fetch_lastrate($compfetchs[0]['compId']);
                if($reviewLast) { 
                  $reviewLast1 = $reviewLast[0]['feedback'];
                } else{
                  $reviewLast1 = "";
                }
                $data['companyDetail'] = [
                                "companyId" => $compfetchs[0]['compId'],
                                "address" => $compfetchs[0]['address'],
                                "companyName" => $compfetchs[0]['cname'],
                                "companyPic" => $compfetchs[0]['companyPic'],
                                "companyDesc" => $compDesc[0]['companyDesc'],
                                "toppicks1" => "$datatoppicks1",
                                "toppicks2" => "$datatoppicks2",
                                "toppicks3" => "$datatoppicks3",
                                "companyPic" => $comPic,
                                "topPicks" =>$topickss,
                                "rating" => $rating,
                                "review" => $reviewLast1,
                                "glassdoorRating" =>$glassrating,
                                "glassdoorDate" =>$glassdate
                            ];
            } else{
                $data['companyDetail'] = [];
            }
            $jobfetchs = $this->Jobpost_Model->job_fetch_bycompId($company_id,$expfilter);
            //echo $this->db->last_query();die;
            if($jobfetchs) {
                $data["jobList"] = [];
                $data["jobLists"] = [];
                foreach ($jobfetchs as $jobfetch) {
                    $jobApplyfetch = $this->Jobpost_Model->job_fetch_appliedbycompid($jobfetch['id'], $userTokenCheck[0]['id']);

                    if($jobApplyfetch) {

                    } else {
                        /*$jobsal = $this->Jobpost_Model->getExpJob1($jobfetch['id'],$expfilter);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = " ";
                        }*/

                        $jobTop1 = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                        if($jobTop1) {
                            if(isset($jobTop1[0]['picks_id'])){
                                $datatoppicks1 = $jobTop1[0]['picks_id'];    
                            } else{
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop1[1]['picks_id'])){
                                $datatoppicks2 =  $jobTop1[1]['picks_id'];    
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop1[2]['picks_id'])){
                                $datatoppicks3 = $jobTop1[2]['picks_id'];    
                            } else{
                                $datatoppicks3 = "";
                            }
                        } else {
                            $datatoppicks1 = "";
                            $datatoppicks2 = "";
                            $datatoppicks3 = "";
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['compId']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }
                        $image = $this->Jobpost_Model->job_image($jobfetch['id']);
                        
                        
                        if(!empty($image[0]['pic'])) {
                            $jobpic = $image[0]['pic'];
                        } else{
                            $jobpic = " ";
                        }
                         $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['recruiter_id']);
                         if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }
                        $savedjob = $this->Jobpost_Model->job_saved($jobfetch['id'], $userTokenCheck[0]['id']);
                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }

                        if($jobfetch['chatbot'] == 1) {
                            $jobfetch['mode'] = "chatbot";
                        }

                        $distance = $this->distance($userData['cur_lat'], $userData['cur_long'], $jobfetch['latitude'], $jobfetch['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', '');
                        $data["jobList"][] = [
                                "jobpost_id" => $jobfetch['id'],
                                "job_title" => $jobfetch['jobtitle'],
                                "jobDesc" => $jobfetch['jobDesc'],
                                "salary" => number_format($jobfetch['salary']),
                                "jobexpire" => $jobfetch['jobexpire'],
                                "companyName" => $jobfetch['cname'],
                                "cname" => $cname,
                                "companyId" => $jobfetch['compId'],
                                "toppicks1" => "$datatoppicks1",
                                "toppicks2" => "$datatoppicks2",
                                "toppicks3" => "$datatoppicks3",
                                "companyPic" => $comPic,
                                "jobpic" => $jobpic,
                                "savedjob" => $savedjob,
                                "distance" => $distance,
                                "mode" => $jobfetch['mode'],
                                "modeurl" => $jobfetch['modeurl'],
                                "sharecount"=> $this->getsharecount($jobfetch['id'])
                            ];
                    }
                }

                if(count($data["jobLists"]) < 1) {
                    $data["jobLists"] = $data["jobList"];
                    if(isset($data["jobList"][0])) {
                        $data["jobList"] = $data["jobList"][0];
                    } else{
                        $data["jobList"] = $data["jobList"];
                    }
                } else{
                    $data["jobList"] = [
                            "jobpost_id" => "",
                            "job_title" => "",
                            "jobDesc" => "",
                            "salary" => "",
                            "jobexpire" => "",
                            "companyName" => "",
                            "cname" => "",
                            "companyId" => "",
                            "toppicks1" => "",
                            "toppicks2" => "",
                            "toppicks3" => "",
                            "companyPic" => "",
                            "jobpic" => "",
                            "savedjob" => "",
                            "distance" => "",
                            "mode" => "",
                            "modeurl" => "",
                        ];
                    $data["jobLists"] =[
                        "jobpost_id" => "",
                            "job_title" => "",
                            "jobDesc" => "",
                            "salary" => "",
                            "jobexpire" => "",
                            "companyName" => "",
                            "cname" => "",
                            "companyId" => "",
                            "toppicks1" => "",
                            "toppicks2" => "",
                            "toppicks3" => "",
                            "companyPic" => "",
                            "jobpic" => "",
                            "savedjob" => "",
                            "distance" => "",
                            "mode" => "",
                            "modeurl" => "",
                    ];    
                }

            } else{
                $data["jobList"] = [
                            "jobpost_id" => "",
                            "job_title" => "",
                            "jobDesc" => "",
                            "salary" => "",
                            "jobexpire" => "",
                            "companyName" => "",
                            "cname" => "",
                            "companyId" => "",
                            "toppicks1" => "",
                            "toppicks2" => "",
                            "toppicks3" => "",
                            "companyPic" => "",
                            "jobpic" => "",
                            "savedjob" => "",
                            "distance" => "",
                            "mode" => "",
                            "modeurl" => "",
                        ];

                $data["jobLists"] = [];
            }
            
            $response = ['companydetail' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function jobtitlecache_post() {
        $cacheData = $this->input->post();
        $data = ["token" => $cacheData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);
        
        if($userTokenCheck) {
            
            if(!empty($cacheData['title'])) {
              $cachecheck = $this->Jobpost_Model->jobtitlecache_check($cacheData['title'], $userTokenCheck[0]['id']);
              
              if(!$cachecheck) {
                  $data = ["user_id" => $userTokenCheck[0]['id'], "title"=> $cacheData['title']];
                  $this->Jobpost_Model->jobtitlecache_insert($data);
              }
            }
            $cacheGet = $this->Jobpost_Model->jobtitlecache_fetch($userTokenCheck[0]['id']);
            if($cacheGet) {

            } else{
              $cacheGet = [];
            }
            $response = ['jobtitlecache' => $cacheGet, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function keywordcache_post() {
        $cacheData = $this->input->post();
        $data = ["token" => $cacheData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);
        
        if($userTokenCheck) {
            
            if(!empty($cacheData['keyword'])) {
              $cachecheck = $this->Jobpost_Model->keywordcache_check($cacheData['keyword'], $userTokenCheck[0]['id']);
              
              if(!$cachecheck) {
                  $data = ["user_id" => $userTokenCheck[0]['id'], "keyword"=> $cacheData['keyword']];
                  $this->Jobpost_Model->keywordcache_insert($data);
              }
            }
            $cacheGet = $this->Jobpost_Model->keywordcachee_fetch($userTokenCheck[0]['id']);
            if($cacheGet) {

            } else{
              $cacheGet = [];
            }
            $response = ['keywordcache' => $cacheGet, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function companySchedule_post() {
        $jobData = $this->input->post();
        $data = ["token" => $jobData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {  
            $getDays = $this->Jobpost_Model->getCompDetails($jobData['companyId']);
            
            $days1 = $getDays[0]['dayfrom'];
            $days2 = $getDays[0]['dayto'];
            $z=1;

            for($i=$days1; $i<=$days2; $i++) {
                if($i == 1) {
                    $dayname = "Monday";
                } else if($i == 2) {
                    $dayname = "Tuesday";
                } else if($i == 3) {
                    $dayname = "Wednesday";
                } else if($i == 4) {
                    $dayname = "Thursday";
                } else if($i == 5) {
                    $dayname = "Friday";
                } else if($i == 6) {
                    $dayname = "Saturday";
                } else if($i == 7) {
                    $dayname = "Sunday";
                }
                $data['days'][] = $dayname;
              $z++;
            }

            $time1 = $getDays[0]['from_time'];
            $time11 = explode(":", $time1);
            $time111 = $time11[0];
            $time2 = $getDays[0]['to_time'];
            $time22 = explode(":", $time2);
            $time222 = $time22[0];

            $data['timeFrom'] = $time111;
            $data['timeTo'] = $time222;
            $response = ['companySchedule' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function jobtitlelist_post() {
        $jobData = $this->input->post();
        $data = ["token" => $jobData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);
        
        if($userTokenCheck) {  
            $jobtitle = $this->Common_Model->getCompJobTitle();
            $response = ['jobtitlelist' => $jobtitle, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function saveJob_post() {
        $job = $this->input->post();
        $data = ["token" => $job['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {
            $this->form_validation->set_rules('jobpost_id', 'job post id', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $errors = $this->form_validation->error_array();
                $error = current($errors);
                $response = ['message' => $error, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);

            } else {
                $data1 = ['user_id' => $userTokenCheck[0]['id'], 'jobpost_id'=> $job['jobpost_id']];
                $jobSaveds = $this->Jobpost_Model->getstatusSavedJob($data1);
                if(empty($jobSaveds)){
                    $jobsave = [
                            "jobpost_id" => $job['jobpost_id'],
                            "user_id" => $userTokenCheck[0]['id'],
                        ];
                    $this->Jobpost_Model->savejob_insert($jobsave);
                    $response = ['saveJob' => $jobsave, 'status' => "SUCCESS"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }else{
                    $response = ['message' => 'already saved', 'status' => "SUCCESS"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
                
            }
        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function deletesaveJob_post() {
        $job = $this->input->post();
        $data = ["token" => $job['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {
            $this->form_validation->set_rules('jobpost_id', 'job post id', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $errors = $this->form_validation->error_array();
                $error = current($errors);
                $response = ['message' => $error, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);

            } else {

                $this->Jobpost_Model->savejob_delete($job['jobpost_id'],$userTokenCheck[0]['id']);
                $response = ['message' => "Deleted Successfully", 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        } }else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function saveJobListing_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {
            $jobSaveds = $this->Jobpost_Model->jobSaved_fetch_byuserid($userTokenCheck[0]['id']);
            if($jobSaveds) {

                foreach ($jobSaveds as $jobSaved) {
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($jobSaved['company_id']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobSaved['company_id']);
                    $appliedStatus = $this->Jobpost_Model->applied_status_fetch($jobSaved['id'], $userTokenCheck[0]['id']);
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobSaved['id']);
                        
                        if($jobTop) {
                            if(isset($jobTop[0]['picks_id'])){
                                $one = (string)$jobTop[0]['picks_id'];
                                $dataJobList[0] = "$one";
                            } else{
                                $dataJobList[0] = "";
                            }
                            if(isset($jobTop[1]['picks_id'])){
                                $two = (string)$jobTop[1]['picks_id'];
                                $dataJobList[1] = "$two";
                            } else{
                                $dataJobList[1] = "";
                            }
                            if(isset($jobTop[2]['picks_id'])){
                                $three = (string)$jobTop[2]['picks_id'];
                                $dataJobList[2] = "$three";
                            } else{
                                $dataJobList[2] = "";
                            }
                        } else {
                            $dataJobList[0] = "";
                            $dataJobList[1] = "";
                            $dataJobList[2] = "";
                        }
                    if($appliedStatus) {
                        $jobstatus = $appliedStatus[0]['status'];
                    } else{
                        $jobstatus = 0;
                    }
                    
                    $image = $this->Jobpost_Model->job_image($jobSaved['id']);
                        
                        
                    if(!empty($image[0]['pic'])) {
                        $jobpic = $image[0]['pic'];
                    } else{
                        $jobpic = " ";
                    }
                    
                    $savedjob = $this->Jobpost_Model->job_saved($jobSaved['id'], $userTokenCheck[0]['id']);
                    //print_r($savedjob);
                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }
                    
                     $jobsal = $this->Jobpost_Model->getExpJob($jobSaved['id']);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = '0';
                        }
                        $companyname = $this->Jobpost_Model->company_name_fetch($jobSaved['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        if($jobSaved['chatbot'] == 1) {
                            $jobSaved['mode'] = "chatbot";
                        }

                    $distance = $this->distance($userData['cur_lat'], $userData['cur_long'], $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');    
                    $data["jobList"][] = [
                            "jobpost_id" => $jobSaved['id'],
                            "job_title" => $jobSaved['jobtitle'],
                            "jobDesc" => $jobSaved['jobDesc'],
                            "salary" => number_format($basicsalary),
                            "status" => $jobstatus,
                            "company_id" =>$jobSaved['company_id'],
                            "jobexpire" =>$jobSaved['jobexpire'],
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "toppicks1" => $dataJobList[0],
                            "toppicks2" => $dataJobList[1],
                            "toppicks3" => $dataJobList[2],
                            "jobpic" => $jobpic,
                            "savedjob" => $savedjob,
                            "distance" => $distance,
                            "mode" =>$jobSaved['mode'],
                            "modeurl" =>$jobSaved['modeurl'],
                            "sharecount" => $this->getsharecount($jobSaved['id']),
                        ];
                }
            } else {
                $data["jobList"] = [];
            }
            $fetchnotifications = $this->Candidate_Model->fetchnotifications($userTokenCheck[0]['id']);
            $fetchpromonotifications = $this->Candidate_Model->fetchpromonotifications($userTokenCheck[0]['id']);
            $response = ['saveJobListing' => $data, 'notification_count' => count($fetchnotifications)+count($fetchpromonotifications), 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function hotjobListings_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {
            $expmonth = $userTokenCheck[0]['exp_month'];
            $expyear = $userTokenCheck[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
            $hotjobData = $this->Jobpost_Model->hotjob_fetch_latlong($userTokenCheck[0]['id'],$expfilter);
            $data1 = array();
            $data1["hotjobss"]=[];
            foreach ($hotjobData as $hotjobsData) {

                $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
              
                if(!empty($jobTop) && count($jobTop)>1) {
                      //echo "</pre>";    
                    if(isset($jobTop[0]['picks_id'])){
                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])){
                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])){
                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }
                    
                      $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['id']);

                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }

                         $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);

                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = 0;
                        }

                        $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                        if(isset($companydetail1[0]['address']))
                        {
                            $companyaddress=$companydetail1[0]['address'];
                        }
                        else
                        {
                            $companyaddress='';
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                        
                        
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }
                        $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);


                        if(isset($jobImage[0]['pic']))
                        {
                            $jobImageData=$jobImage[0]['pic'];
                        }
                        else
                        {
                            $jobImageData=$comPic;
                        }

                        $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($userData['cur_lat'], $userData['cur_long'],$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', ''); 

                        $data1["hotjobss"][] = [
                        "jobpost_id" => $hotjobsData['id'],
                        "comapnyId" =>$hotjobsData['compId'],
                        "job_title" => $hotjobsData['jobtitle'],
                        "jobDesc" => $hotjobsData['jobDesc'],
                        "salary" => $hotjobsData['salary'],
                        "companyName"=> $companydetail[0]["cname"],
                        "cname"=> $cname,
                        "companyAddress"=>$companyaddress,
                        "toppicks1" => $datatoppicks1,
                        "toppicks2" => $datatoppicks2,
                        "toppicks3" => $datatoppicks3,
                        "job_image" =>$jobImageData,
                        "latitude"=> $hotjobsData['latitude'], 
                        "longitude"=>$hotjobsData['longitude'],
                        "jobexpire"=> $hotjobsData['jobexpire'],
                        "companyPic"=> $comPic,
                        "savedjob" =>$savedjob,
                        "boostjob" =>$hotjobsData['boost_status'],
                        "distance" => $distance
                        ];

                }
            }
            $fetchnotifications = $this->Candidate_Model->fetchnotifications($userTokenCheck[0]['id']);
            $fetchpromonotifications = $this->Candidate_Model->fetchpromonotifications($userTokenCheck[0]['id']);
            $response = ['hotjobslist' => $data1,'notification_count' =>count($fetchnotifications)+count($fetchpromonotifications), 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function notificationlist_post() {
        $jobData = $this->input->post();
        $data = ["token" => $jobData['token']];
        $userTokenCheck = $this->User_Model->token_match1($data);
        
        if($userTokenCheck) {

                $data2 = ["badge_count"=>0];
                $notificationUpdate = $this->Candidate_Model->notificationbadge_update($userTokenCheck[0]['user_id'],$data2);
                
                $notification_list = $this->Common_Model->getnotification($userTokenCheck[0]['user_id']);

                $fetchpromonotifications = $this->Candidate_Model->fetchpromonotifications($userTokenCheck[0]['user_id']);
                $fetchjobnotifications = $this->Candidate_Model->fetchjobnotifications($userTokenCheck[0]['user_id']);
                $x=0;
                
                if(!empty($notification_list)) {

                    foreach ($notification_list as $notification_lists) {
                        $companyName = $this->Jobpost_Model->company_detail_fetch($notification_lists['recruiter_id']);
                        $job_detail = $this->Jobpost_Model->job_fetch_appliedid($notification_lists['jobapp_id']);
                        $reviews = $this->Jobpost_Model->review_listsbyuser($notification_lists['recruiter_id'],$userTokenCheck[0]['id']);
                        $story = $this->Jobpost_Model->story_listsbyuser($notification_lists['recruiter_id'],$userTokenCheck[0]['id']);
                        if($reviews){
                            $reviewstatus = '1';
                        }else{
                            $reviewstatus = '0';
                        }
                        if($story){
                            $storystatus = '1';
                        }else{
                            $storystatus = '0';
                        }

                        if(!empty($companyName[0]['cname'])){
                            $companyName[0]['cname']=$companyName[0]['cname'];
                        }else{
                            $companyName[0]['cname']='';
                        }
                        if(!empty($job_detail[0]['jobpost_id'])){
                            $job_detail[0]['jobpost_id'] = $job_detail[0]['jobpost_id'];
                        }else{
                            $job_detail[0]['jobpost_id']='';
                        }
                        $notification_lists1[$x] = [
                              "id" => $notification_lists['id'],
                              "notification" => $notification_lists['notification'],
                              "status" => $notification_lists['status'],
                              "recruiter_id" => $notification_lists['recruiter_id'],          
                              "job_status" =>   $notification_lists['job_status'],
                              "companyname" =>  $companyName[0]['cname'],
                              "jobpost_id" =>  $job_detail[0]['jobpost_id'],
                              "date" => date("F d, Y H:i:s", strtotime($notification_lists['createdon'])),
                              "reviewstatus" => $reviewstatus,
                              "storystatus" => $storystatus
                            ];
                        $x++;
                    }
                }else{
                    $notification_lists1=[];
                }  
                
                $response = ['notificationlist' => $notification_lists1, "promo_count" => (string)count($fetchpromonotifications), "job_count" => (string)count($fetchjobnotifications), 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);

            //}
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function allnotificationcount_post() {
        
        $jobData = $this->input->post();
        $data = ["token" => $jobData['token']];
        $userTokenCheck = $this->User_Model->token_match1($data);
        
        if($userTokenCheck) {

            $fetchjobnotifications = $this->Candidate_Model->fetchjobnotifications($userTokenCheck[0]['user_id']);
            $fetchpromonotifications = $this->Candidate_Model->fetchpromonotifications($userTokenCheck[0]['user_id']);

            $response = ["promo_count" => (string)count($fetchpromonotifications), "job_count"=>(string)count($fetchjobnotifications), 'status' => "SUCCESS"];
            
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function promonotificationlist_post() {
        $jobData = $this->input->post();
        $data = ["token" => $jobData['token']];
        $userTokenCheck = $this->User_Model->token_match1($data);
        
        if($userTokenCheck) {
            
                $data2 = ["badge_count"=>0];
                $notificationUpdate = $this->Candidate_Model->promonotificationbadge_update($userTokenCheck[0]['user_id'],$data2);
                $notification_list = $this->Common_Model->getpromonotification($userTokenCheck[0]['user_id']);
                //print_r($notification_list);die;

                $fetchnotifications = $this->Candidate_Model->fetchnotifications($userTokenCheck[0]['user_id']);
                $fetchjobnotifications = $this->Candidate_Model->fetchjobnotifications($userTokenCheck[0]['user_id']);

                $x=0;
                if(!empty($notification_list)) {
                    foreach ($notification_list as $notification_lists) {
                        $notification_lists1[$x] = [
                              "id" => $notification_lists['id'],
                              "title" => urldecode($notification_lists['title']),
                              "message" => urldecode($notification_lists['message']),
                              "date" => date("F d, Y H:i:s", strtotime($notification_lists['added_at'])),
                            ];
                        $x++;
                    }
                }else{
                    $notification_lists1=[];
                }  
                
                $response = ['promonotificationlist' => $notification_lists1, "notification_count"=>(string)count($fetchnotifications), "job_count"=>(string)count($fetchjobnotifications), 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);

            //}
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function jobnotificationlist_post() {
        $jobData = $this->input->post();
        $data = ["token" => $jobData['token']];
        $userTokenCheck = $this->User_Model->token_match1($data);
        $notification_lists1 = array();

        if($userTokenCheck) {

                $data2 = ["badge_count"=>0];
                $notificationUpdate = $this->Candidate_Model->notificationbadge_updatejob($userTokenCheck[0]['user_id'],$data2);

                $notification_list = $this->Common_Model->getnotificationjob($userTokenCheck[0]['user_id']);
                $fetchnotifications = $this->Candidate_Model->fetchnotifications($userTokenCheck[0]['user_id']);
                $fetchpromonotifications = $this->Candidate_Model->fetchpromonotifications($userTokenCheck[0]['user_id']);
                
                $x=0;
                
                if(!empty($notification_list)) {

                    foreach ($notification_list as $notification_lists) {

                        $main_notify = $this->Common_Model->getmainnotificationjob($notification_lists['notify_id']);

                        $companyName = $this->Jobpost_Model->company_detail_fetch($main_notify[0]['recruiter_id']);
                        $job_detail = $this->Jobpost_Model->job_fetch_appliedid($main_notify[0]['job_id']);

                        if(!empty($companyName[0]['cname'])) {
                            $companyName[0]['cname']=$companyName[0]['cname'];
                        } else {
                            $companyName[0]['cname']='';
                        }

                        if(!empty($job_detail[0]['jobpost_id'])) {
                            $job_detail[0]['jobpost_id'] = $job_detail[0]['jobpost_id'];
                        } else {
                            $job_detail[0]['jobpost_id']='';
                        }
                        //if($notification_lists['id'] == 9923192) { } else {
                            $notification_lists1[] = [
                              "id" => $notification_lists['id'],
                              "notification" => urldecode($main_notify[0]['notification']),
                              "status" => $notification_lists['read_status'],
                              "recruiter_id" => $main_notify[0]['recruiter_id'],
                              "companyname" =>  $companyName[0]['cname'],
                              "jobpost_id" =>  $main_notify[0]['job_id'],
                              "date" => date("F d, Y H:i:s", strtotime($notification_lists['created_at'])),
                            ];
                        //}

                        $x++;
                    }
                } else {
                    $notification_lists1=[];
                }
                $response = ['notificationlist' => $notification_lists1, "notification_count"=>(string)count($fetchnotifications), "promo_count" => (string)count($fetchpromonotifications), 'status' => "SUCCESS"];

                $this->set_response($response, REST_Controller::HTTP_CREATED);

        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function addstory_post() {
        $userData = $this->input->post();
        $this->form_validation->set_rules('token', 'token', 'trim|required');
        $this->form_validation->set_rules('story', 'story', 'trim|required');
        $this->form_validation->set_rules('recruiter_id', 'recruiter_id', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {
            $data = ["token" => $userData['token']];
            $userTokenCheck1 = $this->User_Model->token_match1($data);
            if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck) {
                  $data = ["user_id" => $userTokenCheck[0]['id'],"recruiter_id"=>$userData['recruiter_id'], "story"=> $userData['story'] ];
                  $story_inserted =  $this->Jobpost_Model->insert_story($data);
                  if($story_inserted){
                    $response = ['message' => 'Story added Successfully', 'status' => "SUCCESS"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
                    
                 
            } }else {
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    public function storylist_post() {
        $jobData = $this->input->post();
        $data = ["token" => $jobData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {  
            $story_list = $this->Jobpost_Model->story_lists();
            if(!empty($story_list)){
                $x=0;
                foreach ($story_list as $story_lists) {
                    $like_story = $this->Jobpost_Model->liked_stories_count($story_lists['id']);
                    $story_list_count = $this->Jobpost_Model->comment_lists($story_lists['id']);
                    $like_story_status = $this->Jobpost_Model->liked_stories_status($story_lists['id'],$userTokenCheck[0]['id']);
                    if(!empty($like_story_status[0]['status'])){
                        $like_status = $like_story_status[0]['status'];

                    }else{
                        $like_status = "";
                    }
                    $story_lists1[$x] = [
                              "story_id" => $story_lists['id'],
                              "user_id" => $userTokenCheck[0]['id'],
                              "date" => date("y-m-d",strtotime($story_lists['createdon'])),
                              "story" => $story_lists['story'],
                              "recruiter_id" => $story_lists['recruiter_id'],
                              "name" => $story_lists['name'],
                              "profilePic" => $story_lists['profilePic'],
                              "like_count" =>   count($like_story),
                              "comment_count" => count($story_list_count),                 
                              "like_status" =>   $like_status ,              
                              "companyName" =>   $story_lists['cname']             

                          ];
                $x++;
                }
            }else{
                $story_lists1 = [];
            }
            
            
            $response = ['storylist' => $story_lists1, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function likestory_post() {
        $userData = $this->input->post();
        $this->form_validation->set_rules('token', 'token', 'trim|required');
        $this->form_validation->set_rules('story_id', 'story_id', 'trim|required');
        $this->form_validation->set_rules('status', 'status', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {
            $data = ["token" => $userData['token']];
            $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck) {
                $data['liked_stories'] = $this->Jobpost_Model->liked_stories($userData['story_id'], $userTokenCheck[0]['id']);
                if(empty($data['liked_stories'])){
                    $data = ["user_id" => $userTokenCheck[0]['id'],"story_id"=>$userData['story_id'], "status"=> $userData['status'] ];
                      $story_like =  $this->Jobpost_Model->like_story($data);
                      if($story_like){
                        $response = ['message' => 'Story updated Successfully', 'status' => "SUCCESS"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                }else{
                    $data = ["status"=> $userData['status'] ];
                      $story_like =  $this->Jobpost_Model->unlike_story($userTokenCheck[0]['id'],$userData['story_id'],$data);
                      if($story_like){
                        $response = ['message' => 'Story updated Successfully', 'status' => "SUCCESS"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                }
                 
            } }else {
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    public function commentstory_post() {
        $userData = $this->input->post();
        $this->form_validation->set_rules('token', 'token', 'trim|required');
        $this->form_validation->set_rules('story_id', 'story_id', 'trim|required');
        $this->form_validation->set_rules('comment', 'comment', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {
            $data = ["token" => $userData['token']];
            //$userTokenCheck = $this->User_Model->token_match($data);
            $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
           // print_r($userTokenCheck);die;
            if($userTokenCheck) {

                $single_story = $this->Jobpost_Model->fetchstory($userData['story_id']);
                //print_r($single_story);die;
                if($single_story[0]['user_id'] != $userTokenCheck[0]['id']){
                    $msg ="You have a new comment on your story";
                    $user_data = $this->User_Model->user_single($single_story[0]['user_id']);
                    //print_r($user_data);die;
                    $fetchnotifications = $this->Candidate_Model->fetchnotifications($user_data[0]['id']);
                    $fetchpromonotifications = $this->Candidate_Model->fetchpromonotifications($user_data[0]['id']);
                   // print_r($user_data);die;
                    $data = ["user_id" => $userTokenCheck[0]['id'],"story_id"=>$userData['story_id'], "comments"=> $userData['comment'] ];
                      $story_commnet =  $this->Jobpost_Model->add_storycomment($data);
                    if($user_data[0]['notification_status']=='1'){
                        $tokendata = ['user_id'=>$user_data[0]['id']];
                        $userTokenCheck = $this->User_Model->user_token_check($tokendata);
                        //print_r($userTokenCheck);die;
                        foreach ($userTokenCheck as $usertoken) {
                       if($usertoken['device_type']=="android"){
                            $res1 = $this->push_notification_android_customer($usertoken['device_token'],"New Comment", $msg,'comment',$userData['story_id'],count($fetchnotifications)+count($fetchpromonotifications));
                        }else if($usertoken['device_type']=="ios"){
                            $this->pushiphonforcomment(array($usertoken['device_token'],$msg,'comment','New Comment',$userData['story_id'],count($fetchnotifications)+count($fetchpromonotifications)));
                        
                         } }
                    }
                    
                    
                }
                    if($story_commnet){
                        $response = ['message' => 'Comment added Successfully', 'status' => "SUCCESS"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                 
            }} else {
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    public function commentlist_post() {
        $jobData = $this->input->post();
        $data = ["token" => $jobData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {  
             $single_story = $this->Jobpost_Model->fetch_singlestory($jobData['story_id']);
             $liked_stories = $this->Jobpost_Model->liked_stories_count($jobData['story_id']);
             $story_list = $this->Jobpost_Model->comment_lists($jobData['story_id']);
             $like_story_status = $this->Jobpost_Model->liked_stories_status($jobData['story_id'],$userTokenCheck[0]['id']);
                if(!empty($like_story_status[0]['status'])){
                    $like_status = $like_story_status[0]['status'];

                }else{
                    $like_status = "";
                }
             //print_r($liked_stories);die;
             $data1['story_list'] = ["story" => $single_story[0]['story'],
                                        "date" => date("y-m-d",strtotime($single_story[0]['createdon'])),
                                        "name" => $single_story[0]['name'],
                                        "profilePic" => $single_story[0]['profilePic'],
                                        "comment_count" => count($story_list),
                                        "like_count" => count($liked_stories),
                                        "like_status" => $like_status
                                    ];
            
            $x=0;
            foreach ($story_list as $story_lists) {
                $data1['story_comments'][$x] = [
                          "comment_id" => $story_lists['id'],
                          "date" => date("y-m-d",strtotime($story_lists['createdon'])),
                          "comments" => $story_lists['comments'],
                          "name" => $story_lists['name'],
                          "profilePic" => $story_lists['profilePic']                    

                      ];
            $x++;
            }
            $response = ['commentlist' => $data1, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } }else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function readnotification_post() {
        $userData = $this->input->post();
        $this->form_validation->set_rules('token', 'token', 'trim|required');
        $this->form_validation->set_rules('notification_id', 'notification_id', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {
            $data = ["token" => $userData['token']];
            $userTokenCheck = $this->User_Model->token_match1($data);
        
            if($userTokenCheck) {
                if($userData['type'] == "jobnotify") {

                    $data = ["read_status"=>'1', "badge_count"=>0];
                    $notification_status =  $this->Jobpost_Model->read_jobnotification($data,$userData['notification_id']);

                    $fetchnotifications = $this->Candidate_Model->fetchnotifications($userTokenCheck[0]['id']);
                    $fetchpromonotifications = $this->Candidate_Model->fetchpromonotifications($userTokenCheck[0]['id']);

                    if($notification_status){
                        $response = ['message' => 'read Successfully','notification_count'=>count($fetchnotifications)+count($fetchpromonotifications), 'status' => "SUCCESS"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                } else {
                    $data = ["status"=>'1'];
                    $notification_status =  $this->Jobpost_Model->read_notification($data,$userData['notification_id']);

                    $fetchnotifications = $this->Candidate_Model->fetchnotifications($userTokenCheck[0]['id']);
                    $fetchpromonotifications = $this->Candidate_Model->fetchpromonotifications($userTokenCheck[0]['id']);
                    if($notification_status){
                        $response = ['message' => 'read Successfully','notification_count'=>count($fetchnotifications)+count($fetchpromonotifications), 'status' => "SUCCESS"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                }
            } else {
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    public function addreviews_post() {
        $userData = $this->input->post();
        $this->form_validation->set_rules('token', 'token', 'trim|required');
        $this->form_validation->set_rules('rating', 'rating', 'trim|required');
        $this->form_validation->set_rules('recommend', 'recommend', 'trim|required');
        $this->form_validation->set_rules('feedback', 'feedback', 'trim|required');
        $this->form_validation->set_rules('recruiter_id', 'recruiter_id', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {
            $data = ["token" => $userData['token']];
            $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck) {
                    $data = ["user_id" => $userTokenCheck[0]['id'],"recruiter_id"=> $userData['recruiter_id'], "rating"=>$userData['rating'],"recommend"=>$userData['recommend'], "feedback"=>$userData['feedback'] ];
                  $review_inserted =  $this->Jobpost_Model->insert_review($data);
                  if($review_inserted){
                    $response = ['feedback' => $userData['feedback'], 'status' => "SUCCESS"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
            }} else {
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }
    
    public function reviewlist_post() {
        $jobData = $this->input->post();
        $data = ["token" => $jobData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {  
            $review_list = $this->Jobpost_Model->review_lists($jobData['recruiter_id']);
            
            if($review_list){
                $x=0;
                foreach ($review_list as $review_lists) {
                    $review_list1[$x] = [
                          "rating" => $review_lists['rating'],
                          "recommend" => $review_lists['recommend'],
                          "date" => date("y-m-d",strtotime($review_lists['created_at'])),
                          "feedback" => $review_lists['feedback'],
                          "name" => $review_lists['name'],
                          "profilePic" => $review_lists['profilePic']                    

                      ];
                    $x++;
                }  
            }else{
                $review_list1=[];
            }
            $response = ['reviewlist' => $review_list1, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function levellist_get() {

        $level_list = $this->Jobpost_Model->level_lists();
        $level_array = array();
        if($level_list){
            
            foreach ($level_list as $level_lists) {
                
                $level_array[] = [
                      "id" => $level_lists['id'],
                      "level" => $level_lists['level']                    

                ];
            }  
        }
        $response = ['levellist' => $level_array, 'status' => "SUCCESS"];
        $this->set_response($response, REST_Controller::HTTP_CREATED);
    }

    public function categorylist_post() {
        $jobData = $this->input->post();
        $data = ["token" => $jobData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {  
            $category_list = $this->Jobpost_Model->category_lists();
            
            if($category_list){
                $x=0;
              foreach ($category_list as $category_lists) {
                $category_list1[$x] = [
                          "id" => $category_lists['id'],
                          "category" => $category_lists['category']                    

                      ];
            $x++;
            }  
            }else{
                $category_list1=[];
            }
            
            $response = ['categorylist' => $category_list1, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function subcategorylist_post() {
        $jobData = $this->input->post();
        $data = ["token" => $jobData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {  
            $subcategory_list = $this->Jobpost_Model->subcategory_listsbycatid($jobData['category']);
            
            if($subcategory_list){
                $x=0;
              foreach ($subcategory_list as $subcategory_lists) {
                $subcategory_list1[$x] = [
                          "id" => $subcategory_lists['id'],
                          "category" => $subcategory_lists['category_id'],                    
                          "subcategory" => $subcategory_lists['subcategory']                    

                      ];
            $x++;
            }  
            }else{
                $subcategory_list1=[];
            }
            
            $response = ['subcategorylist' => $subcategory_list1, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } }else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function filtersubcategorylist_post() {
        $jobData = $this->input->post();
        $subcategory_list = $this->Jobpost_Model->filtersubcategory_listsbycatid($jobData['catids']);
        if($subcategory_list){
            $x=0;
            foreach ($subcategory_list as $subcategory_lists) {
                $subcategory_list1[$x] = [
                      "id" => $subcategory_lists['id'],
                      "category" => $subcategory_lists['category_id'],                    
                      "subcategory" => $subcategory_lists['subcategory']                    

                  ];
                $x++;
            }  
        }else{
            $subcategory_list1=[];
        }
        
        $response = ['filtersubcategorylist' => $subcategory_list1, 'status' => "SUCCESS"];
        $this->set_response($response, REST_Controller::HTTP_CREATED);
    }

    public function statuspopup_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        
        if($userTokenCheck1) {
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            $now = time();
        
            if($userTokenCheck) {
                $jobApplyfetch = $this->Jobpost_Model->job_fetch_applied($userTokenCheck[0]['id']);

                if($jobApplyfetch) {
                        
                    foreach ($jobApplyfetch as $jobfetch) {

                        $reconciliation =  $this->Jobpost_Model->job_fetch_appliedCheck11($jobfetch['jobpost_id'], $userTokenCheck[0]['id']);
                        $companyName = $this->Jobpost_Model->fetch_companyName($jobfetch['recruiter_id']);

                        if($reconciliation) {

                            if($reconciliation[0]['status'] == 2) {
                                $currentDate = date("y-m-d");
                                $getDate = $reconciliation[0]['updated_at'];
                                $diff = strtotime($currentDate) - strtotime($getDate);
                                $calDays = abs(round($diff / 86400));

                                if($calDays == 3) {
                                //if($calDays > 0) {
                                    $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
                                    $userdata = $this->User_Model->token_match($tokendata);
                                    $notifyMsg1 = "We know you’ve been waiting, and we’re excited to hear the latest on your application for Job Title at Company Name. Tell Us!";

                                    $data1["jobList"][] = [
                                        "jobpost_id" => $jobfetch['jobpost_id'],
                                        "job_title" => $jobfetch['jobtitle'],
                                        "companyName" => $companyName[0]['cname'],
                                        "companyId" => $jobfetch['recruiter_id'],
                                        "msg" => "open popup",
                                        "type"=>"waiting",
                                        "name" => $userTokenCheck[0]['name'],
                                        "content"=>$notifyMsg1
                                    ];

                                    $response = ['jobListing'=>$data1, 'status' => "SUCCESS"];

                                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                                
                                } else {
                                    $response = ['message' => "FAILURE", 'status' => "FAILURE"];
                                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                                }
                            } else {
                                $response = ['message' => "FAILURE", 'status' => "FAILURE"];
                                $this->set_response($response, REST_Controller::HTTP_CREATED);
                            }

                        } else {

                            $companyName = $this->Jobpost_Model->fetch_companyName($jobfetch['recruiter_id']);
                            $jobDate = strtotime($jobfetch['updated_at']);
                            $datediff = $now - $jobDate;
                            $daydiff = floor($datediff / (60 * 60 * 24));

                            if($daydiff >= 1) {
                            //if($daydiff >= 0) {
                                $data1["jobList"][] = [
                                    "jobpost_id" => $jobfetch['jobpost_id'],
                                    "job_title" => $jobfetch['jobtitle'],
                                    "companyName" => $companyName[0]['cname'],
                                    "companyId" => $jobfetch['recruiter_id'],
                                    "msg" => "open popup",
                                    "name" => $userTokenCheck[0]['name']
                                ];

                                $response = ['jobListing' => $data1, 'status' => "SUCCESS"];
                                $this->set_response($response, REST_Controller::HTTP_CREATED); 
                                
                            } else {
                                $data1["jobList"] = [];
                                $response = ['jobListing' => $data1, 'status' => "SUCCESS"];
                                $this->set_response($response, REST_Controller::HTTP_CREATED);
                            }
                        }
                    }
                    
                } else{
                    $data1["jobList"] = [];
                    $response = ['jobListing' => $data1, 'status' => "SUCCESS"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
            } 
        }else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function addreconciliation_post() {
        $userData = $this->input->post();

        $data = ["token" => $userData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);

        if($userTokenCheck1) {

            $this->form_validation->set_rules('token', 'token', 'trim|required');
            $this->form_validation->set_rules('job_id', 'job_id', 'trim|required');
            $this->form_validation->set_rules('status', 'status', 'trim|required');
            
            if ($this->form_validation->run() == FALSE) {
                $errors = $this->form_validation->error_array();
                $error = current($errors);
                $response = ['message' => $error, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            
            } else {
            
                $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
                $userTokenCheck = $this->User_Model->token_match($tokendata);
                
                if($userTokenCheck) {

                    $reconciliation =  $this->Jobpost_Model->job_fetch_appliedCheck11($userData['job_id'], $userTokenCheck1[0]['user_id']);

                    if($reconciliation) {
                        
                        $job_detail = $this->Jobpost_Model->job_detail_fetch($userData['job_id']);
                        $data = ["status"=> $userData['status']];
                        
                        $this->Jobpost_Model->update_reconciliation($reconciliation[0]['id'], $data);

                        if($userData['status'] == 0) {
                            $notifyMsg = "Success is not final, failure is not fatal: It is the courage to continue that counts. Browse for more jobs now!";

                        } else if($userData['status'] == 1) {
                            $notifyMsg = "Congratulations for landing your dream job!";
                        
                        } else if($userData['status'] == 2) {
                            $notifyMsg = "Waiting for feedback";
                        } else if($userData['status'] == 3) {
                            $notifyMsg = $userData['schedule'];
                        }
                        $response = ['message' => $notifyMsg, 'jobexpire'=>$job_detail[0]['jobexpire'], 'status' => "SUCCESS"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);

                    } else {
                        $data = ["user_id" => $userTokenCheck[0]['id'],"job_id"=>$userData['job_id'], "status"=> $userData['status'] ,"showupreason"=>$userData['reason'], "schedule"=>$userData['schedule']];
                        
                        $reconciliation =  $this->Jobpost_Model->add_reconciliation($data);

                        $job_detail = $this->Jobpost_Model->job_detail_fetch($userData['job_id']);
                          
                        if($reconciliation) {
                            
                            if($userData['status'] == 0) {
                                $notifyMsg = "Success is not final, failure is not fatal: It is the courage to continue that counts. Browse for more jobs now!";

                            } else if($userData['status'] == 1) {
                                $notifyMsg = "Congratulations for landing your dream job!";
                            
                            } else if($userData['status'] == 2) {
                                $notifyMsg = "Waiting for feedback";
                            
                            } else if($userData['status'] == 3) {
                                $notifyMsg = $userData['schedule'];
                            }
                            $response = ['message' => $notifyMsg, 'jobexpire'=>$job_detail[0]['jobexpire'],'status' => "SUCCESS"];
                            $this->set_response($response, REST_Controller::HTTP_CREATED);
                        }
                    }     
                }
            }

        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function savedate_post() {
        $userData = $this->input->post();
        $this->form_validation->set_rules('token', 'token', 'trim|required');
        $this->form_validation->set_rules('job_id', 'job_id', 'trim|required');
        $this->form_validation->set_rules('date', 'date', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {
            $data = ["token" => $userData['token']];
            $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck) {
                    $data = ["accepted_date" =>$userData['date']];
                      $saveddate =  $this->Jobpost_Model->save_date($userTokenCheck[0]['id'], $userData['job_id'], $data);
                      if($saveddate){
                        $response = ['message' => 'Date saved Successfully', 'status' => "SUCCESS"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                
                 
            }} else {
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    public function companylist_post() {
        $jobData = $this->input->post();
        $usercurlat = "14.5807385";
        $usercurlong = "121.0634843";

        $data = ["token" => $jobData['token']];
        $userTokenCheck = $this->User_Model->token_match1($data);
        
        if($userTokenCheck) {
                
            $company_lists = $this->Newpoints_Model->companysite_lists($usercurlat,$usercurlong);

            if(!empty($company_lists)) {
                $x=0;
                
                $companyidsarray = array();
                foreach ($company_lists as $company_list) {
                    $parent = $this->Newpoints_Model->company_parent($company_list['parent_id']);

                    $jobcountfetch = $this->Newpoints_Model->companysite_jobcount($userTokenCheck[0]['user_id'], $company_list['id'], $expfilter, $usercurlat,$usercurlong);
                    if($jobcountfetch > 0) {
                        
                        if(strlen($company_list['companyPic']) > 0) {
                            
                            $companylist[$x] = [
                                  "id" => $company_list['id'],
                                  "image" => $company_list['companyPic'],
                                  "cname" => $parent[0]['cname'].', '.$company_list['cname'],
                                  "cname_value" => $company_list['cname'],
                                  "jobcount" => $jobcountfetch,
                                  "parent_id"=>$company_list['parent_id']
                            ];
                        
                        } else {

                            $siteimgs = $this->Jobpost_Model->fetch_companyPic($company_list['parent_id']);
                            if($siteimgs) {
                                $companylist[$x] = [
                                      "id" => $company_list['id'],
                                      "image" => $siteimgs[0]['companyPic'],
                                      "cname" => $parent[0]['cname'].', '.$company_list['cname'],
                                      "cname_value" => $company_list['cname'],
                                      "jobcount" => $jobcountfetch,
                                      "parent_id"=>$company_list['parent_id']
                                ];
                            }
                        }
                        $companyidsarray[] = $company_list['parent_id'];
                        $x++;
                    }
                }
            } else {
                $companylist = [];
            }
            $companyidsarray = array_unique($companyidsarray);

            $jobcount = array();
            foreach ($companylist as $key => $row) {
                $jobcount[$key] = $row['jobcount'];
            }
            array_multisort($jobcount, SORT_DESC, $companylist);
        
            $response = ['companylist' => $companylist, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        }else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function advertiselist_post() {
        $jobData = $this->input->post();
        $data = ["token" => $jobData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1) {
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);

            if($userTokenCheck) {  
                $ad_list = $this->Jobpost_Model->advertise_lists($jobData['cur_lat'],$jobData['cur_long']);
                if(!empty($ad_list)){
                    $x=0;
                    foreach ($ad_list as $ad_lists) {
                        $ad_lists1[$x] = [
                              "ad_id" => $ad_lists['id'],
                              "banner" => $ad_lists['banner'],
                              //"banner" => $ad_lists['banner'],
                              "description" => $ad_lists['description'],
                              "recruiter_id" => $ad_lists['site_id'],
                              "profilePic" => $ad_lists['companyPic'],             
                              "companyName" =>   $ad_lists['cname']             

                          ];
                        $x++;
                    }
                }else{
                    $ad_lists1 = [];
                }
                
                
                $response = ['adlist' => $ad_lists1, 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);

            }
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function matchVersion_post(){
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);

            if($userTokenCheck) {
                $versionData1 = $this->User_Model->fetch_version();
                //print_r($versionData);die;
                $data1['versionData']=[
                    'api_version'=>$versionData1[0]['api_version'],
                    'recommend_update'=>$versionData1[0]['recommend_update'],
                    'force_update'=>$versionData1[0]['force_update'],
                    'message'=>urldecode($versionData1[0]['message'])
                ];
                if($versionData1[0]['api_version']<=$userData['api_version']){
                    $response = ['message' => 'Ignore', 'status' => "FAILURE"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }else{
                    if($versionData1[0]['recommend_update']==0 && $versionData1[0]['force_update']==0){
                        $response = ['message' => 'Ignore', 'status' => "FAILURE"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }else{
                        $response = ['matchVersion' =>$data1, 'status'=> "SUCCESS" ];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                    
                }
                
            }
        }else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }    
    }

    public function homedatanew_post() {

        $ad_list = array();
        $companylist = array();
        $topCompanylist = array();

        $userData = $this->input->post();
        $usercurlat = $userData['cur_lat'];
        $usercurlong = $userData['cur_long'];

        //  Index for response
        $response_index = $userData['indexing'];

        $data = ["token" => $userData['token']];
        $userTokenCheck = $this->User_Model->token_match1($data);

        if($userTokenCheck) {

            $comProfile = ["latitude" => $usercurlat, "longitude"=>$usercurlong];
            $this->User_Model->update_latlong($comProfile, $userTokenCheck[0]['user_id']);

            $tokendata = ['id'=>$userTokenCheck[0]['user_id']];
            $userData = $this->User_Model->token_match($tokendata);
            
            $expmonth = $userData[0]['exp_month'];
            $expyear = $userData[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
            } else if($expyear >= 7) { 
                $expfilter = "10";
            } else {
                $expfilter = "";
            }
            
            if($response_index == 0) {

                // Section 1
                $ad_list = $this->Jobpost_Model->advertise_lists($usercurlat,$usercurlong);
                if($ad_list) {
                    $x=0;
                    foreach ($ad_list as $ad_lists) {
                        $ad_lists1[$x] = [
                              "ad_id" => $ad_lists['id'],
                              "banner" => $ad_lists['banner'],
                              "description" => $ad_lists['description'],
                              "recruiter_id" => $ad_lists['site_id'],
                              "profilePic" => $ad_lists['companyPic'],
                              "companyName" =>   $ad_lists['cname']
                        ];
                        $x++;
                    }
                } else {
                    $ad_lists1[] = [
                            "ad_id" => '',
                            "banner" => base_url() ."images/new_banner_Man.png",
                            "description" => '',
                            "recruiter_id" => '',
                            "profilePic" => '',
                            "companyName" => ''
                    ];
                }

                //section 2 - company list
                $company_lists = $this->Newpoints_Model->companysite_lists($usercurlat,$usercurlong);

                if(!empty($company_lists)) {
                    $x=0;
                    
                    $companyidsarray = array();
                    foreach ($company_lists as $company_list) {
                        $parent = $this->Newpoints_Model->company_parent($company_list['parent_id']);

                        $jobcountfetch = $this->Homepage_Model->companysite_jobcount($userTokenCheck[0]['user_id'], $company_list['id'], $expfilter, $usercurlat,$usercurlong);
                        if($jobcountfetch) {

                            $jobcount = count($jobcountfetch);
                            $total_oppurnity=0;
                            foreach($jobcountfetch as $jobcountf) {
                                $total_oppurnity = $total_oppurnity + $jobcountf['opening'];
                            }
                            
                            if(strlen($company_list['companyPic']) > 0) {
                                
                                $companylist[$x] = [
                                      "id" => $company_list['id'],
                                      "image" => $company_list['companyPic'],
                                      "cname" => $parent[0]['cname'].', '.$company_list['cname'],
                                      "jobcount" => $jobcount,
                                      "openingcount" => $total_oppurnity,
                                      "parent_id"=>$company_list['parent_id']
                                ];
                            
                            } else {

                                $siteimgs = $this->Jobpost_Model->fetch_companyPic($company_list['parent_id']);
                                if($siteimgs) {
                                    $companylist[$x] = [
                                          "id" => $company_list['id'],
                                          "image" => $siteimgs[0]['companyPic'],
                                          "cname" => $parent[0]['cname'].', '.$company_list['cname'],
                                          "jobcount" => $jobcount,
                                          "openingcount" => $total_oppurnity,
                                          "parent_id"=>$company_list['parent_id']
                                    ];
                                }
                            }
                            $companyidsarray[] = $company_list['parent_id'];
                            $x++;
                        }
                    }
                } else {
                    $companylist = [];
                }
                $companyidsarray = array_unique($companyidsarray);

                $jobcount = array();
                foreach ($companylist as $key => $row) {
                    $jobcount[$key] = $row['jobcount'];
                }
                array_multisort($jobcount, SORT_DESC, $companylist);


                $activelyJob = $this->activelyhiringJobs($userTokenCheck[0]['user_id'], $usercurlat, $usercurlong, $expfilter);

            
            } else if($response_index == 1) {

                //  Job by No experience category
                $noexpjobs = array();
                $negletarr = array();

                $noexpjobDatas = $this->Jobsapp_Model->homelist_no_experience_groupby($userTokenCheck[0]['user_id'], $expfilter, $usercurlat,$usercurlong, [], $this->default_distance);
                foreach ($noexpjobDatas as $noexpjobData) {
                    
                    $negletarr[] = $noexpjobData['id'];
                    $jobTop = $this->Jobpost_Model->job_toppicks($noexpjobData['id']);
                    //if(!empty($jobTop) && count($jobTop)>=0) {
                        if(isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id']; 
                        } else{
                            $datatoppicks1 = "";
                        }
                        if(isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id']; 
                        } else{
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id']; 
                        } else{
                            $datatoppicks3 = "";
                        }
                        
                        $savedjob = $this->Jobpost_Model->job_saved($noexpjobData['id'], $userTokenCheck[0]['user_id']);
                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }

                        $jobsal = $this->Jobpost_Model->getExpJob($noexpjobData['id']);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = 0;
                        }

                        $companydetail = $this->Jobpost_Model->company_detail_fetch($noexpjobData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($noexpjobData['compId']);
                        if(isset($companydetail1[0]['address'])){
                            $companyaddress=$companydetail1[0]['address'];
                        }
                        else{
                            $companyaddress='';
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($noexpjobData['compId']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }

                        $jobImage = $this->Jobpost_Model->job_image($noexpjobData['id']);
                        if(isset($jobImage[0]['pic'])){
                            $jobImageData=$jobImage[0]['pic'];
                        }
                        else{
                            $jobImageData=$comPic;
                        }

                        $companyname = $this->Jobpost_Model->company_name_fetch($noexpjobData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($usercurlat,$usercurlong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', ''); 

                        $noexpjobs[] = [
                                "jobpost_id" => $noexpjobData['id'],
                                "comapnyId" =>$noexpjobData['compId'],
                                "job_title" => $noexpjobData['jobtitle'],
                                "jobDesc" => $noexpjobData['jobDesc'],
                                "jobPitch"=>$noexpjobData['jobPitch'],
                                "salary" => number_format($noexpjobData['salary']),
                                "companyName"=> $companydetail[0]["cname"],
                                "cname"=> $cname,
                                "companyAddress"=>$companyaddress,
                                "toppicks1" => $datatoppicks1,
                                "toppicks2" => $datatoppicks2,
                                "toppicks3" => $datatoppicks3,
                                "job_image" =>$jobImageData,
                                "latitude"=> $noexpjobData['latitude'], 
                                "longitude"=>$noexpjobData['longitude'],
                                "jobexpire"=> $noexpjobData['jobexpire'],
                                "companyPic"=> $comPic,
                                "savedjob" =>$savedjob,
                                "boostjob" =>$noexpjobData['boost_status'],
                                "distance" => $distance
                        ];
                    //}
                }
                $noexpjobDatas = $this->Jobsapp_Model->homelist_no_experience($userTokenCheck[0]['user_id'], $expfilter, $usercurlat,$usercurlong, $this->default_distance);
                foreach ($noexpjobDatas as $noexpjobData) {

                    if(in_array($noexpjobData['id'], $negletarr)) { } else {
                        $jobTop = $this->Jobpost_Model->job_toppicks($noexpjobData['id']);
                        //if(!empty($jobTop) && count($jobTop)>=0) {
                            if(isset($jobTop[0]['picks_id'])) {
                                $datatoppicks1 = $jobTop[0]['picks_id']; 
                            } else{
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $datatoppicks2 = $jobTop[1]['picks_id']; 
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop[2]['picks_id'])) {
                                $datatoppicks3 = $jobTop[2]['picks_id']; 
                            } else{
                                $datatoppicks3 = "";
                            }
                            
                            $savedjob = $this->Jobpost_Model->job_saved($noexpjobData['id'], $userTokenCheck[0]['user_id']);
                            if(!empty($savedjob[0]['id'])) {
                                $savedjob = '1';
                            } else{
                                $savedjob = "0";
                            }

                            $jobsal = $this->Jobpost_Model->getExpJob($noexpjobData['id']);
                            if($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else{
                                $basicsalary = 0;
                            }

                            $companydetail = $this->Jobpost_Model->company_detail_fetch($noexpjobData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($noexpjobData['compId']);
                            if(isset($companydetail1[0]['address'])){
                                $companyaddress=$companydetail1[0]['address'];
                            }
                            else{
                                $companyaddress='';
                            }

                            $companyPic = $this->Jobpost_Model->fetch_companyPic($noexpjobData['compId']);
                            if(!empty($companyPic[0]['companyPic'])) {
                                $comPic = trim($companyPic[0]['companyPic']);
                            } else{
                                $comPic = "";
                            }

                            $jobImage = $this->Jobpost_Model->job_image($noexpjobData['id']);
                            if(isset($jobImage[0]['pic'])){
                                $jobImageData=$jobImage[0]['pic'];
                            }
                            else{
                                $jobImageData=$comPic;
                            }

                            $companyname = $this->Jobpost_Model->company_name_fetch($noexpjobData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            $distance = $this->distance($usercurlat,$usercurlong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', ''); 

                            $noexpjobs[] = [
                                    "jobpost_id" => $noexpjobData['id'],
                                    "comapnyId" =>$noexpjobData['compId'],
                                    "job_title" => $noexpjobData['jobtitle'],
                                    "jobDesc" => $noexpjobData['jobDesc'],
                                    "jobPitch"=>$noexpjobData['jobPitch'],
                                    "salary" => number_format($noexpjobData['salary']),
                                    "companyName"=> $companydetail[0]["cname"],
                                    "cname"=> $cname,
                                    "companyAddress"=>$companyaddress,
                                    "toppicks1" => $datatoppicks1,
                                    "toppicks2" => $datatoppicks2,
                                    "toppicks3" => $datatoppicks3,
                                    "job_image" =>$jobImageData,
                                    "latitude"=> $noexpjobData['latitude'], 
                                    "longitude"=>$noexpjobData['longitude'],
                                    "jobexpire"=> $noexpjobData['jobexpire'],
                                    "companyPic"=> $comPic,
                                    "savedjob" =>$savedjob,
                                    "boostjob" =>$noexpjobData['boost_status'],
                                    "distance" => $distance
                            ];
                        //}
                    }
                }


                //  City listing
                $city_lists = $this->Jobpost_Model->homecity_lists();
                if(!empty($city_lists)) {

                    if($userData) {
                        $usercity = $userData[0]['city'];
                    } else {
                        $usercity = "";
                    }
                    $c=0;
                    $citylist = array();

                    foreach ($city_lists as $city_list) {

                        if(strpos($usercity, $city_list['name']) !== false) {
                            if($city_list['name'] == "Metro Manila") {
                                $city_list['name'] = "Manila";
                            }
                            $jobcountfetch = $this->Homepage_Model->city_jobcount($userTokenCheck[0]['user_id'], $city_list['name'], $expfilter, $usercurlat, $usercurlong);
                            if($jobcountfetch) {

                                $jobcount = count($jobcountfetch);
                                $total_oppurnity=0;
                                foreach($jobcountfetch as $jobcountf) {
                                    $total_oppurnity = $total_oppurnity + $jobcountf['opening'];
                                }

                                if($city_list['name'] == "Manila") {
                                    $city_list['name'] = "Metro Manila";
                                }
                                $citylist[$c] = [
                                      "id" => $city_list['id'],
                                      "cityname" => $city_list['name'],
                                      "image" => base_url().$city_list['image'],
                                      "jobcount" => $jobcount,
                                      "openingcount" => $total_oppurnity,
                                ];
                                $c++;
                            }
                        }
                    }
                    if(count($citylist) > 0) {
                        $cc = $c;
                    } else {
                        $cc = 0;
                    }
                    foreach ($city_lists as $city_list) {

                        if(strpos($usercity, $city_list['name']) !== false) {
                        } else {
                            if($city_list['name'] == "Metro Manila") {
                                $city_list['name'] = "Manila";
                            }
                            $jobcountfetch = $this->Homepage_Model->city_jobcount($userTokenCheck[0]['user_id'], $city_list['name'], $expfilter, $usercurlat, $usercurlong);
                            if($jobcountfetch) {

                                $jobcount = count($jobcountfetch);
                                $total_oppurnity=0;
                                foreach($jobcountfetch as $jobcountf) {
                                    $total_oppurnity = $total_oppurnity + $jobcountf['opening'];
                                }

                                $citylist[$cc] = [
                                      "id" => $city_list['id'],
                                      "cityname" => $city_list['name'],
                                      "image" => base_url().$city_list['image'],
                                      "jobcount" => $jobcount,
                                      "openingcount" => $total_oppurnity,
                                ];
                                $cc++;
                            }
                        }
                    }
                } else {
                    $citylist = [];
                }


                // Top Recruiters
                $company_lists = $this->Newpoints_Model->mainrecruiter_lists($usercurlat,$usercurlong);
                if(!empty($company_lists)) {
                    $x=0;
                    
                    $companyidsarray = array();
                    foreach ($company_lists as $company_list) {
                        //$parent = $this->Newpoints_Model->company_parent($company_list['parent_id']);

                        if($company_list['top_recruiter'] > 0) {

                            // $parentPic = $this->Jobpost_Model->fetch_companyPic($company_list['parent_id']);
                            // $comapnyPic = $parentPic[0]['companyPic'];
                            if(strlen($company_list['companyPic']) > 0) {
                                $comapnyPic = $company_list['companyPic'];
                            }  else {
                                $comapnyPic = "";
                            }

                            $getTopPicks = $this->Newpoints_Model->company_parent_toppicks($company_list['id']);
                            if($getTopPicks) {
                                $toppics_first = $getTopPicks[0]['picks_id'];
                                $toppics_second = $getTopPicks[1]['picks_id'];
                            } else {
                                $toppics_first = 0;
                                $toppics_second = 0;
                            }
                            if(strlen($company_list['headquarter']) > 0) {
                                $parent_headquater = $company_list['headquarter'];
                            } else {
                                $parent_headquater = "";
                            }
                            if(strlen($company_list['founded']) > 0) {
                                $parent_founded = $company_list['founded'];
                            } else {
                                $parent_founded = "";
                            }
                            if(strlen($company_list['size']) > 0) {
                                $parent_size = $company_list['size'];
                            } else {
                                $parent_size = "";
                            }
                            if(strlen($company_list['num_sites']) > 0) {
                                $parent_num_sites = $company_list['num_sites'];
                            } else {
                                $parent_num_sites = "";
                            }
                            $topCompanylist[$x] = [
                                          "id" => $company_list['id'],
                                          "image" => $comapnyPic,
                                          "cname" => $company_list['cname'],
                                          "top_Recruiter" => $company_list['top_recruiter'],
                                          "headquater" => $parent_headquater,
                                          "founded" => $parent_founded,
                                          "size" => $parent_size,
                                          "num_sites" => $parent_num_sites,
                                          "toppics_first" => $toppics_first,
                                          "toppics_second" => $toppics_second,
                                    ];
                            $x++;
                        }
                    }
                } else {
                    $topCompanylist = [];
                }
                $companyidsarray = array_unique($companyidsarray);
                $jobcount = array();
                foreach ($topCompanylist as $key => $row) {
                    $jobcount[$key] = $row['top_recruiter'];
                }
                array_multisort($jobcount, SORT_ASC, $topCompanylist);


            } else if($response_index == 2) {


                //section 3  --  Category list
                $cat_lists = $this->Jobpost_Model->category_lists();
                if(!empty($cat_lists)) {
                    $x=0;
                    foreach ($cat_lists as $cat_list) {
                        $jobcountfetch = $this->Homepage_Model->category_jobcount($userTokenCheck[0]['user_id'], $cat_list['id'], $expfilter, $usercurlat, $usercurlong);
                        
                        if($jobcountfetch) {
                            $openingCount = 0; 
                            foreach($jobcountfetch as $jobcountf) {
                                $openingCount = $openingCount + $jobcountf['opening'];
                            }
                            if($openingCount > 0) {

                                $categorylist[$x] = [
                                      "id" => $cat_list['id'],
                                      "catname" => $cat_list['category'],
                                      "jobcount" => count($jobcountfetch),
                                      "openingcount" => (string)$openingCount,
                                ];

                                $x++;
                            }
                        }
                    }
                } else {
                    $categorylist = [];
                }


                // Review list
                $reviewlist = array();
                $review_lists = $this->Jobsapp_Model->appreviews_lists_limit();
                if(!empty($review_lists)) {

                    foreach ($review_lists as $review_list) {

                        $company_list = $this->Newpoints_Model->companysite_lists_byid($review_list['company']);

                        if(strlen($company_list[0]['companyPic']) > 0) {
                            $comapnyPic = $company_list[0]['companyPic'];
                        } else {
                            $parentPic = $this->Jobpost_Model->fetch_companyPic($company_list[0]['parent_id']);
                            $comapnyPic = $parentPic[0]['companyPic'];
                        }

                        $reviewlist[] = [
                                  "id" => $review_list['id'],
                                  "name" => $review_list['name'],
                                  "image" => base_url() .$review_list['image'],
                                  "comment" => $review_list['rate_desc'],
                                  "rate" => $review_list['rate'],
                                  "date" => $review_list['rating_date'],
                                  "company_pic" => $comapnyPic,
                                  "cname" => $company_list[0]['cname'],
                                  "companyimage" => base_url() .$review_list['companyimage'],
                            ];
                    }
                
                } else {
                    $reviewlist = [];
                }


                $hotjobss = array();
                $bostArr = array();
                $neglectArr = array();

                // Company hot jobs
                $hotjobData = $this->Jobsapp_Model->homelist_hotjob_groupby($userTokenCheck[0]['user_id'],$expfilter, $usercurlat,$usercurlong, [], $this->default_distance);
                foreach ($hotjobData as $hotjobsData) {

                    $neglectArr[] = $hotjobsData['id'];
                    $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                    if(!empty($jobTop) && count($jobTop)>=1) {  
                        if(isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id']; 
                        } else{
                            $datatoppicks1 = "";
                        }
                        if(isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id']; 
                        } else{
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id']; 
                        } else{
                            $datatoppicks3 = "";
                        }
                        
                        $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['user_id']);
                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }

                        $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = 0;
                        }

                        $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                        if(isset($companydetail1[0]['address'])){
                            $companyaddress=$companydetail1[0]['address'];
                        }
                        else{
                            $companyaddress='';
                        }

                        $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                        if(isset($jobImage[0]['pic'])){
                            $jobImageData=$jobImage[0]['pic'];
                        } else {
                            $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                            if(!empty($companyPic[0]['companyPic'])) {
                                $comPic = trim($companyPic[0]['companyPic']);
                            } else{
                                $comPic = "";
                            }
                            $jobImageData=$comPic;
                        }

                        $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($usercurlat,$usercurlong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', ''); 

                        $hotjobss[] = [
                                "jobpost_id" => $hotjobsData['id'],
                                "comapnyId" =>$hotjobsData['compId'],
                                "job_title" => $hotjobsData['jobtitle'],
                                "jobDesc" => $hotjobsData['jobDesc'],
                                "jobPitch"=>$hotjobsData['jobPitch'],
                                "salary" => number_format($hotjobsData['salary']),
                                "companyName"=> $companydetail[0]["cname"],
                                "cname"=> $cname,
                                "companyAddress"=>$companyaddress,
                                "toppicks1" => $datatoppicks1,
                                "toppicks2" => $datatoppicks2,
                                "toppicks3" => $datatoppicks3,
                                "job_image" =>$jobImageData,
                                "latitude"=> $hotjobsData['latitude'], 
                                "longitude"=>$hotjobsData['longitude'],
                                "jobexpire"=> $hotjobsData['jobexpire'],
                                "companyPic"=> $comPic,
                                "savedjob" =>$savedjob,
                                "boostjob" =>$hotjobsData['boost_status'],
                                "distance" => $distance
                        ];
                    }
                }

                //Boost job
                if(count($hotjobss) <= 8) {
                    $zz=1;
                    $boostjobData = $this->Jobsapp_Model->homelist_boostjob($userTokenCheck[0]['user_id'],$expfilter, $usercurlat,$usercurlong, $this->default_distance);
                    foreach ($boostjobData as $boostjobsData) {

                        if(in_array($boostjobsData['id'], $neglectArr)) { } else {

                            if($zz <= 2) {
                                $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
                                if(!empty($jobTop) && count($jobTop)>=1) {
                                    if(isset($jobTop[0]['picks_id'])) {
                                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                                    } else{
                                        $datatoppicks1 = "";
                                    }
                                    if(isset($jobTop[1]['picks_id'])) {
                                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                                    } else{
                                        $datatoppicks2 = "";
                                    }
                                    if(isset($jobTop[2]['picks_id'])) {
                                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                                    } else{
                                        $datatoppicks3 = "";
                                    }
                                    
                                    $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userTokenCheck[0]['user_id']);
                                    if(!empty($savedjob[0]['id'])) {
                                        $savedjob = '1';
                                    } else{
                                        $savedjob = "0";
                                    }

                                    $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                                    if($jobsal[0]['basicsalary']) {
                                        $basicsalary = $jobsal[0]['basicsalary'];
                                    } else{
                                        $basicsalary = 0;
                                    }

                                    $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                                    if(isset($companydetail1[0]['address'])){
                                        $companyaddress=$companydetail1[0]['address'];
                                    }
                                    else{
                                        $companyaddress='';
                                    }

                                    $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                                    if(isset($jobImage[0]['pic'])){
                                        $jobImageData=$jobImage[0]['pic'];
                                    } else {
                                        $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                                        if(!empty($companyPic[0]['companyPic'])) {
                                            $comPic = trim($companyPic[0]['companyPic']);
                                        } else{
                                            $comPic = "";
                                        }
                                        $jobImageData=$comPic;
                                    }

                                    $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                                    if(!empty($companyname[0]["cname"])){
                                        $cname = $companyname[0]["cname"];
                                    }else{
                                        $cname = '';
                                    }

                                    $distance = $this->distance($usercurlat,$usercurlong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                    $distance = number_format((float)$distance, 2, '.', ''); 

                                    $bostArr[] = $boostjobsData['id'];

                                    $hotjobss[] = [
                                            "jobpost_id" => $boostjobsData['id'],
                                            "comapnyId" =>$boostjobsData['compId'],
                                            "job_title" => $boostjobsData['jobtitle'],
                                            "jobDesc" => $boostjobsData['jobDesc'],
                                            "jobPitch"=>$boostjobsData['jobPitch'],
                                            "salary" => number_format($boostjobsData['salary']),
                                            "companyName"=> $companydetail[0]["cname"],
                                            "cname"=> $cname,
                                            "companyAddress"=>$companyaddress,
                                            "toppicks1" => $datatoppicks1,
                                            "toppicks2" => $datatoppicks2,
                                            "toppicks3" => $datatoppicks3,
                                            "job_image" =>$jobImageData,
                                            "latitude"=> $boostjobsData['latitude'], 
                                            "longitude"=>$boostjobsData['longitude'],
                                            "jobexpire"=> $boostjobsData['jobexpire'],
                                            "companyPic"=> $comPic,
                                            "savedjob" =>$savedjob,
                                            "boostjob" =>$boostjobsData['boost_status'],
                                            "distance" => $distance
                                    ];
                                }
                            }
                            $zz++;
                        }
                    }

                    //Hot job
                    $zzz=1;
                    $hotjobData = $this->Jobsapp_Model->homelist_hotjobs($userTokenCheck[0]['user_id'],$expfilter, $usercurlat,$usercurlong, $this->default_distance);
                    foreach ($hotjobData as $hotjobsData) {

                        if(in_array($hotjobsData['id'], $bostArr)) {
                        } else {

                            if($zzz <= 2){
                                $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                                if(!empty($jobTop) && count($jobTop)>=1) {  
                                    if(isset($jobTop[0]['picks_id'])) {
                                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                                    } else{
                                        $datatoppicks1 = "";
                                    }
                                    if(isset($jobTop[1]['picks_id'])) {
                                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                                    } else{
                                        $datatoppicks2 = "";
                                    }
                                    if(isset($jobTop[2]['picks_id'])) {
                                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                                    } else{
                                        $datatoppicks3 = "";
                                    }
                                    
                                    $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['user_id']);
                                    if(!empty($savedjob[0]['id'])) {
                                        $savedjob = '1';
                                    } else{
                                        $savedjob = "0";
                                    }

                                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                                    if($jobsal[0]['basicsalary']) {
                                        $basicsalary = $jobsal[0]['basicsalary'];
                                    } else{
                                        $basicsalary = 0;
                                    }

                                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                                    if(isset($companydetail1[0]['address'])){
                                        $companyaddress=$companydetail1[0]['address'];
                                    }
                                    else{
                                        $companyaddress='';
                                    }

                                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                                    if(isset($jobImage[0]['pic'])){
                                        $jobImageData=$jobImage[0]['pic'];
                                    } else {
                                        $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                                        if(!empty($companyPic[0]['companyPic'])) {
                                            $comPic = trim($companyPic[0]['companyPic']);
                                        } else{
                                            $comPic = "";
                                        }
                                        $jobImageData=$comPic;
                                    }

                                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                                    if(!empty($companyname[0]["cname"])){
                                        $cname = $companyname[0]["cname"];
                                    }else{
                                        $cname = '';
                                    }

                                    $distance = $this->distance($usercurlat,$usercurlong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                    $distance = number_format((float)$distance, 2, '.', ''); 

                                    $hotjobss[] = [
                                            "jobpost_id" => $hotjobsData['id'],
                                            "comapnyId" =>$hotjobsData['compId'],
                                            "job_title" => $hotjobsData['jobtitle'],
                                            "jobDesc" => $hotjobsData['jobDesc'],
                                            "jobPitch"=>$hotjobsData['jobPitch'],
                                            "salary" => number_format($hotjobsData['salary']),
                                            "companyName"=> $companydetail[0]["cname"],
                                            "cname"=> $cname,
                                            "companyAddress"=>$companyaddress,
                                            "toppicks1" => $datatoppicks1,
                                            "toppicks2" => $datatoppicks2,
                                            "toppicks3" => $datatoppicks3,
                                            "job_image" =>$jobImageData,
                                            "latitude"=> $hotjobsData['latitude'], 
                                            "longitude"=>$hotjobsData['longitude'],
                                            "jobexpire"=> $hotjobsData['jobexpire'],
                                            "companyPic"=> $comPic,
                                            "savedjob" =>$savedjob,
                                            "boostjob" =>$hotjobsData['boost_status'],
                                            "distance" => $distance
                                    ];
                                }
                                $zzz++;
                            }
                        }
                    }
                }


            } else if($response_index == 3) {


                $featuredTopPicks = $this->getFeaturedTopPicks1($userTokenCheck[0]['user_id'], $usercurlat, $usercurlong, $expfilter, "indexingtwo");

                

                //  Job by Information Technology category
                $itcatjobs = array();
                $negletarr = array();

                $itcatjobDatas = $this->Jobsapp_Model->homelist_information_technology_groupby($userTokenCheck[0]['user_id'], $expfilter, $usercurlat,$usercurlong, [], $this->default_distance);
                foreach ($itcatjobDatas as $itcatjobData) {
                    $negletarr[] = $itcatjobData['id'];
                    $jobTop = $this->Jobpost_Model->job_toppicks($itcatjobData['id']);
                    //if(!empty($jobTop) && count($jobTop)>=0) {
                        if(isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id']; 
                        } else{
                            $datatoppicks1 = "";
                        }
                        if(isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id']; 
                        } else{
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id']; 
                        } else{
                            $datatoppicks3 = "";
                        }
                        
                        $savedjob = $this->Jobpost_Model->job_saved($itcatjobData['id'], $userTokenCheck[0]['user_id']);
                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }

                        $jobsal = $this->Jobpost_Model->getExpJob($itcatjobData['id']);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = 0;
                        }

                        $companydetail = $this->Jobpost_Model->company_detail_fetch($itcatjobData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($itcatjobData['compId']);
                        if(isset($companydetail1[0]['address'])){
                            $companyaddress=$companydetail1[0]['address'];
                        }
                        else{
                            $companyaddress='';
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($itcatjobData['compId']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }

                        $jobImage = $this->Jobpost_Model->job_image($itcatjobData['id']);
                        if(isset($jobImage[0]['pic'])){
                            $jobImageData=$jobImage[0]['pic'];
                        }
                        else{
                            $jobImageData=$comPic;
                        }

                        $companyname = $this->Jobpost_Model->company_name_fetch($itcatjobData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($usercurlat,$usercurlong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', ''); 

                        $itcatjobs[] = [
                                "jobpost_id" => $itcatjobData['id'],
                                "comapnyId" =>$itcatjobData['compId'],
                                "job_title" => $itcatjobData['jobtitle'],
                                "jobDesc" => $itcatjobData['jobDesc'],
                                "jobPitch"=>$itcatjobData['jobPitch'],
                                "salary" => number_format($itcatjobData['salary']),
                                "companyName"=> $companydetail[0]["cname"],
                                "cname"=> $cname,
                                "companyAddress"=>$companyaddress,
                                "toppicks1" => $datatoppicks1,
                                "toppicks2" => $datatoppicks2,
                                "toppicks3" => $datatoppicks3,
                                "job_image" =>$jobImageData,
                                "latitude"=> $itcatjobData['latitude'], 
                                "longitude"=>$itcatjobData['longitude'],
                                "jobexpire"=> $itcatjobData['jobexpire'],
                                "companyPic"=> $comPic,
                                "savedjob" =>$savedjob,
                                "boostjob" =>$itcatjobData['boost_status'],
                                "distance" => $distance
                        ];
                    //}
                }
                $itcatjobDatas = $this->Jobsapp_Model->homelist_information_technology($userTokenCheck[0]['user_id'], $expfilter, $usercurlat,$usercurlong, $this->default_distance);
                foreach ($itcatjobDatas as $itcatjobData) {
                    if(in_array($itcatjobData['id'], $negletarr)) { } else {
                        $jobTop = $this->Jobpost_Model->job_toppicks($itcatjobData['id']);
                        //if(!empty($jobTop) && count($jobTop)>=0) {
                            if(isset($jobTop[0]['picks_id'])) {
                                $datatoppicks1 = $jobTop[0]['picks_id']; 
                            } else{
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $datatoppicks2 = $jobTop[1]['picks_id']; 
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop[2]['picks_id'])) {
                                $datatoppicks3 = $jobTop[2]['picks_id']; 
                            } else{
                                $datatoppicks3 = "";
                            }
                            
                            $savedjob = $this->Jobpost_Model->job_saved($itcatjobData['id'], $userTokenCheck[0]['user_id']);
                            if(!empty($savedjob[0]['id'])) {
                                $savedjob = '1';
                            } else{
                                $savedjob = "0";
                            }

                            $jobsal = $this->Jobpost_Model->getExpJob($itcatjobData['id']);
                            if($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else{
                                $basicsalary = 0;
                            }

                            $companydetail = $this->Jobpost_Model->company_detail_fetch($itcatjobData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($itcatjobData['compId']);
                            if(isset($companydetail1[0]['address'])){
                                $companyaddress=$companydetail1[0]['address'];
                            }
                            else{
                                $companyaddress='';
                            }

                            $companyPic = $this->Jobpost_Model->fetch_companyPic($itcatjobData['compId']);
                            if(!empty($companyPic[0]['companyPic'])) {
                                $comPic = trim($companyPic[0]['companyPic']);
                            } else{
                                $comPic = "";
                            }

                            $jobImage = $this->Jobpost_Model->job_image($itcatjobData['id']);
                            if(isset($jobImage[0]['pic'])){
                                $jobImageData=$jobImage[0]['pic'];
                            }
                            else{
                                $jobImageData=$comPic;
                            }

                            $companyname = $this->Jobpost_Model->company_name_fetch($itcatjobData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            $distance = $this->distance($usercurlat,$usercurlong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', ''); 

                            $itcatjobs[] = [
                                    "jobpost_id" => $itcatjobData['id'],
                                    "comapnyId" =>$itcatjobData['compId'],
                                    "job_title" => $itcatjobData['jobtitle'],
                                    "jobDesc" => $itcatjobData['jobDesc'],
                                    "jobPitch"=>$itcatjobData['jobPitch'],
                                    "salary" => number_format($itcatjobData['salary']),
                                    "companyName"=> $companydetail[0]["cname"],
                                    "cname"=> $cname,
                                    "companyAddress"=>$companyaddress,
                                    "toppicks1" => $datatoppicks1,
                                    "toppicks2" => $datatoppicks2,
                                    "toppicks3" => $datatoppicks3,
                                    "job_image" =>$jobImageData,
                                    "latitude"=> $itcatjobData['latitude'], 
                                    "longitude"=>$itcatjobData['longitude'],
                                    "jobexpire"=> $itcatjobData['jobexpire'],
                                    "companyPic"=> $comPic,
                                    "savedjob" =>$savedjob,
                                    "boostjob" =>$itcatjobData['boost_status'],
                                    "distance" => $distance
                            ];
                        //}
                    }
                }


                // section 5
                $nearJob = $this->nearJobs2($userTokenCheck[0]['user_id'], $usercurlat, $usercurlong, $expfilter);


            }  else if($response_index == 4) {

                //  Job by No experience
                $nursingjobs = array();
                $negletarr = array();
                $nursingjobDatas = $this->Jobsapp_Model->homelist_nursing_groupby($userTokenCheck[0]['user_id'], $expfilter, $usercurlat,$usercurlong, [], $this->default_distance);
                foreach ($nursingjobDatas as $nursingjobData) {
                    $negletarr[] = $nursingjobData['id'];
                    $jobTop = $this->Jobpost_Model->job_toppicks($nursingjobData['id']);
                    //if(!empty($jobTop) && count($jobTop)>=0) {
                        if(isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id']; 
                        } else{
                            $datatoppicks1 = "";
                        }
                        if(isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id']; 
                        } else{
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id']; 
                        } else{
                            $datatoppicks3 = "";
                        }
                        
                        $savedjob = $this->Jobpost_Model->job_saved($nursingjobData['id'], $userTokenCheck[0]['user_id']);
                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }

                        $jobsal = $this->Jobpost_Model->getExpJob($nursingjobData['id']);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = 0;
                        }

                        $companydetail = $this->Jobpost_Model->company_detail_fetch($nursingjobData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($nursingjobData['compId']);
                        if(isset($companydetail1[0]['address'])){
                            $companyaddress=$companydetail1[0]['address'];
                        }
                        else{
                            $companyaddress='';
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($nursingjobData['compId']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }

                        $jobImage = $this->Jobpost_Model->job_image($nursingjobData['id']);
                        if(isset($jobImage[0]['pic'])){
                            $jobImageData=$jobImage[0]['pic'];
                        }
                        else{
                            $jobImageData=$comPic;
                        }

                        $companyname = $this->Jobpost_Model->company_name_fetch($nursingjobData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($usercurlat,$usercurlong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', ''); 

                        $nursingjobs[] = [
                                "jobpost_id" => $nursingjobData['id'],
                                "comapnyId" =>$nursingjobData['compId'],
                                "job_title" => $nursingjobData['jobtitle'],
                                "jobDesc" => $nursingjobData['jobDesc'],
                                "jobPitch"=>$nursingjobData['jobPitch'],
                                "salary" => number_format($nursingjobData['salary']),
                                "companyName"=> $companydetail[0]["cname"],
                                "cname"=> $cname,
                                "companyAddress"=>$companyaddress,
                                "toppicks1" => $datatoppicks1,
                                "toppicks2" => $datatoppicks2,
                                "toppicks3" => $datatoppicks3,
                                "job_image" =>$jobImageData,
                                "latitude"=> $nursingjobData['latitude'], 
                                "longitude"=>$nursingjobData['longitude'],
                                "jobexpire"=> $nursingjobData['jobexpire'],
                                "companyPic"=> $comPic,
                                "savedjob" =>$savedjob,
                                "boostjob" =>$nursingjobData['boost_status'],
                                "distance" => $distance
                        ];
                    //}
                }
                $nursingjobDatas = $this->Jobsapp_Model->homelist_nursing($userTokenCheck[0]['user_id'], $expfilter, $usercurlat,$usercurlong, $this->default_distance);
                foreach ($nursingjobDatas as $nursingjobData) {
                    if(in_array($nursingjobData['id'], $negletarr)) { } else {
                        $jobTop = $this->Jobpost_Model->job_toppicks($nursingjobData['id']);
                        //if(!empty($jobTop) && count($jobTop)>=0) {
                            if(isset($jobTop[0]['picks_id'])) {
                                $datatoppicks1 = $jobTop[0]['picks_id']; 
                            } else{
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $datatoppicks2 = $jobTop[1]['picks_id']; 
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop[2]['picks_id'])) {
                                $datatoppicks3 = $jobTop[2]['picks_id']; 
                            } else{
                                $datatoppicks3 = "";
                            }
                            
                            $savedjob = $this->Jobpost_Model->job_saved($nursingjobData['id'], $userTokenCheck[0]['user_id']);
                            if(!empty($savedjob[0]['id'])) {
                                $savedjob = '1';
                            } else{
                                $savedjob = "0";
                            }

                            $jobsal = $this->Jobpost_Model->getExpJob($nursingjobData['id']);
                            if($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else{
                                $basicsalary = 0;
                            }

                            $companydetail = $this->Jobpost_Model->company_detail_fetch($nursingjobData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($nursingjobData['compId']);
                            if(isset($companydetail1[0]['address'])){
                                $companyaddress=$companydetail1[0]['address'];
                            }
                            else{
                                $companyaddress='';
                            }

                            $companyPic = $this->Jobpost_Model->fetch_companyPic($nursingjobData['compId']);
                            if(!empty($companyPic[0]['companyPic'])) {
                                $comPic = trim($companyPic[0]['companyPic']);
                            } else{
                                $comPic = "";
                            }

                            $jobImage = $this->Jobpost_Model->job_image($nursingjobData['id']);
                            if(isset($jobImage[0]['pic'])){
                                $jobImageData=$jobImage[0]['pic'];
                            }
                            else{
                                $jobImageData=$comPic;
                            }

                            $companyname = $this->Jobpost_Model->company_name_fetch($nursingjobData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            $distance = $this->distance($usercurlat,$usercurlong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', ''); 

                            $nursingjobs[] = [
                                    "jobpost_id" => $nursingjobData['id'],
                                    "comapnyId" =>$nursingjobData['compId'],
                                    "job_title" => $nursingjobData['jobtitle'],
                                    "jobDesc" => $nursingjobData['jobDesc'],
                                    "jobPitch"=>$nursingjobData['jobPitch'],
                                    "salary" => number_format($nursingjobData['salary']),
                                    "companyName"=> $companydetail[0]["cname"],
                                    "cname"=> $cname,
                                    "companyAddress"=>$companyaddress,
                                    "toppicks1" => $datatoppicks1,
                                    "toppicks2" => $datatoppicks2,
                                    "toppicks3" => $datatoppicks3,
                                    "job_image" =>$jobImageData,
                                    "latitude"=> $nursingjobData['latitude'], 
                                    "longitude"=>$nursingjobData['longitude'],
                                    "jobexpire"=> $nursingjobData['jobexpire'],
                                    "companyPic"=> $comPic,
                                    "savedjob" =>$savedjob,
                                    "boostjob" =>$nursingjobData['boost_status'],
                                    "distance" => $distance
                            ];
                        //}
                    }
                }



                $featuredTopPicks = $this->getFeaturedTopPicks1($userTokenCheck[0]['user_id'], $usercurlat, $usercurlong, $expfilter, "indexingthree");


            }  else if($response_index == 5) {

                $featuredTopPicks = $this->getFeaturedTopPicks1($userTokenCheck[0]['user_id'], $usercurlat, $usercurlong, $expfilter, "indexingfour");


            }  else if($response_index == 6) {


                //Fetch videos
                $videosArray = array();
                $fetchvideos = $this->Newpoints_Model->fetchvideos();
                foreach($fetchvideos as $fetchvideo) {
                    $videosArray[] = ["id"=>$fetchvideo['id'],"video"=>$fetchvideo['video'],"banner"=>$fetchvideo['thumbnail'],"title"=>$fetchvideo['title'],"desc"=>$fetchvideo['description'],"posted"=>date("d F Y", strtotime($fetchvideo['updated_at']))];
                }

                //Fetch news
                $newsArray = array();
                $fetchnews = $this->Newpoints_Model->fetchnews();
                foreach($fetchnews as $fetchnew) {
                    $newsArray[] = ["id"=>$fetchnew['id'],"banner"=>$fetchnew['banner'],"title"=>$fetchnew['title'],"desc"=>$fetchnew['description'],"posted"=>date("d F Y", strtotime($fetchnew['updated_at']))];
                }

                $featuredTopPicks = $this->getFeaturedTopPicks1($userTokenCheck[0]['user_id'], $usercurlat, $usercurlong, $expfilter, "indexingfive");


                //  Job by leadership category
                $leadercatjobs = array();
                $negletarr = array();

                $catjobDatas = $this->Jobsapp_Model->homelist_leadership_groupby($userTokenCheck[0]['user_id'], $expfilter, $usercurlat,$usercurlong,[], $this->default_distance);
                foreach ($catjobDatas as $catjobData) {
                    $negletarr[] = $catjobData['id'];
                    $jobTop = $this->Jobpost_Model->job_toppicks($catjobData['id']);
                    //if(!empty($jobTop) && count($jobTop)>=0) {
                          //echo "</pre>";    
                        if(isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id']; 
                        } else{
                            $datatoppicks1 = "";
                        }
                        if(isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id']; 
                        } else{
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id']; 
                        } else{
                            $datatoppicks3 = "";
                        }
                        
                        $savedjob = $this->Jobpost_Model->job_saved($catjobData['id'], $userTokenCheck[0]['user_id']);
                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }

                        $jobsal = $this->Jobpost_Model->getExpJob($catjobData['id']);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = 0;
                        }

                        $companydetail = $this->Jobpost_Model->company_detail_fetch($catjobData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($catjobData['compId']);
                        if(isset($companydetail1[0]['address'])){
                            $companyaddress=$companydetail1[0]['address'];
                        }
                        else{
                            $companyaddress='';
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($catjobData['compId']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }

                        $jobImage = $this->Jobpost_Model->job_image($catjobData['id']);
                        if(isset($jobImage[0]['pic'])){
                            $jobImageData=$jobImage[0]['pic'];
                        }
                        else{
                            $jobImageData=$comPic;
                        }

                        $companyname = $this->Jobpost_Model->company_name_fetch($catjobData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($usercurlat,$usercurlong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', ''); 

                        $leadercatjobs[] = [
                                "jobpost_id" => $catjobData['id'],
                                "comapnyId" =>$catjobData['compId'],
                                "job_title" => $catjobData['jobtitle'],
                                "jobDesc" => $catjobData['jobDesc'],
                                "jobPitch"=>$catjobData['jobPitch'],
                                "salary" => number_format($catjobData['salary']),
                                "companyName"=> $companydetail[0]["cname"],
                                "cname"=> $cname,
                                "companyAddress"=>$companyaddress,
                                "toppicks1" => $datatoppicks1,
                                "toppicks2" => $datatoppicks2,
                                "toppicks3" => $datatoppicks3,
                                "job_image" =>$jobImageData,
                                "latitude"=> $catjobData['latitude'], 
                                "longitude"=>$catjobData['longitude'],
                                "jobexpire"=> $catjobData['jobexpire'],
                                "companyPic"=> $comPic,
                                "savedjob" =>$savedjob,
                                "boostjob" =>$catjobData['boost_status'],
                                "distance" => $distance
                        ];
                    //}
                }
                $catjobDatas = $this->Jobsapp_Model->homelist_leadership($userTokenCheck[0]['user_id'], $expfilter, $usercurlat,$usercurlong, $this->default_distance);
                foreach ($catjobDatas as $catjobData) {

                    if(in_array($catjobData['id'], $negletarr)) { } else {
                        $jobTop = $this->Jobpost_Model->job_toppicks($catjobData['id']);
                        if(!empty($jobTop) && count($jobTop)>=0) {
                            if(isset($jobTop[0]['picks_id'])) {
                                $datatoppicks1 = $jobTop[0]['picks_id']; 
                            } else{
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $datatoppicks2 = $jobTop[1]['picks_id']; 
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop[2]['picks_id'])) {
                                $datatoppicks3 = $jobTop[2]['picks_id']; 
                            } else{
                                $datatoppicks3 = "";
                            }
                            
                            $savedjob = $this->Jobpost_Model->job_saved($catjobData['id'], $userTokenCheck[0]['user_id']);
                            if(!empty($savedjob[0]['id'])) {
                                $savedjob = '1';
                            } else{
                                $savedjob = "0";
                            }

                            $jobsal = $this->Jobpost_Model->getExpJob($catjobData['id']);
                            if($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else{
                                $basicsalary = 0;
                            }

                            $companydetail = $this->Jobpost_Model->company_detail_fetch($catjobData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($catjobData['compId']);
                            if(isset($companydetail1[0]['address'])){
                                $companyaddress=$companydetail1[0]['address'];
                            }
                            else{
                                $companyaddress='';
                            }

                            $companyPic = $this->Jobpost_Model->fetch_companyPic($catjobData['compId']);
                            if(!empty($companyPic[0]['companyPic'])) {
                                $comPic = trim($companyPic[0]['companyPic']);
                            } else{
                                $comPic = "";
                            }

                            $jobImage = $this->Jobpost_Model->job_image($catjobData['id']);
                            if(isset($jobImage[0]['pic'])){
                                $jobImageData=$jobImage[0]['pic'];
                            }
                            else{
                                $jobImageData=$comPic;
                            }

                            $companyname = $this->Jobpost_Model->company_name_fetch($catjobData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            $distance = $this->distance($usercurlat,$usercurlong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', ''); 

                            $leadercatjobs[] = [
                                    "jobpost_id" => $catjobData['id'],
                                    "comapnyId" =>$catjobData['compId'],
                                    "job_title" => $catjobData['jobtitle'],
                                    "jobDesc" => $catjobData['jobDesc'],
                                    "jobPitch"=>$catjobData['jobPitch'],
                                    "salary" => number_format($catjobData['salary']),
                                    "companyName"=> $companydetail[0]["cname"],
                                    "cname"=> $cname,
                                    "companyAddress"=>$companyaddress,
                                    "toppicks1" => $datatoppicks1,
                                    "toppicks2" => $datatoppicks2,
                                    "toppicks3" => $datatoppicks3,
                                    "job_image" =>$jobImageData,
                                    "latitude"=> $catjobData['latitude'], 
                                    "longitude"=>$catjobData['longitude'],
                                    "jobexpire"=> $catjobData['jobexpire'],
                                    "companyPic"=> $comPic,
                                    "savedjob" =>$savedjob,
                                    "boostjob" =>$catjobData['boost_status'],
                                    "distance" => $distance
                            ];
                        }
                    }
                }


            } else {

                //section 6
                //$featuredTopPicks = $this->getFeaturedTopPicks1($userTokenCheck[0]['user_id'], $usercurlat, $usercurlong, $expfilter, $companyidsarray);

            } // End indexing

            $getCompletenes = $this->fetchUserCompleteData($userTokenCheck[0]['user_id']);  
            if($userData[0]['exp_month']>=0   && $userData[0]['exp_year']>=0){
                $exp_added = "No";
            }  else{
                $exp_added = 'Yes';
            }
            $completeness = $getCompletenes['comp'];
            if($completeness<=14 || $exp_added== 'Yes'){
                $completeness = "Yes";
            }else{
                $completeness = "No";
            }
            $fetchnotifications = $this->Candidate_Model->fetchnotifications($userTokenCheck[0]['user_id']);
            $fetchpromonotifications = $this->Candidate_Model->fetchpromonotifications($userTokenCheck[0]['user_id']);
            $fetchjobnotifications = $this->Candidate_Model->fetchjobnotifications($userTokenCheck[0]['user_id']);

            $tokendata = ['id'=>$userTokenCheck[0]['user_id']];
            $interestedCheck = $this->User_Model->token_match($tokendata);
            if($interestedCheck) {
                if(strlen($interestedCheck[0]['jobsInterested']) > 1) {
                    $interestedHave = 1;
                } else {
                    $interestedHave = 0;
                }
            } else {
                $interestedHave = 0;
            }

            if($response_index == 0) {

                $response = ['status'=>"SUCCESS", 'section1'=>$ad_lists1, 'section2'=>$companylist, 'activelyjobs'=>$activelyJob, 'noexpjobs' =>[], 'citylist'=>[], 'toprecruiters'=>[], 'section3'=>[], "reviewlist"=>[],  'hotjobslist' =>[],'nearjob'=>[],'nursingjobs'=>[], "leadershipjobs"=>[], "itjobs"=>[], "monthpay"=>[], "shiftjobs"=>[], "retirement"=>[], "day1hmo"=>[], "freefood"=>[], "bonus"=>[], "workfromhome"=>[], 'videos'=>[], 'news'=>[], 'completeness'=>$completeness,'notification_count' => count($fetchnotifications)+count($fetchpromonotifications)+count($fetchjobnotifications), "interestedHave"=>$interestedHave];
            
            } else if($response_index == 1) {

                $response = ['status'=>"SUCCESS", 'section1'=>[], 'section2'=>[], 'activelyjobs'=>[], 'noexpjobs'=>$noexpjobs, 'citylist'=>$citylist, 'toprecruiters'=>$topCompanylist, 'section3'=>[], "reviewlist"=>[],  'hotjobslist' =>[],'nearjob'=>[],'nursingjobs'=>[], "leadershipjobs"=>[], "itjobs"=>[], "monthpay"=>[], "shiftjobs"=>[], "retirement"=>[], "day1hmo"=>[], "freefood"=>[], "bonus"=>[], "workfromhome"=>[], 'videos'=>[], 'news'=>[], 'completeness'=>$completeness,'notification_count' => count($fetchnotifications)+count($fetchpromonotifications)+count($fetchjobnotifications), "interestedHave"=>$interestedHave];
            
            } else if($response_index == 2) {

                $response = ['status'=>"SUCCESS", 'section1'=>[], 'section2'=>[], 'activelyjobs'=>[], 'noexpjobs'=>[], 'citylist'=>[], 'toprecruiters'=>[], 'section3'=>$categorylist, "reviewlist"=>$reviewlist,  'hotjobslist'=>$hotjobss, 'nearjob'=>[],'nursingjobs'=>[], 'workfromhome'=>[], "leadershipjobs"=>[], "itjobs"=>[], "monthpay"=>[], "shiftjobs"=>[], "retirement"=>[], "day1hmo"=>[], "freefood"=>[], "bonus"=>[], 'videos'=>[], 'news'=>[], 'completeness'=>$completeness,'notification_count' => count($fetchnotifications)+count($fetchpromonotifications)+count($fetchjobnotifications), "interestedHave"=>$interestedHave];
            
            } else if($response_index == 3) {

                $response = ['status'=>"SUCCESS", 'section1'=>[], 'section2'=>[], 'activelyjobs'=>[], 'noexpjobs' =>[], 'citylist'=>[], 'toprecruiters'=>[], 'section3'=>[], "reviewlist"=>[], 'hotjobslist'=>[],'nearjob'=>$nearJob, 'nursingjobs'=>[], 'workfromhome'=>$featuredTopPicks['workfromhome'], "leadershipjobs"=>[], "itjobs"=>$itcatjobs, "monthpay"=>[], "shiftjobs"=>[], "retirement"=>[], "day1hmo"=>[], "freefood"=>[], "bonus"=>[], 'videos'=>[], 'news'=>[], 'completeness'=>$completeness,'notification_count' => count($fetchnotifications)+count($fetchpromonotifications)+count($fetchjobnotifications), "interestedHave"=>$interestedHave];
            
            } else if($response_index == 4) {

                $response = ['status'=>"SUCCESS", 'section1'=>[], 'section2'=>[], 'activelyjobs'=>[], 'noexpjobs' =>[], 'citylist'=>[], 'toprecruiters'=>[], 'section3'=>[], "reviewlist"=>[], 'hotjobslist' =>[],'nearjob'=>[],'nursingjobs'=>$nursingjobs, 'workfromhome'=>[], "leadershipjobs"=>[], "itjobs"=>[], "monthpay"=>$featuredTopPicks['monthpay'], "shiftjobs"=>$featuredTopPicks['shiftjobs'], "retirement"=>[], "day1hmo"=>[], "freefood"=>[], "bonus"=>[], 'videos'=>[], 'news'=>[], 'completeness'=>$completeness,'notification_count' => count($fetchnotifications)+count($fetchpromonotifications)+count($fetchjobnotifications), "interestedHave"=>$interestedHave];
            
            } else if($response_index == 5) {

                $response = ['status'=>"SUCCESS", 'section1'=>[], 'section2'=>[], 'activelyjobs'=>[], 'noexpjobs' =>[], 'citylist'=>[], 'toprecruiters'=>[], 'section3'=>[], "reviewlist"=>[], 'hotjobslist' =>[],'nearjob'=>[],'nursingjobs'=>[], 'workfromhome'=>[], "leadershipjobs"=>[], "itjobs"=>[], "monthpay"=>[], "shiftjobs"=>[], "retirement"=>$featuredTopPicks['retirement'], "day1hmo"=>$featuredTopPicks['day1hmo'], "freefood"=>$featuredTopPicks['freefood'], "bonus"=>[], 'videos'=>[], 'news'=>[], 'completeness'=>$completeness,'notification_count' => count($fetchnotifications)+count($fetchpromonotifications)+count($fetchjobnotifications), "interestedHave"=>$interestedHave];
            
            } else if($response_index == 6) {

                $response = ['status'=>"SUCCESS", 'section1'=>[], 'section2'=>[], 'activelyjobs'=>[], 'noexpjobs' =>[], 'citylist'=>[], 'toprecruiters'=>[], 'section3'=>[], "reviewlist"=>[], 'hotjobslist' =>[],'nearjob'=>[],'nursingjobs'=>[], 'workfromhome'=>[], "leadershipjobs"=>$leadercatjobs, "itjobs"=>[], "monthpay"=>[], "shiftjobs"=>[], "retirement"=>[], "day1hmo"=>[], "freefood"=>[], "bonus"=>$featuredTopPicks['bonus'], 'videos'=>$videosArray, 'news'=>$newsArray, 'completeness'=>$completeness,'notification_count' => count($fetchnotifications)+count($fetchpromonotifications)+count($fetchjobnotifications), "interestedHave"=>$interestedHave];

            } else {

                $response = ['status'=>"SUCCESS", 'section2'=>$companylist, 'activelyjobs'=>[], 'noexpjobs'=>$noexpjobs, 'citylist'=>$citylist, 'section1'=>$ad_lists1, 'toprecruiters'=>[], "reviewlist"=>[], 'section3'=>$categorylist, 'hotjobslist' => $hotjobss,'nearjob'=>$nearJob,'nursingjobs'=>$nursingjobs, "leadershipjobs"=>$leadercatjobs, "itjobs"=>$itcatjobs, "monthpay"=> $featuredTopPicks['monthpay'] , "shiftjobs"=> $featuredTopPicks['shiftjobs'], "retirement"=> $featuredTopPicks['retirement'], "day1hmo"=> $featuredTopPicks['day1hmo'], "freefood"=> $featuredTopPicks['freefood'], "bonus"=> $featuredTopPicks['bonus'], "workfromhome"=> $featuredTopPicks['workfromhome'], 'videos'=>$videosArray, 'news'=>$newsArray, 'completeness'=>$completeness,'notification_count' => count($fetchnotifications)+count($fetchpromonotifications)+count($fetchjobnotifications), "interestedHave"=>$interestedHave];    
            }

            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } else {

            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function viewallListingsnew_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        
        $userTokenCheck1 = $this->User_Model->token_match1($data);

        $userLat = $userData['cur_lat'];
        $userLong = $userData['cur_long'];

        $indexing = $userData['indexing'];
        if(!empty($userData['offset'])) {
            $offset = $userData['offset'];
            if($offset != 0) {
                $offset = $offset + 1;
            }
        } else {
            $offset = 0;
        }
        if(!empty($userData['limit'])) {
            $limit = 20;
        } else {
            $limit = 20;
        }

        if($userTokenCheck1) {
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            
            if($userTokenCheck) {
                $expmonth = $userTokenCheck[0]['exp_month'];
                $expyear = $userTokenCheck[0]['exp_year'];

                if($expyear == 0 && $expmonth == 0) {
                    $expfilter = "1";
                }else if($expyear == 0 && $expmonth < 6) {
                    $expfilter = "2";
                } else if($expyear < 1 && $expmonth >= 6) {
                    $expfilter = "3";
                } else if($expyear < 2 && $expyear >= 1) {
                    $expfilter = "4";
                } else if($expyear < 3 && $expyear >= 2) {
                    $expfilter = "5";
                }else if($expyear < 4 && $expyear >= 3) {
                    $expfilter = "6";
                }else if($expyear < 5 && $expyear >= 4) {
                    $expfilter = "7";
                }else if($expyear < 6 && $expyear >= 5) {
                    $expfilter = "8";
                }else if($expyear < 7 && $expyear >= 6) {
                    $expfilter = "9";
                } else if($expyear >= 7) { 
                    $expfilter = "10";
                } else {
                    $expfilter = "";
                }

                $company_lists = $this->Newpoints_Model->companysite_lists($userLat,$userLong);
                if(!empty($company_lists)) {
                    $x=0;
                    
                    $companyidsarray = array();
                    foreach ($company_lists as $company_list) {
                        $parent = $this->Newpoints_Model->company_parent($company_list['parent_id']);

                        $jobcountfetch = $this->Newpoints_Model->companysite_jobcount($userTokenCheck[0]['user_id'], $company_list['id'], $expfilter, $userLat,$userLong);
                        if($jobcountfetch > 0) {
                            
                            $companyidsarray[] = $company_list['parent_id'];
                            $x++;
                        }
                    }
                } else {
                    $companylist = [];
                }
                $companyidsarray = array_unique($companyidsarray);

                if(strlen($userData['toppicks']) > 0) {
                    $toppicksArr = explode(',',$userData['toppicks']);
                    foreach($toppicksArr as $toppicksAr) {
                        $toppickss[] = $toppicksAr;
                    }
                }
                if(strlen($userData['allowances']) > 0) {
                    $allowanesArr = explode(',',$userData['allowances']);
                    foreach($allowanesArr as $allowanesAr) {
                        $allowaness[] = $allowanesAr;
                    }
                }
                if(strlen($userData['medical']) > 0) {
                    $medicalArr = explode(',',$userData['medical']);
                    foreach($medicalArr as $medicalAr) {
                        $medicals[] = $medicalAr;
                    }
                }
                if(strlen($userData['workshift']) > 0) {
                    $workshiftArr = explode(',',$userData['workshift']);
                    foreach($workshiftArr as $workshiftAr) {
                        $workshifts[] = $workshiftAr;
                    }
                }
                if(strlen($userData['leaves']) > 0) {
                    $leavesArr = explode(',',$userData['leaves']);
                    foreach($leavesArr as $leavesAr) {
                        $leavess[] = $leavesAr;
                    }
                }
                $filter_values = [
                    "sorting" => $userData['Sort'],
                    "location" => $userData['location'],
                    "lat" => $userData['lat'],
                    "long" => $userData['long'],
                    "getcity" => $userData['city'],
                    "basicSalary" => $userData['basicSalary'],
                    "toppicks" => $toppickss,
                    "allowances" => $allowaness,
                    "medical" => $medicals,
                    "workshift" => $workshifts,
                    "leaves" => $leavess,
                ];

                $data1 = array();
                $data2 = array();
                $data3 = array();
                $data4 = array();
                $negletarr = array();

                if($userData['type'] == "hotjob") {

                    $bostArr = array();

                    if($indexing == 1) {
                    
                        $hotjobData = $this->Jobsapp_Model->hotjob_fetch_latlongbylimit_groupby($userTokenCheck[0]['id'],$expfilter, $userLat, $userLong, $filter_values, $this->default_distance);
                        foreach ($hotjobData as $hotjobsData) {

                            $negletarr[] = $hotjobsData['id'];
                            $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                            if(!empty($jobTop) && count($jobTop)>=1) {
                                if(isset($jobTop[0]['picks_id'])){
                                    $datatoppicks1 = $jobTop[0]['picks_id']; 
                                } else {
                                    $datatoppicks1 = "";
                                }
                                if(isset($jobTop[1]['picks_id'])) {
                                    $datatoppicks2 = $jobTop[1]['picks_id']; 
                                } else {
                                    $datatoppicks2 = "";
                                }
                                if(isset($jobTop[2]['picks_id'])) {
                                    $datatoppicks3 = $jobTop[2]['picks_id']; 
                                } else {
                                    $datatoppicks3 = "";
                                }
                                
                                $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['id']);

                                if(!empty($savedjob[0]['id'])) {
                                    $savedjob = '1';
                                } else{
                                    $savedjob = "0";
                                }

                                 $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);

                                if($jobsal[0]['basicsalary']) {
                                    $basicsalary = $jobsal[0]['basicsalary'];
                                } else{
                                    $basicsalary = 0;
                                }

                                $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                                if(isset($companydetail1[0]['address'])) {
                                    $companyaddress=$companydetail1[0]['address'];
                                }else {
                                    $companyaddress='';
                                }

                                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }
                                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);

                                if(isset($jobImage[0]['pic'])) {
                                    $jobImageData=$jobImage[0]['pic'];
                                }else{
                                    $jobImageData=$comPic;
                                }

                                $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                                if(!empty($companyname[0]["cname"])){
                                    $cname = $companyname[0]["cname"];
                                }else{
                                    $cname = '';
                                }

                                if($hotjobsData['chatbot'] == 1) {
                                    $hotjobsData['mode'] = "chatbot";
                                }

                                $distance = $this->distance($userLat,$userLong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                $distance = number_format((float)$distance, 2, '.', ''); 

                                $data1[] = [
                                    "jobpost_id" => $hotjobsData['id'],
                                    "comapnyId" =>$hotjobsData['compId'],
                                    "job_title" => $hotjobsData['jobtitle'],
                                    "jobDesc" => $hotjobsData['jobDesc'],
                                    "salary" => number_format($hotjobsData['salary']),
                                    "companyName"=> $companydetail[0]["cname"],
                                    "cname"=> $cname,
                                    "companyAddress"=>$companyaddress,
                                    "toppicks1" => $datatoppicks1,
                                    "toppicks2" => $datatoppicks2,
                                    "toppicks3" => $datatoppicks3,
                                    "job_image" =>$jobImageData,
                                    "latitude"=> $hotjobsData['latitude'], 
                                    "longitude"=>$hotjobsData['longitude'],
                                    "jobexpire"=> $hotjobsData['jobexpire'],
                                    "companyPic"=> $comPic,
                                    "savedjob" =>$savedjob,
                                    "boostjob" =>$hotjobsData['boost_status'],
                                    "distance" => $distance,
                                    "mode"=> $hotjobsData['mode'],
                                    "modeurl"=> $hotjobsData['modeurl'],
                                    "sharecount"=>$this->getsharecount($hotjobsData['id'])
                                ];
                            }   
                        }

                        // if($filter_values['sorting'] == 1) {
                        //     usort($data1, function($a, $b) {
                        //         return $b['salary'] <=> $a['salary'];
                        //     });
                        // } else {
                        //     if($filter_values['sorting'] == 2) {
                        //         usort($data1, function($a, $b) {
                        //             return $a['distance'] <=> $b['distance'];
                        //         });
                        //     } else {
                        //         usort($data1, function($a, $b) {
                        //             return $a['distance'] <=> $b['distance'];
                        //         });
                        //     }
                        // }

                        $boostjobData = $this->Jobsapp_Model->boostjob_fetch_latlong($userTokenCheck[0]['id'],$expfilter, $userLat, $userLong, $filter_values, $this->default_distance);
                        foreach ($boostjobData as $boostjobsData) {

                            if(in_array($boostjobsData['id'], $negletarr)) { } else {
                                $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
                                //if(!empty($jobTop) && count($jobTop)>=0) {
                                    if(isset($jobTop[0]['picks_id'])) {
                                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                                    } else{
                                        $datatoppicks1 = "";
                                    }
                                    if(isset($jobTop[1]['picks_id'])) {
                                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                                    } else{
                                        $datatoppicks2 = "";
                                    }
                                    if(isset($jobTop[2]['picks_id'])) {
                                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                                    } else{
                                        $datatoppicks3 = "";
                                    }
                                    
                                    $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userTokenCheck[0]['id']);
                                    if(!empty($savedjob[0]['id'])) {
                                        $savedjob = '1';
                                    } else{
                                        $savedjob = "0";
                                    }

                                    $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                                    if($jobsal[0]['basicsalary']) {
                                        $basicsalary = $jobsal[0]['basicsalary'];
                                    } else{
                                        $basicsalary = 0;
                                    }

                                    $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                                    if(isset($companydetail1[0]['address'])){
                                        $companyaddress=$companydetail1[0]['address'];
                                    }
                                    else{
                                        $companyaddress='';
                                    }

                                    $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                                    if(!empty($companyPic[0]['companyPic'])) {
                                        $comPic = trim($companyPic[0]['companyPic']);
                                    } else{
                                        $comPic = "";
                                    }

                                    $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                                    if(isset($jobImage[0]['pic'])){
                                        $jobImageData=$jobImage[0]['pic'];
                                    }
                                    else{
                                        $jobImageData=$comPic;
                                    }

                                    $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                                    if(!empty($companyname[0]["cname"])){
                                        $cname = $companyname[0]["cname"];
                                    }else{
                                        $cname = '';
                                    }

                                    if($boostjobsData['chatbot'] == 1) {
                                        $boostjobsData['mode'] = "chatbot";
                                    }

                                    $distance = $this->distance($userLat,$userLong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                    $distance = number_format((float)$distance, 2, '.', ''); 

                                    $bostArr[] = $boostjobsData['id'];

                                    $data2[] = [
                                            "jobpost_id" => $boostjobsData['id'],
                                            "comapnyId" =>$boostjobsData['compId'],
                                            "job_title" => $boostjobsData['jobtitle'],
                                            "jobDesc" => $boostjobsData['jobDesc'],
                                            "jobPitch"=>$boostjobsData['jobPitch'],
                                            "salary" => number_format($boostjobsData['salary']),
                                            "companyName"=> $companydetail[0]["cname"],
                                            "cname"=> $cname,
                                            "companyAddress"=>$companyaddress,
                                            "toppicks1" => $datatoppicks1,
                                            "toppicks2" => $datatoppicks2,
                                            "toppicks3" => $datatoppicks3,
                                            "job_image" =>$jobImageData,
                                            "latitude"=> $boostjobsData['latitude'], 
                                            "longitude"=>$boostjobsData['longitude'],
                                            "jobexpire"=> $boostjobsData['jobexpire'],
                                            "companyPic"=> $comPic,
                                            "savedjob" =>$savedjob,
                                            "boostjob" =>$boostjobsData['boost_status'],
                                            "distance" => $distance,
                                            "mode"=> $boostjobsData['mode'],
                                            "modeurl"=> $boostjobsData['modeurl'],
                                            "sharecount"=>$this->getsharecount($boostjobsData['id'])
                                    ];
                                //}
                            }
                        }

                        if($filter_values['sorting'] == 1) {
                            usort($data2, function($a, $b) {
                                return $b['salary'] <=> $a['salary'];
                            });
                        } else {
                            if($filter_values['sorting'] == 2) {
                                usort($data2, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            } else {
                                usort($data2, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            }
                        }

                        $data3 = array_merge($data1, $data2);
                        $data1 = $data3;

                    } else {

                        $hotjobData = $this->Jobsapp_Model->hotjob_fetch_latlongbylimit_groupby($userTokenCheck[0]['id'],$expfilter, $userLat, $userLong, $filter_values, $this->default_distance);
                        foreach ($hotjobData as $hotjobsData) {
                            $negletarr[] = $hotjobsData['id'];
                        }

                        $boostjobData = $this->Jobsapp_Model->boostjob_fetch_latlong($userTokenCheck[0]['id'],$expfilter, $userLat, $userLong, $filter_values, $this->default_distance);
                        foreach ($boostjobData as $boostjobsData) {
                            if(in_array($boostjobsData['id'], $negletarr)) { } else {
                                $bostArr[] = $boostjobsData['id'];
                            }
                        }
                        $combineArray = array_merge($negletarr, $bostArr);

                        $hotjobData = $this->Jobsapp_Model->hotjob_fetch_latlong($userTokenCheck[0]['id'],$expfilter, $userLat, $userLong, $offset, $limit, $filter_values, $this->default_distance);

                        foreach ($hotjobData as $hotjobsData) {

                            if(in_array($hotjobsData['id'], $combineArray)) { 
                            } else {
                                $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                                if(!empty($jobTop) && count($jobTop)>=1) {
                                    
                                    if(isset($jobTop[0]['picks_id'])){
                                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                                    } else {
                                        $datatoppicks1 = "";
                                    }
                                    if(isset($jobTop[1]['picks_id'])) {
                                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                                    } else {
                                        $datatoppicks2 = "";
                                    }
                                    if(isset($jobTop[2]['picks_id'])) {
                                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                                    } else {
                                        $datatoppicks3 = "";
                                    }
                                    
                                    $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['id']);
                                    if(!empty($savedjob[0]['id'])) {
                                        $savedjob = '1';
                                    } else{
                                        $savedjob = "0";
                                    }

                                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                                    if($jobsal[0]['basicsalary']) {
                                        $basicsalary = $jobsal[0]['basicsalary'];
                                    } else{
                                        $basicsalary = 0;
                                    }

                                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                                    if(isset($companydetail1[0]['address']))
                                    {
                                        $companyaddress=$companydetail1[0]['address'];
                                    }
                                    else
                                    {
                                        $companyaddress='';
                                    }

                                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                                    if(!empty($companyPic[0]['companyPic'])) {
                                        $comPic = trim($companyPic[0]['companyPic']);
                                    } else{
                                        $comPic = "";
                                    }

                                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                                    if(isset($jobImage[0]['pic'])) {
                                        $jobImageData=$jobImage[0]['pic'];
                                    }else{
                                        $jobImageData=$comPic;
                                    }

                                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                                    if(!empty($companyname[0]["cname"])){
                                        $cname = $companyname[0]["cname"];
                                    }else{
                                        $cname = '';
                                    }

                                    if($hotjobsData['chatbot'] == 1) {
                                        $hotjobsData['mode'] = "chatbot";
                                    }

                                    $distance = $this->distance($userLat,$userLong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                    $distance = number_format((float)$distance, 2, '.', ''); 

                                    $data1[] = [
                                        "jobpost_id" => $hotjobsData['id'],
                                        "comapnyId" =>$hotjobsData['compId'],
                                        "job_title" => $hotjobsData['jobtitle'],
                                        "jobDesc" => $hotjobsData['jobDesc'],
                                        "salary" => number_format($hotjobsData['salary']),
                                        "companyName"=> $companydetail[0]["cname"],
                                        "cname"=> $cname,
                                        "companyAddress"=>$companyaddress,
                                        "toppicks1" => $datatoppicks1,
                                        "toppicks2" => $datatoppicks2,
                                        "toppicks3" => $datatoppicks3,
                                        "job_image" =>$jobImageData,
                                        "latitude"=> $hotjobsData['latitude'], 
                                        "longitude"=>$hotjobsData['longitude'],
                                        "jobexpire"=> $hotjobsData['jobexpire'],
                                        "companyPic"=> $comPic,
                                        "savedjob" =>$savedjob,
                                        "boostjob" =>$hotjobsData['boost_status'],
                                        "distance" => $distance,
                                        "mode"=> $hotjobsData['mode'], 
                                        "modeurl"=> $hotjobsData['modeurl'],
                                        "sharecount"=>$this->getsharecount($hotjobsData['id'])
                                    ];
                                }
                            }
                        }

                        if($filter_values['sorting'] == 1) {
                            usort($data1, function($a, $b) {
                                return $b['salary'] <=> $a['salary'];
                            });
                        } else {
                            if($filter_values['sorting'] == 2) {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            } else {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            }
                        }
                    }
                
                } else if($userData['type'] == "company") {

                    $company_lists = $this->Newpoints_Model->companysite_lists($userLat, $userLong);
                    if(!empty($company_lists)) {
                        $x=0;
                        $tokendata = ['id'=>$userTokenCheck[0]['id']];
                        $userData = $this->User_Model->token_match($tokendata);
                        $expmonth = $userData[0]['exp_month'];
                        $expyear = $userData[0]['exp_year'];

                        if($expyear == 0 && $expmonth == 0) {
                            $expfilter = "1";
                        }else if($expyear == 0 && $expmonth < 6) {
                            $expfilter = "2";
                        } else if($expyear < 1 && $expmonth >= 6) {
                            $expfilter = "3";
                        } else if($expyear < 2 && $expyear >= 1) {
                            $expfilter = "4";
                        } else if($expyear < 3 && $expyear >= 2) {
                            $expfilter = "5";
                        }else if($expyear < 4 && $expyear >= 3) {
                            $expfilter = "6";
                        }else if($expyear < 5 && $expyear >= 4) {
                            $expfilter = "7";
                        }else if($expyear < 6 && $expyear >= 5) {
                            $expfilter = "8";
                        }else if($expyear < 7 && $expyear >= 6) {
                            $expfilter = "9";
                        } else if($expyear >= 7) { 
                            $expfilter = "10";
                        } else {
                            $expfilter = "";
                        }
                        foreach ($company_lists as $company_list) {
                            $jobcountfetch = $this->Newpoints_Model->companysite_jobcount($userTokenCheck[0]['id'], $company_list['id']);
                            if($jobcountfetch > 0) {
                                if(strlen($company_list['companyPic']) > 0) {
                                    $companylist[$x] = [
                                          "id" => $company_list['id'],
                                          "image" => $company_list['companyPic'],
                                          "cname" => $company_list['cname'],
                                          "jobcount" => $jobcountfetch
                                    ];
                                    $x++;
                                }
                            }
                        }
                    } else {
                        $companylist = [];
                    }

                    $jobcount = array();
                    foreach ($companylist as $key => $row)
                    {
                        $jobcount[$key] = $row['jobcount'];
                    }
                    array_multisort($jobcount, SORT_DESC, $companylist);
                    $data1["hotjobss"] = $companylist;


                } else if($userData['type'] == "appreview") {

                    $review_lists = $this->Jobsapp_Model->appreviews_lists();
                    if(!empty($review_lists)) {
                        $x=0;
                        
                        foreach ($review_lists as $review_list) {

                            $company_list = $this->Newpoints_Model->companysite_lists_byid($review_list['company']);
                            if(strlen($company_list[0]['companyPic']) > 0) {
                                $comapnyPic = $company_list[0]['companyPic'];
                            } else {
                                $parentPic = $this->Jobpost_Model->fetch_companyPic($company_list[0]['parent_id']);
                                $comapnyPic = $parentPic[0]['companyPic'];
                            }
                            
                            $companylist[$x] = [
                                  "id" => $review_list['id'],
                                  "name" => $review_list['name'],
                                  "image" => base_url() .$review_list['image'],
                                  "comment" => $review_list['rate_desc'],
                                  "rate" => $review_list['rate'],
                                  "date" => $review_list['rating_date'],
                                  "company_pic" => $comapnyPic,
                                  "cname" => $company_list[0]['cname'],
                                  "companyimage" => base_url() .$review_list['companyimage'],
                            ];
                            $x++;
                        }
                    } else {
                        $companylist = [];
                    }

                    $data1 = $companylist;


                } else if($userData['type'] == "category") {

                    if($indexing == 1) {

                        $hotjobData = $this->Jobsapp_Model->categoryjob_fetch_latlong($userTokenCheck[0]['id'],$expfilter, $userData['typeid'], $userData['cur_lat'], $userData['cur_long'], $filter_values, $this->default_distance);
                        foreach ($hotjobData as $hotjobsData) {

                            $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                            if(isset($jobTop[0]['picks_id'])){
                                $datatoppicks1 = $jobTop[0]['picks_id']; 
                            } else {
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $datatoppicks2 = $jobTop[1]['picks_id']; 
                            } else {
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop[2]['picks_id'])) {
                                $datatoppicks3 = $jobTop[2]['picks_id']; 
                            } else {
                                $datatoppicks3 = "";
                            }
                            
                            $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['id']);
                            if(!empty($savedjob[0]['id'])) {
                                $savedjob = '1';
                            } else{
                                $savedjob = "0";
                            }

                            $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                            if($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else{
                                $basicsalary = 0;
                            }

                            $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                            if(isset($companydetail1[0]['address']))
                            {
                                $companyaddress=$companydetail1[0]['address'];
                            }
                            else
                            {
                                $companyaddress='';
                            }

                            $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                            if(!empty($companyPic[0]['companyPic'])) {
                                $comPic = trim($companyPic[0]['companyPic']);
                            } else{
                                $comPic = "";
                            }
                            $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                            if(isset($jobImage[0]['pic'])) {
                                $jobImageData=$jobImage[0]['pic'];
                            } else {
                                $jobImageData=$comPic;
                            }

                            $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            if($hotjobsData['chatbot'] == 1) {
                                $hotjobsData['mode'] = "chatbot";
                            }

                            $distance = $this->distance($userData['cur_lat'], $userData['cur_long'],$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', ''); 

                            $data1[] = [
                                "jobpost_id" => $hotjobsData['id'],
                                "comapnyId" =>$hotjobsData['compId'],
                                "job_title" => $hotjobsData['jobtitle'],
                                "jobDesc" => $hotjobsData['jobDesc'],
                                "salary" => number_format($hotjobsData['salary']),
                                "companyName"=> $companydetail[0]["cname"],
                                "cname"=> $cname,
                                "companyAddress"=>$companyaddress,
                                "toppicks1" => $datatoppicks1,
                                "toppicks2" => $datatoppicks2,
                                "toppicks3" => $datatoppicks3,
                                "job_image" =>$jobImageData,
                                "latitude"=> $hotjobsData['latitude'], 
                                "longitude"=>$hotjobsData['longitude'],
                                "jobexpire"=> $hotjobsData['jobexpire'],
                                "companyPic"=> $comPic,
                                "savedjob" =>$savedjob,
                                "boostjob" =>$hotjobsData['boost_status'],
                                "distance" => $distance,
                                "mode"=> $hotjobsData['mode'],
                                "modeurl"=> $hotjobsData['modeurl'],
                                "sharecount"=>$this->getsharecount($hotjobsData['id'])
                            ];
                        }

                        if($filter_values['sorting'] == 1) {
                            usort($data1, function($a, $b) {
                                return $b['salary'] <=> $a['salary'];
                            });
                        } else {
                            if($filter_values['sorting'] == 2) {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            } else {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            }
                        }

                    } else {

                        $hotjobData = $this->Jobsapp_Model->categoryjob_fetch_latlong($userTokenCheck[0]['id'],$expfilter, $userData['typeid'], $userData['cur_lat'], $userData['cur_long'], $filter_values, $this->default_distance);
                        foreach ($hotjobData as $hotjobsData) {
                            $negletarr[] = $hotjobsData['id'];
                        }

                        $hotjobData = $this->Jobsapp_Model->categoryjob_fetch_latlong_all($userTokenCheck[0]['id'],$expfilter, $userData['typeid'], $userData['cur_lat'], $userData['cur_long'], $offset, $limit, $filter_values, $this->default_distance);
                        foreach ($hotjobData as $hotjobsData) {

                            if(in_array($hotjobsData['id'], $negletarr)) { } else {
                                $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                                if(isset($jobTop[0]['picks_id'])){
                                    $datatoppicks1 = $jobTop[0]['picks_id']; 
                                } else {
                                    $datatoppicks1 = "";
                                }
                                if(isset($jobTop[1]['picks_id'])) {
                                    $datatoppicks2 = $jobTop[1]['picks_id']; 
                                } else {
                                    $datatoppicks2 = "";
                                }
                                if(isset($jobTop[2]['picks_id'])) {
                                    $datatoppicks3 = $jobTop[2]['picks_id']; 
                                } else {
                                    $datatoppicks3 = "";
                                }
                                
                                $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['id']);
                                if(!empty($savedjob[0]['id'])) {
                                    $savedjob = '1';
                                } else{
                                    $savedjob = "0";
                                }

                                $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                                if($jobsal[0]['basicsalary']) {
                                    $basicsalary = $jobsal[0]['basicsalary'];
                                } else{
                                    $basicsalary = 0;
                                }

                                $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                                if(isset($companydetail1[0]['address']))
                                {
                                    $companyaddress=$companydetail1[0]['address'];
                                }
                                else
                                {
                                    $companyaddress='';
                                }

                                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }
                                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                                if(isset($jobImage[0]['pic'])) {
                                    $jobImageData=$jobImage[0]['pic'];
                                } else {
                                    $jobImageData=$comPic;
                                }

                                $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                                if(!empty($companyname[0]["cname"])){
                                    $cname = $companyname[0]["cname"];
                                }else{
                                    $cname = '';
                                }

                                if($hotjobsData['chatbot'] == 1) {
                                    $hotjobsData['mode'] = "chatbot";
                                }

                                $distance = $this->distance($userData['cur_lat'], $userData['cur_long'],$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                $distance = number_format((float)$distance, 2, '.', ''); 

                                $data1[] = [
                                    "jobpost_id" => $hotjobsData['id'],
                                    "comapnyId" =>$hotjobsData['compId'],
                                    "job_title" => $hotjobsData['jobtitle'],
                                    "jobDesc" => $hotjobsData['jobDesc'],
                                    "salary" => number_format($hotjobsData['salary']),
                                    "companyName"=> $companydetail[0]["cname"],
                                    "cname"=> $cname,
                                    "companyAddress"=>$companyaddress,
                                    "toppicks1" => $datatoppicks1,
                                    "toppicks2" => $datatoppicks2,
                                    "toppicks3" => $datatoppicks3,
                                    "job_image" =>$jobImageData,
                                    "latitude"=> $hotjobsData['latitude'], 
                                    "longitude"=>$hotjobsData['longitude'],
                                    "jobexpire"=> $hotjobsData['jobexpire'],
                                    "companyPic"=> $comPic,
                                    "savedjob" =>$savedjob,
                                    "boostjob" =>$hotjobsData['boost_status'],
                                    "distance" => $distance,
                                    "mode"=> $hotjobsData['mode'],
                                    "modeurl"=> $hotjobsData['modeurl'],
                                    "sharecount"=>$this->getsharecount($hotjobsData['id'])
                                ];
                            }
                        }

                        if($filter_values['sorting'] == 1) {
                            usort($data1, function($a, $b) {
                                return $b['salary'] <=> $a['salary'];
                            });
                        } else {
                            if($filter_values['sorting'] == 2) {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            } else {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            }
                        }
                    }
                
                } else if($userData['type'] == "nearby") {

                    if($indexing == 1) {

                        $hotjobData = $this->Jobsapp_Model->job_fetch_homeforappnearby_groupby($userTokenCheck[0]['id'], $userLat, $userLong, $expfilter, $filter_values, $this->default_distance);
                        foreach ($hotjobData as $hotjobsData) {
                            $negletarr[] = $hotjobsData['id'];
                            $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                            if(isset($jobTop[0]['picks_id'])){
                                $datatoppicks1 = $jobTop[0]['picks_id']; 
                            } else {
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $datatoppicks2 = $jobTop[1]['picks_id']; 
                            } else {
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop[2]['picks_id'])) {
                                $datatoppicks3 = $jobTop[2]['picks_id']; 
                            } else {
                                $datatoppicks3 = "";
                            }
                            
                            $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['id']);
                            if(!empty($savedjob[0]['id'])) {
                                $savedjob = '1';
                            } else{
                                $savedjob = "0";
                            }

                            $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                            if($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else{
                                $basicsalary = 0;
                            }

                            $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                            if(isset($companydetail1[0]['address'])) {
                                $companyaddress=$companydetail1[0]['address'];
                            }
                            else {
                                $companyaddress='';
                            }

                            $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                            if(!empty($companyPic[0]['companyPic'])) {
                                $comPic = trim($companyPic[0]['companyPic']);
                            } else{
                                $comPic = "";
                            }

                            $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                            if(isset($jobImage[0]['pic'])) {
                                $jobImageData=$jobImage[0]['pic'];
                            }
                            else {
                                $jobImageData=$comPic;
                            }

                            $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            if($hotjobsData['chatbot'] == 1) {
                                $hotjobsData['mode'] = "chatbot";
                            }

                            $distance = $this->distance($userLat, $userLong, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', ''); 

                            $data1[] = [
                                "jobpost_id" => $hotjobsData['id'],
                                "recruiter_id" => $hotjobsData['recruiter_id'],
                                "comapnyId" =>$hotjobsData['compId'],
                                "job_title" => $hotjobsData['jobtitle'],
                                "jobDesc" => $hotjobsData['jobDesc'],
                                "salary" => number_format($hotjobsData['salary']),
                                "companyName"=> $companydetail[0]["cname"],
                                "cname"=> $cname,
                                "companyAddress"=>$companyaddress,
                                "toppicks1" => $datatoppicks1,
                                "toppicks2" => $datatoppicks2,
                                "toppicks3" => $datatoppicks3,
                                "job_image" =>$jobImageData,
                                "latitude"=> $hotjobsData['latitude'], 
                                "longitude"=>$hotjobsData['longitude'],
                                "jobexpire"=> $hotjobsData['jobexpire'],
                                "companyPic"=> $comPic,
                                "savedjob" =>$savedjob,
                                "boostjob" =>$hotjobsData['boost_status'],
                                "distance" => $distance,
                                "mode"=> $hotjobsData['mode'],
                                "modeurl"=> $hotjobsData['modeurl'],
                                "sharecount"=>$this->getsharecount($hotjobsData['id'])
                            ];
                        }  

                        // if($filter_values['sorting'] == 1) {
                        //     usort($data1, function($a, $b) {
                        //         return $b['salary'] <=> $a['salary'];
                        //     });
                        // } else {
                        //     if($filter_values['sorting'] == 2) {
                        //         usort($data1, function($a, $b) {
                        //             return $a['distance'] <=> $b['distance'];
                        //         });
                        //     } else {
                        //         usort($data1, function($a, $b) {
                        //             return $a['distance'] <=> $b['distance'];
                        //         });
                        //     }
                        // }

                    } else {

                        $hotjobData = $this->Jobsapp_Model->job_fetch_homeforappnearby_groupby($userTokenCheck[0]['id'], $userLat, $userLong, $expfilter, $filter_values, $this->default_distance);
                        foreach ($hotjobData as $hotjobsData) {
                            $negletarr[] = $hotjobsData['id'];
                        }  
                        $hotjobData = $this->Jobsapp_Model->nearbyjob_fetch_latlong($userTokenCheck[0]['id'], $userLat, $userLong, $expfilter, $offset, $limit, $filter_values, $this->default_distance);
                        foreach ($hotjobData as $hotjobsData) {

                            if(in_array($hotjobsData['id'], $negletarr)) { } else {
                            
                                $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                                if(isset($jobTop[0]['picks_id'])){
                                    $datatoppicks1 = $jobTop[0]['picks_id']; 
                                } else {
                                    $datatoppicks1 = "";
                                }
                                if(isset($jobTop[1]['picks_id'])) {
                                    $datatoppicks2 = $jobTop[1]['picks_id']; 
                                } else {
                                    $datatoppicks2 = "";
                                }
                                if(isset($jobTop[2]['picks_id'])) {
                                    $datatoppicks3 = $jobTop[2]['picks_id']; 
                                } else {
                                    $datatoppicks3 = "";
                                }
                                
                                $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['id']);

                                if(!empty($savedjob[0]['id'])) {
                                    $savedjob = '1';
                                } else{
                                    $savedjob = "0";
                                }

                                 $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);

                                if($jobsal[0]['basicsalary']) {
                                    $basicsalary = $jobsal[0]['basicsalary'];
                                } else{
                                    $basicsalary = 0;
                                }

                                $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                                if(isset($companydetail1[0]['address'])){
                                    $companyaddress=$companydetail1[0]['address'];
                                }else{
                                    $companyaddress='';
                                }

                                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }
                                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                                if(isset($jobImage[0]['pic'])){
                                    $jobImageData=$jobImage[0]['pic'];
                                }else{
                                    $jobImageData=$comPic;
                                }

                                $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                                if(!empty($companyname[0]["cname"])){
                                    $cname = $companyname[0]["cname"];
                                }else{
                                    $cname = '';
                                }

                                if($hotjobsData['chatbot'] == 1) {
                                    $hotjobsData['mode'] = "chatbot";
                                }

                                $distance = $this->distance($userData['cur_lat'], $userData['cur_long'],$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                $distance = number_format((float)$distance, 2, '.', ''); 

                                $data1[] = [
                                    "jobpost_id" => $hotjobsData['id'],
                                    "comapnyId" =>$hotjobsData['compId'],
                                    "job_title" => $hotjobsData['jobtitle'],
                                    "jobDesc" => $hotjobsData['jobDesc'],
                                    "salary" => number_format($hotjobsData['salary']),
                                    "companyName"=> $companydetail[0]["cname"],
                                    "cname"=> $cname,
                                    "companyAddress"=>$companyaddress,
                                    "toppicks1" => $datatoppicks1,
                                    "toppicks2" => $datatoppicks2,
                                    "toppicks3" => $datatoppicks3,
                                    "job_image" =>$jobImageData,
                                    "latitude"=> $hotjobsData['latitude'], 
                                    "longitude"=>$hotjobsData['longitude'],
                                    "jobexpire"=> $hotjobsData['jobexpire'],
                                    "companyPic"=> $comPic,
                                    "savedjob" =>$savedjob,
                                    "boostjob" =>$hotjobsData['boost_status'],
                                    "distance" => $distance,
                                    "mode"=> $hotjobsData['mode'],
                                    "modeurl"=> $hotjobsData['modeurl'],
                                    "sharecount"=>$this->getsharecount($hotjobsData['id'])
                                ];
                            }
                        }

                        if($filter_values['sorting'] == 1) {
                            usort($data1, function($a, $b) {
                                return $b['salary'] <=> $a['salary'];
                            });
                        } else {
                            if($filter_values['sorting'] == 2) {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            } else {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            }
                        }
                    }

                } else if($userData['type'] == "toppicks") {

                    if($indexing == 1) {

                        $hotjobDataonegroup = $this->Jobsapp_Model->feturedtoppicks_fetch_all_groupby($userTokenCheck[0]['id'], $expfilter, $userLat, $userLong, $userData['typeid'], $filter_values, $this->default_distance);
                        if ($hotjobDataonegroup) {
                            foreach ($hotjobDataonegroup as $hotjobsData) {
                                $neglectarr[] = $hotjobsData['id'];
                                $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], $userData['typeid']);
                                $datatoppicks1 = $userData['typeid']; 
                                
                                if(isset($jobTopseleted[0]['picks_id'])) {
                                    $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                                } else{
                                    $datatoppicks2 = "";
                                }
                                if(isset($jobTopseleted[1]['picks_id'])) {
                                    $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                                } else{
                                    $datatoppicks3 = "";
                                }

                                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }

                                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                                if (isset($jobImage[0]['pic'])) {
                                    $jobImageData = $jobImage[0]['pic'];
                                } else {
                                    $jobImageData = $comPic;
                                }
                                
                                $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $userID);
                                $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                                if (isset($jobsavedStatus[0]['status'])) {
                                    $jobsstatus = '1';
                                } else {
                                    $jobsstatus = '0';
                                }
                                
                                $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                                if ($jobsal[0]['basicsalary']) {
                                    $basicsalary = $jobsal[0]['basicsalary'];
                                } else {
                                    $basicsalary = "0";
                                }
                                
                                $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                                if (isset($companydetail1[0]['address'])) {
                                    $companyaddress = $companydetail1[0]['address'];
                                } else {
                                    $companyaddress = '';
                                }
                                
                                $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                                if(!empty($companyname[0]["cname"])){
                                    $cname = $companyname[0]["cname"];
                                }else{
                                    $cname = '';
                                }

                                $distance = $this->distance($userLat, $userLong, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                                $distance = number_format((float)$distance, 2, '.', '');
                                //$distance = '';
                                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                                if (isset($jobImage[0]['pic'])) {
                                    $jobImageData = $jobImage[0]['pic'];
                                } else {
                                    $jobImageData = '';
                                }

                                if($hotjobsData['chatbot'] == 1) {
                                    $hotjobsData['mode'] = "chatbot";
                                }

                                $data1[] = ["jobpost_id" => $hotjobsData['id'], 
                                          "comapnyId" => $hotjobsData['compId'],
                                          "recruiter_id" => $hotjobsData['recruiter_id'],
                                          "job_title" => $hotjobsData['jobtitle'], 
                                          "jobDesc" => $hotjobsData['jobDesc'], 
                                          "salary" => number_format($basicsalary), 
                                          "companyName" => $companydetail[0]["cname"], 
                                          "companyAddress" => $companyaddress,
                                          "job_image" => $jobImageData,
                                          "jobPitch"=>$hotjobsData['jobPitch'], 
                                          "cname"=>$cname, 
                                          "distance" =>$distance, 
                                          'job_image' =>$jobImageData,
                                          "savedjob"=> $jobsstatus,
                                          "toppicks1" => $datatoppicks1,
                                          "toppicks2" => $datatoppicks2,
                                          "toppicks3" => $datatoppicks3,
                                          "mode"=>$hotjobsData['mode'], 
                                          "modeurl"=> $hotjobsData['modeurl'],
                                          "sharecount"=>$this->getsharecount($hotjobsData['id'])
                                ];

                            }
                        }

                        // if($filter_values['sorting'] == 1) {
                        //     usort($data1, function($a, $b) {
                        //         return $b['salary'] <=> $a['salary'];
                        //     });
                        // } else {
                        //     if($filter_values['sorting'] == 2) {
                        //         usort($data1, function($a, $b) {
                        //             return $a['distance'] <=> $b['distance'];
                        //         });
                        //     } else {
                        //         usort($data1, function($a, $b) {
                        //             return $a['distance'] <=> $b['distance'];
                        //         });
                        //     }
                        // }
                    } else {

                        $hotjobDataonegroup = $this->Jobsapp_Model->feturedtoppicks_fetch_all_groupby($userTokenCheck[0]['id'], $expfilter, $userLat, $userLong, $userData['typeid'], $filter_values, $this->default_distance);
                        if ($hotjobDataonegroup) {
                            foreach ($hotjobDataonegroup as $hotjobsData) {
                                $negletarr[] = $hotjobsData['id'];
                            }
                        }

                        $hotjobData = $this->Jobsapp_Model->feturedtoppicks_fetch_latlong($userTokenCheck[0]['id'], $expfilter, $userLat, $userLong, $userData['typeid'], $offset, $limit, $filter_values, $this->default_distance);
                        foreach ($hotjobData as $hotjobsData) {

                            if(in_array($hotjobsData['id'], $negletarr)) { } else {
                                $jobTop = $this->Jobpost_Model->job_toppicks1($hotjobsData['id']);
                                $pids = array(); 
                                foreach($jobTop as $jobTopp) {
                                    $pids[] = $jobTopp['picks_id'];
                                }
                                if(in_array($userData['typeid'], $pids)) {

                                    if(!empty($jobTop) && count($jobTop)>=1) {

                                        $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], $userData['typeid']);
                                        $datatoppicks1 = $userData['typeid']; 
                                        
                                        if(isset($jobTopseleted[0]['picks_id'])) {
                                            $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                                        } else{
                                            $datatoppicks2 = "";
                                        }
                                        if(isset($jobTopseleted[1]['picks_id'])) {
                                            $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                                        } else{
                                            $datatoppicks3 = "";
                                        }
                                        
                                        $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['id']);

                                        if(!empty($savedjob[0]['id'])) {
                                            $savedjob = '1';
                                        } else{
                                            $savedjob = "0";
                                        }

                                        $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                                        if($jobsal[0]['basicsalary']) {
                                            $basicsalary = $jobsal[0]['basicsalary'];
                                        } else{
                                            $basicsalary = 0;
                                        }

                                        $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                                        if(isset($companydetail1[0]['address']))
                                        {
                                            $companyaddress=$companydetail1[0]['address'];
                                        }
                                        else
                                        {
                                            $companyaddress='';
                                        }

                                        $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                                        if(!empty($companyPic[0]['companyPic'])) {
                                            $comPic = trim($companyPic[0]['companyPic']);
                                        } else{
                                            $comPic = "";
                                        }
                                        $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);


                                        if(isset($jobImage[0]['pic']))
                                        {
                                            $jobImageData=$jobImage[0]['pic'];
                                        }
                                        else
                                        {
                                            $jobImageData=$comPic;
                                        }

                                        $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                                        if(!empty($companyname[0]["cname"])){
                                            $cname = $companyname[0]["cname"];
                                        }else{
                                            $cname = '';
                                        }

                                        if($hotjobsData['chatbot'] == 1) {
                                            $hotjobsData['mode'] = "chatbot";
                                        }

                                        $distance = $this->distance($userData['cur_lat'], $userData['cur_long'],$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                        $distance = number_format((float)$distance, 2, '.', ''); 

                                        $data1[] = [
                                            "jobpost_id" => $hotjobsData['id'],
                                            "comapnyId" =>$hotjobsData['compId'],
                                            "job_title" => $hotjobsData['jobtitle'],
                                            "jobDesc" => $hotjobsData['jobDesc'],
                                            "salary" => number_format($hotjobsData['salary']),
                                            "companyName"=> $companydetail[0]["cname"],
                                            "cname"=> $cname,
                                            "companyAddress"=>$companyaddress,
                                            "toppicks1" => $datatoppicks1,
                                            "toppicks2" => $datatoppicks2,
                                            "toppicks3" => $datatoppicks3,
                                            "job_image" =>$jobImageData,
                                            "latitude"=> $hotjobsData['latitude'], 
                                            "longitude"=>$hotjobsData['longitude'],
                                            "jobexpire"=> $hotjobsData['jobexpire'],
                                            "companyPic"=> $comPic,
                                            "savedjob" =>$savedjob,
                                            "boostjob" =>$hotjobsData['boost_status'],
                                            "distance" => $distance,
                                            "mode"=> $hotjobsData['mode'],
                                            "modeurl"=> $hotjobsData['modeurl'],
                                            "sharecount"=>$this->getsharecount($hotjobsData['id'])
                                        ];
                                    }
                                }
                            }
                        }

                        if($filter_values['sorting'] == 1) {
                            usort($data1, function($a, $b) {
                                return $b['salary'] <=> $a['salary'];
                            });
                        } else {
                            if($filter_values['sorting'] == 2) {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            } else {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            }
                        }
                    }

                } else if($userData['type'] == "Allowance") {

                    if($indexing == 1) {

                        $hotjobDataonegroup = $this->Jobsapp_Model->feturedtoppicks_fetch_latlong_allowances_groupby($userTokenCheck[0]['id'], $expfilter, $userLat, $userLong, $filter_values, $this->default_distance);
                        if ($hotjobDataonegroup) {
                            foreach ($hotjobDataonegroup as $hotjobsData) {
                                $negletarr[] = $hotjobsData['id'];
                                $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], $getParamType);
                                //$datatoppicks1 = $getParamType; 
                                if(isset($jobTopseleted[0]['picks_id'])) {
                                    $datatoppicks1 = $jobTopseleted[0]['picks_id']; 
                                } else{
                                    $datatoppicks1 = "";
                                }
                                if(isset($jobTopseleted[1]['picks_id'])) {
                                    $datatoppicks2 = $jobTopseleted[1]['picks_id']; 
                                } else{
                                    $datatoppicks2 = "";
                                }
                                if(isset($jobTopseleted[2]['picks_id'])) {
                                    $datatoppicks3 = $jobTopseleted[2]['picks_id']; 
                                } else{
                                    $datatoppicks3 = "";
                                }

                                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }

                                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                                if (isset($jobImage[0]['pic'])) {
                                    $jobImageData = $jobImage[0]['pic'];
                                } else {
                                    $jobImageData = $comPic;
                                }
                                
                                $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $userTokenCheck[0]['id']);
                                $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                                if (isset($jobsavedStatus[0]['status'])) {
                                    $jobsstatus = '1';
                                } else {
                                    $jobsstatus = '0';
                                }
                                
                                $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                                if ($jobsal[0]['basicsalary']) {
                                    $basicsalary = $jobsal[0]['basicsalary'];
                                } else {
                                    $basicsalary = "0";
                                }
                                
                                $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                                if (isset($companydetail1[0]['address'])) {
                                    $companyaddress = $companydetail1[0]['address'];
                                } else {
                                    $companyaddress = '';
                                }
                                
                                $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                                if(!empty($companyname[0]["cname"])){
                                    $cname = $companyname[0]["cname"];
                                }else{
                                    $cname = '';
                                }

                                $distance = $this->distance($userLat, $userLong, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                                $distance = number_format((float)$distance, 2, '.', '');
                                //$distance = '';
                                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                                if (isset($jobImage[0]['pic'])) {
                                    $jobImageData = $jobImage[0]['pic'];
                                } else {
                                    $jobImageData = '';
                                }

                                if($hotjobsData['chatbot'] == 1) {
                                    $hotjobsData['mode'] = "chatbot";
                                }

                                $data1[] = ["jobpost_id" => $hotjobsData['id'], 
                                          "comapnyId" => $hotjobsData['compId'],
                                          "recruiter_id" => $hotjobsData['recruiter_id'],
                                          "job_title" => $hotjobsData['jobtitle'], 
                                          "jobDesc" => $hotjobsData['jobDesc'], 
                                          "salary" => number_format($basicsalary), 
                                          "companyName" => $companydetail[0]["cname"], 
                                          "companyAddress" => $companyaddress,
                                          "job_image" => $jobImageData,
                                          "jobPitch"=>$hotjobsData['jobPitch'], 
                                          "cname"=>$cname, 
                                          "distance" =>$distance, 
                                          'job_image' =>$jobImageData,
                                          "savedjob"=> $jobsstatus,
                                          "toppicks1" => $datatoppicks1,
                                          "toppicks2" => $datatoppicks2,
                                          "toppicks3" => $datatoppicks3,
                                          "mode"=>$hotjobsData['mode'], 
                                          "modeurl"=> $hotjobsData['modeurl'],
                                          "sharecount"=>$this->getsharecount($hotjobsData['id'])
                                ];

                            }
                        }

                        if($filter_values['sorting'] == 1) {
                            usort($data1, function($a, $b) {
                                return $b['salary'] <=> $a['salary'];
                            });
                        } else {
                            if($filter_values['sorting'] == 2) {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            } else {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            }
                        }

                    } else {

                        $hotjobDataonegroup = $this->Jobsapp_Model->feturedtoppicks_fetch_latlong_allowances_groupby($userTokenCheck[0]['id'], $expfilter, $userLat, $userLong, $filter_values, $this->default_distance);
                        if ($hotjobDataonegroup) {
                            foreach ($hotjobDataonegroup as $hotjobsData) {
                                $negletarr[] = $hotjobsData['id'];
                            }
                        }

                        $hotjobData = $this->Jobsapp_Model->feturedtoppicks_fetch_latlong_bonus_allowances($userTokenCheck[0]['id'], $expfilter, $userLat, $userLong, $offset, $limit, $filter_values, $this->default_distance);
                        //var_dump(count($hotjobData);die;
                        foreach ($hotjobData as $hotjobsData) {

                            if(in_array($hotjobsData['id'], $negletarr)) { } else {
                                $jobTop = $this->Jobpost_Model->job_allowancesbyid($hotjobsData['id'], $userData['typeid']);
                                //if(!empty($jobTop) && count($jobTop)>=1) {

                                    if(isset($jobTop[0]['picks_id'])) {
                                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                                    } else {
                                        $datatoppicks1 = "";
                                    }
                                    if(isset($jobTop[1]['picks_id'])) {
                                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                                    } else {
                                        $datatoppicks2 = "";
                                    }
                                    if(isset($jobTop[2]['picks_id'])) {
                                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                                    } else {
                                        $datatoppicks3 = "";
                                    }
                                    
                                    $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['id']);
                                    if(!empty($savedjob[0]['id'])) {
                                        $savedjob = '1';
                                    } else{
                                        $savedjob = "0";
                                    }

                                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                                    if($jobsal[0]['basicsalary']) {
                                        $basicsalary = $jobsal[0]['basicsalary'];
                                    } else{
                                        $basicsalary = 0;
                                    }

                                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                                    if(isset($companydetail1[0]['address']))
                                    {
                                        $companyaddress=$companydetail1[0]['address'];
                                    }else{
                                        $companyaddress='';
                                    }

                                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                                    if(!empty($companyPic[0]['companyPic'])) {
                                        $comPic = trim($companyPic[0]['companyPic']);
                                    } else{
                                        $comPic = "";
                                    }
                                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                                    if(isset($jobImage[0]['pic']))
                                    {
                                        $jobImageData=$jobImage[0]['pic'];
                                    }
                                    else
                                    {
                                        $jobImageData=$comPic;
                                    }

                                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                                    if(!empty($companyname[0]["cname"])){
                                        $cname = $companyname[0]["cname"];
                                    }else{
                                        $cname = '';
                                    }

                                    if($hotjobsData['chatbot'] == 1) {
                                        $hotjobsData['mode'] = "chatbot";
                                    }

                                    $distance = $this->distance($userData['cur_lat'], $userData['cur_long'],$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                    $distance = number_format((float)$distance, 2, '.', ''); 

                                    $data1[] = [
                                        "jobpost_id" => $hotjobsData['id'],
                                        "comapnyId" =>$hotjobsData['compId'],
                                        "job_title" => $hotjobsData['jobtitle'],
                                        "jobDesc" => $hotjobsData['jobDesc'],
                                        "salary" => number_format($hotjobsData['salary']),
                                        "companyName"=> $companydetail[0]["cname"],
                                        "cname"=> $cname,
                                        "companyAddress"=>$companyaddress,
                                        "toppicks1" => $datatoppicks1,
                                        "toppicks2" => $datatoppicks2,
                                        "toppicks3" => $datatoppicks3,
                                        "job_image" =>$jobImageData,
                                        "latitude"=> $hotjobsData['latitude'], 
                                        "longitude"=>$hotjobsData['longitude'],
                                        "jobexpire"=> $hotjobsData['jobexpire'],
                                        "companyPic"=> $comPic,
                                        "savedjob" =>$savedjob,
                                        "boostjob" =>$hotjobsData['boost_status'],
                                        "distance" => $distance,
                                        "mode"=> $hotjobsData['mode'],
                                        "modeurl"=> $hotjobsData['modeurl'],
                                        "sharecount"=>$this->getsharecount($hotjobsData['id'])
                                    ];
                                //}
                            }
                        }

                        if($filter_values['sorting'] == 1) {
                            usort($data1, function($a, $b) {
                                return $b['salary'] <=> $a['salary'];
                            });
                        } else {
                            if($filter_values['sorting'] == 2) {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            } else {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            }
                        }

                    }

                } else if($userData['type'] == "leadership") {

                    if($indexing == 1) {

                        $boostjobData = $this->Jobsapp_Model->catjob_fetch_leadership($userTokenCheck[0]['id'], $expfilter, $userLat, $userLong, $filter_values, $this->default_distance);
                        foreach ($boostjobData as $boostjobsData) {
                            $negletarr[] = $boostjobsData['id'];
                            $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
                            //if(!empty($jobTop) && count($jobTop)>=0) {

                                if(isset($jobTop[0]['picks_id'])) {
                                    $datatoppicks1 = $jobTop[0]['picks_id']; 
                                } else{
                                    $datatoppicks1 = "";
                                }
                                if(isset($jobTop[1]['picks_id'])) {
                                    $datatoppicks2 = $jobTop[1]['picks_id']; 
                                } else{
                                    $datatoppicks2 = "";
                                }
                                if(isset($jobTop[2]['picks_id'])) {
                                    $datatoppicks3 = $jobTop[2]['picks_id']; 
                                } else{
                                    $datatoppicks3 = "";
                                }
                                
                                $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userTokenCheck[0]['id']);
                                if(!empty($savedjob[0]['id'])) {
                                    $savedjob = '1';
                                } else{
                                    $savedjob = "0";
                                }

                                $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                                if($jobsal[0]['basicsalary']) {
                                    $basicsalary = $jobsal[0]['basicsalary'];
                                } else{
                                    $basicsalary = 0;
                                }

                                $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                                if(isset($companydetail1[0]['address'])){
                                    $companyaddress=$companydetail1[0]['address'];
                                }else{
                                    $companyaddress='';
                                }

                                $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }

                                $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                                if(isset($jobImage[0]['pic'])){
                                    $jobImageData=$jobImage[0]['pic'];
                                }else{
                                    $jobImageData=$comPic;
                                }

                                $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                                if(!empty($companyname[0]["cname"])){
                                    $cname = $companyname[0]["cname"];
                                }else{
                                    $cname = '';
                                }

                                if($boostjobsData['chatbot'] == 1) {
                                    $boostjobsData['mode'] = "chatbot";
                                }

                                $distance = $this->distance($userLat, $userLong, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                $distance = number_format((float)$distance, 2, '.', ''); 

                                $bostArr[] = $boostjobsData['id'];

                                $data1[] = [
                                        "jobpost_id" => $boostjobsData['id'],
                                        "recruiter_id" => $boostjobsData['recruiter_id'],
                                        "comapnyId" =>$boostjobsData['compId'],
                                        "job_title" => $boostjobsData['jobtitle'],
                                        "jobDesc" => $boostjobsData['jobDesc'],
                                        "jobPitch"=>$boostjobsData['jobPitch'],
                                        "salary" => number_format($boostjobsData['salary']),
                                        "companyName"=> $companydetail[0]["cname"],
                                        "cname"=> $cname,
                                        "companyAddress"=>$companyaddress,
                                        "toppicks1" => $datatoppicks1,
                                        "toppicks2" => $datatoppicks2,
                                        "toppicks3" => $datatoppicks3,
                                        "job_image" =>$jobImageData,
                                        "latitude"=> $boostjobsData['latitude'], 
                                        "longitude"=>$boostjobsData['longitude'],
                                        "jobexpire"=> $boostjobsData['jobexpire'],
                                        "companyPic"=> $comPic,
                                        "savedjob" =>$savedjob,
                                        "boostjob" =>$boostjobsData['boost_status'],
                                        "distance" => $distance,
                                        "mode"=> $boostjobsData['mode'], 
                                        "modeurl"=> $boostjobsData['modeurl'], 
                                        "sharecount"=>$this->getsharecount($boostjobsData['id'])
                                ];
                            //}
                        } 

                        if($filter_values['sorting'] == 1) {
                            usort($data1, function($a, $b) {
                                return $b['salary'] <=> $a['salary'];
                            });
                        } else {
                            if($filter_values['sorting'] == 2) {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            } else {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            }
                        }   

                    } else {

                        $boostjobData = $this->Jobsapp_Model->catjob_fetch_leadership($userTokenCheck[0]['id'], $expfilter, $userLat, $userLong, $filter_values, $this->default_distance);
                        foreach ($boostjobData as $boostjobsData) {
                            $negletarr[] = $boostjobsData['id'];
                        } 

                        $boostjobData = $this->Jobsapp_Model->catjob_fetch_leadership_all($userTokenCheck[0]['id'],$expfilter, $userLat, $userLong, $offset, $limit, $filter_values, $this->default_distance);
                        foreach ($boostjobData as $boostjobsData) {

                            if(in_array($boostjobsData['id'], $negletarr)) { } else {
                                $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
                                //if(!empty($jobTop) && count($jobTop)>=0) {
          
                                    if(isset($jobTop[0]['picks_id'])) {
                                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                                    } else{
                                        $datatoppicks1 = "";
                                    }
                                    if(isset($jobTop[1]['picks_id'])) {
                                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                                    } else{
                                        $datatoppicks2 = "";
                                    }
                                    if(isset($jobTop[2]['picks_id'])) {
                                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                                    } else{
                                        $datatoppicks3 = "";
                                    }
                                    
                                    $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userTokenCheck[0]['id']);
                                    if(!empty($savedjob[0]['id'])) {
                                        $savedjob = '1';
                                    } else{
                                        $savedjob = "0";
                                    }

                                    $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                                    if($jobsal[0]['basicsalary']) {
                                        $basicsalary = $jobsal[0]['basicsalary'];
                                    } else{
                                        $basicsalary = 0;
                                    }

                                    $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                                    if(isset($companydetail1[0]['address'])){
                                        $companyaddress=$companydetail1[0]['address'];
                                    }else{
                                        $companyaddress='';
                                    }

                                    $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                                    if(!empty($companyPic[0]['companyPic'])) {
                                        $comPic = trim($companyPic[0]['companyPic']);
                                    } else{
                                        $comPic = "";
                                    }

                                    $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                                    if(isset($jobImage[0]['pic'])){
                                        $jobImageData=$jobImage[0]['pic'];
                                    }else{
                                        $jobImageData=$comPic;
                                    }

                                    $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                                    if(!empty($companyname[0]["cname"])){
                                        $cname = $companyname[0]["cname"];
                                    }else{
                                        $cname = '';
                                    }

                                    if($boostjobsData['chatbot'] == 1) {
                                        $boostjobsData['mode'] = "chatbot";
                                    }

                                    $distance = $this->distance($userLat,$userLong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                    $distance = number_format((float)$distance, 2, '.', ''); 

                                    $bostArr[] = $boostjobsData['id'];

                                    $data1[] = [
                                            "jobpost_id" => $boostjobsData['id'],
                                            "comapnyId" =>$boostjobsData['compId'],
                                            "job_title" => $boostjobsData['jobtitle'],
                                            "jobDesc" => $boostjobsData['jobDesc'],
                                            "jobPitch"=>$boostjobsData['jobPitch'],
                                            "salary" => number_format($boostjobsData['salary']),
                                            "companyName"=> $companydetail[0]["cname"],
                                            "cname"=> $cname,
                                            "companyAddress"=>$companyaddress,
                                            "toppicks1" => $datatoppicks1,
                                            "toppicks2" => $datatoppicks2,
                                            "toppicks3" => $datatoppicks3,
                                            "job_image" =>$jobImageData,
                                            "latitude"=> $boostjobsData['latitude'], 
                                            "longitude"=>$boostjobsData['longitude'],
                                            "jobexpire"=> $boostjobsData['jobexpire'],
                                            "companyPic"=> $comPic,
                                            "savedjob" =>$savedjob,
                                            "boostjob" =>$boostjobsData['boost_status'],
                                            "distance" => $distance,
                                            "mode"=> $boostjobsData['mode'], 
                                            "modeurl"=> $boostjobsData['modeurl'], 
                                            "sharecount"=>$this->getsharecount($boostjobsData['id'])
                                    ];
                                //}
                            }
                        }

                        if($filter_values['sorting'] == 1) {
                            usort($data1, function($a, $b) {
                                return $b['salary'] <=> $a['salary'];
                            });
                        } else {
                            if($filter_values['sorting'] == 2) {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            } else {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            }
                        }
                    }

                } else if($userData['type'] == "information") {

                    if($indexing == 1) {
                        $boostjobData = $this->Jobsapp_Model->catjob_fetch_information_technology($userTokenCheck[0]['id'], $expfilter, $userLat, $userLong, $filter_values, $this->default_distance);
                        foreach ($boostjobData as $boostjobsData) {
                            $negletarr[] = $boostjobsData['id'];
                            $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
                            //if(!empty($jobTop) && count($jobTop)>=0) {

                                if(isset($jobTop[0]['picks_id'])) {
                                    $datatoppicks1 = $jobTop[0]['picks_id']; 
                                } else{
                                    $datatoppicks1 = "";
                                }
                                if(isset($jobTop[1]['picks_id'])) {
                                    $datatoppicks2 = $jobTop[1]['picks_id']; 
                                } else{
                                    $datatoppicks2 = "";
                                }
                                if(isset($jobTop[2]['picks_id'])) {
                                    $datatoppicks3 = $jobTop[2]['picks_id']; 
                                } else{
                                    $datatoppicks3 = "";
                                }
                                
                                $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userTokenCheck[0]['id']);
                                if(!empty($savedjob[0]['id'])) {
                                    $savedjob = '1';
                                } else{
                                    $savedjob = "0";
                                }

                                $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                                if($jobsal[0]['basicsalary']) {
                                    $basicsalary = $jobsal[0]['basicsalary'];
                                } else{
                                    $basicsalary = 0;
                                }

                                $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                                if(isset($companydetail1[0]['address'])){
                                    $companyaddress=$companydetail1[0]['address'];
                                }else{
                                    $companyaddress='';
                                }

                                $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }

                                $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                                if(isset($jobImage[0]['pic'])){
                                    $jobImageData=$jobImage[0]['pic'];
                                }else{
                                    $jobImageData=$comPic;
                                }

                                $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                                if(!empty($companyname[0]["cname"])){
                                    $cname = $companyname[0]["cname"];
                                }else{
                                    $cname = '';
                                }

                                if($boostjobsData['chatbot'] == 1) {
                                    $boostjobsData['mode'] = "chatbot";
                                }

                                $distance = $this->distance($userLat, $userLong, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                $distance = number_format((float)$distance, 2, '.', ''); 

                                $bostArr[] = $boostjobsData['id'];

                                $data1[] = [
                                        "jobpost_id" => $boostjobsData['id'],
                                        "recruiter_id" => $boostjobsData['recruiter_id'],
                                        "comapnyId" =>$boostjobsData['compId'],
                                        "job_title" => $boostjobsData['jobtitle'],
                                        "jobDesc" => $boostjobsData['jobDesc'],
                                        "jobPitch"=>$boostjobsData['jobPitch'],
                                        "salary" => number_format($boostjobsData['salary']),
                                        "companyName"=> $companydetail[0]["cname"],
                                        "cname"=> $cname,
                                        "companyAddress"=>$companyaddress,
                                        "toppicks1" => $datatoppicks1,
                                        "toppicks2" => $datatoppicks2,
                                        "toppicks3" => $datatoppicks3,
                                        "job_image" =>$jobImageData,
                                        "latitude"=> $boostjobsData['latitude'], 
                                        "longitude"=>$boostjobsData['longitude'],
                                        "jobexpire"=> $boostjobsData['jobexpire'],
                                        "companyPic"=> $comPic,
                                        "savedjob" =>$savedjob,
                                        "boostjob" =>$boostjobsData['boost_status'],
                                        "distance" => $distance,
                                        "mode"=> $boostjobsData['mode'], 
                                        "modeurl"=> $boostjobsData['modeurl'], 
                                        "sharecount"=>$this->getsharecount($boostjobsData['id'])
                                ];
                            //}
                        } 

                        if($filter_values['sorting'] == 1) {
                            usort($data1, function($a, $b) {
                                return $b['salary'] <=> $a['salary'];
                            });
                        } else {
                            if($filter_values['sorting'] == 2) {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            } else {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            }
                        }
                    
                    } else {

                        $boostjobData = $this->Jobsapp_Model->catjob_fetch_information_technology($userTokenCheck[0]['id'], $expfilter, $userLat, $userLong, $filter_values, $this->default_distance);
                        foreach ($boostjobData as $boostjobsData) {
                            $negletarr[] = $boostjobsData['id'];
                        } 

                        $boostjobData = $this->Jobsapp_Model->catjob_fetch_information_technology_all($userTokenCheck[0]['id'],$expfilter, $userLat, $userLong, $offset, $limit, $filter_values, $this->default_distance);
                        foreach ($boostjobData as $boostjobsData) {

                            if(in_array($boostjobsData['id'], $negletarr)) { } else {
                                $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
                                //if(!empty($jobTop) && count($jobTop)>=0) {
          
                                    if(isset($jobTop[0]['picks_id'])) {
                                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                                    } else{
                                        $datatoppicks1 = "";
                                    }
                                    if(isset($jobTop[1]['picks_id'])) {
                                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                                    } else{
                                        $datatoppicks2 = "";
                                    }
                                    if(isset($jobTop[2]['picks_id'])) {
                                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                                    } else{
                                        $datatoppicks3 = "";
                                    }
                                    
                                    $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userTokenCheck[0]['id']);
                                    if(!empty($savedjob[0]['id'])) {
                                        $savedjob = '1';
                                    } else{
                                        $savedjob = "0";
                                    }

                                    $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                                    if($jobsal[0]['basicsalary']) {
                                        $basicsalary = $jobsal[0]['basicsalary'];
                                    } else{
                                        $basicsalary = 0;
                                    }

                                    $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                                    if(isset($companydetail1[0]['address'])){
                                        $companyaddress=$companydetail1[0]['address'];
                                    }else{
                                        $companyaddress='';
                                    }

                                    $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                                    if(!empty($companyPic[0]['companyPic'])) {
                                        $comPic = trim($companyPic[0]['companyPic']);
                                    } else{
                                        $comPic = "";
                                    }

                                    $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                                    if(isset($jobImage[0]['pic'])){
                                        $jobImageData=$jobImage[0]['pic'];
                                    }else{
                                        $jobImageData=$comPic;
                                    }

                                    $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                                    if(!empty($companyname[0]["cname"])){
                                        $cname = $companyname[0]["cname"];
                                    }else{
                                        $cname = '';
                                    }

                                    if($boostjobsData['chatbot'] == 1) {
                                        $boostjobsData['mode'] = "chatbot";
                                    }

                                    $distance = $this->distance($userLat,$userLong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                    $distance = number_format((float)$distance, 2, '.', ''); 

                                    $bostArr[] = $boostjobsData['id'];

                                    $data1[] = [
                                            "jobpost_id" => $boostjobsData['id'],
                                            "comapnyId" =>$boostjobsData['compId'],
                                            "job_title" => $boostjobsData['jobtitle'],
                                            "jobDesc" => $boostjobsData['jobDesc'],
                                            "jobPitch"=>$boostjobsData['jobPitch'],
                                            "salary" => number_format($boostjobsData['salary']),
                                            "companyName"=> $companydetail[0]["cname"],
                                            "cname"=> $cname,
                                            "companyAddress"=>$companyaddress,
                                            "toppicks1" => $datatoppicks1,
                                            "toppicks2" => $datatoppicks2,
                                            "toppicks3" => $datatoppicks3,
                                            "job_image" =>$jobImageData,
                                            "latitude"=> $boostjobsData['latitude'], 
                                            "longitude"=>$boostjobsData['longitude'],
                                            "jobexpire"=> $boostjobsData['jobexpire'],
                                            "companyPic"=> $comPic,
                                            "savedjob" =>$savedjob,
                                            "boostjob" =>$boostjobsData['boost_status'],
                                            "distance" => $distance,
                                            "mode"=> $boostjobsData['mode'], 
                                            "modeurl"=> $boostjobsData['modeurl'], 
                                            "sharecount"=>$this->getsharecount($boostjobsData['id'])
                                    ];
                                //}
                            }
                        }

                        if($filter_values['sorting'] == 1) {
                            usort($data1, function($a, $b) {
                                return $b['salary'] <=> $a['salary'];
                            });
                        } else {
                            if($filter_values['sorting'] == 2) {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            } else {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            }
                        }
                    }
                
                } else if($userData['type'] == "noexp") {

                    if($indexing == 1) {
                        $boostjobData = $this->Jobsapp_Model->catjob_fetch_no_experience($userTokenCheck[0]['id'], $expfilter, $userLat, $userLong, $filter_values, $this->default_distance);
                        foreach ($boostjobData as $boostjobsData) {
                            $negletarr[] = $boostjobsData['id'];
                            $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
                            //if(!empty($jobTop) && count($jobTop)>=0) {

                                if(isset($jobTop[0]['picks_id'])) {
                                    $datatoppicks1 = $jobTop[0]['picks_id']; 
                                } else{
                                    $datatoppicks1 = "";
                                }
                                if(isset($jobTop[1]['picks_id'])) {
                                    $datatoppicks2 = $jobTop[1]['picks_id']; 
                                } else{
                                    $datatoppicks2 = "";
                                }
                                if(isset($jobTop[2]['picks_id'])) {
                                    $datatoppicks3 = $jobTop[2]['picks_id']; 
                                } else{
                                    $datatoppicks3 = "";
                                }
                                
                                $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userTokenCheck[0]['id']);
                                if(!empty($savedjob[0]['id'])) {
                                    $savedjob = '1';
                                } else{
                                    $savedjob = "0";
                                }

                                $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                                if($jobsal[0]['basicsalary']) {
                                    $basicsalary = $jobsal[0]['basicsalary'];
                                } else{
                                    $basicsalary = 0;
                                }

                                $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                                if(isset($companydetail1[0]['address'])){
                                    $companyaddress=$companydetail1[0]['address'];
                                }else{
                                    $companyaddress='';
                                }

                                $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }

                                $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                                if(isset($jobImage[0]['pic'])){
                                    $jobImageData=$jobImage[0]['pic'];
                                }else{
                                    $jobImageData=$comPic;
                                }

                                $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                                if(!empty($companyname[0]["cname"])){
                                    $cname = $companyname[0]["cname"];
                                }else{
                                    $cname = '';
                                }

                                if($boostjobsData['chatbot'] == 1) {
                                    $boostjobsData['mode'] = "chatbot";
                                }

                                $distance = $this->distance($userLat, $userLong, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                $distance = number_format((float)$distance, 2, '.', ''); 

                                $bostArr[] = $boostjobsData['id'];

                                $data1[] = [
                                        "jobpost_id" => $boostjobsData['id'],
                                        "recruiter_id" => $boostjobsData['recruiter_id'],
                                        "comapnyId" =>$boostjobsData['compId'],
                                        "job_title" => $boostjobsData['jobtitle'],
                                        "jobDesc" => $boostjobsData['jobDesc'],
                                        "jobPitch"=>$boostjobsData['jobPitch'],
                                        "salary" => number_format($boostjobsData['salary']),
                                        "companyName"=> $companydetail[0]["cname"],
                                        "cname"=> $cname,
                                        "companyAddress"=>$companyaddress,
                                        "toppicks1" => $datatoppicks1,
                                        "toppicks2" => $datatoppicks2,
                                        "toppicks3" => $datatoppicks3,
                                        "job_image" =>$jobImageData,
                                        "latitude"=> $boostjobsData['latitude'], 
                                        "longitude"=>$boostjobsData['longitude'],
                                        "jobexpire"=> $boostjobsData['jobexpire'],
                                        "companyPic"=> $comPic,
                                        "savedjob" =>$savedjob,
                                        "boostjob" =>$boostjobsData['boost_status'],
                                        "distance" => $distance,
                                        "mode"=> $boostjobsData['mode'], 
                                        "modeurl"=> $boostjobsData['modeurl'], 
                                        "sharecount"=>$this->getsharecount($boostjobsData['id'])
                                ];
                            //}
                        } 

                        if($filter_values['sorting'] == 1) {
                            usort($data1, function($a, $b) {
                                return $b['salary'] <=> $a['salary'];
                            });
                        } else {
                            if($filter_values['sorting'] == 2) {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            } else {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            }
                        }
                    
                    } else {

                        $boostjobData = $this->Jobsapp_Model->catjob_fetch_no_experience($userTokenCheck[0]['id'], $expfilter, $userLat, $userLong, $filter_values, $this->default_distance);
                        foreach ($boostjobData as $boostjobsData) {
                            $negletarr[] = $boostjobsData['id'];
                        } 

                        $boostjobData = $this->Jobsapp_Model->catjob_fetch_no_experience_all($userTokenCheck[0]['id'],$expfilter, $userLat, $userLong, $offset, $limit, $filter_values, $this->default_distance);
                        foreach ($boostjobData as $boostjobsData) {

                            if(in_array($boostjobsData['id'], $negletarr)) { } else {
                                $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
                                //if(!empty($jobTop) && count($jobTop)>=0) {
          
                                    if(isset($jobTop[0]['picks_id'])) {
                                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                                    } else{
                                        $datatoppicks1 = "";
                                    }
                                    if(isset($jobTop[1]['picks_id'])) {
                                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                                    } else{
                                        $datatoppicks2 = "";
                                    }
                                    if(isset($jobTop[2]['picks_id'])) {
                                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                                    } else{
                                        $datatoppicks3 = "";
                                    }
                                    
                                    $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userTokenCheck[0]['id']);
                                    if(!empty($savedjob[0]['id'])) {
                                        $savedjob = '1';
                                    } else{
                                        $savedjob = "0";
                                    }

                                    $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                                    if($jobsal[0]['basicsalary']) {
                                        $basicsalary = $jobsal[0]['basicsalary'];
                                    } else{
                                        $basicsalary = 0;
                                    }

                                    $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                                    if(isset($companydetail1[0]['address'])){
                                        $companyaddress=$companydetail1[0]['address'];
                                    }else{
                                        $companyaddress='';
                                    }

                                    $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                                    if(!empty($companyPic[0]['companyPic'])) {
                                        $comPic = trim($companyPic[0]['companyPic']);
                                    } else{
                                        $comPic = "";
                                    }

                                    $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                                    if(isset($jobImage[0]['pic'])){
                                        $jobImageData=$jobImage[0]['pic'];
                                    }else{
                                        $jobImageData=$comPic;
                                    }

                                    $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                                    if(!empty($companyname[0]["cname"])){
                                        $cname = $companyname[0]["cname"];
                                    }else{
                                        $cname = '';
                                    }

                                    if($boostjobsData['chatbot'] == 1) {
                                        $boostjobsData['mode'] = "chatbot";
                                    }

                                    $distance = $this->distance($userLat,$userLong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                    $distance = number_format((float)$distance, 2, '.', ''); 

                                    $bostArr[] = $boostjobsData['id'];

                                    $data1[] = [
                                            "jobpost_id" => $boostjobsData['id'],
                                            "comapnyId" =>$boostjobsData['compId'],
                                            "job_title" => $boostjobsData['jobtitle'],
                                            "jobDesc" => $boostjobsData['jobDesc'],
                                            "jobPitch"=>$boostjobsData['jobPitch'],
                                            "salary" => number_format($boostjobsData['salary']),
                                            "companyName"=> $companydetail[0]["cname"],
                                            "cname"=> $cname,
                                            "companyAddress"=>$companyaddress,
                                            "toppicks1" => $datatoppicks1,
                                            "toppicks2" => $datatoppicks2,
                                            "toppicks3" => $datatoppicks3,
                                            "job_image" =>$jobImageData,
                                            "latitude"=> $boostjobsData['latitude'], 
                                            "longitude"=>$boostjobsData['longitude'],
                                            "jobexpire"=> $boostjobsData['jobexpire'],
                                            "companyPic"=> $comPic,
                                            "savedjob" =>$savedjob,
                                            "boostjob" =>$boostjobsData['boost_status'],
                                            "distance" => $distance,
                                            "mode"=> $boostjobsData['mode'], 
                                            "modeurl"=> $boostjobsData['modeurl'], 
                                            "sharecount"=>$this->getsharecount($boostjobsData['id'])
                                    ];
                                //}
                            }
                        }

                        if($filter_values['sorting'] == 1) {
                            usort($data1, function($a, $b) {
                                return $b['salary'] <=> $a['salary'];
                            });
                        } else {
                            if($filter_values['sorting'] == 2) {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            } else {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            }
                        }
                    }
                
                } else if($userData['type'] == "nursing") {

                    if($indexing == 1) {
                        $boostjobData = $this->Jobsapp_Model->catjob_fetch_nursing($userTokenCheck[0]['id'], $expfilter, $userLat, $userLong, $filter_values, $this->default_distance);
                        foreach ($boostjobData as $boostjobsData) {
                            $negletarr[] = $boostjobsData['id'];
                            $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
                            //if(!empty($jobTop) && count($jobTop)>=0) {

                                if(isset($jobTop[0]['picks_id'])) {
                                    $datatoppicks1 = $jobTop[0]['picks_id']; 
                                } else{
                                    $datatoppicks1 = "";
                                }
                                if(isset($jobTop[1]['picks_id'])) {
                                    $datatoppicks2 = $jobTop[1]['picks_id']; 
                                } else{
                                    $datatoppicks2 = "";
                                }
                                if(isset($jobTop[2]['picks_id'])) {
                                    $datatoppicks3 = $jobTop[2]['picks_id']; 
                                } else{
                                    $datatoppicks3 = "";
                                }
                                
                                $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userTokenCheck[0]['id']);
                                if(!empty($savedjob[0]['id'])) {
                                    $savedjob = '1';
                                } else{
                                    $savedjob = "0";
                                }

                                $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                                if($jobsal[0]['basicsalary']) {
                                    $basicsalary = $jobsal[0]['basicsalary'];
                                } else{
                                    $basicsalary = 0;
                                }

                                $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                                if(isset($companydetail1[0]['address'])){
                                    $companyaddress=$companydetail1[0]['address'];
                                }else{
                                    $companyaddress='';
                                }

                                $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }

                                $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                                if(isset($jobImage[0]['pic'])){
                                    $jobImageData=$jobImage[0]['pic'];
                                }else{
                                    $jobImageData=$comPic;
                                }

                                $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                                if(!empty($companyname[0]["cname"])){
                                    $cname = $companyname[0]["cname"];
                                }else{
                                    $cname = '';
                                }

                                if($boostjobsData['chatbot'] == 1) {
                                    $boostjobsData['mode'] = "chatbot";
                                }

                                $distance = $this->distance($userLat, $userLong, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                $distance = number_format((float)$distance, 2, '.', ''); 

                                $bostArr[] = $boostjobsData['id'];

                                $data1[] = [
                                        "jobpost_id" => $boostjobsData['id'],
                                        "recruiter_id" => $boostjobsData['recruiter_id'],
                                        "comapnyId" =>$boostjobsData['compId'],
                                        "job_title" => $boostjobsData['jobtitle'],
                                        "jobDesc" => $boostjobsData['jobDesc'],
                                        "jobPitch"=>$boostjobsData['jobPitch'],
                                        "salary" => number_format($boostjobsData['salary']),
                                        "companyName"=> $companydetail[0]["cname"],
                                        "cname"=> $cname,
                                        "companyAddress"=>$companyaddress,
                                        "toppicks1" => $datatoppicks1,
                                        "toppicks2" => $datatoppicks2,
                                        "toppicks3" => $datatoppicks3,
                                        "job_image" =>$jobImageData,
                                        "latitude"=> $boostjobsData['latitude'], 
                                        "longitude"=>$boostjobsData['longitude'],
                                        "jobexpire"=> $boostjobsData['jobexpire'],
                                        "companyPic"=> $comPic,
                                        "savedjob" =>$savedjob,
                                        "boostjob" =>$boostjobsData['boost_status'],
                                        "distance" => $distance,
                                        "mode"=> $boostjobsData['mode'], 
                                        "modeurl"=> $boostjobsData['modeurl'], 
                                        "sharecount"=>$this->getsharecount($boostjobsData['id'])
                                ];
                            //}
                        } 

                        if($filter_values['sorting'] == 1) {
                            usort($data1, function($a, $b) {
                                return $b['salary'] <=> $a['salary'];
                            });
                        } else {
                            if($filter_values['sorting'] == 2) {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            } else {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            }
                        }
                    
                    } else {

                        $boostjobData = $this->Jobsapp_Model->catjob_fetch_nursing($userTokenCheck[0]['id'], $expfilter, $userLat, $userLong, $filter_values, $this->default_distance);
                        foreach ($boostjobData as $boostjobsData) {
                            $negletarr[] = $boostjobsData['id'];
                        } 

                        $boostjobData = $this->Jobsapp_Model->catjob_fetch_nursing_all($userTokenCheck[0]['id'],$expfilter, $userLat, $userLong, $offset, $limit, $filter_values, $this->default_distance);
                        foreach ($boostjobData as $boostjobsData) {

                            if(in_array($boostjobsData['id'], $negletarr)) { } else {
                                $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
                                //if(!empty($jobTop) && count($jobTop)>=0) {
          
                                    if(isset($jobTop[0]['picks_id'])) {
                                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                                    } else{
                                        $datatoppicks1 = "";
                                    }
                                    if(isset($jobTop[1]['picks_id'])) {
                                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                                    } else{
                                        $datatoppicks2 = "";
                                    }
                                    if(isset($jobTop[2]['picks_id'])) {
                                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                                    } else{
                                        $datatoppicks3 = "";
                                    }
                                    
                                    $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userTokenCheck[0]['id']);
                                    if(!empty($savedjob[0]['id'])) {
                                        $savedjob = '1';
                                    } else{
                                        $savedjob = "0";
                                    }

                                    $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                                    if($jobsal[0]['basicsalary']) {
                                        $basicsalary = $jobsal[0]['basicsalary'];
                                    } else{
                                        $basicsalary = 0;
                                    }

                                    $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                                    if(isset($companydetail1[0]['address'])){
                                        $companyaddress=$companydetail1[0]['address'];
                                    }else{
                                        $companyaddress='';
                                    }

                                    $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                                    if(!empty($companyPic[0]['companyPic'])) {
                                        $comPic = trim($companyPic[0]['companyPic']);
                                    } else{
                                        $comPic = "";
                                    }

                                    $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                                    if(isset($jobImage[0]['pic'])){
                                        $jobImageData=$jobImage[0]['pic'];
                                    }else{
                                        $jobImageData=$comPic;
                                    }

                                    $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                                    if(!empty($companyname[0]["cname"])){
                                        $cname = $companyname[0]["cname"];
                                    }else{
                                        $cname = '';
                                    }

                                    if($boostjobsData['chatbot'] == 1) {
                                        $boostjobsData['mode'] = "chatbot";
                                    }

                                    $distance = $this->distance($userLat,$userLong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                    $distance = number_format((float)$distance, 2, '.', ''); 

                                    $bostArr[] = $boostjobsData['id'];

                                    $data1[] = [
                                            "jobpost_id" => $boostjobsData['id'],
                                            "comapnyId" =>$boostjobsData['compId'],
                                            "job_title" => $boostjobsData['jobtitle'],
                                            "jobDesc" => $boostjobsData['jobDesc'],
                                            "jobPitch"=>$boostjobsData['jobPitch'],
                                            "salary" => number_format($boostjobsData['salary']),
                                            "companyName"=> $companydetail[0]["cname"],
                                            "cname"=> $cname,
                                            "companyAddress"=>$companyaddress,
                                            "toppicks1" => $datatoppicks1,
                                            "toppicks2" => $datatoppicks2,
                                            "toppicks3" => $datatoppicks3,
                                            "job_image" =>$jobImageData,
                                            "latitude"=> $boostjobsData['latitude'], 
                                            "longitude"=>$boostjobsData['longitude'],
                                            "jobexpire"=> $boostjobsData['jobexpire'],
                                            "companyPic"=> $comPic,
                                            "savedjob" =>$savedjob,
                                            "boostjob" =>$boostjobsData['boost_status'],
                                            "distance" => $distance,
                                            "mode"=> $boostjobsData['mode'], 
                                            "modeurl"=> $boostjobsData['modeurl'], 
                                            "sharecount"=>$this->getsharecount($boostjobsData['id'])
                                    ];
                                //}
                            }
                        }

                        if($filter_values['sorting'] == 1) {
                            usort($data1, function($a, $b) {
                                return $b['salary'] <=> $a['salary'];
                            });
                        } else {
                            if($filter_values['sorting'] == 2) {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            } else {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            }
                        }
                    }
                
                } else if($userData['type'] == "city") {

                    $getCity = $this->Jobpost_Model->singlecity_fetch($getParamType);
                    $data1 = array();
                    $bostArr = array();

                    if($indexing == 1) {
                        $boostjobData = $this->Jobsapp_Model->cityjob_fetch_groupby($userID, $expfilter, $userLat, $userLong, $userData['typeid'], $filter_values, $this->default_distance);
                        foreach ($boostjobData as $boostjobsData) {

                            if(strtotime($boostjobsData['jobexpire']) >= strtotime($cDate)) {
                                $neglectarr[] = $boostjobsData['id'];
                                $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
                                if(isset($jobTop[0]['picks_id'])) {
                                    $datatoppicks1 = $jobTop[0]['picks_id']; 
                                } else{
                                    $datatoppicks1 = "";
                                }
                                if(isset($jobTop[1]['picks_id'])) {
                                    $datatoppicks2 = $jobTop[1]['picks_id']; 
                                } else{
                                    $datatoppicks2 = "";
                                }
                                if(isset($jobTop[2]['picks_id'])) {
                                    $datatoppicks3 = $jobTop[2]['picks_id']; 
                                } else{
                                    $datatoppicks3 = "";
                                }
                                
                                $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userID);
                                if(!empty($savedjob[0]['id']) && $savedjob[0]['id']==1) {
                                    $savedjob = '1';
                                } else{
                                    $savedjob = "0";
                                }

                                $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                                if($jobsal[0]['basicsalary']) {
                                    $basicsalary = $jobsal[0]['basicsalary'];
                                } else{
                                    $basicsalary = 0;
                                }

                                $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                                if(isset($companydetail1[0]['address'])){
                                    $companyaddress=$companydetail1[0]['address'];
                                }else{
                                    $companyaddress='';
                                }

                                $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }

                                $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                                if(isset($jobImage[0]['pic'])){
                                    $jobImageData=$jobImage[0]['pic'];
                                }else{
                                    $jobImageData=$comPic;
                                }

                                $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                                if(!empty($companyname[0]["cname"])){
                                    $cname = $companyname[0]["cname"];
                                }else{
                                    $cname = '';
                                }

                                if($boostjobsData['chatbot'] == 1) {
                                    $boostjobsData['mode'] = "chatbot";
                                }

                                $distance = $this->distance($userLat,$userLong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                $distance = number_format((float)$distance, 2, '.', ''); 
                                $data1[] = [
                                        "jobpost_id" => $boostjobsData['id'],
                                        "recruiter_id" => $boostjobsData['recruiter_id'],
                                        "comapnyId" =>$boostjobsData['compId'],
                                        "job_title" => $boostjobsData['jobtitle'],
                                        "jobDesc" => $boostjobsData['jobDesc'],
                                        "jobPitch"=>$boostjobsData['jobPitch'],
                                        "salary" => number_format($boostjobsData['salary']),
                                        "companyName"=> $companydetail[0]["cname"],
                                        "cname"=> $cname,
                                        "companyAddress"=>$companyaddress,
                                        "toppicks1" => $datatoppicks1,
                                        "toppicks2" => $datatoppicks2,
                                        "toppicks3" => $datatoppicks3,
                                        "job_image" =>$jobImageData,
                                        "latitude"=> $boostjobsData['latitude'], 
                                        "longitude"=>$boostjobsData['longitude'],
                                        "jobexpire"=> $boostjobsData['jobexpire'],
                                        "companyPic"=> $comPic,
                                        "savedjob" =>$savedjob,
                                        "boostjob" =>$boostjobsData['boost_status'],
                                        "distance" => $distance,
                                        "mode"=> $boostjobsData['mode'], 
                                        "modeurl"=> $boostjobsData['modeurl'], 
                                        "sharecount"=>$this->getsharecount($boostjobsData['id'])
                                ];
                            }
                        }

                        if($filter_values['sorting'] == 1) {
                            usort($data1, function($a, $b) {
                                return $b['salary'] <=> $a['salary'];
                            });
                        } else {
                            if($filter_values['sorting'] == 2) {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            } else {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            }
                        }

                    } else {

                        $boostjobData = $this->Jobsapp_Model->cityjob_fetch_groupby($userID, $expfilter, $userLat, $userLong, $userData['typeid'], $filter_values, $this->default_distance);
                        foreach ($boostjobData as $boostjobsData) {

                            if(strtotime($boostjobsData['jobexpire']) >= strtotime($cDate)) {
                                $neglectarr[] = $boostjobsData['id'];
                            }
                        }

                        $boostjobData = $this->Jobsapp_Model->cityjob_fetch($userID, $expfilter, $userLat, $userLong, $userData['typeid'], $offset, $limit, $filter_values, $this->default_distance);
                        foreach ($boostjobData as $boostjobsData) {

                            if(strtotime($boostjobsData['jobexpire']) >= strtotime($cDate)) {

                                if(in_array($boostjobsData['id'], $neglectarr)) { } else {
                                    $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
                                    if(isset($jobTop[0]['picks_id'])) {
                                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                                    } else{
                                        $datatoppicks1 = "";
                                    }
                                    if(isset($jobTop[1]['picks_id'])) {
                                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                                    } else{
                                        $datatoppicks2 = "";
                                    }
                                    if(isset($jobTop[2]['picks_id'])) {
                                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                                    } else{
                                        $datatoppicks3 = "";
                                    }
                                    
                                    $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userID);
                                    if(!empty($savedjob[0]['id']) && $savedjob[0]['id']==1) {
                                        $savedjob = '1';
                                    } else{
                                        $savedjob = "0";
                                    }

                                    $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                                    if($jobsal[0]['basicsalary']) {
                                        $basicsalary = $jobsal[0]['basicsalary'];
                                    } else{
                                        $basicsalary = 0;
                                    }

                                    $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                                    if(isset($companydetail1[0]['address'])){
                                        $companyaddress=$companydetail1[0]['address'];
                                    }else{
                                        $companyaddress='';
                                    }

                                    $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                                    if(!empty($companyPic[0]['companyPic'])) {
                                        $comPic = trim($companyPic[0]['companyPic']);
                                    } else{
                                        $comPic = "";
                                    }

                                    $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                                    if(isset($jobImage[0]['pic'])){
                                        $jobImageData=$jobImage[0]['pic'];
                                    }else{
                                        $jobImageData=$comPic;
                                    }

                                    $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                                    if(!empty($companyname[0]["cname"])){
                                        $cname = $companyname[0]["cname"];
                                    }else{
                                        $cname = '';
                                    }

                                    if($boostjobsData['chatbot'] == 1) {
                                        $boostjobsData['mode'] = "chatbot";
                                    }

                                    $distance = $this->distance($userLat,$userLong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                    $distance = number_format((float)$distance, 2, '.', '');
                                    $data1[] = [
                                            "jobpost_id" => $boostjobsData['id'],
                                            "recruiter_id" => $boostjobsData['recruiter_id'],
                                            "comapnyId" =>$boostjobsData['compId'],
                                            "job_title" => $boostjobsData['jobtitle'],
                                            "jobDesc" => $boostjobsData['jobDesc'],
                                            "jobPitch"=>$boostjobsData['jobPitch'],
                                            "salary" => number_format($boostjobsData['salary']),
                                            "companyName"=> $companydetail[0]["cname"],
                                            "cname"=> $cname,
                                            "companyAddress"=>$companyaddress,
                                            "toppicks1" => $datatoppicks1,
                                            "toppicks2" => $datatoppicks2,
                                            "toppicks3" => $datatoppicks3,
                                            "job_image" =>$jobImageData,
                                            "latitude"=> $boostjobsData['latitude'], 
                                            "longitude"=>$boostjobsData['longitude'],
                                            "jobexpire"=> $boostjobsData['jobexpire'],
                                            "companyPic"=> $comPic,
                                            "savedjob" =>$savedjob,
                                            "boostjob" =>$boostjobsData['boost_status'],
                                            "distance" => $distance,
                                            "mode"=> $boostjobsData['mode'], 
                                            "modeurl"=> $boostjobsData['modeurl'], 
                                            "sharecount"=>$this->getsharecount($boostjobsData['id'])
                                    ];
                                }
                            }
                        }

                        if($filter_values['sorting'] == 1) {
                            usort($data1, function($a, $b) {
                                return $b['salary'] <=> $a['salary'];
                            });
                        } else {
                            if($filter_values['sorting'] == 2) {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            } else {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            }
                        }
                    }
                
                } else if($userData['type'] == "actively") {

                    if($indexing == 1) {

                        $hotjobData = $this->Jobsapp_Model->job_fetch_actively_groupby($userTokenCheck[0]['id'], $userLat, $userLong, $expfilter, $filter_values, $this->default_distance);
                        foreach ($hotjobData as $hotjobsData) {
                            $negletarr[] = $hotjobsData['id'];
                            $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                            if(isset($jobTop[0]['picks_id'])){
                                $datatoppicks1 = $jobTop[0]['picks_id']; 
                            } else {
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $datatoppicks2 = $jobTop[1]['picks_id']; 
                            } else {
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop[2]['picks_id'])) {
                                $datatoppicks3 = $jobTop[2]['picks_id']; 
                            } else {
                                $datatoppicks3 = "";
                            }
                            
                            $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['id']);
                            if(!empty($savedjob[0]['id'])) {
                                $savedjob = '1';
                            } else{
                                $savedjob = "0";
                            }

                            $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                            if($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else{
                                $basicsalary = 0;
                            }

                            $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                            if(isset($companydetail1[0]['address'])) {
                                $companyaddress=$companydetail1[0]['address'];
                            }
                            else {
                                $companyaddress='';
                            }

                            $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                            if(!empty($companyPic[0]['companyPic'])) {
                                $comPic = trim($companyPic[0]['companyPic']);
                            } else{
                                $comPic = "";
                            }

                            $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                            if(isset($jobImage[0]['pic'])) {
                                $jobImageData=$jobImage[0]['pic'];
                            }
                            else {
                                $jobImageData=$comPic;
                            }

                            $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            if($hotjobsData['chatbot'] == 1) {
                                $hotjobsData['mode'] = "chatbot";
                            }

                            $distance = $this->distance($userLat, $userLong, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', ''); 

                            $data1[] = [
                                "jobpost_id" => $hotjobsData['id'],
                                "recruiter_id" => $hotjobsData['recruiter_id'],
                                "comapnyId" =>$hotjobsData['compId'],
                                "job_title" => $hotjobsData['jobtitle'],
                                "jobDesc" => $hotjobsData['jobDesc'],
                                "salary" => number_format($hotjobsData['salary']),
                                "companyName"=> $companydetail[0]["cname"],
                                "cname"=> $cname,
                                "companyAddress"=>$companyaddress,
                                "toppicks1" => $datatoppicks1,
                                "toppicks2" => $datatoppicks2,
                                "toppicks3" => $datatoppicks3,
                                "job_image" =>$jobImageData,
                                "latitude"=> $hotjobsData['latitude'], 
                                "longitude"=>$hotjobsData['longitude'],
                                "jobexpire"=> $hotjobsData['jobexpire'],
                                "companyPic"=> $comPic,
                                "savedjob" =>$savedjob,
                                "boostjob" =>$hotjobsData['boost_status'],
                                "distance" => $distance,
                                "mode"=> $hotjobsData['mode'],
                                "modeurl"=> $hotjobsData['modeurl'],
                                "sharecount"=>$this->getsharecount($hotjobsData['id'])
                            ];
                        }  

                        // if($filter_values['sorting'] == 1) {
                        //     usort($data1, function($a, $b) {
                        //         return $b['salary'] <=> $a['salary'];
                        //     });
                        // } else {
                        //     if($filter_values['sorting'] == 2) {
                        //         usort($data1, function($a, $b) {
                        //             return $a['distance'] <=> $b['distance'];
                        //         });
                        //     } else {
                        //         usort($data1, function($a, $b) {
                        //             return $a['distance'] <=> $b['distance'];
                        //         });
                        //     }
                        // }

                    } else {

                        $hotjobData = $this->Jobsapp_Model->job_fetch_actively_groupby($userTokenCheck[0]['id'], $userLat, $userLong, $expfilter, $filter_values, $this->default_distance);
                        foreach ($hotjobData as $hotjobsData) {
                            $negletarr[] = $hotjobsData['id'];
                        }  
                        $hotjobData = $this->Jobsapp_Model->actively_fetch_latlong($userTokenCheck[0]['id'], $userLat, $userLong, $expfilter, $offset, $limit, $filter_values, $this->default_distance);
                        foreach ($hotjobData as $hotjobsData) {

                            if(in_array($hotjobsData['id'], $negletarr)) { } else {
                            
                                $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                                if(isset($jobTop[0]['picks_id'])){
                                    $datatoppicks1 = $jobTop[0]['picks_id']; 
                                } else {
                                    $datatoppicks1 = "";
                                }
                                if(isset($jobTop[1]['picks_id'])) {
                                    $datatoppicks2 = $jobTop[1]['picks_id']; 
                                } else {
                                    $datatoppicks2 = "";
                                }
                                if(isset($jobTop[2]['picks_id'])) {
                                    $datatoppicks3 = $jobTop[2]['picks_id']; 
                                } else {
                                    $datatoppicks3 = "";
                                }
                                
                                $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['id']);

                                if(!empty($savedjob[0]['id'])) {
                                    $savedjob = '1';
                                } else{
                                    $savedjob = "0";
                                }

                                 $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);

                                if($jobsal[0]['basicsalary']) {
                                    $basicsalary = $jobsal[0]['basicsalary'];
                                } else{
                                    $basicsalary = 0;
                                }

                                $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                                if(isset($companydetail1[0]['address'])){
                                    $companyaddress=$companydetail1[0]['address'];
                                }else{
                                    $companyaddress='';
                                }

                                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }
                                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                                if(isset($jobImage[0]['pic'])){
                                    $jobImageData=$jobImage[0]['pic'];
                                }else{
                                    $jobImageData=$comPic;
                                }

                                $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                                if(!empty($companyname[0]["cname"])){
                                    $cname = $companyname[0]["cname"];
                                }else{
                                    $cname = '';
                                }

                                if($hotjobsData['chatbot'] == 1) {
                                    $hotjobsData['mode'] = "chatbot";
                                }

                                $distance = $this->distance($userData['cur_lat'], $userData['cur_long'],$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                $distance = number_format((float)$distance, 2, '.', ''); 

                                $data1[] = [
                                    "jobpost_id" => $hotjobsData['id'],
                                    "comapnyId" =>$hotjobsData['compId'],
                                    "job_title" => $hotjobsData['jobtitle'],
                                    "jobDesc" => $hotjobsData['jobDesc'],
                                    "salary" => number_format($hotjobsData['salary']),
                                    "companyName"=> $companydetail[0]["cname"],
                                    "cname"=> $cname,
                                    "companyAddress"=>$companyaddress,
                                    "toppicks1" => $datatoppicks1,
                                    "toppicks2" => $datatoppicks2,
                                    "toppicks3" => $datatoppicks3,
                                    "job_image" =>$jobImageData,
                                    "latitude"=> $hotjobsData['latitude'], 
                                    "longitude"=>$hotjobsData['longitude'],
                                    "jobexpire"=> $hotjobsData['jobexpire'],
                                    "companyPic"=> $comPic,
                                    "savedjob" =>$savedjob,
                                    "boostjob" =>$hotjobsData['boost_status'],
                                    "distance" => $distance,
                                    "mode"=> $hotjobsData['mode'],
                                    "modeurl"=> $hotjobsData['modeurl'],
                                    "sharecount"=>$this->getsharecount($hotjobsData['id'])
                                ];
                            }
                        }

                        if($filter_values['sorting'] == 1) {
                            usort($data1, function($a, $b) {
                                return $b['salary'] <=> $a['salary'];
                            });
                        } else {
                            if($filter_values['sorting'] == 2) {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            } else {
                                usort($data1, function($a, $b) {
                                    return $a['distance'] <=> $b['distance'];
                                });
                            }
                        }
                    }

                }

                
                $data2["hotjobss"] = array();
                $data2["hotjobss"] = $data1;

                $fetchnotifications = $this->Candidate_Model->fetchnotifications($userTokenCheck[0]['id']);
                $fetchpromonotifications = $this->Candidate_Model->fetchpromonotifications($userTokenCheck[0]['id']);
                $response = ['hotjobslist' => $data2,'notification_count' =>count($fetchnotifications)+count($fetchpromonotifications), 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function cityListings_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        
        $userTokenCheck1 = $this->User_Model->token_match1($data);

        $userLat = $userData['cur_lat'];
        $userLong = $userData['cur_long'];
        $userCity = $userData['city'];
        
        if($userTokenCheck1) {
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            
            if($userTokenCheck) {
                $expmonth = $userTokenCheck[0]['exp_month'];
                $expyear = $userTokenCheck[0]['exp_year'];

                if($expyear == 0 && $expmonth == 0) {
                    $expfilter = "1";
                }else if($expyear == 0 && $expmonth < 6) {
                    $expfilter = "2";
                } else if($expyear < 1 && $expmonth >= 6) {
                    $expfilter = "3";
                } else if($expyear < 2 && $expyear >= 1) {
                    $expfilter = "4";
                } else if($expyear < 3 && $expyear >= 2) {
                    $expfilter = "5";
                }else if($expyear < 4 && $expyear >= 3) {
                    $expfilter = "6";
                }else if($expyear < 5 && $expyear >= 4) {
                    $expfilter = "7";
                }else if($expyear < 6 && $expyear >= 5) {
                    $expfilter = "8";
                }else if($expyear < 7 && $expyear >= 6) {
                    $expfilter = "9";
                } else if($expyear >= 7) { 
                    $expfilter = "10";
                } else {
                    $expfilter = "";
                }
            }

            $boostjobData = $this->Jobpost_Model->cityjob_fetch($userTokenCheck[0]['id'],$expfilter, $userLat, $userLong, $userCity);

            $data1 = array();
            $data1=[];
            $bostArr = array();

            foreach ($boostjobData as $boostjobsData) {

                $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
              
                if(!empty($jobTop) && count($jobTop)>=0) {

                    if(isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }
                    
                    $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userTokenCheck[0]['id']);
                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }

                    $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = 0;
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                    if(isset($companydetail1[0]['address'])){
                        $companyaddress=$companydetail1[0]['address'];
                    }else{
                        $companyaddress='';
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                    if(isset($jobImage[0]['pic'])){
                        $jobImageData=$jobImage[0]['pic'];
                    }else{
                        $jobImageData=$comPic;
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($userLat,$userLong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', ''); 

                    $bostArr[] = $boostjobsData['id'];

                    $data1[] = [
                            "jobpost_id" => $boostjobsData['id'],
                            "comapnyId" =>$boostjobsData['compId'],
                            "job_title" => $boostjobsData['jobtitle'],
                            "jobDesc" => $boostjobsData['jobDesc'],
                            "jobPitch"=>$boostjobsData['jobPitch'],
                            "salary" => number_format($boostjobsData['salary']),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $boostjobsData['latitude'], 
                            "longitude"=>$boostjobsData['longitude'],
                            "jobexpire"=> $boostjobsData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$boostjobsData['boost_status'],
                            "distance" => $distance,
                            "mode"=> $boostjobsData['mode'], 
                    ];
                }
            }

            $fetchnotifications = $this->Candidate_Model->fetchnotifications($userTokenCheck[0]['id']);
            $fetchpromonotifications = $this->Candidate_Model->fetchpromonotifications($userTokenCheck[0]['id']);
            $response = ['cityjobslist' => $data1,'notification_count' =>count($fetchnotifications)+count($fetchpromonotifications), 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } else {

            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function instantscreening_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);

        $jobpost_id = $userData['jobpost_id'];
        
        if($userTokenCheck1) {
            $user_id = $userTokenCheck1[0]['user_id'];

            $checkScreening = $this->Jobpost_Model->check_phone_screening($user_id, $jobpost_id);
            
            if($checkScreening) {

                $response = ['instantscreening' => "SUCCESS", 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);

            } else {

                $data = ["user_id"=>$user_id, "jobpost_id"=>$jobpost_id, "status"=>1];
                $this->Jobpost_Model->job_screening($data);

                $response = ['instantscreening' => "SUCCESS", 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }

        } else {

            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function fetchcompany_post() {
        $searchTerm = $this->input->post('keyword');
        $token = $this->input->post('token');
        $search_array = array();
        $search_array1 = array();
        $search_array2 = array();
        $search_array3 = array();
        $search_array4 = array();

        $data = ["token" => $token];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        $userLat = $userData['cur_lat'];
        $userLong = $userData['cur_long'];

        
        if(strlen($searchTerm) > 0) {

            if($userTokenCheck1) {

                $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
                $userTokenCheck = $this->User_Model->token_match($tokendata);
                
                if($userTokenCheck) {
                    $expmonth = $userTokenCheck[0]['exp_month'];
                    $expyear = $userTokenCheck[0]['exp_year'];

                    if($expyear == 0 && $expmonth == 0) {
                        $expfilter = "1";
                    }else if($expyear == 0 && $expmonth < 6) {
                        $expfilter = "2";
                    } else if($expyear < 1 && $expmonth >= 6) {
                        $expfilter = "3";
                    } else if($expyear < 2 && $expyear >= 1) {
                        $expfilter = "4";
                    } else if($expyear < 3 && $expyear >= 2) {
                        $expfilter = "5";
                    }else if($expyear < 4 && $expyear >= 3) {
                        $expfilter = "6";
                    }else if($expyear < 5 && $expyear >= 4) {
                        $expfilter = "7";
                    }else if($expyear < 6 && $expyear >= 5) {
                        $expfilter = "8";
                    }else if($expyear < 7 && $expyear >= 6) {
                        $expfilter = "9";
                    } else if($expyear >= 7) { 
                        $expfilter = "10";
                    } else {
                        $expfilter = "";
                    }

                    $job_list = $this->Common_Model->home_search_job_recruiter($searchTerm);
                    if (!empty($job_list)) {
                        $search_array1 = $job_list;            
                    }
                    $job_list = $this->Common_Model->home_search_job_company($searchTerm);
                    if (!empty($job_list)) {
                        $search_array2 = $job_list;            
                    }
                    $job_list1 = $this->Common_Model->home_search_job_title($searchTerm, $expfilter, $userTokenCheck1[0]['user_id']);
                    if (!empty($job_list1)) {
                        $search_array3 = $job_list1;
                    }

                    $search_array4 = array_merge($search_array1, $search_array2);
                    $search_array4 = array_merge($search_array3, $search_array4);
                    
                    if(count($search_array4) > 0) {
                        $response = ['status'=>"SUCCESS", 'company_lists'=> $search_array4];
                        return $this->set_response($response, REST_Controller::HTTP_CREATED);
                        exit;
                    
                    } else {

                        $break_string = explode(' ',$searchTerm);
                        if(count($break_string) >= 3) {
                            $searchTerm1 = $break_string[0].' '.$break_string[1];
                        
                        } else if(count($break_string) == 2) {
                            $searchTerm1 = $break_string[0];
                        } else {
                            $searchTerm1 = $searchTerm;
                        }

                        $job_list = $this->Common_Model->home_search_job_recruiter($searchTerm1);
                        if (!empty($job_list)) {
                            $search_array1 = $job_list;            
                        }
                        $job_list = $this->Common_Model->home_search_job_company($searchTerm1);
                        if (!empty($job_list)) {
                            $search_array2 = $job_list;            
                        }
                        $job_list1 = $this->Common_Model->home_search_job_title($searchTerm1, $expfilter, $userTokenCheck1[0]['user_id']);
                        if (!empty($job_list1)) {
                            $search_array3 = $job_list1;
                        }

                        $search_array4 = array_merge($search_array1, $search_array2);
                        $search_array4 = array_merge($search_array3, $search_array4);

                        if(count($search_array4) > 0) {
                            $response = ['status'=>"SUCCESS", 'company_lists'=> $search_array4];
                            return $this->set_response($response, REST_Controller::HTTP_CREATED);
                            exit;
                        } else {

                            $break_string = explode(' ',$searchTerm);
                            if(count($break_string) >= 3) {
                                $searchTerm2 = $break_string[0];
                        
                            } else if(count($break_string) == 2) {
                                $searchTerm2 = $break_string[0];
                            } else {
                                $searchTerm2 = $searchTerm;
                            }

                            $job_list = $this->Common_Model->home_search_job_recruiter($searchTerm2);
                            if (!empty($job_list)) {
                                $search_array1 = $job_list;            
                            }
                            $job_list = $this->Common_Model->home_search_job_company($searchTerm2);
                            if (!empty($job_list)) {
                                $search_array2 = $job_list;            
                            }
                            $job_list1 = $this->Common_Model->home_search_job_title($searchTerm2, $expfilter, $userID);
                            if (!empty($job_list1)) {
                                $search_array3 = $job_list1;
                            }
                            $search_array4 = array_merge($search_array1, $search_array2);
                            $search_array4 = array_merge($search_array3, $search_array4);
                            
                            if(count($search_array4) > 0) {
                                $response = ['status'=>"SUCCESS", 'company_lists'=> $search_array4];
                                return $this->set_response($response, REST_Controller::HTTP_CREATED);
                                exit;
                            } else {
                                $response = ['status'=>"SUCCESS", 'company_lists'=> []];
                                return $this->set_response($response, REST_Controller::HTTP_CREATED);
                                exit;
                            }
                        }
                    }
                }
            } else {

                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    public function chatbot_post() {
        $searchTerm = $this->input->post();
        $data = ["token" => $searchTerm['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        $search_array = array();

        if($userTokenCheck1) {

            if(!empty($searchTerm)) {

                $job_list = $this->Jobpost_Model->job_detail_fetch($searchTerm['jobpost_id']);
                if ($job_list) {
                
                    if($job_list[0]['chatbot'] == "1") {

                        $questionnair = $this->Questionnaire_Model->questionnaire_fetch($searchTerm['jobpost_id']);
                        if($questionnair) {

                            foreach($questionnair as $quest) {

                                if($quest['type'] == "option") {
                                    $search_array[] = [
                                        "id" => $quest['id'],
                                        "question" => $quest['question'],
                                        "type" => $quest['type'],
                                        "options" => explode(',', $quest['options']),
                                        "answer" => $quest['answer'],
                                    ];
                                } else {
                                    $search_array[] = [
                                        "id" => $quest['id'],
                                        "question" => $quest['question'],
                                        "type" => $quest['type'],
                                        "options" => [],
                                        "answer" => $quest['answer'],
                                    ];
                                }
                            }
                            $response = ['status'=>"SUCCESS", 'chatbot'=>$search_array];
                            return $this->set_response($response, REST_Controller::HTTP_CREATED);     

                        } else {

                            $response = ['status'=>"SUCCESS", 'chatbot'=>'Chatbot not available'];
                            return $this->set_response($response, REST_Controller::HTTP_CREATED);                        
                        }

                    } else {
                        
                        $response = ['status'=>"SUCCESS", 'chatbot'=>'Chatbot not available'];
                        return $this->set_response($response, REST_Controller::HTTP_CREATED);                    
                    }
                
                } else {

                    $response = ['status'=>"SUCCESS", 'chatbot'=>'Job not found'];
                    return $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
            } else {
                $response = ['status'=>"SUCCESS", 'chatbot'=>'Job id required'];
                return $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        } else {

            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function chatbotsave_post() {

        $chatData = $this->input->post();
        $data = ["token" => $chatData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        $search_array = array();
        $flag = 1;
        if($userTokenCheck1) {

            $data_ques = explode('$',$chatData['ques_id']);
            $data_answer = explode('$', $chatData['answer']);

            for($x=0; $x<count($data_ques); $x++) {
                
                $quesData = $this->Questionnaire_Model->questionnaire_fetch_byques($data_ques[$x]);
                if($quesData[0]['haverequired'] == 1) {

                    if($quesData[0]['type'] == "text") {
                        $flag = 3;
                    } else {
                        if($data_answer[$x] != $quesData[0]['answer']) {
                            $flag = 2;
                        }
                    }
                } else {
                    if($quesData[0]['type'] == "text") {
                        $flag = 3;
                    }
                }

                $data = ["user_id"=>$userTokenCheck1[0]['user_id'], "job_id"=>$chatData['job_id'], "ques_id"=>$data_ques[$x], "answer"=>$data_answer[$x], "percentage"=>$chatData['percentage']];
                $this->Questionnaire_Model->questionnaire_user_save($data);
            }

            if($flag == 2) {
                $this->Questionnaire_Model->questionnaire_user_update($userTokenCheck1[0]['user_id'], $chatData['job_id']);
            }
            if($flag == 3) {
                $this->Questionnaire_Model->questionnaire_user_update1($userTokenCheck1[0]['user_id'], $chatData['job_id']);
            }

            $response = ['status'=>"SUCCESS", 'chatbot'=>'chatbot saved'];
            return $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {

            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function chatbotarraysave_post() {

        $chatData = $this->input->post();
        $data = ["token" => $chatData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        $search_array = array();

        if($userTokenCheck1) {

            $data_ques = explode('==',$chatData['ques_id']);
            $data_answer = explode('==', $chatData['answer']);

            for($x=0; $x<count($data_ques); $x++) {
                
                $data = ["user_id"=>$userTokenCheck1[0]['user_id'], "job_id"=>$chatData['job_id'], "ques_id"=>$data_ques[$x], "answer"=>$data_answer[$x], "percentage"=>$chatData['percentage']];
                $this->Questionnaire_Model->questionnaire_user_save($data);
            }

            $response = ['status'=>"SUCCESS", 'chatbot'=>'chatbot saved'];
            return $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {

            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function chathistory_post() {

        $chatData = $this->input->post();
        $data = ["token" => $chatData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        $search_array = array();

        if($userTokenCheck1) {
            $data = array();
            $questSaved = $this->Questionnaire_Model->questionnaire_saved_list($userTokenCheck1[0]['user_id']);
            if($questSaved) {
                
                foreach($questSaved as $questS) {

                    $job_detail = $this->Jobpost_Model->job_detail_fetch($questS['job_id']);
                    $companydetail = $this->Jobpost_Model->job_detailLocation_fetch($job_detail[0]['company_id']);
                    if(empty($companydetail[0]['site_name'])) {
                        $companydetail = $this->Jobpost_Model->fetch_companyaddress($job_detail[0]['recruiter_id']);
                        $companyName = $companydetail[0]['cname']; 
                    } else {
                        $companyName = $companydetail[0]['site_name'];
                    }
                    $jobImage = $this->Jobpost_Model->job_image($questS['job_id']);
                    if(isset($jobImage[0]['pic'])){
                        $jobImageData=$jobImage[0]['pic'];
                    } else {
                        $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }
                        $jobImageData=$comPic;
                    }

                    $data[] = [
                        "job_id"=>$questS['job_id'],
                        "jobtitle" => $job_detail[0]['jobtitle'],
                        "cname" => $companyName,
                        "image" => $jobImageData,
                        "applied_at"=>date('Y-m-d', strtotime($questS['created_at']))
                    ];
                }

                $response = ['status'=>"SUCCESS", 'chathistory'=>$data];
                return $this->set_response($response, REST_Controller::HTTP_CREATED);
            } else {

                $response = ['status'=>"FAILURE", 'chathistory'=>[]];
                return $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        
        } else {

            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function chathistoryview_post() {

        $chatData = $this->input->post();
        $jobid = $chatData["job_id"];
        $data = ["token" => $chatData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        $search_array = array();

        if($userTokenCheck1) {
            $data = array();
            $questSaved = $this->Questionnaire_Model->questionnaire_saved_single($jobid, $userTokenCheck1[0]['user_id']);
            if($questSaved) {
                
                foreach($questSaved as $questS) {

                    $questionnair = $this->Questionnaire_Model->questionnaire_ques_fetch($questS['ques_id']);
                    if($questionnair) {

                        foreach($questionnair as $quest) {

                            if($quest['type'] == "option") {
                                $search_array[] = [
                                    "question" => $quest['question'],
                                    "type" => $quest['type'],
                                    "options" => explode(',', $quest['options']),
                                    "answer" => $quest['answer'],
                                    "useranswer" => $questS['answer'],
                                    "percentage" => $questS['percentage'],
                                ];
                            } else {
                                $search_array[] = [
                                    "question" => $quest['question'],
                                    "type" => $quest['type'],
                                    "options" => [],
                                    "answer" => $quest['answer'],
                                    "useranswer" => $questS['answer'],
                                    "percentage" => $questS['percentage'],
                                ];
                            }
                        }
                    }
                }
                $response = ['status'=>"SUCCESS", 'chathistoryview'=>$search_array];
                return $this->set_response($response, REST_Controller::HTTP_CREATED);  
            } else {
                $response = ['status'=>"SUCCESS", 'chathistoryview'=>[]];
                return $this->set_response($response, REST_Controller::HTTP_CREATED);                        
            }
        
        } else {

            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function sharecount_get() {

            $questSaved = $this->Questionnaire_Model->job_fetch();
            if($questSaved) {
                
                foreach($questSaved as $questS) {
                    $random = rand(50,100);
                    $data = ["job_id"=>$questS['id'], "sharecount"=>$random];
                    $this->Questionnaire_Model->job_share_count($data);
                }
                $response = ['status'=>"SUCCESS", 'chathistoryview'=>"done"];
                return $this->set_response($response, REST_Controller::HTTP_CREATED);  
            } else {
                $response = ['status'=>"SUCCESS", 'chathistoryview'=>[]];
                return $this->set_response($response, REST_Controller::HTTP_CREATED);                        
            }
        
    }

    public function savesharecount_post() {

            $jobid = $this->input->post('job_id');

            $questSaved = $this->Questionnaire_Model->job_fetch_share_single($jobid);
            if($questSaved) {
                
                    $sharecount = $questSaved[0]['sharecount'] + 1;
                    $this->Questionnaire_Model->job_share_count_save($jobid, $sharecount);
                
                $response = ['status'=>"SUCCESS", 'chathistoryview'=>"done"];
                return $this->set_response($response, REST_Controller::HTTP_CREATED);  
            } else {
                $response = ['status'=>"SUCCESS", 'chathistoryview'=>[]];
                return $this->set_response($response, REST_Controller::HTTP_CREATED);                        
            }
        
    }
    
    public function autocompleteaddress_post() {
        $search_filter = $this->input->post();

        $search_array = array();

        $searchingResults = $this->Jobpost_Model->company_address_autocomplete($search_filter['search']);

        foreach($searchingResults as $searchingResult) {

            $search_array[] = ["address"=>$searchingResult['address'], "latitude"=>$searchingResult['latitude'], "longitude"=>$searchingResult['longitude']];
        }

        $response = ['status'=>"SUCCESS", 'autocompleteaddress'=>$search_array];
        return $this->set_response($response, REST_Controller::HTTP_CREATED);
    }

    public function distance($lat1, $lon1, $lat2, $lon2, $unit) {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) *sin(deg2rad($lat2)) + cos(deg2rad($lat1))* cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist* 60 *1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    public function getsharecount($jobpost_id) {
        $jobShare = $this->Jobpost_Model->get_share_fetch($jobpost_id);
        if($jobShare) {
            $jobShareCount = $jobShare[0]['sharecount'];
        } else{
            $jobShareCount = "0";
        }
        return $jobShareCount;
    }
    
    public function fetchUserCompleteData($id) {
        $userData = $this->User_Model->userComplete_single($id);
        
        if($userData) {
          $data['comp'] = $userData[0]['signup'] + $userData[0]['profile'] + $userData[0]['profilePic'] + $userData[0]['resume'] + $userData[0]['experience'] + $userData[0]['education'] + $userData[0]['assessment'] + $userData[0]['language'] + $userData[0]['expert'] + $userData[0]['topClient']+ $userData[0]['dob']+ $userData[0]['location']+ $userData[0]['designation']+ $userData[0]['current_salary']+ $userData[0]['exp'];
        } else{
          $data['comp'] = 0;
        }
        return $data;
    }


}
?>
