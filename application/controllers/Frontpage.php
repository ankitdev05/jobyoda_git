<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittall
 * @license         Mobulous
 */
class FrontPage extends CI_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('newmodels/User_Model');
        $this->load->model('newmodels/Recruiteradmin_Model');
        $this->load->model('newmodels/recruiter/Jobpost_Model');
        $this->load->model('newmodels/recruiter/Newpoints_Model');
        $this->load->model('newmodels/Common_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');
    }

    public function contact() {

        if($this->session->userdata('contactsuccessmsg')) {
            $this->session->set_tempdata('successmsg', "Submitted Successfully.");
            $this->session->unset_userdata('contactsuccessmsg');
        }

        $data['contact'] = $this->Recruiteradmin_Model->fetchCondRow('setting',array('id'=>1));

        $data['meta_title'] = $data['contact']->meta_tag;
        $data['meta_description'] = $data['contact']->meta_description;

        $this->load->view('contact', $data);
    }

    public function usercontact(){
        $userData = $this->input->post();
        
        $data = ["name"=>$userData['fname'], "email"=> $userData['email'], 'phone'=>$userData['phone'], "message"=>$userData['message']];
        $contact_inserted =  $this->User_Model->insert_contact($data);
        $contact_inserted = true;
        if($contact_inserted) {
            $this->session->set_userdata('contactsuccessmsg', "Submitted Successfully.");
            redirect("thank_you");
        }
    }
    
    public function thankyou() {

        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat = '14.5676285';
            $current_long = '120.7397551';
        }

        $data['openings'] = $this->Jobpost_Model->job_openings();

        $this->load->view('thankyoupage', $data);
    }

    public function aboutUs() {
        $data['content'] = $this->Common_Model->about_us();
        $data['meta_title'] = $data['content'][0]['meta_tag'];
        $data['meta_description'] = $data['content'][0]['meta_description'];
        
        $this->load->view('about_Us',$data);
    }

    public function terms() {
        $data['content'] = $this->Common_Model->terms();
        $data['meta_title'] = $data['content'][0]['meta_tag'];
        $data['meta_description'] = $data['content'][0]['meta_description'];
        
        $this->load->view('terms',$data);
    }

    public function privacy() {
        $data['content'] = $this->Common_Model->privacy();
        $data['meta_title'] = $data['content'][0]['meta_tag'];
        $data['meta_description'] = $data['content'][0]['meta_description'];

    	$this->load->view('privacy',$data);
    }

    public function faq() {

        $data['content'] = $this->Common_Model->faq_lists();
        $data['contact'] = $this->Recruiteradmin_Model->fetchCondRow('setting',array('id'=>1));
        $data['meta_title'] = $data['contact']->faq_meta_tag;
        $data['meta_description'] = $data['contact']->faq_meta_description;

        $this->load->view('faqpage', $data);
    }

    public function howitworks() {

        $company_list = $this->Common_Model->company_lists();
        if ($company_list) {
            $x = 0;
            foreach ($company_list as $company_lists) {
                $checkjob = $this->Common_Model->check_jobs($company_lists['id']);
                if ($checkjob) {
                    $company_lists1[$x] = ["id" => $company_lists['id'], "cname" => $company_lists['cname']];
                    $x++;
                }
            }
        } else {
            $company_lists1 = [];
        }
        
        $data['companynamelist'] = $company_lists1;
        $data['companylists'] = $company_list;
        $data['openings'] = $this->Jobpost_Model->job_openings();
        $data['userlist'] = $this->Common_Model->user_list();
        $data['edit'] = $this->Recruiteradmin_Model->fetchCondRow('how_it_works',array('id'=>1));
        $data['meta_title'] = $data['edit']->meta_tag;
        $data['meta_description'] = $data['edit']->meta_description;

        $this->load->view('howitworks', $data);
    }

    public function sitemap() {

        $this->load->view('sitemap');
    }

}
?>