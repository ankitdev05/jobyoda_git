<?php
ob_start();
class Jobseekeradmin_Model extends CI_Model {

   function __construct() { 
      parent::__construct(); 
   } 

   public function promo_inserted($data) {
      
      if($this->db->insert("test_promo_inserted", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }

   }

   public function jobseeker_lists() {
      //$this->db->where('parent_id', 0);
      $this->db->select('a.name,a.phone, a.app_version, a.last_used, a.platform, a.active, a.email, a.education, a.location, a.nationality, a.superpower, a.exp_month, a.exp_year, a.id, a.created_at, a.type,a.state,a.city,a.jobsInterested, a.device_type, COUNT(g.id) as countid');
        $this->db->from('user a');
        $this->db->join('sign_in_bonus g', 'g.user_id=a.id', 'left');
        $this->db->where('a.phone!=','');
        $this->db->order_by('a.id','desc');
        $this->db->group_by('a.id');
        $query = $this->db->get(); 
      return $query->result_array();
   }

   public function jobseeker_lists_by_filters($superpower, $location) {
      //$this->db->where('parent_id', 0);
      $this->db->select('id');
        $this->db->from('user');
        $this->db->where('phone!=','');
        
        if(!empty($superpower)) {
          $this->db->like('superpower', $superpower, 'both');
        }
        if(!empty($location)) {
          $this->db->like('location', $location, 'both');
        }
        $this->db->order_by('id','desc');
        $this->db->group_by('id');
        $query = $this->db->get(); 
      return $query->result_array();
   }

   public function jobseeker_bylocationlists($jobsubcat, $joblocation) {
      $this->db->select('id, name, phone, app_version, last_used, platform, active, email, education, location, nationality, superpower, exp_month, exp_year, created_at, type, device_type, jobsInterested, latitude, longitude');
        $this->db->from('user');
        $this->db->where('phone!=','');
        $this->db->where('active', 1);
        $this->db->where('status', 1);
        $this->db->like('jobsInterested', $jobsubcat);
        $this->db->like('location', $joblocation);
        $this->db->order_by('id','desc');
        $query = $this->db->get(); 
      return $query->result_array();
   }

   public function jobseeker_bylocationlists_newone($userLat, $userLong) {
      $this->db->select("id, name, phone, app_version, last_used, platform, active, email, education, location, nationality, superpower, exp_month, exp_year, created_at, type, device_type, jobsInterested, latitude, longitude, (6371 * acos (cos ( radians($userLat) )* cos( radians( latitude ) ) * cos( radians( longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( latitude ) ))) AS distance");
        $this->db->from('user');
        $this->db->where('phone!=','');
        //$this->db->where('verify_status', 1);
        $this->db->where('active', 1);
        $this->db->where('status', 1);
        $this->db->where('notification_status', 1);
        $this->db->where('jobsInterested IS NOT NULL', null, false);
        $this->db->where('latitude IS NOT NULL', null, false);
        $this->db->where('last_used  >= ( CURDATE() - INTERVAL 180 DAY )');

        $this->db->where('notification_status', 1);

        //$this->db->like('jobsInterested', $jobsubcat);
        //$this->db->like('location', $joblocation);
        $this->db->having("distance <=","50");
        $this->db->order_by('id','ASC');
        $query = $this->db->get(); 
      return $query->result_array();
   }

   public function jobseekerpower_bylocationlists($power, $joblocation) {
      $this->db->select('id, name, phone, app_version, last_used, platform, active, email, education, location, nationality, superpower, exp_month, exp_year, created_at, type, device_type, jobsInterested, latitude, longitude');
        $this->db->from('user');
        $this->db->where('phone!=','');
        $this->db->like('superpower', $power);
        //$this->db->like('location', $joblocation);
        $this->db->where('superpower is NOT NULL', NULL, FALSE);
        $this->db->order_by('id','desc');
        $query = $this->db->get(); 
      return $query->result_array();
   }

   public function jobseeker_byIntrestedInId($userIdArr) {
      $this->db->select('id, name, phone, app_version, last_used, platform, active, email, education, location, nationality, superpower, exp_month, exp_year, created_at, type, device_type, jobsInterested, latitude, longitude');
        $this->db->from('user');
        $this->db->where('phone!=','');
        $this->db->where_in('id', $userIdArr);
        //$this->db->like('location', $joblocation);
        $this->db->order_by('id','desc');
        $query = $this->db->get(); 
      return $query->result_array();
   }

   public function fetch_bonus($user_id){
      $this->db->select('sign_in_bonus.bonus,sign_in_bonus.added_at,user.name');
      $this->db->from('sign_in_bonus');
      $this->db->join('user','sign_in_bonus.user_id=user.id');
      $this->db->where('sign_in_bonus.user_id',$user_id);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function notification_lists(){
      $this->db->select('promo_notifications.title,promo_notifications.message, promo_notifications.added_at,user.name,user.superpower');
      $this->db->from('promo_notifications');
      $this->db->join('user','promo_notifications.user_id=user.id');
      $this->db->order_by('promo_notifications.id','desc');
      $this->db->limit(3000);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function notificationcount_notsend(){
      $this->db->select('count(id) as countid');
      $this->db->from('promo_notifications');
      $this->db->where("cron_status", 1);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function notificationcount_send(){
      $this->db->select('count(id) as countid');
      $this->db->from('promo_notifications');
      $this->db->where("cron_status", 2);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function jobrecordnotification_lists(){
      $this->db->select('id,job_id,title,notification,intrestedIn,superpower,created_at');
      $this->db->order_by('id','desc');
      $this->db->limit(5000);
      $query = $this->db->get('jobs_notify_records');
      return $query->result_array();
   }

   public function jobnotificationcount_lists($id){
      $this->db->select('count(id) as countid');
      $this->db->from('jobs_notify');
      $this->db->where("jobs_notify.notify_id",$id);
      $this->db->order_by('jobs_notify.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function jobnotificationcount_lists_notsend($id){
      $this->db->select('count(id) as countid');
      $this->db->from('jobs_notify');
      $this->db->where("jobs_notify.notify_id",$id);
      $this->db->where("jobs_notify.status", 1);
      $this->db->order_by('jobs_notify.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function jobnotification_lists($id){
      $this->db->select('jobs_notify.id, user.name, user.email');
      $this->db->from('jobs_notify');
      $this->db->where("jobs_notify.notify_id",$id);
      $this->db->join('user','jobs_notify.user_id=user.id');
      $this->db->group_by('jobs_notify.user_id');
      $this->db->order_by('jobs_notify.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

    public function promonotification_lists(){
      $this->db->select('promo_notifications.title,promo_notifications.id, promo_notifications.message,promo_notifications.user_id, promo_notifications.added_at, user.name, user.superpower,user.notification_status, user.device_type, user.device_token');
      $this->db->from('promo_notifications');
      $this->db->join('user','promo_notifications.user_id=user.id');
      $this->db->where('promo_notifications.cron_status',1);
      $this->db->order_by('user.id','asc');
      $query = $this->db->get();
      return $query->result_array();
   }  

   public function promo_count(){
    $this->db->select('count(id) as promo_count')
             ->from('promo_notifications')
             ->where('cron_status',1);
    $query = $this->db->get();
    return $query->result_array();          
   }

   public function fetchPromoLimit() {
        $this->db->select('title,id,message,user_id,added_at');
        $this->db->where('cron_status', 1);
        $this->db->where_in('user_id', [334671, 334561, 334607, 334563, 332252, 331714, 334241, 294002, 329462]);
        $this->db->order_by('id','DESC');
        $query = $this->db->get('promo_notifications');
        return $query->result_array();
   }

   // public function fetchPromoLimit() {
   //      $this->db->select('title,id,message,user_id,added_at');
   //      $this->db->where('cron_status', 1);
   //      $this->db->where_in('user_id', [61578, 174023, 177117, 87078, 170332, 177112, 176070, 28273, 29497, 73944, 55088, 173832, 177083]);
   //      $this->db->order_by('id','DESC');
   //      $query = $this->db->get('promo_notifications');
   //      return $query->result_array();
   // }

   public function fetchPromo() {
        $this->db->select('title,id,message,user_id,added_at');
        $this->db->where('cron_status', 1);
        $this->db->order_by('id','ASC');
        $this->db->limit(900);
        $query = $this->db->get('promo_notifications');
        return $query->result_array();
   }

   public function fetchUser($id) {
      $this->db->where('user_id',$id);
      $this->db->order_by('id', 'DESC');
      $this->db->limit(2);
      $query = $this->db->get('user_token');
      return $query->result_array();
   }

   public function fetchUserDeviceToken($id) {
      $this->db->where('user_id',$id);
      $this->db->order_by('id', 'DESC');
      $this->db->limit(3);
      $query = $this->db->get('user_token');
      return $query->result_array();
   }

   public function fetchUserAgain($id) {
      $this->db->where('id',$id);
      $query = $this->db->get('user');
      return $query->result_array();
   }

   public function user_single($id) {
      $this->db->where('id', $id);
      $query = $this->db->get('user');
      return $query->result_array();
   }


   public function batchUpdatePromo($id){
      return  $this->db->where("id", $id)
            ->update('promo_notifications',["cron_status"=>2]);
      // return $this->db->update('promo_notifications', 'cron_status=2', $user_array);     
   }

   public function batchUpdate($user){
      return  $this->db->where_in("id", $user)
            ->update('promo_notifications',["cron_status"=>2]);
      // return $this->db->update('promo_notifications', 'cron_status=2', $user_array);     
   }

   public function jobpushUpdate($user) {
      //var_dump($user[0]);die;
      return  $this->db->where_in("id", $user)->update('jobs_notify', ["status"=>2]);
   }

   public function reconciliation_lists() {
      $this->db->select('reconciliation.status,reconciliation.added_at,reconciliation.showupreason,reconciliation.schedule,recruiter.cname, user.email,user.name,user.phone,job_posting.jobtitle');
      $this->db->from('reconciliation');
      $this->db->join('user','reconciliation.user_id=user.id');
      $this->db->join('job_posting','job_posting.id=reconciliation.job_id');
      $this->db->join('recruiter','job_posting.recruiter_id=recruiter.id');
      $this->db->order_by('reconciliation.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function contact_lists() {
      //$this->db->where('parent_id', 0);
      $this->db->select('*');
      $this->db->from('user_contact');
      $this->db->order_by('id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function fetchversion(){
      $this->db->select('*');
      $query = $this->db->get('api_version');
      return $query->result_array();
   }

   public function contact_lists1() {
      $this->db->where('status', 0);
      $this->db->select('*');
      $this->db->from('user_contact');
      $this->db->order_by('id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function advertise_lists() {
      $this->db->where('status', 2);
      $this->db->select('*');
      $this->db->from('advertise_list');
      $this->db->order_by('id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function contactbyuserid($id) {
      $this->db->where('id', $id);
      $this->db->select('*');
      $this->db->from('user_contact');
      $query = $this->db->get();
      return $query->result_array();
   }
   public function quote_lists() {
      //$this->db->where('parent_id', 0);
      $query = $this->db->get('motivational_quotes');
      return $query->result_array();
   }

   public function singlequote($id) {
      $this->db->where('id', $id);
      $query = $this->db->get('motivational_quotes');
      return $query->result_array();
   }
   
   public function quotebyScreen($id) {
      $this->db->where('title', $id);
      $query = $this->db->get('motivational_quotes');
      return $query->result_array();
   }

   
   
   public function delete_user($id) {
      $this->db->where('id', $id);
      $this->db->delete('user');
      return true;
   }

   public function delete_userquery($id) {
      $this->db->where('id', $id);
      $this->db->delete('user_contact');
      return true;
   }

   public function delete_quote($id) {
      $this->db->where('id', $id);
      $this->db->delete('motivational_quotes');
      return true;
   }
   
   public function delete_userAssessment($id) {
      $this->db->where('user_id', $id);
      $this->db->delete('user_assessment');
      return true;
   }
   
   public function delete_userEducation($id) {
      $this->db->where('user_id', $id);
      $this->db->delete('user_education');
      return true;
   }
   
   public function delete_userExpert($id) {
      $this->db->where('user_id', $id);
      $this->db->delete('user_expert');
      return true;
   }
   
   public function delete_userForgot($id) {
      $this->db->where('user_id', $id);
      $this->db->delete('user_forgot');
      return true;
   }
   
   public function delete_userLanguage($id) {
      $this->db->where('user_id', $id);
      $this->db->delete('user_language');
      return true;
   }
   
   public function delete_userMoreDetails($id) {
      $this->db->where('user_id', $id);
      $this->db->delete('user_more_details');
      return true;
   }
   
   public function delete_userResume($id) {
      $this->db->where('user_id', $id);
      $this->db->delete('user_resume');
      return true;
   }
   
   public function delete_userSkills($id) {
      $this->db->where('user_id', $id);
      $this->db->delete('user_skills');
      return true;
   }
   
   public function delete_userTopclients($id) {
      $this->db->where('user_id', $id);
      $this->db->delete('user_topclients');
      return true;
   }
   
   public function delete_userWorkExperience($id) {
      $this->db->where('user_id', $id);
      $this->db->delete('user_work_experience');
      return true;
   }
   
   public function delete_verificationEmail($id) {
      $this->db->where('user_id', $id);
      $this->db->delete('verification_email');
      return true;
   }

   public function delete_token($id) {
      $this->db->where('user_id', $id);
      $this->db->delete('user_token');
      return true;
   }
   
   public function jobseekerStatus($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('user', $data);
      return true;
   }

   public function updatepromo($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('promo_notifications', $data);
      return true;
   }

   public function jobseekerQuery($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('user_contact', $data);
      return true;
   }

   public function updateVersion($data) {
      $this->db->update('api_version', $data);
      return true;
   }
   
   public function jobseeker_detail_fetch($id) {
        $this->db->select('*');
        $this->db->from('user a');
        $this->db->join('user_more_details g', 'g.user_id=a.id', 'left');
        $this->db->where('a.id',$id);
        $query = $this->db->get(); 
        if($query->num_rows() != 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        } 
   }
   
   public function jobseeker_exp_fetch($id) {
      $this->db->where('user_id', $id);
      $query = $this->db->get('user_work_experience');
      return $query->result_array();
   }
   public function jobseeker_edu_fetch($id) {
      $this->db->where('user_id', $id);
      $query = $this->db->get('user_education');
      return $query->result_array();
   }
   public function jobseeker_assisment_fetch($id) {
      $this->db->where('user_id', $id);
      $query = $this->db->get('user_assessment');
      return $query->result_array();
   }
   public function jobseeker_expert_fetch($id) {
      $this->db->where('user_id', $id);
      $query = $this->db->get('user_expert');
      return $query->result_array();
   }
    public function jobseeker_skills_fetch($id) {
      $this->db->where('user_id', $id);
      $query = $this->db->get('user_skills');
      return $query->result_array();
   }
   
   public function hiredCandidates() {
      $this->db->where('status', 4);
      $query = $this->db->get('user');
      return $query->result_array();
   }

   public function getboostamount() { 
      $query = $this->db->get('boost_amount');
      return $query->result_array();
   }

   public function getactivelyamount() { 
      $query = $this->db->get('actively_amount');
      return $query->result_array();
   }

   public function quote_insert($data) {
      $this->db->where('title', $data['title']);
      $query = $this->db->get('motivational_quotes');
      $numrow = $query->num_rows();
      if($numrow >= 1) {
        return false;
      } else{
        if($this->db->insert("motivational_quotes", $data)) { 
           $insert_id = $this->db->insert_id();
           return $insert_id;
        } else{
           return false;
        }
      }
   }  

   public function promo_notification_insert($data) {
      if($this->db->insert("promo_notifications", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function bonusinert($data) {
      if($this->db->insert("sign_in_bonus", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function boostinert($data) {
      if($this->db->insert("boost_amount", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function activelyinert($data) {
      if($this->db->insert("actively_amount", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function boostupdate($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('boost_amount', $data);
      return true;
   }

   public function activelyupdate($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('actively_amount', $data);
      return true;
   }

   public function quote_update($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('motivational_quotes', $data);
      return true;
   }


      var $order_column = array(null, "name", "email", "phone", 'created_at',null,"education","location","nationality","superpower","device_type",'app_version',null,"type",null,null,null,null,null);
      
  function make_query() {  
           // $this->db->select('a.*,COUNT(g.id) as countid');
           // $this->db->from('user a');
           // $this->db->join('sign_in_bonus g', 'g.user_id=a.id', 'left');
           // $this->db->where('a.phone!=','0');
              
           // if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))  
           // {  
           //      $this->db->where('(a.name like "%'.$_POST["search"]["value"].'%" OR a.email like "%'.$_POST["search"]["value"].'%" OR a.phone like "%'.$_POST["search"]["value"].'%" OR a.education like "%'.$_POST["search"]["value"].'%" OR a.location like "%'.$_POST["search"]["value"].'%" OR a.nationality like "%'.$_POST["search"]["value"].'%" OR a.superpower like "%'.$_POST["search"]["value"].'%" OR a.device_type like "%'.$_POST["search"]["value"].'%" OR a.app_version like "%'.$_POST["search"]["value"].'%" OR a.type like "%'.$_POST["search"]["value"].'%" OR a.created_at like "%'.$_POST["search"]["value"].'%")');  
                 
           // }  
           
           // $this->db->order_by('a.id','desc');  
             
           // $this->db->group_by('a.id');


          $this->db->select('a.*,COUNT(g.id) as countid');
          $this->db->from('user a');
          $this->db->join('sign_in_bonus g', 'g.user_id=a.id', 'left');

          $column_search = array(null, "name", "email", "phone", 'created_at',null,"education","location","nationality","superpower","device_type",'app_version',null,"type",null,null,null,null,null);
          
          if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"])) {

              $column_search = array('superpower','location',"name", "email", "phone", 'created_at',"education","location","nationality","superpower","device_type",'app_version',"type");

                $this->db->where('(a.name like "%'.$_POST["search"]["value"].'%" OR a.email like "%'.$_POST["search"]["value"].'%" OR a.phone like "%'.$_POST["search"]["value"].'%" OR a.education like "%'.$_POST["search"]["value"].'%" OR a.location like "%'.$_POST["search"]["value"].'%" OR a.nationality like "%'.$_POST["search"]["value"].'%" OR a.superpower like "%'.$_POST["search"]["value"].'%" OR a.device_type like "%'.$_POST["search"]["value"].'%" OR a.app_version like "%'.$_POST["search"]["value"].'%" OR a.type like "%'.$_POST["search"]["value"].'%" OR a.created_at like "%'.$_POST["search"]["value"].'%")');  
                 
          } else {

            if($this->input->post('superpower') || $this->input->post('location')) {
                $column_search = array('superpower','location');

                if($this->input->post('superpower')) {
                  $this->db->like('superpower', $this->input->post('superpower'));
                }
                if($this->input->post('location')) {
                  $this->db->like('location', $this->input->post('location'));
                }
            } else {

                $column_search = array(null, "name", "email", "phone", 'created_at',null,"education","location","nationality","superpower","device_type",'app_version',null,"type",null,null,null,null,null);

            }
          }

          $this->db->where('a.phone!=','0');
          $this->db->order_by('a.id','desc');
          $this->db->group_by('a.id');

          $i = 0;
     
        foreach ($column_search as $item) { // loop column 
            if($_POST['search']['value']) // if datatable send POST for search
            {
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
      }


      function make_datatables() {  
           $this->make_query();  
           if($_POST["length"] != -1)  
           {  
                $this->db->limit($_POST['length'], $_POST['start']);  
           }  
           $query = $this->db->get();  
           return $query->result();  
      }  

      function get_filtered_data(){  
           $this->make_query();
           $query = $this->db->get(); 
           //echo $this->db->last_query(); 
           return $query->num_rows();  
      }       
      function get_all_data()  
      {  
            $this->db->select('a.*,COUNT(g.id) as countid');
           $this->db->from('user a');
           $this->db->join('sign_in_bonus g', 'g.user_id=a.id', 'left');
           $this->db->where('a.phone!=','0');
           $this->db->group_by('a.id');
           return $this->db->count_all_results();  
      }  

      public function get_user_experience($id) {

         $this->db->select('company');
         $this->db->from('user_work_experience');
         $this->db->where('user_id', $id);
         $this->db->order_by('id','DESC');
         $this->db->limit(1);
         $query = $this->db->get(); 
         return $query->result_array();
      }

      public function get_all_jobseekerrecord($rowno, $rowperpage, $search="") {

          $this->db->select('a.*,COUNT(g.id) as countid');
          $this->db->from('user a');
          $this->db->where('a.phone !=',0);
          $this->db->join('sign_in_bonus g', 'g.user_id=a.id', 'left');

          if($search != '') {

            $this->db->where("(a.email LIKE '%".$search."%' OR a.phone LIKE '%".$search."%' OR a.type LIKE '%".$search."%' OR a.location LIKE '%".$search."%' OR a.designation LIKE '%".$search."%' OR a.current_salary LIKE '%".$search."%' OR a.exp_month LIKE '%".$search."%' OR a.exp_year LIKE '%".$search."%' OR a.education LIKE '%".$search."%' OR a.superpower LIKE '%".$search."%' OR a.nationality LIKE '%".$search."%' OR a.created_at LIKE '%".$search."%' OR a.device_type LIKE '%".$search."%' OR a.app_version LIKE '%".$search."%' OR a.platform LIKE '%".$search."%' OR a.state LIKE '%".$search."%' OR a.city LIKE '%".$search."%' OR a.jobsInterested LIKE '%".$search."%' OR a.topbpo LIKE '%".$search."%' OR a.industry LIKE '%".$search."%' OR a.specialization LIKE '%".$search."%' OR a.sub_specialization LIKE '%".$search."%' OR a.jobLevel LIKE '%".$search."%' OR a.jobsbenefits LIKE '%".$search."%')", NULL, FALSE);

            //var_dump("bbbb--".$search);die;
            // $this->db->like('a.name', $search);
            // $this->db->or_like('a.email', $search);
            // $this->db->or_like('a.phone', $search);
            // $this->db->or_like('a.type', $search);
            // $this->db->or_like('a.location', $search);
            // $this->db->or_like('a.designation', $search);
            // $this->db->or_like('a.current_salary', $search);
            // $this->db->or_like('a.exp_month', $search);
            // $this->db->or_like('a.exp_year', $search);
            // $this->db->or_like('a.education', $search);
            // $this->db->or_like('a.superpower', $search);
            // $this->db->or_like('a.nationality', $search);
            // $this->db->or_like('a.created_at', $search);
            // $this->db->or_like('a.device_type', $search);
            // $this->db->or_like('a.app_version', $search);
            // $this->db->or_like('a.platform', $search);
            // $this->db->or_like('a.state', $search);
            // $this->db->or_like('a.city', $search);
            // $this->db->or_like('a.jobsInterested', $search);
            // $this->db->or_like('a.topbpo', $search);
            // $this->db->or_like('a.industry', $search);
            // $this->db->or_like('a.specialization', $search);
            // $this->db->or_like('a.sub_specialization', $search);
            // $this->db->or_like('a.jobLevel', $search);
            // $this->db->or_like('a.jobsbenefits', $search);
          }

          $this->db->limit($rowperpage, $rowno); 
          $this->db->group_by('a.id');
          $this->db->order_by('a.id','DESC');

          $query = $this->db->get(); 
          return $query->result_array();
      }  

      public function get_count_jobseekerrecord($search="") {
        
          $this->db->select('count(*) as allcount');
          $this->db->from('user');
          $this->db->where('phone!=',0);
            
          if($search != '') {
            $this->db->like('name', $search);
            $this->db->or_like('email', $search);
            $this->db->or_like('phone', $search);
            $this->db->or_like('type', $search);
            $this->db->or_like('location', $search);
            $this->db->or_like('designation', $search);
            $this->db->or_like('current_salary', $search);
            $this->db->or_like('exp_month', $search);
            $this->db->or_like('exp_year', $search);
            $this->db->or_like('education', $search);
            $this->db->or_like('superpower', $search);
            $this->db->or_like('nationality', $search);
            $this->db->or_like('created_at', $search);
            $this->db->or_like('device_type', $search);
            $this->db->or_like('app_version', $search);
            $this->db->or_like('platform', $search);
            $this->db->or_like('state', $search);
            $this->db->or_like('city', $search);
            $this->db->or_like('jobsInterested', $search);
            $this->db->or_like('topbpo', $search);
            $this->db->or_like('industry', $search);
            $this->db->or_like('specialization', $search);
            $this->db->or_like('sub_specialization', $search);
            $this->db->or_like('jobLevel', $search);
            $this->db->or_like('jobsbenefits', $search);
          }

          //$this->db->group_by('id');
          $query = $this->db->get(); 
          $result = $query->result_array();
          //var_dump($result);
          return $result[0]['allcount'];
      }  

      public function get_search_jobseekerrecord($search="") {

          $this->db->select('a.*,COUNT(g.id) as countid');
          $this->db->from('user a');
          $this->db->join('sign_in_bonus g', 'g.user_id=a.id', 'left');
          $this->db->where('a.phone!=','0');

          if($search != '') {
            //var_dump("bbbb--".$search);die;
            $this->db->like('a.name', $search);
            $this->db->or_like('a.email', $search);
            $this->db->or_like('a.phone', $search);
            $this->db->or_like('a.type', $search);
            $this->db->or_like('a.location', $search);
            $this->db->or_like('a.designation', $search);
            $this->db->or_like('a.current_salary', $search);
            $this->db->or_like('a.exp_month', $search);
            $this->db->or_like('a.exp_year', $search);
            $this->db->or_like('a.education', $search);
            $this->db->or_like('a.superpower', $search);
            $this->db->or_like('a.nationality', $search);
            $this->db->or_like('a.created_at', $search);
            $this->db->or_like('a.device_type', $search);
            $this->db->or_like('a.app_version', $search);
            $this->db->or_like('a.platform', $search);
            $this->db->or_like('a.state', $search);
            $this->db->or_like('a.city', $search);
            $this->db->or_like('a.jobsInterested', $search);
            $this->db->or_like('a.topbpo', $search);
            $this->db->or_like('a.industry', $search);
            $this->db->or_like('a.specialization', $search);
            $this->db->or_like('a.sub_specialization', $search);
            $this->db->or_like('a.jobLevel', $search);
            $this->db->or_like('a.jobsbenefits', $search);
          }

          //$this->db->limit($rowperpage, $rowno); 
          $this->db->group_by('a.id');
          $this->db->order_by('a.id','DESC');

          $query = $this->db->get(); 
          return $query->result_array();
      }



      public function get_count_metricrecord($search) {
          
          $this->db->select('count(*) as allcount');
          $this->db->from('user');
          $this->db->where('phone!=','0');
          $this->db->like('created_at', $search);
          $query = $this->db->get(); 
          $result = $query->result_array();
          return $result[0]['allcount'];
      }  

      public function get_count_metricrecordBPO($search) {
          
          $this->db->select('count(*) as allcount');
          $this->db->from('user');
          $this->db->where('phone!=','0');
          $this->db->where('industry','BPO');
          $this->db->like('created_at', $search);
          $query = $this->db->get(); 
          $result = $query->result_array();
          return $result[0]['allcount'];
      }  

      public function get_count_metricrecordNonBPO($search) {
          
          $this->db->select('count(*) as allcount');
          $this->db->from('user');
          $this->db->where('phone!=','0');
          $this->db->where('industry','Non BPO');
          $this->db->like('created_at', $search);
          $query = $this->db->get(); 
          $result = $query->result_array();
          return $result[0]['allcount'];
      }  

      public function get_count_metricrecordApplication($search) {
          
          $this->db->select('count(*) as allcount');
          $this->db->from('applied_jobs a');
          
          $this->db->join('job_posting p', 'p.id = a.jobpost_id','left');
          $this->db->join('recruiter r', 'r.id = p.recruiter_id','left');
          $this->db->join('user u', 'u.id = a.user_id');

          $this->db->like('a.created_at', $search);
          $query = $this->db->get(); 
          $result = $query->result_array();
          //var_dump($result[0]['allcount']);die;
          return $result[0]['allcount'];
      }  

      public function get_count_metricrecordInstant($search) {
          
          $this->db->select('count(*) as allcount');
          $this->db->from('job_screening a');
          $this->db->join('job_posting p', 'a.jobpost_id = p.id','left');
          $this->db->join('recruiter r', 'p.recruiter_id = r.id','left');
          $this->db->join('user u', 'u.id = a.user_id','left');

          //$this->db->where('p.mode', "Instant screening");

          $this->db->like('a.created_at', $search);
          $query = $this->db->get(); 
          $result = $query->result_array();
          return $result[0]['allcount'];
      }

      public function get_count_metricrecordCall($search) {
          
          $this->db->select('count(*) as allcount');
          $this->db->from('applied_jobs a');
          $this->db->join('job_posting p', 'a.jobpost_id = p.id');
          $this->db->join('recruiter r', 'p.recruiter_id = r.id','left');
          $this->db->join('user u', 'u.id = a.user_id');

          //$this->db->where('p.mode', "Call");
          //$this->db->or_where('p.mode', "Instant screening"); 

          $this->db->like('a.created_at', $search);
          $query = $this->db->get();
          $result = $query->result_array();
          return $result[0]['allcount'];
      }

      public function get_count_metricrecordVisit($search) {
          
          $this->db->select('count(*) as allcount');
          $this->db->from('applied_jobs a');
          $this->db->join('job_posting p', 'a.jobpost_id = p.id','left');
          $this->db->join('recruiter r', 'p.recruiter_id = r.id','left');
          $this->db->join('user u', 'u.id = a.user_id');

          $this->db->where('p.mode', "Walk-in");
          //$this->db->or_where('p.mode', "Instant screening"); 

          $this->db->like('a.created_at', $search);
          $query = $this->db->get(); 
          $result = $query->result_array();
          return $result[0]['allcount'];
      }
      
      public function get_main_recruiter() {

          $this->db->select('id, cname');
          $this->db->from('recruiter');
          $this->db->where('parent_id','0');
          $this->db->where('status',1);
          $this->db->where('active',1);
          $this->db->where('logincount>',0);

          if($showType == "daily")  {
              
              $this->db->like('created_at', $curr_date);

          } else if($showType == "weekly")  {

              $this->db->where('created_at BETWEEN CURDATE() - INTERVAL 7 DAY AND CURDATE()');

          } else if($showType == "monthly")  {

              $this->db->where('created_at BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()');

          } else if($showType == "yearly")  {
              $this->db->where('created_at BETWEEN CURDATE() - INTERVAL 365 DAY AND CURDATE()');
          }

          $query = $this->db->get(); 
          return $result = $query->result_array();
      }

      public function get_comapnywisemain_recruiter() {

          $this->db->select('id, cname');
          $this->db->from('recruiter');
          $this->db->where('parent_id','0');
          $this->db->where('status',1);
          $this->db->where('active',1);
          $this->db->where('logincount>',0);

          $query = $this->db->get(); 
          return $result = $query->result_array();
      }

      public function get_companywisecount_havejobs($search, $showType) {
          
          $curr_date = date('Y-m-d');

          $this->db->select('count(*) as allcount');
          $this->db->from('job_posting');
          $this->db->where('recruiter_id', $search);
          $this->db->where('status', 1);
          $this->db->where('jobexpire >=', $curr_date);
          $query = $this->db->get(); 
          $result = $query->result_array();
          return $result[0]['allcount'];
      }

      public function get_count_havejobs($search, $showType) {
          
          $curr_date = date('Y-m-d');

          $this->db->select('count(*) as allcount');
          $this->db->from('job_posting');
          $this->db->where('recruiter_id', $search);
          $this->db->where('status', 1);
          //$this->db->where('jobexpire >=', $curr_date);
          $query = $this->db->get(); 
          $result = $query->result_array();
          return $result[0]['allcount'];
      }

      public function get_monthwisecount_havejobs($search, $month) {
          //$curr_date = date('Y-m-d');

          $this->db->select('count(*) as allcount');
          $this->db->from('job_posting');
          $this->db->where('recruiter_id', $search);
          $this->db->where('status', 1);
          $this->db->like('created_at', $month);
          $query = $this->db->get(); 
          $result = $query->result_array();
          return $result[0]['allcount'];
      }

      public function get_count_activejobs($search, $showType) {
          
          $cDate = date('Y-m-d');
          $curr_date = date('Y-m-d');

          $this->db->select('count(*) as allcount');
          $this->db->from('job_posting');
          $this->db->where('recruiter_id', $search);
          $this->db->where('status', 1);
          //$this->db->where("jobexpire >=", $cDate);

          if($showType == "daily")  {
              
              $this->db->where('jobexpire >=', $curr_date);

          } else if($showType == "weekly")  {

               $week_date = date('Y-m-d', strtotime("-7 days"));

               $this->db->where('jobexpire <', $curr_date);
               $this->db->where('jobexpire >=', $week_date);

              //$this->db->where('jobexpire BETWEEN CURDATE() - INTERVAL 7 DAY AND CURDATE()');

          } else if($showType == "monthly")  {

               $week_date = date('Y-m-d', strtotime("-30 days"));

               $this->db->where('jobexpire <', $curr_date);
               $this->db->where('jobexpire >=', $week_date);

          } else if($showType == "yearly")  {
               
               $week_date = date('Y-m-d', strtotime("-365 days"));

               $this->db->where('jobexpire <', $curr_date);
               $this->db->where('jobexpire >=', $week_date);
          }

          $query = $this->db->get(); 
          $result = $query->result_array();
          return $result[0]['allcount'];
      }  

      public function get_monthwisecount_activejobs($search, $month) {
          
          $cDate = date('Y-m-d');
          $curr_date = date('Y-m-d');

          $this->db->select('count(*) as allcount');
          $this->db->from('job_posting');
          $this->db->where('recruiter_id', $search);
          $this->db->where('status', 1);
          $this->db->like("jobexpire", $month);

          $query = $this->db->get(); 
          $result = $query->result_array();
          return $result[0]['allcount'];
      }  

      public function get_count_upcomingexpirejobs($search, $showType) {
          
          $cDate = date('Y-m-d');
          $curr_date = date('Y-m-d');
          $upcomingDate = date('Y-m-d', strtotime("+7 days"));

          $this->db->select('count(*) as allcount');
          $this->db->from('job_posting');
          $this->db->where('recruiter_id', $search);
          $this->db->where('status', 1);

          if($showType == "daily")  {
            
              $this->db->where('jobexpire <=', $upcomingDate);
              $this->db->where('jobexpire >=', $curr_date);

          } else if($showType == "weekly")  {

               $week_date = date('Y-m-d', strtotime("-7 days"));

               $this->db->where('jobexpire <=', $upcomingDate);
               $this->db->where('jobexpire >=', $week_date);

              //$this->db->where('jobexpire BETWEEN CURDATE() - INTERVAL 7 DAY AND CURDATE()');

          } else if($showType == "monthly")  {

               $week_date = date('Y-m-d', strtotime("-30 days"));

               $this->db->where('jobexpire <=', $upcomingDate);
               $this->db->where('jobexpire >=', $week_date);

          } else if($showType == "yearly")  {
               
               $week_date = date('Y-m-d', strtotime("-365 days"));

               $this->db->where('jobexpire <=', $upcomingDate);
               $this->db->where('jobexpire >=', $week_date);
          }

          $query = $this->db->get(); 
          $result = $query->result_array();
          return $result[0]['allcount'];
      }

      public function get_monthwisecount_upcomingexpirejobs($search, $month) {
          
          $cDate = date('Y-m-d');
          //$curr_date = date('Y-m-d');
          //$upcomingDate = date('Y-m-d', strtotime("+7 days"));

          $this->db->select('count(*) as allcount');
          $this->db->from('job_posting');
          $this->db->where('recruiter_id', $search);
          $this->db->where('status', 1);
          $this->db->like("jobexpire", $month);

          $query = $this->db->get(); 
          $result = $query->result_array();
          return $result[0]['allcount'];
      }


      public function get_all_commonjobs($search) {
          
          $cDate = date('Y-m-d');

          $this->db->select('id, mode');
          $this->db->from('job_posting');
          $this->db->where('recruiter_id', $search);
          $this->db->where('status', 1);
          $query = $this->db->get(); 
          return $result = $query->result_array();
      }


      public function get_all_instantjobs($search) {
          
          $cDate = date('Y-m-d');

          $this->db->select('id');
          $this->db->from('job_posting');
          $this->db->where('recruiter_id', $search);
          $this->db->where('status', 1);
          //$this->db->where("jobexpire >=", $cDate);
          $this->db->where("mode", "Instant screening");

          $query = $this->db->get(); 
          return $result = $query->result_array();
      }

      public function get_count_instantapplyjobs($search, $showType) {
          
          $curr_date = date('Y-m-d');

          $this->db->select('count(*) as allcount');
          $this->db->from('job_screening');
          $this->db->join('user', 'user.id = job_screening.user_id');
          $this->db->where('job_screening.jobpost_id', $search);

          if($showType == "daily")  {
              
              $this->db->like('job_screening.created_at', $curr_date);

          } else if($showType == "weekly")  {

              $this->db->where('job_screening.created_at >= ( CURDATE() - INTERVAL 7 DAY )');

          } else if($showType == "monthly")  {

              $this->db->where('job_screening.created_at >= ( CURDATE() - INTERVAL 30 DAY )');

          } else if($showType == "yearly")  {
              $this->db->where('job_screening.created_at >= ( CURDATE() - INTERVAL 365 DAY )');
          }

          $query = $this->db->get(); 
          $result = $query->result_array();
          return $result[0]['allcount'];
      }

      public function get_monthwisecount_instantapplyjobs($search, $month) {
          
          $curr_date = date('Y-m-d');

          $this->db->select('count(*) as allcount');
          $this->db->from('job_screening');
          $this->db->join('user', 'user.id = job_screening.user_id');
          $this->db->where('job_screening.jobpost_id', $search);
          $this->db->like('job_screening.created_at', $month);
          $query = $this->db->get(); 
          $result = $query->result_array();
          return $result[0]['allcount'];
      }

      public function get_all_Phonejobs($search) {
          
          $cDate = date('Y-m-d');

          $this->db->select('id');
          $this->db->from('job_posting');
          $this->db->where('recruiter_id', $search);
          $this->db->where('status', 1);
          //$this->db->where("jobexpire >=", $cDate);
          $this->db->where("mode", "Call");

          $query = $this->db->get(); 
          return $result = $query->result_array();
      }
      public function get_count_phoneapplyjobs($search, $showType) {
          
          $curr_date = date('Y-m-d');

          $this->db->select('count(*) as allcount');
          $this->db->from('applied_jobs');
          $this->db->join('user', 'user.id = applied_jobs.user_id');
          $this->db->where('applied_jobs.jobpost_id', $search);

          if($showType == "daily")  {
              
              $this->db->like('applied_jobs.created_at', $curr_date);

          } else if($showType == "weekly")  {

              $this->db->where('applied_jobs.created_at  >= ( CURDATE() - INTERVAL 7 DAY )');

          } else if($showType == "monthly")  {

              $this->db->where('applied_jobs.created_at  >= ( CURDATE() - INTERVAL 30 DAY )');

          } else if($showType == "yearly")  {
              $this->db->where('applied_jobs.created_at >= ( CURDATE() - INTERVAL 365 DAY )');
          }

          $query = $this->db->get(); 
          $result = $query->result_array();
          return $result[0]['allcount'];
      }

      public function get_monthwisecount_phoneapplyjobs($search, $month) {
          
          $curr_date = date('Y-m-d');

          $this->db->select('count(*) as allcount');
          $this->db->from('applied_jobs');
          $this->db->join('user', 'user.id = applied_jobs.user_id');
          $this->db->where('applied_jobs.jobpost_id', $search);
          $this->db->like('applied_jobs.created_at', $month);
          $query = $this->db->get(); 
          $result = $query->result_array();
          return $result[0]['allcount'];
      }

      public function get_all_Walkjobs($search) {
          
          $cDate = date('Y-m-d');

          $this->db->select('id');
          $this->db->from('job_posting');
          $this->db->where('recruiter_id', $search);
          $this->db->where('status', 1);
          //$this->db->where("jobexpire >=", $cDate);
          $this->db->where("mode", "Walk-in");

          $query = $this->db->get(); 
          return $result = $query->result_array();
      }
      public function get_count_walkapplyjobs($search, $showType) {
          
          $curr_date = date('Y-m-d');

          $this->db->select('count(*) as allcount');
          $this->db->from('applied_jobs');
          $this->db->join('user', 'user.id = applied_jobs.user_id');
          $this->db->where('applied_jobs.jobpost_id', $search);

          if($showType == "daily")  {
              
              $this->db->like('applied_jobs.created_at', $curr_date);

          } else if($showType == "weekly")  {

              $this->db->where('applied_jobs.created_at  >= ( CURDATE() - INTERVAL 7 DAY )');

          } else if($showType == "monthly")  {

              $this->db->where('applied_jobs.created_at  >= ( CURDATE() - INTERVAL 30 DAY )');

          } else if($showType == "yearly")  {
              $this->db->where('applied_jobs.created_at >= ( CURDATE() - INTERVAL 365 DAY )');
          }

          $query = $this->db->get(); 
          $result = $query->result_array();
          return $result[0]['allcount'];
      }

      public function get_monthwisecount_walkapplyjobs($search, $month) {
          
          $curr_date = date('Y-m-d');

          $this->db->select('count(*) as allcount');
          $this->db->from('applied_jobs');
          $this->db->join('user', 'user.id = applied_jobs.user_id');
          $this->db->where('applied_jobs.jobpost_id', $search);
          $this->db->like('applied_jobs.created_at', $month);

          $query = $this->db->get(); 
          $result = $query->result_array();
          return $result[0]['allcount'];
      }

      public function get_all_notifyjobs($search) {
          
          $cDate = date('Y-m-d');

          $this->db->select('id');
          $this->db->from('job_posting');
          $this->db->where('recruiter_id', $search);
          $this->db->where('status', 1);
          $this->db->where("jobexpire >=", $cDate);
          //$this->db->where("mode", "Walk-in");

          $query = $this->db->get(); 
          return $result = $query->result_array();
      }
      public function get_count_notifysendjobs($search, $showType) {
          
          $curr_date = date('Y-m-d');

          $this->db->select('count(*) as allcount');
          $this->db->from('jobs_notify_records');
          $this->db->where('job_id', $search);

          if($showType == "daily")  {
              
              $this->db->like('created_at', $curr_date);

          } else if($showType == "weekly")  {

              $this->db->where('created_at BETWEEN CURDATE() - INTERVAL 7 DAY AND CURDATE()');

          } else if($showType == "monthly")  {

              $this->db->where('created_at BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()');

          } else if($showType == "yearly")  {
              $this->db->where('created_at BETWEEN CURDATE() - INTERVAL 365 DAY AND CURDATE()');
          }

          $query = $this->db->get(); 
          $result = $query->result_array();
          return $result[0]['allcount'];
      }


      public function get_monthwisecount_notifysendjobs($search, $month) {
          
          $curr_date = date('Y-m-d');

          $this->db->select('count(*) as allcount');
          $this->db->from('jobs_notify_records');
          $this->db->where('job_id', $search);
          $this->db->like('created_at', $month);

          $query = $this->db->get(); 
          $result = $query->result_array();
          return $result[0]['allcount'];
      }






       public function fetchJobIntrestedInNotify() {
            $this->db->where('status', 1);
            $this->db->where('send_type', 2);
            $this->db->order_by('id','ASC');
            $query = $this->db->get('jobs_notify_records');
            return $query->result_array();
       }

      public function fetchMainJobNotify() {
            $this->db->where('send_type', 2);
            //$this->db->where('job_id', 1529);
            $this->db->where('status', 1);
            $this->db->order_by('id','ASC');
            $this->db->limit(1);
            $query = $this->db->get('jobs_notify_records');
            return $query->result_array();
      }
      public function fetchMainJobNotifyy() {
            $this->db->where('send_type', 2);
            //$this->db->where('job_id', 1529);
            $this->db->where('status', 1);
            $this->db->order_by('id','DESC');
            $this->db->limit(1);
            $query = $this->db->get('jobs_notify_records');
            return $query->result_array();
      }

      public function fetchMainJobUpdationNotify() {
            $this->db->where('send_type', 1);
            $this->db->where('status', 1);
            $this->db->order_by('id','ASC');
            $this->db->limit(1);
            $query = $this->db->get('jobs_notify_records');
            return $query->result_array();
      }

      public function checkMainJobNotify($id) {
            $this->db->where('notify_id', $id);
            $this->db->where('status', 1);
            $query = $this->db->get('jobs_notify');
            return $query->result_array();
       }

     public function updateMainJobNotify($id) {
        return  $this->db->where("id", $id)
              ->update('jobs_notify_records',["status"=>2]);
     }

      public function fetchJobNotify($id) {
            $this->db->where('status', 1);
            $this->db->where('notify_id', $id);
            $this->db->order_by('id','DESC');
            $this->db->limit(980);
            $query = $this->db->get('jobs_notify');
            return $query->result_array();
       }
       public function fetchJobNotify_one($id) {
            $this->db->where('status', 1);
            $this->db->where('notify_id', $id);
            $this->db->order_by('id','ASC');
            $this->db->limit(980);
            $query = $this->db->get('jobs_notify');
            return $query->result_array();
       }

       //[55088, 216956, 216965, 187355, 217024, 215703, 216958, 217013, 216799, 28273, 29497]

      public function fetchJobNotifyLimited() {
            $this->db->where('status', 1);
            $this->db->where_in('user_id', [334671, 334561, 334607, 334563, 332252, 331714, 334241, 329462, 339061, 294002]);
            $this->db->order_by('id','DESC');
            $query = $this->db->get('jobs_notify');
            return $query->result_array();
       }

       public function fetchJobNotifyLimitedMainData($id) {
          $this->db->where("id", $id);
          $query = $this->db->get('jobs_notify_records');
          return $query->result_array();
       }

       public function delete_jobnotification($date) {
          $this->db->where('created_at < ', $date);
          $this->db->delete('jobs_notify');
          return true;
       }

        public function delete_jobnotificationrecord($date) {
          $this->db->where('created_at < ', $date);
          $this->db->delete('jobs_notify_records');
          return true;
       }

       public function delete_jobnotificationpromo($date) {
          $this->db->where('added_at < ', $date);
          $this->db->delete('promo_notifications');
          return true;
       }

}
?>
