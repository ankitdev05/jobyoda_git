<?php
ob_start();
class Questionnaire_Model extends CI_Model {

    function __construct() { 
      parent::__construct(); 
    }

    public function questionnaire_save($data) {
 		    if($this->db->insert("questionnaire", $data)) { 
        	$insert_id = $this->db->insert_id();
        	return $insert_id;
      	} else{
        	return false;
      	}
    }

    public function questionnaire_fetch($id) {
 		    $this->db->where("jobpost_id", $id);
            $this->db->order_by("id", "ASC");
      	$query = $this->db->get('questionnaire');
      	return $query->result_array();      
    }

    public function questionnaire_fetch_byques($id) {
            $this->db->where("id", $id);
            $this->db->order_by("id", "ASC");
        $query = $this->db->get('questionnaire');
        return $query->result_array();      
    }

    public function questionnaire_user_fetch($id, $qid, $uid) {
            $this->db->where("job_id", $id);
            $this->db->where("user_id", $uid);
            $this->db->where("ques_id", $qid);
        $query = $this->db->get('questionnaire_user');
        return $query->result_array();      
    }

    public function questionnaire_delete($id) {
        $this->db->where("jobpost_id", $id);
        $delete = $this->db->delete("questionnaire");
        if($delete) {
          return true;
        } else{
          return false;
        }
    }

    public function questionnaire_user_save($data) {
        if($this->db->insert("questionnaire_user", $data)) { 
          $insert_id = $this->db->insert_id();
          return $insert_id;
        } else{
          return false;
        }
    }

    public function questionnaire_saved_list($id) {
        $this->db->where("user_id", $id);
        $this->db->group_by("job_id");
        $this->db->order_by("created_at", "DESC");
        $query = $this->db->get('questionnaire_user');
        return $query->result_array();      
    }

    public function questionnaire_saved_single($id, $uid) {
        $this->db->where("job_id", $id);
        $this->db->where("user_id", $uid);
        $query = $this->db->get('questionnaire_user');
        return $query->result_array();      
    }

    public function questionnaire_user_single_limit($id, $uid) {
        $this->db->where("user_id", $uid);
        $this->db->where("job_id", $id);
        $this->db->limit(1);
        $query = $this->db->get('questionnaire_user');
        return $query->result_array();      
    }

    public function questionnaire_ques_fetch($id) {
        $this->db->where("id", $id);
        $query = $this->db->get('questionnaire');
        return $query->result_array();      
    }

    public function job_fetch() {
        $query = $this->db->get('job_posting');
        return $query->result_array();      
    }

    public function job_share_count($data) {
        if($this->db->insert("jobsharecount", $data)) { 
          $insert_id = $this->db->insert_id();
          return $insert_id;
        } else{
          return false;
        }
    }

    public function job_fetch_share_single($id) {
        $this->db->where('job_id', $id);
        $query = $this->db->get('jobsharecount');
        return $query->result_array();      
    }

    public function questionnaire_user_update($uid, $jid) {
        $this->db->where('user_id', $uid);
        $this->db->where('job_id', $jid);
        $query = $this->db->update('questionnaire_user', array("result"=>"failed"));
        return true;      
    }

    public function questionnaire_user_update1($uid, $jid) {
        $this->db->where('user_id', $uid);
        $this->db->where('job_id', $jid);
        $query = $this->db->update('questionnaire_user', array("result"=>"recruiter review"));
        return true;      
    }


    public function job_share_count_save($jobid, $sharecount) {
        $this->db->where('job_id', $jobid);
        $query = $this->db->update('jobsharecount', array("sharecount"=>$sharecount));
        return true;      
    }


}