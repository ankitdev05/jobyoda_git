<?php
ob_start();
date_default_timezone_set('Asia/Kolkata');
class Homepage_Model extends CI_Model {

   function __construct() { 
      parent::__construct(); 
      $this->load->model('recruiter/Candidate_Model');
   } 

   public function home_nearby_count($id, $userLat, $userLong, $expfilter) {
      $userLat = (double)$userLat;
      $userLong = (double)$userLong;

      $cDate = date('Y-m-d');
      $uniquearraybyother = array();
      if(strlen($expfilter) > 0) {
         for($ityu = $expfilter; $ityu >-1; $ityu--) {
            $uniquearraybyother[] = $ityu;
         }
      }

      $this->db->select("job_posting.id, job_posting.opening, job_posting.jobexpire, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->where("job_posting.jobexpire >=", $cDate);

      if($id != 0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
      }
      $this->db->having("distance <","7000");
      $query = $this->db->get();
      return $query->result_array();
   }

    public function information_technology_count($id,$expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      $this->db->select("job_posting.id, job_posting.opening,job_posting.jobexpire, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.category", 22);
      if($id != 0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->having("distance<", '7000');
      $this->db->group_by("job_posting.id");
      $query = $this->db->get();
      return $query->result_array();
    }

    public function leadership_count($id,$expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      $this->db->select("job_posting.id, job_posting.opening,job_posting.jobexpire, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.category", 24);
      if($id != 0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->having("distance<", '7000');
      $this->db->group_by("job_posting.id");
      $query = $this->db->get();
      return $query->result_array();
    }

   public function instantscreening_count($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      $this->db->select("job_posting.id, job_posting.opening,job_posting.jobexpire, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->like("job_posting.mode", 'Instant screening');

      if($id != 0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->having("distance<", '7000');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      return $query->result_array();
   }


   public function noexp_count($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      $this->db->select("job_posting.id, job_posting.opening,job_posting.jobexpire, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.experience", 'No Experience');
      if($id != 0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->having("distance<", '7000');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function actively_count($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      $this->db->select("job_posting.id, job_posting.opening,job_posting.jobexpire, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.actively", 1);

      if($id != 0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->having("distance<", '7000');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function nursing_count($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      $this->db->select("job_posting.id, job_posting.opening,job_posting.jobexpire, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where_in("job_posting.subcategory", [41,45]);

      if($id != 0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->having("distance<", '7000');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      return $query->result_array();
   }


   public function hotjob_count($id,$expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      $this->db->select("job_posting.id, job_posting.opening, job_posting.jobexpire, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      //$this->db->where("job_posting.boost_status", 0);
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->having("distance<", '7000');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      return $query->result_array();
      }

      public function feturedtoppicks_bonus_count($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("job_posting.id, job_posting.opening, job_posting.jobexpire, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",1);
      $this->db->having("distance<", '7000');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      return $query->result_array();
      }

      public function feturedtoppicks_freefood_count($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("job_posting.id, job_posting.opening, job_posting.jobexpire, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",2);
      $this->db->having("distance<", '7000');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      return $query->result_array();
      }

   public function feturedtoppicks_dayihmo_count($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("job_posting.id, job_posting.opening, job_posting.jobexpire, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",3);
      $this->db->having("distance<", '7000');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function feturedtoppicks_shift_count($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("job_posting.id, job_posting.opening, job_posting.jobexpire, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",5);
      $this->db->having("distance<", '7000');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function feturedtoppicks_monthpay_count($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("job_posting.id, job_posting.opening, job_posting.jobexpire, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",6);
      $this->db->having("distance<", '7000');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function feturedtoppicks_workfromhome_count($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("job_posting.id, job_posting.opening, job_posting.jobexpire, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",7);
      $this->db->having("distance<", '7000');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function feturedtoppicks_allowances_count($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("job_posting.id, job_posting.opening, job_posting.jobexpire, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_allowances.allowances_id",7);
      $this->db->having("distance<", '7000');
      $query = $this->db->get();
      return $query->result_array();
   }


   public function companysite_jobcount($id, $typeid, $expfilter, $userLat, $userLong) {

      $currentDate = date('Y-m-d');
      $uniquerarray = array();
      for($ikl = 0;$ikl<=$expfilter;$ikl++) {
         $uniquerarray[] = $ikl;
      }

      $this->db->select("job_posting.id, job_posting.opening, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(!empty($expfilter)) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_posting.jobexpire >=", $currentDate);
      $this->db->where("job_posting.company_id", $typeid);
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function city_jobcount($id, $cityname , $expfilter, $userLat, $userLong) {

      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("job_posting.id, job_posting.opening, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->like("recruiter_details.address", $cityname, 'both');
      if($id != 0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(!empty($expfilter)) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->having("distance <","7000");
      $this->db->group_by("job_posting.id");
      $query = $this->db->get();

      // $str = $this->db->last_query();

         //     echo "<pre>";

         //     print_r($str);

         //     die;
      return $query->result_array();
   }

   public function category_jobcount($id, $typeid , $expfilter, $userLat, $userLong) {

      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }
      $this->db->select("job_posting.id, job_posting.opening, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id != 0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.category", $typeid);
      if(!empty($expfilter)) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->having("distance <","7000");
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      return $query->result_array();
   }

   // hot jobcount
   public function hotjob_count_groupby($id,$expfilter, $userLat, $userLong, $filterdata) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }

      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.opening,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire,job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }

      $this->db->having("distance<", '7000');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      return $query->result_array();
   }



   //  Hotjob view all listing
   public function hotjob_fetch_latlongbylimit_groupby($id,$expfilter, $userLat, $userLong, $filterdata) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }

      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.opening,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire,job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');

      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }
      
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }

      if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
         $this->db->where("job_posting.category", $filterdata['jobcategory']);

      } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
         $this->db->where("job_posting.level", $filterdata['joblevel']);

      } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         $this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
         //$this->db->or_like("recruiter.cname", $filterdata['cname'], 'both');
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $this->db->where_in('job_toppicks.picks_id', $toppicks);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $this->db->where_in('job_allowances.allowances_id', $allowances);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $this->db->where_in('job_workshift.workshift_id', $workshift);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         $this->db->where_in('job_leaves.leaves_id', $leaves);
      }

      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->having('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->having('salary <=', 10000);
            $this->db->having('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->having('salary <=', 20000);
            $this->db->having('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->having('salary <=', 30000);
            $this->db->having('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->having('salary <=', 40000);
            $this->db->having('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->having('salary <=', 50000);
            $this->db->having('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->having('salary <=', 70000);
            $this->db->having('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->having('salary <=', 80000);
            $this->db->having('salary >=', 70000);
         }
      }
      $this->db->having("distance<", '7000');
      $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      if($query->num_rows() > 0) {
         return $query->result_array();
      } else {

         $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.opening,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire,job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
         $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
         $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');

         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
           $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
           $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
           $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
         }
         
         $this->db->where("job_posting.jobexpire >=", $cDate);
         if($id!=0) {
            $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
         }
         if(strlen($expfilter) > 0) {
            $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
         }

         if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
            $this->db->where("job_posting.category", $filterdata['jobcategory']);

         } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
            $this->db->where("job_posting.level", $filterdata['joblevel']);

         } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
            //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
            $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
         }

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
            $toppicks = $filterdata['toppicks'];
            $this->db->where_in('job_toppicks.picks_id', $toppicks);
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
            $allowances = $filterdata['allowances'];
            $this->db->where_in('job_allowances.allowances_id', $allowances);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $medical = $filterdata['medical'];
            $this->db->where_in('job_medical.medical_id', $medical);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $workshift = $filterdata['workshift'];
            $this->db->where_in('job_workshift.workshift_id', $workshift);
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
            $leaves = $filterdata['leaves'];
            $this->db->where_in('job_leaves.leaves_id', $leaves);
         }

         if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
            //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
         } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
            //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
         }

         if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
            
            if($filterdata['basicSalary'] == 5) {
               $this->db->having('salary <=', 5000);

            } else if($filterdata['basicSalary'] == 10) {
               $this->db->having('salary <=', 10000);
               $this->db->having('salary >=', 5000);

            } else if($filterdata['basicSalary'] == 20) {
               $this->db->having('salary <=', 20000);
               $this->db->having('salary >=', 10000);

            } else if($filterdata['basicSalary'] == 30) {
               $this->db->having('salary <=', 30000);
               $this->db->having('salary >=', 20000);
            
            } else if($filterdata['basicSalary'] == 40) {
               $this->db->having('salary <=', 40000);
               $this->db->having('salary >=', 30000);
            
            } else if($filterdata['basicSalary'] == 50) {
               $this->db->having('salary <=', 50000);
               $this->db->having('salary >=', 40000);
            
            } else if($filterdata['basicSalary'] == 70) {
               $this->db->having('salary <=', 70000);
               $this->db->having('salary >=', 50000);
            
            } else if($filterdata['basicSalary'] == 80) {
               $this->db->having('salary <=', 80000);
               $this->db->having('salary >=', 70000);
            }
         }
         $this->db->having("distance<", '7000');
         $this->db->group_by("job_posting.company_id");
         $query = $this->db->get();
         if($query->num_rows() > 0) {
            return $query->result_array();
         } else {

            $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.recruiter_id');
            $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
            $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
            $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');

            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
              $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
              $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
              $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }
            
            $this->db->where("job_posting.jobexpire >=", $cDate);
            if($id!=0) {
               $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            }
            if(strlen($expfilter) > 0) {
               $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
            }

            if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
               $this->db->where("job_posting.category", $filterdata['jobcategory']);

            } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
               $this->db->where("job_posting.level", $filterdata['joblevel']);

            } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
               //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
               $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
            }

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
               $toppicks = $filterdata['toppicks'];
               $this->db->where_in('job_toppicks.picks_id', $toppicks);
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
               $allowances = $filterdata['allowances'];
               $this->db->where_in('job_allowances.allowances_id', $allowances);
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
               $medical = $filterdata['medical'];
               $this->db->where_in('job_medical.medical_id', $medical);
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
               $workshift = $filterdata['workshift'];
               $this->db->where_in('job_workshift.workshift_id', $workshift);
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
               $leaves = $filterdata['leaves'];
               $this->db->where_in('job_leaves.leaves_id', $leaves);
            }

            if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
               //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
            } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
               //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
            }

            if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
               
               if($filterdata['basicSalary'] == 5) {
                  $this->db->having('salary <=', 5000);

               } else if($filterdata['basicSalary'] == 10) {
                  $this->db->having('salary <=', 10000);
                  $this->db->having('salary >=', 5000);

               } else if($filterdata['basicSalary'] == 20) {
                  $this->db->having('salary <=', 20000);
                  $this->db->having('salary >=', 10000);

               } else if($filterdata['basicSalary'] == 30) {
                  $this->db->having('salary <=', 30000);
                  $this->db->having('salary >=', 20000);
               
               } else if($filterdata['basicSalary'] == 40) {
                  $this->db->having('salary <=', 40000);
                  $this->db->having('salary >=', 30000);
               
               } else if($filterdata['basicSalary'] == 50) {
                  $this->db->having('salary <=', 50000);
                  $this->db->having('salary >=', 40000);
               
               } else if($filterdata['basicSalary'] == 70) {
                  $this->db->having('salary <=', 70000);
                  $this->db->having('salary >=', 50000);
               
               } else if($filterdata['basicSalary'] == 80) {
                  $this->db->having('salary <=', 80000);
                  $this->db->having('salary >=', 70000);
               }
            }
            $this->db->having("distance<", '7000');
            $this->db->group_by("job_posting.recruiter_id");
            $query = $this->db->get();
            return $query->result_array();
         }
      }
   }

   public function boostjob_fetch_latlong($id,$expfilter, $userLat, $userLong, $filterdata) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) { 
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }

      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.opening,job_posting.jobPitch, job_posting.boost_status,job_posting.mode, job_posting.modeurl, job_posting.chatbot,job_posting.jobexpire,job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      //$this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      
      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }
      
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.boost_status !=", 0);
      if(strlen($expfilter) > 0) { 
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }

      if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
         $this->db->where("job_posting.category", $filterdata['jobcategory']);

      } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
         $this->db->where("job_posting.level", $filterdata['joblevel']);

      } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         $this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
         //$this->db->or_like("recruiter.cname", $filterdata['cname'], 'both');
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $this->db->where_in('job_toppicks.picks_id', $toppicks);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $this->db->where_in('job_allowances.allowances_id', $allowances);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $this->db->where_in('job_workshift.workshift_id', $workshift);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         $this->db->where_in('job_leaves.leaves_id', $leaves);
      }
      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }
      
      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->having('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->having('salary <=', 10000);
            $this->db->having('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->having('salary <=', 20000);
            $this->db->having('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->having('salary <=', 30000);
            $this->db->having('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->having('salary <=', 40000);
            $this->db->having('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->having('salary <=', 50000);
            $this->db->having('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->having('salary <=', 70000);
            $this->db->having('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->having('salary <=', 80000);
            $this->db->having('salary >=', 70000);
         }
      }
      $this->db->having("distance <","7000");
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      if($query->num_rows() > 0) {
         return $query->result_array();
      } else {

         $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.opening,job_posting.jobPitch, job_posting.boost_status,job_posting.mode, job_posting.modeurl, job_posting.chatbot,job_posting.jobexpire,job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
         $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
         //$this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         
         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
           $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
           $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
           $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
           $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
         }
         
         $this->db->where("job_posting.jobexpire >=", $cDate);
         if($id!=0) {
            $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
         }
         $this->db->where("job_posting.boost_status !=", 0);
         if(strlen($expfilter) > 0) { 
            $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
         }

         if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
            $this->db->where("job_posting.category", $filterdata['jobcategory']);

         } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
            $this->db->where("job_posting.level", $filterdata['joblevel']);

         } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
            //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
            $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
         }

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
            $toppicks = $filterdata['toppicks'];
            $this->db->where_in('job_toppicks.picks_id', $toppicks);
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
            $allowances = $filterdata['allowances'];
            $this->db->where_in('job_allowances.allowances_id', $allowances);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $medical = $filterdata['medical'];
            $this->db->where_in('job_medical.medical_id', $medical);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $workshift = $filterdata['workshift'];
            $this->db->where_in('job_workshift.workshift_id', $workshift);
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
            $leaves = $filterdata['leaves'];
            $this->db->where_in('job_leaves.leaves_id', $leaves);
         }
         if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
            //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
         } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
            //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
         }
         
         if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
            
            if($filterdata['basicSalary'] == 5) {
               $this->db->having('salary <=', 5000);

            } else if($filterdata['basicSalary'] == 10) {
               $this->db->having('salary <=', 10000);
               $this->db->having('salary >=', 5000);

            } else if($filterdata['basicSalary'] == 20) {
               $this->db->having('salary <=', 20000);
               $this->db->having('salary >=', 10000);

            } else if($filterdata['basicSalary'] == 30) {
               $this->db->having('salary <=', 30000);
               $this->db->having('salary >=', 20000);
            
            } else if($filterdata['basicSalary'] == 40) {
               $this->db->having('salary <=', 40000);
               $this->db->having('salary >=', 30000);
            
            } else if($filterdata['basicSalary'] == 50) {
               $this->db->having('salary <=', 50000);
               $this->db->having('salary >=', 40000);
            
            } else if($filterdata['basicSalary'] == 70) {
               $this->db->having('salary <=', 70000);
               $this->db->having('salary >=', 50000);
            
            } else if($filterdata['basicSalary'] == 80) {
               $this->db->having('salary <=', 80000);
               $this->db->having('salary >=', 70000);
            }
         }
         $this->db->having("distance <","7000");
         $this->db->group_by("exp_with_salary.jobpost_id");
         $query = $this->db->get();
         if($query->num_rows() > 0) {
            return $query->result_array();
         } else {

            $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.opening,job_posting.jobPitch, job_posting.boost_status,job_posting.mode, job_posting.modeurl, job_posting.chatbot,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'job_posting.recruiter_id = recruiter_details.recruiter_id');
            $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
            $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
            //$this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
            
            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
              $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
              $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
              $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
              $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }
            
            $this->db->where("job_posting.jobexpire >=", $cDate);
            if($id!=0) {
               $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            }
            $this->db->where("job_posting.boost_status !=", 0);
            if(strlen($expfilter) > 0) { 
               $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
            }

            if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
               $this->db->where("job_posting.category", $filterdata['jobcategory']);

            } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
               $this->db->where("job_posting.level", $filterdata['joblevel']);

            } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
               //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
               $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
            }

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
               $toppicks = $filterdata['toppicks'];
               $this->db->where_in('job_toppicks.picks_id', $toppicks);
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
               $allowances = $filterdata['allowances'];
               $this->db->where_in('job_allowances.allowances_id', $allowances);
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
               $medical = $filterdata['medical'];
               $this->db->where_in('job_medical.medical_id', $medical);
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
               $workshift = $filterdata['workshift'];
               $this->db->where_in('job_workshift.workshift_id', $workshift);
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
               $leaves = $filterdata['leaves'];
               $this->db->where_in('job_leaves.leaves_id', $leaves);
            }
            if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
               //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
            } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
               //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
            }
            
            if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
               
               if($filterdata['basicSalary'] == 5) {
                  $this->db->having('salary <=', 5000);

               } else if($filterdata['basicSalary'] == 10) {
                  $this->db->having('salary <=', 10000);
                  $this->db->having('salary >=', 5000);

               } else if($filterdata['basicSalary'] == 20) {
                  $this->db->having('salary <=', 20000);
                  $this->db->having('salary >=', 10000);

               } else if($filterdata['basicSalary'] == 30) {
                  $this->db->having('salary <=', 30000);
                  $this->db->having('salary >=', 20000);
               
               } else if($filterdata['basicSalary'] == 40) {
                  $this->db->having('salary <=', 40000);
                  $this->db->having('salary >=', 30000);
               
               } else if($filterdata['basicSalary'] == 50) {
                  $this->db->having('salary <=', 50000);
                  $this->db->having('salary >=', 40000);
               
               } else if($filterdata['basicSalary'] == 70) {
                  $this->db->having('salary <=', 70000);
                  $this->db->having('salary >=', 50000);
               
               } else if($filterdata['basicSalary'] == 80) {
                  $this->db->having('salary <=', 80000);
                  $this->db->having('salary >=', 70000);
               }
            }
            $this->db->having("distance <","7000");
            $this->db->group_by("exp_with_salary.jobpost_id");
            $query = $this->db->get();
            return $query->result_array();
         }
      }
   }

   public function hotjob_fetch_latlong($id,$expfilter, $userLat, $userLong, $filterdata) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();

      if(strlen($expfilter) > 0) { 
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.opening,job_posting.jobPitch, job_posting.boost_status,job_posting.mode, job_posting.modeurl, job_posting.chatbot,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.boost_status", 0);
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      
      if(strlen($expfilter) > 0) { 
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
         $this->db->where("job_posting.category", $filterdata['jobcategory']);

      } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
         $this->db->where("job_posting.level", $filterdata['joblevel']);

      } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         $this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
         //$this->db->or_like("recruiter.cname", $filterdata['cname'], 'both');
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $this->db->where_in('job_toppicks.picks_id', $toppicks);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $this->db->where_in('job_allowances.allowances_id', $allowances);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $this->db->where_in('job_workshift.workshift_id', $workshift);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         $this->db->where_in('job_leaves.leaves_id', $leaves);
      }
      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->having('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->having('salary <=', 10000);
            $this->db->having('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->having('salary <=', 20000);
            $this->db->having('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->having('salary <=', 30000);
            $this->db->having('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->having('salary <=', 40000);
            $this->db->having('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->having('salary <=', 50000);
            $this->db->having('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->having('salary <=', 70000);
            $this->db->having('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->having('salary <=', 80000);
            $this->db->having('salary >=', 70000);
         }
      }
      $this->db->having("distance <","7000");
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      if($query->num_rows() > 0) {
         return $query->result_array();
      } else {

         $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.opening,job_posting.jobPitch, job_posting.boost_status,job_posting.mode, job_posting.modeurl, job_posting.chatbot,job_posting.jobexpire,job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
         $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
         $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
           $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
           $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
           $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
         }
         $this->db->where("job_posting.jobexpire >=", $cDate);
         $this->db->where("job_posting.boost_status", 0);
         if($id!=0) {
            $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
         }
         
         if(strlen($expfilter) > 0) { 
            $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
         }
         if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
            $this->db->where("job_posting.category", $filterdata['jobcategory']);

         } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
            $this->db->where("job_posting.level", $filterdata['joblevel']);

         } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
            //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
            $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
         }

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
            $toppicks = $filterdata['toppicks'];
            $this->db->where_in('job_toppicks.picks_id', $toppicks);
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
            $allowances = $filterdata['allowances'];
            $this->db->where_in('job_allowances.allowances_id', $allowances);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $medical = $filterdata['medical'];
            $this->db->where_in('job_medical.medical_id', $medical);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $workshift = $filterdata['workshift'];
            $this->db->where_in('job_workshift.workshift_id', $workshift);
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
            $leaves = $filterdata['leaves'];
            $this->db->where_in('job_leaves.leaves_id', $leaves);
         }
         if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
            //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
         } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
            //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
         }

         if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
            
            if($filterdata['basicSalary'] == 5) {
               $this->db->having('salary <=', 5000);

            } else if($filterdata['basicSalary'] == 10) {
               $this->db->having('salary <=', 10000);
               $this->db->having('salary >=', 5000);

            } else if($filterdata['basicSalary'] == 20) {
               $this->db->having('salary <=', 20000);
               $this->db->having('salary >=', 10000);

            } else if($filterdata['basicSalary'] == 30) {
               $this->db->having('salary <=', 30000);
               $this->db->having('salary >=', 20000);
            
            } else if($filterdata['basicSalary'] == 40) {
               $this->db->having('salary <=', 40000);
               $this->db->having('salary >=', 30000);
            
            } else if($filterdata['basicSalary'] == 50) {
               $this->db->having('salary <=', 50000);
               $this->db->having('salary >=', 40000);
            
            } else if($filterdata['basicSalary'] == 70) {
               $this->db->having('salary <=', 70000);
               $this->db->having('salary >=', 50000);
            
            } else if($filterdata['basicSalary'] == 80) {
               $this->db->having('salary <=', 80000);
               $this->db->having('salary >=', 70000);
            }
         }
         $this->db->having("distance <","7000");
         $this->db->group_by("exp_with_salary.jobpost_id");
         $query = $this->db->get();
         if($query->num_rows() > 0) {
            return $query->result_array();
         } else {

            $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.opening,job_posting.jobPitch, job_posting.boost_status,job_posting.mode, job_posting.modeurl, job_posting.chatbot,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'job_posting.recruiter_id = recruiter_details.recruiter_id');
            $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
            $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
            $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
            
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
              $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
              $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
              $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }
            $this->db->where("job_posting.jobexpire >=", $cDate);
            $this->db->where("job_posting.boost_status", 0);
            if($id!=0) {
               $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            }
            
            if(strlen($expfilter) > 0) { 
               $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
            }
            if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
               $this->db->where("job_posting.category", $filterdata['jobcategory']);

            } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
               $this->db->where("job_posting.level", $filterdata['joblevel']);

            } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
               //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
               $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
            }

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
               $toppicks = $filterdata['toppicks'];
               $this->db->where_in('job_toppicks.picks_id', $toppicks);
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
               $allowances = $filterdata['allowances'];
               $this->db->where_in('job_allowances.allowances_id', $allowances);
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
               $medical = $filterdata['medical'];
               $this->db->where_in('job_medical.medical_id', $medical);
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
               $workshift = $filterdata['workshift'];
               $this->db->where_in('job_workshift.workshift_id', $workshift);
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
               $leaves = $filterdata['leaves'];
               $this->db->where_in('job_leaves.leaves_id', $leaves);
            }
            if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
               //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
            } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
               //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
            }

            if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
               
               if($filterdata['basicSalary'] == 5) {
                  $this->db->having('salary <=', 5000);

               } else if($filterdata['basicSalary'] == 10) {
                  $this->db->having('salary <=', 10000);
                  $this->db->having('salary >=', 5000);

               } else if($filterdata['basicSalary'] == 20) {
                  $this->db->having('salary <=', 20000);
                  $this->db->having('salary >=', 10000);

               } else if($filterdata['basicSalary'] == 30) {
                  $this->db->having('salary <=', 30000);
                  $this->db->having('salary >=', 20000);
               
               } else if($filterdata['basicSalary'] == 40) {
                  $this->db->having('salary <=', 40000);
                  $this->db->having('salary >=', 30000);
               
               } else if($filterdata['basicSalary'] == 50) {
                  $this->db->having('salary <=', 50000);
                  $this->db->having('salary >=', 40000);
               
               } else if($filterdata['basicSalary'] == 70) {
                  $this->db->having('salary <=', 70000);
                  $this->db->having('salary >=', 50000);
               
               } else if($filterdata['basicSalary'] == 80) {
                  $this->db->having('salary <=', 80000);
                  $this->db->having('salary >=', 70000);
               }
            }
            $this->db->having("distance <","7000");
            $this->db->group_by("exp_with_salary.jobpost_id");
            $query = $this->db->get();
            return $query->result_array();
         }
      }
   }


   // nearby view all listing

   public function job_fetch_homeforappnearby_groupby($id, $userLat, $userLong, $expfilter, $filterdata) {
      $userLat = (double)$userLat;
      $userLong = (double)$userLong;
      $cDate = date('Y-m-d');
      $uniquearraybyother = array();
      if(strlen($expfilter) > 0) {
         for($ityu = $expfilter; $ityu >-1; $ityu--) {
             $uniquearraybyother[] = $ityu;
         }
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }

      $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.actively,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, recruiter_details.latitude,recruiter_details.longitude,  (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      
      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }

      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }

      $this->db->where("job_posting.jobexpire >=", $cDate);
      if($id != 0) {
          $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
          $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
      }

      if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
         $this->db->where("job_posting.category", $filterdata['jobcategory']);

      } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
         $this->db->where("job_posting.level", $filterdata['joblevel']);

      } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {

         $this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');

         //$this->db->where('MATCH("job_posting.jobtitle") AGAINT (' . $this->db->escape($filterdata['cname']) . ')');
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $this->db->where_in('job_toppicks.picks_id', $toppicks);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $this->db->where_in('job_allowances.allowances_id', $allowances);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $this->db->where_in('job_workshift.workshift_id', $workshift);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         $this->db->where_in('job_leaves.leaves_id', $leaves);
      }

      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->having('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->having('salary <=', 10000);
            $this->db->having('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->having('salary <=', 20000);
            $this->db->having('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->having('salary <=', 30000);
            $this->db->having('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->having('salary <=', 40000);
            $this->db->having('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->having('salary <=', 50000);
            $this->db->having('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->having('salary <=', 70000);
            $this->db->having('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->having('salary <=', 80000);
            $this->db->having('salary >=', 70000);
         }
      }

      $this->db->having("distance <","7000");
      $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      //var_dump($query->num_rows());die;
      if($query->num_rows() > 0) {
         return $query->result_array();
      } else {

         $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.actively,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, recruiter_details.latitude,recruiter_details.longitude,  (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
         $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
         
         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
           $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
           $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
           $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
           $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
         }

         $this->db->where("job_posting.jobexpire >=", $cDate);
         if($id != 0) {
             $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
         }
         if(strlen($expfilter) > 0) {
             $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
         }

         if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
            $this->db->where("job_posting.category", $filterdata['jobcategory']);

         } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
            $this->db->where("job_posting.level", $filterdata['joblevel']);

         } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
            $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
         }

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
            $toppicks = $filterdata['toppicks'];
            $this->db->where_in('job_toppicks.picks_id', $toppicks);
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
            $allowances = $filterdata['allowances'];
            $this->db->where_in('job_allowances.allowances_id', $allowances);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $medical = $filterdata['medical'];
            $this->db->where_in('job_medical.medical_id', $medical);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $workshift = $filterdata['workshift'];
            $this->db->where_in('job_workshift.workshift_id', $workshift);
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
            $leaves = $filterdata['leaves'];
            //$categories5 = explode(",", $leaves);
            $this->db->where_in('job_leaves.leaves_id', $leaves);
         }

         if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
            //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
         } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
            //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
         }

         if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
            
            if($filterdata['basicSalary'] == 5) {
               $this->db->having('salary <=', 5000);

            } else if($filterdata['basicSalary'] == 10) {
               $this->db->having('salary <=', 10000);
               $this->db->having('salary >=', 5000);

            } else if($filterdata['basicSalary'] == 20) {
               $this->db->having('salary <=', 20000);
               $this->db->having('salary >=', 10000);

            } else if($filterdata['basicSalary'] == 30) {
               $this->db->having('salary <=', 30000);
               $this->db->having('salary >=', 20000);
            
            } else if($filterdata['basicSalary'] == 40) {
               $this->db->having('salary <=', 40000);
               $this->db->having('salary >=', 30000);
            
            } else if($filterdata['basicSalary'] == 50) {
               $this->db->having('salary <=', 50000);
               $this->db->having('salary >=', 40000);
            
            } else if($filterdata['basicSalary'] == 70) {
               $this->db->having('salary <=', 70000);
               $this->db->having('salary >=', 50000);
            
            } else if($filterdata['basicSalary'] == 80) {
               $this->db->having('salary <=', 80000);
               $this->db->having('salary >=', 70000);
            }
         }
         $this->db->having("distance <","7000");
         $this->db->group_by("job_posting.company_id");
         $query = $this->db->get();
         if($query->num_rows() > 0) {
            return $query->result_array();
         } else {
            
            $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.actively,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, recruiter_details.latitude,recruiter_details.longitude,  (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.recruiter_id');
            $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
            $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
            
            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
              $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
              $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
              $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
              $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }

            $this->db->where("job_posting.jobexpire >=", $cDate);
            if($id != 0) {
                $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            }
            if(strlen($expfilter) > 0) {
                $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
            }

            if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
               $this->db->where("job_posting.category", $filterdata['jobcategory']);

            } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
               $this->db->where("job_posting.level", $filterdata['joblevel']);

            } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
               $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
            }

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
               $toppicks = $filterdata['toppicks'];
               $this->db->where_in('job_toppicks.picks_id', $toppicks);
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
               $allowances = $filterdata['allowances'];
               $this->db->where_in('job_allowances.allowances_id', $allowances);
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
               $medical = $filterdata['medical'];
               $this->db->where_in('job_medical.medical_id', $medical);
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
               $workshift = $filterdata['workshift'];
               $this->db->where_in('job_workshift.workshift_id', $workshift);
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
               $leaves = $filterdata['leaves'];
               //$categories5 = explode(",", $leaves);
               $this->db->where_in('job_leaves.leaves_id', $leaves);
            }

            if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
               //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
            } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
               //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
            }

            if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
               
               if($filterdata['basicSalary'] == 5) {
                  $this->db->having('salary <=', 5000);

               } else if($filterdata['basicSalary'] == 10) {
                  $this->db->having('salary <=', 10000);
                  $this->db->having('salary >=', 5000);

               } else if($filterdata['basicSalary'] == 20) {
                  $this->db->having('salary <=', 20000);
                  $this->db->having('salary >=', 10000);

               } else if($filterdata['basicSalary'] == 30) {
                  $this->db->having('salary <=', 30000);
                  $this->db->having('salary >=', 20000);
               
               } else if($filterdata['basicSalary'] == 40) {
                  $this->db->having('salary <=', 40000);
                  $this->db->having('salary >=', 30000);
               
               } else if($filterdata['basicSalary'] == 50) {
                  $this->db->having('salary <=', 50000);
                  $this->db->having('salary >=', 40000);
               
               } else if($filterdata['basicSalary'] == 70) {
                  $this->db->having('salary <=', 70000);
                  $this->db->having('salary >=', 50000);
               
               } else if($filterdata['basicSalary'] == 80) {
                  $this->db->having('salary <=', 80000);
                  $this->db->having('salary >=', 70000);
               }
            }
            $this->db->having("distance <","7000");
            $this->db->group_by("job_posting.recruiter_id");
            $query = $this->db->get();
            return $query->result_array();
         }

      }
   }

   public function nearbyjob_fetch_latlong($id, $userLat, $userLong, $expfilter, $filterdata) {
      $userLat = (double)$userLat;
      $userLong = (double)$userLong;
      $cDate = date('Y-m-d');
      $uniquearraybyother = array();
      for($ityu = $expfilter; $ityu >-1; $ityu--) {
         $uniquearraybyother[] = $ityu;
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }

      $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.actively,job_posting.jobPitch,job_posting.mode, job_posting.modeurl, job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id,job_posting.boost_status, recruiter_details.latitude,recruiter_details.longitude, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }
      
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
      }

      if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
         $this->db->where("job_posting.category", $filterdata['jobcategory']);

      } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
         $this->db->where("job_posting.level", $filterdata['joblevel']);

      } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         $this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
         //$this->db->or_like("recruiter.cname", $filterdata['cname'], 'both');
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         //$categories1 = explode(",", $toppicks);
         $this->db->where_in('job_toppicks.picks_id', $toppicks);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         //$categories2 = explode(",", $allowances);
         $this->db->where_in('job_allowances.allowances_id', $allowances);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         //$categories3 = explode(",", $medical);
         $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         //$categories4 = explode(",", $workshift);
         $this->db->where_in('job_workshift.workshift_id', $workshift);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         //$categories5 = explode(",", $leaves);
         $this->db->where_in('job_leaves.leaves_id', $leaves);
      }

      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->having('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->having('salary <=', 10000);
            $this->db->having('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->having('salary <=', 20000);
            $this->db->having('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->having('salary <=', 30000);
            $this->db->having('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->having('salary <=', 40000);
            $this->db->having('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->having('salary <=', 50000);
            $this->db->having('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->having('salary <=', 70000);
            $this->db->having('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->having('salary <=', 80000);
            $this->db->having('salary >=', 70000);
         }
      }
      $this->db->having("distance <","7000");
      $this->db->group_by("exp_with_salary.jobpost_id");

      $query = $this->db->get();
      if($query->num_rows() > 0) {

         return $query->result_array();
      } else {

         $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.actively,job_posting.jobPitch,job_posting.mode, job_posting.modeurl, job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id,job_posting.boost_status, recruiter_details.latitude,recruiter_details.longitude, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      
         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
         $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
           $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
           $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
           $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
           $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
         }
         
         $this->db->where("job_posting.jobexpire >=", $cDate);
         if($id!=0) {
            $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
         }
         if(strlen($expfilter) > 0) {
            $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
         }

         if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
            $this->db->where("job_posting.category", $filterdata['jobcategory']);

         } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
            $this->db->where("job_posting.level", $filterdata['joblevel']);

         } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
            //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
            $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
         }

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
            $toppicks = $filterdata['toppicks'];
            //$categories1 = explode(",", $toppicks);
            $this->db->where_in('job_toppicks.picks_id', $toppicks);
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
            $allowances = $filterdata['allowances'];
            //$categories2 = explode(",", $allowances);
            $this->db->where_in('job_allowances.allowances_id', $allowances);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $medical = $filterdata['medical'];
            //$categories3 = explode(",", $medical);
            $this->db->where_in('job_medical.medical_id', $medical);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $workshift = $filterdata['workshift'];
            //$categories4 = explode(",", $workshift);
            $this->db->where_in('job_workshift.workshift_id', $workshift);
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
            $leaves = $filterdata['leaves'];
            //$categories5 = explode(",", $leaves);
            $this->db->where_in('job_leaves.leaves_id', $leaves);
         }

         if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
            //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
         } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
            //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
         }

         if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
            
            if($filterdata['basicSalary'] == 5) {
               $this->db->having('salary <=', 5000);

            } else if($filterdata['basicSalary'] == 10) {
               $this->db->having('salary <=', 10000);
               $this->db->having('salary >=', 5000);

            } else if($filterdata['basicSalary'] == 20) {
               $this->db->having('salary <=', 20000);
               $this->db->having('salary >=', 10000);

            } else if($filterdata['basicSalary'] == 30) {
               $this->db->having('salary <=', 30000);
               $this->db->having('salary >=', 20000);
            
            } else if($filterdata['basicSalary'] == 40) {
               $this->db->having('salary <=', 40000);
               $this->db->having('salary >=', 30000);
            
            } else if($filterdata['basicSalary'] == 50) {
               $this->db->having('salary <=', 50000);
               $this->db->having('salary >=', 40000);
            
            } else if($filterdata['basicSalary'] == 70) {
               $this->db->having('salary <=', 70000);
               $this->db->having('salary >=', 50000);
            
            } else if($filterdata['basicSalary'] == 80) {
               $this->db->having('salary <=', 80000);
               $this->db->having('salary >=', 70000);
            }
         }
         $this->db->having("distance <","7000");
         $this->db->group_by("exp_with_salary.jobpost_id");
         $query = $this->db->get();

         if($query->num_rows() > 0) {
            return $query->result_array();
         } else {

            $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.actively,job_posting.jobPitch,job_posting.mode, job_posting.modeurl, job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id,job_posting.boost_status, recruiter_details.latitude,recruiter_details.longitude, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      
            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.recruiter_id');
            $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
            $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
              $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
              $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
              $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
              $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }
            
            $this->db->where("job_posting.jobexpire >=", $cDate);
            if($id!=0) {
               $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            }
            if(strlen($expfilter) > 0) {
               $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
            }

            if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
               $this->db->where("job_posting.category", $filterdata['jobcategory']);

            } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
               $this->db->where("job_posting.level", $filterdata['joblevel']);

            } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
               //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
               $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
            }

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
               $toppicks = $filterdata['toppicks'];
               //$categories1 = explode(",", $toppicks);
               $this->db->where_in('job_toppicks.picks_id', $toppicks);
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
               $allowances = $filterdata['allowances'];
               //$categories2 = explode(",", $allowances);
               $this->db->where_in('job_allowances.allowances_id', $allowances);
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
               $medical = $filterdata['medical'];
               //$categories3 = explode(",", $medical);
               $this->db->where_in('job_medical.medical_id', $medical);
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
               $workshift = $filterdata['workshift'];
               //$categories4 = explode(",", $workshift);
               $this->db->where_in('job_workshift.workshift_id', $workshift);
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
               $leaves = $filterdata['leaves'];
               //$categories5 = explode(",", $leaves);
               $this->db->where_in('job_leaves.leaves_id', $leaves);
            }

            if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
               //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
            } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
               //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
            }

            if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
               
               if($filterdata['basicSalary'] == 5) {
                  $this->db->having('salary <=', 5000);

               } else if($filterdata['basicSalary'] == 10) {
                  $this->db->having('salary <=', 10000);
                  $this->db->having('salary >=', 5000);

               } else if($filterdata['basicSalary'] == 20) {
                  $this->db->having('salary <=', 20000);
                  $this->db->having('salary >=', 10000);

               } else if($filterdata['basicSalary'] == 30) {
                  $this->db->having('salary <=', 30000);
                  $this->db->having('salary >=', 20000);
               
               } else if($filterdata['basicSalary'] == 40) {
                  $this->db->having('salary <=', 40000);
                  $this->db->having('salary >=', 30000);
               
               } else if($filterdata['basicSalary'] == 50) {
                  $this->db->having('salary <=', 50000);
                  $this->db->having('salary >=', 40000);
               
               } else if($filterdata['basicSalary'] == 70) {
                  $this->db->having('salary <=', 70000);
                  $this->db->having('salary >=', 50000);
               
               } else if($filterdata['basicSalary'] == 80) {
                  $this->db->having('salary <=', 80000);
                  $this->db->having('salary >=', 70000);
               }
            }
            $this->db->having("distance <","7000");
            $this->db->group_by("exp_with_salary.jobpost_id");
            $query = $this->db->get();
            return $query->result_array();
         }
      }
   }


   // No Experience

   public function job_fetch_homeforappNoExp_groupby($id, $userLat, $userLong, $expfilter, $filterdata) {
      $userLat = (double)$userLat;
      $userLong = (double)$userLong;
      $cDate = date('Y-m-d');
      $uniquearraybyother = array();
      if(strlen($expfilter) > 0) {
         for($ityu = $expfilter; $ityu >-1; $ityu--) {
             $uniquearraybyother[] = $ityu;
         }
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }

      $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.actively,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, recruiter_details.latitude,recruiter_details.longitude,  (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      
      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }

      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }

      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.experience", "No Experience");

      if($id != 0) {
          $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
          $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
      }

      if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
         $this->db->where("job_posting.category", $filterdata['jobcategory']);

      } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
         $this->db->where("job_posting.level", $filterdata['joblevel']);

      } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {

         $this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');

         //$this->db->where('MATCH("job_posting.jobtitle") AGAINT (' . $this->db->escape($filterdata['cname']) . ')');
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $this->db->where_in('job_toppicks.picks_id', $toppicks);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $this->db->where_in('job_allowances.allowances_id', $allowances);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $this->db->where_in('job_workshift.workshift_id', $workshift);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         $this->db->where_in('job_leaves.leaves_id', $leaves);
      }

      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->having('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->having('salary <=', 10000);
            $this->db->having('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->having('salary <=', 20000);
            $this->db->having('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->having('salary <=', 30000);
            $this->db->having('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->having('salary <=', 40000);
            $this->db->having('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->having('salary <=', 50000);
            $this->db->having('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->having('salary <=', 70000);
            $this->db->having('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->having('salary <=', 80000);
            $this->db->having('salary >=', 70000);
         }
      }

      $this->db->having("distance <","7000");
      $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      //var_dump($query->num_rows());die;
      if($query->num_rows() > 0) {
         return $query->result_array();
      } else {

         $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.actively,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, recruiter_details.latitude,recruiter_details.longitude,  (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
         $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
         
         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
           $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
           $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
           $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
           $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
         }

         $this->db->where("job_posting.jobexpire >=", $cDate);
         $this->db->where("job_posting.experience", "No Experience");
         if($id != 0) {
             $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
         }
         if(strlen($expfilter) > 0) {
             $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
         }

         if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
            $this->db->where("job_posting.category", $filterdata['jobcategory']);

         } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
            $this->db->where("job_posting.level", $filterdata['joblevel']);

         } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
            $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
         }

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
            $toppicks = $filterdata['toppicks'];
            $this->db->where_in('job_toppicks.picks_id', $toppicks);
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
            $allowances = $filterdata['allowances'];
            $this->db->where_in('job_allowances.allowances_id', $allowances);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $medical = $filterdata['medical'];
            $this->db->where_in('job_medical.medical_id', $medical);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $workshift = $filterdata['workshift'];
            $this->db->where_in('job_workshift.workshift_id', $workshift);
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
            $leaves = $filterdata['leaves'];
            //$categories5 = explode(",", $leaves);
            $this->db->where_in('job_leaves.leaves_id', $leaves);
         }

         if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
            //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
         } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
            //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
         }

         if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
            
            if($filterdata['basicSalary'] == 5) {
               $this->db->having('salary <=', 5000);

            } else if($filterdata['basicSalary'] == 10) {
               $this->db->having('salary <=', 10000);
               $this->db->having('salary >=', 5000);

            } else if($filterdata['basicSalary'] == 20) {
               $this->db->having('salary <=', 20000);
               $this->db->having('salary >=', 10000);

            } else if($filterdata['basicSalary'] == 30) {
               $this->db->having('salary <=', 30000);
               $this->db->having('salary >=', 20000);
            
            } else if($filterdata['basicSalary'] == 40) {
               $this->db->having('salary <=', 40000);
               $this->db->having('salary >=', 30000);
            
            } else if($filterdata['basicSalary'] == 50) {
               $this->db->having('salary <=', 50000);
               $this->db->having('salary >=', 40000);
            
            } else if($filterdata['basicSalary'] == 70) {
               $this->db->having('salary <=', 70000);
               $this->db->having('salary >=', 50000);
            
            } else if($filterdata['basicSalary'] == 80) {
               $this->db->having('salary <=', 80000);
               $this->db->having('salary >=', 70000);
            }
         }
         $this->db->having("distance <","7000");
         $this->db->group_by("job_posting.company_id");
         $query = $this->db->get();
         if($query->num_rows() > 0) {
            return $query->result_array();
         } else {
            
            $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.actively,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, recruiter_details.latitude,recruiter_details.longitude,  (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.recruiter_id');
            $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
            $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
            
            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
              $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
              $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
              $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
              $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }

            $this->db->where("job_posting.jobexpire >=", $cDate);
            $this->db->where("job_posting.experience", "No Experience");
            if($id != 0) {
                $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            }
            if(strlen($expfilter) > 0) {
                $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
            }

            if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
               $this->db->where("job_posting.category", $filterdata['jobcategory']);

            } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
               $this->db->where("job_posting.level", $filterdata['joblevel']);

            } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
               $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
            }

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
               $toppicks = $filterdata['toppicks'];
               $this->db->where_in('job_toppicks.picks_id', $toppicks);
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
               $allowances = $filterdata['allowances'];
               $this->db->where_in('job_allowances.allowances_id', $allowances);
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
               $medical = $filterdata['medical'];
               $this->db->where_in('job_medical.medical_id', $medical);
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
               $workshift = $filterdata['workshift'];
               $this->db->where_in('job_workshift.workshift_id', $workshift);
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
               $leaves = $filterdata['leaves'];
               //$categories5 = explode(",", $leaves);
               $this->db->where_in('job_leaves.leaves_id', $leaves);
            }

            if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
               //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
            } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
               //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
            }

            if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
               
               if($filterdata['basicSalary'] == 5) {
                  $this->db->having('salary <=', 5000);

               } else if($filterdata['basicSalary'] == 10) {
                  $this->db->having('salary <=', 10000);
                  $this->db->having('salary >=', 5000);

               } else if($filterdata['basicSalary'] == 20) {
                  $this->db->having('salary <=', 20000);
                  $this->db->having('salary >=', 10000);

               } else if($filterdata['basicSalary'] == 30) {
                  $this->db->having('salary <=', 30000);
                  $this->db->having('salary >=', 20000);
               
               } else if($filterdata['basicSalary'] == 40) {
                  $this->db->having('salary <=', 40000);
                  $this->db->having('salary >=', 30000);
               
               } else if($filterdata['basicSalary'] == 50) {
                  $this->db->having('salary <=', 50000);
                  $this->db->having('salary >=', 40000);
               
               } else if($filterdata['basicSalary'] == 70) {
                  $this->db->having('salary <=', 70000);
                  $this->db->having('salary >=', 50000);
               
               } else if($filterdata['basicSalary'] == 80) {
                  $this->db->having('salary <=', 80000);
                  $this->db->having('salary >=', 70000);
               }
            }
            $this->db->having("distance <","7000");
            $this->db->group_by("job_posting.recruiter_id");
            $query = $this->db->get();
            return $query->result_array();
         }

      }
   }

   public function noexpjob_fetch_latlong($id, $userLat, $userLong, $expfilter, $filterdata) {
      $userLat = (double)$userLat;
      $userLong = (double)$userLong;
      $cDate = date('Y-m-d');
      $uniquearraybyother = array();
      for($ityu = $expfilter; $ityu >-1; $ityu--) {
         $uniquearraybyother[] = $ityu;
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }

      $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.actively,job_posting.jobPitch,job_posting.mode, job_posting.modeurl, job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id,job_posting.boost_status, recruiter_details.latitude,recruiter_details.longitude, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }
      
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.experience", "No Experience");
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
      }

      if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
         $this->db->where("job_posting.category", $filterdata['jobcategory']);

      } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
         $this->db->where("job_posting.level", $filterdata['joblevel']);

      } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         $this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
         //$this->db->or_like("recruiter.cname", $filterdata['cname'], 'both');
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         //$categories1 = explode(",", $toppicks);
         $this->db->where_in('job_toppicks.picks_id', $toppicks);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         //$categories2 = explode(",", $allowances);
         $this->db->where_in('job_allowances.allowances_id', $allowances);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         //$categories3 = explode(",", $medical);
         $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         //$categories4 = explode(",", $workshift);
         $this->db->where_in('job_workshift.workshift_id', $workshift);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         //$categories5 = explode(",", $leaves);
         $this->db->where_in('job_leaves.leaves_id', $leaves);
      }

      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->having('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->having('salary <=', 10000);
            $this->db->having('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->having('salary <=', 20000);
            $this->db->having('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->having('salary <=', 30000);
            $this->db->having('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->having('salary <=', 40000);
            $this->db->having('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->having('salary <=', 50000);
            $this->db->having('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->having('salary <=', 70000);
            $this->db->having('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->having('salary <=', 80000);
            $this->db->having('salary >=', 70000);
         }
      }
      $this->db->having("distance <","7000");
      $this->db->group_by("exp_with_salary.jobpost_id");

      $query = $this->db->get();
      if($query->num_rows() > 0) {

         return $query->result_array();
      } else {

         $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.actively,job_posting.jobPitch,job_posting.mode, job_posting.modeurl, job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id,job_posting.boost_status, recruiter_details.latitude,recruiter_details.longitude, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      
         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
         $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
           $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
           $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
           $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
           $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
         }
         
         $this->db->where("job_posting.jobexpire >=", $cDate);
         $this->db->where("job_posting.experience", "No Experience");
         if($id!=0) {
            $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
         }
         if(strlen($expfilter) > 0) {
            $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
         }

         if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
            $this->db->where("job_posting.category", $filterdata['jobcategory']);

         } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
            $this->db->where("job_posting.level", $filterdata['joblevel']);

         } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
            //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
            $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
         }

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
            $toppicks = $filterdata['toppicks'];
            //$categories1 = explode(",", $toppicks);
            $this->db->where_in('job_toppicks.picks_id', $toppicks);
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
            $allowances = $filterdata['allowances'];
            //$categories2 = explode(",", $allowances);
            $this->db->where_in('job_allowances.allowances_id', $allowances);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $medical = $filterdata['medical'];
            //$categories3 = explode(",", $medical);
            $this->db->where_in('job_medical.medical_id', $medical);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $workshift = $filterdata['workshift'];
            //$categories4 = explode(",", $workshift);
            $this->db->where_in('job_workshift.workshift_id', $workshift);
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
            $leaves = $filterdata['leaves'];
            //$categories5 = explode(",", $leaves);
            $this->db->where_in('job_leaves.leaves_id', $leaves);
         }

         if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
            //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
         } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
            //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
         }

         if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
            
            if($filterdata['basicSalary'] == 5) {
               $this->db->having('salary <=', 5000);

            } else if($filterdata['basicSalary'] == 10) {
               $this->db->having('salary <=', 10000);
               $this->db->having('salary >=', 5000);

            } else if($filterdata['basicSalary'] == 20) {
               $this->db->having('salary <=', 20000);
               $this->db->having('salary >=', 10000);

            } else if($filterdata['basicSalary'] == 30) {
               $this->db->having('salary <=', 30000);
               $this->db->having('salary >=', 20000);
            
            } else if($filterdata['basicSalary'] == 40) {
               $this->db->having('salary <=', 40000);
               $this->db->having('salary >=', 30000);
            
            } else if($filterdata['basicSalary'] == 50) {
               $this->db->having('salary <=', 50000);
               $this->db->having('salary >=', 40000);
            
            } else if($filterdata['basicSalary'] == 70) {
               $this->db->having('salary <=', 70000);
               $this->db->having('salary >=', 50000);
            
            } else if($filterdata['basicSalary'] == 80) {
               $this->db->having('salary <=', 80000);
               $this->db->having('salary >=', 70000);
            }
         }
         $this->db->having("distance <","7000");
         $this->db->group_by("exp_with_salary.jobpost_id");
         $query = $this->db->get();

         if($query->num_rows() > 0) {
            return $query->result_array();
         } else {

            $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.actively,job_posting.jobPitch,job_posting.mode, job_posting.modeurl, job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id,job_posting.boost_status, recruiter_details.latitude,recruiter_details.longitude, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      
            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.recruiter_id');
            $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
            $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
              $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
              $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
              $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
              $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }
            
            $this->db->where("job_posting.jobexpire >=", $cDate);
            $this->db->where("job_posting.experience", "No Experience");
            if($id!=0) {
               $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            }
            if(strlen($expfilter) > 0) {
               $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
            }

            if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
               $this->db->where("job_posting.category", $filterdata['jobcategory']);

            } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
               $this->db->where("job_posting.level", $filterdata['joblevel']);

            } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
               //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
               $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
            }

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
               $toppicks = $filterdata['toppicks'];
               //$categories1 = explode(",", $toppicks);
               $this->db->where_in('job_toppicks.picks_id', $toppicks);
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
               $allowances = $filterdata['allowances'];
               //$categories2 = explode(",", $allowances);
               $this->db->where_in('job_allowances.allowances_id', $allowances);
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
               $medical = $filterdata['medical'];
               //$categories3 = explode(",", $medical);
               $this->db->where_in('job_medical.medical_id', $medical);
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
               $workshift = $filterdata['workshift'];
               //$categories4 = explode(",", $workshift);
               $this->db->where_in('job_workshift.workshift_id', $workshift);
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
               $leaves = $filterdata['leaves'];
               //$categories5 = explode(",", $leaves);
               $this->db->where_in('job_leaves.leaves_id', $leaves);
            }

            if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
               //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
            } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
               //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
            }

            if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
               
               if($filterdata['basicSalary'] == 5) {
                  $this->db->having('salary <=', 5000);

               } else if($filterdata['basicSalary'] == 10) {
                  $this->db->having('salary <=', 10000);
                  $this->db->having('salary >=', 5000);

               } else if($filterdata['basicSalary'] == 20) {
                  $this->db->having('salary <=', 20000);
                  $this->db->having('salary >=', 10000);

               } else if($filterdata['basicSalary'] == 30) {
                  $this->db->having('salary <=', 30000);
                  $this->db->having('salary >=', 20000);
               
               } else if($filterdata['basicSalary'] == 40) {
                  $this->db->having('salary <=', 40000);
                  $this->db->having('salary >=', 30000);
               
               } else if($filterdata['basicSalary'] == 50) {
                  $this->db->having('salary <=', 50000);
                  $this->db->having('salary >=', 40000);
               
               } else if($filterdata['basicSalary'] == 70) {
                  $this->db->having('salary <=', 70000);
                  $this->db->having('salary >=', 50000);
               
               } else if($filterdata['basicSalary'] == 80) {
                  $this->db->having('salary <=', 80000);
                  $this->db->having('salary >=', 70000);
               }
            }
            $this->db->having("distance <","7000");
            $this->db->group_by("exp_with_salary.jobpost_id");
            $query = $this->db->get();
            return $query->result_array();
         }
      }
   }


   // Nursing

   public function job_fetch_nursing_groupby($id, $userLat, $userLong, $expfilter, $filterdata) {
      $userLat = (double)$userLat;
      $userLong = (double)$userLong;
      $cDate = date('Y-m-d');
      $uniquearraybyother = array();
      if(strlen($expfilter) > 0) {
         for($ityu = $expfilter; $ityu >-1; $ityu--) {
             $uniquearraybyother[] = $ityu;
         }
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }

      $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.actively,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, recruiter_details.latitude,recruiter_details.longitude,  (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      
      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }

      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }

      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where_in("job_posting.subcategory", [41,45]);

      if($id != 0) {
          $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
          $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
      }

      if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
         $this->db->where("job_posting.category", $filterdata['jobcategory']);

      } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
         $this->db->where("job_posting.level", $filterdata['joblevel']);

      } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {

         $this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');

         //$this->db->where('MATCH("job_posting.jobtitle") AGAINT (' . $this->db->escape($filterdata['cname']) . ')');
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $this->db->where_in('job_toppicks.picks_id', $toppicks);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $this->db->where_in('job_allowances.allowances_id', $allowances);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $this->db->where_in('job_workshift.workshift_id', $workshift);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         $this->db->where_in('job_leaves.leaves_id', $leaves);
      }

      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->having('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->having('salary <=', 10000);
            $this->db->having('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->having('salary <=', 20000);
            $this->db->having('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->having('salary <=', 30000);
            $this->db->having('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->having('salary <=', 40000);
            $this->db->having('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->having('salary <=', 50000);
            $this->db->having('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->having('salary <=', 70000);
            $this->db->having('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->having('salary <=', 80000);
            $this->db->having('salary >=', 70000);
         }
      }

      $this->db->having("distance <","7000");
      $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      //var_dump($query->num_rows());die;
      if($query->num_rows() > 0) {
         return $query->result_array();
      } else {

         $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.actively,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, recruiter_details.latitude,recruiter_details.longitude,  (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
         $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
         
         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
           $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
           $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
           $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
           $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
         }

         $this->db->where("job_posting.jobexpire >=", $cDate);
         $this->db->where_in("job_posting.subcategory", [41,45]);
         if($id != 0) {
             $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
         }
         if(strlen($expfilter) > 0) {
             $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
         }

         if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
            $this->db->where("job_posting.category", $filterdata['jobcategory']);

         } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
            $this->db->where("job_posting.level", $filterdata['joblevel']);

         } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
            $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
         }

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
            $toppicks = $filterdata['toppicks'];
            $this->db->where_in('job_toppicks.picks_id', $toppicks);
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
            $allowances = $filterdata['allowances'];
            $this->db->where_in('job_allowances.allowances_id', $allowances);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $medical = $filterdata['medical'];
            $this->db->where_in('job_medical.medical_id', $medical);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $workshift = $filterdata['workshift'];
            $this->db->where_in('job_workshift.workshift_id', $workshift);
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
            $leaves = $filterdata['leaves'];
            //$categories5 = explode(",", $leaves);
            $this->db->where_in('job_leaves.leaves_id', $leaves);
         }

         if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
            //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
         } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
            //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
         }

         if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
            
            if($filterdata['basicSalary'] == 5) {
               $this->db->having('salary <=', 5000);

            } else if($filterdata['basicSalary'] == 10) {
               $this->db->having('salary <=', 10000);
               $this->db->having('salary >=', 5000);

            } else if($filterdata['basicSalary'] == 20) {
               $this->db->having('salary <=', 20000);
               $this->db->having('salary >=', 10000);

            } else if($filterdata['basicSalary'] == 30) {
               $this->db->having('salary <=', 30000);
               $this->db->having('salary >=', 20000);
            
            } else if($filterdata['basicSalary'] == 40) {
               $this->db->having('salary <=', 40000);
               $this->db->having('salary >=', 30000);
            
            } else if($filterdata['basicSalary'] == 50) {
               $this->db->having('salary <=', 50000);
               $this->db->having('salary >=', 40000);
            
            } else if($filterdata['basicSalary'] == 70) {
               $this->db->having('salary <=', 70000);
               $this->db->having('salary >=', 50000);
            
            } else if($filterdata['basicSalary'] == 80) {
               $this->db->having('salary <=', 80000);
               $this->db->having('salary >=', 70000);
            }
         }
         $this->db->having("distance <","7000");
         $this->db->group_by("job_posting.company_id");
         $query = $this->db->get();
         if($query->num_rows() > 0) {
            return $query->result_array();
         } else {
            
            $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.actively,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, recruiter_details.latitude,recruiter_details.longitude,  (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.recruiter_id');
            $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
            $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
            
            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
              $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
              $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
              $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
              $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }

            $this->db->where("job_posting.jobexpire >=", $cDate);
            $this->db->where_in("job_posting.subcategory", [41,45]);
            if($id != 0) {
                $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            }
            if(strlen($expfilter) > 0) {
                $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
            }

            if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
               $this->db->where("job_posting.category", $filterdata['jobcategory']);

            } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
               $this->db->where("job_posting.level", $filterdata['joblevel']);

            } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
               $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
            }

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
               $toppicks = $filterdata['toppicks'];
               $this->db->where_in('job_toppicks.picks_id', $toppicks);
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
               $allowances = $filterdata['allowances'];
               $this->db->where_in('job_allowances.allowances_id', $allowances);
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
               $medical = $filterdata['medical'];
               $this->db->where_in('job_medical.medical_id', $medical);
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
               $workshift = $filterdata['workshift'];
               $this->db->where_in('job_workshift.workshift_id', $workshift);
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
               $leaves = $filterdata['leaves'];
               //$categories5 = explode(",", $leaves);
               $this->db->where_in('job_leaves.leaves_id', $leaves);
            }

            if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
               //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
            } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
               //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
            }

            if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
               
               if($filterdata['basicSalary'] == 5) {
                  $this->db->having('salary <=', 5000);

               } else if($filterdata['basicSalary'] == 10) {
                  $this->db->having('salary <=', 10000);
                  $this->db->having('salary >=', 5000);

               } else if($filterdata['basicSalary'] == 20) {
                  $this->db->having('salary <=', 20000);
                  $this->db->having('salary >=', 10000);

               } else if($filterdata['basicSalary'] == 30) {
                  $this->db->having('salary <=', 30000);
                  $this->db->having('salary >=', 20000);
               
               } else if($filterdata['basicSalary'] == 40) {
                  $this->db->having('salary <=', 40000);
                  $this->db->having('salary >=', 30000);
               
               } else if($filterdata['basicSalary'] == 50) {
                  $this->db->having('salary <=', 50000);
                  $this->db->having('salary >=', 40000);
               
               } else if($filterdata['basicSalary'] == 70) {
                  $this->db->having('salary <=', 70000);
                  $this->db->having('salary >=', 50000);
               
               } else if($filterdata['basicSalary'] == 80) {
                  $this->db->having('salary <=', 80000);
                  $this->db->having('salary >=', 70000);
               }
            }
            $this->db->having("distance <","7000");
            $this->db->group_by("job_posting.recruiter_id");
            $query = $this->db->get();
            return $query->result_array();
         }

      }
   }

   public function nursingjob_fetch_latlong($id, $userLat, $userLong, $expfilter, $filterdata) {
      $userLat = (double)$userLat;
      $userLong = (double)$userLong;
      $cDate = date('Y-m-d');
      $uniquearraybyother = array();
      for($ityu = $expfilter; $ityu >-1; $ityu--) {
         $uniquearraybyother[] = $ityu;
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }

      $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.actively,job_posting.jobPitch,job_posting.mode, job_posting.modeurl, job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id,job_posting.boost_status, recruiter_details.latitude,recruiter_details.longitude, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }
      
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where_in("job_posting.subcategory", [41,45]);
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
      }

      if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
         $this->db->where("job_posting.category", $filterdata['jobcategory']);

      } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
         $this->db->where("job_posting.level", $filterdata['joblevel']);

      } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         $this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
         //$this->db->or_like("recruiter.cname", $filterdata['cname'], 'both');
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         //$categories1 = explode(",", $toppicks);
         $this->db->where_in('job_toppicks.picks_id', $toppicks);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         //$categories2 = explode(",", $allowances);
         $this->db->where_in('job_allowances.allowances_id', $allowances);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         //$categories3 = explode(",", $medical);
         $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         //$categories4 = explode(",", $workshift);
         $this->db->where_in('job_workshift.workshift_id', $workshift);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         //$categories5 = explode(",", $leaves);
         $this->db->where_in('job_leaves.leaves_id', $leaves);
      }

      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->having('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->having('salary <=', 10000);
            $this->db->having('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->having('salary <=', 20000);
            $this->db->having('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->having('salary <=', 30000);
            $this->db->having('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->having('salary <=', 40000);
            $this->db->having('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->having('salary <=', 50000);
            $this->db->having('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->having('salary <=', 70000);
            $this->db->having('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->having('salary <=', 80000);
            $this->db->having('salary >=', 70000);
         }
      }
      $this->db->having("distance <","7000");
      $this->db->group_by("exp_with_salary.jobpost_id");

      $query = $this->db->get();
      if($query->num_rows() > 0) {

         return $query->result_array();
      } else {

         $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.actively,job_posting.jobPitch,job_posting.mode, job_posting.modeurl, job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id,job_posting.boost_status, recruiter_details.latitude,recruiter_details.longitude, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      
         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
         $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
           $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
           $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
           $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
           $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
         }
         
         $this->db->where("job_posting.jobexpire >=", $cDate);
         $this->db->where_in("job_posting.subcategory", [41,45]);
         if($id!=0) {
            $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
         }
         if(strlen($expfilter) > 0) {
            $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
         }

         if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
            $this->db->where("job_posting.category", $filterdata['jobcategory']);

         } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
            $this->db->where("job_posting.level", $filterdata['joblevel']);

         } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
            //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
            $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
         }

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
            $toppicks = $filterdata['toppicks'];
            //$categories1 = explode(",", $toppicks);
            $this->db->where_in('job_toppicks.picks_id', $toppicks);
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
            $allowances = $filterdata['allowances'];
            //$categories2 = explode(",", $allowances);
            $this->db->where_in('job_allowances.allowances_id', $allowances);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $medical = $filterdata['medical'];
            //$categories3 = explode(",", $medical);
            $this->db->where_in('job_medical.medical_id', $medical);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $workshift = $filterdata['workshift'];
            //$categories4 = explode(",", $workshift);
            $this->db->where_in('job_workshift.workshift_id', $workshift);
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
            $leaves = $filterdata['leaves'];
            //$categories5 = explode(",", $leaves);
            $this->db->where_in('job_leaves.leaves_id', $leaves);
         }

         if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
            //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
         } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
            //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
         }

         if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
            
            if($filterdata['basicSalary'] == 5) {
               $this->db->having('salary <=', 5000);

            } else if($filterdata['basicSalary'] == 10) {
               $this->db->having('salary <=', 10000);
               $this->db->having('salary >=', 5000);

            } else if($filterdata['basicSalary'] == 20) {
               $this->db->having('salary <=', 20000);
               $this->db->having('salary >=', 10000);

            } else if($filterdata['basicSalary'] == 30) {
               $this->db->having('salary <=', 30000);
               $this->db->having('salary >=', 20000);
            
            } else if($filterdata['basicSalary'] == 40) {
               $this->db->having('salary <=', 40000);
               $this->db->having('salary >=', 30000);
            
            } else if($filterdata['basicSalary'] == 50) {
               $this->db->having('salary <=', 50000);
               $this->db->having('salary >=', 40000);
            
            } else if($filterdata['basicSalary'] == 70) {
               $this->db->having('salary <=', 70000);
               $this->db->having('salary >=', 50000);
            
            } else if($filterdata['basicSalary'] == 80) {
               $this->db->having('salary <=', 80000);
               $this->db->having('salary >=', 70000);
            }
         }
         $this->db->having("distance <","7000");
         $this->db->group_by("exp_with_salary.jobpost_id");
         $query = $this->db->get();

         if($query->num_rows() > 0) {
            return $query->result_array();
         } else {

            $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.actively,job_posting.jobPitch,job_posting.mode, job_posting.modeurl, job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id,job_posting.boost_status, recruiter_details.latitude,recruiter_details.longitude, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      
            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.recruiter_id');
            $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
            $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
              $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
              $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
              $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
              $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }
            
            $this->db->where("job_posting.jobexpire >=", $cDate);
            $this->db->where_in("job_posting.subcategory", [41,45]);
            if($id!=0) {
               $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            }
            if(strlen($expfilter) > 0) {
               $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
            }

            if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
               $this->db->where("job_posting.category", $filterdata['jobcategory']);

            } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
               $this->db->where("job_posting.level", $filterdata['joblevel']);

            } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
               //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
               $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
            }

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
               $toppicks = $filterdata['toppicks'];
               //$categories1 = explode(",", $toppicks);
               $this->db->where_in('job_toppicks.picks_id', $toppicks);
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
               $allowances = $filterdata['allowances'];
               //$categories2 = explode(",", $allowances);
               $this->db->where_in('job_allowances.allowances_id', $allowances);
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
               $medical = $filterdata['medical'];
               //$categories3 = explode(",", $medical);
               $this->db->where_in('job_medical.medical_id', $medical);
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
               $workshift = $filterdata['workshift'];
               //$categories4 = explode(",", $workshift);
               $this->db->where_in('job_workshift.workshift_id', $workshift);
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
               $leaves = $filterdata['leaves'];
               //$categories5 = explode(",", $leaves);
               $this->db->where_in('job_leaves.leaves_id', $leaves);
            }

            if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
               //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
            } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
               //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
            }

            if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
               
               if($filterdata['basicSalary'] == 5) {
                  $this->db->having('salary <=', 5000);

               } else if($filterdata['basicSalary'] == 10) {
                  $this->db->having('salary <=', 10000);
                  $this->db->having('salary >=', 5000);

               } else if($filterdata['basicSalary'] == 20) {
                  $this->db->having('salary <=', 20000);
                  $this->db->having('salary >=', 10000);

               } else if($filterdata['basicSalary'] == 30) {
                  $this->db->having('salary <=', 30000);
                  $this->db->having('salary >=', 20000);
               
               } else if($filterdata['basicSalary'] == 40) {
                  $this->db->having('salary <=', 40000);
                  $this->db->having('salary >=', 30000);
               
               } else if($filterdata['basicSalary'] == 50) {
                  $this->db->having('salary <=', 50000);
                  $this->db->having('salary >=', 40000);
               
               } else if($filterdata['basicSalary'] == 70) {
                  $this->db->having('salary <=', 70000);
                  $this->db->having('salary >=', 50000);
               
               } else if($filterdata['basicSalary'] == 80) {
                  $this->db->having('salary <=', 80000);
                  $this->db->having('salary >=', 70000);
               }
            }
            $this->db->having("distance <","7000");
            $this->db->group_by("exp_with_salary.jobpost_id");
            $query = $this->db->get();
            return $query->result_array();
         }
      }
   }


   // Actively hiring

   public function job_fetch_actively_groupby($id, $userLat, $userLong, $expfilter, $filterdata) {
      $userLat = (double)$userLat;
      $userLong = (double)$userLong;
      $cDate = date('Y-m-d');
      $uniquearraybyother = array();
      if(strlen($expfilter) > 0) {
         for($ityu = $expfilter; $ityu >-1; $ityu--) {
             $uniquearraybyother[] = $ityu;
         }
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }

      $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.actively,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, recruiter_details.latitude,recruiter_details.longitude,  (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      
      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }

      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }

      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.actively", 1);

      if($id != 0) {
          $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
          $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
      }

      if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
         $this->db->where("job_posting.category", $filterdata['jobcategory']);

      } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
         $this->db->where("job_posting.level", $filterdata['joblevel']);

      } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {

         $this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');

         //$this->db->where('MATCH("job_posting.jobtitle") AGAINT (' . $this->db->escape($filterdata['cname']) . ')');
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $this->db->where_in('job_toppicks.picks_id', $toppicks);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $this->db->where_in('job_allowances.allowances_id', $allowances);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $this->db->where_in('job_workshift.workshift_id', $workshift);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         $this->db->where_in('job_leaves.leaves_id', $leaves);
      }

      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->having('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->having('salary <=', 10000);
            $this->db->having('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->having('salary <=', 20000);
            $this->db->having('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->having('salary <=', 30000);
            $this->db->having('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->having('salary <=', 40000);
            $this->db->having('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->having('salary <=', 50000);
            $this->db->having('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->having('salary <=', 70000);
            $this->db->having('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->having('salary <=', 80000);
            $this->db->having('salary >=', 70000);
         }
      }

      $this->db->having("distance <","7000");
      $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      //var_dump($query->num_rows());die;
      if($query->num_rows() > 0) {
         return $query->result_array();
      } else {

         $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.actively,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, recruiter_details.latitude,recruiter_details.longitude,  (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
         $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
         
         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
           $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
           $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
           $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
           $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
         }

         $this->db->where("job_posting.jobexpire >=", $cDate);
         $this->db->where("job_posting.actively", 1);
         if($id != 0) {
             $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
         }
         if(strlen($expfilter) > 0) {
             $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
         }

         if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
            $this->db->where("job_posting.category", $filterdata['jobcategory']);

         } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
            $this->db->where("job_posting.level", $filterdata['joblevel']);

         } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
            $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
         }

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
            $toppicks = $filterdata['toppicks'];
            $this->db->where_in('job_toppicks.picks_id', $toppicks);
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
            $allowances = $filterdata['allowances'];
            $this->db->where_in('job_allowances.allowances_id', $allowances);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $medical = $filterdata['medical'];
            $this->db->where_in('job_medical.medical_id', $medical);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $workshift = $filterdata['workshift'];
            $this->db->where_in('job_workshift.workshift_id', $workshift);
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
            $leaves = $filterdata['leaves'];
            //$categories5 = explode(",", $leaves);
            $this->db->where_in('job_leaves.leaves_id', $leaves);
         }

         if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
            //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
         } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
            //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
         }

         if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
            
            if($filterdata['basicSalary'] == 5) {
               $this->db->having('salary <=', 5000);

            } else if($filterdata['basicSalary'] == 10) {
               $this->db->having('salary <=', 10000);
               $this->db->having('salary >=', 5000);

            } else if($filterdata['basicSalary'] == 20) {
               $this->db->having('salary <=', 20000);
               $this->db->having('salary >=', 10000);

            } else if($filterdata['basicSalary'] == 30) {
               $this->db->having('salary <=', 30000);
               $this->db->having('salary >=', 20000);
            
            } else if($filterdata['basicSalary'] == 40) {
               $this->db->having('salary <=', 40000);
               $this->db->having('salary >=', 30000);
            
            } else if($filterdata['basicSalary'] == 50) {
               $this->db->having('salary <=', 50000);
               $this->db->having('salary >=', 40000);
            
            } else if($filterdata['basicSalary'] == 70) {
               $this->db->having('salary <=', 70000);
               $this->db->having('salary >=', 50000);
            
            } else if($filterdata['basicSalary'] == 80) {
               $this->db->having('salary <=', 80000);
               $this->db->having('salary >=', 70000);
            }
         }
         $this->db->having("distance <","7000");
         $this->db->group_by("job_posting.company_id");
         $query = $this->db->get();
         if($query->num_rows() > 0) {
            return $query->result_array();
         } else {
            
            $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.actively,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, recruiter_details.latitude,recruiter_details.longitude,  (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.recruiter_id');
            $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
            $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
            
            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
              $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
              $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
              $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
              $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }

            $this->db->where("job_posting.jobexpire >=", $cDate);
            $this->db->where("job_posting.actively", 1);
            if($id != 0) {
                $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            }
            if(strlen($expfilter) > 0) {
                $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
            }

            if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
               $this->db->where("job_posting.category", $filterdata['jobcategory']);

            } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
               $this->db->where("job_posting.level", $filterdata['joblevel']);

            } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
               $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
            }

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
               $toppicks = $filterdata['toppicks'];
               $this->db->where_in('job_toppicks.picks_id', $toppicks);
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
               $allowances = $filterdata['allowances'];
               $this->db->where_in('job_allowances.allowances_id', $allowances);
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
               $medical = $filterdata['medical'];
               $this->db->where_in('job_medical.medical_id', $medical);
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
               $workshift = $filterdata['workshift'];
               $this->db->where_in('job_workshift.workshift_id', $workshift);
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
               $leaves = $filterdata['leaves'];
               //$categories5 = explode(",", $leaves);
               $this->db->where_in('job_leaves.leaves_id', $leaves);
            }

            if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
               //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
            } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
               //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
            }

            if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
               
               if($filterdata['basicSalary'] == 5) {
                  $this->db->having('salary <=', 5000);

               } else if($filterdata['basicSalary'] == 10) {
                  $this->db->having('salary <=', 10000);
                  $this->db->having('salary >=', 5000);

               } else if($filterdata['basicSalary'] == 20) {
                  $this->db->having('salary <=', 20000);
                  $this->db->having('salary >=', 10000);

               } else if($filterdata['basicSalary'] == 30) {
                  $this->db->having('salary <=', 30000);
                  $this->db->having('salary >=', 20000);
               
               } else if($filterdata['basicSalary'] == 40) {
                  $this->db->having('salary <=', 40000);
                  $this->db->having('salary >=', 30000);
               
               } else if($filterdata['basicSalary'] == 50) {
                  $this->db->having('salary <=', 50000);
                  $this->db->having('salary >=', 40000);
               
               } else if($filterdata['basicSalary'] == 70) {
                  $this->db->having('salary <=', 70000);
                  $this->db->having('salary >=', 50000);
               
               } else if($filterdata['basicSalary'] == 80) {
                  $this->db->having('salary <=', 80000);
                  $this->db->having('salary >=', 70000);
               }
            }
            $this->db->having("distance <","7000");
            $this->db->group_by("job_posting.recruiter_id");
            $query = $this->db->get();
            return $query->result_array();
         }

      }
   }

   public function activelyjob_fetch_latlong($id, $userLat, $userLong, $expfilter, $filterdata) {
      $userLat = (double)$userLat;
      $userLong = (double)$userLong;
      $cDate = date('Y-m-d');
      $uniquearraybyother = array();
      for($ityu = $expfilter; $ityu >-1; $ityu--) {
         $uniquearraybyother[] = $ityu;
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }

      $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.actively,job_posting.jobPitch,job_posting.mode, job_posting.modeurl, job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id,job_posting.boost_status, recruiter_details.latitude,recruiter_details.longitude, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }
      
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.actively", 1);
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
      }

      if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
         $this->db->where("job_posting.category", $filterdata['jobcategory']);

      } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
         $this->db->where("job_posting.level", $filterdata['joblevel']);

      } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         $this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
         //$this->db->or_like("recruiter.cname", $filterdata['cname'], 'both');
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         //$categories1 = explode(",", $toppicks);
         $this->db->where_in('job_toppicks.picks_id', $toppicks);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         //$categories2 = explode(",", $allowances);
         $this->db->where_in('job_allowances.allowances_id', $allowances);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         //$categories3 = explode(",", $medical);
         $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         //$categories4 = explode(",", $workshift);
         $this->db->where_in('job_workshift.workshift_id', $workshift);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         //$categories5 = explode(",", $leaves);
         $this->db->where_in('job_leaves.leaves_id', $leaves);
      }

      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->having('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->having('salary <=', 10000);
            $this->db->having('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->having('salary <=', 20000);
            $this->db->having('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->having('salary <=', 30000);
            $this->db->having('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->having('salary <=', 40000);
            $this->db->having('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->having('salary <=', 50000);
            $this->db->having('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->having('salary <=', 70000);
            $this->db->having('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->having('salary <=', 80000);
            $this->db->having('salary >=', 70000);
         }
      }
      $this->db->having("distance <","7000");
      $this->db->group_by("exp_with_salary.jobpost_id");

      $query = $this->db->get();
      if($query->num_rows() > 0) {

         return $query->result_array();
      } else {

         $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.actively,job_posting.jobPitch,job_posting.mode, job_posting.modeurl, job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id,job_posting.boost_status, recruiter_details.latitude,recruiter_details.longitude, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      
         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
         $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
           $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
           $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
           $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
           $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
         }
         
         $this->db->where("job_posting.jobexpire >=", $cDate);
         $this->db->where("job_posting.actively", 1);
         if($id!=0) {
            $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
         }
         if(strlen($expfilter) > 0) {
            $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
         }

         if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
            $this->db->where("job_posting.category", $filterdata['jobcategory']);

         } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
            $this->db->where("job_posting.level", $filterdata['joblevel']);

         } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
            //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
            $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
         }

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
            $toppicks = $filterdata['toppicks'];
            //$categories1 = explode(",", $toppicks);
            $this->db->where_in('job_toppicks.picks_id', $toppicks);
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
            $allowances = $filterdata['allowances'];
            //$categories2 = explode(",", $allowances);
            $this->db->where_in('job_allowances.allowances_id', $allowances);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $medical = $filterdata['medical'];
            //$categories3 = explode(",", $medical);
            $this->db->where_in('job_medical.medical_id', $medical);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $workshift = $filterdata['workshift'];
            //$categories4 = explode(",", $workshift);
            $this->db->where_in('job_workshift.workshift_id', $workshift);
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
            $leaves = $filterdata['leaves'];
            //$categories5 = explode(",", $leaves);
            $this->db->where_in('job_leaves.leaves_id', $leaves);
         }

         if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
            //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
         } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
            //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
         }

         if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
            
            if($filterdata['basicSalary'] == 5) {
               $this->db->having('salary <=', 5000);

            } else if($filterdata['basicSalary'] == 10) {
               $this->db->having('salary <=', 10000);
               $this->db->having('salary >=', 5000);

            } else if($filterdata['basicSalary'] == 20) {
               $this->db->having('salary <=', 20000);
               $this->db->having('salary >=', 10000);

            } else if($filterdata['basicSalary'] == 30) {
               $this->db->having('salary <=', 30000);
               $this->db->having('salary >=', 20000);
            
            } else if($filterdata['basicSalary'] == 40) {
               $this->db->having('salary <=', 40000);
               $this->db->having('salary >=', 30000);
            
            } else if($filterdata['basicSalary'] == 50) {
               $this->db->having('salary <=', 50000);
               $this->db->having('salary >=', 40000);
            
            } else if($filterdata['basicSalary'] == 70) {
               $this->db->having('salary <=', 70000);
               $this->db->having('salary >=', 50000);
            
            } else if($filterdata['basicSalary'] == 80) {
               $this->db->having('salary <=', 80000);
               $this->db->having('salary >=', 70000);
            }
         }
         $this->db->having("distance <","7000");
         $this->db->group_by("exp_with_salary.jobpost_id");
         $query = $this->db->get();

         if($query->num_rows() > 0) {
            return $query->result_array();
         } else {

            $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.actively,job_posting.jobPitch,job_posting.mode, job_posting.modeurl, job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id,job_posting.boost_status, recruiter_details.latitude,recruiter_details.longitude, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      
            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.recruiter_id');
            $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
            $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
              $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
              $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
              $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
              $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }
            
            $this->db->where("job_posting.jobexpire >=", $cDate);
            $this->db->where("job_posting.actively", 1);
            if($id!=0) {
               $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            }
            if(strlen($expfilter) > 0) {
               $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
            }

            if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
               $this->db->where("job_posting.category", $filterdata['jobcategory']);

            } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
               $this->db->where("job_posting.level", $filterdata['joblevel']);

            } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
               //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
               $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
            }

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
               $toppicks = $filterdata['toppicks'];
               //$categories1 = explode(",", $toppicks);
               $this->db->where_in('job_toppicks.picks_id', $toppicks);
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
               $allowances = $filterdata['allowances'];
               //$categories2 = explode(",", $allowances);
               $this->db->where_in('job_allowances.allowances_id', $allowances);
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
               $medical = $filterdata['medical'];
               //$categories3 = explode(",", $medical);
               $this->db->where_in('job_medical.medical_id', $medical);
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
               $workshift = $filterdata['workshift'];
               //$categories4 = explode(",", $workshift);
               $this->db->where_in('job_workshift.workshift_id', $workshift);
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
               $leaves = $filterdata['leaves'];
               //$categories5 = explode(",", $leaves);
               $this->db->where_in('job_leaves.leaves_id', $leaves);
            }

            if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
               //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
            } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
               //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
            }

            if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
               
               if($filterdata['basicSalary'] == 5) {
                  $this->db->having('salary <=', 5000);

               } else if($filterdata['basicSalary'] == 10) {
                  $this->db->having('salary <=', 10000);
                  $this->db->having('salary >=', 5000);

               } else if($filterdata['basicSalary'] == 20) {
                  $this->db->having('salary <=', 20000);
                  $this->db->having('salary >=', 10000);

               } else if($filterdata['basicSalary'] == 30) {
                  $this->db->having('salary <=', 30000);
                  $this->db->having('salary >=', 20000);
               
               } else if($filterdata['basicSalary'] == 40) {
                  $this->db->having('salary <=', 40000);
                  $this->db->having('salary >=', 30000);
               
               } else if($filterdata['basicSalary'] == 50) {
                  $this->db->having('salary <=', 50000);
                  $this->db->having('salary >=', 40000);
               
               } else if($filterdata['basicSalary'] == 70) {
                  $this->db->having('salary <=', 70000);
                  $this->db->having('salary >=', 50000);
               
               } else if($filterdata['basicSalary'] == 80) {
                  $this->db->having('salary <=', 80000);
                  $this->db->having('salary >=', 70000);
               }
            }
            $this->db->having("distance <","7000");
            $this->db->group_by("exp_with_salary.jobpost_id");
            $query = $this->db->get();
            return $query->result_array();
         }
      }
   }


   // Toppicks view all list

   public function feturedtoppicks_fetch_all_groupby($id, $expfilter, $userLat, $userLong, $getParamType, $filterdata) {

      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire,job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');

      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }

      $this->db->where("job_posting.jobexpire >=", $cDate);
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }

      if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
         $this->db->where("job_posting.category", $filterdata['jobcategory']);

      } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
         $this->db->where("job_posting.level", $filterdata['joblevel']);

      } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         $this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
         //$this->db->or_like("recruiter.cname", $filterdata['cname'], 'both');
      }

      if(isset($filterdata['toppicks']) && count($filterdata['toppicks']) > 0) {
         $toppicks = $filterdata['toppicks'];
         //$categories1 = explode(",", $toppicks);
         $this->db->where_in('job_toppicks.picks_id', $toppicks);
      } else {
         $this->db->where("job_toppicks.picks_id",$getParamType);
      }

      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $this->db->where_in('job_allowances.allowances_id', $allowances);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $this->db->where_in('job_workshift.workshift_id', $workshift);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         $this->db->where_in('job_leaves.leaves_id', $leaves);
      }

      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->having('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->having('salary <=', 10000);
            $this->db->having('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->having('salary <=', 20000);
            $this->db->having('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->having('salary <=', 30000);
            $this->db->having('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->having('salary <=', 40000);
            $this->db->having('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->having('salary <=', 50000);
            $this->db->having('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->having('salary <=', 70000);
            $this->db->having('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->having('salary <=', 80000);
            $this->db->having('salary >=', 70000);
         }
      }
      $this->db->having("distance<", '7000');
      $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      if($query->num_rows() > 0) {
         return $query->result_array();
      } else {

         $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
         $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
         $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');

         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
           $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
           $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
           $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
         }

         $this->db->where("job_posting.jobexpire >=", $cDate);
         if($id!=0) {
            $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
         }
         if(strlen($expfilter) > 0){
            $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
         }

         if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
            $this->db->where("job_posting.category", $filterdata['jobcategory']);

         } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
            $this->db->where("job_posting.level", $filterdata['joblevel']);

         } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
            //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
            $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
         }

         if(isset($filterdata['toppicks']) && count($filterdata['toppicks']) > 0) {
            $toppicks = $filterdata['toppicks'];
            //$categories1 = explode(",", $toppicks);
            $this->db->where_in('job_toppicks.picks_id', $toppicks);
         } else {
            $this->db->where("job_toppicks.picks_id",$getParamType);
         }

         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
            $allowances = $filterdata['allowances'];
            $this->db->where_in('job_allowances.allowances_id', $allowances);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $medical = $filterdata['medical'];
            $this->db->where_in('job_medical.medical_id', $medical);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $workshift = $filterdata['workshift'];
            $this->db->where_in('job_workshift.workshift_id', $workshift);
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
            $leaves = $filterdata['leaves'];
            $this->db->where_in('job_leaves.leaves_id', $leaves);
         }

         if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
            //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
         } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
            //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
         }

         if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
            
            if($filterdata['basicSalary'] == 5) {
               $this->db->having('salary <=', 5000);

            } else if($filterdata['basicSalary'] == 10) {
               $this->db->having('salary <=', 10000);
               $this->db->having('salary >=', 5000);

            } else if($filterdata['basicSalary'] == 20) {
               $this->db->having('salary <=', 20000);
               $this->db->having('salary >=', 10000);

            } else if($filterdata['basicSalary'] == 30) {
               $this->db->having('salary <=', 30000);
               $this->db->having('salary >=', 20000);
            
            } else if($filterdata['basicSalary'] == 40) {
               $this->db->having('salary <=', 40000);
               $this->db->having('salary >=', 30000);
            
            } else if($filterdata['basicSalary'] == 50) {
               $this->db->having('salary <=', 50000);
               $this->db->having('salary >=', 40000);
            
            } else if($filterdata['basicSalary'] == 70) {
               $this->db->having('salary <=', 70000);
               $this->db->having('salary >=', 50000);
            
            } else if($filterdata['basicSalary'] == 80) {
               $this->db->having('salary <=', 80000);
               $this->db->having('salary >=', 70000);
            }
         }
         $this->db->having("distance<", '7000');
         $this->db->group_by("job_posting.company_id");
         $query = $this->db->get();
         if($query->num_rows() > 0) {
            return $query->result_array();
         } else {

            $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'job_posting.recruiter_id = recruiter_details.recruiter_id');
            $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
            $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
            $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');

            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
              $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
              $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
              $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }

            $this->db->where("job_posting.jobexpire >=", $cDate);
            if($id!=0) {
               $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            }
            if(strlen($expfilter) > 0){
               $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
            }

            if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
               $this->db->where("job_posting.category", $filterdata['jobcategory']);

            } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
               $this->db->where("job_posting.level", $filterdata['joblevel']);

            } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
               //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
               $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
            }

            if(isset($filterdata['toppicks']) && count($filterdata['toppicks']) > 0) {
               $toppicks = $filterdata['toppicks'];
               //$categories1 = explode(",", $toppicks);
               $this->db->where_in('job_toppicks.picks_id', $toppicks);
            } else {
               $this->db->where("job_toppicks.picks_id",$getParamType);
            }

            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
               $allowances = $filterdata['allowances'];
               $this->db->where_in('job_allowances.allowances_id', $allowances);
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
               $medical = $filterdata['medical'];
               $this->db->where_in('job_medical.medical_id', $medical);
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
               $workshift = $filterdata['workshift'];
               $this->db->where_in('job_workshift.workshift_id', $workshift);
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
               $leaves = $filterdata['leaves'];
               $this->db->where_in('job_leaves.leaves_id', $leaves);
            }

            if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
               //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
            } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
               //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
            }

            if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
               
               if($filterdata['basicSalary'] == 5) {
                  $this->db->having('salary <=', 5000);

               } else if($filterdata['basicSalary'] == 10) {
                  $this->db->having('salary <=', 10000);
                  $this->db->having('salary >=', 5000);

               } else if($filterdata['basicSalary'] == 20) {
                  $this->db->having('salary <=', 20000);
                  $this->db->having('salary >=', 10000);

               } else if($filterdata['basicSalary'] == 30) {
                  $this->db->having('salary <=', 30000);
                  $this->db->having('salary >=', 20000);
               
               } else if($filterdata['basicSalary'] == 40) {
                  $this->db->having('salary <=', 40000);
                  $this->db->having('salary >=', 30000);
               
               } else if($filterdata['basicSalary'] == 50) {
                  $this->db->having('salary <=', 50000);
                  $this->db->having('salary >=', 40000);
               
               } else if($filterdata['basicSalary'] == 70) {
                  $this->db->having('salary <=', 70000);
                  $this->db->having('salary >=', 50000);
               
               } else if($filterdata['basicSalary'] == 80) {
                  $this->db->having('salary <=', 80000);
                  $this->db->having('salary >=', 70000);
               }
            }
            $this->db->having("distance<", '7000');
            $this->db->group_by("job_posting.recruiter_id");
            $query = $this->db->get();
            return $query->result_array();
         }
      }
   }

   public function feturedtoppicks_fetch_latlong($id, $expfilter, $userLat, $userLong, $getParamType, $filterdata) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();

      for($ikl = 0;$ikl<=$expfilter;$ikl++) {
         $uniquerarray[] = $ikl;
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire,job_posting.actively, job_posting.mode, job_posting.modeurl, job_posting.chatbot,recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }

      $this->db->where("job_posting.jobexpire >=", $cDate);
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }

      if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
         $this->db->where("job_posting.category", $filterdata['jobcategory']);

      } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
         $this->db->where("job_posting.level", $filterdata['joblevel']);

      } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         $this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
         //$this->db->or_like("recruiter.cname", $filterdata['cname'], 'both');
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $this->db->where_in('job_toppicks.picks_id', $toppicks);
      } else {
         $this->db->where("job_toppicks.picks_id",$getParamType);
      }

      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $this->db->where_in('job_allowances.allowances_id', $allowances);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $this->db->where_in('job_workshift.workshift_id', $workshift);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         $this->db->where_in('job_leaves.leaves_id', $leaves);
      }

      if(isset($filterdata['language']) && !empty($filterdata['language'])) {
         $this->db->where('job_posting.language', $filterdata['language']);
      }
      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }
      
      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->having('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->having('salary <=', 10000);
            $this->db->having('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->having('salary <=', 20000);
            $this->db->having('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->having('salary <=', 30000);
            $this->db->having('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->having('salary <=', 40000);
            $this->db->having('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->having('salary <=', 50000);
            $this->db->having('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->having('salary <=', 70000);
            $this->db->having('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->having('salary <=', 80000);
            $this->db->having('salary >=', 70000);
         }
      }

      $this->db->having("distance<", '7000');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      if($query->num_rows() > 0) {

         return $query->result_array();
      
      } else {

         $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire,job_posting.actively,job_posting.mode, job_posting.modeurl, job_posting.chatbot,recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
         $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
         $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
           $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
           $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
           $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
         }

         $this->db->where("job_posting.jobexpire >=", $cDate);
         if($id!=0) {
            $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
         }
         if(strlen($expfilter) > 0){
            $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
         }

         if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
            $this->db->where("job_posting.category", $filterdata['jobcategory']);

         } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
            $this->db->where("job_posting.level", $filterdata['joblevel']);

         } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
            //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
            $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
         }

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
            $toppicks = $filterdata['toppicks'];
            $this->db->where_in('job_toppicks.picks_id', $toppicks);
         } else {
            $this->db->where("job_toppicks.picks_id",$getParamType);
         }

         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
            $allowances = $filterdata['allowances'];
            $this->db->where_in('job_allowances.allowances_id', $allowances);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $medical = $filterdata['medical'];
            $this->db->where_in('job_medical.medical_id', $medical);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $workshift = $filterdata['workshift'];
            $this->db->where_in('job_workshift.workshift_id', $workshift);
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
            $leaves = $filterdata['leaves'];
            $this->db->where_in('job_leaves.leaves_id', $leaves);
         }

         if(isset($filterdata['language']) && !empty($filterdata['language'])) {
            $this->db->where('job_posting.language', $filterdata['language']);
         }
         if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
            //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
         } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
            //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
         }
         
         if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
            
            if($filterdata['basicSalary'] == 5) {
               $this->db->having('salary <=', 5000);

            } else if($filterdata['basicSalary'] == 10) {
               $this->db->having('salary <=', 10000);
               $this->db->having('salary >=', 5000);

            } else if($filterdata['basicSalary'] == 20) {
               $this->db->having('salary <=', 20000);
               $this->db->having('salary >=', 10000);

            } else if($filterdata['basicSalary'] == 30) {
               $this->db->having('salary <=', 30000);
               $this->db->having('salary >=', 20000);
            
            } else if($filterdata['basicSalary'] == 40) {
               $this->db->having('salary <=', 40000);
               $this->db->having('salary >=', 30000);
            
            } else if($filterdata['basicSalary'] == 50) {
               $this->db->having('salary <=', 50000);
               $this->db->having('salary >=', 40000);
            
            } else if($filterdata['basicSalary'] == 70) {
               $this->db->having('salary <=', 70000);
               $this->db->having('salary >=', 50000);
            
            } else if($filterdata['basicSalary'] == 80) {
               $this->db->having('salary <=', 80000);
               $this->db->having('salary >=', 70000);
            }
         }

         $this->db->having("distance<", '7000');
         $this->db->group_by("exp_with_salary.jobpost_id");
         $query = $this->db->get();
         if($query->num_rows() > 0) {
            return $query->result_array();
         
         } else {

            $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire,job_posting.actively,job_posting.mode, job_posting.modeurl, job_posting.chatbot,recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'job_posting.recruiter_id = recruiter_details.recruiter_id');
            $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
            $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
            $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
            
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
              $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
              $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
              $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }

            $this->db->where("job_posting.jobexpire >=", $cDate);
            if($id!=0) {
               $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            }
            if(strlen($expfilter) > 0){
               $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
            }

            if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
               $this->db->where("job_posting.category", $filterdata['jobcategory']);

            } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
               $this->db->where("job_posting.level", $filterdata['joblevel']);

            } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
               //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
               $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
            }

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
               $toppicks = $filterdata['toppicks'];
               $this->db->where_in('job_toppicks.picks_id', $toppicks);
            } else {
               $this->db->where("job_toppicks.picks_id",$getParamType);
            }

            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
               $allowances = $filterdata['allowances'];
               $this->db->where_in('job_allowances.allowances_id', $allowances);
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
               $medical = $filterdata['medical'];
               $this->db->where_in('job_medical.medical_id', $medical);
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
               $workshift = $filterdata['workshift'];
               $this->db->where_in('job_workshift.workshift_id', $workshift);
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
               $leaves = $filterdata['leaves'];
               $this->db->where_in('job_leaves.leaves_id', $leaves);
            }

            if(isset($filterdata['language']) && !empty($filterdata['language'])) {
               $this->db->where('job_posting.language', $filterdata['language']);
            }
            if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
               //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
            } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
               //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
            }
            
            if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
               
               if($filterdata['basicSalary'] == 5) {
                  $this->db->having('salary <=', 5000);

               } else if($filterdata['basicSalary'] == 10) {
                  $this->db->having('salary <=', 10000);
                  $this->db->having('salary >=', 5000);

               } else if($filterdata['basicSalary'] == 20) {
                  $this->db->having('salary <=', 20000);
                  $this->db->having('salary >=', 10000);

               } else if($filterdata['basicSalary'] == 30) {
                  $this->db->having('salary <=', 30000);
                  $this->db->having('salary >=', 20000);
               
               } else if($filterdata['basicSalary'] == 40) {
                  $this->db->having('salary <=', 40000);
                  $this->db->having('salary >=', 30000);
               
               } else if($filterdata['basicSalary'] == 50) {
                  $this->db->having('salary <=', 50000);
                  $this->db->having('salary >=', 40000);
               
               } else if($filterdata['basicSalary'] == 70) {
                  $this->db->having('salary <=', 70000);
                  $this->db->having('salary >=', 50000);
               
               } else if($filterdata['basicSalary'] == 80) {
                  $this->db->having('salary <=', 80000);
                  $this->db->having('salary >=', 70000);
               }
            }

            $this->db->having("distance<", '7000');
            $this->db->group_by("exp_with_salary.jobpost_id");
            $query = $this->db->get();
            return $query->result_array();
         }
      }
   }


   // Allowances view all list

   public function feturedtoppicks_fetch_latlong_allowances_groupby($id, $expfilter, $userLat, $userLong, $filterdata) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_allowances.allowances_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }

      $this->db->where("job_posting.jobexpire >=", $cDate);
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }

      if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
         $this->db->where("job_posting.category", $filterdata['jobcategory']);

      } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
         $this->db->where("job_posting.level", $filterdata['joblevel']);

      } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         $this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
         //$this->db->or_like("recruiter.cname", $filterdata['cname'], 'both');
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $this->db->where_in('job_toppicks.picks_id', $toppicks);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $this->db->where_in('job_workshift.workshift_id', $workshift);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         $this->db->where_in('job_leaves.leaves_id', $leaves);
      }

      $this->db->where("job_allowances.allowances_id",7);

      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         if($filterdata['basicSalary'] == 5) {
            $this->db->having('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->having('salary <=', 10000);
            $this->db->having('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->having('salary <=', 20000);
            $this->db->having('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->having('salary <=', 30000);
            $this->db->having('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->having('salary <=', 40000);
            $this->db->having('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->having('salary <=', 50000);
            $this->db->having('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->having('salary <=', 70000);
            $this->db->having('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->having('salary <=', 80000);
            $this->db->having('salary >=', 70000);
         }
      }

      $this->db->having("distance<", '7000');
      $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      if($query->num_rows() > 0) {
         return $query->result_array();
      } else {

         $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire,job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_allowances.allowances_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
         $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
         $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
           $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
           $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
           $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
         }

         $this->db->where("job_posting.jobexpire >=", $cDate);
         if($id!=0) {
            $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
         }
         if(strlen($expfilter) > 0){
            $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
         }

         if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
            $this->db->where("job_posting.category", $filterdata['jobcategory']);

         } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
            $this->db->where("job_posting.level", $filterdata['joblevel']);

         } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
            //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
            $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
         }

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
            $toppicks = $filterdata['toppicks'];
            $this->db->where_in('job_toppicks.picks_id', $toppicks);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $medical = $filterdata['medical'];
            $this->db->where_in('job_medical.medical_id', $medical);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $workshift = $filterdata['workshift'];
            $this->db->where_in('job_workshift.workshift_id', $workshift);
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
            $leaves = $filterdata['leaves'];
            $this->db->where_in('job_leaves.leaves_id', $leaves);
         }

         $this->db->where("job_allowances.allowances_id",7);

         if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
            //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
         } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
            //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
         }

         if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
            if($filterdata['basicSalary'] == 5) {
               $this->db->having('salary <=', 5000);

            } else if($filterdata['basicSalary'] == 10) {
               $this->db->having('salary <=', 10000);
               $this->db->having('salary >=', 5000);

            } else if($filterdata['basicSalary'] == 20) {
               $this->db->having('salary <=', 20000);
               $this->db->having('salary >=', 10000);

            } else if($filterdata['basicSalary'] == 30) {
               $this->db->having('salary <=', 30000);
               $this->db->having('salary >=', 20000);
            
            } else if($filterdata['basicSalary'] == 40) {
               $this->db->having('salary <=', 40000);
               $this->db->having('salary >=', 30000);
            
            } else if($filterdata['basicSalary'] == 50) {
               $this->db->having('salary <=', 50000);
               $this->db->having('salary >=', 40000);
            
            } else if($filterdata['basicSalary'] == 70) {
               $this->db->having('salary <=', 70000);
               $this->db->having('salary >=', 50000);
            
            } else if($filterdata['basicSalary'] == 80) {
               $this->db->having('salary <=', 80000);
               $this->db->having('salary >=', 70000);
            }
         }

         $this->db->having("distance<", '7000');
         $this->db->group_by("job_posting.company_id");
         $query = $this->db->get();
         if($query->num_rows() > 0) {
            return $query->result_array();
         } else {

            $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_allowances.allowances_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'job_posting.recruiter_id = recruiter_details.recruiter_id');
            $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
            $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
            $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
              $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
              $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
              $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }

            $this->db->where("job_posting.jobexpire >=", $cDate);
            if($id!=0) {
               $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            }
            if(strlen($expfilter) > 0){
               $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
            }

            if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
               $this->db->where("job_posting.category", $filterdata['jobcategory']);

            } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
               $this->db->where("job_posting.level", $filterdata['joblevel']);

            } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
               //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
               $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
            }

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
               $toppicks = $filterdata['toppicks'];
               $this->db->where_in('job_toppicks.picks_id', $toppicks);
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
               $medical = $filterdata['medical'];
               $this->db->where_in('job_medical.medical_id', $medical);
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
               $workshift = $filterdata['workshift'];
               $this->db->where_in('job_workshift.workshift_id', $workshift);
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
               $leaves = $filterdata['leaves'];
               $this->db->where_in('job_leaves.leaves_id', $leaves);
            }

            $this->db->where("job_allowances.allowances_id",7);

            if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
               //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
            } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
               //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
            }

            if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
               if($filterdata['basicSalary'] == 5) {
                  $this->db->having('salary <=', 5000);

               } else if($filterdata['basicSalary'] == 10) {
                  $this->db->having('salary <=', 10000);
                  $this->db->having('salary >=', 5000);

               } else if($filterdata['basicSalary'] == 20) {
                  $this->db->having('salary <=', 20000);
                  $this->db->having('salary >=', 10000);

               } else if($filterdata['basicSalary'] == 30) {
                  $this->db->having('salary <=', 30000);
                  $this->db->having('salary >=', 20000);
               
               } else if($filterdata['basicSalary'] == 40) {
                  $this->db->having('salary <=', 40000);
                  $this->db->having('salary >=', 30000);
               
               } else if($filterdata['basicSalary'] == 50) {
                  $this->db->having('salary <=', 50000);
                  $this->db->having('salary >=', 40000);
               
               } else if($filterdata['basicSalary'] == 70) {
                  $this->db->having('salary <=', 70000);
                  $this->db->having('salary >=', 50000);
               
               } else if($filterdata['basicSalary'] == 80) {
                  $this->db->having('salary <=', 80000);
                  $this->db->having('salary >=', 70000);
               }
            }

            $this->db->having("distance<", '7000');
            $this->db->group_by("job_posting.recruiter_id");
            $query = $this->db->get();
            return $query->result_array();
         }
      }
   }

   public function feturedtoppicksallowances_fetch_latlong($id, $expfilter, $userLat, $userLong, $getParamType, $filterdata) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();

      for($ikl = 0;$ikl<=$expfilter;$ikl++) {
         $uniquerarray[] = $ikl;
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire,job_posting.actively, job_posting.mode, job_posting.modeurl, job_posting.chatbot,recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      
      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }

      $this->db->where("job_posting.jobexpire >=", $cDate);
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }

      if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
         $this->db->where("job_posting.category", $filterdata['jobcategory']);

      } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
         $this->db->where("job_posting.level", $filterdata['joblevel']);

      } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         $this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
         //$this->db->or_like("recruiter.cname", $filterdata['cname'], 'both');
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $this->db->where_in('job_toppicks.picks_id', $toppicks);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $this->db->where_in('job_workshift.workshift_id', $workshift);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         $this->db->where_in('job_leaves.leaves_id', $leaves);
      }

      $this->db->where("job_allowances.allowances_id",7);

      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }
      
      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->having('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->having('salary <=', 10000);
            $this->db->having('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->having('salary <=', 20000);
            $this->db->having('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->having('salary <=', 30000);
            $this->db->having('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->having('salary <=', 40000);
            $this->db->having('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->having('salary <=', 50000);
            $this->db->having('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->having('salary <=', 70000);
            $this->db->having('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->having('salary <=', 80000);
            $this->db->having('salary >=', 70000);
         }
      }

      $this->db->having("distance<", '7000');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      if($query->num_rows() > 0) {
         return $query->result_array();
      } else {

         $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire,job_posting.actively, job_posting.mode, job_posting.modeurl, job_posting.chatbot,recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
         $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
         $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         
         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
           $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
           $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
           $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
         }

         $this->db->where("job_posting.jobexpire >=", $cDate);
         if($id!=0) {
            $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
         }
         if(strlen($expfilter) > 0){
            $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
         }

         if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
            $this->db->where("job_posting.category", $filterdata['jobcategory']);

         } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
            $this->db->where("job_posting.level", $filterdata['joblevel']);

         } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
            //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
            $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
         }

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
            $toppicks = $filterdata['toppicks'];
            $this->db->where_in('job_toppicks.picks_id', $toppicks);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $medical = $filterdata['medical'];
            $this->db->where_in('job_medical.medical_id', $medical);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $workshift = $filterdata['workshift'];
            $this->db->where_in('job_workshift.workshift_id', $workshift);
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
            $leaves = $filterdata['leaves'];
            $this->db->where_in('job_leaves.leaves_id', $leaves);
         }

         $this->db->where("job_allowances.allowances_id",7);

         if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
            //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
         } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
            //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
         }
         
         if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
            
            if($filterdata['basicSalary'] == 5) {
               $this->db->having('salary <=', 5000);

            } else if($filterdata['basicSalary'] == 10) {
               $this->db->having('salary <=', 10000);
               $this->db->having('salary >=', 5000);

            } else if($filterdata['basicSalary'] == 20) {
               $this->db->having('salary <=', 20000);
               $this->db->having('salary >=', 10000);

            } else if($filterdata['basicSalary'] == 30) {
               $this->db->having('salary <=', 30000);
               $this->db->having('salary >=', 20000);
            
            } else if($filterdata['basicSalary'] == 40) {
               $this->db->having('salary <=', 40000);
               $this->db->having('salary >=', 30000);
            
            } else if($filterdata['basicSalary'] == 50) {
               $this->db->having('salary <=', 50000);
               $this->db->having('salary >=', 40000);
            
            } else if($filterdata['basicSalary'] == 70) {
               $this->db->having('salary <=', 70000);
               $this->db->having('salary >=', 50000);
            
            } else if($filterdata['basicSalary'] == 80) {
               $this->db->having('salary <=', 80000);
               $this->db->having('salary >=', 70000);
            }
         }

         $this->db->having("distance<", '7000');
         $this->db->group_by("exp_with_salary.jobpost_id");
         $query = $this->db->get();
         if($query->num_rows() > 0) {
            return $query->result_array();
         } else {

            $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire,job_posting.actively,job_posting.mode, job_posting.modeurl, job_posting.chatbot,recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'job_posting.recruiter_id = recruiter_details.recruiter_id');
            $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
            $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
            $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
            
            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
              $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
              $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
              $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }

            $this->db->where("job_posting.jobexpire >=", $cDate);
            if($id!=0) {
               $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            }
            if(strlen($expfilter) > 0){
               $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
            }

            if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
               $this->db->where("job_posting.category", $filterdata['jobcategory']);

            } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
               $this->db->where("job_posting.level", $filterdata['joblevel']);

            } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
               //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
               $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
            }

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
               $toppicks = $filterdata['toppicks'];
               $this->db->where_in('job_toppicks.picks_id', $toppicks);
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
               $medical = $filterdata['medical'];
               $this->db->where_in('job_medical.medical_id', $medical);
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
               $workshift = $filterdata['workshift'];
               $this->db->where_in('job_workshift.workshift_id', $workshift);
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
               $leaves = $filterdata['leaves'];
               $this->db->where_in('job_leaves.leaves_id', $leaves);
            }

            $this->db->where("job_allowances.allowances_id",7);

            if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
               //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
            } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
               //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
            }
            
            if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
               
               if($filterdata['basicSalary'] == 5) {
                  $this->db->having('salary <=', 5000);

               } else if($filterdata['basicSalary'] == 10) {
                  $this->db->having('salary <=', 10000);
                  $this->db->having('salary >=', 5000);

               } else if($filterdata['basicSalary'] == 20) {
                  $this->db->having('salary <=', 20000);
                  $this->db->having('salary >=', 10000);

               } else if($filterdata['basicSalary'] == 30) {
                  $this->db->having('salary <=', 30000);
                  $this->db->having('salary >=', 20000);
               
               } else if($filterdata['basicSalary'] == 40) {
                  $this->db->having('salary <=', 40000);
                  $this->db->having('salary >=', 30000);
               
               } else if($filterdata['basicSalary'] == 50) {
                  $this->db->having('salary <=', 50000);
                  $this->db->having('salary >=', 40000);
               
               } else if($filterdata['basicSalary'] == 70) {
                  $this->db->having('salary <=', 70000);
                  $this->db->having('salary >=', 50000);
               
               } else if($filterdata['basicSalary'] == 80) {
                  $this->db->having('salary <=', 80000);
                  $this->db->having('salary >=', 70000);
               }
            }

            $this->db->having("distance<", '7000');
            $this->db->group_by("exp_with_salary.jobpost_id");
            $query = $this->db->get();
            return $query->result_array();
         }
      }
   }


   // Leadership job listing

   public function catjob_fetch_leadership($id,$expfilter, $userLat, $userLong, $filterdata) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

      if(isset($filterdata['allowancestoppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }

      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.category", 24);
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }

      if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
         $this->db->where("job_posting.category", $filterdata['jobcategory']);

      } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
         $this->db->where("job_posting.level", $filterdata['joblevel']);

      } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         $this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
         //$this->db->or_like("recruiter.cname", $filterdata['cname'], 'both');
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $this->db->where_in('job_toppicks.picks_id', $toppicks);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $this->db->where_in('job_allowances.allowances_id', $allowances);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $this->db->where_in('job_workshift.workshift_id', $workshift);
      }

      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->having('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->having('salary <=', 10000);
            $this->db->having('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->having('salary <=', 20000);
            $this->db->having('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->having('salary <=', 30000);
            $this->db->having('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->having('salary <=', 40000);
            $this->db->having('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->having('salary <=', 50000);
            $this->db->having('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->having('salary <=', 70000);
            $this->db->having('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->having('salary <=', 80000);
            $this->db->having('salary >=', 70000);
         }
      }

      $this->db->having("distance<", '7000');
      $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      if($query->num_rows() > 0) {
         return $query->result_array();
      } else {

         $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
         $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

         if(isset($filterdata['allowancestoppicks']) && !empty($filterdata['toppicks'])) {
           $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
           $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
           $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
           $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }

         $this->db->where("job_posting.jobexpire >=", $cDate);
         $this->db->where("job_posting.category", 24);
         if($id!=0) {
            $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
         }
         if(strlen($expfilter) > 0) {
            $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
         }

         if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
            $this->db->where("job_posting.category", $filterdata['jobcategory']);

         } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
            $this->db->where("job_posting.level", $filterdata['joblevel']);

         } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
            //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
            $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
         }

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
            $toppicks = $filterdata['toppicks'];
            $this->db->where_in('job_toppicks.picks_id', $toppicks);
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
            $allowances = $filterdata['allowances'];
            $this->db->where_in('job_allowances.allowances_id', $allowances);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $medical = $filterdata['medical'];
            $this->db->where_in('job_medical.medical_id', $medical);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $workshift = $filterdata['workshift'];
            $this->db->where_in('job_workshift.workshift_id', $workshift);
         }

         if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
            //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
         } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
            //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
         }

         if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
            
            if($filterdata['basicSalary'] == 5) {
               $this->db->having('salary <=', 5000);

            } else if($filterdata['basicSalary'] == 10) {
               $this->db->having('salary <=', 10000);
               $this->db->having('salary >=', 5000);

            } else if($filterdata['basicSalary'] == 20) {
               $this->db->having('salary <=', 20000);
               $this->db->having('salary >=', 10000);

            } else if($filterdata['basicSalary'] == 30) {
               $this->db->having('salary <=', 30000);
               $this->db->having('salary >=', 20000);
            
            } else if($filterdata['basicSalary'] == 40) {
               $this->db->having('salary <=', 40000);
               $this->db->having('salary >=', 30000);
            
            } else if($filterdata['basicSalary'] == 50) {
               $this->db->having('salary <=', 50000);
               $this->db->having('salary >=', 40000);
            
            } else if($filterdata['basicSalary'] == 70) {
               $this->db->having('salary <=', 70000);
               $this->db->having('salary >=', 50000);
            
            } else if($filterdata['basicSalary'] == 80) {
               $this->db->having('salary <=', 80000);
               $this->db->having('salary >=', 70000);
            }
         }

         $this->db->having("distance<", '7000');
         $this->db->group_by("job_posting.company_id");
         $query = $this->db->get();
         if($query->num_rows() > 0) {
            return $query->result_array();
         } else {

            $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'job_posting.recruiter_id = recruiter_details.recruiter_id');
            $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
            $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

            if(isset($filterdata['allowancestoppicks']) && !empty($filterdata['toppicks'])) {
              $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
              $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
              $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
              $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
            }

            $this->db->where("job_posting.jobexpire >=", $cDate);
            $this->db->where("job_posting.category", 24);
            if($id!=0) {
               $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            }
            if(strlen($expfilter) > 0) {
               $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
            }

            if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
               $this->db->where("job_posting.category", $filterdata['jobcategory']);

            } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
               $this->db->where("job_posting.level", $filterdata['joblevel']);

            } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
               //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
               $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
            }

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
               $toppicks = $filterdata['toppicks'];
               $this->db->where_in('job_toppicks.picks_id', $toppicks);
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
               $allowances = $filterdata['allowances'];
               $this->db->where_in('job_allowances.allowances_id', $allowances);
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
               $medical = $filterdata['medical'];
               $this->db->where_in('job_medical.medical_id', $medical);
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
               $workshift = $filterdata['workshift'];
               $this->db->where_in('job_workshift.workshift_id', $workshift);
            }

            if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
               //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
            } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
               //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
            }

            if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
               
               if($filterdata['basicSalary'] == 5) {
                  $this->db->having('salary <=', 5000);

               } else if($filterdata['basicSalary'] == 10) {
                  $this->db->having('salary <=', 10000);
                  $this->db->having('salary >=', 5000);

               } else if($filterdata['basicSalary'] == 20) {
                  $this->db->having('salary <=', 20000);
                  $this->db->having('salary >=', 10000);

               } else if($filterdata['basicSalary'] == 30) {
                  $this->db->having('salary <=', 30000);
                  $this->db->having('salary >=', 20000);
               
               } else if($filterdata['basicSalary'] == 40) {
                  $this->db->having('salary <=', 40000);
                  $this->db->having('salary >=', 30000);
               
               } else if($filterdata['basicSalary'] == 50) {
                  $this->db->having('salary <=', 50000);
                  $this->db->having('salary >=', 40000);
               
               } else if($filterdata['basicSalary'] == 70) {
                  $this->db->having('salary <=', 70000);
                  $this->db->having('salary >=', 50000);
               
               } else if($filterdata['basicSalary'] == 80) {
                  $this->db->having('salary <=', 80000);
                  $this->db->having('salary >=', 70000);
               }
            }

            $this->db->having("distance<", '7000');
            $this->db->group_by("job_posting.recruiter_id");
            $query = $this->db->get();
            return $query->result_array();
         }
      }
   }

   public function catjob_fetch_leadership_all($id,$expfilter, $userLat, $userLong, $filterdata) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , job_posting.mode, job_posting.modeurl, job_posting.chatbot, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }

      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.category", 24);
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }

      if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
         $this->db->where("job_posting.category", $filterdata['jobcategory']);

      } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
         $this->db->where("job_posting.level", $filterdata['joblevel']);

      } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         $this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
         //$this->db->or_like("recruiter.cname", $filterdata['cname'], 'both');
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $this->db->where_in('job_toppicks.picks_id', $toppicks);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $this->db->where_in('job_allowances.allowances_id', $allowances);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $this->db->where_in('job_workshift.workshift_id', $workshift);
      }

      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->having('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->having('salary <=', 10000);
            $this->db->having('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->having('salary <=', 20000);
            $this->db->having('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->having('salary <=', 30000);
            $this->db->having('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->having('salary <=', 40000);
            $this->db->having('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->having('salary <=', 50000);
            $this->db->having('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->having('salary <=', 70000);
            $this->db->having('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->having('salary <=', 80000);
            $this->db->having('salary >=', 70000);
         }
      }

      $this->db->having("distance<", '7000');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      if($query->num_rows() > 0) {
         return $query->result_array();
      } else {

         $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , job_posting.mode, job_posting.modeurl, job_posting.chatbot, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
         $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
           $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
           $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
           $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
           $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }

         $this->db->where("job_posting.jobexpire >=", $cDate);
         $this->db->where("job_posting.category", 24);
         if($id!=0) {
            $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
         }
         if(strlen($expfilter) > 0) {
            $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
         }

         if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
            $this->db->where("job_posting.category", $filterdata['jobcategory']);

         } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
            $this->db->where("job_posting.level", $filterdata['joblevel']);

         } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
            //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
            $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
         }

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
            $toppicks = $filterdata['toppicks'];
            $this->db->where_in('job_toppicks.picks_id', $toppicks);
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
            $allowances = $filterdata['allowances'];
            $this->db->where_in('job_allowances.allowances_id', $allowances);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $medical = $filterdata['medical'];
            $this->db->where_in('job_medical.medical_id', $medical);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $workshift = $filterdata['workshift'];
            $this->db->where_in('job_workshift.workshift_id', $workshift);
         }

         if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
            //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
         } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
            //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
         }

         if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
            
            if($filterdata['basicSalary'] == 5) {
               $this->db->having('salary <=', 5000);

            } else if($filterdata['basicSalary'] == 10) {
               $this->db->having('salary <=', 10000);
               $this->db->having('salary >=', 5000);

            } else if($filterdata['basicSalary'] == 20) {
               $this->db->having('salary <=', 20000);
               $this->db->having('salary >=', 10000);

            } else if($filterdata['basicSalary'] == 30) {
               $this->db->having('salary <=', 30000);
               $this->db->having('salary >=', 20000);
            
            } else if($filterdata['basicSalary'] == 40) {
               $this->db->having('salary <=', 40000);
               $this->db->having('salary >=', 30000);
            
            } else if($filterdata['basicSalary'] == 50) {
               $this->db->having('salary <=', 50000);
               $this->db->having('salary >=', 40000);
            
            } else if($filterdata['basicSalary'] == 70) {
               $this->db->having('salary <=', 70000);
               $this->db->having('salary >=', 50000);
            
            } else if($filterdata['basicSalary'] == 80) {
               $this->db->having('salary <=', 80000);
               $this->db->having('salary >=', 70000);
            }
         }

         $this->db->having("distance<", '7000');
         $this->db->group_by("exp_with_salary.jobpost_id");
         $query = $this->db->get();
         if($query->num_rows() > 0) {
            return $query->result_array();
         } else {

            $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , job_posting.mode, job_posting.modeurl, job_posting.chatbot, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'job_posting.recruiter_id = recruiter_details.recruiter_id');
            $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
            $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
              $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
              $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
              $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
              $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
            }

            $this->db->where("job_posting.jobexpire >=", $cDate);
            $this->db->where("job_posting.category", 24);
            if($id!=0) {
               $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            }
            if(strlen($expfilter) > 0) {
               $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
            }

            if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
               $this->db->where("job_posting.category", $filterdata['jobcategory']);

            } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
               $this->db->where("job_posting.level", $filterdata['joblevel']);

            } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
               //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
               $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
            }

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
               $toppicks = $filterdata['toppicks'];
               $this->db->where_in('job_toppicks.picks_id', $toppicks);
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
               $allowances = $filterdata['allowances'];
               $this->db->where_in('job_allowances.allowances_id', $allowances);
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
               $medical = $filterdata['medical'];
               $this->db->where_in('job_medical.medical_id', $medical);
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
               $workshift = $filterdata['workshift'];
               $this->db->where_in('job_workshift.workshift_id', $workshift);
            }

            if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
               //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
            } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
               //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
            }

            if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
               
               if($filterdata['basicSalary'] == 5) {
                  $this->db->having('salary <=', 5000);

               } else if($filterdata['basicSalary'] == 10) {
                  $this->db->having('salary <=', 10000);
                  $this->db->having('salary >=', 5000);

               } else if($filterdata['basicSalary'] == 20) {
                  $this->db->having('salary <=', 20000);
                  $this->db->having('salary >=', 10000);

               } else if($filterdata['basicSalary'] == 30) {
                  $this->db->having('salary <=', 30000);
                  $this->db->having('salary >=', 20000);
               
               } else if($filterdata['basicSalary'] == 40) {
                  $this->db->having('salary <=', 40000);
                  $this->db->having('salary >=', 30000);
               
               } else if($filterdata['basicSalary'] == 50) {
                  $this->db->having('salary <=', 50000);
                  $this->db->having('salary >=', 40000);
               
               } else if($filterdata['basicSalary'] == 70) {
                  $this->db->having('salary <=', 70000);
                  $this->db->having('salary >=', 50000);
               
               } else if($filterdata['basicSalary'] == 80) {
                  $this->db->having('salary <=', 80000);
                  $this->db->having('salary >=', 70000);
               }
            }

            $this->db->having("distance<", '7000');
            $this->db->group_by("exp_with_salary.jobpost_id");
            $query = $this->db->get();
            return $query->result_array();
         }
      }
   }


   // IT view all job list

   public function catjob_fetch_information_technology($id,$expfilter, $userLat, $userLong, $filterdata) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      
      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }

      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.category", 22);
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }

      if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
         $this->db->where("job_posting.category", $filterdata['jobcategory']);

      } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
         $this->db->where("job_posting.level", $filterdata['joblevel']);

      } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         $this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
         //$this->db->or_like("recruiter.cname", $filterdata['cname'], 'both');
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $this->db->where_in('job_toppicks.picks_id', $toppicks);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $this->db->where_in('job_allowances.allowances_id', $allowances);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $this->db->where_in('job_workshift.workshift_id', $workshift);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         $this->db->where_in('job_leaves.leaves_id', $leaves);
      }

      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->having('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->having('salary <=', 10000);
            $this->db->having('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->having('salary <=', 20000);
            $this->db->having('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->having('salary <=', 30000);
            $this->db->having('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->having('salary <=', 40000);
            $this->db->having('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->having('salary <=', 50000);
            $this->db->having('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->having('salary <=', 70000);
            $this->db->having('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->having('salary <=', 80000);
            $this->db->having('salary >=', 70000);
         }
      }

      $this->db->having("distance<", '7000');
      $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      if($query->num_rows() > 0) {
         return $query->result_array();
      } else {

         $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
         $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
         
         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
           $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
           $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
           $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
           $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
         }

         $this->db->where("job_posting.jobexpire >=", $cDate);
         $this->db->where("job_posting.category", 22);
         if($id!=0) {
            $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
         }
         if(strlen($expfilter) > 0) {
            $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
         }

         if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
            $this->db->where("job_posting.category", $filterdata['jobcategory']);

         } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
            $this->db->where("job_posting.level", $filterdata['joblevel']);

         } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
            //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
            $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
         }

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
            $toppicks = $filterdata['toppicks'];
            $this->db->where_in('job_toppicks.picks_id', $toppicks);
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
            $allowances = $filterdata['allowances'];
            $this->db->where_in('job_allowances.allowances_id', $allowances);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $medical = $filterdata['medical'];
            $this->db->where_in('job_medical.medical_id', $medical);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $workshift = $filterdata['workshift'];
            $this->db->where_in('job_workshift.workshift_id', $workshift);
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
            $leaves = $filterdata['leaves'];
            $this->db->where_in('job_leaves.leaves_id', $leaves);
         }

         if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
            //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
         } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
            //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
         }

         if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
            
            if($filterdata['basicSalary'] == 5) {
               $this->db->having('salary <=', 5000);

            } else if($filterdata['basicSalary'] == 10) {
               $this->db->having('salary <=', 10000);
               $this->db->having('salary >=', 5000);

            } else if($filterdata['basicSalary'] == 20) {
               $this->db->having('salary <=', 20000);
               $this->db->having('salary >=', 10000);

            } else if($filterdata['basicSalary'] == 30) {
               $this->db->having('salary <=', 30000);
               $this->db->having('salary >=', 20000);
            
            } else if($filterdata['basicSalary'] == 40) {
               $this->db->having('salary <=', 40000);
               $this->db->having('salary >=', 30000);
            
            } else if($filterdata['basicSalary'] == 50) {
               $this->db->having('salary <=', 50000);
               $this->db->having('salary >=', 40000);
            
            } else if($filterdata['basicSalary'] == 70) {
               $this->db->having('salary <=', 70000);
               $this->db->having('salary >=', 50000);
            
            } else if($filterdata['basicSalary'] == 80) {
               $this->db->having('salary <=', 80000);
               $this->db->having('salary >=', 70000);
            }
         }

         $this->db->having("distance<", '7000');
         $this->db->group_by("job_posting.company_id");
         $query = $this->db->get();
         if($query->num_rows() > 0) {
            return $query->result_array();
         } else {

            $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire,job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'job_posting.recruiter_id = recruiter_details.recruiter_id');
            $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
            $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
            
            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
              $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
              $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
              $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
              $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }

            $this->db->where("job_posting.jobexpire >=", $cDate);
            $this->db->where("job_posting.category", 22);
            if($id!=0) {
               $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            }
            if(strlen($expfilter) > 0) {
               $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
            }

            if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
               $this->db->where("job_posting.category", $filterdata['jobcategory']);

            } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
               $this->db->where("job_posting.level", $filterdata['joblevel']);

            } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
               //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
               $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
            }

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
               $toppicks = $filterdata['toppicks'];
               $this->db->where_in('job_toppicks.picks_id', $toppicks);
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
               $allowances = $filterdata['allowances'];
               $this->db->where_in('job_allowances.allowances_id', $allowances);
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
               $medical = $filterdata['medical'];
               $this->db->where_in('job_medical.medical_id', $medical);
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
               $workshift = $filterdata['workshift'];
               $this->db->where_in('job_workshift.workshift_id', $workshift);
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
               $leaves = $filterdata['leaves'];
               $this->db->where_in('job_leaves.leaves_id', $leaves);
            }

            if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
               //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
            } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
               //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
            }

            if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
               
               if($filterdata['basicSalary'] == 5) {
                  $this->db->having('salary <=', 5000);

               } else if($filterdata['basicSalary'] == 10) {
                  $this->db->having('salary <=', 10000);
                  $this->db->having('salary >=', 5000);

               } else if($filterdata['basicSalary'] == 20) {
                  $this->db->having('salary <=', 20000);
                  $this->db->having('salary >=', 10000);

               } else if($filterdata['basicSalary'] == 30) {
                  $this->db->having('salary <=', 30000);
                  $this->db->having('salary >=', 20000);
               
               } else if($filterdata['basicSalary'] == 40) {
                  $this->db->having('salary <=', 40000);
                  $this->db->having('salary >=', 30000);
               
               } else if($filterdata['basicSalary'] == 50) {
                  $this->db->having('salary <=', 50000);
                  $this->db->having('salary >=', 40000);
               
               } else if($filterdata['basicSalary'] == 70) {
                  $this->db->having('salary <=', 70000);
                  $this->db->having('salary >=', 50000);
               
               } else if($filterdata['basicSalary'] == 80) {
                  $this->db->having('salary <=', 80000);
                  $this->db->having('salary >=', 70000);
               }
            }

            $this->db->having("distance<", '7000');
            $this->db->group_by("job_posting.recruiter_id");
            $query = $this->db->get();
            return $query->result_array();
         }
      }
   }

   public function catjob_fetch_information_technology_all($id,$expfilter, $userLat, $userLong, $filterdata) {

      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , job_posting.mode, job_posting.modeurl, job_posting.chatbot, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }

      //$this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.category", 22);
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }

      if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
         $this->db->where("job_posting.category", $filterdata['jobcategory']);

      } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
         $this->db->where("job_posting.level", $filterdata['joblevel']);

      } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         $this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
         //$this->db->or_like("recruiter.cname", $filterdata['cname'], 'both');
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $this->db->where_in('job_toppicks.picks_id', $toppicks);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $this->db->where_in('job_allowances.allowances_id', $allowances);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $this->db->where_in('job_workshift.workshift_id', $workshift);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         $this->db->where_in('job_leaves.leaves_id', $leaves);
      }

      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->having('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->having('salary <=', 10000);
            $this->db->having('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->having('salary <=', 20000);
            $this->db->having('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->having('salary <=', 30000);
            $this->db->having('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->having('salary <=', 40000);
            $this->db->having('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->having('salary <=', 50000);
            $this->db->having('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->having('salary <=', 70000);
            $this->db->having('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->having('salary <=', 80000);
            $this->db->having('salary >=', 70000);
         }
      }

      $this->db->having("distance<", '7000');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      if($query->num_rows() > 0) {
         return $query->result_array();
      } else {

         $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , job_posting.mode, job_posting.modeurl, job_posting.chatbot, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
         $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
           $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
           $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
           $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
           $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
         }

         $this->db->where("job_posting.jobexpire >=", $cDate);
         $this->db->where("job_posting.category", 22);
         if($id!=0) {
            $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
         }
         if(strlen($expfilter) > 0) {
            $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
         }

         if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
            $this->db->where("job_posting.category", $filterdata['jobcategory']);

         } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
            $this->db->where("job_posting.level", $filterdata['joblevel']);

         } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
            //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
            $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
         }

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
            $toppicks = $filterdata['toppicks'];
            $this->db->where_in('job_toppicks.picks_id', $toppicks);
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
            $allowances = $filterdata['allowances'];
            $this->db->where_in('job_allowances.allowances_id', $allowances);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $medical = $filterdata['medical'];
            $this->db->where_in('job_medical.medical_id', $medical);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $workshift = $filterdata['workshift'];
            $this->db->where_in('job_workshift.workshift_id', $workshift);
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
            $leaves = $filterdata['leaves'];
            $this->db->where_in('job_leaves.leaves_id', $leaves);
         }

         if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
            //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
         } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
            //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
         }

         if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
            
            if($filterdata['basicSalary'] == 5) {
               $this->db->having('salary <=', 5000);

            } else if($filterdata['basicSalary'] == 10) {
               $this->db->having('salary <=', 10000);
               $this->db->having('salary >=', 5000);

            } else if($filterdata['basicSalary'] == 20) {
               $this->db->having('salary <=', 20000);
               $this->db->having('salary >=', 10000);

            } else if($filterdata['basicSalary'] == 30) {
               $this->db->having('salary <=', 30000);
               $this->db->having('salary >=', 20000);
            
            } else if($filterdata['basicSalary'] == 40) {
               $this->db->having('salary <=', 40000);
               $this->db->having('salary >=', 30000);
            
            } else if($filterdata['basicSalary'] == 50) {
               $this->db->having('salary <=', 50000);
               $this->db->having('salary >=', 40000);
            
            } else if($filterdata['basicSalary'] == 70) {
               $this->db->having('salary <=', 70000);
               $this->db->having('salary >=', 50000);
            
            } else if($filterdata['basicSalary'] == 80) {
               $this->db->having('salary <=', 80000);
               $this->db->having('salary >=', 70000);
            }
         }

         $this->db->having("distance<", '7000');
         $this->db->group_by("exp_with_salary.jobpost_id");
         $query = $this->db->get();
         if($query->num_rows() > 0) {
            return $query->result_array();
         } else {

            $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , job_posting.mode, job_posting.modeurl, job_posting.chatbot, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'job_posting.recruiter_id = recruiter_details.recruiter_id');
            $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
            $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
              $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
              $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
              $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
              $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }

            $this->db->where("job_posting.jobexpire >=", $cDate);
            $this->db->where("job_posting.category", 22);
            if($id!=0) {
               $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            }
            if(strlen($expfilter) > 0) {
               $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
            }

            if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
               $this->db->where("job_posting.category", $filterdata['jobcategory']);

            } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
               $this->db->where("job_posting.level", $filterdata['joblevel']);

            } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
               //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
               $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
            }

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
               $toppicks = $filterdata['toppicks'];
               $this->db->where_in('job_toppicks.picks_id', $toppicks);
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
               $allowances = $filterdata['allowances'];
               $this->db->where_in('job_allowances.allowances_id', $allowances);
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
               $medical = $filterdata['medical'];
               $this->db->where_in('job_medical.medical_id', $medical);
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
               $workshift = $filterdata['workshift'];
               $this->db->where_in('job_workshift.workshift_id', $workshift);
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
               $leaves = $filterdata['leaves'];
               $this->db->where_in('job_leaves.leaves_id', $leaves);
            }

            if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
               //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
            } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
               //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
            }

            if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
               
               if($filterdata['basicSalary'] == 5) {
                  $this->db->having('salary <=', 5000);

               } else if($filterdata['basicSalary'] == 10) {
                  $this->db->having('salary <=', 10000);
                  $this->db->having('salary >=', 5000);

               } else if($filterdata['basicSalary'] == 20) {
                  $this->db->having('salary <=', 20000);
                  $this->db->having('salary >=', 10000);

               } else if($filterdata['basicSalary'] == 30) {
                  $this->db->having('salary <=', 30000);
                  $this->db->having('salary >=', 20000);
               
               } else if($filterdata['basicSalary'] == 40) {
                  $this->db->having('salary <=', 40000);
                  $this->db->having('salary >=', 30000);
               
               } else if($filterdata['basicSalary'] == 50) {
                  $this->db->having('salary <=', 50000);
                  $this->db->having('salary >=', 40000);
               
               } else if($filterdata['basicSalary'] == 70) {
                  $this->db->having('salary <=', 70000);
                  $this->db->having('salary >=', 50000);
               
               } else if($filterdata['basicSalary'] == 80) {
                  $this->db->having('salary <=', 80000);
                  $this->db->having('salary >=', 70000);
               }
            }

            $this->db->having("distance<", '7000');
            $this->db->group_by("exp_with_salary.jobpost_id");
            $query = $this->db->get();
            return $query->result_array();
         }
      }
   }

   // City view all list

   public function cityjob_fetch_groupby($id,$expfilter, $userLat, $userLong, $userCity, $filterdata) {
      $cDate = date('Y-m-d');

      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , job_posting.mode, job_posting.modeurl, job_posting.chatbot ,(6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id','left');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }

      if(isset($userCity)) {
         $userCity = strtolower($userCity);
         $this->db->like("recruiter_details.address", $userCity, 'both');
      } else {
         if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
            //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
         } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
            //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
         }
      }

      $this->db->where("job_posting.jobexpire >=", $cDate);
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }

      if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
         $this->db->where("job_posting.category", $filterdata['jobcategory']);

      } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
         $this->db->where("job_posting.level", $filterdata['joblevel']);

      } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         $this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
         //$this->db->or_like("recruiter.cname", $filterdata['cname'], 'both');
      }
      
      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $this->db->where_in('job_toppicks.picks_id', $toppicks);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $this->db->where_in('job_allowances.allowances_id', $allowances);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $this->db->where_in('job_workshift.workshift_id', $workshift);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         $this->db->where_in('job_leaves.leaves_id', $leaves);
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->having('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->having('salary <=', 10000);
            $this->db->having('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->having('salary <=', 20000);
            $this->db->having('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->having('salary <=', 30000);
            $this->db->having('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->having('salary <=', 40000);
            $this->db->having('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->having('salary <=', 50000);
            $this->db->having('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->having('salary <=', 70000);
            $this->db->having('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->having('salary <=', 80000);
            $this->db->having('salary >=', 70000);
         }
      }

      $this->db->having("distance<", '7000');
      $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      if($query->num_rows() > 0) {

         return $query->result_array();
      } else {

         // $str = $this->db->last_query();

         //     echo "<pre>";

         //     print_r($str);

         //     die;

         $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , job_posting.mode, job_posting.modeurl, job_posting.chatbot ,(6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
         $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
           $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
           $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
           $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
           $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
         }
         
         $this->db->where("job_posting.jobexpire >=", $cDate);
         if($id!=0) {
            $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
         }
         if(strlen($expfilter) > 0) {
            $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
         }

         if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
            $this->db->where("job_posting.category", $filterdata['jobcategory']);

         } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
            $this->db->where("job_posting.level", $filterdata['joblevel']);

         } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
            //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
            $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
         }
         
         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
            $toppicks = $filterdata['toppicks'];
            $this->db->where_in('job_toppicks.picks_id', $toppicks);
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
            $allowances = $filterdata['allowances'];
            $this->db->where_in('job_allowances.allowances_id', $allowances);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $medical = $filterdata['medical'];
            $this->db->where_in('job_medical.medical_id', $medical);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $workshift = $filterdata['workshift'];
            $this->db->where_in('job_workshift.workshift_id', $workshift);
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
            $leaves = $filterdata['leaves'];
            $this->db->where_in('job_leaves.leaves_id', $leaves);
         }

         if(isset($userCity)) {
            $this->db->like("recruiter_details.address", $userCity, 'both');
         } else {
            if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
               //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
            } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
               //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
            }
         }

         if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
            
            if($filterdata['basicSalary'] == 5) {
               $this->db->having('salary <=', 5000);

            } else if($filterdata['basicSalary'] == 10) {
               $this->db->having('salary <=', 10000);
               $this->db->having('salary >=', 5000);

            } else if($filterdata['basicSalary'] == 20) {
               $this->db->having('salary <=', 20000);
               $this->db->having('salary >=', 10000);

            } else if($filterdata['basicSalary'] == 30) {
               $this->db->having('salary <=', 30000);
               $this->db->having('salary >=', 20000);
            
            } else if($filterdata['basicSalary'] == 40) {
               $this->db->having('salary <=', 40000);
               $this->db->having('salary >=', 30000);
            
            } else if($filterdata['basicSalary'] == 50) {
               $this->db->having('salary <=', 50000);
               $this->db->having('salary >=', 40000);
            
            } else if($filterdata['basicSalary'] == 70) {
               $this->db->having('salary <=', 70000);
               $this->db->having('salary >=', 50000);
            
            } else if($filterdata['basicSalary'] == 80) {
               $this->db->having('salary <=', 80000);
               $this->db->having('salary >=', 70000);
            }
         }
         $this->db->having("distance<", '7000');
         $this->db->group_by("job_posting.company_id");
         $query = $this->db->get();
         if($query->num_rows() > 0) {
            return $query->result_array();
         } else {

            $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , job_posting.mode, job_posting.modeurl, job_posting.chatbot ,(6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'job_posting.recruiter_id = recruiter_details.recruiter_id');
            $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
            $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
              $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
              $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
              $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
              $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }
            
            $this->db->where("job_posting.jobexpire >=", $cDate);
            if($id!=0) {
               $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            }
            if(strlen($expfilter) > 0) {
               $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
            }

            if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
               $this->db->where("job_posting.category", $filterdata['jobcategory']);

            } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
               $this->db->where("job_posting.level", $filterdata['joblevel']);

            } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
               //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
               $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
            }
            
            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
               $toppicks = $filterdata['toppicks'];
               $this->db->where_in('job_toppicks.picks_id', $toppicks);
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
               $allowances = $filterdata['allowances'];
               $this->db->where_in('job_allowances.allowances_id', $allowances);
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
               $medical = $filterdata['medical'];
               $this->db->where_in('job_medical.medical_id', $medical);
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
               $workshift = $filterdata['workshift'];
               $this->db->where_in('job_workshift.workshift_id', $workshift);
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
               $leaves = $filterdata['leaves'];
               $this->db->where_in('job_leaves.leaves_id', $leaves);
            }

            if(isset($userCity)) {
               $this->db->like("recruiter_details.address", $userCity, 'both');
            } else {
               if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
                  //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
               } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
                  //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
               }
            }

            if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
               
               if($filterdata['basicSalary'] == 5) {
                  $this->db->having('salary <=', 5000);

               } else if($filterdata['basicSalary'] == 10) {
                  $this->db->having('salary <=', 10000);
                  $this->db->having('salary >=', 5000);

               } else if($filterdata['basicSalary'] == 20) {
                  $this->db->having('salary <=', 20000);
                  $this->db->having('salary >=', 10000);

               } else if($filterdata['basicSalary'] == 30) {
                  $this->db->having('salary <=', 30000);
                  $this->db->having('salary >=', 20000);
               
               } else if($filterdata['basicSalary'] == 40) {
                  $this->db->having('salary <=', 40000);
                  $this->db->having('salary >=', 30000);
               
               } else if($filterdata['basicSalary'] == 50) {
                  $this->db->having('salary <=', 50000);
                  $this->db->having('salary >=', 40000);
               
               } else if($filterdata['basicSalary'] == 70) {
                  $this->db->having('salary <=', 70000);
                  $this->db->having('salary >=', 50000);
               
               } else if($filterdata['basicSalary'] == 80) {
                  $this->db->having('salary <=', 80000);
                  $this->db->having('salary >=', 70000);
               }
            }
            $this->db->having("distance<", '7000');
            $this->db->group_by("job_posting.recruiter_id");
            $query = $this->db->get();
            return $query->result_array();
         }
      }
   }

   public function cityjob_fetch($id,$expfilter, $userLat, $userLong, $userCity, $filterdata) {
      $cDate = date('Y-m-d');

      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , job_posting.mode, job_posting.modeurl, job_posting.chatbot ,(6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }
      
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }

      if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
         $this->db->where("job_posting.category", $filterdata['jobcategory']);

      } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
         $this->db->where("job_posting.level", $filterdata['joblevel']);

      } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         $this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
         //$this->db->or_like("recruiter.cname", $filterdata['cname'], 'both');
      }
      
      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $this->db->where_in('job_toppicks.picks_id', $toppicks);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $this->db->where_in('job_allowances.allowances_id', $allowances);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $this->db->where_in('job_workshift.workshift_id', $workshift);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         $this->db->where_in('job_leaves.leaves_id', $leaves);
      }

      if(isset($userCity)) {
         
         $this->db->like("recruiter_details.address", $userCity, 'both');
      } else {
         if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
            //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
         } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
            //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
         }
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->having('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->having('salary <=', 10000);
            $this->db->having('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->having('salary <=', 20000);
            $this->db->having('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->having('salary <=', 30000);
            $this->db->having('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->having('salary <=', 40000);
            $this->db->having('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->having('salary <=', 50000);
            $this->db->having('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->having('salary <=', 70000);
            $this->db->having('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->having('salary <=', 80000);
            $this->db->having('salary >=', 70000);
         }
      }

      $this->db->having("distance<", '7000');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      if($query->num_rows() > 0) {
         return $query->result_array();
      } else {

         $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , job_posting.mode, job_posting.modeurl, job_posting.chatbot ,(6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
         $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
           $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
           $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
           $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
           $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
         }
         
         $this->db->where("job_posting.jobexpire >=", $cDate);
         if($id!=0) {
            $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
         }
         if(strlen($expfilter) > 0) {
            $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
         }

         if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
            $this->db->where("job_posting.category", $filterdata['jobcategory']);

         } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
            $this->db->where("job_posting.level", $filterdata['joblevel']);

         } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
            //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
            $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
         }
         
         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
            $toppicks = $filterdata['toppicks'];
            $this->db->where_in('job_toppicks.picks_id', $toppicks);
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
            $allowances = $filterdata['allowances'];
            $this->db->where_in('job_allowances.allowances_id', $allowances);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $medical = $filterdata['medical'];
            $this->db->where_in('job_medical.medical_id', $medical);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $workshift = $filterdata['workshift'];
            $this->db->where_in('job_workshift.workshift_id', $workshift);
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
            $leaves = $filterdata['leaves'];
            $this->db->where_in('job_leaves.leaves_id', $leaves);
         }

         if(isset($userCity)) {
            $this->db->like("recruiter_details.address", $userCity, 'both');
         } else {
            if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
               //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
            } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
               //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
            }
         }

         if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
            
            if($filterdata['basicSalary'] == 5) {
               $this->db->having('salary <=', 5000);

            } else if($filterdata['basicSalary'] == 10) {
               $this->db->having('salary <=', 10000);
               $this->db->having('salary >=', 5000);

            } else if($filterdata['basicSalary'] == 20) {
               $this->db->having('salary <=', 20000);
               $this->db->having('salary >=', 10000);

            } else if($filterdata['basicSalary'] == 30) {
               $this->db->having('salary <=', 30000);
               $this->db->having('salary >=', 20000);
            
            } else if($filterdata['basicSalary'] == 40) {
               $this->db->having('salary <=', 40000);
               $this->db->having('salary >=', 30000);
            
            } else if($filterdata['basicSalary'] == 50) {
               $this->db->having('salary <=', 50000);
               $this->db->having('salary >=', 40000);
            
            } else if($filterdata['basicSalary'] == 70) {
               $this->db->having('salary <=', 70000);
               $this->db->having('salary >=', 50000);
            
            } else if($filterdata['basicSalary'] == 80) {
               $this->db->having('salary <=', 80000);
               $this->db->having('salary >=', 70000);
            }
         }

         $this->db->having("distance<", '7000');
         $this->db->group_by("exp_with_salary.jobpost_id");
         $query = $this->db->get();
         if($query->num_rows() > 0) {
            return $query->result_array();
         } else {

            $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , job_posting.mode, job_posting.modeurl, job_posting.chatbot ,(6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'job_posting.recruiter_id = recruiter_details.recruiter_id');
            $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
            $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
              $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
              $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
              $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
              $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }
            
            $this->db->where("job_posting.jobexpire >=", $cDate);
            if($id!=0) {
               $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            }
            if(strlen($expfilter) > 0) {
               $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
            }

            if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
               $this->db->where("job_posting.category", $filterdata['jobcategory']);

            } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
               $this->db->where("job_posting.level", $filterdata['joblevel']);

            } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
               //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
               $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
            }
            
            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
               $toppicks = $filterdata['toppicks'];
               $this->db->where_in('job_toppicks.picks_id', $toppicks);
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
               $allowances = $filterdata['allowances'];
               $this->db->where_in('job_allowances.allowances_id', $allowances);
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
               $medical = $filterdata['medical'];
               $this->db->where_in('job_medical.medical_id', $medical);
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
               $workshift = $filterdata['workshift'];
               $this->db->where_in('job_workshift.workshift_id', $workshift);
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
               $leaves = $filterdata['leaves'];
               $this->db->where_in('job_leaves.leaves_id', $leaves);
            }

            if(isset($userCity)) {
               $this->db->like("recruiter_details.address", $userCity, 'both');
            } else {
               if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
                  //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
               } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
                  //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
               }
            }

            if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
               
               if($filterdata['basicSalary'] == 5) {
                  $this->db->having('salary <=', 5000);

               } else if($filterdata['basicSalary'] == 10) {
                  $this->db->having('salary <=', 10000);
                  $this->db->having('salary >=', 5000);

               } else if($filterdata['basicSalary'] == 20) {
                  $this->db->having('salary <=', 20000);
                  $this->db->having('salary >=', 10000);

               } else if($filterdata['basicSalary'] == 30) {
                  $this->db->having('salary <=', 30000);
                  $this->db->having('salary >=', 20000);
               
               } else if($filterdata['basicSalary'] == 40) {
                  $this->db->having('salary <=', 40000);
                  $this->db->having('salary >=', 30000);
               
               } else if($filterdata['basicSalary'] == 50) {
                  $this->db->having('salary <=', 50000);
                  $this->db->having('salary >=', 40000);
               
               } else if($filterdata['basicSalary'] == 70) {
                  $this->db->having('salary <=', 70000);
                  $this->db->having('salary >=', 50000);
               
               } else if($filterdata['basicSalary'] == 80) {
                  $this->db->having('salary <=', 80000);
                  $this->db->having('salary >=', 70000);
               }
            }

            $this->db->having("distance<", '7000');
            $this->db->group_by("exp_with_salary.jobpost_id");
            $query = $this->db->get();
            return $query->result_array();
         }
      }
   }


   // Instant screen view all  jobs

   public function instantscreening_fetch_groupby($id, $expfilter, $userLat, $userLong, $filterdata) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }

      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->like("job_posting.mode", 'Instant screening');

      if($id != 0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }

      if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
         $this->db->where("job_posting.category", $filterdata['jobcategory']);

      } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
         $this->db->where("job_posting.level", $filterdata['joblevel']);

      } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         $this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
         //$this->db->or_like("recruiter.cname", $filterdata['cname'], 'both');
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $this->db->where_in('job_toppicks.picks_id', $toppicks);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $this->db->where_in('job_allowances.allowances_id', $allowances);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $this->db->where_in('job_workshift.workshift_id', $workshift);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         $this->db->where_in('job_leaves.leaves_id', $leaves);
      }

      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->having('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->having('salary <=', 10000);
            $this->db->having('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->having('salary <=', 20000);
            $this->db->having('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->having('salary <=', 30000);
            $this->db->having('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->having('salary <=', 40000);
            $this->db->having('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->having('salary <=', 50000);
            $this->db->having('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->having('salary <=', 70000);
            $this->db->having('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->having('salary <=', 80000);
            $this->db->having('salary >=', 70000);
         }
      }

      $this->db->having("distance<", '7000');
      $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      if($query->num_rows() > 0) {
         return $query->result_array();
      } else {

         $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
         $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
           $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
           $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
           $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
           $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
         }

         $this->db->where("job_posting.jobexpire >=", $cDate);
         $this->db->like("job_posting.mode", 'Instant screening');
         if($id != 0) {
            $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
         }
         if(strlen($expfilter) > 0) {
            $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
         }

         if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
            $this->db->where("job_posting.category", $filterdata['jobcategory']);

         } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
            $this->db->where("job_posting.level", $filterdata['joblevel']);

         } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
            //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
            $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
         }

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
            $toppicks = $filterdata['toppicks'];
            $this->db->where_in('job_toppicks.picks_id', $toppicks);
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
            $allowances = $filterdata['allowances'];
            $this->db->where_in('job_allowances.allowances_id', $allowances);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $medical = $filterdata['medical'];
            $this->db->where_in('job_medical.medical_id', $medical);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $workshift = $filterdata['workshift'];
            $this->db->where_in('job_workshift.workshift_id', $workshift);
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
            $leaves = $filterdata['leaves'];
            $this->db->where_in('job_leaves.leaves_id', $leaves);
         }

         if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
            //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
         } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
            //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
         }

         if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
            
            if($filterdata['basicSalary'] == 5) {
               $this->db->having('salary <=', 5000);

            } else if($filterdata['basicSalary'] == 10) {
               $this->db->having('salary <=', 10000);
               $this->db->having('salary >=', 5000);

            } else if($filterdata['basicSalary'] == 20) {
               $this->db->having('salary <=', 20000);
               $this->db->having('salary >=', 10000);

            } else if($filterdata['basicSalary'] == 30) {
               $this->db->having('salary <=', 30000);
               $this->db->having('salary >=', 20000);
            
            } else if($filterdata['basicSalary'] == 40) {
               $this->db->having('salary <=', 40000);
               $this->db->having('salary >=', 30000);
            
            } else if($filterdata['basicSalary'] == 50) {
               $this->db->having('salary <=', 50000);
               $this->db->having('salary >=', 40000);
            
            } else if($filterdata['basicSalary'] == 70) {
               $this->db->having('salary <=', 70000);
               $this->db->having('salary >=', 50000);
            
            } else if($filterdata['basicSalary'] == 80) {
               $this->db->having('salary <=', 80000);
               $this->db->having('salary >=', 70000);
            }
         }

         $this->db->having("distance<", '7000');
         $this->db->group_by("job_posting.company_id");
         $query = $this->db->get();
         if($query->num_rows() > 0) {
            return $query->result_array();
         } else {

            $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'job_posting.recruiter_id = recruiter_details.recruiter_id');
            $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
            $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
              $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
              $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
              $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
              $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }

            $this->db->where("job_posting.jobexpire >=", $cDate);
            $this->db->where("job_posting.mode", 'Instant screening');
            if($id != 0) {
               $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            }
            if(strlen($expfilter) > 0) {
               $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
            }

            if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
               $this->db->where("job_posting.category", $filterdata['jobcategory']);

            } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
               $this->db->where("job_posting.level", $filterdata['joblevel']);

            } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
               //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
               $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
            }

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
               $toppicks = $filterdata['toppicks'];
               $this->db->where_in('job_toppicks.picks_id', $toppicks);
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
               $allowances = $filterdata['allowances'];
               $this->db->where_in('job_allowances.allowances_id', $allowances);
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
               $medical = $filterdata['medical'];
               $this->db->where_in('job_medical.medical_id', $medical);
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
               $workshift = $filterdata['workshift'];
               $this->db->where_in('job_workshift.workshift_id', $workshift);
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
               $leaves = $filterdata['leaves'];
               $this->db->where_in('job_leaves.leaves_id', $leaves);
            }

            if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
               //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
            } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
               //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
            }

            if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
               
               if($filterdata['basicSalary'] == 5) {
                  $this->db->having('salary <=', 5000);

               } else if($filterdata['basicSalary'] == 10) {
                  $this->db->having('salary <=', 10000);
                  $this->db->having('salary >=', 5000);

               } else if($filterdata['basicSalary'] == 20) {
                  $this->db->having('salary <=', 20000);
                  $this->db->having('salary >=', 10000);

               } else if($filterdata['basicSalary'] == 30) {
                  $this->db->having('salary <=', 30000);
                  $this->db->having('salary >=', 20000);
               
               } else if($filterdata['basicSalary'] == 40) {
                  $this->db->having('salary <=', 40000);
                  $this->db->having('salary >=', 30000);
               
               } else if($filterdata['basicSalary'] == 50) {
                  $this->db->having('salary <=', 50000);
                  $this->db->having('salary >=', 40000);
               
               } else if($filterdata['basicSalary'] == 70) {
                  $this->db->having('salary <=', 70000);
                  $this->db->having('salary >=', 50000);
               
               } else if($filterdata['basicSalary'] == 80) {
                  $this->db->having('salary <=', 80000);
                  $this->db->having('salary >=', 70000);
               }
            }

            $this->db->having("distance<", '7000');
            $this->db->group_by("job_posting.recruiter_id");
            $query = $this->db->get();
            return $query->result_array();
         }
      }
   }

   public function instantscreening_fetch_all($id, $expfilter, $userLat, $userLong, $filterdata) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.modeurl, job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->like("job_posting.mode", 'Instant screening');
      if($id != 0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }

      if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
         $this->db->where("job_posting.category", $filterdata['jobcategory']);

      } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
         $this->db->where("job_posting.level", $filterdata['joblevel']);

      } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         $this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
         //$this->db->or_like("recruiter.cname", $filterdata['cname'], 'both');
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $this->db->where_in('job_toppicks.picks_id', $toppicks);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $this->db->where_in('job_allowances.allowances_id', $allowances);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $this->db->where_in('job_workshift.workshift_id', $workshift);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         $this->db->where_in('job_leaves.leaves_id', $leaves);
      }

      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->having('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->having('salary <=', 10000);
            $this->db->having('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->having('salary <=', 20000);
            $this->db->having('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->having('salary <=', 30000);
            $this->db->having('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->having('salary <=', 40000);
            $this->db->having('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->having('salary <=', 50000);
            $this->db->having('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->having('salary <=', 70000);
            $this->db->having('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->having('salary <=', 80000);
            $this->db->having('salary >=', 70000);
         }
      }

      $this->db->having("distance<", '7000');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      if($query->num_rows() > 0) {
         return $query->result_array();
      } else {

         $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.modeurl, job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
         $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
           $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
           $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
           $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
           $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
         }
         $this->db->where("job_posting.jobexpire >=", $cDate);
         $this->db->like("job_posting.mode", 'Instant screening');
         if($id != 0) {
            $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
         }
         if(strlen($expfilter) > 0) {
            $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
         }

         if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
            $this->db->where("job_posting.category", $filterdata['jobcategory']);

         } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
            $this->db->where("job_posting.level", $filterdata['joblevel']);

         } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
            //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
            $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
         }

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
            $toppicks = $filterdata['toppicks'];
            $this->db->where_in('job_toppicks.picks_id', $toppicks);
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
            $allowances = $filterdata['allowances'];
            $this->db->where_in('job_allowances.allowances_id', $allowances);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $medical = $filterdata['medical'];
            $this->db->where_in('job_medical.medical_id', $medical);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $workshift = $filterdata['workshift'];
            $this->db->where_in('job_workshift.workshift_id', $workshift);
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
            $leaves = $filterdata['leaves'];
            $this->db->where_in('job_leaves.leaves_id', $leaves);
         }

         if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
            //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
         } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
            //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
         }

         if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
            
            if($filterdata['basicSalary'] == 5) {
               $this->db->having('salary <=', 5000);

            } else if($filterdata['basicSalary'] == 10) {
               $this->db->having('salary <=', 10000);
               $this->db->having('salary >=', 5000);

            } else if($filterdata['basicSalary'] == 20) {
               $this->db->having('salary <=', 20000);
               $this->db->having('salary >=', 10000);

            } else if($filterdata['basicSalary'] == 30) {
               $this->db->having('salary <=', 30000);
               $this->db->having('salary >=', 20000);
            
            } else if($filterdata['basicSalary'] == 40) {
               $this->db->having('salary <=', 40000);
               $this->db->having('salary >=', 30000);
            
            } else if($filterdata['basicSalary'] == 50) {
               $this->db->having('salary <=', 50000);
               $this->db->having('salary >=', 40000);
            
            } else if($filterdata['basicSalary'] == 70) {
               $this->db->having('salary <=', 70000);
               $this->db->having('salary >=', 50000);
            
            } else if($filterdata['basicSalary'] == 80) {
               $this->db->having('salary <=', 80000);
               $this->db->having('salary >=', 70000);
            }
         }

         $this->db->having("distance<", '7000');
         $this->db->group_by("exp_with_salary.jobpost_id");
         $query = $this->db->get();
         if($query->num_rows() > 0) {
            return $query->result_array();
         } else {

            $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.modeurl, job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'job_posting.recruiter_id = recruiter_details.recruiter_id');
            $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
            $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
              $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
              $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
              $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
              $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }
            $this->db->where("job_posting.jobexpire >=", $cDate);
            $this->db->like("job_posting.mode", 'Instant screening');
            if($id != 0) {
               $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            }
            if(strlen($expfilter) > 0) {
               $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
            }

            if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
               $this->db->where("job_posting.category", $filterdata['jobcategory']);

            } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
               $this->db->where("job_posting.level", $filterdata['joblevel']);

            } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
               //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
               $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
            }

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
               $toppicks = $filterdata['toppicks'];
               $this->db->where_in('job_toppicks.picks_id', $toppicks);
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
               $allowances = $filterdata['allowances'];
               $this->db->where_in('job_allowances.allowances_id', $allowances);
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
               $medical = $filterdata['medical'];
               $this->db->where_in('job_medical.medical_id', $medical);
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
               $workshift = $filterdata['workshift'];
               $this->db->where_in('job_workshift.workshift_id', $workshift);
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
               $leaves = $filterdata['leaves'];
               $this->db->where_in('job_leaves.leaves_id', $leaves);
            }

            if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
               //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
            } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
               //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
            }

            if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
               
               if($filterdata['basicSalary'] == 5) {
                  $this->db->having('salary <=', 5000);

               } else if($filterdata['basicSalary'] == 10) {
                  $this->db->having('salary <=', 10000);
                  $this->db->having('salary >=', 5000);

               } else if($filterdata['basicSalary'] == 20) {
                  $this->db->having('salary <=', 20000);
                  $this->db->having('salary >=', 10000);

               } else if($filterdata['basicSalary'] == 30) {
                  $this->db->having('salary <=', 30000);
                  $this->db->having('salary >=', 20000);
               
               } else if($filterdata['basicSalary'] == 40) {
                  $this->db->having('salary <=', 40000);
                  $this->db->having('salary >=', 30000);
               
               } else if($filterdata['basicSalary'] == 50) {
                  $this->db->having('salary <=', 50000);
                  $this->db->having('salary >=', 40000);
               
               } else if($filterdata['basicSalary'] == 70) {
                  $this->db->having('salary <=', 70000);
                  $this->db->having('salary >=', 50000);
               
               } else if($filterdata['basicSalary'] == 80) {
                  $this->db->having('salary <=', 80000);
                  $this->db->having('salary >=', 70000);
               }
            }

            $this->db->having("distance<", '7000');
            $this->db->group_by("exp_with_salary.jobpost_id");
            $query = $this->db->get();
            return $query->result_array();
         }
      }
   }

   // Site view job all

   public function sitejobs_fetch_all($id, $expfilter, $userLat, $userLong, $paramType, $filterdata) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.modeurl, job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.company_id", $paramType);
      if($id != 0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }

      if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
         $this->db->where("job_posting.category", $filterdata['jobcategory']);

      } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
         $this->db->where("job_posting.level", $filterdata['joblevel']);

      } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         $this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
         //$this->db->or_like("recruiter.cname", $filterdata['cname'], 'both');
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $this->db->where_in('job_toppicks.picks_id', $toppicks);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $this->db->where_in('job_allowances.allowances_id', $allowances);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $this->db->where_in('job_workshift.workshift_id', $workshift);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         $this->db->where_in('job_leaves.leaves_id', $leaves);
      }

      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->having('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->having('salary <=', 10000);
            $this->db->having('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->having('salary <=', 20000);
            $this->db->having('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->having('salary <=', 30000);
            $this->db->having('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->having('salary <=', 40000);
            $this->db->having('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->having('salary <=', 50000);
            $this->db->having('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->having('salary <=', 70000);
            $this->db->having('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->having('salary <=', 80000);
            $this->db->having('salary >=', 70000);
         }
      }

      $this->db->having("distance<", '7000');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      if($query->num_rows() > 0) {
         return $query->result_array();
      } else {

         $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.modeurl, job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
         $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
           $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
           $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
           $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
           $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
         }
         $this->db->where("job_posting.jobexpire >=", $cDate);
         $this->db->where("job_posting.company_id", $paramType);
         if($id != 0) {
            $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
         }
         if(strlen($expfilter) > 0) {
            $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
         }

         if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
            $this->db->where("job_posting.category", $filterdata['jobcategory']);

         } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
            $this->db->where("job_posting.level", $filterdata['joblevel']);

         } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
            //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
            $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
         }

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
            $toppicks = $filterdata['toppicks'];
            $this->db->where_in('job_toppicks.picks_id', $toppicks);
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
            $allowances = $filterdata['allowances'];
            $this->db->where_in('job_allowances.allowances_id', $allowances);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $medical = $filterdata['medical'];
            $this->db->where_in('job_medical.medical_id', $medical);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $workshift = $filterdata['workshift'];
            $this->db->where_in('job_workshift.workshift_id', $workshift);
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
            $leaves = $filterdata['leaves'];
            $this->db->where_in('job_leaves.leaves_id', $leaves);
         }

         if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
            //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
         } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
            //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
         }

         if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
            
            if($filterdata['basicSalary'] == 5) {
               $this->db->having('salary <=', 5000);

            } else if($filterdata['basicSalary'] == 10) {
               $this->db->having('salary <=', 10000);
               $this->db->having('salary >=', 5000);

            } else if($filterdata['basicSalary'] == 20) {
               $this->db->having('salary <=', 20000);
               $this->db->having('salary >=', 10000);

            } else if($filterdata['basicSalary'] == 30) {
               $this->db->having('salary <=', 30000);
               $this->db->having('salary >=', 20000);
            
            } else if($filterdata['basicSalary'] == 40) {
               $this->db->having('salary <=', 40000);
               $this->db->having('salary >=', 30000);
            
            } else if($filterdata['basicSalary'] == 50) {
               $this->db->having('salary <=', 50000);
               $this->db->having('salary >=', 40000);
            
            } else if($filterdata['basicSalary'] == 70) {
               $this->db->having('salary <=', 70000);
               $this->db->having('salary >=', 50000);
            
            } else if($filterdata['basicSalary'] == 80) {
               $this->db->having('salary <=', 80000);
               $this->db->having('salary >=', 70000);
            }
         }

         $this->db->having("distance<", '7000');
         $this->db->group_by("exp_with_salary.jobpost_id");
         $query = $this->db->get();
         if($query->num_rows() > 0) {
            return $query->result_array();
         } else {

            $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.modeurl, job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'job_posting.recruiter_id = recruiter_details.recruiter_id');
            $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
            $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
              $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
              $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
              $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
              $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }
            $this->db->where("job_posting.jobexpire >=", $cDate);
            $this->db->where("job_posting.company_id", $paramType);
            if($id != 0) {
               $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            }
            if(strlen($expfilter) > 0) {
               $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
            }

            if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
               $this->db->where("job_posting.category", $filterdata['jobcategory']);

            } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
               $this->db->where("job_posting.level", $filterdata['joblevel']);

            } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
               //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
               $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
            }

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
               $toppicks = $filterdata['toppicks'];
               $this->db->where_in('job_toppicks.picks_id', $toppicks);
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
               $allowances = $filterdata['allowances'];
               $this->db->where_in('job_allowances.allowances_id', $allowances);
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
               $medical = $filterdata['medical'];
               $this->db->where_in('job_medical.medical_id', $medical);
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
               $workshift = $filterdata['workshift'];
               $this->db->where_in('job_workshift.workshift_id', $workshift);
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
               $leaves = $filterdata['leaves'];
               $this->db->where_in('job_leaves.leaves_id', $leaves);
            }

            if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
               //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
            } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
               //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
            }

            if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
               
               if($filterdata['basicSalary'] == 5) {
                  $this->db->having('salary <=', 5000);

               } else if($filterdata['basicSalary'] == 10) {
                  $this->db->having('salary <=', 10000);
                  $this->db->having('salary >=', 5000);

               } else if($filterdata['basicSalary'] == 20) {
                  $this->db->having('salary <=', 20000);
                  $this->db->having('salary >=', 10000);

               } else if($filterdata['basicSalary'] == 30) {
                  $this->db->having('salary <=', 30000);
                  $this->db->having('salary >=', 20000);
               
               } else if($filterdata['basicSalary'] == 40) {
                  $this->db->having('salary <=', 40000);
                  $this->db->having('salary >=', 30000);
               
               } else if($filterdata['basicSalary'] == 50) {
                  $this->db->having('salary <=', 50000);
                  $this->db->having('salary >=', 40000);
               
               } else if($filterdata['basicSalary'] == 70) {
                  $this->db->having('salary <=', 70000);
                  $this->db->having('salary >=', 50000);
               
               } else if($filterdata['basicSalary'] == 80) {
                  $this->db->having('salary <=', 80000);
                  $this->db->having('salary >=', 70000);
               }
            }

            $this->db->having("distance<", '7000');
            $this->db->group_by("exp_with_salary.jobpost_id");
            $query = $this->db->get();
            return $query->result_array();
         }
      }
   }


   // Company job all 

   public function companyjobs_fetch_all($id, $expfilter, $userLat, $userLong, $paramType, $filterdata) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.modeurl, job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.recruiter_id", $paramType);
      if($id != 0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }

      if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
         $this->db->where("job_posting.category", $filterdata['jobcategory']);

      } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
         $this->db->where("job_posting.level", $filterdata['joblevel']);

      } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         $this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
         //$this->db->or_like("recruiter.cname", $filterdata['cname'], 'both');
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $this->db->where_in('job_toppicks.picks_id', $toppicks);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $this->db->where_in('job_allowances.allowances_id', $allowances);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $this->db->where_in('job_workshift.workshift_id', $workshift);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         $this->db->where_in('job_leaves.leaves_id', $leaves);
      }

      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->having('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->having('salary <=', 10000);
            $this->db->having('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->having('salary <=', 20000);
            $this->db->having('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->having('salary <=', 30000);
            $this->db->having('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->having('salary <=', 40000);
            $this->db->having('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->having('salary <=', 50000);
            $this->db->having('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->having('salary <=', 70000);
            $this->db->having('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->having('salary <=', 80000);
            $this->db->having('salary >=', 70000);
         }
      }

      $this->db->having("distance<", '100');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      if($query->num_rows() > 0) {
         return $query->result_array();
      } else {

         $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.modeurl, job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
         $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
           $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
           $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
           $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
           $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
         }
         $this->db->where("job_posting.jobexpire >=", $cDate);
         $this->db->where("job_posting.recruiter_id", $paramType);
         if($id != 0) {
            $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
         }
         if(strlen($expfilter) > 0) {
            $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
         }

         if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
            $this->db->where("job_posting.category", $filterdata['jobcategory']);

         } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
            $this->db->where("job_posting.level", $filterdata['joblevel']);

         } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
            //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
            $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
         }

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
            $toppicks = $filterdata['toppicks'];
            $this->db->where_in('job_toppicks.picks_id', $toppicks);
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
            $allowances = $filterdata['allowances'];
            $this->db->where_in('job_allowances.allowances_id', $allowances);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $medical = $filterdata['medical'];
            $this->db->where_in('job_medical.medical_id', $medical);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $workshift = $filterdata['workshift'];
            $this->db->where_in('job_workshift.workshift_id', $workshift);
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
            $leaves = $filterdata['leaves'];
            $this->db->where_in('job_leaves.leaves_id', $leaves);
         }

         if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
            //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
         } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
            //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
         }

         if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
            
            if($filterdata['basicSalary'] == 5) {
               $this->db->having('salary <=', 5000);

            } else if($filterdata['basicSalary'] == 10) {
               $this->db->having('salary <=', 10000);
               $this->db->having('salary >=', 5000);

            } else if($filterdata['basicSalary'] == 20) {
               $this->db->having('salary <=', 20000);
               $this->db->having('salary >=', 10000);

            } else if($filterdata['basicSalary'] == 30) {
               $this->db->having('salary <=', 30000);
               $this->db->having('salary >=', 20000);
            
            } else if($filterdata['basicSalary'] == 40) {
               $this->db->having('salary <=', 40000);
               $this->db->having('salary >=', 30000);
            
            } else if($filterdata['basicSalary'] == 50) {
               $this->db->having('salary <=', 50000);
               $this->db->having('salary >=', 40000);
            
            } else if($filterdata['basicSalary'] == 70) {
               $this->db->having('salary <=', 70000);
               $this->db->having('salary >=', 50000);
            
            } else if($filterdata['basicSalary'] == 80) {
               $this->db->having('salary <=', 80000);
               $this->db->having('salary >=', 70000);
            }
         }

         $this->db->having("distance<", '100');
         $this->db->group_by("exp_with_salary.jobpost_id");
         $query = $this->db->get();
         if($query->num_rows() > 0) {
            return $query->result_array();
         } else {

            $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.modeurl, job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'job_posting.recruiter_id = recruiter_details.recruiter_id');
            $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
            $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
              $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
              $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
              $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
              $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }
            $this->db->where("job_posting.jobexpire >=", $cDate);
            $this->db->where("job_posting.recruiter_id", $paramType);
            if($id != 0) {
               $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            }
            if(strlen($expfilter) > 0) {
               $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
            }

            if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
               $this->db->where("job_posting.category", $filterdata['jobcategory']);

            } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
               $this->db->where("job_posting.level", $filterdata['joblevel']);

            } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
               //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
               $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
            }

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
               $toppicks = $filterdata['toppicks'];
               $this->db->where_in('job_toppicks.picks_id', $toppicks);
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
               $allowances = $filterdata['allowances'];
               $this->db->where_in('job_allowances.allowances_id', $allowances);
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
               $medical = $filterdata['medical'];
               $this->db->where_in('job_medical.medical_id', $medical);
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
               $workshift = $filterdata['workshift'];
               $this->db->where_in('job_workshift.workshift_id', $workshift);
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
               $leaves = $filterdata['leaves'];
               $this->db->where_in('job_leaves.leaves_id', $leaves);
            }

            if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
               //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
            } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
               //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
            }

            if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
               
               if($filterdata['basicSalary'] == 5) {
                  $this->db->having('salary <=', 5000);

               } else if($filterdata['basicSalary'] == 10) {
                  $this->db->having('salary <=', 10000);
                  $this->db->having('salary >=', 5000);

               } else if($filterdata['basicSalary'] == 20) {
                  $this->db->having('salary <=', 20000);
                  $this->db->having('salary >=', 10000);

               } else if($filterdata['basicSalary'] == 30) {
                  $this->db->having('salary <=', 30000);
                  $this->db->having('salary >=', 20000);
               
               } else if($filterdata['basicSalary'] == 40) {
                  $this->db->having('salary <=', 40000);
                  $this->db->having('salary >=', 30000);
               
               } else if($filterdata['basicSalary'] == 50) {
                  $this->db->having('salary <=', 50000);
                  $this->db->having('salary >=', 40000);
               
               } else if($filterdata['basicSalary'] == 70) {
                  $this->db->having('salary <=', 70000);
                  $this->db->having('salary >=', 50000);
               
               } else if($filterdata['basicSalary'] == 80) {
                  $this->db->having('salary <=', 80000);
                  $this->db->having('salary >=', 70000);
               }
            }

            $this->db->having("distance<", '7000');
            $this->db->group_by("exp_with_salary.jobpost_id");
            $query = $this->db->get();
            return $query->result_array();
         }
      }
   }


   // Expert job all list
   public function categoryjob_fetch_latlong_groupby($id, $expfilter, $userLat, $userLong, $getParamType, $filterdata) {

      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire,job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      
      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }

      $this->db->where("job_posting.jobexpire >=", $cDate);
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }

      if(!empty($getParamType)) {
         $this->db->where("job_posting.category", $getParamType);
      }else if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
         $this->db->where("job_posting.category", $filterdata['jobcategory']);

      } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
         $this->db->where("job_posting.level", $filterdata['joblevel']);

      } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         $this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $this->db->where_in('job_toppicks.picks_id', $toppicks);
      }

      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $this->db->where_in('job_allowances.allowances_id', $allowances);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $this->db->where_in('job_workshift.workshift_id', $workshift);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         $this->db->where_in('job_leaves.leaves_id', $leaves);
      }

      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->having('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->having('salary <=', 10000);
            $this->db->having('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->having('salary <=', 20000);
            $this->db->having('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->having('salary <=', 30000);
            $this->db->having('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->having('salary <=', 40000);
            $this->db->having('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->having('salary <=', 50000);
            $this->db->having('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->having('salary <=', 70000);
            $this->db->having('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->having('salary <=', 80000);
            $this->db->having('salary >=', 70000);
         }
      }
      $this->db->having("distance<", '7000');
      $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      if($query->num_rows() > 0) {
         return $query->result_array();
      } else {

         $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
         $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
         
         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
           $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
           $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
           $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
           $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
         }

         $this->db->where("job_posting.jobexpire >=", $cDate);
         if($id!=0) {
            $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
         }
         if(strlen($expfilter) > 0){
            $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
         }

         if(!empty($getParamType)) {
            $this->db->where("job_posting.category", $getParamType);
         }else if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
            $this->db->where("job_posting.category", $filterdata['jobcategory']);

         } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
            $this->db->where("job_posting.level", $filterdata['joblevel']);

         } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
            $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
         }

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
            $toppicks = $filterdata['toppicks'];
            $this->db->where_in('job_toppicks.picks_id', $toppicks);
         }

         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
            $allowances = $filterdata['allowances'];
            $this->db->where_in('job_allowances.allowances_id', $allowances);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $medical = $filterdata['medical'];
            $this->db->where_in('job_medical.medical_id', $medical);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $workshift = $filterdata['workshift'];
            $this->db->where_in('job_workshift.workshift_id', $workshift);
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
            $leaves = $filterdata['leaves'];
            $this->db->where_in('job_leaves.leaves_id', $leaves);
         }

         if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
            //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
         } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
            //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
         }

         if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
            
            if($filterdata['basicSalary'] == 5) {
               $this->db->having('salary <=', 5000);

            } else if($filterdata['basicSalary'] == 10) {
               $this->db->having('salary <=', 10000);
               $this->db->having('salary >=', 5000);

            } else if($filterdata['basicSalary'] == 20) {
               $this->db->having('salary <=', 20000);
               $this->db->having('salary >=', 10000);

            } else if($filterdata['basicSalary'] == 30) {
               $this->db->having('salary <=', 30000);
               $this->db->having('salary >=', 20000);
            
            } else if($filterdata['basicSalary'] == 40) {
               $this->db->having('salary <=', 40000);
               $this->db->having('salary >=', 30000);
            
            } else if($filterdata['basicSalary'] == 50) {
               $this->db->having('salary <=', 50000);
               $this->db->having('salary >=', 40000);
            
            } else if($filterdata['basicSalary'] == 70) {
               $this->db->having('salary <=', 70000);
               $this->db->having('salary >=', 50000);
            
            } else if($filterdata['basicSalary'] == 80) {
               $this->db->having('salary <=', 80000);
               $this->db->having('salary >=', 70000);
            }
         }
         $this->db->having("distance<", '7000');
         $this->db->group_by("job_posting.company_id");
         $query = $this->db->get();
         if($query->num_rows() > 0) {
            return $query->result_array();
         } else {

            $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl, job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, job_posting.actively, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'job_posting.recruiter_id = recruiter_details.recruiter_id');
            $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
            $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
            
            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
              $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
              $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
              $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
              $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }

            $this->db->where("job_posting.jobexpire >=", $cDate);
            if($id!=0) {
               $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            }
            if(strlen($expfilter) > 0){
               $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
            }

            if(!empty($getParamType)) {
               $this->db->where("job_posting.category", $getParamType);
            }else if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
               $this->db->where("job_posting.category", $filterdata['jobcategory']);

            } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
               $this->db->where("job_posting.level", $filterdata['joblevel']);

            } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
               $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
            }

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
               $toppicks = $filterdata['toppicks'];
               $this->db->where_in('job_toppicks.picks_id', $toppicks);
            }

            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
               $allowances = $filterdata['allowances'];
               $this->db->where_in('job_allowances.allowances_id', $allowances);
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
               $medical = $filterdata['medical'];
               $this->db->where_in('job_medical.medical_id', $medical);
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
               $workshift = $filterdata['workshift'];
               $this->db->where_in('job_workshift.workshift_id', $workshift);
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
               $leaves = $filterdata['leaves'];
               $this->db->where_in('job_leaves.leaves_id', $leaves);
            }

            if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
               //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
            } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
               //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
            }

            if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
               
               if($filterdata['basicSalary'] == 5) {
                  $this->db->having('salary <=', 5000);

               } else if($filterdata['basicSalary'] == 10) {
                  $this->db->having('salary <=', 10000);
                  $this->db->having('salary >=', 5000);

               } else if($filterdata['basicSalary'] == 20) {
                  $this->db->having('salary <=', 20000);
                  $this->db->having('salary >=', 10000);

               } else if($filterdata['basicSalary'] == 30) {
                  $this->db->having('salary <=', 30000);
                  $this->db->having('salary >=', 20000);
               
               } else if($filterdata['basicSalary'] == 40) {
                  $this->db->having('salary <=', 40000);
                  $this->db->having('salary >=', 30000);
               
               } else if($filterdata['basicSalary'] == 50) {
                  $this->db->having('salary <=', 50000);
                  $this->db->having('salary >=', 40000);
               
               } else if($filterdata['basicSalary'] == 70) {
                  $this->db->having('salary <=', 70000);
                  $this->db->having('salary >=', 50000);
               
               } else if($filterdata['basicSalary'] == 80) {
                  $this->db->having('salary <=', 80000);
                  $this->db->having('salary >=', 70000);
               }
            }
            $this->db->having("distance<", '7000');
            $this->db->group_by("job_posting.recruiter_id");
            $query = $this->db->get();
            return $query->result_array();
         }
      }
   }

   public function categoryjob_fetch_latlong($id, $expfilter, $userLat, $userLong, $getParamType, $filterdata) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();

      for($ikl = 0;$ikl<=$expfilter;$ikl++) {
         $uniquerarray[] = $ikl;
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire,job_posting.actively, job_posting.mode, job_posting.modeurl, job_posting.chatbot,recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      
      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }

      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }

      if(!empty($getParamType)) {
         $this->db->where("job_posting.category", $getParamType);
      }else if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
         $this->db->where("job_posting.category", $filterdata['jobcategory']);

      } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
         $this->db->where("job_posting.level", $filterdata['joblevel']);

      } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         $this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
         //$this->db->or_like("recruiter.cname", $filterdata['cname'], 'both');
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $this->db->where_in('job_toppicks.picks_id', $toppicks);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $this->db->where_in('job_allowances.allowances_id', $allowances);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $this->db->where_in('job_workshift.workshift_id', $workshift);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         $this->db->where_in('job_leaves.leaves_id', $leaves);
      }

      if(isset($filterdata['language']) && !empty($filterdata['language'])) {
         $this->db->where('job_posting.language', $filterdata['language']);
      }
      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->having('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->having('salary <=', 10000);
            $this->db->having('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->having('salary <=', 20000);
            $this->db->having('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->having('salary <=', 30000);
            $this->db->having('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->having('salary <=', 40000);
            $this->db->having('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->having('salary <=', 50000);
            $this->db->having('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->having('salary <=', 70000);
            $this->db->having('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->having('salary <=', 80000);
            $this->db->having('salary >=', 70000);
         }
      }
      
      $this->db->having("distance<", '7000');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      if($query->num_rows() > 0) {
         return $query->result_array();
      } else {

         $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire,job_posting.actively,job_posting.mode, job_posting.modeurl, job_posting.chatbot,recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
         $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
         
         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
           $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
           $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
           $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
           $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
         }

         $this->db->where("job_posting.jobexpire >=", $cDate);
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
         if(strlen($expfilter) > 0){
            $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
         }

         if(!empty($getParamType)) {
            $this->db->where("job_posting.category", $getParamType);
         }else if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
            $this->db->where("job_posting.category", $filterdata['jobcategory']);

         } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
            $this->db->where("job_posting.level", $filterdata['joblevel']);

         } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
            //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
            $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
         }

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
            $toppicks = $filterdata['toppicks'];
            $this->db->where_in('job_toppicks.picks_id', $toppicks);
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
            $allowances = $filterdata['allowances'];
            $this->db->where_in('job_allowances.allowances_id', $allowances);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $medical = $filterdata['medical'];
            $this->db->where_in('job_medical.medical_id', $medical);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $workshift = $filterdata['workshift'];
            $this->db->where_in('job_workshift.workshift_id', $workshift);
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
            $leaves = $filterdata['leaves'];
            $this->db->where_in('job_leaves.leaves_id', $leaves);
         }

         if(isset($filterdata['language']) && !empty($filterdata['language'])) {
            $this->db->where('job_posting.language', $filterdata['language']);
         }
         if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
            //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
         } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
            //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
         }

         if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
            
            if($filterdata['basicSalary'] == 5) {
               $this->db->having('salary <=', 5000);

            } else if($filterdata['basicSalary'] == 10) {
               $this->db->having('salary <=', 10000);
               $this->db->having('salary >=', 5000);

            } else if($filterdata['basicSalary'] == 20) {
               $this->db->having('salary <=', 20000);
               $this->db->having('salary >=', 10000);

            } else if($filterdata['basicSalary'] == 30) {
               $this->db->having('salary <=', 30000);
               $this->db->having('salary >=', 20000);
            
            } else if($filterdata['basicSalary'] == 40) {
               $this->db->having('salary <=', 40000);
               $this->db->having('salary >=', 30000);
            
            } else if($filterdata['basicSalary'] == 50) {
               $this->db->having('salary <=', 50000);
               $this->db->having('salary >=', 40000);
            
            } else if($filterdata['basicSalary'] == 70) {
               $this->db->having('salary <=', 70000);
               $this->db->having('salary >=', 50000);
            
            } else if($filterdata['basicSalary'] == 80) {
               $this->db->having('salary <=', 80000);
               $this->db->having('salary >=', 70000);
            }
         }
         
         $this->db->having("distance<", '7000');
         $this->db->group_by("exp_with_salary.jobpost_id");
         $query = $this->db->get();
         if($query->num_rows() > 0) {
            return $query->result_array();
         } else {

            $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire,job_posting.actively,job_posting.mode, job_posting.modeurl, job_posting.chatbot,recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'job_posting.recruiter_id = recruiter_details.recruiter_id');
            $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
            $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
            
            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
              $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
              $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
              $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
              $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }

            $this->db->where("job_posting.jobexpire >=", $cDate);
            $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            if(strlen($expfilter) > 0){
               $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
            }

            if(!empty($getParamType)) {
               $this->db->where("job_posting.category", $getParamType);
            }else if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
               $this->db->where("job_posting.category", $filterdata['jobcategory']);

            } else if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
               $this->db->where("job_posting.level", $filterdata['joblevel']);

            } else if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
               //$this->db->like("job_posting.jobtitle", $filterdata['cname'], 'both');
               $this->db->like("recruiter.cname", $filterdata['cname'], 'both');
            }

            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
               $toppicks = $filterdata['toppicks'];
               $this->db->where_in('job_toppicks.picks_id', $toppicks);
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
               $allowances = $filterdata['allowances'];
               $this->db->where_in('job_allowances.allowances_id', $allowances);
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
               $medical = $filterdata['medical'];
               $this->db->where_in('job_medical.medical_id', $medical);
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
               $workshift = $filterdata['workshift'];
               $this->db->where_in('job_workshift.workshift_id', $workshift);
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
               $leaves = $filterdata['leaves'];
               $this->db->where_in('job_leaves.leaves_id', $leaves);
            }

            if(isset($filterdata['language']) && !empty($filterdata['language'])) {
               $this->db->where('job_posting.language', $filterdata['language']);
            }
            if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
               //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
            } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
               //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
            }

            if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
               
               if($filterdata['basicSalary'] == 5) {
                  $this->db->having('salary <=', 5000);

               } else if($filterdata['basicSalary'] == 10) {
                  $this->db->having('salary <=', 10000);
                  $this->db->having('salary >=', 5000);

               } else if($filterdata['basicSalary'] == 20) {
                  $this->db->having('salary <=', 20000);
                  $this->db->having('salary >=', 10000);

               } else if($filterdata['basicSalary'] == 30) {
                  $this->db->having('salary <=', 30000);
                  $this->db->having('salary >=', 20000);
               
               } else if($filterdata['basicSalary'] == 40) {
                  $this->db->having('salary <=', 40000);
                  $this->db->having('salary >=', 30000);
               
               } else if($filterdata['basicSalary'] == 50) {
                  $this->db->having('salary <=', 50000);
                  $this->db->having('salary >=', 40000);
               
               } else if($filterdata['basicSalary'] == 70) {
                  $this->db->having('salary <=', 70000);
                  $this->db->having('salary >=', 50000);
               
               } else if($filterdata['basicSalary'] == 80) {
                  $this->db->having('salary <=', 80000);
                  $this->db->having('salary >=', 70000);
               }
            }
            
            $this->db->having("distance<", '7000');
            $this->db->group_by("exp_with_salary.jobpost_id");
            $query = $this->db->get();
            return $query->result_array();
         }
      }
   }


}
