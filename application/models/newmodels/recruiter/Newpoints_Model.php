<?php
ob_start();
class Newpoints_Model extends CI_Model {

   function __construct() { 
      parent::__construct(); 
   }


   public function mainrecruiter_lists($userLat, $userLong) {
      $this->db->select("recruiter.id, recruiter.cname, recruiter.top_recruiter, recruiter.headquarter, recruiter.founded, recruiter.size, recruiter.num_sites, recruiter_details.companyPic");
      $this->db->from("recruiter");
      $this->db->join("recruiter_details","recruiter_details.recruiter_id = recruiter.id");
      $this->db->where('recruiter.parent_id', 0);
      $this->db->where('recruiter.active', 1);
      $this->db->where('recruiter.top_recruiter >=', 1);
      //$this->db->order_by('distance','ASC');
      $query = $this->db->get();
      return $query->result_array();
   }


   public function companysite_lists($userLat, $userLong) {
      $this->db->select("recruiter_details.companyPic, recruiter.cname, recruiter.fname, recruiter.lname, recruiter.id, recruiter.parent_id, recruiter.top_recruiter, recruiter_details.site_name, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from("recruiter");
      $this->db->join("recruiter_details","recruiter_details.recruiter_id = recruiter.id");
      $this->db->where('recruiter.parent_id!=', 0);
      $this->db->where('recruiter.active', 1);
      $this->db->where_in('recruiter.label', [0,2]);
      $this->db->having("distance<", '7000');
      $this->db->order_by('distance','ASC');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function companysite_groupby_city() {
      $this->db->select("recruiter_details.companyPic, recruiter.cname, recruiter.fname, recruiter.lname, recruiter.id, recruiter.parent_id, recruiter_details.site_name, recruiter_details.city");
      $this->db->from("recruiter");
      $this->db->join("recruiter_details","recruiter_details.recruiter_id = recruiter.id");
      $this->db->where('recruiter.parent_id!=', 0);
      $this->db->where('recruiter.active', 1);
      $this->db->where_in('recruiter.label', [0,2]);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function companysite_lists_byid($id) {
      $this->db->select("recruiter_details.companyPic, recruiter.cname, recruiter.fname, recruiter.lname, recruiter.id, recruiter.parent_id, recruiter_details.site_name");
      $this->db->from("recruiter");
      $this->db->join("recruiter_details","recruiter_details.recruiter_id = recruiter.id");
      $this->db->where('recruiter.id', $id);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function company_parent($compid) {
      $this->db->select("recruiter.cname,recruiter.top_recruiter,recruiter.headquarter,recruiter.founded,recruiter.size,recruiter.num_sites");
      $this->db->from("recruiter");
      $this->db->where('recruiter.id', $compid);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function company_parent_toppicks($compid) {
      $this->db->select("picks_id");
      $this->db->from("recruiter_toppicks");
      $this->db->where('recruiter_id', $compid);
      $this->db->limit(2);
      $query = $this->db->get();
      return $query->result_array();
   }

   // public function companysite_jobcount($id, $typeid , $expfilter, $userLat, $userLong) {
   //    $currentDate = date('Y-m-d');
   //    $uniquerarray = array();
   //    if(strlen($expfilter) > 0){
   //       for($ikl = 0;$ikl<=$expfilter;$ikl++) {
   //          $uniquerarray[] = $ikl;
   //       }
   //    }
   //    $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.recruiter_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
   //    $this->db->from('job_posting');
   //    $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
   //    $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
   //    $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
   //    $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
   //    $this->db->where("job_posting.jobexpire >=", $currentDate);
   //    $this->db->where("job_posting.company_id", $typeid);
   //    if(strlen($expfilter) > 0){
   //       $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
   //    }
   //    $this->db->having("distance<", '5000');
   //    $this->db->order_by('salary','desc');
   //    $this->db->group_by("exp_with_salary.jobpost_id");

   //    return $this->db->count_all_results();
   // }

   public function companysite_jobcount($id, $typeid, $expfilter, $userLat, $userLong) {

      $currentDate = date('Y-m-d');
      $uniquerarray = array();
      for($ikl = 0;$ikl<=$expfilter;$ikl++) {
         $uniquerarray[] = $ikl;
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire,job_posting.mode,  recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(!empty($expfilter)) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_posting.jobexpire >=", $currentDate);
      $this->db->where("job_posting.company_id", $typeid);
      $this->db->group_by("exp_with_salary.jobpost_id");
      return $this->db->count_all_results();
   }

   public function companyjob_fetch_latlong($id, $expfilter, $typeid, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      for($ikl = 0;$ikl<=$expfilter;$ikl++) {
         $uniquerarray[] = $ikl;
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire,job_posting.mode, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id != 0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.company_id", $typeid);
      if(!empty($expfilter)) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      //$this->db->order_by('job_posting.id','desc');
      $this->db->order_by('distance','asc');
      $this->db->group_by("exp_with_salary.jobpost_id");
      //$this->db->limit(10);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function recruitercategory_jobcount($id, $company_id, $typeid , $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire,job_posting.mode, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id != 0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.category", $typeid);
      $this->db->where("job_posting.recruiter_id", $company_id);
      if(!empty($expfilter)) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->having("distance <","5000");
      $this->db->order_by('distance','asc');
      $this->db->group_by("exp_with_salary.jobpost_id");
      return $this->db->count_all_results();
   }

   public function companycategory_jobcount($id, $company_id, $typeid , $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.opening, job_posting.boost_status ,job_posting.jobexpire,job_posting.mode, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id != 0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.category", $typeid);
      $this->db->where("job_posting.company_id", $company_id);
      if(!empty($expfilter)) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      //$this->db->order_by('job_posting.id','desc');
      $this->db->having("distance <","5000");
      $this->db->order_by('distance','asc');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function category_jobcount($id, $typeid , $expfilter, $userLat, $userLong) {

      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id != 0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.category", $typeid);
      if(!empty($expfilter)) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->having("distance <","5000");
      $this->db->order_by('distance','asc');
      $this->db->group_by("exp_with_salary.jobpost_id");
      return $this->db->count_all_results();
   }

   public function categoryjob_fetch_latlong($id, $expfilter, $typeid, $userLat, $userLong, $filterdata) {

      $cDate = date('Y-m-d');
      $uniquerarray = array();
      for($ikl = 0;$ikl<=$expfilter;$ikl++) {
         $uniquerarray[] = $ikl;
      }

      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire,job_posting.mode,job_posting.modeurl, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      
      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }

      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.category", $typeid);
      if(!empty($expfilter)) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $categories1 = explode(",", $toppicks);
         $this->db->where_in('job_toppicks.picks_id', $categories1);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $categories2 = explode(",", $allowances);
         $this->db->or_where_in('job_allowances.allowances_id', $categories2);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $categories3 = explode(",", $medical);
         $this->db->or_where_in('job_medical.medical_id', $categories3);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $categories4 = explode(",", $workshift);
         $this->db->or_where_in('job_workshift.workshift_id', $categories4);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         $categories5 = explode(",", $leaves);
         $this->db->or_where_in('job_leaves.leaves_id', $categories5);
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->where('exp_with_salary.basicsalary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->where('exp_with_salary.basicsalary <=', 10000);
            $this->db->where('exp_with_salary.basicsalary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->where('exp_with_salary.basicsalary <=', 20000);
            $this->db->where('exp_with_salary.basicsalary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->where('exp_with_salary.basicsalary <=', 30000);
            $this->db->where('exp_with_salary.basicsalary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->where('exp_with_salary.basicsalary <=', 40000);
            $this->db->where('exp_with_salary.basicsalary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->where('exp_with_salary.basicsalary <=', 50000);
            $this->db->where('exp_with_salary.basicsalary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->where('exp_with_salary.basicsalary <=', 70000);
            $this->db->where('exp_with_salary.basicsalary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->where('exp_with_salary.basicsalary <=', 80000);
            $this->db->where('exp_with_salary.basicsalary >=', 70000);
         }
      }

      if(isset($filterdata['language']) && !empty($filterdata['language'])) {
         $this->db->where('job_posting.language', $filterdata['language']);
      }
      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         $this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         $this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

      $this->db->having("distance <","5000");
      
      if(isset($filterdata['salarySort']) && !empty($filterdata['salarySort']) && $filterdata['salarySort'] == 1) {
         $this->db->order_by('salary', "DESC");
      } else {
         $this->db->order_by('distance','asc');
      }

      $this->db->group_by("exp_with_salary.jobpost_id");
      //$this->db->limit(10);
      $query = $this->db->get();
      return $query->result_array();
   }


   public function city_jobcount($id, $cityname , $expfilter, $userLat, $userLong) {

      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      $this->db->where("job_posting.jobexpire >=", $cDate);
      //$this->db->like("recruiter_details.city", $cityname, 'both');
      $this->db->like("recruiter_details.address", $cityname, 'both');
      if($id != 0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(!empty($expfilter)) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->having("distance <","7000");
      $this->db->order_by('distance','asc');
      $this->db->group_by("exp_with_salary.jobpost_id");
      return $this->db->count_all_results();
   }

   public function nearbyjob_fetch_latlong($id, $userLat, $userLong, $expfilter, $filterdata) {
      $userLat = (double)$userLat;
      $userLong = (double)$userLong;

      $cDate = date('Y-m-d');
      $uniquearraybyother = array();
      for($ityu = $expfilter; $ityu >-1; $ityu--) {
         $uniquearraybyother[] = $ityu;
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }

      $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.jobPitch,job_posting.mode, job_posting.modeurl, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id,job_posting.boost_status, recruiter_details.latitude,recruiter_details.longitude, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }

      $this->db->where_in("job_posting.id", $filterdata['jid_array']);
      
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
      }
      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $categories1 = explode(",", $toppicks);
         $this->db->where_in('job_toppicks.picks_id', $categories1);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $categories2 = explode(",", $allowances);
         $this->db->or_where_in('job_allowances.allowances_id', $categories2);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $categories3 = explode(",", $medical);
         $this->db->or_where_in('job_medical.medical_id', $categories3);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $categories4 = explode(",", $workshift);
         $this->db->or_where_in('job_workshift.workshift_id', $categories4);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         $categories5 = explode(",", $leaves);
         $this->db->or_where_in('job_leaves.leaves_id', $categories5);
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->where('exp_with_salary.basicsalary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->where('exp_with_salary.basicsalary <=', 10000);
            $this->db->where('exp_with_salary.basicsalary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->where('exp_with_salary.basicsalary <=', 20000);
            $this->db->where('exp_with_salary.basicsalary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->where('exp_with_salary.basicsalary <=', 30000);
            $this->db->where('exp_with_salary.basicsalary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->where('exp_with_salary.basicsalary <=', 40000);
            $this->db->where('exp_with_salary.basicsalary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->where('exp_with_salary.basicsalary <=', 50000);
            $this->db->where('exp_with_salary.basicsalary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->where('exp_with_salary.basicsalary <=', 70000);
            $this->db->where('exp_with_salary.basicsalary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->where('exp_with_salary.basicsalary <=', 80000);
            $this->db->where('exp_with_salary.basicsalary >=', 70000);
         }
      }

      if(isset($filterdata['language']) && !empty($filterdata['language'])) {
         $this->db->where('job_posting.language', $filterdata['language']);
      }
      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         $this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         $this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

      $this->db->having("distance <","7000");
      
      if(isset($filterdata['salarySort']) && !empty($filterdata['salarySort']) && $filterdata['salarySort'] == 1) {
         $this->db->order_by('salary', "DESC");
      } else {
         $this->db->order_by('distance','asc');
      }

      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function job_toppicks($id) {

      $this->db->select("id, picks_id");
      $this->db->where("jobpost_id", $id);
      $query = $this->db->get('job_toppicks');
      return $query->result_array();
   }

   public function job_toppicksViewall($id, $typeid) {

      $this->db->select("id, picks_id");
      $this->db->where("jobpost_id", $id);
      $this->db->where("picks_id", $typeid);
      $query = $this->db->get('job_toppicks');
      return $query->result_array();
   }

   public function feturedtoppicks_fetch_latlong($id, $expfilter, $userLat, $userLong, $getParamType, $filterdata) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();

      for($ikl = 0;$ikl<=$expfilter;$ikl++) {
         $uniquerarray[] = $ikl;
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire,job_posting.mode, job_posting.modeurl,recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }

      $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",$getParamType);

      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $categories2 = explode(",", $allowances);
         $this->db->or_where_in('job_allowances.allowances_id', $categories2);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $categories3 = explode(",", $medical);
         $this->db->or_where_in('job_medical.medical_id', $categories3);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $categories4 = explode(",", $workshift);
         $this->db->or_where_in('job_workshift.workshift_id', $categories4);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         $categories5 = explode(",", $leaves);
         $this->db->or_where_in('job_leaves.leaves_id', $categories5);
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->where('exp_with_salary.basicsalary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->where('exp_with_salary.basicsalary <=', 10000);
            $this->db->where('exp_with_salary.basicsalary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->where('exp_with_salary.basicsalary <=', 20000);
            $this->db->where('exp_with_salary.basicsalary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->where('exp_with_salary.basicsalary <=', 30000);
            $this->db->where('exp_with_salary.basicsalary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->where('exp_with_salary.basicsalary <=', 40000);
            $this->db->where('exp_with_salary.basicsalary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->where('exp_with_salary.basicsalary <=', 50000);
            $this->db->where('exp_with_salary.basicsalary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->where('exp_with_salary.basicsalary <=', 70000);
            $this->db->where('exp_with_salary.basicsalary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->where('exp_with_salary.basicsalary <=', 80000);
            $this->db->where('exp_with_salary.basicsalary >=', 70000);
         }
      }

      if(isset($filterdata['language']) && !empty($filterdata['language'])) {
         $this->db->where('job_posting.language', $filterdata['language']);
      }
      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         $this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         $this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }
      
      $this->db->having("distance<", '6000');
      
      if(isset($filterdata['salarySort']) && !empty($filterdata['salarySort']) && $filterdata['salarySort'] == 1) {
         $this->db->order_by('salary', "DESC");
      } else {
         $this->db->order_by('distance','asc');
      }

      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function feturedtoppicksallowances_fetch_latlong($id, $expfilter, $userLat, $userLong, $getParamType, $filterdata) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();

      for($ikl = 0;$ikl<=$expfilter;$ikl++) {
         $uniquerarray[] = $ikl;
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire,job_posting.mode, job_posting.modeurl,recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      
      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }

      $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $categories1 = explode(",", $toppicks);
         $this->db->where_in('job_toppicks.picks_id', $categories1);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $categories3 = explode(",", $medical);
         $this->db->or_where_in('job_medical.medical_id', $categories3);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $categories4 = explode(",", $workshift);
         $this->db->or_where_in('job_workshift.workshift_id', $categories4);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         $categories5 = explode(",", $leaves);
         $this->db->or_where_in('job_leaves.leaves_id', $categories5);
      }

      $this->db->where("job_allowances.allowances_id",7);

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->where('exp_with_salary.basicsalary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->where('exp_with_salary.basicsalary <=', 10000);
            $this->db->where('exp_with_salary.basicsalary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->where('exp_with_salary.basicsalary <=', 20000);
            $this->db->where('exp_with_salary.basicsalary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->where('exp_with_salary.basicsalary <=', 30000);
            $this->db->where('exp_with_salary.basicsalary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->where('exp_with_salary.basicsalary <=', 40000);
            $this->db->where('exp_with_salary.basicsalary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->where('exp_with_salary.basicsalary <=', 50000);
            $this->db->where('exp_with_salary.basicsalary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->where('exp_with_salary.basicsalary <=', 70000);
            $this->db->where('exp_with_salary.basicsalary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->where('exp_with_salary.basicsalary <=', 80000);
            $this->db->where('exp_with_salary.basicsalary >=', 70000);
         }
      }

      if(isset($filterdata['language']) && !empty($filterdata['language'])) {
         $this->db->where('job_posting.language', $filterdata['language']);
      }
      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         $this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         $this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }
      
      $this->db->having("distance<", '6000');
      
      if(isset($filterdata['salarySort']) && !empty($filterdata['salarySort']) && $filterdata['salarySort'] == 1) {
         $this->db->order_by('salary', "DESC");
      } else {
         $this->db->order_by('distance','asc');
      }

      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      return $query->result_array();
   }


   public function fetchnews() {
      $this->db->order_by('id','desc');
      $query = $this->db->get("news_list");
      return $query->result_array();
   }

   public function fetchvideos() {
      $this->db->where('status',2);
      $this->db->order_by('id','desc');
      $query = $this->db->get("videoadvertise_list");
      return $query->result_array();
   }
}
?>