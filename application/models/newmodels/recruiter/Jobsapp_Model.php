<?php
ob_start();
class Jobsapp_Model extends CI_Model {

    function __construct() { 
    	parent::__construct(); 
    }

    public function homelist_hotjob_groupby($id,$expfilter, $userLat, $userLong, $filterdata, $default_distance) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');

      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->having("distance<", $default_distance);
      $this->db->order_by('distance','asc');
      $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function homelist_boostjob($id,$expfilter, $userLat, $userLong, $default_distance) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, job_posting.mode, job_posting.modeurl, job_posting.chatbot, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.boost_status !=", 0);
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->having("distance<", $default_distance);
      $this->db->order_by('distance','asc');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function homelist_hotjobs($id,$expfilter, $userLat, $userLong, $default_distance) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.boost_status", 0);
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->having("distance<", $default_distance);
      $this->db->order_by('distance','asc');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->limit(80);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function homelist_information_technology_groupby($id,$expfilter, $userLat, $userLong, $filterdata, $default_distance) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');

       $this->db->where("job_posting.jobexpire >=", $cDate);
       $this->db->where("job_posting.category", 22);
        if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
        }

       $this->db->having("distance<", $default_distance);
       $this->db->order_by('distance','asc');   
       $this->db->group_by("job_posting.company_id");
       $query = $this->db->get();
       return $query->result_array();
   }

   public function homelist_information_technology($id,$expfilter, $userLat, $userLong, $default_distance) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.category", 22);
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->having("distance<", $default_distance);
      $this->db->order_by('distance','asc');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->limit(100);
      $query = $this->db->get();
      return $query->result_array();
   }


   public function homelist_nursing_groupby($id,$expfilter, $userLat, $userLong, $filterdata, $default_distance) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');

       $this->db->where("job_posting.jobexpire >=", $cDate);
       $this->db->where_in("job_posting.subcategory", [41,45]);
        if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
        }

       $this->db->having("distance<", $default_distance);
       $this->db->order_by('distance','asc');   
       $this->db->group_by("job_posting.company_id");
       $query = $this->db->get();
       return $query->result_array();
   }

   public function homelist_nursing($id,$expfilter, $userLat, $userLong, $default_distance) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where_in("job_posting.subcategory", [41,45]);
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->having("distance<", $default_distance);
      $this->db->order_by('distance','asc');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->limit(100);
      $query = $this->db->get();
      return $query->result_array();
   }


   public function homelist_no_experience_groupby($id,$expfilter, $userLat, $userLong, $filterdata, $default_distance) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');

       $this->db->where("job_posting.jobexpire >=", $cDate);
       $this->db->where("job_posting.experience", "No Experience");
        if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
        }

       $this->db->having("distance<", $default_distance);
       $this->db->order_by('distance','asc');   
       $this->db->group_by("job_posting.company_id");
       $query = $this->db->get();
       return $query->result_array();
   }

   public function homelist_no_experience($id,$expfilter, $userLat, $userLong, $default_distance) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.experience", "No Experience");
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->having("distance<", $default_distance);
      $this->db->order_by('distance','asc');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->limit(100);
      $query = $this->db->get();
      return $query->result_array();
   }

   

   public function homelist_nearby_groupby($id, $userLat, $userLong, $expfilter, $filterdata, $default_distance) {
		$userLat = (double)$userLat;
		$userLong = (double)$userLong;
		$cDate = date('Y-m-d');

		$uniquearraybyother = array();
		if(strlen($expfilter) > 0) {
	   		for($ityu = $expfilter; $ityu >-1; $ityu--) {
	   		    $uniquearraybyother[] = $ityu;
	   		}
		}

		$this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, recruiter_details.latitude,recruiter_details.longitude,  (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

		$this->db->from('job_posting');
		$this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
		$this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
		$this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');

      	$this->db->where("job_posting.jobexpire >=", $cDate);
		if($id != 0) {
		    $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
		}
		if(strlen($expfilter) > 0) {
		    $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
		}
		$this->db->having("distance <",$default_distance);
        $this->db->order_by('distance','asc');   
      	$this->db->group_by("job_posting.company_id");
		$query = $this->db->get();
		return $query->result_array();
   }

   public function homelist_nearby($id, $userLat, $userLong, $expfilter, $default_distance) {
      $userLat = (double)$userLat;
      $userLong = (double)$userLong;

      $cDate = date('Y-m-d');
      $uniquearraybyother = array();
      if(strlen($expfilter) > 0) {
         for($ityu = $expfilter; $ityu >-1; $ityu--) {
            $uniquearraybyother[] = $ityu;
         }
      }
      $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, recruiter_details.latitude,recruiter_details.longitude, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if($id != 0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
      }
      $this->db->having("distance <",$default_distance);
      $this->db->order_by("distance",'ASC');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->limit(50);
      $query = $this->db->get();
      return $query->result_array();
   }


   public function homelist_actively_groupby($id, $userLat, $userLong, $expfilter, $filterdata, $default_distance) {
      $userLat = (double)$userLat;
      $userLong = (double)$userLong;
      $cDate = date('Y-m-d');

      $uniquearraybyother = array();
      if(strlen($expfilter) > 0) {
            for($ityu = $expfilter; $ityu >-1; $ityu--) {
                $uniquearraybyother[] = $ityu;
            }
      }

      $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, recruiter_details.latitude,recruiter_details.longitude,  (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');

         $this->db->where("job_posting.actively", 1);
         $this->db->where("job_posting.jobexpire >=", $cDate);
      if($id != 0) {
          $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
          $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
      }
      $this->db->having("distance <",$default_distance);
        $this->db->order_by('distance','asc');   
         $this->db->group_by("job_posting.company_id");
         $this->db->limit(10);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function homelist_actively($id, $userLat, $userLong, $expfilter, $default_distance) {
      $userLat = (double)$userLat;
      $userLong = (double)$userLong;

      $cDate = date('Y-m-d');
      $uniquearraybyother = array();
      if(strlen($expfilter) > 0) {
         for($ityu = $expfilter; $ityu >-1; $ityu--) {
            $uniquearraybyother[] = $ityu;
         }
      }
      $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, recruiter_details.latitude,recruiter_details.longitude, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.actively", 1);
      if($id != 0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
      }
      $this->db->having("distance <",$default_distance);
      $this->db->order_by("distance",'ASC');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->limit(10);
      $query = $this->db->get();
      return $query->result_array();
   }

   
   public function homelist_leadership_groupby($id,$expfilter, $userLat, $userLong, $filterdata, $default_distance) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');

      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.category", 24);
      if($id != 0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->having("distance<", $default_distance);
      $this->db->order_by('distance','asc');   
      $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      return $query->result_array();
    }

    public function homelist_leadership($id,$expfilter, $userLat, $userLong, $default_distance) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.category", 24);
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->having("distance<", $default_distance);
      $this->db->order_by('distance','asc');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->limit(100);
      $query = $this->db->get();
      return $query->result_array();
   }

    public function homelist_workfromhome($id, $expfilter, $userLat, $userLong, $default_distance) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",7);
      $this->db->having("distance<", $default_distance);
      $this->db->order_by('distance','ASC');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_toppicks.jobpost_id");
      $this->db->limit(50);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function homelist_workfromhome_groupby($id, $expfilter, $userLat, $userLong, $default_distance) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",7);
      $this->db->having("distance<", $default_distance);
      $this->db->order_by('distance','ASC');
      $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      return $query->result_array();
    }

    public function homelist_monthpay($id, $expfilter, $userLat, $userLong, $default_distance) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",6);
      $this->db->having("distance<", $default_distance);
      $this->db->order_by('distance','ASC');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_toppicks.jobpost_id");
      $this->db->limit(50);
      $query = $this->db->get();
      return $query->result_array();
    }

    public function homelist_monthpay_groupby($id, $expfilter, $userLat, $userLong, $default_distance) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",6);
      $this->db->having("distance<", $default_distance);
      $this->db->order_by('distance','ASC');
      $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      return $query->result_array();
    }

    public function homelist_dayshift($id, $expfilter, $userLat, $userLong, $default_distance) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",5);
      $this->db->having("distance<", $default_distance);
      $this->db->order_by('distance','ASC');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_toppicks.jobpost_id");
      $query = $this->db->get();
      return $query->result_array();
   	}

   	public function homelist_dayshift_groupby($id, $expfilter, $userLat, $userLong, $default_distance) {
		$cDate = date('Y-m-d');
		$uniquerarray = array();
		if(strlen($expfilter) > 0){
			 for($ikl = 0;$ikl<=$expfilter;$ikl++) {
			    $uniquerarray[] = $ikl;
			 }
		}
		$this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

		$this->db->from('job_posting');
		$this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
		$this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
		$this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
		$this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
		if($id!=0) {
		 $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
		}
		$this->db->where("job_posting.jobexpire >=", $cDate);
		if(strlen($expfilter) > 0){
		 $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
		}
		$this->db->where("job_toppicks.picks_id",5);
		$this->db->having("distance<", $default_distance);
		$this->db->order_by('distance','ASC');
		$this->db->group_by("job_posting.company_id");
		$query = $this->db->get();
		return $query->result_array();
   	}

   	public function homelist_allowances($id, $expfilter, $userLat, $userLong, $default_distance) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_allowances.allowances_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_allowances.allowances_id",5);
      $this->db->having("distance<", $default_distance);
      $this->db->order_by('distance','ASC');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_allowances.jobpost_id");
      $query = $this->db->get();
      return $query->result_array();
    }

   public function homelist_allowances_groupby($id, $expfilter, $userLat, $userLong, $default_distance) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_allowances.allowances_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_allowances.allowances_id",5);
      $this->db->having("distance<", $default_distance);
      $this->db->order_by('distance','asc'); 
      $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function homelist_dayihmo($id, $expfilter, $userLat, $userLong, $default_distance) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",3);
      $this->db->having("distance<", $default_distance);
      $this->db->order_by('distance','ASC');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_toppicks.jobpost_id");
      $this->db->limit(50);
      $query = $this->db->get();
      return $query->result_array();
   	}

   	public function homelist_dayihmo_groupby($id, $expfilter, $userLat, $userLong, $default_distance) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",3);
      $this->db->having("distance<", $default_distance);
      $this->db->order_by('distance','ASC');
      $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      return $query->result_array();
   	}

   	public function homelist_freefood($id, $expfilter, $userLat, $userLong, $default_distance) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",2);
      $this->db->having("distance<", $default_distance);
      $this->db->order_by('distance','ASC');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_toppicks.jobpost_id");
      $this->db->limit(50);
      $query = $this->db->get();
      return $query->result_array();
   	}

   	public function homelist_freefood_groupby($id, $expfilter, $userLat, $userLong, $default_distance) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",2);
      $this->db->having("distance<", $default_distance);
      $this->db->order_by('distance','ASC');
      $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      return $query->result_array();
   	}

   	public function homelist_bonus($id, $expfilter, $userLat, $userLong, $default_distance) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",1);
      $this->db->having("distance<", $default_distance);
      $this->db->order_by('distance','ASC');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_toppicks.jobpost_id");
      $this->db->limit(50);
      $query = $this->db->get();
      return $query->result_array();
   	}

   	public function homelist_bonus_groupby($id, $expfilter, $userLat, $userLong, $default_distance) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",1);
      $this->db->having("distance<", $default_distance);
      $this->db->order_by('distance','ASC');
      $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      return $query->result_array();
   }


   public function job_fetch_homeforappnearby_groupby($id, $userLat, $userLong, $expfilter, $filterdata, $default_distance) {
		$userLat = (double)$userLat;
		$userLong = (double)$userLong;

		$cDate = date('Y-m-d');
		$uniquearraybyother = array();
		if(strlen($expfilter) > 0) {
	   		for($ityu = $expfilter; $ityu >-1; $ityu--) {
	   		    $uniquearraybyother[] = $ityu;
	   		}
		}

		if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
        }

		$this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, recruiter_details.latitude,recruiter_details.longitude, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
		$this->db->from('job_posting');
		$this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
		$this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
		$this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');

		if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
		$this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
		$this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
		$this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
		$this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
	        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
	    }

      	$this->db->where("job_posting.jobexpire >=", $cDate);
		if($id != 0) {
		    $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
		}
		if(strlen($expfilter) > 0) {
		    $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
		}

		if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
		$toppicks = $filterdata['toppicks'];
		//$categories1 = explode(",", $toppicks);
		$this->db->where_in('job_toppicks.picks_id', $toppicks);
		}
		if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
		$allowances = $filterdata['allowances'];
		//$categories2 = explode(",", $allowances);
		$this->db->where_in('job_allowances.allowances_id', $allowances);
		}
		if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
		$medical = $filterdata['medical'];
		//$categories3 = explode(",", $medical);
		$this->db->where_in('job_medical.medical_id', $medical);
		}
		if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
		$workshift = $filterdata['workshift'];
		//$categories4 = explode(",", $workshift);
		$this->db->where_in('job_workshift.workshift_id', $workshift);
		}
		if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
		$leaves = $filterdata['leaves'];
		//$categories5 = explode(",", $leaves);
		$this->db->where_in('job_leaves.leaves_id', $leaves);
		}
      
		if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
		 	//$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
		} else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
			//$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
		}
		if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
			$this->db->having('salary <=', $filterdata['basicSalary']);
		}
		$this->db->having("distance <",$default_distance);
	   $this->db->group_by("job_posting.company_id");
      if(isset($filterdata['sorting']) && $filterdata['sorting'] == 1) {
         $this->db->order_by("salary","DESC");
      } else {
         $this->db->order_by("distance","ASC");
      }
		$query = $this->db->get();
		return $query->result_array();
   }

	public function nearbyjob_fetch_latlong($id, $userLat, $userLong, $expfilter, $offset, $limit, $filterdata, $default_distance) {
		$userLat = (double)$userLat;
		$userLong = (double)$userLong;

		$cDate = date('Y-m-d');
		$uniquearraybyother = array();
		for($ityu = $expfilter; $ityu >-1; $ityu--) {
		 $uniquearraybyother[] = $ityu;
		}

		if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
        }

		$this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.jobPitch,job_posting.mode, job_posting.modeurl,job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id,job_posting.boost_status, recruiter_details.latitude,recruiter_details.longitude, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

		$this->db->from('job_posting');
		$this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
		$this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
		$this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');

		if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
		$this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
		$this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
		$this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
		$this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
	        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
	    }

		$this->db->where("job_posting.jobexpire >=", $cDate);
		if($id!=0) {
			$this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
		}
		if(strlen($expfilter) > 0) {
		 $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
		}

		if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
		$toppicks = $filterdata['toppicks'];
		//$categories1 = explode(",", $toppicks);
		$this->db->where_in('job_toppicks.picks_id', $toppicks);
		}
		if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
		$allowances = $filterdata['allowances'];
		//$categories2 = explode(",", $allowances);
		$this->db->where_in('job_allowances.allowances_id', $allowances);
		}
		if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
		$medical = $filterdata['medical'];
		//$categories3 = explode(",", $medical);
		$this->db->where_in('job_medical.medical_id', $medical);
		}
		if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
		$workshift = $filterdata['workshift'];
		//$categories4 = explode(",", $workshift);
		$this->db->where_in('job_workshift.workshift_id', $workshift);
		}
		if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
		$leaves = $filterdata['leaves'];
		//$categories5 = explode(",", $leaves);
		$this->db->where_in('job_leaves.leaves_id', $leaves);
		}

		if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
		   //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
		} else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
			//$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
		}

		if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
			$this->db->having('salary <=', $filterdata['basicSalary']);
		}

		$this->db->having("distance <",$default_distance);
		$this->db->group_by("exp_with_salary.jobpost_id");
		if(isset($filterdata['sorting']) && $filterdata['sorting'] == 1) {
      		$this->db->order_by("salary","DESC");
      	} else {
      		$this->db->order_by("distance","ASC");
      	}
		$this->db->limit($limit, $offset);
		$query = $this->db->get();
		return $query->result_array();
   }


   public function job_fetch_actively_groupby($id, $userLat, $userLong, $expfilter, $filterdata, $default_distance) {
      $userLat = (double)$userLat;
      $userLong = (double)$userLong;

      $cDate = date('Y-m-d');
      $uniquearraybyother = array();
      if(strlen($expfilter) > 0) {
            for($ityu = $expfilter; $ityu >-1; $ityu--) {
                $uniquearraybyother[] = $ityu;
            }
      }

      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
        }

      $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, recruiter_details.latitude,recruiter_details.longitude, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
      $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
      $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
      $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
       }

      $this->db->where("job_posting.actively", 1);
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if($id != 0) {
          $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
          $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
      $toppicks = $filterdata['toppicks'];
      //$categories1 = explode(",", $toppicks);
      $this->db->where_in('job_toppicks.picks_id', $toppicks);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
      $allowances = $filterdata['allowances'];
      //$categories2 = explode(",", $allowances);
      $this->db->where_in('job_allowances.allowances_id', $allowances);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
      $medical = $filterdata['medical'];
      //$categories3 = explode(",", $medical);
      $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
      $workshift = $filterdata['workshift'];
      //$categories4 = explode(",", $workshift);
      $this->db->where_in('job_workshift.workshift_id', $workshift);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
      $leaves = $filterdata['leaves'];
      //$categories5 = explode(",", $leaves);
      $this->db->where_in('job_leaves.leaves_id', $leaves);
      }
      
      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }
      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         $this->db->having('salary <=', $filterdata['basicSalary']);
      }
      $this->db->having("distance <",$default_distance);
      $this->db->group_by("job_posting.company_id");
      $this->db->limit(10);
      
      if(isset($filterdata['sorting']) && $filterdata['sorting'] == 1) {
         $this->db->order_by("salary","DESC");
      } else {
         $this->db->order_by("distance","ASC");
      }
      $query = $this->db->get();
      return $query->result_array();
   }

   public function actively_fetch_latlong($id, $userLat, $userLong, $expfilter, $offset, $limit, $filterdata, $default_distance) {
      $userLat = (double)$userLat;
      $userLong = (double)$userLong;

      $cDate = date('Y-m-d');
      $uniquearraybyother = array();
      for($ityu = $expfilter; $ityu >-1; $ityu--) {
       $uniquearraybyother[] = $ityu;
      }

      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
        }

      $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.jobPitch,job_posting.mode, job_posting.modeurl,job_posting.chatbot, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id,job_posting.boost_status, recruiter_details.latitude,recruiter_details.longitude, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
      $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
      $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
      $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
       }

      $this->db->where("job_posting.actively", 1);
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
       $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
      $toppicks = $filterdata['toppicks'];
      //$categories1 = explode(",", $toppicks);
      $this->db->where_in('job_toppicks.picks_id', $toppicks);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
      $allowances = $filterdata['allowances'];
      //$categories2 = explode(",", $allowances);
      $this->db->where_in('job_allowances.allowances_id', $allowances);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
      $medical = $filterdata['medical'];
      //$categories3 = explode(",", $medical);
      $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
      $workshift = $filterdata['workshift'];
      //$categories4 = explode(",", $workshift);
      $this->db->where_in('job_workshift.workshift_id', $workshift);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
      $leaves = $filterdata['leaves'];
      //$categories5 = explode(",", $leaves);
      $this->db->where_in('job_leaves.leaves_id', $leaves);
      }

      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         $this->db->having('salary <=', $filterdata['basicSalary']);
      }

      $this->db->having("distance <",$default_distance);
      $this->db->group_by("exp_with_salary.jobpost_id");
      if(isset($filterdata['sorting']) && $filterdata['sorting'] == 1) {
            $this->db->order_by("salary","DESC");
         } else {
            $this->db->order_by("distance","ASC");
         }
      $this->db->limit($limit, $offset);
      $query = $this->db->get();
      return $query->result_array();
   }

	public function feturedtoppicks_fetch_all_groupby($id, $expfilter, $userLat, $userLong, $getParamType, $filterdata, $default_distance) {

		$cDate = date('Y-m-d');
		$uniquerarray = array();
		if(strlen($expfilter) > 0){
		 for($ikl = 0;$ikl<=$expfilter;$ikl++) {
		    $uniquerarray[] = $ikl;
		 }
		}

		if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
        }

		$this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

		$this->db->from('job_posting');
		$this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
		$this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
		$this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
		$this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

		if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
		$this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
		$this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
		$this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
	        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
	   }

		if($id!=0) {
			$this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
		}
		$this->db->where("job_posting.jobexpire >=", $cDate);
		if(strlen($expfilter) > 0){
			$this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
		}

		if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
		$toppicks = $filterdata['toppicks'];
		//$categories2 = explode(",", $allowances);
		$this->db->where_in('job_toppicks.picks_id', $toppicks);
		} else {
			$this->db->where("job_toppicks.picks_id",$getParamType);
		}
		if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
		$allowances = $filterdata['allowances'];
		//$categories2 = explode(",", $allowances);
		$this->db->where_in('job_allowances.allowances_id', $allowances);
		}
		if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
		$medical = $filterdata['medical'];
		//$categories3 = explode(",", $medical);
		$this->db->where_in('job_medical.medical_id', $medical);
		}
		if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
		$workshift = $filterdata['workshift'];
		//$categories4 = explode(",", $workshift);
		$this->db->where_in('job_workshift.workshift_id', $workshift);
		}
		if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
		$leaves = $filterdata['leaves'];
		//$categories5 = explode(",", $leaves);
		$this->db->where_in('job_leaves.leaves_id', $leaves);
		}
      
		if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
		   //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
		} else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
			//$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
		}

		if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
			$this->db->having('salary <=', $filterdata['basicSalary']);
		}

	  	$this->db->having("distance<", $default_distance);
		$this->db->group_by("job_posting.company_id");
      if(isset($filterdata['sorting']) && $filterdata['sorting'] == 1) {
         $this->db->order_by("salary","DESC");
      } else {
         $this->db->order_by("distance","ASC");
      }
		$query = $this->db->get();
		return $query->result_array();
	}

	public function feturedtoppicks_fetch_latlong($id, $expfilter, $userLat, $userLong, $getParamType, $offset, $limit, $filterdata, $default_distance) {
		$cDate = date('Y-m-d');
		$uniquerarray = array();

		for($ikl = 0;$ikl<=$expfilter;$ikl++) {
			$uniquerarray[] = $ikl;
		}

		if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
        }

      	$this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire,job_posting.mode, job_posting.modeurl, job_posting.chatbot, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

		$this->db->from('job_posting');
		$this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
		$this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
		$this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
		$this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

		if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
		$this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
		$this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
		$this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
		$this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
	        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
	    }

		if($id!=0) {
			$this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
		}
		$this->db->where("job_posting.jobexpire >=", $cDate);
		if(strlen($expfilter) > 0){
			$this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
		}

		if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
		$toppicks = $filterdata['toppicks'];
		//$categories1 = explode(",", $toppicks);
		$this->db->where_in('job_toppicks.picks_id', $toppicks);
		} else {
			$this->db->where("job_toppicks.picks_id",$getParamType);
		}
		if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
		$allowances = $filterdata['allowances'];
		//$categories2 = explode(",", $allowances);
		$this->db->where_in('job_allowances.allowances_id', $allowances);
		}
		if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
		$medical = $filterdata['medical'];
		//$categories3 = explode(",", $medical);
		$this->db->where_in('job_medical.medical_id', $medical);
		}
		if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
		$workshift = $filterdata['workshift'];
		//$categories4 = explode(",", $workshift);
		$this->db->where_in('job_workshift.workshift_id', $workshift);
		}
		if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
		$leaves = $filterdata['leaves'];
		//$categories5 = explode(",", $leaves);
		$this->db->where_in('job_leaves.leaves_id', $leaves);
		}

		if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
		  //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
		} else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
			//$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
		}

		if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
			$this->db->having('salary <=', $filterdata['basicSalary']);
		}
      	
      	$this->db->having("distance<", $default_distance);
      	$this->db->group_by("exp_with_salary.jobpost_id");
      	if(isset($filterdata['sorting']) && $filterdata['sorting'] == 1) {
      		$this->db->order_by("salary","DESC");
      	} else {
      		$this->db->order_by("distance","ASC");
      	}
      	$this->db->limit($limit, $offset);
      	$query = $this->db->get();
      	return $query->result_array();
    }

    public function feturedtoppicks_fetch_latlong_allowances_groupby($id, $expfilter, $userLat, $userLong, $filterdata, $default_distance) {
		$cDate = date('Y-m-d');
		$uniquerarray = array();
		if(strlen($expfilter) > 0){
			for($ikl = 0;$ikl<=$expfilter;$ikl++) {
				$uniquerarray[] = $ikl;
			}
		}

		if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
        }

      	$this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_allowances.allowances_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

		$this->db->from('job_posting');
		$this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
		$this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
		$this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
		$this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

		if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
		$this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
		$this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
		$this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
		$this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
	        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
	    }

		if($id!=0) {
			$this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
		}
		$this->db->where("job_posting.jobexpire >=", $cDate);
		if(strlen($expfilter) > 0){
			$this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
		}
		
		//$this->db->where("job_posting.recruiter_id",$companyidsarr);

		if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
		$toppicks = $filterdata['toppicks'];
		//$categories1 = explode(",", $toppicks);
		$this->db->where_in('job_toppicks.picks_id', $toppicks);
		}
		if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
		$allowances = $filterdata['allowances'];
		//$categories2 = explode(",", $allowances);
		$this->db->where_in('job_allowances.allowances_id', $allowances);
		} else {
			$this->db->where("job_allowances.allowances_id",5);
		}
		if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
		$medical = $filterdata['medical'];
		//$categories3 = explode(",", $medical);
		$this->db->where_in('job_medical.medical_id', $medical);
		}
		if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
		$workshift = $filterdata['workshift'];
		//$categories4 = explode(",", $workshift);
		$this->db->where_in('job_workshift.workshift_id', $workshift);
		}
		if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
		$leaves = $filterdata['leaves'];
		//$categories5 = explode(",", $leaves);
		$this->db->where_in('job_leaves.leaves_id', $leaves);
		}

		if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
		 	//$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
		} else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
			//$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
		}

		if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
			$this->db->having('salary <=', $filterdata['basicSalary']);
		}

      	$this->db->having("distance<", $default_distance);
      	$this->db->group_by("job_posting.company_id");
		$query = $this->db->get();
		return $query->result_array();
    }

    public function feturedtoppicks_fetch_latlong_bonus_allowances($id, $expfilter, $userLat, $userLong, $offset, $limit, $filterdata, $default_distance) {
		$cDate = date('Y-m-d');
		$uniquerarray = array();
		if(strlen($expfilter) > 0){
			for($ikl = 0;$ikl<=$expfilter;$ikl++) {
				$uniquerarray[] = $ikl;
			}
		}

		if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
        }

      	$this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_allowances.allowances_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

		$this->db->from('job_posting');
		$this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
		$this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
		$this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
		$this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

		if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
		$this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
		$this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
		$this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
		$this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
	        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
	    }

		if($id!=0) {
			$this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
		}
		$this->db->where("job_posting.jobexpire >=", $cDate);
		if(strlen($expfilter) > 0){
			$this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
		}
		
		if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
		$toppicks = $filterdata['toppicks'];
		//$categories1 = explode(",", $toppicks);
		$this->db->where_in('job_toppicks.picks_id', $toppicks);
		}
		if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
		$allowances = $filterdata['allowances'];
		//$categories2 = explode(",", $allowances);
		$this->db->where_in('job_allowances.allowances_id', $allowances);
		} else {
			$this->db->where("job_allowances.allowances_id",5);
		}
		if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
		$medical = $filterdata['medical'];
		//$categories3 = explode(",", $medical);
		$this->db->where_in('job_medical.medical_id', $medical);
		}
		if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
		$workshift = $filterdata['workshift'];
		//$categories4 = explode(",", $workshift);
		$this->db->where_in('job_workshift.workshift_id', $workshift);
		}
		if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
		$leaves = $filterdata['leaves'];
		//$categories5 = explode(",", $leaves);
		$this->db->where_in('job_leaves.leaves_id', $leaves);
		}
      
		if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
		   //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
		} else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
			//$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
		}

		if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
			$this->db->having('salary <=', $filterdata['basicSalary']);
		}

		$this->db->having("distance<", $default_distance);
		$this->db->group_by("exp_with_salary.jobpost_id");
		if(isset($filterdata['sorting']) && $filterdata['sorting'] == 1) {
      		$this->db->order_by("salary","DESC");
      	} else {
      		$this->db->order_by("distance","ASC");
      	}
		$this->db->limit($limit, $offset);
		$query = $this->db->get();
		return $query->result_array();
   	}

   	public function catjob_fetch_leadership($id,$expfilter, $userLat, $userLong, $filterdata, $default_distance) {
		$cDate = date('Y-m-d');
		$uniquerarray = array();
		if(strlen($expfilter) > 0) {
			for($ikl = 0;$ikl<=$expfilter;$ikl++){
				$uniquerarray[] = $ikl;
			}
		}

		if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
        }

      	$this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
		$this->db->from('job_posting');
		$this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
		$this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
		$this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

		if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
		$this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
		$this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
		$this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
		$this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
	        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
	    }

      	$this->db->where("job_posting.jobexpire >=", $cDate);
      	$this->db->where("job_posting.category", 24);
      	if(strlen($expfilter) > 0) {
        	$this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      	}

      	if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
		$toppicks = $filterdata['toppicks'];
		//$categories1 = explode(",", $toppicks);
		$this->db->where_in('job_toppicks.picks_id', $toppicks);
		}
		if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
		$allowances = $filterdata['allowances'];
		//$categories2 = explode(",", $allowances);
		$this->db->where_in('job_allowances.allowances_id', $allowances);
		}
		if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
		$medical = $filterdata['medical'];
		//$categories3 = explode(",", $medical);
		$this->db->where_in('job_medical.medical_id', $medical);
		}
		if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
		$workshift = $filterdata['workshift'];
		//$categories4 = explode(",", $workshift);
		$this->db->where_in('job_workshift.workshift_id', $workshift);
		}
		if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
		$leaves = $filterdata['leaves'];
		//$categories5 = explode(",", $leaves);
		$this->db->where_in('job_leaves.leaves_id', $leaves);
		}

		if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
		   //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
		} else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
			//$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
		}
		
		if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
			$this->db->having('salary <=', $filterdata['basicSalary']);
		}

      $this->db->having("distance<", $default_distance);
		$this->db->group_by("job_posting.company_id");
		$query = $this->db->get();
		return $query->result_array();
    }

    public function catjob_fetch_leadership_all($id,$expfilter, $userLat, $userLong, $offset, $limit, $filterdata, $default_distance) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }

       if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
        }

      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , job_posting.mode, job_posting.modeurl,job_posting.chatbot, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');

        if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
		$this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
		$this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
		$this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
		$this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
	        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
	    }

      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.category", 24);
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
		$toppicks = $filterdata['toppicks'];
		//$categories1 = explode(",", $toppicks);
		$this->db->where_in('job_toppicks.picks_id', $toppicks);
		}
		if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
		$allowances = $filterdata['allowances'];
		//$categories2 = explode(",", $allowances);
		$this->db->where_in('job_allowances.allowances_id', $allowances);
		}
		if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
		$medical = $filterdata['medical'];
		//$categories3 = explode(",", $medical);
		$this->db->where_in('job_medical.medical_id', $medical);
		}
		if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
		$workshift = $filterdata['workshift'];
		//$categories4 = explode(",", $workshift);
		$this->db->where_in('job_workshift.workshift_id', $workshift);
		}
		if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
		$leaves = $filterdata['leaves'];
		//$categories5 = explode(",", $leaves);
		$this->db->where_in('job_leaves.leaves_id', $leaves);
		}

		if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
		   //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
		} else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
			//$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
		}

		if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
			$this->db->having('salary <=', $filterdata['basicSalary']);
		}

        $this->db->having("distance<", $default_distance);
      $this->db->group_by("exp_with_salary.jobpost_id");
      if(isset($filterdata['sorting']) && $filterdata['sorting'] == 1) {
      		$this->db->order_by("salary","DESC");
      	} else {
      		$this->db->order_by("distance","ASC");
      	}
      $this->db->limit($limit, $offset);
      $query = $this->db->get();
      return $query->result_array();
    }

   public function catjob_fetch_information_technology($id,$expfilter, $userLat, $userLong, $filterdata, $default_distance) {
		$cDate = date('Y-m-d');
		$uniquerarray = array();
		if(strlen($expfilter) > 0) {
			for($ikl = 0;$ikl<=$expfilter;$ikl++){
				$uniquerarray[] = $ikl;
			}
		}

		if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
        }

      	$this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
		$this->db->from('job_posting');
		$this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
		$this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
		$this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

		if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
		$this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
		$this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
		$this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
		$this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
	        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
	    }

		$this->db->where("job_posting.jobexpire >=", $cDate);
		$this->db->where("job_posting.category", 22);
		if(strlen($expfilter) > 0) {
			$this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
		}

		if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
		$toppicks = $filterdata['toppicks'];
		//$categories1 = explode(",", $toppicks);
		$this->db->where_in('job_toppicks.picks_id', $toppicks);
		}
		if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
		$allowances = $filterdata['allowances'];
		//$categories2 = explode(",", $allowances);
		$this->db->where_in('job_allowances.allowances_id', $allowances);
		}
		if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
		$medical = $filterdata['medical'];
		//$categories3 = explode(",", $medical);
		$this->db->where_in('job_medical.medical_id', $medical);
		}
		if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
		$workshift = $filterdata['workshift'];
		//$categories4 = explode(",", $workshift);
		$this->db->where_in('job_workshift.workshift_id', $workshift);
		}
		if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
		$leaves = $filterdata['leaves'];
		//$categories5 = explode(",", $leaves);
		$this->db->where_in('job_leaves.leaves_id', $leaves);
		}

		if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
		  //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
		} else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
			//$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
		}
      	
      	if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
			$this->db->having('salary <=', $filterdata['basicSalary']);
		}

      	$this->db->having("distance<", $default_distance);
        $this->db->group_by("job_posting.company_id");
		$query = $this->db->get();
		return $query->result_array();
   }

   public function catjob_fetch_information_technology_all($id,$expfilter, $userLat, $userLong, $offset, $limit, $filterdata, $default_distance) {
      	$cDate = date('Y-m-d');
      	$uniquerarray = array();
      	if(strlen($expfilter) > 0) {
           	for($ikl = 0;$ikl<=$expfilter;$ikl++){
               	$uniquerarray[] = $ikl;
           	}
      	}

      	if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
         }

      	$this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , job_posting.mode, job_posting.modeurl,job_posting.chatbot, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      	$this->db->from('job_posting');
      	$this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      	$this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      	$this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

      	if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
   		$this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
   		}
   		if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
   		$this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
   		}
   		if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
   		$this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
   		}
   		if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
   		$this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
   		}
   		if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
   	        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
   	    }

      	$this->db->where("job_posting.jobexpire >=", $cDate);
      	$this->db->where("job_posting.category", 22);
      	if(strlen($expfilter) > 0) {
        	$this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      	}
      	
      	if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
   		$toppicks = $filterdata['toppicks'];
   		//$categories1 = explode(",", $toppicks);
   		$this->db->where_in('job_toppicks.picks_id', $toppicks);
   		}
   		if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
   		$allowances = $filterdata['allowances'];
   		//$categories2 = explode(",", $allowances);
   		$this->db->where_in('job_allowances.allowances_id', $allowances);
   		}
   		if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
   		$medical = $filterdata['medical'];
   		//$categories3 = explode(",", $medical);
   		$this->db->where_in('job_medical.medical_id', $medical);
   		}
   		if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
   		$workshift = $filterdata['workshift'];
   		//$categories4 = explode(",", $workshift);
   		$this->db->where_in('job_workshift.workshift_id', $workshift);
   		}
   		if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
   		$leaves = $filterdata['leaves'];
   		//$categories5 = explode(",", $leaves);
   		$this->db->where_in('job_leaves.leaves_id', $leaves);
   		}
 
   		if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
   		   //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
   		} else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
   			//$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
   		}

   		if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
   			$this->db->having('salary <=', $filterdata['basicSalary']);
   		}

       	$this->db->having("distance<", $default_distance);
      	$this->db->group_by("exp_with_salary.jobpost_id");
      	if(isset($filterdata['sorting']) && $filterdata['sorting'] == 1) {
      		$this->db->order_by("salary","DESC");
      	} else {
      		$this->db->order_by("distance","ASC");
      	}
      	$this->db->limit($limit, $offset);
      	$query = $this->db->get();
      	return $query->result_array();
   }

   
   public function catjob_fetch_no_experience($id,$expfilter, $userLat, $userLong, $filterdata, $default_distance) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }

      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
        }

         $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
      $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
      $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
      $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
       }

      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.experience", "No Experience");
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
      $toppicks = $filterdata['toppicks'];
      //$categories1 = explode(",", $toppicks);
      $this->db->where_in('job_toppicks.picks_id', $toppicks);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
      $allowances = $filterdata['allowances'];
      //$categories2 = explode(",", $allowances);
      $this->db->where_in('job_allowances.allowances_id', $allowances);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
      $medical = $filterdata['medical'];
      //$categories3 = explode(",", $medical);
      $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
      $workshift = $filterdata['workshift'];
      //$categories4 = explode(",", $workshift);
      $this->db->where_in('job_workshift.workshift_id', $workshift);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
      $leaves = $filterdata['leaves'];
      //$categories5 = explode(",", $leaves);
      $this->db->where_in('job_leaves.leaves_id', $leaves);
      }

      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
        //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }
         
         if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         $this->db->having('salary <=', $filterdata['basicSalary']);
      }

         $this->db->having("distance<", $default_distance);
        $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function catjob_fetch_no_experience_all($id,$expfilter, $userLat, $userLong, $offset, $limit, $filterdata, $default_distance) {
         $cDate = date('Y-m-d');
         $uniquerarray = array();
         if(strlen($expfilter) > 0) {
            for($ikl = 0;$ikl<=$expfilter;$ikl++){
                  $uniquerarray[] = $ikl;
            }
         }

         if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
         }

         $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , job_posting.mode, job_posting.modeurl,job_posting.chatbot, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
         $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
          }

         $this->db->where("job_posting.jobexpire >=", $cDate);
         $this->db->where("job_posting.experience", "No Experience");
         if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
         }
         
         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         //$categories1 = explode(",", $toppicks);
         $this->db->where_in('job_toppicks.picks_id', $toppicks);
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         //$categories2 = explode(",", $allowances);
         $this->db->where_in('job_allowances.allowances_id', $allowances);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         //$categories3 = explode(",", $medical);
         $this->db->where_in('job_medical.medical_id', $medical);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         //$categories4 = explode(",", $workshift);
         $this->db->where_in('job_workshift.workshift_id', $workshift);
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         //$categories5 = explode(",", $leaves);
         $this->db->where_in('job_leaves.leaves_id', $leaves);
         }
 
         if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
            //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
         } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
            //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
         }

         if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
            $this->db->having('salary <=', $filterdata['basicSalary']);
         }

         $this->db->having("distance<", $default_distance);
         $this->db->group_by("exp_with_salary.jobpost_id");
         if(isset($filterdata['sorting']) && $filterdata['sorting'] == 1) {
            $this->db->order_by("salary","DESC");
         } else {
            $this->db->order_by("distance","ASC");
         }
         $this->db->limit($limit, $offset);
         $query = $this->db->get();
         return $query->result_array();
   }

   public function catjob_fetch_nursing($id,$expfilter, $userLat, $userLong, $filterdata, $default_distance) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }

      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
        }

         $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode,job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
      $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
      $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
      $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
       }

      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where_in("job_posting.subcategory", [41,45]);
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
      $toppicks = $filterdata['toppicks'];
      //$categories1 = explode(",", $toppicks);
      $this->db->where_in('job_toppicks.picks_id', $toppicks);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
      $allowances = $filterdata['allowances'];
      //$categories2 = explode(",", $allowances);
      $this->db->where_in('job_allowances.allowances_id', $allowances);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
      $medical = $filterdata['medical'];
      //$categories3 = explode(",", $medical);
      $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
      $workshift = $filterdata['workshift'];
      //$categories4 = explode(",", $workshift);
      $this->db->where_in('job_workshift.workshift_id', $workshift);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
      $leaves = $filterdata['leaves'];
      //$categories5 = explode(",", $leaves);
      $this->db->where_in('job_leaves.leaves_id', $leaves);
      }

      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
        //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }
         
         if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         $this->db->having('salary <=', $filterdata['basicSalary']);
      }

         $this->db->having("distance<", $default_distance);
        $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function catjob_fetch_nursing_all($id,$expfilter, $userLat, $userLong, $offset, $limit, $filterdata, $default_distance) {
         $cDate = date('Y-m-d');
         $uniquerarray = array();
         if(strlen($expfilter) > 0) {
            for($ikl = 0;$ikl<=$expfilter;$ikl++){
                  $uniquerarray[] = $ikl;
            }
         }

         if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
         }

         $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , job_posting.mode, job_posting.modeurl,job_posting.chatbot, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
         $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
          }

         $this->db->where("job_posting.jobexpire >=", $cDate);
         $this->db->where_in("job_posting.subcategory", [41,45]);
         if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
         }
         
         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         //$categories1 = explode(",", $toppicks);
         $this->db->where_in('job_toppicks.picks_id', $toppicks);
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         //$categories2 = explode(",", $allowances);
         $this->db->where_in('job_allowances.allowances_id', $allowances);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         //$categories3 = explode(",", $medical);
         $this->db->where_in('job_medical.medical_id', $medical);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         //$categories4 = explode(",", $workshift);
         $this->db->where_in('job_workshift.workshift_id', $workshift);
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         //$categories5 = explode(",", $leaves);
         $this->db->where_in('job_leaves.leaves_id', $leaves);
         }
 
         if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
            //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
         } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
            //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
         }

         if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
            $this->db->having('salary <=', $filterdata['basicSalary']);
         }

         $this->db->having("distance<", $default_distance);
         $this->db->group_by("exp_with_salary.jobpost_id");
         if(isset($filterdata['sorting']) && $filterdata['sorting'] == 1) {
            $this->db->order_by("salary","DESC");
         } else {
            $this->db->order_by("distance","ASC");
         }
         $this->db->limit($limit, $offset);
         $query = $this->db->get();
         return $query->result_array();
   }



    public function categoryjob_fetch_latlong($id, $expfilter, $typeid, $userLat, $userLong, $filterdata, $default_distance) {

		$cDate = date('Y-m-d');
		$uniquerarray = array();
		for($ikl = 0;$ikl<=$expfilter;$ikl++) {
			$uniquerarray[] = $ikl;
		}

		if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
        }

      	$this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire,job_posting.mode,job_posting.modeurl,job_posting.chatbot, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      	$this->db->from('job_posting');
      	$this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      	$this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      	$this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

      	if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
		$this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
		$this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
		$this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
		$this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
	        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
	    }

		if($id!=0) {
			$this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
		}
		$this->db->where("job_posting.jobexpire >=", $cDate);
		$this->db->where("job_posting.category", $typeid);
		//$this->db->where("job_posting.recruiter_id", $companyidsarr);
		if(!empty($expfilter)) {
			$this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
		}

		if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
		$toppicks = $filterdata['toppicks'];
		//$categories1 = explode(",", $toppicks);
		$this->db->where_in('job_toppicks.picks_id', $toppicks);
		}
		if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
		$allowances = $filterdata['allowances'];
		//$categories2 = explode(",", $allowances);
		$this->db->where_in('job_allowances.allowances_id', $allowances);
		}
		if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
		$medical = $filterdata['medical'];
		//$categories3 = explode(",", $medical);
		$this->db->where_in('job_medical.medical_id', $medical);
		}
		if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
		$workshift = $filterdata['workshift'];
		//$categories4 = explode(",", $workshift);
		$this->db->where_in('job_workshift.workshift_id', $workshift);
		}
		if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
		$leaves = $filterdata['leaves'];
		//$categories5 = explode(",", $leaves);
		$this->db->where_in('job_leaves.leaves_id', $leaves);
		}

		if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
		  //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
		} else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
			//$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
		}

		if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
			$this->db->having('salary <=', $filterdata['basicSalary']);
		}

      	$this->db->having("distance <",$default_distance);
        $this->db->group_by("job_posting.company_id");
      	$query = $this->db->get();
      	return $query->result_array();
   	}

    public function categoryjob_fetch_latlong_all($id, $expfilter, $typeid, $userLat, $userLong, $offset, $limit, $filterdata, $default_distance) {

		$cDate = date('Y-m-d');
		$uniquerarray = array();
		for($ikl = 0;$ikl<=$expfilter;$ikl++) {
			$uniquerarray[] = $ikl;
		}

		if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
        }

      	$this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire,job_posting.mode,job_posting.modeurl,job_posting.chatbot, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      	$this->db->from('job_posting');
      	$this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      	$this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      	$this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

      	if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
		$this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
		$this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
		$this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
		$this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
	        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
	    }

		if($id!=0) {
			$this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
		}
		$this->db->where("job_posting.jobexpire >=", $cDate);
		$this->db->where("job_posting.category", $typeid);
		if(!empty($expfilter)) {
			$this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
		}

		if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
		$toppicks = $filterdata['toppicks'];
		//$categories1 = explode(",", $toppicks);
		$this->db->where_in('job_toppicks.picks_id', $toppicks);
		}
		if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
		$allowances = $filterdata['allowances'];
		//$categories2 = explode(",", $allowances);
		$this->db->where_in('job_allowances.allowances_id', $allowances);
		}
		if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
		$medical = $filterdata['medical'];
		//$categories3 = explode(",", $medical);
		$this->db->where_in('job_medical.medical_id', $medical);
		}
		if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
		$workshift = $filterdata['workshift'];
		//$categories4 = explode(",", $workshift);
		$this->db->where_in('job_workshift.workshift_id', $workshift);
		}
		if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
		$leaves = $filterdata['leaves'];
		//$categories5 = explode(",", $leaves);
		$this->db->where_in('job_leaves.leaves_id', $leaves);
		}
      
		if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
		 //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
		} else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
			//$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
		}

		if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
			$this->db->having('salary <=', $filterdata['basicSalary']);
		}

      	$this->db->having("distance <",$default_distance);
      	$this->db->group_by("exp_with_salary.jobpost_id");
      	if(isset($filterdata['sorting']) && $filterdata['sorting'] == 1) {
      		$this->db->order_by("salary","DESC");
      	} else {
      		$this->db->order_by("distance","ASC");
      	}
      	$this->db->limit($limit, $offset);
      	$query = $this->db->get();
      	return $query->result_array();
   }

   public function hotjob_fetch_latlongbylimit_groupby($id, $expfilter, $userLat, $userLong, $filterdata, $default_distance) {
		$cDate = date('Y-m-d');
		$uniquerarray = array();
		if(strlen($expfilter) > 0) {
			for($ikl = 0;$ikl<=$expfilter;$ikl++){
				$uniquerarray[] = $ikl;
			}
		}
		if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
         $userLat = $filterdata['lat'];
         $userLong = $filterdata['long'];
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.modeurl,job_posting.chatbot, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
   	$this->db->from('job_posting');
   	$this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
   	$this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
   	$this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
   	$this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');

   	if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }

      if($id!=0) {
        	$this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }

		if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
   		$toppicks = $filterdata['toppicks'];
   		$this->db->where_in('job_toppicks.picks_id', $toppicks);
		}
		if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
   		$allowances = $filterdata['allowances'];
   		$this->db->where_in('job_allowances.allowances_id', $allowances);
		}
		if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
   		$medical = $filterdata['medical'];
   		$this->db->where_in('job_medical.medical_id', $medical);
		}
		if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
   		$workshift = $filterdata['workshift'];
   		$this->db->where_in('job_workshift.workshift_id', $workshift);
		}
		if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
   		$leaves = $filterdata['leaves'];
   		$this->db->where_in('job_leaves.leaves_id', $leaves);
		}
		if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
		   //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
		} else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
			//$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
		}
		if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
			$this->db->having('salary <=', $filterdata['basicSalary']);
		}
   	$this->db->having("distance<", $default_distance);
   	$this->db->group_by("job_posting.company_id");

      if(isset($filterdata['sorting']) && $filterdata['sorting'] == 1) {
         $this->db->order_by("salary","DESC");
      } else {
         $this->db->order_by("distance","ASC");
      }
   	$query = $this->db->get();
   	return $query->result_array();
   }

   public function boostjob_fetch_latlong($id,$expfilter, $userLat, $userLong, $filterdata, $default_distance) {
   	$cDate = date('Y-m-d');
   	$uniquerarray = array();
   	if(strlen($expfilter) > 0) { 
     	   for($ikl = 0;$ikl<=$expfilter;$ikl++) {
         	$uniquerarray[] = $ikl;
      	}
   	}
   	if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
         $userLat = $filterdata['lat'];
         $userLong = $filterdata['long'];
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status,job_posting.mode, job_posting.modeurl,job_posting.chatbot,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
   	$this->db->from('job_posting');
   	$this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
   	$this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
   	$this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
		
      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }

		if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
		   $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
		   $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
		   $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
	      $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
	   }

      if($id!=0) {
        	$this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.boost_status !=", 0);
		if(strlen($expfilter) > 0) { 
			$this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
		}

		if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
		   $toppicks = $filterdata['toppicks'];
		   $this->db->where_in('job_toppicks.picks_id', $toppicks);
		}
		if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
		   $allowances = $filterdata['allowances'];
		   $this->db->where_in('job_allowances.allowances_id', $allowances);
		}
		if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
		   $medical = $filterdata['medical'];
		   $this->db->where_in('job_medical.medical_id', $medical);
		}
		if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
		   $workshift = $filterdata['workshift'];
		   $this->db->where_in('job_workshift.workshift_id', $workshift);
		}
		if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
		   $leaves = $filterdata['leaves'];
		   $this->db->where_in('job_leaves.leaves_id', $leaves);
		}
      
		if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
		   //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
		} else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
			//$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
		}

		if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
			$this->db->having('salary <=', $filterdata['basicSalary']);
		}

		$this->db->having("distance<", $default_distance);
   	$this->db->group_by("exp_with_salary.jobpost_id");
   	$query = $this->db->get();
   	return $query->result_array();
    }

    public function hotjob_fetch_latlong($id,$expfilter, $userLat, $userLong, $offset, $limit, $filterdata, $default_distance) {
      	$cDate = date('Y-m-d');
      	$uniquerarray = array();
      	if(strlen($expfilter) > 0) { 
         	for($ikl = 0;$ikl<=$expfilter;$ikl++){
            	$uniquerarray[] = $ikl;
         	}
      	}

      	if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
        }

      	$this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status,job_posting.mode, job_posting.modeurl,job_posting.chatbot,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      	$this->db->from('job_posting');
      	$this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      	$this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      	$this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      	$this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      	
		if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
		$this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
		$this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
		$this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
		}
		if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
	        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
	    }

      	if($id!=0) {
        	$this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      	}
    	$this->db->where("job_posting.jobexpire >=", $cDate);
      	$this->db->where("job_posting.boost_status", 0);
      	if(strlen($expfilter) > 0) { 
        	$this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      	}

      	if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
		$toppicks = $filterdata['toppicks'];
		//$categories1 = explode(",", $toppicks);
		$this->db->where_in('job_toppicks.picks_id', $toppicks);
		}
		if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
		$allowances = $filterdata['allowances'];
		//$categories2 = explode(",", $allowances);
		$this->db->where_in('job_allowances.allowances_id', $allowances);
		}
		if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
		$medical = $filterdata['medical'];
		//$categories3 = explode(",", $medical);
		$this->db->where_in('job_medical.medical_id', $medical);
		}
		if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
		$workshift = $filterdata['workshift'];
		//$categories4 = explode(",", $workshift);
		$this->db->where_in('job_workshift.workshift_id', $workshift);
		}
		if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
		$leaves = $filterdata['leaves'];
		//$categories5 = explode(",", $leaves);
		$this->db->where_in('job_leaves.leaves_id', $leaves);
		}

		if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
		   //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
		} else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
			//$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
		}

		if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
			$this->db->having('salary <=', $filterdata['basicSalary']);
		}
   	$this->db->group_by("exp_with_salary.jobpost_id");
   	
      $this->db->having("distance<", $default_distance);

   	if(isset($filterdata['sorting']) && $filterdata['sorting'] == 1) {
   		$this->db->order_by("salary","DESC");
   	} else {
   		$this->db->order_by("distance","ASC");
   	}

   	$this->db->limit($limit, $offset);
   	$query = $this->db->get();
   	return $query->result_array();
    }

    // City view all list

   public function cityjob_fetch_groupby($id,$expfilter, $userLat, $userLong, $userCity, $filterdata, $default_distance) {
      $cDate = date('Y-m-d');

      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , job_posting.mode, job_posting.modeurl,job_posting.chatbot, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }
      
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      
      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         //$categories1 = explode(",", $toppicks);
         $this->db->where_in('job_toppicks.picks_id', $toppicks);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         //$categories2 = explode(",", $allowances);
         $this->db->where_in('job_allowances.allowances_id', $allowances);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         //$categories3 = explode(",", $medical);
         $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         //$categories4 = explode(",", $workshift);
         $this->db->where_in('job_workshift.workshift_id', $workshift);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         //$categories5 = explode(",", $leaves);
         $this->db->where_in('job_leaves.leaves_id', $leaves);
      }

      if(isset($userCity)) {
         $this->db->like("recruiter_details.address", $userCity, 'both');
      } else {
         if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
            //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
         } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
            //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
         }
      }
      
      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
			$this->db->having('salary <=', $filterdata['basicSalary']);
	  }

      $this->db->having("distance<", $default_distance);
      $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      return $query->result_array();
   }

    public function cityjob_fetch($id,$expfilter, $userLat, $userLong, $userCity, $offset, $limit, $filterdata, $default_distance) {
      $cDate = date('Y-m-d');

      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , job_posting.mode, job_posting.modeurl,job_posting.chatbot, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }
      
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      
      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         //$categories1 = explode(",", $toppicks);
         $this->db->where_in('job_toppicks.picks_id', $toppicks);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         //$categories2 = explode(",", $allowances);
         $this->db->where_in('job_allowances.allowances_id', $allowances);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         //$categories3 = explode(",", $medical);
         $this->db->where_in('job_medical.medical_id', $medical);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         //$categories4 = explode(",", $workshift);
         $this->db->where_in('job_workshift.workshift_id', $workshift);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         //$categories5 = explode(",", $leaves);
         $this->db->where_in('job_leaves.leaves_id', $leaves);
      }

      if(isset($userCity)) {
         $this->db->like("recruiter_details.address", $userCity, 'both');
      } else {
         if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
            //$this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
         } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
            //$this->db->like("recruiter_details.address", $filterdata['location'], 'both');
         }
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
			$this->db->having('salary <=', $filterdata['basicSalary']);
	  }

      $this->db->having("distance<", $default_distance);
      $this->db->group_by("exp_with_salary.jobpost_id");
      if(isset($filterdata['sorting']) && $filterdata['sorting'] == 1) {
      		$this->db->order_by("salary","DESC");
      	} else {
      		$this->db->order_by("distance","ASC");
      	}
      $this->db->limit($limit, $offset);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function appreviews_lists_limit() {
      $this->db->select("*");
      $this->db->order_by("id","DESC");
      $this->db->limit(10);
      $this->db->from('app_reviews');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function appreviews_lists() {
      $this->db->select("*");
      $this->db->order_by("id","DESC");
      $this->db->from('app_reviews');
      $query = $this->db->get();
      return $query->result_array();
   }


}
?>