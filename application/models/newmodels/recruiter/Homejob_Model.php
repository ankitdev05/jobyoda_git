<?php
ob_start();
class Homejob_Model extends CI_Model {

   function __construct() { 
    	parent::__construct(); 
   }

   public function job_fetch_homeforappnearby_groupby($id, $userLat, $userLong, $expfilter, $filterdata) {
		$userLat = (double)$userLat;
		$userLong = (double)$userLong;
		$cDate = date('Y-m-d');
		$uniquearraybyother = array();
		if(strlen($expfilter) > 0) {
   		for($ityu = $expfilter; $ityu >-1; $ityu--) {
   		    $uniquearraybyother[] = $ityu;
   		}
		}
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }

		$this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.jobPitch,job_posting.mode, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, recruiter_details.latitude,recruiter_details.longitude,  (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

		$this->db->from('job_posting');
		$this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
		$this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
		$this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');

      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }

      $this->db->where_in("job_posting.id", $filterdata['jid_array']);

      $this->db->where("job_posting.jobexpire >=", $cDate);
		//$this->db->where("job_posting.recruiter_id", $companyidsarr);
		if($id != 0) {
		    $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
		}
		if(strlen($expfilter) > 0) {
		    $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
		}
      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $categories1 = explode(",", $toppicks);
         $this->db->where_in('job_toppicks.picks_id', $categories1);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $categories2 = explode(",", $allowances);
         $this->db->or_where_in('job_allowances.allowances_id', $categories2);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $categories3 = explode(",", $medical);
         $this->db->or_where_in('job_medical.medical_id', $categories3);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $categories4 = explode(",", $workshift);
         $this->db->or_where_in('job_workshift.workshift_id', $categories4);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         $categories5 = explode(",", $leaves);
         $this->db->or_where_in('job_leaves.leaves_id', $categories5);
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->where('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->where('salary <=', 10000);
            $this->db->where('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->where('salary <=', 20000);
            $this->db->where('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->where('salary <=', 30000);
            $this->db->where('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->where('salary <=', 40000);
            $this->db->where('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->where('salary <=', 50000);
            $this->db->where('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->where('salary <=', 70000);
            $this->db->where('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->where('salary <=', 80000);
            $this->db->where('salary >=', 70000);
         }
      }

      if(isset($filterdata['language']) && !empty($filterdata['language'])) {
         $this->db->where('job_posting.language', $filterdata['language']);
      }
      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         $this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         $this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

		$this->db->having("distance <","7000");
      if(isset($filterdata['sorting']) && !empty($filterdata['sorting'])) {
         if($filterdata['sorting'] == 1) {
            $this->db->order_by('salary','desc');
         } else {
            $this->db->order_by('distance','asc');   
         }
      }
      $this->db->group_by("job_posting.company_id");
		$query = $this->db->get();
		return $query->result_array();
   }


   public function hotjob_fetch_latlongbylimit_groupby($id,$expfilter, $userLat, $userLong, $filterdata) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }

      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');

      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }

      if(isset($filterdata['jid_array']) && !empty($filterdata['jid_array'])) {
         $this->db->where_in("job_posting.id", explode(',',$filterdata['jid_array']));
      }
      
      $this->db->where("job_posting.jobexpire >=", $cDate);
      //$this->db->where("job_posting.recruiter_id", $companyidsarr);
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $categories1 = explode(",", $toppicks);
         $this->db->where_in('job_toppicks.picks_id', $categories1);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $categories2 = explode(",", $allowances);
         $this->db->or_where_in('job_allowances.allowances_id', $categories2);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $categories3 = explode(",", $medical);
         $this->db->or_where_in('job_medical.medical_id', $categories3);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $categories4 = explode(",", $workshift);
         $this->db->or_where_in('job_workshift.workshift_id', $categories4);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         $categories5 = explode(",", $leaves);
         $this->db->or_where_in('job_leaves.leaves_id', $categories5);
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->where('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->where('salary <=', 10000);
            $this->db->where('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->where('salary <=', 20000);
            $this->db->where('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->where('salary <=', 30000);
            $this->db->where('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->where('salary <=', 40000);
            $this->db->where('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->where('salary <=', 50000);
            $this->db->where('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->where('salary <=', 70000);
            $this->db->where('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->where('salary <=', 80000);
            $this->db->where('salary >=', 70000);
         }
      }

      if(isset($filterdata['language']) && !empty($filterdata['language'])) {
         $this->db->where('job_posting.language', $filterdata['language']);
      }
      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         $this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         $this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

      $this->db->having("distance<", '7000');
      $this->db->order_by('distance','asc');
      $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function catjob_fetch_information_technology($id,$expfilter, $userLat, $userLong, $filterdata) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      
      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }

      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.category", 22);
      $this->db->where("job_posting.recruiter_id", $companyidsarr);
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $categories1 = explode(",", $toppicks);
         $this->db->where_in('job_toppicks.picks_id', $categories1);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $categories2 = explode(",", $allowances);
         $this->db->or_where_in('job_allowances.allowances_id', $categories2);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $categories3 = explode(",", $medical);
         $this->db->or_where_in('job_medical.medical_id', $categories3);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $categories4 = explode(",", $workshift);
         $this->db->or_where_in('job_workshift.workshift_id', $categories4);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         $categories5 = explode(",", $leaves);
         $this->db->or_where_in('job_leaves.leaves_id', $categories5);
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->where('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->where('salary <=', 10000);
            $this->db->where('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->where('salary <=', 20000);
            $this->db->where('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->where('salary <=', 30000);
            $this->db->where('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->where('salary <=', 40000);
            $this->db->where('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->where('salary <=', 50000);
            $this->db->where('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->where('salary <=', 70000);
            $this->db->where('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->where('salary <=', 80000);
            $this->db->where('salary >=', 70000);
         }
      }

      if(isset($filterdata['language']) && !empty($filterdata['language'])) {
         $this->db->where('job_posting.language', $filterdata['language']);
      }
      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         $this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         $this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

      $this->db->having("distance<", '7000');
      if(isset($filterdata['sorting']) && !empty($filterdata['sorting'])) {
         if($filterdata['sorting'] == 1) {
            $this->db->order_by('salary','desc');
         } else {
            $this->db->order_by('distance','asc');   
         }
      }
      $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function catjob_fetch_leadership($id,$expfilter, $userLat, $userLong, $filterdata) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');

      if(isset($filterdata['allowancestoppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.category", 24);
      //$this->db->where("job_posting.recruiter_id", $companyidsarr);
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $categories1 = explode(",", $toppicks);
         $this->db->where_in('job_toppicks.picks_id', $categories1);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $categories2 = explode(",", $allowances);
         $this->db->or_where_in('job_allowances.allowances_id', $categories2);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $categories3 = explode(",", $medical);
         $this->db->or_where_in('job_medical.medical_id', $categories3);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $categories4 = explode(",", $workshift);
         $this->db->or_where_in('job_workshift.workshift_id', $categories4);
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->where('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->where('salary <=', 10000);
            $this->db->where('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->where('salary <=', 20000);
            $this->db->where('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->where('salary <=', 30000);
            $this->db->where('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->where('salary <=', 40000);
            $this->db->where('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->where('salary <=', 50000);
            $this->db->where('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->where('salary <=', 70000);
            $this->db->where('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->where('salary <=', 80000);
            $this->db->where('salary >=', 70000);
         }
      }

      if(isset($filterdata['language']) && !empty($filterdata['language'])) {
         $this->db->where('job_posting.language', $filterdata['language']);
      }
      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         $this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         $this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

      $this->db->having("distance<", '7000');
      if(isset($filterdata['sorting']) && !empty($filterdata['sorting'])) {
         if($filterdata['sorting'] == 1) {
            $this->db->order_by('salary','desc');
         } else {
            $this->db->order_by('distance','asc');   
         }
      }
      $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      return $query->result_array();
    }

   public function feturedtoppicks_fetch_latlong_bonus_groupby($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",1);
      $this->db->having("distance<", '7000');
      $this->db->order_by('distance','ASC');
      $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function feturedtoppicks_fetch_latlong_bonus($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",1);
      $this->db->having("distance<", '6000');
      $this->db->order_by('distance','ASC');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_toppicks.jobpost_id");
      //$this->db->limit(20);
      $query = $this->db->get();
      return $query->result_array();
   	}

   	public function feturedtoppicks_fetch_latlong_freefood_groupby($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",2);
      //$this->db->where("job_posting.recruiter_id",$companyidsarr);
      $this->db->having("distance<", '6000');
      $this->db->order_by('distance','ASC');
      //$this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_posting.company_id");
      //$this->db->limit(1);
      $query = $this->db->get();
      return $query->result_array();
   	}

   	public function feturedtoppicks_fetch_latlong_bonus_freefood($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",2);
      $this->db->having("distance<", '6000');
      $this->db->order_by('distance','ASC');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_toppicks.jobpost_id");
      //$this->db->limit(20);
      $query = $this->db->get();
      return $query->result_array();
   	}

   public function feturedtoppicks_fetch_latlong_dayihmo_groupby($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",3);
      //$this->db->where("job_posting.recruiter_id",$companyidsarr);
      $this->db->having("distance<", '6000');
      $this->db->order_by('distance','ASC');
      //$this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_posting.company_id");
      //$this->db->limit(1);
      $query = $this->db->get();
      return $query->result_array();
   	}

   public function feturedtoppicks_fetch_latlong_bonus_dayihmo($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",3);
      $this->db->having("distance<", '6000');
      $this->db->order_by('distance','ASC');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_toppicks.jobpost_id");
      //$this->db->limit(20);
      $query = $this->db->get();
      return $query->result_array();
   	}

   public function feturedtoppicks_fetch_latlong_shift_groupby($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",5);
      $this->db->having("distance<", '6000');
      $this->db->order_by('distance','ASC');
      $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      return $query->result_array();
   	}

   public function feturedtoppicks_fetch_latlong_bonus_shift($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",5);
      $this->db->having("distance<", '6000');
      $this->db->order_by('distance','ASC');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_toppicks.jobpost_id");
      //$this->db->limit(20);
      $query = $this->db->get();
      return $query->result_array();
   	}

   public function feturedtoppicks_fetch_latlong_monthpay_groupby($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",6);
      //$this->db->where("job_posting.recruiter_id",$companyidsarr);
      $this->db->having("distance<", '6000');
      $this->db->order_by('distance','ASC');
      //$this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_posting.company_id");
      //$this->db->limit(1);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function feturedtoppicks_fetch_latlong_bonus_monthpay($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",6);
      $this->db->having("distance<", '7000');
      $this->db->order_by('distance','ASC');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_toppicks.jobpost_id");
      $this->db->limit(50);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function feturedtoppicks_fetch_latlong_workfromhome_groupby($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",7);
      $this->db->having("distance<", '7000');
      $this->db->order_by('distance','ASC');
      $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function feturedtoppicks_fetch_latlong_bonus_workfromhome($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",7);
      $this->db->having("distance<", '7000');
      $this->db->order_by('distance','ASC');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_toppicks.jobpost_id");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function feturedtoppicks_fetch_latlong_allowances_groupby($id, $expfilter, $userLat, $userLong, $filterdata) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_allowances.allowances_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }

      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $categories1 = explode(",", $toppicks);
         $this->db->where_in('job_toppicks.picks_id', $categories1);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $categories3 = explode(",", $medical);
         $this->db->or_where_in('job_medical.medical_id', $categories3);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $categories4 = explode(",", $workshift);
         $this->db->or_where_in('job_workshift.workshift_id', $categories4);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         $categories5 = explode(",", $leaves);
         $this->db->or_where_in('job_leaves.leaves_id', $categories5);
      }

      $this->db->where("job_allowances.allowances_id",7);

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         if($filterdata['basicSalary'] == 5) {
            $this->db->where('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->where('salary <=', 10000);
            $this->db->where('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->where('salary <=', 20000);
            $this->db->where('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->where('salary <=', 30000);
            $this->db->where('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->where('salary <=', 40000);
            $this->db->where('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->where('salary <=', 50000);
            $this->db->where('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->where('salary <=', 70000);
            $this->db->where('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->where('salary <=', 80000);
            $this->db->where('salary >=', 70000);
         }
      }

      if(isset($filterdata['language']) && !empty($filterdata['language'])) {
         $this->db->where('job_posting.language', $filterdata['language']);
      }
      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         $this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         $this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

      $this->db->having("distance<", '6000');
      if(isset($filterdata['sorting']) && !empty($filterdata['sorting'])) {
         if($filterdata['sorting'] == 1) {
            $this->db->order_by('salary','desc');
         } else {
            $this->db->order_by('distance','asc');   
         }
      }
      $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      return $query->result_array();
   }

    public function feturedtoppicks_fetch_latlong_allowances_apps($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_allowances.allowances_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_allowances.allowances_id",7);
      $this->db->having("distance<", '6000');
      $this->db->order_by('distance','asc'); 
      $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function feturedtoppicks_fetch_latlong_bonus_allowances($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_allowances.allowances_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_allowances.allowances_id",7);
      $this->db->having("distance<", '6000');
      $this->db->order_by('distance','ASC');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_allowances.jobpost_id");
      //$this->db->limit(20);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function instantscreening_fetch_groupby($id, $expfilter, $userLat, $userLong, $filterdata) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }

      $this->db->where("job_posting.jobexpire >=", $cDate);
      if($id != 0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }$this->db->where("job_posting.mode", 'Instant screening');
      
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $categories1 = explode(",", $toppicks);
         $this->db->where_in('job_toppicks.picks_id', $categories1);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $categories2 = explode(",", $allowances);
         $this->db->or_where_in('job_allowances.allowances_id', $categories2);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $categories3 = explode(",", $medical);
         $this->db->or_where_in('job_medical.medical_id', $categories3);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $categories4 = explode(",", $workshift);
         $this->db->or_where_in('job_workshift.workshift_id', $categories4);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         $categories5 = explode(",", $leaves);
         $this->db->or_where_in('job_leaves.leaves_id', $categories5);
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->where('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->where('salary <=', 10000);
            $this->db->where('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->where('salary <=', 20000);
            $this->db->where('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->where('salary <=', 30000);
            $this->db->where('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->where('salary <=', 40000);
            $this->db->where('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->where('salary <=', 50000);
            $this->db->where('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->where('salary <=', 70000);
            $this->db->where('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->where('salary <=', 80000);
            $this->db->where('salary >=', 70000);
         }
      }

      if(isset($filterdata['language']) && !empty($filterdata['language'])) {
         $this->db->where('job_posting.language', $filterdata['language']);
      }
      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         $this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         $this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

      $this->db->where("job_posting.recruiter_id", $companyidsarr);
      $this->db->having("distance<", '7000');
      if(isset($filterdata['sorting']) && !empty($filterdata['sorting'])) {
         if($filterdata['sorting'] == 1) {
            $this->db->order_by('salary','desc');
         } else {
            $this->db->order_by('distance','asc');   
         }
      }
      $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function feturedtoppicks_fetch_all_groupby($id, $expfilter, $userLat, $userLong, $getParamType, $filterdata) {

      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }
      if(isset($filterdata['lat']) && !empty($filterdata['lat'])) {
            $userLat = $filterdata['lat'];
            $userLong = $filterdata['long'];
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');

      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }

      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",$getParamType);
      //$this->db->where("job_posting.recruiter_id",$companyidsarr);

      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $categories2 = explode(",", $allowances);
         $this->db->or_where_in('job_allowances.allowances_id', $categories2);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $categories3 = explode(",", $medical);
         $this->db->or_where_in('job_medical.medical_id', $categories3);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $categories4 = explode(",", $workshift);
         $this->db->or_where_in('job_workshift.workshift_id', $categories4);
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leaves = $filterdata['leaves'];
         $categories5 = explode(",", $leaves);
         $this->db->or_where_in('job_leaves.leaves_id', $categories5);
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         
         if($filterdata['basicSalary'] == 5) {
            $this->db->where('salary <=', 5000);

         } else if($filterdata['basicSalary'] == 10) {
            $this->db->where('salary <=', 10000);
            $this->db->where('salary >=', 5000);

         } else if($filterdata['basicSalary'] == 20) {
            $this->db->where('salary <=', 20000);
            $this->db->where('salary >=', 10000);

         } else if($filterdata['basicSalary'] == 30) {
            $this->db->where('salary <=', 30000);
            $this->db->where('salary >=', 20000);
         
         } else if($filterdata['basicSalary'] == 40) {
            $this->db->where('salary <=', 40000);
            $this->db->where('salary >=', 30000);
         
         } else if($filterdata['basicSalary'] == 50) {
            $this->db->where('salary <=', 50000);
            $this->db->where('salary >=', 40000);
         
         } else if($filterdata['basicSalary'] == 70) {
            $this->db->where('salary <=', 70000);
            $this->db->where('salary >=', 50000);
         
         } else if($filterdata['basicSalary'] == 80) {
            $this->db->where('salary <=', 80000);
            $this->db->where('salary >=', 70000);
         }
      }

      if(isset($filterdata['language']) && !empty($filterdata['language'])) {
         $this->db->where('job_posting.language', $filterdata['language']);
      }
      if(isset($filterdata['getcity']) && !empty($filterdata['getcity'])) {
         $this->db->like("recruiter_details.address", $filterdata['getcity'], 'both');
      } else if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         $this->db->like("recruiter_details.address", $filterdata['location'], 'both');
      }

      $this->db->having("distance<", '7000');
      if(isset($filterdata['sorting']) && !empty($filterdata['sorting'])) {
         if($filterdata['sorting'] == 1) {
            $this->db->order_by('salary','desc');
         } else {
            $this->db->order_by('distance','asc');   
         }
      }
      $this->db->group_by("job_posting.company_id");
      $query = $this->db->get();
      return $query->result_array();
   }



   public function job_sorting_by_value($id, $sortvalue, $jobids, $userLat, $userLong) {

      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');

      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where_in("job_posting.id", explode(',',$jobids));
      $this->db->having("distance<", '5000');

      if($sortvalue == 1) {
         $this->db->order_by('salary', "DESC");
      } else {
         $this->db->order_by('distance','ASC');
      }

      $this->db->group_by("exp_with_salary.jobpost_id");
      //$this->db->limit(10);
      $query = $this->db->get();
      return $query->result_array();
   }
}