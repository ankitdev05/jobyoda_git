<?php
ob_start();

class Subscription_Model extends CI_Model {

   function __construct() { 
      parent::__construct(); 
   }

   public function list() {
      $this->db->where('status', 1);
      $query = $this->db->get('subscription_plan');
      return $query->result_array();
   }

   public function subscribe_insert($data) {

      if($this->db->insert("subscription_plan", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }   

   public function view($id) {
      $this->db->where('id', $id);
      $query = $this->db->get('subscription_plan');
      return $query->result_array();
   }

   public function subscribe_update($data, $id) {
      $this->db->where('id', $id);
      $query = $this->db->update('subscription_plan', $data);
      return true;
   }

   public function delete_plan($id) {
      $this->db->where('id', $id);
      $this->db->delete('subscription_plan');
      return true;
   }

   public function view_existing($id) {
      $this->db->where('recruiter_id', $id);
      $this->db->where('status', 1);
      $this->db->order_by('id', 'DESC');
      $this->db->limit(1);
      $query = $this->db->get('recruiter_subscribed');
      return $query->result_array();
   }

   public function view_existing_all($id) {
      $this->db->where('recruiter_id', $id);
      //$this->db->where('status', 2);
      $this->db->order_by('id', 'DESC');
      $query = $this->db->get('recruiter_subscribed');
      return $query->result_array();
   }

   public function view_existing_by_recruiter_id($id) {
      $this->db->where('recruiter_id', $id);
      $this->db->order_by('id', 'DESC');
      $query = $this->db->get('recruiter_subscribed');
      return $query->result_array();
   }

   public function view_existing_by_id($id) {
      $this->db->where('id', $id);
      $query = $this->db->get('recruiter_subscribed');
      return $query->result_array();
   }

   public function recruiter_subscribe_insert($data) {

      if($this->db->insert("recruiter_subscribed", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function recruiter_subscribe_update($data, $id) {
      $this->db->where('recruiter_id', $id);
      $this->db->where('status', 1);
      $query = $this->db->update('recruiter_subscribed', $data);
      return true;
   }

   public function view_existing_updation($data, $id) {
      $this->db->where('id', $id);
      $query = $this->db->update('recruiter_subscribed', $data);
      return true;
   }


   public function subscribe_request_save($data, $id) {

      $record = ["recruite_id"=>$id, "name"=>$data['name'], "email"=>$data['email'], "phone"=>$data['phone'], "plan"=>$data['plan'], "subject"=>$data['subject'], "description"=>$data['description']];

      if($this->db->insert("subscription_request_email", $record)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }   

   public function get_subscribe_request() {
      $this->db->order_by('id','DESC');
      $query = $this->db->get('subscription_request_email');
      return $query->result_array();
   }

   public function get_subscribe_request_unseen() {
      $this->db->order_by('id','DESC');
      $this->db->where('status', 1);
      $query = $this->db->get('subscription_request_email');
      return $query->result_array();
   }

   public function subscribe_request_update() {
      $query = $this->db->update('subscription_request_email', ["status"=>2]);
      return true;
   }

}
?>