<?php
ob_start();
class Newpoints_Model extends CI_Model {

   function __construct() { 
      parent::__construct(); 
   }

   public function companysite_lists() {
      $this->db->select("recruiter_details.companyPic, recruiter.email, recruiter_details.companyDesc, recruiter_details.address, recruiter_details.phone, recruiter.cname, recruiter.fname, recruiter.lname, recruiter.id");
      $this->db->from("recruiter");
      $this->db->join("recruiter_details","recruiter_details.recruiter_id = recruiter.id");
      $this->db->where('recruiter.parent_id !=', 0);
      $this->db->where('recruiter.label', 2);
      $this->db->order_by('recruiter.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function companysite_jobcount($id) {
      $currentDate = date("y-m-d");
      $this->db->select('COUNT(job_posting.id) as jobCount)')
               ->where("recruiter_id", $id)
               ->where("jobexpire >", $currentDate)
               ->where("job_posting.status", 1);
      $query = $this->db->get("job_posting");
      return $query->result_array();
   }

}
?>