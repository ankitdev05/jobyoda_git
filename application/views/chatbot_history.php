

<style>
   .innerbglay.noshad{
   padding: 25px;
   background: #fff;
   }
   .firstLayer{
   box-shadow: 0 2px 4px 0 rgb(0 0 0 / 16%);
   padding: 15px 20px;
   border-radius: 6px;
   display: flex;
   justify-content: space-between;
   align-items: center; 
   margin-top: 20px;
   }
   .firstLayer:hover{
   text-decoration: none;
   }
   .firstLayer:last-child{
   margin-bottom: 20px;
   }
   .managerpart.managerpart2{
   margin-top: 137px;
   min-height: 300px;
   }
   .innerbglay.noshad{
   margin-top: 15px;
   padding: 0;
   } 
   .firstLeft{
   display: flex;
   align-items: center;
   }
   .leftContent h2{
   font-family: "Quicksand", sans-serif;
   font-weight: 600;
   color: #000;
   font-size: 18px;
   }
   .leftContent p{
   font-family: "Quicksand", sans-serif;
   font-weight: 500;
   color: rgb(0 0 0 / 50%);
   font-size: 14px;
   }
   .firstRight p{
   font-family: "Quicksand", sans-serif;
   font-weight: 500;
   color: rgb(0 0 0 / 50%);
   font-size: 14px;  
   }
   .leftImg {
   width: 100px;
       margin-right: 15px;
   }
   .leftImg img{
   width: 100%;
   object-fit: cover;
   }
</style>
<?php include_once('header.php'); ?>
<div class="managerpart managerpart2">
   <div class="container">
      <div class="innerbglay noshad">
      <?php
        if(count($quest) > 0) {

          foreach($quest as $ques) {
      ?>
         <a href="<?php echo base_url();?>chatbot_view/<?php echo base64_encode($ques['job_id']);?>" class="firstLayer">
            <div class="firstLeft">
               <div class="leftImg">
                  <img src="<?php echo $ques['image'];?>" class="img-fluid" alt="img">
               </div>
               <div class="leftContent">
                  <h2> <?php echo $ques['jobtitle'];?> </h2>
                  <p><?php echo $ques['cname'];?></p>
               </div>
            </div>
            <div class="firstRight">
               <p>Applied on <?php echo $ques['applied_at'];?> </p>
            </div>
         </a>
      <?php
        }
      } else {
      ?>

            <img class="bx_img" src="<?php echo base_url();?>webfiles/img/emptybx.png">
            <p class="posttypes text-center">No Data Found in Chat</p>

      <?php
      }
      ?>
      </div>
   </div>
</div>
<?php include_once('footer.php'); ?>
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Change Password</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <span id="errorfield"></span>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                  <div class="forminputspswd">
                     <input type="password" class="form-control" placeholder="Enter New Password" name="newpass" id="newpass">
                     <img src="<?php echo base_url().'webfiles/';?>img/keypass.png">
                  </div>
                  <div class="forminputspswd">
                     <input type="password" class="form-control" placeholder="Confirm New Password" name="confpass" id="confpass">
                     <img src="<?php echo base_url().'webfiles/';?>img/keypass.png">
                  </div>
                  <button type="button" id="changepassbtn" class="srchbtns">Change</button>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>recruiterfiles/css/jquery.timepicker.min.css" />
<script type="text/javascript" src="<?php echo base_url();?>recruiterfiles/js/jquery.timepicker.min.js"></script>
</body>
</html>

