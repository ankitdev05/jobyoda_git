<?php
    include_once('header2.php');
// if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {

//     } else {
//         $link = "https";
//         $link .= "://";
//         $link .= $_SERVER['HTTP_HOST'];
//         $link .= $_SERVER['REQUEST_URI'];
//         redirect($link);
//     }
    $userSess = $this->session->userdata('usersess'); 
    if ($this->session->userdata('userfsess')) {
        $userfsess = $this->session->userdata('userfsess');
        $type      = $userfsess['type'];
    }
?>
<style type="text/css">
    .topemployerpage figcaption p{text-align: left;}
</style>

    <section>
        <div class="BannerArea" style="background-image: url('https://jobyoda.com/webfiles/img/newmap.jpg');">
            <h1>Top Employer  </h1>
            <h2>Get hired in top BPO Companies through JobYoDA</h2>
        </div>
    </section>

 
 
    <section>
        <div class="CompanyArea">
            <div class="container"> 
                <div class="row">
                <?php
                    if($ourPartners) {
                        $x=0;
                        foreach($ourPartners as $topPartner) {
                ?>
                            <div class="col-sm-6 col-md-3 blogBox moreBox" <?php if($x >=8 ){ echo "style='display:none;'"; } ?>>
                                <div class="CompanyBox topemployerpage"> 
                                    <figure>
                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($topPartner['id']);}else{ echo base_url();?>site_details/<?php echo base64_encode($topPartner['id']); }?>">
                                            <img src="<?php echo $topPartner['image']; ?>">
                                        </a>
                                    </figure>
                                    <figcaption>
                                        <h3>
                                            <a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($topPartner['id']);}else{ echo base_url();?>site_details/<?php echo base64_encode($topPartner['id']); }?>">
                                                <?php if(strlen($topPartner['cname']) > 60) { echo substr($topPartner['cname'], 0,60).'...'; } else {echo $topPartner['cname']; } ?>
                                            </a>
                                        </h3>
                                        <p>
                                            <i class="fa fa-building"></i> 
                                            Headquaters : <span><?php echo $topPartner['headquater']; ?></span>
                                        </p>
                                        <p>
                                            <i class="fa fa-search"></i> 
                                            Founded : <span><?php echo $topPartner['founded']; ?></span>
                                        </p>
                                        <p>
                                            <i class="fa fa-users"></i> 
                                            Size : <span><?php echo $topPartner['size']; ?></span>
                                        </p>
                                        <p>
                                            <i class="fa fa-sitemap"></i> 
                                            No. of Sites : <span><?php echo $topPartner['num_sites']; ?></span>
                                        </p>

                                        <ul class="toprecruiter_toppicks">
                                            <?php echo getToppickFunction($topPartner['toppics_first']); ?>
                                            <?php echo getToppickFunction($topPartner['toppics_second']); ?>
                                        </ul>
                                    </figcaption>
                                    <!-- <a href="">03 Openings</a> -->
                                </div>
                            </div>
                <?php
                        $x++;
                        }
                    }
                ?>

                    <div class="col-sm-12" id="loadMore" style="">
                        <a href="javascript:void(0);" class="Loadmore">Load More</a>
                    </div>

                </div> 
            </div>
        </div>
    </section>

<?php
    function getToppickFunction($toppickID) {

        if($toppickID == 1) {
                                                            
            return '<li><img src="'.base_url() .'recruiterfiles/images/m_bonus.png"> Joining Bonus</li>';

        } else if($toppickID == 2) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_freefood.png"> Free Food</li>';

        } else if($toppickID == 3) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_day_1_hmo.png"> Day 1 HMO</li>';

        } else if($toppickID == 4) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_dependent_hmo.png"> Day 1 HMO for Dependent</li>';

        } else if($toppickID == 5) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_dayshift.png"> Day Shift</li>';

        } else if($toppickID == 6) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_14th_pay.png"> 14th Month Pay </li>';
        
        } else if($toppickID == 7) {

            return '<li><img src="'. base_url(). 'webfiles/newone/images/Searches-2.png"> Work From Home </li>';
        
        } else {
            return "";
        }                                                 
    }
?>

<?php
    include_once('footer1.php');
?>