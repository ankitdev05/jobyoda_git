<?php
    include_once('header2.php');
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {
        $requestURI = $_SERVER['REQUEST_URI'];
        if($requestURI == "/homepage/index") {
            redirect("https://jobyoda.com/"); 
        }
    } else {
        $link = "https";
        $link .= "://";
        $link .= $_SERVER['HTTP_HOST'];
        $link .= $_SERVER['REQUEST_URI'];
        redirect($link);
    }

    $userSess = $this->session->userdata('usersess'); 
    if ($this->session->userdata('userfsess')) {
        $userfsess = $this->session->userdata('userfsess');
        $type      = $userfsess['type'];
        $listingTypeFun = "jobs";
    } else {
        $listingTypeFun = "jobs";
    }
?>
<style>
    .salaryColor{color:#fbaf3d;}
    .JobArea .tab-content .tab-pane{min-height: 150px!important;}
    .JobArea .tab-content .tab-pane .nofound{font-size: 20px!important;} 

    .JobArea .tab-content .tab-pane .nofound{
        border: 1px solid #ddd;
    text-align: center;
    padding: 40px 0;
    margin: 40px 0 30px 0;
    border-radius: 7px;
    text-transform: capitalize;
    font-family: Roboto;
    font-weight: 600;
    font-size: 30px !important;
    color: #000;
}
.locationDetail ul li figcaption h5{ font-size: 11px!important;margin-top: 5px!important;margin-bottom: 5px!important;   }

.ActiveSearch{    background-color: #27aa60 !important;
    border: 5px solid #fff;
    width: 212x !important;
    height: 133x;}

.ActiveSearch .bpoImg{    transform: scale(1.1);}

.ActiveSearch .bpoImg img{    transform: scale(1) !important;}

.ActiveSearch .bpoContent{}

.ActiveSearch .bpoContent h2{
    font-size: 18px;
    padding: 0 0;
}
.inputDetails ul li {
    cursor: pointer;
}


.AllBpo{
    display: flex;
    flex-wrap: wrap;
}

.AllBpo li{ width: 23%; }

.AllBpo li:nth-child(5n +5){ margin: 15px  15px 0px 0px; }

@media screen and (max-width: 500px){
    .AllBpo li{ width: 100%; margin: 0 0 15px 0 !important; }
}


</style>

<!-- <div class='Loader'>
    <div class="Circle"></div>
</div> -->

  <section>
   <div class="SearchArea" style="padding: 94px 0 15px;">
      <div class="container">
         <div class="serachHead">
            
            <h1>Search Jobs <span style="font-size: 15px;">( Hiring for over <span style="color: #11b719;font-weight:600;font-size: 15px;"><?php echo $openings[0]['openings'];?></span> BPO positions! )</span></h1>

            <?php if (!empty($userSess)) { ?>
               <form method="post" action="<?php  echo base_url(); ?>search" class="has-validation-callback">
            <?php } else { ?>
                <form method="post" action="<?php echo base_url(); ?>search" class="has-validation-callback">
            <?php } ?>

            <div class="searchForm">
               <div class="form-group location">
                  <label>Keywords(What)</label>
                  <input type="text" id="myInput" class="form-control FocusShow" name="cname" placeholder="Search By Job Title, Company">
                  <!-- <ul class="inputDetails" style="display:none">
                      
                  </ul> -->

                  <input type="hidden" value="" name="dropdown_cname_selected" id="dropdown_cname_selected">
                  <input type="hidden" value="28.5355161" name="cur_lat">
                  <input type="hidden" value="77.3910265" name="cur_long">
                  <span class="locationPos"><i class="fa fa-search"></i></span>
                  

                  <!-- <div class="inputDetails" style="display:none">
                     
                  </div> -->
               </div>
               <div class="form-group location">
                  <label>Location(Where)</label>
                  <input type="text" class="form-control locationShow" name="locationn" placeholder="Enter City, Province" id="txtPlaces" autocomplete="off">
                  <input type="hidden" name="lat" value="" id="lati">
                  <input type="hidden" name="long" value="" id="longi">
                  <input type="hidden" name="getcity" id="cityi">
                  <span class="locationPos"><i class="fa fa-map-marker"></i></span>
                  
                  <div class="locationDetail">
                     
                     
                  </div>
               </div>
               <div class="btns">
                  <button type="submit" class="buttons">Search Jobs</button>
               </div>
               
               <div class="searhDrop">
                  
                  

               </div>

            </div>

            </form>

         </div>
      </div>
   </div>
</section>
    
    <section>
        <div class="AdvertisementArea" style="padding: 10px 0 0px!important;">
            <div class="container">
                <div class="advertNew">
                    <h3><span><img src="<?php echo base_url().'webfiles/';?>newone/images/Rocket.png" style="width: 25px;filter: inherit;margin-bottom:0px;margin-right: 5px;"></span><a href="<?php echo base_url(); ?>jobs/instant_screening">Apply For Instant  Screening Jobs here</a></h3>
                    <h3><span><img src="<?php echo base_url().'webfiles/';?>newone/images/Searches-2.png" style="width: 25px;filter: inherit;margin-bottom:5px;    margin-right: 5px;"></span><a href="<?php echo base_url(); ?>jobs/work_from_home">Apply For Work From Home here</a></h3>
                </div>
                <?php 
                    if(!empty($ad_list)) {
                ?>

                    <div class="owl-carousel owl-theme" id="Advertisement">
                <?php
                    foreach($ad_list as $ad_lists) {
                ?>
                    <div class="item">
                            <div class="AdvertisementBox">
                                <figure>
                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($ad_lists['recruiter_id']);} else { echo base_url();?>site_details/<?php echo base64_encode($ad_lists['recruiter_id']); }?>">
                                        <img src="<?php echo $ad_lists['banner']; ?>">
                                    </a>
                                </figure>
                                <?php
                                    if(strlen($ad_lists['description']) > 1) {
                                ?>
                                    <p> <?php echo $ad_lists['description']; ?> </p>
                                <?php
                                    }
                                ?>
                            </div>
                        </div>
                    <?php
                        }
                    ?>
                    </div>

                <?php
                    } else {
                ?>
                        <div class="owl-carousel owl-theme" id="Advertisement">
                            <div class="item">
                                <div class="AdvertisementBox">
                                    <figure>
                                            <img src="<?php echo base_url(); ?>images/new_banner_Man.png">
                                    </figure>
                                </div>
                            </div>
                        </div>
                <?php
                    }
                ?>
            </div>
        </div>    
    </section>


<section>
        <div class="SliderArea" style="padding: 14px 0 10px 0;">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="SliderText">

                            <h4>Best BPO jobs in the Philippines in one single site sorted by benefits and distance</h4>

                            <ul class="hotJobs">
                                <li><i class="fa fa-tags"></i> Trending Keywords :</li>
                                <li><a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/hotjob">Hot Jobs,</a></li>
                                <li><a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/work_from_home">Work From Home Jobs,</a></li> 
                                <li><a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/14_month_pay">Jobs With 14th Month Pay,</a></li>
                                <li><a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/day_shift">Day Shift Jobs</a></li> 
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="DownloadApp">
                            <a href="https://apps.apple.com/us/app/jobyoda/id1471619860?ls=1" class="download-btn" target="_blank">
                                        <span>
                                            <!-- <img src="https://jobyoda.com/webfiles/img/home/apple.png"> -->
                                            <img src="<?php echo base_url();?>webfiles/newone/social/ios_download_jobyoda.png">

                                        </span>
                                        <!-- <p>
                                            <small>Download On</small>
                                            <br>
                                            App Store
                                        </p> -->
                                    </a>
                                    <a href="https://play.google.com/store/apps/details?id=com.jobyodamo" class="download-btn" target="_blank">
                                        <span>
                                            <!-- <img src="https://jobyoda.com/webfiles/img/home/appstore.png"> -->
                                            <img src="<?php echo base_url();?>webfiles/newone/social/android_download_jobyoda.png">
                                        </span>
                                        <!-- <p>
                                            <small>Get It On</small>
                                            <br>
                                            Google Play
                                        </p> -->
                                    </a>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<section>
   <div class="popularSearch">
      <div class="container">
         <div class="popularSerIn">
            <h1>Popular Searches </h1>
            <h2>Get hired in top BPO Companies through JobYoDA</h2>
            <ul class="AllBpo">

               <li>
                  <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/nearby">
                     <div class="bpoAll">
                        <div class="bpoImg">
                           <img src="<?php echo base_url(); ?>webfiles/newone/images/icon-1.png" class="img-fluid" alt="img">
                        </div>
                        <div class="bpoContent">
                           <h2>Nearby Jobs</h2>
                           <p> Number Of Jobs : <?php echo $nearByJobs['jobcount']; ?></p>
                           <p> Opportunities : <?php echo $nearByJobs['openingcount']; ?></p>
                        </div>
                     </div>
                  </a>
               </li>

               <li>
                  <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/hotjob">
                     <div class="bpoAll">
                        <div class="bpoImg">
                           <img src="<?php echo base_url(); ?>webfiles/newone/images/fire.png" class="img-fluid" alt="img">
                        </div>
                        <div class="bpoContent">
                           <h2>Hot Jobs</h2>
                           <p> Number Of Jobs : <?php echo $hotJobs['jobcount']; ?></p>
                           <p> Opportunities : <?php echo $hotJobs['openingcount']; ?></p>
                        </div>
                     </div>
                  </a>
               </li>
               <li>
                  <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/work_from_home">
                     <div class="bpoAll">
                        <div class="bpoImg">
                           <img src="<?php echo base_url(); ?>webfiles/newone/images/icon-3.png" class="img-fluid" alt="img">
                        </div>
                        <div class="bpoContent">
                           <h2>Work From Home Jobs</h2>
                           <p> Number Of Jobs : <?php echo $workfromhome['jobcount']; ?></p>
                           <p> Opportunities : <?php echo $workfromhome['openingcount']; ?></p>
                        </div>
                     </div>
                  </a>
               </li>
               <li>
                  <a href="<?php echo base_url(); ?>jobs/instant_screening">
                     <div class="bpoAll">
                        <div class="bpoImg">
                           <img src="<?php echo base_url(); ?>webfiles/newone/images/icon-4.png" class="img-fluid" alt="img">
                        </div>
                        <div class="bpoContent">
                           <h2>Jobs With Instant Screening</h2>
                           <p> Number Of Jobs : <?php echo $instant['jobcount']; ?></p>
                           <p> Opportunities : <?php echo $instant['openingcount']; ?></p>
                        </div>
                     </div>
                  </a>
               </li>
               <li>
                  <a href="<?php echo base_url(); ?>jobs/free_food">
                     <div class="bpoAll">
                        <div class="bpoImg">
                           <img src="<?php echo base_url(); ?>webfiles/newone/images/icon-5.png" class="img-fluid" alt="img">
                        </div>
                        <div class="bpoContent">
                           <h2>Jobs With Free Food</h2>
                           <p> Number Of Jobs : <?php echo $freefood['jobcount']; ?></p>
                           <p> Opportunities : <?php echo $freefood['openingcount']; ?></p>
                        </div>
                     </div>
                  </a>
               </li>
               <li>
                  <a href="<?php echo base_url(); ?>jobs/information_technology">
                     <div class="bpoAll">
                        <div class="bpoImg">
                           <img src="<?php echo base_url(); ?>webfiles/newone/images/icon-8.png" class="img-fluid" alt="img">
                        </div>
                        <div class="bpoContent">
                           <h2>IT Jobs</h2>
                           <p> Number Of Jobs : <?php echo $itjobs['jobcount']; ?></p>
                           <p> Opportunities : <?php echo $itjobs['openingcount']; ?></p>
                        </div>
                     </div>
                  </a>
               </li>
               <li>
                  <a href="<?php echo base_url(); ?>jobs/14_month_pay">
                     <div class="bpoAll">
                        <div class="bpoImg">
                           <img src="<?php echo base_url(); ?>webfiles/newone/images/icon-7.png" class="img-fluid" alt="img">
                        </div>
                        <div class="bpoContent">
                           <h2>Jobs With 14th Month pay</h2>
                           <p> Number Of Jobs : <?php echo $monthpayJobs['jobcount']; ?></p>
                           <p> Opportunities : <?php echo $monthpayJobs['openingcount']; ?></p>
                        </div>
                     </div>
                  </a>
               </li>
               <li>
                  <a href="<?php echo base_url(); ?>jobs/hmo">
                     <div class="bpoAll">
                        <div class="bpoImg">
                           <img src="<?php echo base_url(); ?>webfiles/newone/images/icon-10.png" class="img-fluid" alt="img">
                        </div>
                        <div class="bpoContent">
                           <h2>Jobs With Day 1 HMO</h2>
                           <p> Number Of Jobs : <?php echo $day1hmo['jobcount']; ?></p>
                           <p> Opportunities : <?php echo $day1hmo['openingcount']; ?></p>
                        </div>
                     </div>
                  </a>
               </li>

               <li>
                  <a href="<?php echo base_url(); ?>jobs/leadership">
                     <div class="bpoAll">
                        <div class="bpoImg">
                           <img src="<?php echo base_url(); ?>webfiles/newone/images/icon-9.png" class="img-fluid" alt="img">
                        </div>
                        <div class="bpoContent">
                           <h2>Jobs With Leadership Position</h2>
                           <p> Number Of Jobs : <?php echo $leadershipJobs['jobcount']; ?></p>
                           <p> Opportunities : <?php echo $leadershipJobs['openingcount']; ?></p>
                        </div>
                     </div>
                  </a>
               </li>
               <li>
                  <a href="<?php echo base_url(); ?>jobs/signing_bonus">
                     <div class="bpoAll">
                        <div class="bpoImg">
                           <img src="<?php echo base_url(); ?>webfiles/newone/images/money-bag.png" class="img-fluid" alt="img">
                        </div>
                        <div class="bpoContent">
                           <h2>Jobs With Signing Bonus</h2>
                           <p> Number Of Jobs : <?php echo $bonusJobs['jobcount']; ?></p>
                           <p> Opportunities : <?php echo $bonusJobs['openingcount']; ?></p>
                        </div>
                     </div>
                  </a>
               </li>
               <li>
                  <a href="<?php echo base_url(); ?>jobs/day_shift">
                     <div class="bpoAll">
                        <div class="bpoImg">
                           <img src="<?php echo base_url(); ?>webfiles/newone/images/icon-11.png" class="img-fluid" alt="img">
                        </div>
                        <div class="bpoContent">
                           <h2>Day Shift Jobs</h2>
                           <p> Number Of Jobs : <?php echo $shiftJobs['jobcount']; ?></p>
                           <p> Opportunities : <?php echo $shiftJobs['openingcount']; ?></p>
                        </div>
                     </div>
                  </a>
               </li>
               <li class="active ActiveSearch">
                  <a href="<?php echo base_url(); ?>explore_jobs">
                     <div class="bpoAll">
                        <div class="bpoImg">
                           <img src="<?php echo base_url(); ?>webfiles/newone/images/icon-12.png" class="img-fluid" alt="img">
                        </div>
                        <div class="bpoContent">
                           <h2>Explore More <i class="fa fa-angle-double-right" aria-hidden="true"></i></h2>
                           <!-- <p> Numbers Of Jobs : <?php //echo $alljobs; ?></p> -->
                        </div>
                     </div>
                  </a>
               </li>
            </ul>
         </div>
      </div>
   </div>
</section>


    <section>
        <div class="CompanyArea">
            <div class="container">
                <h1>Our Partners
                    <a href="<?php echo base_url();?>our_partners">view all <i class="fa fa-angle-double-right"></i></a>
                </h1>
                <h2>Get hired in top BPO Companies through JobYoDA</h2>
                <?php
                    if(!empty($ourPartners)) {
                ?>
                <div class="owl-carousel owl-theme" id="Company">
                    <?php
                        foreach($ourPartners as $ourPartner) {
                    ?>
                            <div class="item">
                                <div class="CompanyBox"> 
                                    <figure>
                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($ourPartner['id']);}else{ echo base_url();?>site_details/<?php echo base64_encode($ourPartner['id']); }?>">
                                            <img src="<?php echo $ourPartner['image']; ?>">
                                        </a>
                                    </figure>
                                    <figcaption>
                                        <h3>
                                            <a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($ourPartner['id']);}else{ echo base_url();?>site_details/<?php echo base64_encode($ourPartner['id']); }?>">
                                                <?php if(strlen($ourPartner['cname']) > 45) { echo substr($ourPartner['cname'], 0,45).'...'; } else {echo $ourPartner['cname']; } ?>
                                            </a>
                                        </h3>
                                        <p>
                                            <i class="fa fa-briefcase"></i> 
                                            Numbers Of Jobs : <span><?php echo $ourPartner['jobcount']; ?></span>
                                        </p>
                                        <p>
                                            <i class="fa fa-briefcase"></i> 
                                            Opportunities : <span><?php echo $ourPartner['openingcount']; ?></span>
                                        </p>
                                    </figcaption>
                                    <!-- <a href="">03 Openings</a> -->
                                </div>
                            </div>
                    <?php
                        }
                    ?>
                </div>
                <?php
                    }
                ?>
            </div>
        </div>
    </section>

    <section>
        <div class="CityArea">
            <div class="container">
                <h1>City Search
                    <a href="<?php echo base_url();?>city_search">view all <i class="fa fa-angle-double-right"></i></a>
                </h1>
                <h2>Find your dream BPO Job in all Major Cities across the Philippines</h2>

                <?php
                    if(!empty($citySearchs)) {
                ?>
                <div class="owl-carousel owl-theme" id="City">
                    <?php
                        $cc=1;
                        foreach($citySearchs as $citySearch) {
                            if($cc <= 10) {
                    ?>
                        <div class="item">
                            <div class="CityBox">
                                <figure>
                                    <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/city/<?php echo $citySearch['cityslug']; ?>">
                                        <img src="<?php echo $citySearch['image']; ?>">
                                    </a>
                                </figure>
                                <figcaption>
                                    <h5> 
                                        <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/city/<?php echo $citySearch['cityslug']; ?>">
                                            <?php echo $citySearch['cityname']; ?>
                                        </a>
                                    </h5>
                                    <p>Number Of Jobs : <?php echo $citySearch['jobcount']; ?></p>
                                    <p>Opportunities : <?php echo $citySearch['openingcount']; ?></p>
                                </figcaption>
                            </div>
                        </div>
                    <?php
                            } else {
                                break;
                            }
                            $cc++;
                        }
                    ?>
                </div>
                <?php
                    }
                ?>
            </div>
        </div>
    </section>

    <section>
        <div class="ExpertiseArea">
            <div class="container">
                <h1 class="Title">
                    Job Categories
                    <a href="<?php echo base_url();?>your_expertise">view all <i class="fa fa-angle-double-right"></i></a>
                </h1>
                <h2>Apply for the best BPO jobs that match your skills</h2>

                <?php
                    if(!empty($expertises)) {
                ?>
                <div class="owl-carousel owl-theme" id="Expertise">
                    <?php
                        foreach($expertises as $expertise) {

                            if($expertise['jobcount'] > 0) {
                    ?>
                            <div class="item">
                                
                                <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/expertise/<?php echo base64_encode($expertise['id']);?>">
                                
                                    <div class="ExpertiseBox">
                                        <?php
                                            if($expertise['catname'] == "Sales") {
                                                $image = "images/Expertise-1.png";
                                            } elseif($expertise['catname'] == "Customer Care") {
                                                $image = "images/Expertise-2.png";
                                            } elseif($expertise['catname'] == "Technical Support") {
                                                $image = "images/Expertise-3.png";
                                            } elseif($expertise['catname'] == "HealthCare") {
                                                $image = "images/Expertise-4.png";
                                            } elseif($expertise['catname'] == "Shared Services Support") {
                                                $image = "images/Expertise-5.png";
                                            } elseif($expertise['catname'] == "Specialized Jobs") {
                                                $image = "images/Expertise-6.png";
                                            } elseif($expertise['catname'] == "Information Technology") {
                                                $image = "images/Expertise-7.png";
                                            } elseif($expertise['catname'] == "Banking") {
                                                $image = "images/Expertise-8.png";
                                            } elseif($expertise['catname'] == "Leadership Roles") {
                                                $image = "images/ex_leader.png";
                                            } elseif($expertise['catname'] == "Finance") {
                                                $image = "images/ex_finance.png";
                                            } elseif($expertise['catname'] == "Quality") {
                                                $image = "images/ex_quality.png";
                                            } elseif($expertise['catname'] == "Training") {
                                                $image = "images/ex_training.png";
                                            } elseif($expertise['catname'] == "Workforce") {
                                                $image = "images/ex_work.png";
                                            } elseif($expertise['catname'] == "Human Resources") {
                                                $image = "images/ex_hr.png";
                                            } 
                                        ?>
                                        
                                        <figure> 
                                            
                                                <img src="<?php echo base_url().'webfiles/newone/'.$image; ?>"> 
                                            
                                        </figure>
                                        <h3><?php echo $expertise['catname']; ?></h3>
                                        <p><span><?php echo $expertise['openingcount']; ?></span> Open Positions </p>
                                        <span class="Icon"><img src="<?php echo base_url().'webfiles/newone/'.$image; ?>"></span>
                                    </div>
                                </a>
                            </div>
                    <?php
                            }
                        }
                    ?>
                </div>
                <?php
                    }
                ?>
            </div>
        </div>
    </section>

<?php
    function getToppickFunction($toppickID) {

        if($toppickID == 1) {
                                                            
            return '<li><img src="'.base_url() .'recruiterfiles/images/m_bonus.png"> Joining Bonus</li>';

        } else if($toppickID == 2) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_freefood.png"> Free Food</li>';

        } else if($toppickID == 3) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_day_1_hmo.png"> Day 1 HMO</li>';

        } else if($toppickID == 4) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_dependent_hmo.png"> Day 1 HMO for Dependent</li>';

        } else if($toppickID == 5) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_dayshift.png"> Day Shift</li>';

        } else if($toppickID == 6) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_14th_pay.png"> 14th Month Pay </li>';
        
        } else if($toppickID == 7) {

            return '<li><img src="'. base_url(). 'webfiles/newone/images/Searches-2.png"> Work From Home </li>';
        
        } else {
            return "";
        }                                                 
    }
?>

<?php
    include_once('footer1.php');
?>