<?php include_once('header2.php'); 
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {

    } else {
        $link = "https";
        $link .= "://";
        $link .= $_SERVER['HTTP_HOST'];
        $link .= $_SERVER['REQUEST_URI'];
        redirect($link);
    }
?>

<style>
  .text-danger {
      color: #a94442!important;
  }
  .text-success {
      color: #3c763d!important;
  }
</style>

<section>
   <div class="BannerArea" style="background-image: url('<?php echo base_url(); ?>webfiles/img/newmap.jpg');">
      <h1>Set Password  </h1>
   </div>
</section>

<section class="forgetPass">
   <div class="container">
      <div class="row">
         <div class="col-sm-6">
            <div class="bgForget"><center style="margin:0px;">
               <img src="<?php echo base_url(); ?>webfiles/newone/images/bgforget.svg" class="img-fluid" alt = "bgforget" style="width:80%;">
             </center>
            </div>
         </div>
         <div class="col-sm-6">
            <div class="forgetRight otpRight">
               <h2 class="head">Set New Password</h2>

              <?php
                if(!empty($this->session->flashdata('msgg'))) {
                  echo $this->session->flashdata('msgg');
                }
              ?>
              <?php
                if(!empty($mismatch_err)) {
                  echo "<p class='text-center text-danger'>".$mismatch_err."</p>";;
                }
              ?>

              <?php 
                
                if(!empty($this->session->flashdata('msg'))) {
                  echo $this->session->flashdata('msg');
                } else {
              ?>

              <form action="<?php echo base_url('User/show')?>" id="registration-form" method="post">

               <div class="form-group formPost formPost2">
                  <input type="text" name="verify_code" placeholder="Enter Verification Code" data-validation="required" class="form-control inputAll" value="<?php if(!empty($userData['verify_code'])){ echo $userData['verify_code']; } ?>">

                  <span class="iconsAl iconsAl2">
                  <img src="<?php echo base_url(); ?>webfiles/newone/images/key.svg" class="img-fluid" alt = "key">
                  </span>

                  <?php if(isset($signuperrors['verify_code'])){echo "<p class='text-center text-danger'>".$signuperrors['verify_code']."</p>"; } ?>
               </div>

               <div class="form-group formPost formPost2">

                  <input type="password" id="password-field" name="new_pass" placeholder="Enter New Password" data-validation="required"  class="form-control inputAll" value="<?php if(!empty($userData['new_pass'])){ echo $userData['new_pass']; } ?>">
                  <span class="iconsAl iconsAl2">
                  <img src="<?php echo base_url(); ?>webfiles/newone/images/key.svg" class="img-fluid" alt = "key">
                  </span>

                  <?php if(isset($signuperrors['new_pass'])){echo "<p class='text-center text-danger'>".$signuperrors['new_pass']."</p>"; } ?>
               </div>

               <div class="form-group formPost formPost2">
                  <input type="password" id="cpassword-field" name="con_pass" placeholder="Enter Confirm Password" data-validation="required" class="form-control inputAll" value="<?php if(!empty($userData['con_pass'])){ echo $userData['con_pass']; } ?>">

                  <span class="iconsAl iconsAl2">
                  <img src="<?php echo base_url(); ?>webfiles/newone/images/key.svg" class="img-fluid" alt = "key"></span>

                  <?php if(isset($signuperrors['con_pass'])){echo "<p class='text-center text-danger'>".$signuperrors['con_pass']."</p>"; } ?>
               </div>

               <div class="resetPass">
                  <input type="hidden" name="uri_data" value="<?php echo base64_decode($uri_data); ?>">
                  <button type="submit" class="commonBtn1">Submit</button>
               </div>
              
              </form>

              <?php
                }
              ?>

            </div>
         </div>
      </div>
   </div>
</section>

<script>
$(document).ready(function(){   
  $("#forgetpass").click(function() {
    
    var user_email = $("#user_email").val();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "user/forgetpassword",
        data: {email:user_email},
        cache:false,
        success:function(htmldata){
            $('#errorfieldd').html(htmldata);    
        },
        error:function(){
          console.log('error');
        }
    });
  });
});
</script>

<?php include_once('footer1.php'); ?>

