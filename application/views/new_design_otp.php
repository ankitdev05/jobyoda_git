<?php include_once('header2.php'); 
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {

    } else {
        $link = "https";
        $link .= "://";
        $link .= $_SERVER['HTTP_HOST'];
        $link .= $_SERVER['REQUEST_URI'];
        redirect($link);
    }
?>

<style type="text/css">
    #partitioned {
      padding-left: 15px;
      letter-spacing: 36px;
      border: 0;
      background-image: linear-gradient(to left, black 70%, rgba(255, 255, 255, 0) 0%);
      background-position: bottom;
      background-size: 50px 1px;
      background-repeat: repeat-x;
      background-position-x: 35px;
      width: 220px;
      min-width: 220px;
      font-size: 20px;
    }

    #divInner{
      left: 0;
      position: sticky;
    }

    #divOuter{
      margin-top: 20px;
      margin-bottom: 20px;
      width: 190px; 
      overflow: hidden;
    }
    input#partitioned:focus {
        outline-width: 0!important;
    }
    .errorcode center{ margin-bottom: 10px!important; }
    .otpRight h2 {
    font-size: 14px!important;
    }
    .otpRight p {
        font-size: 16px!important;
    }
    .otpDesign {
            padding-top: 20px!important;
            padding-bottom: 20px!important;
    }
    #some_div{
        display: initial;
        margin-left: 20px;
        font-size: 16px;
        color: #27aa60;
        font-weight: 700;
    }

    .otpresend {
        font: normal normal normal 16px/19px "Quicksand", sans-serif;
        font-size: 16px;
        color: #fff;
        min-width: 186px;
        background: #084d87;
        border: none;
        border-radius: 4px;
        padding: 10px;
    }
    span#captchacodeimg {
        padding: 11px 20px;
        display: inline-block;
        background-color: #e4e4e4;
        font-size: 17px;
        font-style: italic;
    }


 .CapchaBox{
    margin: 0 0 20px 0;
}

    .CapchaBox .CapchaHead{
    margin: 0 0 20px 0;
    display: flex;
    align-items: center;
}

    .CapchaBox .CapchaHead span{
    padding: 12px 30px;
    display: inline-block;
    background-color: #d6d0d0;
    font-size: 18px;
    font-style: italic;
    font-weight: 500;
}

    .CapchaBox .CapchaHead a{
    margin: 0 0 0 10px;
    font-size: 17px;
    color: #7d7d7d;
}

    .CapchaBox .CapchaBody{}

    .CapchaBox .CapchaBody input{
    width: 100%;
    padding: 10px 20px;
    border: 1px solid #ddd;
    border-radius: 5px;
    font-size: 15px;
}



</style>

	<section>
        <div class="BannerArea" style="background-image: url('<?php echo base_url(); ?>webfiles/img/newmap.jpg');">
            <h1>Signup Verification </h1> 
        </div>
    </section>


    <section class="otpDesign">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="bgOtp">
                        <img src="<?php echo base_url(); ?>webfiles/newone/images/bgotp.svg" class="img-fluid" alt = "bgotp">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="otpRight">
                        
                        <p>One-Time Password has been sent to <?php //echo $phone_number; ?> <?php echo $email; ?></p>

                        <h2 class="head">Please enter the One-Time Password to verify your account. If you didn't get email on your inbox. Please check in your spam</h2>
                            
                        <form method="post" action="<?php echo base_url();?>auth/verifyotp" id="otpformsubmit">

                            <!-- <div class="otpAl">
                                <input type="text" name="number1" class="form-control otpInput" id="codeone">
                                <input type="text" name="number2" class="form-control otpInput" id="codetwo">
                                <input type="text" name="number3" class="form-control otpInput" id="codethree">
                                <input type="text" name="number4" class="form-control otpInput" id="codefour">
                            </div> -->

                            <div id="divOuter">
                                <div id="divInner">
                                    <input id="partitioned" name="number1" type="text" maxlength="4" autocomplete="off" />
                                </div>
                            </div>
                            
                            <?php
                                if(isset($error) && !empty($error)) {
                            ?>
                            <div class="col-sm-12 errorcode">
                                <?php echo $error; ?>    
                            </div>
                            <?php
                                }
                            ?>

                            <!-- <div>
                                <span id="captchacodeimg"> <?php echo $captcha_img;?> </span>
                            </div> -->

                            <div class="CapchaBox">
                                <div class="CapchaHead">
                                    <span id="recaptcha"><?php echo $captcha_img;?></span>
                                    <a href="javascript:void(0)" onclick="refreshcaptcha()"> <i class="fa fa-refresh"></i> </a>
                                </div>

                                <div class="CapchaBody">
                                    <input type="text" placeholder="Type the character above" id="captchafield" autocomplete="off"> 
                                    <span id="captchaerror" class="text-danger">  </span>
                                </div>

                            </div>
                            

                            <div class="valditBtn">
                                <button type="button" id="validatecaptcha" class="commonBtn1">Validate</button>
                                
                                <button type="button" class="otpresend" onclick="otpsendfunction()">Resend OTP</button>

                                <span id="some_div"> </span>
                            </div>
                        
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </section>

<script type="text/javascript">
    $(document).ready(function() {
        $("#codeone").keyup(function() {
            $("#codetwo").focus();
        });
        $("#codetwo").keyup(function() {
            $("#codethree").focus();
        });
        $("#codethree").keyup(function() {
            $("#codefour").focus();
        });
    });
</script>
<script type="text/javascript">
    var obj = document.getElementById('partitioned');
    obj.addEventListener('keydown', stopCarret); 
    obj.addEventListener('keyup', stopCarret); 

    function stopCarret() {
        if (obj.value.length > 3){
            setCaretPosition(obj, 3);
        }
    }

    function setCaretPosition(elem, caretPos) {
        if(elem != null) {
            if(elem.createTextRange) {
                var range = elem.createTextRange();
                range.move('character', caretPos);
                range.select();
            }
            else {
                if(elem.selectionStart) {
                    elem.focus();
                    elem.setSelectionRange(caretPos, caretPos);
                }
                else
                    elem.focus();
            }
        }
    }
</script>

<script>
    function otpsendfunction() {
        $.ajax({
           type: "GET",
           url: "<?php echo base_url(); ?>" + "auth/resendotp",
           cache: false,
           success: function(htmldata) {
               $("#some_div").css("display","initial");
               $(".otpresend").css("display","none");
                counterotp();
           },
           error: function() {
               console.log('error');
           }
        });
   }
</script>

<script type="text/javascript">

    function counterotp() {
        var timeLeft = 30;
        var elem = document.getElementById('some_div');
        var timerId = setInterval(countdown, 1000);

        function countdown() {
            if (timeLeft == -1) {
                clearTimeout(timerId);
                doSomething();
            } else {
                elem.innerHTML = 'Time left:' + timeLeft;
                timeLeft--;
            }
        }

        function doSomething() {
            $("#some_div").css("display","none");
            $(".otpresend").css("display","initial");
        }
    }
</script>

<script>
    function refreshcaptcha() {
        $("#captchaerror").html("");

        $.ajax({
           type: "GET",
           url: "<?php echo base_url(); ?>" + "auth/regeneratecaptcha",
           cache: false,
           success: function(htmldata) {
               //console.log(htmldata);
               $("#recaptcha").html(htmldata);
           },
           error: function() {
               console.log('error');
           }
        });
   }
</script>

<?php include_once('footer1.php'); ?>

<script>
    $(document).ready(function() {
        $("#validatecaptcha").on("click", function() {
            
            $("#captchaerror").html("");

            var captcha_text = $("#captchafield").val(); 
            
            if(captcha_text.length > 0) {

                $.ajax({
                    url: "<?php echo base_url(); ?>" + "auth/validatingcaptcha",
                    type: "POST",
                    //dataType: "json",
                    data: { captcha: captcha_text},
                    success: function(htmldata) {
                       console.log(htmldata);
                       if(htmldata == 1) {

                            $("#captchaerror").html("");

                            $("#otpformsubmit").submit();

                       } else {

                            $("#captchafield").val("");
                            $("#captchaerror").html("Captcha invalid, please retry!");

                            $.ajax({
                               type: "GET",
                               url: "<?php echo base_url(); ?>" + "auth/regeneratecaptcha",
                               cache: false,
                               success: function(htmldata) {
                                   $("#recaptcha").html(htmldata);
                               },
                               error: function() {
                                   console.log('error');
                               }
                            });   
                       }
                       
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log('error');
                        console.log(textStatus, errorThrown);
                    }
                });
            } else {
                
               $("#captchaerror").html("Captcha field is required");  
            }
        });
    });
   
</script>