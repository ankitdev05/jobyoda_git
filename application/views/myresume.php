<?php include_once('header.php'); ?>

<style>
    body {
        margin: 0;
        padding: 0;
    }

    .mainResume2 {
        width: 70%;
       margin: 0 auto;
        display: flex;
    box-shadow: 0 6px 12px rgba(0,0,0,.175);
    }

    .leftResume {
        width: 48%;
/*        background: #2C2E81;*/
        padding: 25px 12px;
    }

    .resumeImg {
        margin: 0 auto;
        height: 150px;
        width: 150px;
        overflow: hidden;
        border-radius: 100%;
    }

    .resumeCont h2 {
        font-size: 25px;
        color: #000;
        font-weight: 600;
        text-align: center;
        margin: 30px 0px;

    }

    h2,
    p,
    span {
        font-family: -webkit-pictograph;
    }

    .resumeCont ul li span.location {
        width: 55%;
        display: inline-block;
        color: #000;
        font-size: 13px;
        font-weight: 600;
        font-family: system-ui;
    }

    .resumeCont ul li span.loca {
        width: 62%;
        display: inline-block;
        color: #000;
        font-size: 13px;
    }

    .resumeCont ul li {
        display: flex;
        margin-bottom: 15px;
    }

    .resumeCont ul li:last-child {
/*        margin-top: 40px;*/
        margin-bottom: 0;
    }

    .resumeImg img {
        width: 100%;
        height: 100%;
        object-fit: cover;
    }

    .rightResume {
        padding: 25px 35px;
        width: 100%;
    }

    .headLeft h2.main {
        font-size: 20px;
        color: #fff;
        font-weight: 800;
        font-family: system-ui;
        text-transform: uppercase;
    }

    .headLeft p.main {
        font-size: 18px; 
        color: #fff;
        font-family: system-ui;
        text-transform: uppercase;
    }
    .headLeft{
        text-align: center;
        padding-top: 15px;
    }
    .headRight ul li span {
        margin-right: 5px;
        display: inline-block;
        width: 15px;
        height: 15px;
        position: relative;
/*        top: 6px;*/
    }
    }

    .headRight ul li span img {
        width: 100%;
        height: 100%;
        object-fit: cover;
    }

    .headRight ul li {
        margin-bottom: 5px;
        list-style-type: none;
        text-decoration: none;

    }

    .headRight ul li a {
        font-size: 13px;
        color: #000;
        ;
        font-family: system-ui;
        text-decoration: none;
        font-weight: 500;
    }

    .resumeHead {
        display: flex;
        justify-content: space-between;
    }

    .resumeCont ul {
        padding-left: 0;
    }

    .resumeDetails {
        padding: 15px 0px;
    }

    .resumeDetails h2 {
        font-size: 15px;
        font-family: system-ui;
        font-weight: 600;
        margin: 0;
    }

    .resumeDetails ul li {
        display: block;
        padding-top: 10px;
    }

    .resumeDetails ul li span.date {
        font-size: 14px;
        font-family: system-ui;
        display: inline;
        padding-right: 35px;
    }

    .resumeDetails ul li span.mainResume {
        font-size: 15px;
        font-family: system-ui;
        display: block;
    }

    ul {
        margin: 0;
        padding: 0;
    }

    ul li.content {
        font-size: 16px;
        color: #000;
        font-family: system-ui;
    }

    .name p {
        font-size: 14px;
        color: #000;
        padding: 0;
        margin: 0;
        font-family: system-ui;
        padding-top: 5px;
    }

    .widthFull {
        width: 100%;
        height: 20px;
        background: #ccc;
        border-radius: 10px;
        overflow: hidden;
    }

    .widthHalf {
        width: 80%;
        height: 20px;
        background: #26ae61;
        border-radius: 10px;
        overflow: hidden;
    }

    .widthAll p {
        margin: 0;
    }

    .widthAll p {
/*        padding-bottom: 15px;*/
    }

    .img {
        display: inline-block;
        position: relative;
        bottom: 18px;
        margin-right: 12px;
        display: inline-block;
    }

    .name {
        display: inline-block;
    }
    .resumeBtns a{
        font-size: 18px;
        color: #fff;
        background: #2C2E81;
        padding: 8px 25px;
        font-family: system-ui;
        border-radius: 5px;
        text-decoration: none;
    }
    .resumeAll{
        margin-top: 160px;
        margin-bottom: 20px;
    }
    .resumeBtns{
        text-align: right;
        padding-right: 7%;
    }
    .resumeDetailss{
            background: #2C2E81;
    padding: 20px 0px;
    border-radius: 10px;
    }
    .headRight{
        padding-top: 30px;
    }
</style>
</head>

<body><div class="resumeAll">
    <div class="container">
    <div class="resumeBtns">
        <a href="<?php echo base_url(); ?>resume_download" target="_blank">Download Resume</a> 
        <a href="<?php echo base_url(); ?>profile">Edit Profile</a> 
    </div> 
    </div>
    </div>
    <div class="mainResume2">
        <div class="leftResume">
            <div class="resumeDetailss">
            <div class="resumeImg">
                <?php
                    if(!empty($profileDetail['profilePic'])){
                ?>
                        <img src="<?php echo $profileDetail['profilePic'];?>" alt="profile">
                <?php
                    } else {
                ?>
                        <img src="<?php echo base_url().'webfiles/';?>img/profilepic.png" alt="profile">
                <?php
                    }
                ?>
            </div>
            <div class="headLeft">
                    <h2 class="main"> <?php if($profileDetail['name']){echo $profileDetail['name'];}?> </h2>
                    <p class="main"> <?php if($profileDetail['designation']){echo $profileDetail['designation'];}?> </p>
                </div>
            </div>
            <div class="resumeCont">
                <h2 class="about">About Me</h2>
                <ul>
                    <li><span class="location">Attainment</span><span class="loca">: <?php if($profileDetail['education']){echo $profileDetail['education'];}?></span></li>

                    <li><span class="location">City/Town</span><span class="loca">: <?php if($profileDetail['city']){echo $profileDetail['city'];}?></span></li>
                    <li><span class="location">State</span><span class="loca">: <?php if($profileDetail['state']){echo $profileDetail['state'];}?></span></li>
                    <!-- <li><span class="location">Designation</span><span class="loca">: <?php //if($profileDetail['designation']){echo $profileDetail['designation'];}?></span></li> -->
                    <li><span class="location">Job Interested In</span><span class="loca">: <?php if($profileDetail['jobsInterested']){echo implode(',',$profileDetail['jobsInterested']);}?></span></li>
                    
                    <li><span class="location">Job Search Status</span><span class="loca">: <?php if($profileDetail['jobsearch_status']){ if($profileDetail['jobsearch_status']==1){ echo "Actively seeking"; }elseif($profileDetail['jobsearch_status']==2){ echo "Open to offers"; }elseif($profileDetail['jobsearch_status']==1){ echo "Exploring"; } } ?></span></li>

                    <li><span class="location">Your Current/Last BPO</span><span class="loca">: <?php foreach($topbpos as $topbpo) { ?><?php if(!empty($profileDetail['topbpo'])){ if($profileDetail['topbpo']==$topbpo['name']) { echo $topbpo['name']; } } ?> <?php } ?></span></li>

                    <li><span class="location">Industry</span><span class="loca">: <?php foreach($industries as $industry) { ?><?php if($profileDetail['industry']==$industry['name']) { echo $industry['name']; }  ?> <?php } ?></span></li>

                    <li><span class="location">Preferred work mode</span><span class="loca">: <?php if($profileDetail['work_mode']){echo $profileDetail['work_mode'];}?></span></li>

                    <li><span class="location">Vaccination Status</span><span class="loca">: <?php if($profileDetail['vaccination']){echo $profileDetail['vaccination'];}?></span></li>

                    <li><span class="location">Willing to relocate</span><span class="loca">: <?php if($profileDetail['relocate']){echo $profileDetail['relocate'];}?></span></li>
                </ul>
            </div>
            <div class="headRight">
                    <ul>
                        <li><a href="#0"><span><img src="https://www.flaticon.com/svg/static/icons/svg/484/484167.svg" alt=""></span> <?php if($profileDetail['location']){echo $profileDetail['location'];}?></a></li>
                        <li><a href="#0"><span><img src="https://www.flaticon.com/svg/static/icons/svg/597/597177.svg" alt=""></span><?php if($profileDetail['country_code']){echo $profileDetail['country_code'];}?><?php if($profileDetail['phone']){echo $profileDetail['phone'];}?>                          </a></li>
                        <li><a href="#0"><span><img src="https://www.flaticon.com/svg/static/icons/svg/725/725643.svg" alt=""></span><?php if($profileDetail['email']){echo $profileDetail['email'];}?>                            </a></li>
                    </ul>
                </div>
        </div>
        <div class="rightResume">
 
            <div class="resumeDetails">
                <h2 class="head">SPECIALIZATION</h2>
                <ul>
                <?php foreach($categorylist as $category) { ?>
                <?php if($profileDetail['specialization']==$category['category']) { ?>
                    <li><span class="date"> <?php echo $category['category']; ?> </span></li>
                <?php } } ?>
                </ul>
            </div>
            <div class="resumeDetails">
                <h2 class="head">SUB-SPECIALIZATION</h2>
                <ul>
                <?php foreach($subcategorylist1 as $subcategory) { ?>
                <?php if($profileDetail['sub_specialization']==$subcategory['subcategory']) { ?>
                            <li><span class="date"> <?php echo $subcategory['subcategory']; ?> </span></li>
                <?php break; } } ?>
                </ul>
            </div>
 

            <div class="resumeDetails">
                <h2 class="head">TOTAL EXPERIENCE</h2>
                <ul>
                    <li><span class="date"><?php if($profileDetail['exp_year']){echo $profileDetail['exp_year'];}?> Years <?php if($profileDetail['exp_month']){echo $profileDetail['exp_month'];}?> Months </span><span class="mainResume">
                        </span></li>
                </ul>
            </div>
            <div class="resumeDetails">
                <h2 class="head">WORK & EXPERIENCE</h2>

                <?php
                    if(!empty(array_filter($workData))) { 
                    foreach($workData as $work) {
                ?> 
                <ul>
                    <li><span class="date">Designation : <?php echo $work['title']; ?> </span><span class="mainResume" style="padding-top:5px;">Company : <?php echo $work['company']; ?>
                        </span></li>
                    <li><span class="date"><?php echo $work['from']; ?> - <?php if($work['to']=='') { echo "Present"; }else{ echo $work['to'];} ?></span>
                    <li class="content"><?php echo $work['desc']; ?></li>
                    </li>
                </ul>
                <?php
                    }}
                ?>

            </div>

            <div class="resumeDetails">
                <h2 class="head">EDUCATION
                </h2>
                <ul>
                <?php
                    if(!empty(array_filter($eduData))) {
                    foreach($eduData as $edu) {
                ?>
                    <li><span class="img"><img src="<?php echo base_url();?>webfiles/newone/images/education.png" alt="education" style="width:35px;height:35px;margin-bottom:10px;">
                        </span><span class="name">
                            <p><?php echo $edu['university']; ?></p>
                            <p><?php echo $edu['from']; ?> - <?php echo $edu['to']; ?></p>
                            <p><?php echo $edu['attainment']; ?> <?php echo $edu['degree']; ?></p>
                        </span>
                    </li>
                <?php
                    }}
                ?>
                </ul>
            </div>
            

            <div class="resumeDetails">
                <h2 class="head">LANGUAGES SPOKEN</h2>
                <ul>
                    <li><span class="img"> <i class="la la-graduation-cap"></i></span><span class="name">
                        <?php
                            if(!empty($languagelists)) {
                                $langArray = array();
                                foreach($languageData as $languageD) {
                                    $langArray[] = $languageD['lang_id'];
                                }
                            foreach($languagelists as $languagelist) {
                        ?>
                        <?php if(in_array($languagelist['id'], $langArray)) { ?>

                                <p><img src="<?php echo base_url();?>webfiles/newone/images/lang.png" alt="education" style="width:15px;height:15px;margin-right:10px;display:inline-block;"><?php echo $languagelist['name']?></p>
                        <?php
                              }
                            }}
                        ?>
                        </span>
                    </li>
                </ul>
            </div>

            <div class="resumeDetails">
                <h2 class="head">TOP CLIENT SUPPORTED
                </h2>
                <ul>
                    <li><span class="img"> <i class="la la-graduation-cap"></i></span>
                        <span class="name">
                        <?php if(!empty(array_filter($clientData))) {
                            foreach($clientData as $client) {
                        ?> 
                            <p><img src="<?php echo base_url(); ?>webfiles/newone/images/home1.png" alt="education" style="width:15px;height:15px;margin-right:10px;display:inline-block;position:relative;bottom:3px;"><?php echo $client['clients']; ?></p>
                        <?php } } ?>
                        </span>
                    </li>
                </ul>
            </div>

            <!-- <div class="resumeDetails">
                <h2 class="head">USER ASSESSMENT</h2>
                <ul class="widthAll">
                    <li>
                        <p>Verbal</p>
                        <div class="widthFull">
                            <div class="widthHalf" style="width:<?php //echo $assessData[0]['verbal'] * 20; ?>%"></div>
                        </div>
                    </li>
                    <li>
                        <p>Written</p>
                        <div class="widthFull">
                            <div class="widthHalf" style="width:<?php //echo $assessData[0]['written'] * 20; ?>%"></div>
                        </div>
                    </li>
                    <li>
                        <p>Listening</p>
                        <div class="widthFull">
                            <div class="widthHalf" style="width:<?php //echo $assessData[0]['listening'] * 20; ?>%"></div>
                        </div>
                    </li>
                    <li>
                        <p>Problem Solving</p>
                        <div class="widthFull">
                            <div class="widthHalf" style="width:<?php //echo $assessData[0]['problem'] * 20; ?>%"></div>
                        </div>
                    </li>
                </ul>
            </div> -->

        </div>
    </div>


    <script src="<?php echo base_url().'webfiles/';?>js/moment.min.js"></script>
    <script src="<?php echo base_url().'webfiles/';?>js/bootstrap-datetimepicker.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <?php include_once('footer.php'); ?>
</body>

</html>
