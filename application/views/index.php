


<head>
  <!DOCTYPE html>
  <html>
  <head>
    <title>Jobyoda</title>
    <link href="<?php echo base_url(); ?>webfiles/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>webfiles/css/style.css" rel="stylesheet" type="text/css">

  </head>
  <style>
body, html {
  height: 100%;
  margin: 0;
}

.bgimg {
  background-image: url(../../webfiles/img/newmap.jpg);
  height: 100%;
  background-position: center;
  background-size: cover;
  position: relative;
  color: white;
  font-size: 25px;
      display: flex;
}

.topleft {
    position: relative;
    width: 18%;
    right: 0;
    margin: auto;
}

.topleft img{
  width:100%;
}


.bottomleft {
  position: absolute;
  bottom: 0;
  left: 16px;
}

.middle {
    position: relative;
    top: 0;
    text-align: center;
    width: 100%;
    float: left;
}

hr {
  margin: auto;
  width: 40%;
}

.bgarlk {
    margin: auto;
    background: #fff;
    width: 43%;
    box-shadow: #fff 0px 0px 12px 1px;
    padding: 30px;
}

.bgarlk h1 {
    color: #797979;
}

.bgarlk p {
    color: #34b060;
}

.bgarlk .topleft {
    width: 54%;
}

</style>
<body>

<div class="bgimg">
<div class="bgarlk">
  <div class="topleft">
    <p><img src="<?php echo base_url(); ?>recruiterfiles/images/jobyoda.png"></p>
  </div>
  <div class="middle">
    <h1>COMING SOON</h1>
    <hr>
    <p id="demo" style="font-size:30px"></p>
  </div>
  </div>
  
</div>

<script> 
// Set the date we're counting down to
var countDownDate = new Date("Sep 30, 2019 2:37:25").getTime();

// Update the count down every 1 second
var countdownfunction = setInterval(function() {

  // Get todays date and time
  var now = new Date().getTime();
  
  // Find the distance between now an the count down date
  var distance = countDownDate - now;
  
  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (400 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
  
  // Output the result in an element with id="demo"
  document.getElementById("demo").innerHTML = days + "d " + hours + "h "
  + minutes + "m " + seconds + "s ";
  
  // If the count down is over, write some text 
  if (distance < 0) {
    clearInterval(countdownfunction);
    document.getElementById("demo").innerHTML = "EXPIRED";
  }
}, 1000);
</script>

</body>
  </html>
</head>
