<!doctype html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <title>JobYoDA | Resume</title>
      <style>
         table {
         font-family: arial, sans-serif;
         border-collapse: collapse;
         width: 100%;
         }
         td,
         th {
         /*            border: 1px solid #dddddd;*/
         text-align: left;
         /*            padding: 8px;*/
         padding: 0;
         margin: 0;
         }
         p{
         padding-top: 10px;
         margin: 0;
         }
         td.bg table tr td p{
         padding-top: 15px;
         }
         tr:nth-child(even) {
         /*  background-color: #dddddd;*/
         }
         td:first-child,
         th:first-child {
         width: 260px;
         }
         .imgsProf {
         text-align: center;
         }
         .imgsProf img {
         border-radius: 60px;
         }
         .name {
         font-size: 42px;
         }
         .RightSide p {
         padding-top: 12px;
         margin: 0;
         /*            font-family: 'D-DIN';*/
         }
         .RightSide p .titleL {
         font-size: 16px;
         color: #000;
         }
         .RightSide p .titleR {
         font-size: 16px;
         color: #000;
         font-weight: 100;
         }
         h2 {
         margin: 0;
         padding: 0;
         }
         .head1,
         .head2 {
         font-size: 14px;
         color: #000;
         display: block;
         /*            float: left;*/
         /*            font-weight: 600;*/
         }
         .head1 {
         display: inline-block;
         color: #558bb8;
         /*            width: 170px;*/
         }
         .padding.imgsProf.LeftSide img{
         border: 4px solid #558bb8;
         }
         .headed,
         .head1 {
         font-weight: 600;
         }
         .headed,
         .headed2 {
         font-size: 14px;
         display: block;
         color: #000;
         font-weight: 400;
         }
         .head1.headnew {
         display: block;
         }
         .head2.head2new {
         display: block;
         }
         .detail12 {
         display: block;
         }
         .headed.headednew {
         display: block;
         }
         .skilldet {
         background: #0e433e;
         height: 15px;
         color: #fff;
         text-align: center;
         margin-left: 5px;
         display: inline-block;
         }
         .head2.head2logo {
         display: block;
         }
         span.headed {
         font-weight: 600;
         }
         .headed2,
         .head2 {
         padding-top: 5px;
         }

         .workingDet {
         display: block;
         padding-top: 5px;
         }
         .img {
         width: 15px;
         margin-right: 8px;
         }
         .education{
         display: block;
         padding-top: 5px;
         }
         .contactDetails{
         padding-top: 5px;
         display: block;
         }
         .detail1{
         padding-bottom: 3px;
         display: block;
         }
         .bg{
         background: #ebe9e961;
         }
         .bg p{
         padding: 0px 10px;
         }
         .paddo p{
         padding-left: 20px;
         }
         .paddo h2{
         padding-left: 20px;
         }
         .RightSide.paddo h2.demo{
         font-size: 48px;
         padding-right: 25px;
         font-weight: 100px;
         text-transform: uppercase
         }
           .skillsDetails p{
          padding-left: 0;
         }
      </style>
   </head>
   <body>
      <table>
         <tr>
            <td   class ="bg" style="vertical-align: top;">
               <table>
                  <tr>
                     <td>
                        <p class="padding imgsProf LeftSide ">
                          <?php
                              if(!empty($profileDetail['profilePic'])){
                          ?>
                              <img src="<?php echo $profileDetail['profilePic'];?>" style="width:150px;height:150px;">
                          <?php
                              } else {
                          ?>
                              <img src="<?php echo base_url();?>webfiles/img/profilepic.png">
                          <?php
                              }
                          ?>
                        </p>
                        <!--
                           <p><span class="head1">JOB SEARCH STATUS :</span><span class="head2">On Hold</span></p>
                           <p><span class="head1">SPECIALIZATION :</span><span class="head2">UI Developer</span></p>
                           <p><span class="head1">JOBS INTERESTED IN :</span><span class="head2">UI Developer</span></p>
                           <p><span class="head1">CURRENT/ LAST BPO :</span><span class="head2">Mobulous</span></p>
                           <p><span class="head1">BPO EXPERIENCE :</span><span class="head2">4+ year</span></p>
                           <p><span class="head1 headnew">CONTACT INFO :</span><span class="head2 head2new">
                                   <span class="contactDetails">
                                       <span class="detail1">PHONE:</span>
                                       <span class="detail12">8126362520</span>
                                   </span>
                                   <span class="contactDetails">
                                       <span class="detail1">EMAIL:</span>
                                       <span class="detail12">Deepanshu@gmail.com</span>
                                   </span>
                               </span></p>
                           <p><span class="head1">BEST TIME TO CONTACT :</span><span class="head2">4 p.m</span></p>
                           <p style="margin-top:125px;"><span class="head1">RESUME CREATED BY :</span><span class="head2 head2logo " style="display:block;width:100%;margin-top:8px;"><img src="https://jobyoda.com/webfiles/newone/images/logonew.png" ></span></p>
                           -->
                     </td>
                  </tr>
                  <?php if(strlen($profileDetail['jobsearch_status']) > 0) { ?>
                  <tr>
                     <td>
                        <p><span class="head1">JOB SEARCH STATUS :</span><span class="head2"> <?php if($profileDetail['jobsearch_status']){ if($profileDetail['jobsearch_status']==1){ echo "Actively seeking"; }elseif($profileDetail['jobsearch_status']==2){ echo "Open to offers"; }elseif($profileDetail['jobsearch_status']==1){ echo "Exploring"; } } ?> </span></p>
                     </td>
                  </tr>
                  <?php } ?>
                  <tr>
                     <td>
                        <p><span class="head1">INDUSTRY :</span><span class="head2"> 
                          <?php if(!empty($profileDetail['industry'])) { ?>
                          <?php //foreach($categorylist as $category) { ?>
                          <?php //if($profileDetail['specialization']==$category['category']) { ?>
                              <?php echo $profileDetail['industry']; ?>
                          <?php //} } ?>                            
                          <?php } ?>       
                      </span></p>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <p><span class="head1">SPECIALIZATION :</span><span class="head2"> 
                          <?php if(!empty($profileDetail['specialization'])) { ?>
                          <?php //foreach($categorylist as $category) { ?>
                          <?php //if($profileDetail['specialization']==$category['category']) { ?>
                              <?php echo $profileDetail['specialization']; ?>
                          <?php //} } ?>                            
                          <?php } ?>       
                      </span></p>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <p><span class="head1">SUB-SPECIALIZATION :</span><span class="head2"> 
                          <?php if(!empty($profileDetail['sub_specialization'])) { ?>
                          <?php //foreach($categorylist as $category) { ?>
                          <?php //if($profileDetail['specialization']==$category['category']) { ?>
                              <?php echo $profileDetail['sub_specialization']; ?>
                          <?php //} } ?>                            
                          <?php } ?>       
                      </span></p>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <p><span class="head1">JOBS INTERESTED IN :</span><span class="head2"> <?php if($profileDetail['jobsInterested']){echo implode(', ',$profileDetail['jobsInterested']);}?> </span></p>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <p><span class="head1">CURRENT/ LAST BPO :</span><span class="head2"> <?php if($profileDetail['topbpo']){echo $profileDetail['topbpo'];}?> </span></p>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <p><span class="head1">PREFERRED WORK MODE :</span><span class="head2"> <?php if($profileDetail['work_mode']){echo $profileDetail['work_mode'];}?> </span></p>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <p><span class="head1">VACCINATION STATUS :</span><span class="head2"> <?php if($profileDetail['vaccination']){echo $profileDetail['vaccination'];}?> </span></p>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <p><span class="head1">WILLING TO RELOCATE :</span><span class="head2"> <?php if($profileDetail['relocate']){echo $profileDetail['relocate'];}?> </span></p>
                     </td>
                  </tr>
                  
                  <tr>
                     <td>
                        <p><span class="head1 headnew">CONTACT INFO :</span><span class="head2 head2new">
                           <span class="contactDetails">
                           <span class="detail1">PHONE:</span>
                           <span class="detail12"><?php if($profileDetail['country_code']){echo $profileDetail['country_code'];}?><?php if($profileDetail['phone']){echo $profileDetail['phone'];}?></span>
                           </span>
                           <span class="contactDetails">
                           <span class="detail1">EMAIL:</span>
                           <span class="detail12"><?php if($profileDetail['email']){echo $profileDetail['email'];}?></span>
                           </span>
                           </span>
                        </p>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <p style="margin-top:125px;"><span class="head1">RESUME CREATED BY :</span><span class="head2 head2logo " style="display:block;width:100%;margin-top:8px;"><img src="<?php echo base_url();?>webfiles/newone/images/logonew.png" ></span></p>
                     </td>
                  </tr>
               </table>
            </td>
            <td class="paddo" style="vertical-align: top;" >
               <table>
                  <tr>
                     <td>
                        <div class="RightSide paddo">
                           <h2 class="demo"> <?php if($profileDetail['name']){echo $profileDetail['name'];}?> </h2>
                           <p></p>
                           <p><span class="titleL"><b>Location :</b></span><span class="titleR"> <?php if($profileDetail['location']){echo $profileDetail['location'];}?> </span></p>
                        </div>
                     </td>
                  </tr>

                  <tr>
                     <td>
                        <p><span class="headed">EXPERIENCE :</span><span class="head2"> <?php if($profileDetail['exp_year']){echo $profileDetail['exp_year'];}?> Years <?php if($profileDetail['exp_month']){echo $profileDetail['exp_month'];}?> Months </span></p>
                     </td>
                  </tr>

                  <tr>
                     <td>
                        <p><span class="headed">Job Level :</span><span class="head2"> <?php if($profileDetail['joblevel']){echo $profileDetail['joblevel'];}?> </span></p>
                     </td>
                  </tr>

                  <tr>
                     <td>
                        <p><span class="headed">Highest Qualification :</span><span class="head2"> <?php if($profileDetail['education']){echo $profileDetail['education'];}?> </span></p>
                     </td>
                  </tr>

                  <tr>
                     <td>
                        <p><span class="headed">EDUCATIONAL ATTAINMENT :</span>
                          
                          <?php
                              if(!empty(array_filter($eduData))) {
                              foreach($eduData as $edu) {
                          ?>

                            <span class="headed2">
                              <span class="workingDet"><img src="<?php echo base_url(); ?>webfiles/newone/images/education.png" class="img" style="display:inline-block;vertical-align:top;margin-right:20px;width:25px;"> 
                              <span style="display:inline-block;">
                              <span class ="education"> <?php echo $edu['university']; ?> </span>
                              <span class ="education"> <?php echo $edu['from']; ?> - <?php echo $edu['to']; ?> </span>
                              <span class ="education"> <?php echo $edu['attainment']; ?> <?php echo $edu['degree']; ?> </span></span>
                              </span>
                            </span>
                          
                          <?php
                              }}
                          ?>
                        </p>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <p><span class="headed">WORK EXPERIENCE : </span>
                          
                          <?php
                              if(!empty(array_filter($workData))) { 
                              foreach($workData as $work) {
                          ?>
                           <span class="headed2" style="padding-top:5px;">
                           <span class="workingDet">
                           Designation : <?php echo $work['title']; ?>
                           </span>
                           <span class="workingDet">
                           Company : <?php echo $work['company']; ?>
                           </span>
                           <span class="workingDet">
                           <?php echo $work['from']; ?> - <?php if($work['to']=='') { echo "Present"; }else{ echo $work['to'];} ?>
                           </span></span>
                          
                          <?php
                              }}
                          ?>
                        </p>
                     </td>
                  </tr>
                  
                  <tr>
                     <td>
                        <p><span class="headed">LANGUAGES SPOKEN : </span><span class="headed2">
                          <?php
                              if(!empty($languagelists)) {
                                  $langArray = array();
                                  foreach($languageData as $languageD) {
                                      $langArray[] = $languageD['lang_id'];
                                  }
                              foreach($languagelists as $languagelist) {
                          ?>
                          <?php if(in_array($languagelist['id'], $langArray)) { ?>
                           <span class="workingDet"><img src="<?php echo base_url(); ?>webfiles/newone/images/lang.png" class="img"> <?php echo $languagelist['name']?> </span>
                          
                          <?php
                                }
                              }}
                          ?>
                           </span>
                        </p>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <p><span class="headed">TOP CLIENT SUPPORTED : </span><span class="headed2">
                          <?php if(!empty(array_filter($clientData))) {
                              foreach($clientData as $client) {
                          ?> 
                           <span class="workingDet"><img src="<?php echo base_url(); ?>webfiles/newone/images/home1.png" class="img"> <?php echo $client['clients']; ?> </span>
                           
                          <?php } } ?>
                           </span>
                        </p>
                     </td>
                  </tr>

               </table>
               <!--
                  <div class="RightSide paddo">
                  <h2 class="demo">Deepanshu Tyagi</h2>
                  <p><span class="titleL">JOB TITLE HERE :</span><span class="titleR">UI Developer</span></p>
                  <p><span class="titleL">LOCATION :</span><span class="titleR">Meerut</span></p>
                  </div>
                  <p><span class="headed">EDUCATIONAL ATTAINMENT :</span><span class="headed2">
                      <span class="workingDet"><img src="<?php echo base_url(); ?>webfiles/newone/images/education.png" class="img" style="display:inline-block;vertical-align:top;margin-right:20px;width:25px;"> 
                         <span style="display:inline-block;">
                          <span class ="education">abc university</span>
                          <span class ="education">06-08-2020 - 06-08-2020</span>
                          <span class ="education">High School Graduate MCA</span></span>
                  </span>
                  
                  
                  
                  </span></p>
                  <p><span class="headed">WORK EXPERIENCE : </span><span class="headed2" style="padding-top:5px;">
                      <span class="workingDet">
                          Designation : developer
                      </span>
                      <span class="workingDet">
                          Company : abc company
                      </span>
                      <span class="workingDet">
                          18-02-2021 - 12-02-2021
                  
                      </span></span></p>
                       <p><span class="headed">WORK EXPERIENCE : </span><span class="headed2" style="padding-top:5px;">
                      <span class="workingDet">
                          Designation : developer
                      </span>
                      <span class="workingDet">
                          Company : abc company
                      </span>
                      <span class="workingDet">
                          18-02-2021 - 12-02-2021
                  
                      </span></span></p>
                  
                  <p><span class="headed">SPECIALIZATION : </span><span class="headed2">UI</span></p>
                  <p><span class="headed">PROFESSIONAL SKILLS : </span><span class="headed2">
                      <span class="workingDet"><img src="<?php echo base_url(); ?>webfiles/newone/images/skills.png" alt="education" class="img">Development</span>
                      <span class="workingDet"><img src="<?php echo base_url(); ?>webfiles/newone/images/skills.png" alt="education" class="img">Communication</span>
                  </span></p>
                  <p><span class="headed">LANGUAGES SPOKEN : </span><span class="headed2">
                      <span class="workingDet"><img src="<?php echo base_url(); ?>webfiles/newone/images/lang.png" class="img"> Hindi</span>
                      <span class="workingDet"><img src="<?php echo base_url(); ?>webfiles/newone/images/lang.png" class="img"> English</span>
                  </span></p>
                  <p><span class="headed">TOP CLIENT SUPPORTED : </span><span class="headed2">
                      <span class="workingDet"><img src="<?php echo base_url(); ?>webfiles/newone/images/home1.png" class="img"> Mobulous</span>
                      <span class="workingDet"><img src="<?php echo base_url(); ?>webfiles/newone/images/home1.png" class="img"> HCL</span>
                  </span></p>
                  <p><span class="headed headednew">USER ASSESSMENT : </span><span class="headed2 headed2new">
                      <span class="skillsDetails">
                          <p><span class="skill">skill1</span><span class="skilldet" style=" width: 200px;">50%</span></p>
                          <p><span class="skill">skill2</span><span class="skilldet" style=" width: 170px;">40%</span></p>
                          <p><span class="skill">skill3</span><span class="skilldet" style=" width: 150px;">30%</span></p>
                          <p><span class="skill">skill4</span><span class="skilldet" style=" width: 100px;">20%</span></p>
                      </span>
                  </span></p>
                  
                  -->
            </td>
         </tr>
      </table>
   </body>
</html>

