<?php include_once('header.php'); ?>
<style type="text/css">
   @import url('https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900&display=swap');
   ..srchbtns{
         width: 20%!important;
         margin: 0 auto!important;
   }
   .filldetails input.form-control,
   .profileformstop .filldetails select {
   box-shadow: none;
   height: 40px !important;
   padding: 7px 15px !important;
   font-family: Roboto;
   font-weight: 500;
   color: #000;
   margin: 0;
   width: 100%;
   float: none;
   }
   .clear {
   clear: both
   }
   .basetst p {
   font-size: 17px;
   margin: 3px 0 0 0;
   font-family: Roboto;
   color: #000;
   font-weight: 500;
   }
   .paneleityodas {
   border: 1px solid #ddd;
   box-shadow: none;
   border-radius: 5px;
   margin: 0 20px 20px 0;
   padding: 15px;
   }
   .showinfoarea.mypaneleditfsa {
   position: relative;
   float: left;
   width: 95%;
   box-shadow: none;
   padding: 20px;
   min-height: 130px;
   margin-bottom: 10px;
   border: 1px solid #ddd;
   border-radius: 8px;
           overflow: auto;
   }
   .showinfoarea.mypaneleditfsa .paneleityodas {
   box-shadow: none;
   position: absolute;
   top: 20px;
   right: 0px;
   width: auto;
   }
   .showinfoarea p.designation {
   color: #1f1f1f;
   font-weight: 500;
   font-family: roboto;
   font-size: 15px;
   padding: 0 10px 0px 0;
   }
   .showinfoarea p {
   color: #949191;
   font-size: 14px;
   margin: 0;
   font-family: roboto;
   }
   .fullratings .checkflowrt label {
   font-size: 11px;
   padding: 5px 0;
   font-family: Roboto;
   }
   .addcall {
   cursor: pointer;
   font-family: Roboto;
   font-weight: 600;
   }
   .checkflowrt label {
   font-size: 14px;
   padding: 4px 0 0 2px;
   font-family: Roboto;
   font-weight: 500;
   }
   .custom-control-label::before {
   border: 1px solid #464646;
   }
   .ratingschecks.formratesweaks .centerratingdf {
   margin: 35px auto;
   width: 50%;
   padding: 10px;
   box-shadow: none;
   border: 1px solid #ddd;
   padding: 25px;
   border-radius: 10px;
   }
   .ratingschecks.formratesweaks .centerratingdf p {
   font-family: Roboto;
   font-weight: 500;
   color: #000;
   font-size: 14px;
   margin: 0 0 10px;
   }
   .ratingschecks.formratesweaks {
   float: left;
   margin: 10px auto 50px;
   background: #fff;
   border: 1px solid #ddd;
   padding: 10px;
   width: 100%;
   box-shadow: 0px 1px 6px #cacaca;
   border-radius: 15px;
   }
   .innerbglay .adminopnts {
   padding: 0;
   box-shadow: none;
   border-radius: 0;
   background-color: transparent;
   }
   .InfomationBox {
   background-color: #fff;
   padding: 25px;
   border-radius: 10px;
   margin: 0 0 30px 0;
   border: 1px solid #ddd;
   }
   .InfomationBox .editssections {
   padding: 0 0 20px 0;
   margin: 0 0 20px 0;
   }
   .InfomationBox .editssections.EditNone {
   padding: 0;
   border: none;
   margin: 0
   }
   .InfomationBox .showinfoarea {
   margin: 20px 10px 0 0;
   float: left;
   width: 48%;
   }
   .InfomationBox .showinfoarea .paneleityodas {
   margin: 0;
   padding: 10px;
   width: 100%;
   display: block;
   }
   .InfomationBox .showinfoarea .paneleityodas p.designation {
   width: 85%;
   float: left;
 overflow: hidden;
           white-space: nowrap;
   }
   .InfomationBox .showinfoarea .paneleityodas p {
   float: right;
   }
   .ProfileUploadPic {
   position: relative;
   width: 150px;
   height: 150px;
   border: 5px solid #27aa60;
   border-radius: 50%;
   overflow: hidden;
   background-color: #000;
   margin: auto;
   }
   .ProfileUploadPic span {
   position: absolute;
   top: 0;
   left: 0;
   width: 40px;
   height: 40px;
   bottom: 0;
   right: 0;
   margin: auto;
   color: #fff;
   font-size: 35px;
   text-align: center;
   z-index: 1;
   cursor: pointer;
   }
   .ProfileUploadPic img {
      width: 100%;
      height: 100%;
      opacity: 0.7;
   }
   .ProfileUploadPic input {
       position: absolute;
    /* top: 0; */
    /* left: 0; */
    width: 100%;
    height: 100%;
    /* border-radius: 50%; */
    opacity: 0;
    cursor: pointer;
    z-index: 1;
   }
   .showinfoarea.mypaneleditfsa .paneleityodas {
   width: auto;
   top: 5px;
   right: 5px;
   padding: 5px 10px;
   background-color: #fff;
   }
   .workexperience .basetst {
   width: 25%;
   }
   .basetst img {
   width: 25px;
   height: 25px;
   }
   .modal .filldetails input.form-control,
   .modal .profileformstop .filldetails select {
   width: 100%;
   height: 45px !important;
   margin: 0 0 15px 0;
   }
   .modal .srchbtns {
   width: 150px;
   margin: 20px auto 0;
   }
   ul.multiselect-container.dropdown-menu {
   min-width: 300px;
   right: 0;
   left: auto !important;
   height: 350px;
   overflow: auto;
   z-index: 9;
   }
   ul.multiselect-container.dropdown-menu li .input-group {
   margin: 0;
   padding: 5px;
   }
   ul.multiselect-container.dropdown-menu li .input-group span {
   display: none;
   }
   ul.multiselect-container.dropdown-menu li .input-group input {
   width: 100%;
   flex: inherit;
   }
   ul.multiselect-container.dropdown-menu li {}
   ul.multiselect-container.dropdown-menu li a {
   display: block;
   color: #000;
   }
   ul.multiselect-container.dropdown-menu li a:focus {
   box-shadow: none;
   border: none;
   outline: none;
   }
   ul.multiselect-container.dropdown-menu li a:hover {
   text-decoration: none;
   }
   ul.multiselect-container.dropdown-menu li a label {
   height: auto;
   padding: 10px 15px;
   display: block;
   outline: 0;
   font-family: Roboto;
   font-size: 14px;
   font-weight: 400;
   }
   ul.multiselect-container.dropdown-menu li a label input {
   margin: 0 10px 0 0
   }
   .InfomationBox .editssections .addcall .multiselect.dropdown-toggle {
   height: 40px !important;
   width: 100% !important;
   border: 1px solid #ddd;
   box-shadow: none;
   font-size: 14px;
   font-family: Roboto;
   font-weight: 500;
   border-radius: 5px;
   padding: 0 17px;
   letter-spacing: 0.3px;
   }

   .labPostion .multiselect.dropdown-toggle {
    background: #fff!important;
    border: 1px solid #084d87!important;
   }

   .resumeurl {
     width: 100%;
    text-align: right;
    font-size: 12px;
    padding: 5px 0;
   }
   .viewiconsd {
   width: 120px !important;
   }
   .personalTabs li{
   display: block;
   width: 100%;
   float: left;
   margin-bottom: 5px;
   }
   .personalTabs li a{
   padding: 5px 10px;
   display: block;
   border-radius: 4px;
   background: #cccccc8f;
   }
   .personalTabs li a .basetst p{
   font-size: 13px;
   }
   .personalTabs li a:hover{
   background: #00a94f;  
   }
   .personalTabs li:last-child{
   margin-bottom: 0;
   }
   .personalTabs li .basetst{
   width: 100%;
   }
   .personalTabs li a:hover{
   text-decoration: none;
   }
   .innerbglay.innerbglay22{
   width: 100%;
   }
   .personalTabs{
   border: none;
   background-color: #fff;
   padding: 25px;
   border-radius: 10px;
   margin: 0 0 30px 0;
   border: 1px solid #ddd;
   }
   .boxss{
   float: left;
   width: 100%;
   }

   .intrs .basetst{
      width: 100%!important;
      float: none!important;
   }
   .modal-header{
      padding: 0px!important;
   }
   .personalTabs .show{
      background-color: #00a94f!important;
   }

   .modal-header .close{
      z-index: 999;
   }
/*   .multiselect-selected-text{
          text-overflow: ellipsis;
    overflow: hidden;
    width: 269px;
    display: block;
    white-space: nowrap;
   }
   .dropdown-toggle::after {
    position: absolute;
    right: 12px;
    top: 18px;
}*/
.resumeupdatebutton {
       background-color: #fbaf31;
    color: #fff;
    border: 1px solid #000;
    padding: 4px 25px;
}

.addresumes.editssections.EditNone .row:last-child {
    justify-content: flex-end;
}


a.viewiconsd {

 margin-right: 20px;

}

.resumeurl {
    width: 98%;

}
.workexperience .filldetails input
{
   font-weight: 100;
    font-family: 'Open Sans', sans-serif;
    color: #717171;
    font-size: 13px;
}
p.text-danger{
       font-size: 11px;
}
</style>


<div class="managerpart">
   <div class="container-fluid">
      <div class="row">
         
         <div class="col-md-9 expanddiv profilemyyods">
            <div class="innerbglay innerbglay22">
               <div class="mainheadings-yoda">
                  <?php 
                     if(!empty($this->session->flashdata('msg'))) {
                        echo $this->session->flashdata('msg');
                     } 
                  ?>
               </div>

               <div class="row boxss">
                  <div class="col-sm-3">
                     <ul class="nav nav-tabs personalTabs">
                        <li>
                           <a href="<?php echo base_url(); ?>resume">
                              <div class="basetst">
                                 <img src="<?php echo base_url();?>webfiles/img/personal.png">
                                 <p>View Profile</p>
                              </div>
                           </a>
                        </li>
                        <li>
                           <a data-toggle="tab" href="#home" <?php if($tab_value == "home") { ?>class="active show" <?php } ?>>
                              <div class="basetst">
                                 <img src="<?php echo base_url();?>webfiles/img/personal.png">
                                 <p>Personal Information</p>
                              </div>
                           </a>
                        </li>
                        <li>
                           <a data-toggle="tab" href="#menu1" <?php if($tab_value == "menu1") { ?>class="active show" <?php } ?>>
                              <div class="basetst">
                                 <img src="<?php echo base_url();?>webfiles/img/personal.png">
                                 <p>Professional Details</p>
                              </div>
                           </a>
                        </li>
                        <li>
                           <a data-toggle="tab" href="#menu2" <?php if($tab_value == "menu2") { ?>class="active show" <?php } ?>>
                              <div class="basetst">
                                 <img src="<?php echo base_url();?>webfiles/img/personal.png">
                                 <p>Other Details</p>
                              </div>
                           </a>
                        </li>
                        <li>
                           <a data-toggle="tab" href="#menu3" <?php if($tab_value == "menu3") { ?>class="active show" <?php } ?>>
                              <div class="basetst">
                                 <img src="<?php echo base_url().'webfiles/';?>img/resume.png">
                                 <p>Attach Resume</p>
                              </div>
                           </a>
                        </li>
                        <li>
                           <a data-toggle="tab" href="#menu4" <?php if($tab_value == "menu4") { ?>class="active show" <?php } ?>>
                              <div class="basetst">
                                 <img src="<?php echo base_url().'webfiles/';?>img/support.png">
                                 <p>Top Client Supported</p>
                              </div>
                           </a>
                        </li>
                        <li>
                           <a data-toggle="tab" href="#menu5" <?php if($tab_value == "menu5") { ?>class="active show" <?php } ?>>
                              <div class="basetst">
                                 <img src="<?php echo base_url().'webfiles/';?>img/Languages.png">
                                 <p>Languages Spoken</p>
                              </div>
                           </a>
                        </li>
                        <!-- <li>
                           <a data-toggle="tab" href="#menu6" <?php //if($tab_value == "menu6") { ?>class="active show" <?php //} ?>>
                              <div class="basetst">
                                 <img src="<?php //echo base_url().'webfiles/';?>img/skills.png">
                                 <p>Add Skills</p>
                              </div>
                           </a>
                        </li> -->
                        <li>
                           <a data-toggle="tab" href="#menu7" <?php if($tab_value == "menu7") { ?>class="active show" <?php } ?>>
                              <div class="basetst">
                                 <img src="<?php echo base_url().'webfiles/';?>img/Jobs.png">
                                 <p>Work Experience</p>
                              </div>
                           </a>
                        </li>
                        <li>
                           <a data-toggle="tab" href="#menu8">
                              <div class="basetst">
                                 <img src="<?php echo base_url().'webfiles/';?>img/education.png" <?php if($tab_value == "menu8") { ?>class="active show" <?php } ?>>
                                 <p>Education</p>
                              </div>
                           </a>
                        </li>
                        <!-- <li>
                           <a data-toggle="tab" href="#menu9" <?php //if($tab_value == "menu9") { ?>class="active show" <?php //} ?>>
                              <div class="basetst">
                                 <img src="<?php //echo base_url().'webfiles/';?>img/Jobs.png">
                                 <p>Add Jobs I'm Interested in</p>
                              </div>
                           </a>
                        </li> -->
                        <!-- <li>
                           <a data-toggle="tab" href="#menu10" <?php //if($tab_value == "menu10") { ?>class="active show" <?php //} ?>>
                              <div class="basetst">
                                 <img src="<?php //echo base_url().'webfiles/';?>img/Jobs.png">
                                 <p>Self Assesment</p>
                              </div>
                           </a>
                        </li>
                        <li>
                           <a data-toggle="tab" href="#menu11" <?php //if($tab_value == "menu11") { ?>class="active show" <?php //} ?>>
                              <div class="basetst">
                                 <img src="<?php //echo base_url().'webfiles/';?>img/personal.png">
                                 <p>More Details</p>
                              </div>
                           </a>
                        </li> -->
                     </ul>
                  </div>


                  <div class="col-sm-9">
                     <div class="tab-content">
                        
                        <div id="home" class="tab-pane fade <?php if($tab_value == "home") { echo 'in show active'; } ?>">
                           <div class="InfomationBox">
                              <div class="addresumes editssections">
                                 <div class="addresumeare">
                                    <div class="basetst">
                                       <img src="<?php echo base_url();?>webfiles/img/personal.png">
                                       <p>Personal Information</p>
                                    </div>
                                 </div>
                              </div>
                              <div class="basicdetailsect editssections EditNone">
                                 <form method="post" action="<?php echo base_url();?>user/user_profile_update_one" enctype="multipart/form-data">
                                    <input type="hidden" name="token" value="<?php if($profileDetail['token']){echo $profileDetail['token'];}?>">
                                     <div class="row">
                                        <div class="col-sm-9">
                                           <div class="profileformstop">
                                              <div class="filldetails" style=" margin: 0 0 0;">
                                                 <div class="row">
                                                    <div class="form-group col-sm-6 labPostion">
                                                        <label class="labAllnew">Name</label>
                                                       <input type="text" class="form-control" name="name" value="<?php echo $profileDetail['name'];?>" placeholder="Name">
                                                    </div>

                                                    <div class="form-group col-sm-6 labPostion">
                                                        <label class="labAllnew">Date Of Birth</label>
                                                       <input type="text" id="proid" class="form-control" name="dob" value="<?php if($profileDetail['dob']){echo $profileDetail['dob'];}?>" placeholder="Date Of Birth" autocomplete="off">
                                                    </div>
                                                    
                                                    <div class="form-group col-sm-6 labPostion">
                                                          <label class="labAllnew">Select Phone Code</label>
                                                       <select name="country_code" placeholder=" Select Phone Code" class="">
                                                          <option value="">Select Phone Code</option>
                                                          <?php
                                                             foreach($phonecodes as $phonecode) {
                                                             ?>
                                                          <option <?php if($phonecode['phonecode']==$profileDetail['country_code']){ echo "selected"; }?> value="+<?php echo $phonecode['phonecode'];?>"> <?php echo $phonecode['name'];?> - <?php echo $phonecode['phonecode'];?> </option>
                                                          <?php
                                                             }
                                                             ?>
                                                       </select>
                                                    </div>
                                                    <div class="form-group col-sm-6 labPostion">
                                                        <label class="labAllnew">Contact Number</label>
                                                       <input type="text" class="form-control" value="<?php echo $profileDetail['phone'];?>" name="phone" maxlength="10" id="phone12" placeholder="Contact Number">
                                                    </div>
                                                    <div class="form-group col-sm-6 labPostion">
                                                       <label class="labAllnew">Email</label>
                                                       <input type="text" class="form-control" value="<?php echo $usersess['email'];?>" name="email" placeholder="Email">

                                                       <?php if(isset($updateerrors['email'])){echo "<p class='text-danger'>".$updateerrors['email']."</p>"; } ?>
                                                    </div>

                                                    <div class="form-group col-sm-6 labPostion">
                                                          <label class="labAllnew">Address</label>
                                                       <input type="text" class="form-control" value="<?php if($profileDetail['location']){echo $profileDetail['location'];}?>" name="location" placeholder="Address" id="txtPlaces">
                                                    </div>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>
                                        <div class="col-sm-3 ">
                                           <div class="ProfileUploadPic">
                                              <?php
                                                 if(empty($profileDetail['profilePic'])){
                                              ?>
                                                   <span><i class="fas fa-camera"></i></span>
                                              <?php
                                                 }
                                              ?>
                                              
                                              <input type="file" name="profilePic" id="imgInp">
                                              <?php
                                                 if(!empty($profileDetail['profilePic'])){
                                              ?>
                                                   <img id="blah" src="<?php echo $profileDetail['profilePic'];?>">
                                              <?php
                                                 } else {
                                              ?>
                                                   <img id="blah" src="<?php echo base_url().'webfiles/';?>img/profilepic.png">
                                              <?php
                                                 }
                                              ?>
                                              <p>Upload Photo</p>
                                           </div>
                                        </div>
                                     </div>
                                     
                                     <div class="profileformstop ">
                                        <div class="filldetails" style=" margin: 0 0 0;">
                                           <div class="row">
                                              <div class="form-group col-sm-4 labPostion">
                                                    <label class="labAllnew">Years of Experience</label>
                                                 <select name="exp_year" placeholder="Years of Experience" class="">
                                                    <option value="">Year</option>
                                                    <?php for($i=0;$i<=20;$i++) {?>
                                                    <option value="<?php echo $i;?>" <?php if($profileDetail['exp_year'])
                                                       {
                                                       if($profileDetail['exp_year']==$i)
                                                       {
                                                          echo "selected";
                                                       }
                                                       }
                                                       ?>><?php echo $i.' '.'years';?>
                                                    </option>
                                                    <?php }?>
                                                 </select>
                                              </div>
                                              <div class="form-group col-sm-4 labPostion">
                                                    <label class="labAllnew">Months of Experience</label>
                                                 <select name="exp_month" placeholder="Months of Experience" class="texts">
                                                    <option value="">Month</option>
                                                    <?php for($i=0;$i<=12;$i++) {?>
                                                    <option value="<?php echo $i;?>" <?php if($profileDetail['exp_month'])
                                                       {
                                                       if($profileDetail['exp_month']==$i)
                                                       {
                                                          echo "selected";
                                                       }
                                                       }
                                                       ?>><?php echo $i.' '.'months';?></option>
                                                    <?php }?>
                                                 </select>
                                              </div>
                                              

                                              <div class="form-group col-sm-4 labPostion">
                                                   <label class="labAllnew">Highest Qualification</label>
                                                 <select name="education" id="education" placeholder="Highest Qualification">
                                                    <option value="">Select Highest Qualification</option>
                                                    <option value="High School Graduate" <?php if($profileDetail['education']){ if($profileDetail['education']=="High School Graduate"){ echo "selected"; } } ?>>High School Graduate</option>
                                                    <option value="Vocational" <?php if($profileDetail['education']){ if($profileDetail['education']=="Vocational"){ echo "selected"; } } ?>>Vocational</option>
                                                    <option value="Undergraduate" <?php if($profileDetail['education']){ if($profileDetail['education']=="Undergraduate"){ echo "selected"; } } ?>>Undergraduate</option>
                                                    <option value="Associate Degree" <?php if($profileDetail['education']){ if($profileDetail['education']=="Associate Degree"){ echo "selected"; } } ?>>Associate Degree</option>
                                                    <option value="College Graduate" <?php if($profileDetail['education']){ if($profileDetail['education']=="College Graduate"){ echo "selected"; } } ?>>College Graduate</option>
                                                    <option value="Post Graduate" <?php if($profileDetail['education']){ if($profileDetail['education']=="Post Graduate"){ echo "selected"; } } ?>>Post Graduate</option>
                                                 </select>
                                              </div>
                                              
                                              <!-- <div class="form-group col-sm-4 labPostion">
                                                   <label class="labAllnew">Nationality</label>
                                                 <select name="nationality" placeholder="Nationality" class="texts">
                                                    <option value="">Select Nationality</option>
                                                    <?php
                                                       //foreach($nations as $nation) {
                                                       ?>
                                                    <option <?php //if(!empty($profileDetail['nationality'])){ if($profileDetail['nationality']==$nation['nationality']) { echo "selected"; } } ?> value="<?php //echo $nation['nationality'];?>"> <?php //echo $nation['nationality'];?> </option>
                                                    <?php
                                                       //}
                                                      ?>
                                                 </select>
                                              </div> -->

                                              <!-- <div class="form-group col-sm-4 labPostion">
                                                    <label class="labAllnew">Designation</label>
                                                 <input type="text" class="form-control" value="<?php //if($profileDetail['designation']){echo $profileDetail['designation'];}?>" name="designation" placeholder="Designation">
                                              </div> -->

                                              <div class="form-group col-sm-4 labPostion">
                                                
                                                <label class="labAllnew">State</label>
                                                <input type="text" class="form-control" name="state" placeholder="State" value="<?php echo $profileDetail['state'];?>" style="padding: 8px 15px;">

                                              </div>

                                              <div class="form-group col-sm-4 labPostion">
                                                <label class="labAllnew">City/Town</label>
                                                <input type="text" class="form-control" name="city" placeholder="City/Town" value="<?php echo $profileDetail['city'];?>" style="padding: 8px 15px;">

                                              </div>
                                              <!-- <div class="form-group col-sm-4 labPostion">
                                                   <label class="labAllnew">Select Best time to call</label>
                                                 <select name="timeforcall" class="form-control">
                                                    <option value="">Select Best time to call</option>
                                                    <option value="10 AM-1 PM" <?php //if($profileDetail['timeforcall'] == "10 AM-1 PM") {  echo "selected"; } ?>> 10 AM - 1 PM </option>
                                                    <option value="1 PM-4 PM" <?php //if($profileDetail['timeforcall'] == "1 PM-4 PM") {  echo "selected"; } ?>> 1 PM - 4 PM </option>
                                                    <option value="4 PM-7 PM" <?php //if($profileDetail['timeforcall'] == "4 PM-7 PM") {  echo "selected"; } ?>> 4 PM - 7 PM </option>
                                                    <option value="7 PM-10 PM" <?php //if($profileDetail['timeforcall'] == "7 PM-10 PM") {  echo "selected"; } ?>> 7 PM - 10 PM </option>
                                                    <option value="10 PM-1 AM" <?php //if($profileDetail['timeforcall'] == "10 PM-1 AM") {  echo "selected"; } ?>> 10 PM - 1 AM </option>
                                                    <option value="1 AM-4 AM" <?php //if($profileDetail['timeforcall'] == "1 AM-4 AM") {  echo "selected"; } ?>> 1 AM - 4 AM </option>
                                                    <option value="4 AM-7 AM" <?php //if($profileDetail['timeforcall'] == "4 AM-7 AM") {  echo "selected"; } ?>> 4 AM - 7 AM </option>
                                                    <option value="7 AM-10 AM" <?php //if($profileDetail['timeforcall'] == "7 AM-10 AM") {  echo "selected"; } ?>> 7 AM - 10 AM </option>
                                                 </select>
                                              </div> -->
                                           </div>
                                        </div>
                                     </div>

                                     <button type="submit" class="srchbtns">Save</button>
                                 </form>
                              </div>
                              <div class="clear"></div>
                           </div>
                        </div>

                        <div id="menu1" class="tab-pane fade <?php if($tab_value == "menu1") { echo 'in show active'; } ?>">
                           <div class="InfomationBox">
                              <div class="addresumes editssections">
                                 <div class="addresumeare">
                                    <div class="basetst">
                                       <img src="<?php echo base_url();?>webfiles/img/personal.png">
                                       <p>Professional Details</p>
                                    </div>
                                 </div>
                              </div>

                              <div class="basicdetailsect editssections EditNone">
                                 <div class="profileformstop">
                                    <div class="filldetails" style=" margin: 0 0 0;">
                                       
                                       <form method="post" action="<?php echo base_url();?>user/user_profile_update_two">
                                          <input type="hidden" name="token" value="<?php if($profileDetail['token']){echo $profileDetail['token'];}?>">
                                          
                                          <div class="row">

                                             

                                             <input type="hidden" name="bpoyear" value="0">
                                             <input type="hidden" name="bpomonth" value="0">

                                             <div class="form-group col-sm-4 labPostion">
                                                <label class="labAllnew">Select Job Search Status</label>
                                                <select name="jobsearch_status" placeholder="Select Job Search Status">
                                                   <option value="">Select Job Search Status</option>
                                                   <option value="1" <?php if($profileDetail['jobsearch_status']){ if($profileDetail['jobsearch_status']==1){ echo "selected"; } } ?>>Actively seeking</option>
                                                   <option value="2" <?php if($profileDetail['jobsearch_status']){ if($profileDetail['jobsearch_status']==2){ echo "selected"; } } ?>>Open to offers</option>
                                                   <option value="3" <?php if($profileDetail['jobsearch_status']){ if($profileDetail['jobsearch_status']==3){ echo "selected"; } } ?>>Exploring</option>
                                                </select>
                                                <?php if(isset($updateerrors['jobsearch_status'])){echo "<p class='text-danger'>".$updateerrors['jobsearch_status']."</p>"; } ?>
                                             </div>
                                             
                                             <div class="form-group col-sm-4 labPostion">
                                                   <label class="labAllnew">Your Current/Last Company</label>
                                                <select name="topbpo" placeholder="Your Current/Last Company" class="texts lastbpo">
                                                   <option value="">Select Your Current/Last Company</option>
                                                   <?php $haveTopBPO = 1; ?>
                                                   
                                                   <?php if($haveTopBPO !=2) {  ?>
                                                      <option value="other" selected> Other </option>
                                                   <?php } else { ?>
                                                      <option value="other"> Other </option>
                                                   <?php
                                                         }
                                                   ?>
                                                   
                                                   <?php foreach($topbpos as $topbpo) { ?>
                                                      <?php
                                                         if(strlen($profileDetail['topbpo']) > 0) {
                                                               if($profileDetail['topbpo']==$topbpo['name']) {
                                                                  $haveTopBPO = 2;
                                                               }
                                                         }
                                                      ?>
                                                      <option <?php if(!empty($profileDetail['topbpo'])){ if($profileDetail['topbpo']==$topbpo['name']) { echo "selected"; $haveTopBPO = 2;} } ?> value="<?php echo $topbpo['name'];?>"> <?php echo $topbpo['name'];?> </option>
                                                   <?php } ?>
                                                   
                                                </select>

                                                <?php if(isset($updateerrors['topbpo'])){echo "<p class='text-danger'>".$updateerrors['topbpo']."</p>"; } ?>
                                             </div>

                                             <div class="form-group col-sm-4 labPostion lastbpoother" <?php if($haveTopBPO !=2) { } else { ?> style="display:none" <?php } ?>>
                                                <label class="labAllnew">Your Current/Last Company Other</label>
                                                <input type="text" name="topbpoother" id="topbpoother" class="form-control" value="<?php echo $profileDetail['topbpo'];?>">
                                             </div>

                                             <div class="form-group col-sm-4 labPostion">
                                                   <label class="labAllnew">Job Level</label>
                                                <select name="joblevel" placeholder="oo" class="">
                                                   <option value="">Select your Job Level</option>
                                                   
                                                   <?php foreach($levellist as $levellistt) { ?>
                                                      <option <?php if(!empty($profileDetail['joblevel'])){ if($profileDetail['joblevel']==$levellistt['level']) { echo "selected"; } } ?> value="<?php echo $levellistt['level'];?>"> <?php echo $levellistt['level'];?> </option>
                                                   <?php } ?>
                                                </select>
                                             </div>

                                             <div class="form-group col-sm-4 labPostion">
                                                   <label class="labAllnew">Industry</label>
                                                <select name="industry" placeholder="oo" class="changeindustry">
                                                   <option value="">Select Industry</option>
                                                   <?php foreach($industries as $industry) { ?>
                                                      <option <?php if(!empty($profileDetail['industry'])){ if(strlen($profileDetail['specialization'])>0) { if($profileDetail['industry']==$industry['name']) { echo "selected"; } } } ?> value="<?php echo $industry['name'];?>"> <?php echo $industry['name'];?> </option>
                                                   <?php } ?>
                                                </select>

                                                <?php if(isset($updateerrors['industry'])){echo "<p class='text-danger'>".$updateerrors['industry']."</p>"; } ?>
                                             </div>
                                             
                                             <div class="form-group col-sm-4 labPostion">
                                                   <label class="labAllnew">Select Your Specialization</label>
                                                
                                                <select name="specialization" class="form-control inputAll changecat">

                                                   <option value="">Select Your Specialization (Category) </option>
                                                   
                                                   <?php //foreach($categorylist as $category) { ?>
                                                      
                                                      <!-- <option <?php //if(!empty($profileDetail['specialization'])){ if($profileDetail['specialization']==$category['category']) { echo "selected"; } } ?> value="<?php //echo $category['category']; ?>"> <?php //echo $category['category']; ?> </option> -->

                                                      <option selected value="<?php echo $profileDetail['specialization']; ?>"> <?php echo $profileDetail['specialization']; ?> </option>

                                                   <?php //} ?>
                                                </select>

                                                <?php if(isset($updateerrors['specialization'])){echo "<p class='text-danger'>".$updateerrors['specialization']."</p>"; } ?>
                                             </div>
                                             <div class="form-group col-sm-4 labPostion">
                                                   <label class="labAllnew">Select Your Sub Specialization</label>
                                                <select name="sub_specialization" class="form-control inputAll getsubcat">
                                                   <option value="">Select Your Sub Specialization (Sub category) </option>
                                                   <?php //foreach($subcategorylist1 as $subcategory) { 
                                                        
                                                        //if($subcategory['subcategory'] == $profileDetail['sub_specialization']) {
                                                   ?>
                                                            <!-- <option <?php //if(!empty($profileDetail['sub_specialization'])){ if($profileDetail['sub_specialization']==$subcategory['subcategory']) { echo "selected"; } } ?> value="<?php //echo $subcategory['subcategory']; ?>"> <?php //echo $subcategory['subcategory']; ?> </option> -->


                                                            <option selected value="<?php echo $profileDetail['sub_specialization']; ?>"> <?php echo $profileDetail['sub_specialization']; ?> </option>

                                                   <?php //} } ?>
                                                </select>

                                                <?php if(isset($updateerrors['sub_specialization'])){echo "<p class='text-danger'>".$updateerrors['sub_specialization']."</p>"; } ?>
                                             </div>


                                             <div class="form-group col-sm-12 labPostion jobinterest">

                                                <label class="labAllnew">Select Interested In</label>

                                                <select class="multiselect-ui" id="framework" name="intrested[]" multiple style="margin-left:0px;width: 100%; margin-bottom: 10px;">
                                                   
                                                   <option value="">Select Interested In</option>
                                                   <?php
                                                      foreach($intrestedin as $getintrested) {
                                                      
                                                      if($getintrested['name'] == "ALL" || $getintrested['name'] == 'all') {} else {
                                                   ?>
                                                         <option <?php if(!empty($profileDetail['jobsInterested'])) { if(in_array($getintrested['name'], $profileDetail['jobsInterested'])) { echo "selected"; } } ?> value="<?php echo $getintrested['name'];?>"> <?php echo $getintrested['name'];?> </option>
                                                   <?php
                                                      }
                                                      }
                                                   ?>
                                                </select>

                                             </div>


                                             <div class="form-group col-sm-12 labPostion jobbenefits">

                                                <label class="labAllnew">Select Job Benefits</label>

                                                <select class="multiselect-ui" id="frameworkone" name="benefits[]" multiple style="margin-left:0px;width: 100%; margin-bottom: 10px;">
                                                   
                                                   <option value="">Select Job Benefits</option>
                                                   <?php
                                                      foreach($jobbenefits as $jobbenefit) {
                                                      
                                                   ?>
                                                         <option <?php if(!empty($profileDetail['jobsbenefits'])) { if(in_array($jobbenefit['name'], $profileDetail['jobsbenefits'])) { echo "selected"; } } ?> value="<?php echo $jobbenefit['name'];?>"> <?php echo $jobbenefit['name'];?> </option>
                                                   <?php
                                                   
                                                      }
                                                   ?>
                                                </select>

                                             </div>
                                          
                                          </div>

                                          <button type="submit" class="srchbtns">Save</button>
                                       </form>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                           </div>
                        </div>

                        <div id="menu2" class="tab-pane fade <?php if($tab_value == "menu2") { echo 'in show active'; } ?>">
                           <div class="InfomationBox">
                              <div class="addresumes editssections">
                                 <div class="addresumeare">
                                    <div class="basetst">
                                       <img src="<?php echo base_url();?>webfiles/img/personal.png">
                                       <p>Other Details</p>
                                    </div>
                                 </div>
                              </div>
                              <div class="basicdetailsect editssections EditNone">
                                 <div class="profileformstop">
                                    <div class="filldetails" style=" margin: 0 0 0;">
                                       <form method="post" action="<?php echo base_url();?>user/user_profile_update_three">
                                          <input type="hidden" name="token" value="<?php if($profileDetail['token']){echo $profileDetail['token'];}?>">
                                       
                                          <div class="row">
                                             
                                             <!-- <div class="form-group col-sm-4 labPostion"> 
                                                       <label class="labAllnew">Internet Connection</label>
                                                <select name="haveinternet" placeholder="Have Internet" class="">
                                                   <option value="">Select Internet Connection</option>
                                                   <option value="Yes" <?php //if($profileDetail['haveinternet'] == "Yes") {  echo "selected"; } ?>> Yes </option>
                                                   <option value="No" <?php //if($profileDetail['haveinternet'] == "No") {  echo "selected"; } ?>> No </option>
                                                </select>
                                             </div> -->

                                             <div class="form-group col-sm-6 labPostion">
                                                       <label class="labAllnew">Are you Work@Home Ready with High Speed Internet?</label>
                                                <select name="internetspeed" placeholder="Are you Work@Home Ready with High Speed Internet?" class="texts">
                                                   <option value="">Select </option>
                                                   
                                                   <option value="Yes" <?php if($profileDetail['internetspeed'] == "Yes") {  echo "selected"; } ?>> Yes </option>
                                                   <option value="No" <?php if($profileDetail['internetspeed'] == "No") {  echo "selected"; } ?>> No </option>
                                                   
                                                </select>
                                             </div>

                                             <div class="form-group col-sm-3 labPostion">
                                                       <label class="labAllnew">Enter current salary</label>
                                                <input type="text" class="form-control" name="current_salary" value="<?php echo $profileDetail['current_salary'];?>" placeholder="Enter current salary">
                                             </div>
                                             
                                             <div class="form-group col-sm-3 labPostion">
                                                 <label class="labAllnew">Select Best time to call </label>
                                                <select name="timeforcall" class="form-control inputAll" placeholder ="Select Best time to call">
                                                   <option value="" class="demo">Select Best time to call</option>

                                                   <option value="9 AM-12 PM" <?php if(!empty($profileDetail['timeforcall'])){ if($profileDetail['timeforcall']=="9 AM-12 PM") { echo "selected"; } } ?>> 9 AM - 12 PM </option>
                                                   <option value="12 PM-3 PM" <?php if(!empty($profileDetail['timeforcall'])){ if($profileDetail['timeforcall']=="12 PM-3 PM") { echo "selected"; } } ?>> 12 PM - 3 PM </option>
                                                   <option value="3 PM-6 PM" <?php if(!empty($profileDetail['timeforcall'])){ if($profileDetail['timeforcall']=="3 PM-6 PM") { echo "selected"; } } ?>> 3 PM - 6 PM </option>
                                                   <option value="6 PM-9 PM" <?php if(!empty($profileDetail['timeforcall'])){ if($profileDetail['timeforcall']=="6 PM-9 PM") { echo "selected"; } } ?>> 6 PM - 9 PM </option>
                                                   <option value="9 PM-12 AM" <?php if(!empty($profileDetail['timeforcall'])){ if($profileDetail['timeforcall']=="9 PM-12 AM") { echo "selected"; } } ?>> 9 PM - 12 AM </option>
                                                   <option value="12 AM-3 AM" <?php if(!empty($profileDetail['timeforcall'])){ if($profileDetail['timeforcall']=="12 AM-3 AM") { echo "selected"; } } ?>> 12 AM - 3 AM </option>
                                                   <option value="3 AM-6 AM" <?php if(!empty($profileDetail['timeforcall'])){ if($profileDetail['timeforcall']=="3 AM-6 AM") { echo "selected"; } } ?>> 3 AM - 6 AM </option>
                                                   <option value="6 AM-9 AM" <?php if(!empty($profileDetail['timeforcall'])){ if($profileDetail['timeforcall']=="6 AM-9 AM") { echo "selected"; } } ?>> 6 AM - 9 AM </option>
                                               </select>
                                             </div>
                                          </div>

                                          <div class="row">
                                             <div class="form-group col-sm-6 labPostion">
                                                <label class="labAllnew">What is your preferred work mode?</label>
                                                <select name="work_mode" placeholder="What is your preferred work mode?" class="texts">
                                                   <option value="">Select </option>
                                                   
                                                   <option value="Onsite" <?php if($profileDetail['work_mode'] == "Onsite") {  echo "selected"; } ?>> Onsite </option>
                                                   <option value="Work At Home" <?php if($profileDetail['work_mode'] == "Work At Home") {  echo "selected"; } ?>> Work At Home </option>
                                                   <option value="Either as long as I get a job" <?php if($profileDetail['work_mode'] == "Either as long as I get a job") {  echo "selected"; } ?>> Either as long as I get a job </option>
                                                   
                                                </select>
                                             </div>

                                             <div class="form-group col-sm-6 labPostion">
                                                <label class="labAllnew">Vaccination Status?</label>
                                                <select name="vaccination" placeholder="Vaccination Status?" class="texts">
                                                   <option value="">Select </option>
                                                   
                                                   <option value="Not Vaccinated" <?php if($profileDetail['vaccination'] == "Not Vaccinated") {  echo "selected"; } ?>> Not Vaccinated </option>

                                                   <option value="1st dose complete" <?php if($profileDetail['vaccination'] == "1st dose complete") {  echo "selected"; } ?>> 1st dose completed </option>
                                                   
                                                   <option value="Fully Vaccinated" <?php if($profileDetail['vaccination'] == "Fully Vaccinated") {  echo "selected"; } ?>> Fully Vaccinated </option>

                                                   
                                                   
                                                </select>
                                             </div>

                                          </div>

                                          <div class="row">
                                             <div class="form-group col-sm-6 labPostion">
                                                <label class="labAllnew">Are you willing to relocate?</label>
                                                <select name="relocate" placeholder="Are you willing to relocate?" class="texts">
                                                   <option value="">Select </option>
                                                   
                                                   <option value="Yes" <?php if($profileDetail['relocate'] == "Yes") {  echo "selected"; } ?>> Yes </option>
                                                   
                                                   <option value="No" <?php if($profileDetail['relocate'] == "No") {  echo "selected"; } ?>> No </option>
                                                   
                                                </select>
                                             </div>
                                          </div>
                                          
                                          <button type="submit" class="srchbtns">Save</button>

                                       </form>

                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                           </div>
                        </div>

                        <div id="menu3" class="tab-pane fade <?php if($tab_value == "menu3") { echo 'in show active'; } ?>">
                           <div class="InfomationBox">
                              <div class="addresumes editssections EditNone">
                                 <?php if($this->uri->segment(3)) { ?>
                                 <div class="text-center text-danger" style="margin-top: 20px;margin-bottom: 20px">
                                    <?php echo $resumemsg = urldecode($this->uri->segment(3)); ?>   
                                 </div>
                                 <?php } ?>
                                 <div class="addresumeare">
                                    
                                    <div class="basetst">
                                       <img src="<?php echo base_url().'webfiles/';?>img/resume.png">
                                       <p>Attach Resume</p>
                                    </div>
                                    <div class="addcall">
                                       <!-- <img src="<?php //echo base_url().'webfiles/';?>img/plusadd.png" title="Add Resume"> -->

                                       <form method="post" action="<?php echo base_url();?>user/resumeUpload" enctype="multipart/form-data">
                                          <input type="file" name="resumeFile" data-validation="required" >
                                          <input type="hidden" value="<?php echo current_url(); ?>" name="currentURL">
                                          <button type="submit" class="updt resumeupdatebutton" id="resumebtn">Update</button>
                                       </form>

                                    </div>
                                 </div>

                                    <div class="row">
                                       <?php if(!empty($checkResume[0]['resume'])){ ?>
                                       <span class="resumeurl"><?php echo $checkResume[0]['resume']?></span>
                                       <?php } ?>
                                    </div>

                                 <div class="row"> 
                                    <?php if(!empty($checkResume[0]['resume'])){ ?>
                                       <a target="blank" class="viewiconsd" href="<?php echo $checkResume[0]['resume']?>">View Resume</a>
                                       <a class="viewiconsd" href="<?php echo base_url();?>resume_delete">Delete Resume</a>
                                    <?php } ?> 
                                 </div>
                              </div>
                              <div class="clear"></div>
                           </div>
                        </div>

                        <div id="menu4" class="tab-pane fade <?php if($tab_value == "menu4") { echo 'in show active'; } ?>">
                           <span id="error_msg"></span>
                           <div class="InfomationBox">
                              <div class="addresumes editssections EditNone">
                                 <div class="addresumeare">
                                    <div class="basetst">
                                       <img src="<?php echo base_url().'webfiles/';?>img/support.png">
                                       <p>Top Client Supported</p>
                                    </div>
                                    <div class="addcall" data-toggle="modal" data-target="#exampleModalCenter818" title="Top Client Supported">
                                       <img src="<?php echo base_url().'webfiles/';?>img/plusadd.png">
                                    </div>
                                 </div>
                                 <?php
                                 if(!empty(array_filter($clientData))) {
                                    $xx=0;
                                    foreach($clientData as $client) {
                                 ?>
                                 <div class="showinfoarea">
                                    <div class="paneleityodas">
                                       <p class="designation" id="<?php echo $xx;?>"><?php echo $client['clients']; ?></p>
                                       <p>
                                          <a href="javascript:void(0)" class="fa fa-edit" data-toggle="modal" data-target="#clientmodel<?php echo $xx;?>"></a> |
                                          <a href="javascript:void(0)" class="fa fa-trash" onclick="return deleteRecord('<?php echo $client['id'] ?>','user_topclients')"></a>
                                       </p>
                                    </div>
                                    <div class="clear"></div>
                                 </div>
                                 <div class="modal fade" id="clientmodel<?php echo $xx;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                             <span aria-hidden="true">×</span>
                                             </button>
                                          </div>
                                          <div class="modal-body">
                                             <div class="formmidaress modpassfull">
                                                <div class="filldetails">
                                                   <p>Top Client Supported</p>
                                                   <span id="respclient<?php echo $xx;?>"></span>
                                                   
                                                   <input type="text" name="clients" id="cli<?php echo $xx;?>" value="<?php echo $client['clients']; ?>" id="clients" class="form-control" placeholder="Top Client">
                                                   <input type="hidden" id="clients<?php echo $xx;?>" value="<?php echo $client['id'] ?>">
                                                   <div class="statsusdd">
                                                      <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                                                      <button type="button" class="updt" id="clientbtnn" onclick="getclientval('<?php echo $xx;?>')">Update</button>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <?php $xx++; } } ?>
                              </div>
                              <div class="clear"></div>
                           </div>
                        </div>

                        <div id="menu5" class="tab-pane fade <?php if($tab_value == "menu5") { echo 'in show active'; } ?>">
                           <div class="InfomationBox">
                              <div class="addresumes editssections EditNone">
                                 <div class="addresumeare">
                                    <div class="basetst">
                                       <img src="<?php echo base_url().'webfiles/';?>img/resume.png">
                                       <p>Languages Spoken</p>
                                    </div>
                                    <div class="addcall" data-toggle="modal" data-target="#exampleModalCenter819">
                                       <img src="<?php echo base_url().'webfiles/';?>img/plusadd.png" title="Add Resume">
                                    </div>
                                 </div>   
                                    
                                <?php  
                                 if(!empty(array_filter($languageData))) {
                                     $yy=0;
                                     foreach($languageData as $languageDataS) {
                                 ?>
                                    <div class="showinfoarea">
                                       <div class="paneleityodas">
                                          <p class="designation" id="<?php echo $yy;?>"><?php echo $languageDataS['name']; ?></p>
                                          <p>
                                             <a href="javascript:void(0)" class="fa fa-edit" data-toggle="modal" data-target="#langmodel<?php echo $yy;?>"></a> | 
                                             <a href="javascript:void(0)" class="fa fa-trash" onclick="return deleteRecord('<?php echo $languageDataS['id'] ?>','user_language')"></a>
                                          </p>
                                       </div>
                                       <div class="clear"></div>
                                    </div>

                                    <div class="modal fade" id="langmodel<?php echo $yy;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                       <div class="modal-dialog modal-dialog-centered" role="document">
                                          <div class="modal-content">
                                             <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                                </button>
                                             </div>
                                             <div class="modal-body">
                                                <div class="formmidaress modpassfull">
                                                   <div class="filldetails">
                                                      <p>Languages Spoken</p>
                                                      <span id="resplang<?php echo $yy;?>"></span>
                                                      <select name="languages" id="languagees<?php echo $yy;?>" class="form-control" placeholder="Select Language" style="width:100%">
                                                         <option value="">Select Language</option>
                                                         <?php
                                                         if(!empty($languagelists)) {
                                                            foreach($languagelists as $languagelist) {
                                                         ?>
                                                            <option value="<?php echo $languagelist['id']?>" 
                                                            <?php if($languageDataS['lang_id']==$languagelist['id']){echo "selected";}?>
                                                            ><?php echo $languagelist['name']?></option>
                                                         <?php     
                                                            }}
                                                         ?>
                                                      </select>
                                                      <input type="hidden" id="languageDataS<?php echo $yy;?>" value="<?php echo $languageDataS['id'] ?>">
                                                      <div class="form-group"></div>
                                                      <div class="statsusdd">
                                                         <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                                                         <button type="button" class="updt" id="clientbtnnnn" onclick="getlangval('<?php echo $yy;?>')">Update</button>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 <?php
                                     }
                                 }
                                 ?>
                                 
                              </div>
                              <div class="clear"></div>
                           </div>
                        </div>

                        <div id="menu6" class="tab-pane fade <?php if($tab_value == "menu6") { echo 'in show active'; } ?>">
                           <div class="InfomationBox">
                              <div class="addresumes editssections EditNone">
                                 <div class="addresumeare">
                                    <div class="basetst">
                                       <img src="<?php echo base_url().'webfiles/';?>img/resume.png">
                                       <p>Add Skills</p>
                                    </div>
                                    <div class="addcall" data-toggle="modal" data-target="#exampleModalCenter820">
                                       <img src="<?php echo base_url().'webfiles/';?>img/plusadd.png" title="Add Resume">
                                    </div>
                                 </div>
                                 <?php
                                 if(!empty(array_filter($skillData))) {
                                    $zz=0;
                                    foreach($skillData as $skillDataS) {
                                 ?>
                                       <div class="showinfoarea">
                                          <div class="paneleityodas">
                                             <p class="designation" id="<?php if(!empty($yy)){ echo $yy; } ?>"><?php echo $skillDataS['skills']; ?></p>
                                             <p>
                                                <a href="javascript:void(0)" class="fa fa-edit" data-toggle="modal" data-target="#skillmodel<?php echo $zz;?>"></a> | 
                                                <a href="javascript:void(0)" class="fa fa-trash" onclick="return deleteRecord('<?php echo $skillDataS['id'] ?>','user_skills')"></a>
                                             </p>
                                          </div>
                                          <div class="clear"></div>
                                       </div>

                                        <div class="modal fade" id="skillmodel<?php echo $zz;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                               <div class="modal-dialog modal-dialog-centered" role="document">
                                                  <div class="modal-content">
                                                     <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                        </button>
                                                     </div>
                                                     <div class="modal-body">
                                                        <div class="formmidaress modpassfull">
                                                           <div class="filldetails">
                                                              <p>Skills</p>
                                                              <span id="respskill<?php echo $zz;?>"></span>
                                                              <input type="text" id="skill<?php  echo $zz;?>" value="<?php echo $skillDataS['skills']; ?>" class="form-control">
                                                              <input type="hidden" id="skillDataS<?php echo $zz;?>" value="<?php echo $skillDataS['id'] ?>">
                                                              <div class="form-group"></div>
                                                              <div class="statsusdd">
                                                                 <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                                                                 <button type="button" class="updt" id="clientbtnnnn" onclick="getskilval('<?php echo $zz;?>')">Update</button>
                                                              </div>
                                                           </div>
                                                        </div>
                                                     </div>
                                                  </div>
                                               </div>
                                        </div>
                                 <?php
                                     }
                                 }
                                 ?>
                              </div>
                              <div class="clear"></div>
                           </div>
                        </div>

                        <div id="menu7" class="tab-pane fade <?php if($tab_value == "menu7") { echo 'in show active'; } ?>">
                           <div class="InfomationBox">
                              <div class="addresumes editssections EditNone">
                                 <div class="addresumeare">
                                    <div class="basetst">
                                       <img src="<?php echo base_url().'webfiles/';?>img/resume.png">
                                       <p>Work Experience</p>
                                    </div>
                                    <div class="addcall" data-toggle="modal" data-target="#exampleModalCenter3">
                                       <img src="<?php echo base_url().'webfiles/';?>img/plusadd.png" title="Add Resume">
                                    </div>
                                 </div>

                                 <?php
                                   if(!empty(array_filter($workData))) { 
                                   foreach($workData as $work) {
                                 ?> 
                                 <div class="showinfoarea mypaneleditfsa">
                                   <p class="designation"> <b>Job Title:</b> <?php echo $work['title']; ?></p>
                                   <p class="organsname"> <b>Company Name:</b> <?php echo $work['company']; ?></p>
                                   <p class="organsname"> <b>Job Level:</b> <?php echo $work['level']; ?></p>
                                   <p class="organsname"> <b>Job Category:</b> <?php echo $work['category']; ?></p>
                                   <p class="organsname"> <b>Job Sub-Category:</b> <?php echo $work['subcategory']; ?></p>
                                   <p class="datedares"><?php echo $work['from']; ?> - <?php if($work['to']=='') { echo "Present"; }else{ echo $work['to'];} ?></p>
                                   <p class="headlineentry">
                                      <?php echo $work['desc']; ?>
                                   </p>
                                   <div class="paneleityodas">
                                      <p>
                                         <a href="javascript:void(0)" class="fa fa-edit" onclick="return gatval('<?php echo $work['id'] ?>')" data-toggle="modal" data-target="#mymodell"></a> | 
                                         <a href="javascript:void(0)" class="fa fa-trash" onclick="return deleteRecord('<?php echo $work['id'] ?>','user_work_experience')"></a>
                                      </p>
                                   </div>
                                 </div>
                                 <?php
                                   }
                                   }
                                 ?>
                              </div>
                              <div class="clear"></div>
                           </div>
                        </div>

                        <div id="menu8" class="tab-pane fade <?php if($tab_value == "menu8") { echo 'in show active'; } ?>">
                           <div class="InfomationBox">
                              <div class="addresumes editssections EditNone">
                                 <div class="addresumeare">
                                    <div class="basetst">
                                       <img src="<?php echo base_url().'webfiles/';?>img/resume.png">
                                       <p>Education</p>
                                    </div>
                                    <div class="addcall" data-toggle="modal" data-target="#exampleModalCenter2">
                                       <img src="<?php echo base_url().'webfiles/';?>img/plusadd.png" title="Add Resume">
                                    </div>
                                 </div>

                                 <!-- <div class="row"> -->
                                       <?php
                                       if(!empty(array_filter($eduData))) {
                                          foreach($eduData as $edu) {
                                       ?>
                                       <!-- <div class="col-sm-4"> -->
                                          <div class="showinfoarea mypaneleditfsa">
                                             <p class="designation"><?php echo $edu['attainment']; ?></p>
                                             <p class="organsname"><?php echo $edu['degree']; ?></p>
                                             <p class="datedares"><?php echo $edu['from']; ?> - <?php echo $edu['to']; ?></p>
                                             <p class="headlineentry">
                                                <?php echo $edu['university']; ?>
                                             </p>
                                             <div class="paneleityodas">
                                                <p>
                                                   <a href="javascript:void(0)" class="fa fa-edit" onclick="return gatvaledu('<?php echo $edu['id'] ?>')" data-toggle="modal" data-target="#exampleModalCenter25"></a> | 
                                                   <a href="javascript:void(0)" class="fa fa-trash" onclick="return deleteRecord('<?php echo $edu['id'] ?>','user_education')"></a>
                                                </p>
                                             </div>
                                          </div>
                                       <!-- </div> -->
                                       <?php
                                          }
                                          }
                                        ?>
                                    <!-- </div> -->

                              </div>
                              <div class="clear"></div>
                           </div>
                        </div>

                        <!-- <div id="menu9" class="tab-pane fade <?php //if($tab_value == "menu9") { echo 'in show active'; } ?>">
                           <div class="InfomationBox">
                              <div class="addresumes editssections EditNone">
                                 <div class="addresumeare intrs" style="display: block;">
                                    <div class="basetst" style="float: left;">
                                       <img src="<?php //echo base_url().'webfiles/';?>img/Jobs.png">
                                       <p>Add Jobs I'm Interested in</p>
                                    </div>

                                    <form method="post" action="<?php //echo base_url();?>user/user_profile_update_four">

                                    <div class="addcall" style="margin-top: 15px;">
                                       
                                          <input type="hidden" name="token" value="<?php //if($profileDetail['token']){echo $profileDetail['token'];}?>">
                                          <select class="multiselect-ui" id="framework" name="intrested[]" multiple style="margin-left:0px;width: 100%; margin-bottom: 10px;">
                                             <option value="">Select Interested In</option>
                                             <?php
                                                //foreach($intrestedin as $getintrested) {
                                                
                                                //if($getintrested['subcategory'] == "ALL" || $getintrested['subcategory'] == 'all') {} else {
                                             ?>
                                                   <option <?php //if(!empty($profileDetail['jobsInterested'])) { if(in_array($getintrested['subcategory'], $profileDetail['jobsInterested'])) { echo "selected"; } } ?> value="<?php //echo $getintrested['subcategory'];?>"> <?php //echo $getintrested['subcategory'];?> </option>
                                             <?php
                                                //}
                                                //}
                                             ?>
                                          </select>

                                    </div>

                                    <div style="margin-top: 4%;">
                                       <div style="clear:both"></div>
                                       <button type="submit" class="srchbtns" style="width:100px!important">Save</button>
                                    </div>

                                    </form>
                                 </div>
                              </div>
                              <div class="clear"></div>
                           </div>
                        </div> -->

                        <div id="menu10" class="tab-pane fade <?php if($tab_value == "menu10") { echo 'in show active'; } ?>">
                           <div class="InfomationBox">
                              <div class="ratingsexperience workexperience addresumes editssections EditNone">
                                 <div class="basetst">
                                    <p style="font-size: 18px; margin-bottom: 20px;">Self Assesment</p>
                                 </div>
                                 <div class="showinfoarea" style="width: 100%">
                                    
                                    <form method="post" action="<?php echo base_url();?>user/user_profile_update_five">
                                       <input type="hidden" name="token" value="<?php if($profileDetail['token']){echo $profileDetail['token'];}?>">

                                       <p class="designation">Please rate your expertise on each of the competencies below :</p>
                                       <div class="ratingsrange">
                                          <p>Rating 1 - Low</p>
                                          <p>Rating 5 - High</p>
                                       </div>
                                       <div class="ratingschecks">
                                          <p class="designation">Verbal Communication Skills :</p>
                                          <div class="checkflowrt">
                                             <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" value="1" id="customRadio" name="verbal" <?php if(!empty($assessData[0]['verbal'])){if($assessData[0]['verbal'] == 1){echo "checked";;}}?>>
                                                <label class="custom-control-label" for="customRadio">1</label>
                                             </div>
                                          </div>
                                          <div class="checkflowrt">
                                             <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" value="2" id="customRadio1" name="verbal" <?php if(!empty($assessData[0]['verbal'])){if($assessData[0]['verbal'] == 2){echo "checked";;}}?>>
                                                <label class="custom-control-label" for="customRadio1">2</label>
                                             </div>
                                          </div>
                                          <div class="checkflowrt">
                                             <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" value="3" id="customRadio2" name="verbal" <?php if(!empty($assessData[0]['verbal'])){if($assessData[0]['verbal'] == 3){echo "checked";;}}?>>
                                                <label class="custom-control-label" for="customRadio2">3</label>
                                             </div>
                                          </div>
                                          <div class="checkflowrt">
                                             <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" value="4" id="customRadio3" name="verbal" <?php if(!empty($assessData[0]['verbal'])){if($assessData[0]['verbal'] == 4){echo "checked";}}?>>
                                                <label class="custom-control-label" for="customRadio3">4</label>
                                             </div>
                                          </div>
                                          <div class="checkflowrt">
                                             <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" value="5" id="customRadio4" name="verbal" <?php if(!empty($assessData[0]['verbal'])){if($assessData[0]['verbal'] == 5){echo "checked";}}?>>
                                                <label class="custom-control-label" for="customRadio4">5</label>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="ratingschecks">
                                          <p class="designation">Written Communication Skills :</p>
                                          <div class="checkflowrt">
                                             <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" value="1" id="customRadio5" name="Written" <?php if(!empty($assessData[0]['written'])){if($assessData[0]['written'] == 1){echo "checked";}}?>>
                                                <label class="custom-control-label" for="customRadio5">1</label>
                                             </div>
                                          </div>
                                          <div class="checkflowrt">
                                             <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" value="2" id="customRadio6" name="Written" <?php if(!empty($assessData[0]['written'])){if($assessData[0]['written'] == 2){echo "checked";}}?>>
                                                <label class="custom-control-label" for="customRadio6">2</label>
                                             </div>
                                          </div>
                                          <div class="checkflowrt">
                                             <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" value="3" id="customRadio7" name="Written" <?php if(!empty($assessData[0]['written'])){if($assessData[0]['written'] == 3){echo "checked";}}?>>
                                                <label class="custom-control-label" for="customRadio7">3</label>
                                             </div>
                                          </div>
                                          <div class="checkflowrt">
                                             <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" value="4" id="customRadio8" name="Written" <?php if(!empty($assessData[0]['written'])){if($assessData[0]['written'] == 4){echo "checked";}}?>>
                                                <label class="custom-control-label" for="customRadio8">4</label>
                                             </div>
                                          </div>
                                          <div class="checkflowrt">
                                             <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" value="5" id="customRadio9" name="Written" <?php if(!empty($assessData[0]['written'])){if($assessData[0]['written'] == 5){echo "checked";}}?>>
                                                <label class="custom-control-label" for="customRadio9">5</label>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="ratingschecks">
                                          <p class="designation">Listening Skills :</p>
                                          <div class="checkflowrt">
                                             <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" value="1" id="customRadio10" name="Listening" <?php if(!empty($assessData[0]['listening'])){if($assessData[0]['listening'] == 1){echo "checked";}}?>>
                                                <label class="custom-control-label" for="customRadio10">1</label>
                                             </div>
                                          </div>
                                          <div class="checkflowrt">
                                             <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" value="2" id="customRadio11" name="Listening" <?php if(!empty($assessData[0]['listening'])){if($assessData[0]['listening'] == 2){echo "checked";}}?>>
                                                <label class="custom-control-label" for="customRadio11">2</label>
                                             </div>
                                          </div>
                                          <div class="checkflowrt">
                                             <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" value="3" id="customRadio12" name="Listening" <?php if(!empty($assessData[0]['listening'])){if($assessData[0]['listening'] == 3){echo "checked";}}?>>
                                                <label class="custom-control-label" for="customRadio12">3</label>
                                             </div>
                                          </div>
                                          <div class="checkflowrt">
                                             <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" value="4" id="customRadio13" name="Listening" <?php if(!empty($assessData[0]['listening'])){if($assessData[0]['listening'] == 4){echo "checked";}}?>>
                                                <label class="custom-control-label" for="customRadio13">4</label>
                                             </div>
                                          </div>
                                          <div class="checkflowrt">
                                             <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" value="5" id="customRadio14" name="Listening" <?php if(!empty($assessData[0]['listening'])){if($assessData[0]['listening'] == 5){echo "checked";}}?>>
                                                <label class="custom-control-label" for="customRadio14">5</label>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="ratingschecks">
                                          <p class="designation">Problem Solving Skills :</p>
                                          <div class="checkflowrt">
                                             <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" value="1" id="customRadio15" name="Problem" <?php if(!empty($assessData[0]['problem'])){if($assessData[0]['problem'] == 1){echo "checked";}}?>>
                                                <label class="custom-control-label" for="customRadio15">1</label>
                                             </div>
                                          </div>
                                          <div class="checkflowrt">
                                             <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" value="2" id="customRadio16" name="Problem" <?php if(!empty($assessData[0]['problem'])){if($assessData[0]['problem'] == 2){echo "checked";}}?>>
                                                <label class="custom-control-label" for="customRadio16">2</label>
                                             </div>
                                          </div>
                                          <div class="checkflowrt">
                                             <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" value="3" id="customRadio17" name="Problem" <?php if(!empty($assessData[0]['problem'])){if($assessData[0]['problem'] == 3){echo "checked";}}?>>
                                                <label class="custom-control-label" for="customRadio17">3</label>
                                             </div>
                                          </div>
                                          <div class="checkflowrt">
                                             <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" value="4" id="customRadio18" name="Problem" <?php if(!empty($assessData[0]['problem'])){if($assessData[0]['problem'] == 4){echo "checked";}}?>>
                                                <label class="custom-control-label" for="customRadio18">4</label>
                                             </div>
                                          </div>
                                          <div class="checkflowrt">
                                             <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" value="5" id="customRadio19" name="Problem" <?php if(!empty($assessData[0]['problem'])){if($assessData[0]['problem'] == 5){echo "checked";}}?>>
                                                <label class="custom-control-label" for="customRadio19">5</label>
                                             </div>
                                          </div>
                                       </div>
                                       <?php 
                                       if(!empty($expertData[0]['expert'])) {
                                          $expert_id= explode(",",$expertData[0]['expert']); 
                                       }
                                       ?>
                                       <div class="ratingschecks fullratings" style="margin-bottom: 40px;">
                                          <p class="designation">In which industry would you consider yourself an expert?</p>
                                          <div class="setsboxsone">
                                             <div class="checkflowrt">
                                                <div class="custom-control custom-radio">
                                                   <input type="checkbox" class="custom-control-input" value="1" id="customRadio20" name="expert[]" <?php if(!empty($expert_id)){if(in_array('1',$expert_id)){echo "checked";}}?>>
                                                   <label class="custom-control-label" for="customRadio20"><img src="<?php echo base_url().'webfiles/';?>img/car.png">Automotive</label>
                                                </div>
                                             </div>
                                             <div class="checkflowrt">
                                                <div class="custom-control custom-radio">
                                                   <input type="checkbox" class="custom-control-input" value="2" id="customRadio21" name="expert[]" <?php if(!empty($expert_id)){if(in_array('2',$expert_id)){echo "checked";}}?>>
                                                   <label class="custom-control-label" for="customRadio21"><img src="<?php echo base_url().'webfiles/';?>img/sun.png">Technology</label>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="setsboxstwo">
                                             <div class="checkflowrt">
                                                <div class="custom-control custom-radio">
                                                   <input type="checkbox" class="custom-control-input" value="3" id="customRadio22" name="expert[]" <?php if(!empty($expert_id)){if(in_array('3',$expert_id)){echo "checked";}}?>>
                                                   <label class="custom-control-label" for="customRadio22"><img src="<?php echo base_url().'webfiles/';?>img/home.png">Banking and Financial Service</label>
                                                </div>
                                             </div>
                                             <div class="checkflowrt">
                                                <div class="custom-control custom-radio">
                                                   <input type="checkbox" class="custom-control-input" value="4" id="customRadio23" name="expert[]" <?php if(!empty($expert_id)){if(in_array('4',$expert_id)){echo "checked";}}?>>
                                                   <label class="custom-control-label" for="customRadio23"><img src="<?php echo base_url().'webfiles/';?>img/people.png">Media & Communications</label>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="setsboxsthree">
                                             <div class="checkflowrt">
                                                <div class="custom-control custom-radio">
                                                   <input type="checkbox" class="custom-control-input" value="5" id="customRadio24" name="expert[]" <?php if(!empty($expert_id)){if(in_array('5',$expert_id)){echo "checked";}}?>>
                                                   <label class="custom-control-label" for="customRadio24"><img src="<?php echo base_url().'webfiles/';?>img/chip.png">Consumer Electronics</label>
                                                </div>
                                             </div>
                                             <div class="checkflowrt">
                                                <div class="custom-control custom-radio">
                                                   <input type="checkbox" class="custom-control-input" value="6" id="customRadio25" name="expert[]" <?php if(!empty($expert_id)){if(in_array('6',$expert_id)){echo "checked";}}?>>
                                                   <label class="custom-control-label" for="customRadio25"><img src="<?php echo base_url().'webfiles/';?>img/cart.png">Retail & ECommerce</label>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="setsboxsfour">
                                             <div class="checkflowrt">
                                                <div class="custom-control custom-radio">
                                                   <input type="checkbox" class="custom-control-input" value="7" id="customRadio26" name="expert[]" <?php if(!empty($expert_id)){if(in_array('7',$expert_id)){echo "checked";}}?>>
                                                   <label class="custom-control-label" for="customRadio26"><img src="<?php echo base_url().'webfiles/';?>img/heartplus.png">Healthcare & Pharmaceutical</label>
                                                </div>
                                             </div>
                                             <div class="checkflowrt">
                                                <div class="custom-control custom-radio">
                                                   <input type="checkbox" class="custom-control-input" value="8" id="customRadio27" name="expert[]" <?php if(!empty($expert_id)){if(in_array('8',$expert_id)){echo "checked";}}?>>
                                                   <label class="custom-control-label" for="customRadio27"><img src="<?php echo base_url().'webfiles/';?>img/flight.png">Travel, Transportation & Tourism</label>
                                                </div>
                                             </div>
                                          </div>
                                       </div>

                                       <button type="submit" class="srchbtns">Save</button>
                                    </form>
                                 </div>
                              </div>
                              <div class="clear"></div>
                           </div>
                        </div>

                        <div id="menu11" class="tab-pane fade <?php if($tab_value == "menu11") { echo 'in show active'; } ?>">
                           <div class="ratingschecks formratesweaks">
                              <div class="centerratingdf">
                                 <form method="post" action="<?php echo base_url();?>user/user_profile_update_six">
                                    <input type="hidden" name="token" value="<?php if($profileDetail['token']){echo $profileDetail['token'];}?>">

                                     <p>What are your strengths?</p>
                                     <textarea name="strength" placeholder="Write here..." class="form-control" width="100px;"><?php if(!empty($strengthdata[0]['strength'])){echo $strengthdata[0]['strength'];}?></textarea>
                                     <p>What are your weaknesses?</p>
                                     <textarea name="weakness" placeholder="Write here...." class="form-control"><?php if(!empty($strengthdata[0]['weakness'])){echo $strengthdata[0]['weakness'];}?></textarea>
                                     <p>What are your 3 biggest achievements?</p>
                                     <textarea name="achievements" placeholder="Write here...." class="form-control"><?php if(!empty($strengthdata[0]['achievements'])){echo $strengthdata[0]['achievements'];}?></textarea>
                                     
                                     <button type="submit" class="srchbtns">Save</button>
                                 
                                 </form>
                              </div>
                           </div>
                        </div>

                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>




<div class="modal fade" id="exampleModalCenter818" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
     <div class="modal-content">
        <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">×</span>
           </button>
        </div>
        <div class="modal-body">
           <div class="formmidaress modpassfull">
              <div class="filldetails">
                 <p>Top Clients Supported</p>
                 <span id="respclient"></span>
                 <input type="text" name="clients" id="clients" class="form-control" placeholder="Please enter Top Client">
                 <div class="statsusdd">
                    <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                    <button type="button" class="updt" id="clientbtn">Add</button>
                 </div>
              </div>
           </div>
        </div>
     </div>
  </div>
</div>

<div class="modal fade" id="exampleModalCenter819" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
     <div class="modal-content">
        <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">×</span>
           </button>
        </div>
        <div class="modal-body">
           <div class="formmidaress modpassfull">
              <div class="filldetails">
                 <p>Languages Spoken</p>
                 <span id="resplanguage"></span>
                 <div class="form-group">
                    <select name="languages" id="languagees" class="form-control" placeholder="Select Language" style="width:100%">
                       <option value="">Select Language</option>
                       <?php
                        if(!empty($languagelists)) {
                           foreach($languagelists as $languagelist) {
                       ?>
                           <option value="<?php echo $languagelist['id']?>"><?php echo $languagelist['name']?></option>
                       <?php     
                          }}
                       ?>
                    </select>
                 </div>
                 <div class="statsusdd">
                    <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                    <button type="button" class="updt" id="languagebtn">Add</button>
                 </div>
              </div>
           </div>
        </div>
     </div>
  </div>
</div>

<div class="modal fade" id="exampleModalCenter820" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
     <div class="modal-content">
        <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">×</span>
           </button>
        </div>
        <div class="modal-body">
           <div class="formmidaress modpassfull">
              <div class="filldetails">
                 <p>Skills</p>
                 <span id="respskill"></span>
                 <div class="form-group">
                    <input type="text" name="skills" id="skills" placeholder="Please enter skills" class="form-control">
                 </div>
                 <div class="statsusdd">
                    <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                    <button type="button" class="updt" id="skillsbtn">Add</button>
                 </div>
              </div>
           </div>
        </div>
     </div>
  </div>
</div>

<div class="modal fade" id="exampleModalCenter3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered addeducation" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="workexperience addresumes editssections">
               <p>Add Experience</p>
               <div class="formmidaress modpassfull">
                  <div class="filldetails">
                     <form method="post" action="<?php echo base_url();?>user/workExperience">
                        <div class="onewayfields">
                           <div class="forminputspswd">
                              <input type="text" name="company" class="form-control" placeholder="Company Name">
                           </div>
                           <div class="forminputspswd">
                              <input type="text" name="title" class="form-control" placeholder="Job Title">
                           </div>
                        </div>
                        <div class="onewayfields">
                           <select name="joblevel" id="testts" class="form-control forminputspswd" placeholder="Select Level" style="margin-left: 0px;">
                              <option value="">Select Level</option>
                              <?php
                                if($levellist) {
                                    foreach($levellist as $levellists) {
                              ?>
                                   <option value="<?php echo $levellists['id'];?>"> <?php echo $levellists['level'];?> </option>
                              <?php
                                  }
                                }
                              ?>
                           </select>
                           <select name="category" id="slctsubcat" class="form-control forminputspswd" placeholder="Select Category" style="margin-left: 0px;" onchange="getSubcat()">
                              <option value="">Select Category</option>
                              <?php
                                 if($categorylist) {
                                    foreach($categorylist as $categorylists) {
                              ?>
                                    <option value="<?php echo $categorylists['id'];?>"> <?php echo $categorylists['category'];?> </option>
                              <?php
                                 }
                                 }
                              ?>
                           </select>
                        </div>

                        <div class="onewayfields">
                           <select name="subcategory" id="subcatres" class="form-control forminputspswd" placeholder="Select Subcategory" style="margin-left: 0px;">
                              <option value="">Select Subcategory</option>
                           </select>
                        </div>
                        
                        <div class="onewayfields">
                           <p class="blachd">Currently Working on this role  <input type ="checkbox" value="1" id="checkwid" name="regularcheck" class="check_status" onchange="valueChanged()"></p>
                           <div class="forminputspswd">
                              <p>From</p>
                              <input type='text' name="from" autocomplete="off" class="form-control" id="datepicker" /> 
                              <i class="far fa-calendar-alt fieldicons" placeholder="Working From.."></i>
                           </div>
                           <span id="rgst" style="float:left;"></span>
                           <div class="forminputspswd" id="statusTOdate">
                              <p>To</p>
                              <input type='text' name="to" class="form-control" autocomplete="off" id="datepicker1" />
                              <i class="far fa-calendar-alt fieldicons"></i>
                           </div>
                        </div>

                        <div class="forminputspswd">
                           <textarea class="form-control" name="desc" placeholder="Add Description"></textarea> 
                        </div>
                        <div class="forminputspswd" style="    margin-top: 20px;">
                           <input type="hidden" name="id" value="">
                           <button type="submit" class="srchbtns">Save</button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="mymodell" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered addeducation" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="workexperience addresumes editssections">
               <p>Edit Experience</p>
               <div class="formmidaress modpassfull">
                  <div class="filldetails">
                     <form method="post" action="<?php echo base_url('user/workExperience')?>">
                        <div class="onewayfields">
                           <div class="forminputspswd">
                              <input type="text" name="company" id="camp_name" class="form-control" placeholder="Company Name" style="color: #717171;font-weight: 300;">
                           </div>
                           <div class="forminputspswd">
                              <input type="text" name="title" id="title" class="form-control" placeholder="Job Title" style="color: #717171;font-weight: 300;">
                           </div>
                        </div>
                        <div class="onewayfields">
                           <select name="joblevel" id="slct" class="form-control forminputspswd" placeholder="Select Level" style="margin-left: 0px;">
                              <option value="">Select Level</option>
                              <?php
                                 if($levellist) {
                                    foreach($levellist as $levellists) {
                                 ?>
                              <option value="<?php echo $levellists['id'];?>"> <?php echo $levellists['level'];?> </option>
                              <?php
                                 }
                                 }
                                 ?>
                           </select>
                           <select name="category" id="slctsubcatt" class="form-control forminputspswd" placeholder="Select Category" style="margin-left: 0px;" onchange="getSubcatt()">
                              <option value="">Select Category</option>
                              <?php
                                 if($categorylist) {
                                    foreach($categorylist as $categorylists) {
                                 ?>
                              <option value="<?php echo $categorylists['id'];?>"> <?php echo $categorylists['category'];?> </option>
                              <?php
                                 }
                                 }
                                 ?>
                           </select>
                        </div>
                        <div class="onewayfields">
                           <select name="subcategory" id="subcatress" class="form-control forminputspswd" placeholder="Select Subcategory" style="margin-left: 0px;">
                              <option value="">Select Subcategory</option>
                              
                           </select>
                        </div>
                        <div class="onewayfields">
                           <p class="blachd">Currently Working on this role  <input type ="checkbox" value="1" id="checkwid" name="regularcheck" class="check_status" onchange="valueChangedd()"></p>
                           <div class="forminputspswd">
                              <p>From</p>
                              <input type='text' name="from" autocomplete="off" class="form-control" id="datepicker5" /> 
                              <!--   <input type="text" class="form-control" id="datepicker"> -->
                           </div>
                           <span id="rgstt" style="float: left;">
                              <p class="text-success"></p>
                           </span>
                           <div class="forminputspswd" id="statusTOdatee">
                              <p>To</p>
                              <input type='text' name="to" class="form-control" autocomplete="off" id="datepicker6" />
                              <i class="far fa-calendar-alt fieldicons"></i>
                           </div>
                        </div>
                        <div class="forminputspswd">
                           <textarea class="form-control" name="desc" id="desc" placeholder="Add Description"></textarea> 
                        </div>
                        <div class="forminputspswd">
                           <input type="hidden" name="id" id="workid">
                           <button type="submit" class="srchbtns">Update</button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="exampleModalCenter2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered addeducation" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="workexperience addresumes editssections">
               <p>Add Education</p>
               <div class="formmidaress modpassfull">
                  <div class="filldetails">
                     <form method="post" action="<?php echo base_url();?>user/education">
                        <div class="forminputspswd">
                           <input type="text" name="university" class="form-control" placeholder="University/Institute">
                        </div>
                        <div class="onewayfields">
                           <div class="forminputspswd">
                              <select name="attainment" id="attainment" class="form-control" placeholder="Attainment" style="width:100%">
                                 <option value="">Choices for Educational Attainment</option>
                                 <option value="High School Graduate" >High School Graduate</option>
                                 <option value="Vocational" >Vocational</option>
                                 <option value="Undergraduate" >Undergraduate</option>
                                 <option value="Associate Degree" >Associate Degree</option>
                                 <option value="College Graduate" >College Graduate</option>
                                 <option value="Post Graduate" >Post Graduate</option>
                              </select>
                           </div>
                           <div class="forminputspswd">
                              <input type="text" name="degree" class="form-control" placeholder="Degree/Course">
                           </div>
                        </div>
                        <div class="onewayfields">
                           <div class="forminputspswd">
                              <p>From</p>
                              <input type='text' name="from" class="form-control" id='datepicker11' autocomplete="off"/> 
                              <i class="far fa-calendar-alt fieldicons"></i>
                           </div>
                           <div class="forminputspswd">
                              <p>To</p>
                              <input type='text' name="to" class="form-control" id='datepicker12' autocomplete="off"/>
                              <i class="far fa-calendar-alt fieldicons"></i>
                           </div>
                        </div>
                        <div class="forminputspswd">
                           <input type="hidden" name="id" value="">
                           <button type="submit" class="srchbtns">Save</button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="exampleModalCenter25" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered addeducation" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="workexperience addresumes editssections">
               <p>Edit Education</p>
               <div class="formmidaress modpassfull">
                  <div class="filldetails">
                     <form method="post" action="<?php echo base_url();?>user/education">
                        <div class="onewayfields">
                           <div class="forminputspswd">
                              <select name="attainment" id="attainment" class="form-control" placeholder="Attainment" style="width:100%">
                                 <option value="">Choices for Educational Attainment</option>
                                 <option value="High School Graduate" >High School Graduate</option>
                                 <option value="Vocational" >Vocational</option>
                                 <option value="Undergraduate" >Undergraduate</option>
                                 <option value="Associate Degree" >Associate Degree</option>
                                 <option value="College Graduate" >College Graduate</option>
                                 <option value="Post Graduate" >Post Graduate</option>
                              </select>
                           </div>
                           <div class="forminputspswd">
                              <input type="text" name="degree" class="form-control" placeholder="Degree/Course" id="degree">
                           </div>
                        </div>
                        <div class="forminputspswd">
                           <input type="text" name="university" class="form-control" placeholder="University/Institute" id="university">
                        </div>
                        <div class="onewayfields">
                           <div class="forminputspswd">
                              <p>From</p>
                              <input type='text' name="from" class="form-control" id='datepicker111' autocomplete="off" /> 
                              <i class="far fa-calendar-alt fieldicons"></i>
                           </div>
                           <div class="forminputspswd">
                              <p>To</p>
                              <input type='text' name="to" class="form-control" id='datepicker122' autocomplete="off" />
                              <i class="far fa-calendar-alt fieldicons"></i>
                           </div>
                        </div>
                        <div class="forminputspswd">
                           <input type="hidden" name="id" id="setid">
                           <button type="submit" class="srchbtns">Update</button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="updateremindermodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered addeducation" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <img src="<?php echo base_url(); ?>webfiles/img/resume-icons-png-71.png" style="width: 10%;margin:auto;margin: 0 auto;display: block;">
            <p class="text-center" style="margin-top: 20px;"> Update your profile and create a FREE Resume from JobYoDA right away! </p>
         </div>
      </div>
   </div>
</div>

<script src="<?php echo base_url().'webfiles/';?>js/moment.min.js"></script>
<script src="<?php echo base_url().'webfiles/';?>js/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

<?php if(empty(array_filter($workData)) || empty(array_filter($eduData)) || empty(array_filter($skillData))) {  ?>

<?php if(empty($tab_value) || $tab_value == "home") { ?>

      <script type="text/javascript">
         $(document).ready(function() {
            $("#updateremindermodal").modal("show");
         });  
      </script>
<?php } ?>
<?php } else { } ?>

<script type="text/javascript">
   function readURL(input) {

      console.log("readurl");
      if (input.files && input.files[0]) {
        var reader = new FileReader();
    
        reader.onload = function(e) {
         console.log(e.target.result);
          $('#blah').css("display","block");  
          $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#imgInp").change(function() {

      readURL(this);
    }
</script>

<script>
   $.validate({
       modules: 'location, date, security, file',
       onModulesLoaded: function() {
           $('#country').suggestCountry();
       }
   });
   $('#presentation').restrictLength($('#pres-max-length'));
   
</script>
<script>
   $(function() {
       $("#proid").datepicker({
           changeYear: true,
           changeMonth: true,
           maxDate: '-1'
       });
   });
</script>

<script>
   function deleteRecord(id, tblname) {
   
      var condel = confirm("Are you sure you want to delete this client?");
      if (condel) {
         $.ajax({
            type: 'POST',
            url: "<?php echo base_url('user/delete_single_work')?>",
            data: '&user_id=' + id + '&tbname=' + tblname,
            success: function(html) {
               if (html == 1) {
                    $('#error_msg').html('<p class="alert alert-success" >Client deleted Successfully.</p>');
                    window.setTimeout(function() {
                        location.href ='<?php echo base_url();?>profile';
                    }, 300)
               } else {
                    $('#error_msg').html('<p class="alert alert-error flash" >Client not Deleted.</p>');
                    hide_html('error_brand');
               }
            }
         });
      }
   }
</script>
<script>
   $("#clientbtn").click(function() {
       var clientname = $("#clients").val();
       if (clientname == "") {
           $("#respclient").html("<p class='text-danger'>please add client</p>");
           return false;
       }
       $.ajax({
           type: "POST",
           url: "<?php echo base_url(); ?>" + "user/topclients",
           data: {
               clients: clientname
           },
           cache: false,
           success: function(htmldata) {
               $("#respclient").html(htmldata);
               $("#clients").val('');
               setTimeout(function() {
                  location.href ='<?php echo base_url();?>profile';
               }, 300);
           },
           error: function() {
               console.log('error');
           }
       });
   });
</script>
<script>
   function getclientval(id) {
       var clientId = $("#clients" + id).val();
       var clientname = $("#cli" + id).val();
       if (clientname == "") {
           $("#respclient" + id).html("<p class='text-danger'>please add clients</p>");
           return false;
       }
       $.ajax({
           type: "POST",
           url: "<?php echo base_url(); ?>" + "user/topclientsedit",
           data: {
               clients: clientname,
               clientId: clientId
           },
           cache: false,
           success: function(htmldata) {
               $("#respclient" + id).html(htmldata);
               setTimeout(function() {
                   location.reload();
               }, 300);
           },
           error: function() {
               console.log('error');
           }
      });
   }
</script>

<script>
   $("#languagebtn").click(function() {
       var languagees = $("#languagees").val();
   
       if (languagees == "") {
           $("#resplanguage").html("<p class='text-danger'>please add Language</p>");
           return false;
       } else {
           $("#resplanguage").html("");
           $.ajax({
               type: "POST",
               url: "<?php echo base_url(); ?>" + "user/languageadd",
               data: {
                   language: languagees
               },
               cache: false,
               success: function(htmldata) {
                   $("#resplanguage").html(htmldata);
                   $("#languagees").val('');
                   setTimeout(function() {
                      location.href ='<?php echo base_url();?>profile';
                   }, 300);
               },
               error: function() {
                   console.log('error');
               }
           });
       }
   });
</script>
<script>
   function getlangval(id) {
       var langId = $("#languageDataS" + id).val();
       var langname = $("#languagees" + id).val();
       if (langname == "") {
           $("#resplang" + id).html("<p class='text-danger'>please add language</p>");
           return false;
       }
       $.ajax({
           type: "POST",
           url: "<?php echo base_url(); ?>" + "user/languageedit",
           data: {
               langname: langname,
               langId: langId
           },
           cache: false,
           success: function(htmldata) {
               $("#resplang" + id).html(htmldata);
               setTimeout(function() {
                   location.reload();
               }, 300);
           },
           error: function() {
               console.log('error');
           }
       });
   }
</script>

<script>
  $(document).ready(function() {
      $(".lastbpo").change(function() {
          var selected = $(this).val();
          if(selected == "other") {
              $(".lastbpoother").css("display","block");
              $(".lastbpoother > input").attr("name","topbpoother");
          } else {
               
               $("#topbpoother").val("");
               $(".lastbpoother").css("display","none");
              
          }
      });
  });
</script>

<script>
   $("#skillsbtn").click(function() {
       var skills = $("#skills").val();
   
       if (skills == "") {
           $("#respskill").html("<p class='text-danger'>please add Skill</p>");
           return false;
       } else {
           $("#respskill").html("");
           $.ajax({
               type: "POST",
               url: "<?php echo base_url(); ?>" + "user/skilladd",
               data: {
                   skill: skills
               },
               cache: false,
               success: function(htmldata) {
                   $("#respskill").html(htmldata);
                   $("#skills").val('');
                   setTimeout(function() {
                      location.href ='<?php echo base_url();?>profile';
                   }, 300);
               },
               error: function() {
                   console.log('error');
               }
           });
       }
   });
</script>
<script>
   function getskilval(id) {
       var skillId = $("#skillDataS" + id).val();
       var skill = $("#skill" + id).val();
       if (skill == "") {
           $("#respskill" + id).html("<p class='text-danger'>please add skill</p>");
           return false;
       }
       $.ajax({
           type: "POST",
           url: "<?php echo base_url(); ?>" + "user/skilledit",
           data: {
               skill: skill,
               skillId: skillId
           },
           cache: false,
           success: function(htmldata) {
               $("#respskill" + id).html(htmldata);
               setTimeout(function() {
                   location.reload();
               }, 300);
           },
           error: function() {
               console.log('error');
           }
       });
   }
</script>
<script>
   function gatval(id) {
       $.ajax({
           type: "POST",
           url: "<?php echo base_url(); ?>" + "user/updateworkview",
           data: {
               id: id
           },
           cache: false,
           success: function(htmldata) {
               var result = JSON.parse(htmldata);
               $("#camp_name").val(result[0].company);
               $("#title").val(result[0].title);
               $("#desc").val(result[0].jobDesc);
               $("#datepicker5").val(result[0].workFrom);
               $("#datepicker6").val(result[0].workTo);

               $("#slct option[value="+result[0].level+"]").attr('selected','selected');
               //$("#slcttt select").val(result[0].level);
               //$("#slctcat select").val(result[0].category);
               $("#slctsubcatt option[value="+result[0].category+"]").attr('selected','selected');
               //$("#slctsubcatttt select").val(result[0].subcategory);
               $("#subcatress").html("<option value='"+result[0].subcategory+"'>"+result[0].subcatname+"</option");

               $("#workid").val(result[0].id);
               $("#mymodell").modal(result);
           },
           error: function() {
               console.log('error');
           }
       });
   }
</script>
<script type="text/javascript">
   function valueChanged() {
       if ($('.check_status').is(":checked")) {
           $("#statusTOdate").hide();
           $("#rgst").html('<p class="text-success">Present</p>');
       } else {
           $("#statusTOdate").show();
           $("#rgst").html('');
       }
   }
   
</script>
<script type="text/javascript">
   function valueChangedd() {
       if ($('.check_status').is(":checked")) {
           $("#statusTOdatee").hide();
           $("#rgstt").html('<p class="text-primary">Present</p>');
       } else {
           $("#statusTOdatee").show();
           $("#rgstt").html('');
       }
   }
</script>
<script>
   $(function() {
       $("#datepicker").datepicker();
   });
   
</script>
<script>
   $(function() {
       $("#datepicker1").datepicker();
   });
   
</script>
<script>
   $(function() {
       $("#datepicker11").datepicker();
   });
   
</script>
<script>
   $(function() {
       $("#datepicker12").datepicker();
   });
   
</script>
<script>
   $(function() {
       $("#datepicker5").datepicker();
   });
   
</script>
<script>
   $(function() {
       $("#datepicker6").datepicker();
   });
   
</script>
<script>
   $(function() {
       $("#datepicker111").datepicker();
   });
   
</script>
<script>
   $(function() {
       $("#datepicker122").datepicker();
   });
   
</script>
<script>
   $(function() {
       $("#proid").datepicker({
           changeYear: true,
           changeMonth: true,
           maxDate: '-1'
       });
   });
</script>
<script>
   function gatvaledu(id) {
       $.ajax({
           type: "POST",
           url: "<?php echo base_url(); ?>" + "user/updateeduview",
           data: {
               id: id
           },
           cache: false,
           success: function(htmldata) {
               var result = JSON.parse(htmldata);
   
               $("#attainment").val(result[0].attainment);
               $("#degree").val(result[0].degree);
               $("#university").val(result[0].university);
               $("#datepicker111").val(result[0].degreeFrom);
               $("#datepicker122").val(result[0].degreeTo);
               $("#setid").val(result[0].id);
               $("#mymodelll").modal(result);
           },
           error: function() {
               console.log('error');
           }
       });
   }
</script>
<script type="text/javascript">
   $(document).ready(function() {
       $('#framework').multiselect({
           nonSelectedText: "Job's Interests (Select any 3)",
           enableFiltering: true,
           enableCaseInsensitiveFiltering: true,
           onChange: function(option, checked) {
               // Get selected options.
               var selectedOptions = $('.jobinterest .multiselect-ui option:selected');
   
               // if (selectedOptions.length >= 3) {
               //     $('.interestmsg').html("You can select 3 Job interests only");
               // }
               if (selectedOptions.length >= 3) {
                   // Disable all other checkboxes.
                   var nonSelectedOptions = $('.jobinterest .multiselect-ui option').filter(function() {
                       return !$(this).is(':selected');
                   });
   
                   nonSelectedOptions.each(function() {
                       var input = $('.jobinterest input[type="checkbox"][value="' + $(this).val() + '"]');
                       input.prop('disabled', true);
                       input.parent('li').addClass('disabled');
                   });
               } else {
                   // Enable all checkboxes.
                   $('.jobinterest .multiselect-ui option').each(function() {
                       var input = $('.jobinterest input[type="checkbox"][value="' + $(this).val() + '"]');
                       input.prop('disabled', false);
                       input.parent('li').addClass('disabled');
                   });
               }
           }
       });

      var selectedOptions = $('.jobinterest .multiselect-ui option:selected');
      if (selectedOptions.length >= 3) {
          var nonSelectedOptions = $('.jobinterest .multiselect-ui option').filter(function() {
              return !$(this).is(':selected');
          });
          nonSelectedOptions.each(function() {
              var input = $('.jobinterest input[type="checkbox"][value="' + $(this).val() + '"]');
              input.prop('disabled', true);
              input.parent('li').addClass('disabled');
          });
      }
   });

   $(document).ready(function() {
       $('#frameworkone').multiselect({
           nonSelectedText: "Job's Benefits (Select any 3)",
           enableFiltering: true,
           enableCaseInsensitiveFiltering: true,
           onChange: function(option, checked) {
               // Get selected options.
               var selectedOptions = $('.jobbenefits .multiselect-ui option:selected');
   
               if (selectedOptions.length >= 3) {
                   // Disable all other checkboxes.
                   var nonSelectedOptions = $('.jobbenefits .multiselect-ui option').filter(function() {
                       return !$(this).is(':selected');
                   });
   
                   nonSelectedOptions.each(function() {
                       var input = $('.jobbenefits input[type="checkbox"][value="' + $(this).val() + '"]');
                       input.prop('disabled', true);
                       input.parent('li').addClass('disabled');
                   });
               } else {
                   // Enable all checkboxes.
                   $('.jobbenefits .multiselect-ui option').each(function() {
                       var input = $('.jobbenefits input[type="checkbox"][value="' + $(this).val() + '"]');
                       input.prop('disabled', false);
                       input.parent('li').addClass('disabled');
                   });
               }
           }
       });

      var selectedOptions = $('.jobbenefits .multiselect-ui option:selected');
      if (selectedOptions.length >= 3) {
          var nonSelectedOptions = $('.jobbenefits .multiselect-ui option').filter(function() {
              return !$(this).is(':selected');
          });
          nonSelectedOptions.each(function() {
              var input = $('.jobbenefits input[type="checkbox"][value="' + $(this).val() + '"]');
              input.prop('disabled', true);
              input.parent('li').addClass('disabled');
          });
      }
   });
</script>
<script type="text/javascript">
    //$(document).ready(function() {
      function addonmultiselect() {

        $('#framework').multiselect({
            nonSelectedText: "Job Interests (Select any 3)",
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            onChange: function(option, checked) {
                // Get selected options.
                var selectedOptions = $('.multiselect-ui option:selected');
                
                if (selectedOptions.length >= 3) {
                     $('.interestmsg').html("You can select 3 Job interests only");
                }
                if (selectedOptions.length >= 3) {
                    // Disable all other checkboxes.
                    var nonSelectedOptions = $('.multiselect-ui option').filter(function() {
                        return !$(this).is(':selected');
                    });
 
                    nonSelectedOptions.each(function() {
                        var input = $('input[type="checkbox"][value="' + $(this).val() + '"]');
                        input.prop('disabled', true);
                        input.parent('li').addClass('disabled');
                    });
                }
                else {
                    // Enable all checkboxes.
                    $('.multiselect-ui option').each(function() {
                        var input = $('input[type="checkbox"][value="' + $(this).val() + '"]');
                        input.prop('disabled', false);
                        input.parent('li').addClass('disabled');
                    });
                }
            }
        });
      }
    //});
</script>

<script>

   function getSubcat() {
       var categoryid = $("#slctsubcat").val();
       $.ajax({
           type: "POST",
           url: "<?php echo base_url(); ?>" + "user/SubcategoryListing",
           data: {
               categoryid: categoryid
           },
           cache: false,
           success: function(htmldata) {
   
               $("#subcatres").html(htmldata);
   
           },
           error: function() {
               console.log('error');
           }
       });
   }
</script>
<script>
   function getSubcatt() {
       var categoryid = $("#slctsubcatt").val();
       $.ajax({
           type: "POST",
           url: "<?php echo base_url(); ?>" + "user/SubcategoryListing",
           data: {
               categoryid: categoryid
           },
           cache: false,
           success: function(htmldata) {
   
               $("#subcatress").html(htmldata);
   
           },
           error: function() {
               console.log('error');
           }
       });
   }
</script>
<script>
   function gatvaledu(id) {
       $.ajax({
           type: "POST",
           url: "<?php echo base_url(); ?>" + "user/updateeduview",
           data: {
               id: id
           },
           cache: false,
           success: function(htmldata) {
               var result = JSON.parse(htmldata);
   
               //$("#attainment").val(result[0].attainment);
               $("#attainment option[value="+result[0].attainment+"]").attr('selected','selected');

               $("#degree").val(result[0].degree);
               $("#university").val(result[0].university);
               $("#datepicker111").val(result[0].degreeFrom);
               $("#datepicker122").val(result[0].degreeTo);
               $("#setid").val(result[0].id);
               $("#exampleModalCenter25").modal(result);
           },
           error: function() {
               console.log('error');
           }
       });
   }
</script>

<script>
$(document).ready(function() {
    $(".changeindustry").on('change', function() {
        var industry = $(this).val();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "auth/getcategory",
            data: {
                industry: industry
            },
            cache: false,
            success: function(htmldata) {
              
                $('.changecat').html(htmldata);
            },
            error: function() {
                console.log('error');
            }
        });
        $('#framework').html("");
        $("#framework").multiselect('destroy');

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "auth/getjobintrestedin",
            data: {
                industry: industry
            },
            cache: false,
            success: function(htmldata) {
              
                $('#framework').css("display","block");
                $('#framework').html(htmldata);
                addonmultiselect();

                $(".getSubcat").html('<option value="">Select Your Sub Specialization (Sub-Category) </option>');

            },
            error: function() {
                console.log('error');
            }
        });
    });
});
</script>

<script>
$(document).ready(function() {
    $(".changecat").on('change', function() {
        var cat = $(this).val();

        var industry = $(".changeindustry :selected").val();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "auth/getsubcategory",
            data: {
                cat: cat,
                industry: industry
            },
            cache: false,
            success: function(htmldata) {

                $('.getsubcat').html(htmldata);
            },
            error: function() {
                console.log('error');
            }
        });
    });
});
</script>
<?php include_once('footer.php'); ?>
</body>
</html>