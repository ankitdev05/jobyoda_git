<div class="SearchTop">
                     <h2 class="head1">Job Type</h2>
                     <ul class="searhDropUl">
                     <?php if(!empty($category_list)) {
                        $y=1;   
                        foreach($category_list as $category_lists) {      
                     ?>
                        <li>
                            <div class="Checkbox">
                                <input type="checkbox" name="jobcategoryy" onchange='onchangecategory("<?php echo $category_lists['category'];?>")' value="<?php echo $category_lists['category'];?>" class="custom-control-input" id="customCheck<?php echo $y.$y;?>">

                                <label for="customCheck<?php echo $y.$y;?>">
                                    <?php echo $category_lists['category'];?> 
                                </label>
                            </div>
                        </li>

                     <?php $y++;}}?>
                     
                     </ul>
                     <input type="hidden" >
                  </div>
                  <div class="SearchTop">
                     <h2 class="head1">Job Level</h2>
                     <!-- <ul class="searhDropUl1"> -->
                     <ul class="searhDropUl">
                     <?php
                        if(!empty($level_list)) {
                            $x=1;
                        foreach($level_list as $level_lists) {
                     ?>
                        <li>
                            <div class="Checkbox">
                                <input type="checkbox" name="joblevell" value="<?php echo $level_lists['id'];?>" onchange='onchangejobtype("<?php echo $level_lists['level'];?>")' class="custom-control-input" id="customCheck<?php echo $x;?>">
                                
                                <label for="customCheck<?php echo $x;?>">
                                    <?php echo $level_lists['level'];?> 
                                </label>
                            </div>
                        </li>
                     
                     <?php $x++;}}?>
                     </ul>
                  </div>
                  <div class="SearchTop">
                     <h2 class="head1">Companies</h2>
                     <div class="owl-carousel owl-theme" id="searchCom">
                     <?php
                        if(!empty($ourSubPartners)) {
                     ?>
                     <?php
                        foreach($ourSubPartners as $ourPartner) {
                     ?>
                        <div class="item">
                           <div class="SearchCompany">
                              <a href="javascript:void(0)" onclick='onchangecompanytype("<?php echo $ourPartner['cname']; ?>")'>
                                <figure>
                                  <img src="<?php echo $ourPartner['image']; ?>">
                                </figure>
                                <h2 class="head1"><?php echo $ourPartner['cname']; ?></h2>
                              </a>
                           </div>
                        </div>
                     <?php
                        }
                     ?>
                     <?php
                        }
                     ?>
                        
                     </div>
                  </div>


<script type="text/javascript">
   $(document).ready(function () {
    $("#searchCom").owlCarousel({
        margin: 0,
        nav: true,
        dots: false,
        autoplayHoverPause: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        loop: false,
        responsive: {
            0: {
                items: 2,
               
            },
                400: {
                items: 2,
                 
            },
            600: {
                items: 4,
            },
            1000: {
                items: 7,
            },
        },
    });
});
</script>