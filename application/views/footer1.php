    
    <div class="clear"></div>
    
    <footer>
        <!-- <div class="elfsight-app-54262ed6-0019-4255-bcbb-f426ae91066c"></div> -->
        
        <div class="Footer">
            <div class="FooterTop">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="Foot">
                                <figure><img src="<?php echo base_url().'recruiterfiles/';?>images/jobyoDa.png"></figure>

                                <p>
                                    <span><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                                    Level 10-1 Fort Legend Tower, 3rd Avenue Bonifacio Global City 31st Street Fort Bonifacio TAGUIG CITY, FOURTH DISTRICT, NCR, Philippines, 1634
                                </p>

                                <p>
                                    <span><i class="fa fa-envelope" aria-hidden="true"></i></span>
                                    <a href="mailto:info@jobhunt.com">help@jobyoda.com</a>
                                </p>

                                <!-- <h6><a href="tel:+63 9178721630">+63 9178721630</a></h6> -->

                            </div>
                        </div>
                        <div class="col-sm-3 col-sm-offset-1">
                            <div class="Foot">
                                <h3>Useful Links</h3>
                                <ul>
                                    <li><a href="<?php echo base_url(); ?>privacy_policy">Privacy Policy </a></li>
                                    <li><a href="<?php echo base_url(); ?>terms">Terms of Service </a></li>
                                    <li><a href="<?php echo base_url(); ?>contact">Contact Us </a></li>
                                    <li><a href="<?php echo base_url(); ?>about">About Us </a></li> 
                                    <li><a href="<?php echo base_url(); ?>how_it_works">How it Works </a></li>
                               </ul>
                            </div>
                        </div>
                        
                        <div class="col-sm-4">
                            <div class="Foot">
                                <h3>Get It On </h3>
                                <div class="DownloadApp">
                                    <a href="https://apps.apple.com/us/app/jobyoda/id1471619860?ls=1" class="download-btn" target="_blank">
                                        <span>
                                            <!-- <img src="https://jobyoda.com/webfiles/img/home/apple.png"> -->
                                            <img src="<?php echo base_url();?>webfiles/newone/social/ios_download_jobyoda.png">

                                        </span>
                                        <!-- <p>
                                            <small>Download On</small>
                                            <br>
                                            App Store
                                        </p> -->
                                    </a>
                                    <a href="https://play.google.com/store/apps/details?id=com.jobyodamo" class="download-btn" target="_blank">
                                        <span>
                                            <!-- <img src="https://jobyoda.com/webfiles/img/home/appstore.png"> -->
                                            <img src="<?php echo base_url();?>webfiles/newone/social/android_download_jobyoda.png">
                                        </span>
                                        <!-- <p>
                                            <small>Get It On</small>
                                            <br>
                                            Google Play
                                        </p> -->
                                    </a>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <div class="Copyright">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <p>Copyright JobYoDA © 2020. All Rights Reserved</p>
                        </div>
                        <div class="col-sm-6">
                            <ul>
                                <li>
                                    <a href="https://www.facebook.com/jobyodapage/" target="_blank">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                 
                                <li>
                                    <a href="https://instagram.com/jobyoda_ig?igshid=1u7xaxp20etlb" target="_blank">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li> 
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </footer>

<!--     <div class="ModalBox">
        <div id="VideoModal" class="modal fade" role="dialog">
            <div class="modal-dialog"> 
                <div class="modal-content"> 
                    <div class="modal-body">
                        <a href="javascript:void(0);" class="Close" data-dismiss="modal">×</a>
                        <iframe src="https://www.youtube.com/embed/G3YeTQEdZmw"></iframe>
                    </div> 
                </div>
            </div>
        </div> 
    </div> -->
    <?php $requestURI = $_SERVER['REQUEST_URI'];?>

    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="<?php echo base_url().'webfiles/';?>newone/js/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <script src="<?php echo base_url().'webfiles/';?>newone/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url().'webfiles/';?>js/owl.carousel.js"></script>
    <script src="<?php echo base_url().'webfiles/';?>newone/js/index.js"></script>
    <script src="<?php echo base_url().'webfiles/';?>newone/js/aos.js"></script>
    <script src="<?php echo base_url().'webfiles/';?>js/adminyoda.js"></script>

    <?php if($requestURI == "/") { ?>
    <script src="https://apps.elfsight.com/p/platform.js" defer></script>
    <script src="<?php echo base_url().'webfiles/';?>js/popper.min.js"></script>
    <link rel="stylesheet" href="//jonthornton.github.io/jquery-timepicker/jquery.timepicker.css">
    <script src="//jonthornton.github.io/jquery-timepicker/jquery.timepicker.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
    <?php } ?>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />
    <script src='https://cdn.jsdelivr.net/jquery.counterup/1.0/jquery.counterup.min.js'></script>
    <style>
        .ui-menu.ui-widget.ui-widget-content.ui-autocomplete.ui-front{
                height: 200px!important;
                overflow: hidden!important;
                overflow-y: scroll!important;
        }
        .ui-menu.ui-widget.ui-widget-content.ui-autocomplete.ui-front a:focus {
            background-color: #ccc;
        }
    </style>

    <script type="text/javascript">
        
        $("#myInput").click(function() {

            $.ajax({
                url: "<?php echo base_url() ?>homepage/fetchfilterhtml",
                type: "GET",
                success: function(data) {

                    $(".searhDrop").html(data);
                },
                error: function(jqXHR, textStatus, errorThrown){
                    console.log(jqXHR);                        
                    console.log(textStatus);                        
                    console.log(errorThrown);                        
                },
            });

            $(".searhDrop").show();
        });

    </script>

    <script type="text/javascript">
        
        $(document).on('focus', '.locationShow', function () {

            $.ajax({
                url: "<?php echo base_url() ?>homepage/fetchfilterlocationhtml",
                type: "GET",
                success: function(data) {

                    $(".locationDetail").html(data);
                },
                error: function(jqXHR, textStatus, errorThrown){
                    console.log(jqXHR);                        
                    console.log(textStatus);                        
                    console.log(errorThrown);                        
                },
            });

            $('.locationDetail').slideDown();
        });

    </script>
    
    <script>
    $(document).ready(function(){
        $("#myInput").autocomplete({
            source: function( request, response ) {

                $.ajax({
                    url: "<?php echo base_url() ?>homepage/fetchcompany",
                    type: "POST",
                    dataType: "json",
                    data: { keyword: request.term},
                    success: function(data){
                        response(data);
                        //console.log(data);
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                        console.log(jqXHR);                        
                        console.log(textStatus);                        
                        console.log(errorThrown);                        
                    },
                    minLength: 3,
                });
            },
            select: function (event, ui) {
               // Set selection
               $('#dropdown_cname_selected').val(ui.item.value); // save selected id to input
               $('#myInput').val(ui.item.value); // save selected id to input
               return false;
            }
        }).data('ui-autocomplete')._renderItem = function (ul, item) {
            return $('<li></li>')
                .data('item.autocomplete', item)
                .append('<a href="javascript:void(0)" style="display: block;">'+item.label+'<span style="float:right;padding-right:10px;">' + item.label1 + ' Openings</span></li>')
                .appendTo(ul);
        };
        
    });
    </script>   

    <script type="text/javascript">
        // Animation
        AOS.init({
        duration: 1200,
        easing: 'ease-in-out-back'
        });
    </script>

    <script type="text/javascript"> 
        if(screen.width<=768){
            $(".SearchTab").click(function() {
                var id=$(this).data('id')
                    $(".PopularTabs").hide();
                    $(".PopularTabs"+id).show();
                    $('html, body').animate({
                    scrollTop: $(".PopularTabs"+id).offset().top-90
                }, 1000);
            });
            $('.SearchTab').bind('click', function() {
                $('.active').removeClass('active')
                $(this).addClass('active');
            });
        }
        else{
              $(".SearchTab").click(function() {
                var id=$(this).data('id')
                $(".PopularTabs").hide();
                $(".PopularTabs"+id).show();
            });
        }
    </script> 

<style type="text/css">
    #loadMore {
    padding-bottom: 30px;
    padding-top: 30px;
    text-align: center;
    width: 100%;
}
#loadMore a {
    background: #faa635;
    border-radius: 3px;
    color: white;
    display: inline-block;
    padding: 10px 30px;
    transition: all 0.25s ease-out;
    -webkit-font-smoothing: antialiased;
}
#loadMore a:hover {
    background-color: #042a63;
}
</style>
<script>
    $(document).on('click','.optBtn li button',function(){
        $('.optBtn li button').removeClass('active');
        $(this).addClass('active');
    });
    </script>

<script>
    $( document ).ready(function () {
  $(".moreBox").slice(0, 3).show();
    if ($(".blogBox:hidden").length != 0) {
      $("#loadMore").show();
    }   
    $("#loadMore").on('click', function (e) {
      e.preventDefault();
      $(".moreBox:hidden").slice(0, 8).slideDown();
      if ($(".moreBox:hidden").length == 0) {
        $("#loadMore").fadeOut('slow');
      }
    });
  });
</script>

<script>
$(function() {
  $("#popularsection").delay(3000).fadeIn(500);
});
</script>


<?php
    if($this->session->userdata('usersess')) {
        $homeurl = '/';
        $homepage = '/';
        if ($currentpage == $homeurl || $homepage) {
            include_once('modal.php');
        } else {
            include_once('innermodal.php');
        }
    } else {
        $homeurl = '/';
        $homepage = '/';
        if ($currentpage == $homeurl || $homepage) {
            include_once('modal.php');
        } else {
            include_once('innermodal.php');
        }
    }
    include_once('ratingscript.php');
?>

</body>

</html>