<?php
   include_once('header2.php');
   $this->session->set_userdata('previous_url_again', current_url());
   if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {

    } else {
        $link = "https";
        $link .= "://";
        $link .= $_SERVER['HTTP_HOST'];
        $link .= $_SERVER['REQUEST_URI'];
        redirect($link);
    }
   $userSess = $this->session->userdata('usersess');
   if ($this->session->userdata('userfsess')) {
        $userfsess = $this->session->userdata('userfsess');
        $type      = $userfsess['type'];
        $listingTypeFun = "jobs";
    } else {
        $listingTypeFun = "jobs";
    }
?>
<style type="text/css">
  .salaryColor{color:#fbaf3d;}
span.Distance{float:right;background-color:#00a94f;color:#fff;padding:4px 8px;border-radius:5px;font-size:11px;font-family:Roboto;margin:0 -5px 0 0}
.InstantBox{padding:30px}
.InstantBox button{position:absolute;top:-15px;right:-15px;width:30px;height:30px;background-color:#00a94f;opacity:1;text-shadow:none;color:#fff;border-radius:50%;font-size:16px}
.InstantBox h5{font-family:Roboto;font-size:14px;font-weight:400;line-height:25px;text-align:center;color:#000;margin:0 0 10px 0}
.InstantBox h5 span{display:block;font-weight:500}
.InstantBox p{margin:0;text-align:center}
.InstantBox p a{background-color:#00a94f;color:#fff;padding:7px 25px;border-radius:5px;font-family:Roboto;font-size:14px;display:inline-block;box-shadow:none}
.InstantBox p a:hover{background-color:#fbaf31}
.JobListingBox aside{ margin: 0 0 30px 0 }

.datepicker td, 
.datepicker th{ font-size: 17px !important }

.schedulejobgs .forminputspswd p { margin-bottom: 16px; }
.filterchekers.filterchekersnew ul li input{    width: 68px!important;}
.filterchekers input[type="checkbox"] {
    opacity: 0!important;
    z-index: 0!important;
}
.filter_apply {
    color: #fff;
    background: #084d87;
    padding: 8px 20px;
    font-size: 16px;
    border-radius: 6px;
    border: none;
    cursor: pointer;
}
.JobListingBox aside figcaption h1 a{
  display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
}
.inputDetails ul li {
    cursor: pointer;
}

#resetbuttonsort {
    color: #084d87;
    /* background: none; */
    padding: 5px 20px;
    font-size: 14px;
    border-radius: 6px;
    border: none;
    cursor: pointer;
    float:right;
}
.margin_right {
    margin-left: 49px!important;
}
/*.ui-menu.ui-widget.ui-widget-content.ui-autocomplete.ui-front {
    top: 0px!important;
    }*/

.newbuttondesign {
      padding: 10px 8px!important;
      font-size: 10px!important;
      /*background-color: #00a94f!important;*/
      /*color: #fff!important;*/
      border-radius: 5px!important;
      font-family: "Quicksand", sans-serif!important;
      border: 0px!important;
}
.newbuttondesign:hover {
    background-color: #fbaf31!important;
}
.sharebuttoncustom.sharebuttoncustom2 h6 span.number, .sharebuttoncustom.sharebuttoncustom2 h6 span.name {
    font-size: 12px!important;
}
.sharebuttoncustom.sharebuttoncustom2 h6 i {
    font-size: 10px!important;
}

.sharebuttoncustom.sharebuttoncustom2 h6 i {
    color: #000!important;
    background: #fff!important;
    border-radius: 50px!important;
    padding: 2px!important;
}

.newbuttondesign.lightgreen{
  background-color: #0aff2936!important;
  color:#00a94f!important;
  font-weight: 800!important;
}

.newbuttondesign.orangecolor{
  background-color: #fbaf31!important;
  color:#fff!important;
  font-weight: 800!important;
      margin-left: 7px !important;
}
.sharebuttoncustom.sharebuttoncustom2 {
  margin-left: 0px!important;
}

.sharebuttoncustom.sharebuttoncustom2 h6 {
    background: #e5e6e6;
    color: #000;
}


.applijoboader{
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    background-color: #ffffffa3;
    height: 100%;
    z-index: 999;
    text-align: center;
    display: none;
}
.applijoboader img{
  width: 100px;
    display: block;
    margin-top: 22%;
    margin-right: auto;
    margin-left: auto;
}

.addupdatecent p {
    margin: 0!important;
    padding: 3px 0 7px 0!important;
    font-size: 15px!important;
    line-height: 28px!important;
}

.statsusdd .updt {
      color: #fff!important;
    background: #fcbf5b!important;
    /* border: none; */
    margin: 0 10px!important;
    /* border: 1px solid #000; */
    padding: 5px 40px 5px 40px!important;
    font-weight: 900!important;
}
@media screen and (max-width: 500px){
  .newbuttondesign.lightgreen {
        margin-bottom: 10px!important;
  }
  .newbuttondesign.orangecolor{
    display: block;
    margin-left: 0 !important;
  }
}

</style>


<div class="applijoboader">
  <div class="sub_applijoboader" style="height:100%;">
    <img src="https://jobyoda.com/webfiles/newone/loadericon.gif" style="width:100px">
  </div>
</div>

    <section>
        <div class="BannerArea" style="background-image: url('<?php echo base_url();?>webfiles/img/newmap.jpg');">
            <h1> 
                <?php 
                  $getSegment = $this->uri->segment(2);
                  $getSegmentone = $this->uri->segment(3);
                  $getSegmenttwo = $this->uri->segment(4);

                  if($getSegment == "nearby") {
                      echo "Nearby Jobs";
                  } else if($getSegment == "no_experience") {
                     echo "Jobs With No Experience";
                  
                  } else if($getSegment == "nursing") {
                     echo "Nursing Jobs (USRNs,PHRNs)";
                  
                  } else if($getSegment == "actively_hiring") {
                     echo "Jobs With Urgently Hiring <span style='font-size:19px;'> (48-72hrs interview process for work ready candidates)</span>";
                  
                  } else if($getSegment == "hotjob") {
                     echo "Hot Jobs";
                  
                  } else if($getSegment == "instant_screening") {
                     echo "Jobs With Instant Screening";
                  } else if($getSegment == "work_from_home") {
                      echo "Work From Home Jobs";
                  }  else if($getSegment == "14_month_pay") {
                      echo "Jobs With 14th Month Pay";
                  }  else if($getSegment == "day_shift") {
                      echo "Day Shift Jobs";
                  }  else if($getSegment == "free_food") {
                      echo "Jobs With Free Food";
                  }  else if($getSegment == "hmo") {
                      echo "Jobs with Day 1 HMO";
                  }  else if($getSegment == "hmo_dependent") {
                      echo "Jobs with Day 1 HMO for Dependent";
                      
                  }  else if($getSegment == "signing_bonus") {
                      echo "Jobs With Signing Bonus";

                  } else if($getSegment == "Allowance") {
                     echo "Jobs With Allowance";
                  
                  } else if($getSegment == "leadership") {
                     echo "Jobs With Leadership Position";
                  
                  } else if($getSegment == "information_technology") {
                     echo "IT Jobs";
                  } else if($getSegment == "city") {

                     echo "Jobs in ".ucfirst(str_replace('_',' ',$getSegmentone));
                     $getCitypage = $this->Jobpost_Model->singlecity_fetch($getSegmentone);
                     $getCitypage = $getCitypage[0]["name"];

                  } else if($getSegment == "work_from_home") {

                     echo "Work From Home Jobs";
                  
                  } else if($getSegment == "expertise") {
                     echo "Job Listings";
                     $getexpertpage = $this->Jobpost_Model->category_lists_byid($getSegmentone);
                     $getexpertpage = $getexpertpage[0]['category'];
                  }  else {
                      echo "Job Listings";
                  }

                ?>
            </h1>
            <!-- <h2>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores.</h2> -->
        </div>
    </section>

    <section>
       <div class="SearchArea" style="padding: 5px 0 5px;">
          <div class="container">
             <div class="serachHead">
                

                <?php
                    if(strlen($getSegment) > 0) {
                  ?>
                      <form action="<?php echo base_url(); ?>jobs_filter/<?php echo $getSegment; ?>/<?php if(isset($getSegmentone)) { echo $getSegmentone;} ?>/<?php if(isset($getSegmenttwo)) { echo $getSegmenttwo;} ?>" id="myformfilter">
                  <?php
                    } else {
                  ?>
                      <form action="<?php echo base_url(); ?>jobs_filter" id="myformfilter">
                  <?php
                    }
                  ?> 

                <div class="searchForm">
                   <div class="form-group location">
                      <label>Keywords(What)</label>
                      <input type="text" id="myInput" class="form-control FocusShow" name="cname" value="<?php if(isset($filter_values_more)){ echo $filter_values_more['cname']; }elseif($getexpertpage){ echo $getexpertpage; } ?>" autocomplete="off" placeholder="Search By Job Title, Company">
                      
                      <input type="hidden" value="<?php if(isset($filter_values_more)){ echo $filter_values_more['dropdown_cname_selected']; } ?>" name="dropdown_cname_selected" id="dropdown_cname_selected">
                      <input type="hidden" value="28.5355161" name="cur_lat">
                      <input type="hidden" value="77.3910265" name="cur_long">
                      <span class="locationPos"><i class="fa fa-search"></i></span>
      
                   </div>
                   <div class="form-group location">
                      <label>Location(Where)</label>
                      <input type="text" class="form-control locationShow" name="locationn" value="<?php if(isset($filter_values_more)){ echo $filter_values_more['locationn']; } else if(isset($getCitypage)){ echo $getCitypage;} ?>" placeholder="Enter City, Province" id="txtPlaces" autocomplete="off">
                      <input type="hidden" name="lat" id="lati" value="<?php if(isset($filter_values_more)){ echo $filter_values_more['lat']; } ?>">
                      <input type="hidden" name="long" id="longi" value="<?php if(isset($filter_values_more)){ echo $filter_values_more['long']; } ?>">
                      <input type="hidden" name="getcity" value="<?php if(isset($filter_values_more)){ echo $filter_values_more['getcity']; } ?>" id="cityi">
                      <span class="locationPos"><i class="fa fa-map-marker"></i></span>
                      <div class="locationDetail">
                         <ul>
                         <?php
                            if(!empty($citySearchs)) {
                         ?>
                         <?php
                            foreach($citySearchs as $citySearch) {
                              $cityArr[] = $citySearch['cityname'];
                        ?>
                                <li>
                                    <figure>
                                        <a href="javascript:void(0)" onclick='onchangecity("<?php echo $citySearch['cityname']; ?>")'>
                                            <img src="<?php echo $citySearch['image']; ?>">
                                        </a>
                                    </figure>
                                    <figcaption>
                                        <h5 style="font-size: 13px;"> 
                                            <a href="javascript:void(0)" onclick='onchangecity("<?php echo $citySearch['cityname']; ?>")'>
                                                <?php echo $citySearch['cityname']; ?>
                                            </a>
                                        </h5>
                                    </figcaption>

                                </li>
                        <?php
                            }

                            if($allcities) {
                            $arrImg = ["cityimages/old/Baguio.jpg","cityimages/old/Cebu.jpg","cityimages/old/Clark.jpg","cityimages/old/Davao.jpg","cityimages/old/Iloilo.jpg"];
                            foreach($allcities as $allcity) {
                                $flag = 1;
                                foreach($cityArr as $cityAr) {
                                    if (strpos($allcity, $cityAr) !== false) {
                                        $flag = 2;
                                    }
                                }
                                if($flag == 1) {

                                    $getImgRand = array_rand($arrImg);
                    ?>
                                    <li>
                                        <figure>
                                            <a href="javascript:void(0)" onclick='onchangecity("<?php echo $allcity; ?>")'>
                                                <img src="<?php echo base_url(); ?><?php echo $arrImg[$getImgRand]; ?>" style="height: 77px;">
                                            </a>
                                        </figure>
                                        <figcaption>
                                            <h5 style="font-size: 13px;"> 
                                                <a href="javascript:void(0)" onclick='onchangecity("<?php echo $allcity; ?>")'>
                                                    <?php echo $allcity; ?>
                                                </a>
                                            </h5>
                                        </figcaption>

                                    </li>
                    <?php
                                }
                            }
                        }
                         }
                        ?>
                         </ul>
                      </div>
                   </div>
                   <div class="btns">
                      <button type="button" id="searchbuttonajax" class="buttons">Search Jobs</button>
                   </div>
                   <div class="searhDrop">
                      <div class="SearchTop">
                         <h2 class="head1">Job Type</h2>
                         <ul class="searhDropUl">
                         <?php if(!empty($category_list)) {
                            $y=1;   
                            foreach($category_list as $category_lists) {      
                         ?>
                            <li>
                                <div class="Checkbox">
                                    <input type="checkbox" name="jobcategoryy" onchange='onchangecategory("<?php echo $category_lists['category'];?>")' value="<?php echo $category_lists['category'];?>" class="custom-control-input" id="customCheck<?php echo $y.$y;?>" <?php if($filter_values['jobcategory'] == $category_lists['category']){ echo "checked"; }elseif($filter_values_more['jobcategory'] == $category_lists['category']){ echo "checked"; }elseif(isset($getexpert)) { if($getexpert == $category_lists['id']){ echo "checked"; }} ?>>

                                    <label for="customCheck<?php echo $y.$y;?>">
                                        <?php echo $category_lists['category'];?> 
                                    </label>
                                </div>
                            </li>

                         <?php $y++;}}?>
                         
                         </ul>
                         <input type="hidden" >
                      </div>
                      <div class="SearchTop">
                         <h2 class="head1">Job Level</h2>
                         <!-- <ul class="searhDropUl1"> -->
                         <ul class="searhDropUl">
                         <?php
                            if(!empty($level_list)) {
                                $x=1;
                            foreach($level_list as $level_lists) {
                         ?>
                            <li>
                                <div class="Checkbox">
                                    <input type="checkbox" name="joblevell" value="<?php echo $level_lists['id'];?>" onchange='onchangejobtype("<?php echo $level_lists['level'];?>")' class="custom-control-input" id="customCheck<?php echo $x;?>" <?php if($filter_values['joblevel'] == $level_lists['level']){ echo "checked"; }elseif($filter_values_more['joblevel'] == $level_lists['level']){ echo "checked"; } ?>>
                                    
                                    <label for="customCheck<?php echo $x;?>">
                                        <?php echo $level_lists['level'];?> 
                                    </label>
                                </div>
                            </li>
                         
                         <?php $x++;}}?>
                         </ul>
                      </div>
                      <div class="SearchTop">
                         <h2 class="head1">Companies</h2>
                         <div class="owl-carousel owl-theme" id="searchCom">
                         <?php
                            if(!empty($ourSubPartners)) {
                         ?>
                         <?php
                            foreach($ourSubPartners as $ourPartner) {
                         ?>
                            <div class="item">
                               <div class="SearchCompany">
                                  <a href="javascript:void(0)" onclick='onchangecompanytype("<?php echo $ourPartner['cname']; ?>")'>
                                    <figure>
                                      <img src="<?php echo $ourPartner['image']; ?>">
                                    </figure>
                                    <h2 class="head1"><?php echo $ourPartner['cname']; ?></h2>
                                  </a>
                               </div>
                            </div>
                         <?php
                            }
                         ?>
                         <?php
                            }
                         ?>
                            
                         </div>
                      </div>
                   </div>

                </div>

             </div>
          </div>
       </div>
    </section>


    <section>
        <div class="JobListingArea">
            <div class="container">

              <div class="col-sm-4 scrolllbarr" style="position: sticky; top: 91px">
          
               <div class="filterAll boxs">

                  <div class="shortBy">
                     <h2>Sort By <button type="button" id="resetbuttonsort" onclick="sortfunctiondiabled()"> Reset </button></h2>

                     <div class="radioAll">
                        <div class="form-group">
                           <input type="radio" id="Salary" class="filtersorting" name="salarySort" value="1" onclick="sortfunction()">
                           <label for="Salary">Salary</label><br>
                        </div>
                        <div class="form-group margin_right">
                           <input type="radio" id="Location" class="filtersorting" name="salarySort" value="2" onclick="sortfunction()">
                           <label for="Location">Nearby</label><br>
                        </div>

                        <div class="form-group margin_right">
                           <input type="radio" id="resetradio" class="filtersorting" style="opacity:0;" name="salarySort" value="">
                        </div>
                        
                     </div>
                  </div>
                  <hr style="border-top: 1px solid #6edc79; margin: 10px 0 20px 0">

                    <div class="shortBy filterHead">
                       <h2>Filters</h2>
                        <div>
                         <button type="button" id="filterbuttonajax">Apply</button>
                         <!-- <button type="button" id="resetbuttonajax">Reset</button> -->
                         <input type="hidden" name="reset" value="" id="resethidden">
                        </div>
                       <?php
                        $id_array = array();
                        if(isset($filter_values)) {
                          $id_str_arr = $filter_values['jid_array'];
                        } else {
                          if(!empty($hotjobss)) {
                             foreach($hotjobss as $hotjobssDatasss) {   
                                $id_array[] = $hotjobssDatasss['jobpost_id'];
                             }
                             $id_str_arr = implode(',',$id_array);
                          }
                        }
                      ?>
                      <input type="hidden" name="jid_array" value="<?php echo $id_str_arr;?>" id="all_job_id">
                    </div>

                    <div class="FilterScroll">
                      
                      <div class="shortBy boxs">
                       <!--  <h2>Advance Filters</h2> -->
                       
                       <div class="accordian boxs">
                          <div class="set set2 boxs">
                             <a href="javascript:void(0)" class="<?php if(isset($filter_values)){ if($filter_values['basicSalary'] > 0) { echo "active"; } } ?>"><i class="fa fa-minus"></i><span>Salary Range</span></a>
                             <div class="content content2" <?php if(isset($filter_values)){ if($filter_values['basicSalary'] > 0) { echo "style='display: block;'"; } else {}} ?>>
                                <div class="contentAll">
                                   <div class="cont_full boxs">
                                      <input type="checkbox" class="salarycheckbox" id="first1" name="basicSalary" value="5" <?php if(isset($filter_values)){ if($filter_values['basicSalary'] == 5) { echo "checked"; } } ?>>
                                      <label for="first1">0 - 5K</label>
                                   </div>
                                   <div class="cont_full boxs">
                                      <input type="checkbox" class="salarycheckbox" id="first2" name="basicSalary" value="10" <?php if(isset($filter_values)){ if($filter_values['basicSalary'] == 10) { echo "checked"; } } ?>>
                                      <label for="first2">5k - 10K</label>
                                   </div>
                                   <div class="cont_full boxs">
                                      <input type="checkbox" class="salarycheckbox" id="first3" name="basicSalary" value="20" <?php if(isset($filter_values)){ if($filter_values['basicSalary'] == 20) { echo "checked"; } } ?>>
                                      <label for="first3">10K - 20K</label>
                                   </div>
                                   <div class="cont_full boxs">
                                      <input type="checkbox" class="salarycheckbox" id="first4" name="basicSalary" value="30" <?php if(isset($filter_values)){ if($filter_values['basicSalary'] == 30) { echo "checked"; } } ?>>
                                      <label for="first4">20k - 30k</label>
                                   </div>
                                   <div class="cont_full boxs">
                                      <input type="checkbox" class="salarycheckbox" id="first5" name="basicSalary" value="40" <?php if(isset($filter_values)){ if($filter_values['basicSalary'] == 40) { echo "checked"; } } ?>>
                                      <label for="first5">30k - 40k</label>
                                   </div>
                                   <div class="cont_full boxs">
                                      <input type="checkbox" class="salarycheckbox" id="first6" name="basicSalary" value="50" <?php if(isset($filter_values)){ if($filter_values['basicSalary'] == 50) { echo "checked"; } } ?>>
                                      <label for="first6">40k - 50k</label>
                                   </div>
                                   <div class="cont_full boxs">
                                      <input type="checkbox" class="salarycheckbox" id="first7" name="basicSalary" value="70" <?php if(isset($filter_values)){ if($filter_values['basicSalary'] == 70) { echo "checked"; } } ?>>
                                      <label for="first7">50k - 70k</label>
                                   </div>
                                   <div class="cont_full boxs">
                                      <input type="checkbox" class="salarycheckbox" id="first8" name="basicSalary" value="80" <?php if(isset($filter_values)){ if($filter_values['basicSalary'] == 80) { echo "checked"; } } ?>>
                                      <label for="first8">70k - 80k</label>
                                   </div>
                                </div>
                             </div>
                          </div>
                       </div>
                       
                       <div class="accordian boxs">
                          <div class="set set2 boxs">
                             <a href="javascript:void(0)" class="<?php if(isset($filter_values)){ if(strlen($filter_values['toppicks'])>0) { echo "active"; } } ?>"><i class="fa fa-minus"></i><span>Tops Picks</span></a>
                             <div class="content content2" <?php if(isset($filter_values)){ if(count($filter_values['toppicks'])>0) { echo "style='display: block;'"; } else {}} ?>>
                                <div class="contentAll">
                                   <div class="cont_full boxs">
                                      <div class="filterchekers filterchekersnew">
                                         <ul>
                                            <li <?php if(isset($filter_values)){ if(strpos($filter_values['toppicks'], "1") !== false) { echo "class='selectedgreen'"; } else if($getSegment == "signing_bonus"){ echo "class='selectedgreen'"; } } ?>>
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/joining-bonus.png" class="normicos">
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/joining-bonus-1.png" class="hvrsicos">
                                               <input type="checkbox" class="toppicksclass" name="toppicks[]" value="1" <?php if(isset($filter_values)){ if(strpos($filter_values['toppicks'], "1") !== false) { echo "checked"; } else if($getSegment == "signing_bonus"){ echo "checked"; } } ?>>
                                               <p> Joining<br> Bonus</p>
                                            </li>
                                            <li <?php if(isset($filter_values)){ if(strpos($filter_values['toppicks'], "2") !== false) { echo "class='selectedgreen'"; } else if($getSegment == "free_food"){ echo "class='selectedgreen'"; } } ?>>
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/free-food.png" class="normicos">
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/free-food-1.png" class="hvrsicos">
                                               <input type="checkbox" class="toppicksclass" name="toppicks[]" value="2" <?php if(isset($filter_values)){ if(strpos($filter_values['toppicks'], "2") !== false) { echo "checked"; } else if($getSegment == "free_food"){ echo "checked"; } } ?>>
                                               <p> Free<br> Food</p>
                                            </li>
                                            <li <?php if(isset($filter_values)){ if(strpos($filter_values['toppicks'], "3") !== false) { echo "class='selectedgreen'"; } else if($getSegment == "hmo_dependent"){ echo "class='selectedgreen'"; } } ?>>
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/day-1-hmo-for-depended.png" class="normicos">
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/day-1-hmo-for-depended-1.png" class="hvrsicos">
                                               <input type="checkbox" class="toppicksclass" name="toppicks[]" value="3" <?php if(isset($filter_values)){ if(strpos($filter_values['toppicks'], "3") !== false) { echo "checked"; } else if($getSegment == "hmo_dependent"){ echo "checked"; } } ?>>
                                               <p>Day 1 HMO for <br> Dependent</p>
                                            </li>
                                            <li <?php if(isset($filter_values)){ if(strpos($filter_values['toppicks'], "4") !== false) { echo "class='selectedgreen'"; } else if($getSegment == "hmo"){ echo "class='selectedgreen'"; } } ?>>
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/day-1-hmo.png" class="normicos">
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/day-1-hmo-white.png" class="hvrsicos">
                                               <input type="checkbox" class="toppicksclass" name="toppicks[]" value="4" <?php if(isset($filter_values)){ if(strpos($filter_values['toppicks'], "4") !== false) { echo "checked"; } else if($getSegment == "hmo"){ echo "checked"; } } ?>>
                                               <p>Day 1<br> HMO</p>
                                            </li>
                                            <li <?php if(isset($filter_values)){ if(strpos($filter_values['toppicks'], "5") !== false) { echo "class='selectedgreen'"; } else if($getSegment == "day_shift"){ echo "class='selectedgreen'"; } } ?>>
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/day-shift.png" class="normicos">
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/day-shift-1.png" class="hvrsicos">
                                               <input type="checkbox" class="toppicksclass" name="toppicks[]" value="5" <?php if(isset($filter_values)){ if(strpos($filter_values['toppicks'], "5") !== false) { echo "checked"; } else if($getSegment == "day_shift"){ echo "checked"; } } ?>>
                                               <p>Day<br> Shift</p>
                                            </li>
                                            <li <?php if(isset($filter_values)){ if(strpos($filter_values['toppicks'], "6") !== false) { echo "class='selectedgreen'"; } else if($getSegment == "14_month_pay"){ echo "class='selectedgreen'"; } } ?>>
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/14-month-pay.png" class="normicos">
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/14-month-pay-1.png" class="hvrsicos">
                                               <input type="checkbox" class="toppicksclass" id="topic9" name="toppicks[]" value="6" <?php if(isset($filter_values)){ if(strpos($filter_values['toppicks'], "6") !== false) { echo "checked"; } else if($getSegment == "14_month_pay"){ echo "checked"; } } ?>>
                                               <p> 14th Month<br> Pay</p>
                                            </li>
                                            <li <?php if(isset($filter_values)){ if(strpos($filter_values['toppicks'], "7") !== false) { echo "class='selectedgreen'"; } else if($getSegment == "work_from_home"){ echo "class='selectedgreen'"; } } ?>>
                                               <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/wfh.png" class="normicos">
                                               <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/wfhun.png" class="hvrsicos">
                                               <input type="checkbox" class="toppicksclass" id="topic9" name="toppicks[]" value="7" <?php if(isset($filter_values)){ if(strpos($filter_values['toppicks'], "7") !== false) { echo "checked"; } else if($getSegment == "work_from_home"){ echo "checked"; } } ?> >
                                               <p> Work From Home</p>
                                            </li>
                                         </ul>
                                      </div>
                                   </div>
                                </div>
                             </div>
                          </div>
                       </div>
                       
                       <div class="accordian boxs">
                          <div class="set set2 boxs">
                             <a href="javascript:void(0)" class="<?php if(isset($filter_values)){ if(count($filter_values['allowances'])>0) { echo "active"; } } ?>"><i class="fa fa-minus"></i><span>Allowances &amp; Incentives</span></a>
                             <div class="content content2" <?php if(isset($filter_values)){ if(count($filter_values['allowances'])>0) { echo "style='display: block;'"; } else {}} ?>>
                                <div class="contentAll">
                                   <div class="cont_full boxs">
                                      <div class="filterchekers filterchekersnew">
                                         <ul>
                                            <li <?php if(isset($filter_values)){ if(strpos($filter_values['allowances'], "1") !== false) { echo "class='selectedgreen'"; } } ?>>
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/cell-phone-allowance.png" class="normicos">
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/cell-phone-allowance-1.png" class="hvrsicos">
                                               <input type="checkbox" class="toppicksclass" name="allowances[]" value="1" <?php if(isset($filter_values)){ if(strpos($filter_values['allowances'], "1") !== false) { echo "checked"; } } ?>>
                                               <p> Cell Phone</p>
                                            </li>
                                            <li <?php if(isset($filter_values)){ if(strpos($filter_values['allowances'], "2") !== false) { echo "class='selectedgreen'"; } } ?>>
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/annual--performance-bonus.png" class="normicos">
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/annual--performance-bonus-1.png" class="hvrsicos">
                                               <input type="checkbox" class="toppicksclass" name="allowances[]" value="2" <?php if(isset($filter_values)){ if(strpos($filter_values['allowances'], "2") !== false) { echo "checked"; } } ?>>
                                               <p> Annual</p>
                                            </li>
                                            <li <?php if(isset($filter_values)){ if(strpos($filter_values['allowances'], "3") !== false) { echo "class='selectedgreen'"; } } ?>>
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/free-parking.png" class="normicos">
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/free-parking-1.png" class="hvrsicos">
                                               <input type="checkbox" class="toppicksclass" name="allowances[]" value="3" <?php if(isset($filter_values)){ if(strpos($filter_values['allowances'], "3") !== false) { echo "checked"; } } ?>>
                                               <p>Free <br>Parking</p>
                                            </li>
                                            <li <?php if(isset($filter_values)){ if(strpos($filter_values['allowances'], "4") !== false) { echo "class='selectedgreen'"; } } ?>>
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/free-shuttle.png" class="normicos">
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/free-shuttle-1.png" class="hvrsicos">
                                               <input type="checkbox" class="toppicksclass" name="allowances[]" value="4" <?php if(isset($filter_values)){ if(strpos($filter_values['allowances'], "4") !== false) { echo "checked"; } } ?>>
                                               <p> Free <br> Shuttle</p>
                                            </li>
                                            <li <?php if(isset($filter_values)){ if(strpos($filter_values['allowances'], "5") !== false) { echo "class='selectedgreen'"; } } ?>>
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/reteirment-benifits.png" class="normicos">
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/reteirments-benefits-1.png" class="hvrsicos">
                                               <input type="checkbox" class="toppicksclass" name="allowances[]" value="5" <?php if(isset($filter_values)){ if(strpos($filter_values['allowances'], "5") !== false) { echo "checked"; } } ?>>
                                               <p> Retirement <br> Benefits</p>
                                            </li>
                                            <li <?php if(isset($filter_values)){ if(strpos($filter_values['allowances'], "6") !== false) { echo "class='selectedgreen'"; } } ?>>
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/transportation.png" class="normicos">
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/transportation-1.png" class="hvrsicos">
                                               <input type="checkbox" class="toppicksclass" name="allowances[]" value="6" <?php if(isset($filter_values)){ if(strpos($filter_values['allowances'], "6") !== false) { echo "checked"; } } ?>>
                                               <p> Transport <br> Allowance</p>
                                            </li>
                                            <li <?php if(isset($filter_values)){ if(strpos($filter_values['allowances'], "7") !== false) { echo "class='selectedgreen'"; } } ?>>
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/monthly-performance-incentive.png" class="normicos">
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/monthly-performance-incentive-1.png" class="hvrsicos">
                                               <input type="checkbox" class="toppicksclass" name="allowances[]" value="7" <?php if(isset($filter_values)){ if(strpos($filter_values['allowances'], "7") !== false) { echo "checked"; } } ?>>
                                               <p> Monthly Performance <br>Incentives</p>
                                            </li>
                                         </ul>
                                      </div>
                                   </div>
                                </div>
                             </div>
                          </div>
                       </div>
                       
                       <div class="accordian boxs">
                          <div class="set set2 boxs">
                             <a href="javascript:void(0)" class="<?php if(isset($filter_values)){ if(count($filter_values['medical'])>0) { echo "active"; } } ?>"><i class="fa fa-minus"></i><span>Medical Benefits</span></a>
                             <div class="content content2" <?php if(isset($filter_values)){ if(count($filter_values['medical'])>0) { echo "style='display: block;'"; } else {}} ?>>
                                <div class="contentAll">
                                   <div class="cont_full boxs">
                                      <div class="filterchekers filterchekersnew">
                                         <ul>
                                            <li <?php if(isset($filter_values)){ if(strpos($filter_values['medical'], "1") !== false) { echo "class='selectedgreen'"; } } ?>>
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/medicine-reimbursemer.png" class="normicos">
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/medicine-reimbursemer-1.png" class="hvrsicos">
                                               <input type="checkbox" class="toppicksclass" name="medical[]" value="1" <?php if(isset($filter_values)){ if(strpos($filter_values['medical'], "1") !== false) { echo "checked"; } } ?>>
                                               <p> Medicine <br>Reimbursement</p>
                                            </li>
                                            <li <?php if(isset($filter_values)){ if(strpos($filter_values['medical'], "2") !== false) { echo "class='selectedgreen'"; } } ?>>
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/free-hmo-for-dependents.png" class="normicos">
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/free-hmo-for-dependents-1.png" class="hvrsicos">
                                               <input type="checkbox" class="toppicksclass" name="medical[]" value="2" <?php if(isset($filter_values)){ if(strpos($filter_values['medical'], "2") !== false) { echo "checked"; } } ?>>
                                               <p> Free HMO for Dependents </p>
                                            </li>
                                            <li <?php if(isset($filter_values)){ if(strpos($filter_values['medical'], "3") !== false) { echo "class='selectedgreen'"; } } ?>>
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/critical-illness-benefits.png" class="normicos">
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/critical-illness-benefits1.png" class="hvrsicos">
                                               <input type="checkbox" class="toppicksclass" name="medical[]" value="3" <?php if(isset($filter_values)){ if(strpos($filter_values['medical'], "3") !== false) { echo "checked"; } } ?>>
                                               <p>Critical Illness <br>Benefits</p>
                                            </li>
                                            <li <?php if(isset($filter_values)){ if(strpos($filter_values['medical'], "4") !== false) { echo "class='selectedgreen'"; } } ?>>
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/life-insurence.png" class="normicos">
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/life-insurence-1.png" class="hvrsicos">
                                               <input type="checkbox" class="toppicksclass" name="medical[]" value="4" <?php if(isset($filter_values)){ if(strpos($filter_values['medical'], "4") !== false) { echo "checked"; } } ?>>
                                               <p> Life<br> Insurance</p>
                                            </li>
                                            <li <?php if(isset($filter_values)){ if(strpos($filter_values['medical'], "5") !== false) { echo "class='selectedgreen'"; } } ?>>
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/maternity-assistance.png" class="normicos">
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/maternity-assistance-1.png" class="hvrsicos">
                                               <input type="checkbox" class="toppicksclass" name="medical[]" value="5" <?php if(isset($filter_values)){ if(strpos($filter_values['medical'], "5") !== false) { echo "checked"; } } ?>>
                                               <p>Maternity <br>Assistance</p>
                                            </li>
                                         </ul>
                                      </div>
                                   </div>
                                </div>
                             </div>
                          </div>
                       </div>

                       <div class="accordian boxs">
                          <div class="set set2 boxs">
                             <a href="javascript:void(0)" class="<?php if(isset($filter_values)){ if(count($filter_values['workshift'])>0) { echo "active"; } } ?>"><i class="fa fa-minus"></i><span>Work Shifts/Schedule</span></a>
                             <div class="content content2" <?php if(isset($filter_values)){ if(count($filter_values['workshift'])>0) { echo "style='display: block;'"; } else {}} ?>>
                                <div class="contentAll">
                                   <div class="cont_full boxs">
                                      <div class="filterchekers filterchekersnew">
                                         <ul>
                                            <li <?php if(isset($filter_values)){ if(strpos($filter_values['workshift'], "1") !== false) { echo "class='selectedgreen'"; } } ?>>
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/mid-shift.png" class="normicos">
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/mid-shift-1.png" class="hvrsicos">
                                               <input type="checkbox" class="toppicksclass" name="workshift[]" value="1" <?php if(isset($filter_values)){ if(strpos($filter_values['workshift'], "1") !== false) { echo "checked"; } } ?>>
                                               <p> Mid <br>Shift</p>
                                            </li>
                                            <li <?php if(isset($filter_values)){ if(strpos($filter_values['workshift'], "2") !== false) { echo "class='selectedgreen'"; } } ?>>
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/night-shift.png" class="normicos">
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/night-shift-1.png" class="hvrsicos">
                                               <input type="checkbox" class="toppicksclass" name="workshift[]" value="2" <?php if(isset($filter_values)){ if(strpos($filter_values['workshift'], "2") !== false) { echo "checked"; } } ?>>
                                               <p> Night <br>Shift</p>
                                            </li>
                                            <li <?php if(isset($filter_values)){ if(strpos($filter_values['workshift'], "3") !== false) { echo "class='selectedgreen'"; } } ?>>
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/24.png" class="normicos">
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/24-1.png" class="hvrsicos">
                                               <input type="checkbox" class="toppicksclass" name="workshift[]" value="3" <?php if(isset($filter_values)){ if(strpos($filter_values['workshift'], "3") !== false) { echo "checked"; } } ?>>
                                               <p>24/7</p>
                                            </li>
                                            <li <?php if(isset($filter_values)){ if(strpos($filter_values['leaves'], "41") !== false) { echo "class='selectedgreen'"; } } ?>>
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/weekend-off.png" class="normicos">
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/weekend-off-1.png" class="hvrsicos">
                                               <input type="checkbox" class="toppicksclass" name="leaves[]" value="41" <?php if(isset($filter_values)){ if(strpos($filter_values['leaves'], "41") !== false) { echo "checked"; } } ?>>
                                               <p> Weekends<br> Off</p>
                                            </li>
                                            <li <?php if(isset($filter_values)){ if(strpos($filter_values['leaves'], "42") !== false) { echo "class='selectedgreen'"; } } ?>>
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/holiday-off.png" class="normicos">
                                               <img src="<?php echo base_url(); ?>webfiles/img/toppics/holiday-off-1.png" class="hvrsicos">
                                               <input type="checkbox" class="toppicksclass" name="leaves[]" value="42" <?php if(isset($filter_values)){ if(strpos($filter_values['leaves'], "42") !== false) { echo "checked"; } } ?>>
                                               <p>Holidays <br>Off</p>
                                            </li>
                                         </ul>
                                      </div>
                                   </div>
                                </div>
                             </div>
                          </div>
                       </div> 
                       <div class="clear"></div>
                      </div>
                      <div class="clear"></div>
                    </div>

               </div>
              </div>

              
              </form>


              <div class="col-sm-8">
                   <div class="JobListingLeft">
                      <div class="JobListingBox">
                      <?php 
                          if(!empty($hotjobss)) {
                            $x=1;
                            
                            foreach($hotjobss as $hotjobssData) {   

                              if($hotjobssData['mode'] == "call" || $hotjobssData['mode'] == "Call") {
                                  $modeText = "OVER THE PHONE INTERVIEW";
                              } else if($hotjobssData['mode'] == "Walk-in") {
                                  $modeText = "WALK IN INTERVIEW";
                              } else if($hotjobssData['mode'] == "Instant screening") {
                                  $modeText = "INSTANT SCREENING";
                              }

                              $shareurl = base_url()."job/description/". base64_encode($hotjobssData['jobpost_id']);
                              $shareargs = ["url"=>$shareurl, "title"=>$hotjobssData['job_title'], "image"=>$hotjobssData['job_image'], "desc"=>$hotjobssData['job_title'], "redirecturl"=>$shareurl];

                              $sharelinks = withShareLinks($shareargs);
                      ?>
                          <aside class="lazy">
                              <?php
                                      if($this->session->userdata('usersess')) {
                                          if($hotjobssData['savedjob']==1) {
                                              $title="Saved";
                                  ?>
                                              <span id="test4<?php echo $hotjobssData['jobpost_id'];?>" onclick="savedjob('<?php echo $hotjobssData['jobpost_id']?>')" class="Wishlist"> <i class="fa fa-heart" title="<?php echo $title;?>"></i> </span>
                                  <?php
                                          } if($hotjobssData['savedjob']==0) {
                                  ?>
                                              <span id="test4<?php echo $hotjobssData['jobpost_id'];?>" onclick="savedjob('<?php echo $hotjobssData['jobpost_id']?>')" class="Wishlist"> <i class="fa fa-heart" title="<?php echo $title;?>" style="color:#b5b5b5;"></i> </span>    
                                  <?php
                                          }
                                      }
                                  ?>
                              <figure>
                                
                                <?php 
                                    if($hotjobssData['actively'] == 1) {
                                ?>

                                  <img src="<?php echo base_url().'webfiles/newone/images/Actively_Job.png';?>" style="position: absolute;top: 10px;left: 10px;width: 40px;">
                                  
                                <?php } ?>

                                  <a href="<?php  if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($hotjobssData['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($hotjobssData['jobpost_id']); }?>">
                                  <?php
                                      if(!empty($hotjobssData['job_image'])) {
                                  ?>
                                          <img src="<?php echo $hotjobssData['job_image'];?>" > 
                                  <?php
                                      } else {
                                  ?>
                                          <img src="<?php echo base_url().'webfiles/';?>img/user_man.png">
                                  <?php
                                      }
                                  ?>
                                      
                                  </a>
                                  
                              </figure>
                              <figcaption>

                                  <h1>
                                      <a href="<?php  if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($hotjobssData['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($hotjobssData['jobpost_id']); }?>"><?php echo $hotjobssData['job_title'];?>  | <span class="salaryColor"><?php echo $hotjobssData['salary']; ?> Monthly</span> | <?php echo $modeText; ?></a>
                                  </h1>
                                  
                                  <h2 class="company_with_distance">
                                      <a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($hotjobssData['comapnyId']);}else{ echo base_url();?>site_details/<?php echo base64_encode($hotjobssData['comapnyId']); }?>"><?php echo $hotjobssData['companyName']; ?></a>

                                      <span class="Distance"><i class="fa fa-map-marker"></i> <?php echo $hotjobssData['distance'];?> KM</span>
                                  </h2>
                                  
                                  <h3>
                                      <a href="<?php if (!empty($userSess)) { echo base_url();?>company_details/<?php echo base64_encode($hotjobssData['recruiter_id']);}else{ echo base_url();?>company_details/<?php echo base64_encode($hotjobssData['recruiter_id']); }?>"> <?php echo $hotjobssData['cname'];?></a>
                                  </h3>
                                  
                                  <h4>
                                      <span><i class="fa fa-map" aria-hidden="true"></i></span>
                                      <?php echo $hotjobssData['companyAddress']; ?>
                                  </h4>
                                  
                                  <h5>
                                      <?php echo substr($hotjobssData['jobPitch'],0, 100); ?>
                                  </h5>

                                  <ul>
                                      <?php
                                          if(!empty($hotjobssData['toppicks1'])) {
                                              echo $getToppickByFunction = getToppickFunction($hotjobssData['toppicks1']);
                                          }
                                          if(!empty($hotjobssData['toppicks2'])) {
                                              echo $getToppickByFunction = getToppickFunction($hotjobssData['toppicks2']);
                                          }
                                          if(!empty($hotjobssData['toppicks3'])) {
                                              echo $getToppickByFunction = getToppickFunction($hotjobssData['toppicks3']);
                                          }
                                      ?>
                                  </ul>
                              <div class="newShareBtns">

                                <?php
                                  if($this->session->userdata('usersess')) {
                                    $recruiterdetail = $this->Jobpost_Model->job_detailLocation_fetch($hotjobssData['comapnyId']);
                                  }
                             ?>
                                <form id="formschedule<?php echo $hotjobssData['jobpost_id']?>">

                                  <input type="hidden" id="dayfromm<?php echo $hotjobssData['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayfrom'];?>">
                                  <input type="hidden" id="daytoo<?php echo $hotjobssData['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayto'];?>">
                                  <input type="hidden" id="timefromm<?php echo $hotjobssData['jobpost_id']?>" value="<?php echo $timeFromm;?>">
                                  <input type="hidden" id="timetoo<?php echo $hotjobssData['jobpost_id']?>" value="<?php echo $timeToo;?>">
                                  <input type="hidden" name="scheduledate">
                                  <input type="hidden" name="scheduletime"> 
                                  <input type="hidden" name="listing" value="<?php echo $hotjobssData['jobpost_id'];?>">
                                  <input type="hidden" name="type" value="<?php echo $x;?>">

                                  <p class="demoP">

                                    <a class="newbuttondesign lightgreen" href="<?php  if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($hotjobssData['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($hotjobssData['jobpost_id']); }?>"> View More Details  </a>

                                  <?php
                                      if($this->session->userdata('usersess')) {

                                          if($hotjobssData['mode'] == "Instant screening") {
                                  ?>
                                              <a href="javascript:void(0)" class="newbuttondesign orangecolor" data-toggle="modal" data-target="#exampleModalCenterInstant<?php echo $x;?>">Apply <?php if($hotjobssData['mode'] == "Call") { echo "for Phone Screening"; } else { echo 'for '. $hotjobssData['mode']; } ?>  </a>
                                  <?php
                                          } else {
                                  ?>

                                              <button type="button" class="ApplyJob newbuttondesign orangecolor" onclick='scheduleclick("<?php echo $hotjobssData['jobpost_id']?>", "<?php echo $x;?>")' id="schedule<?php echo $hotjobssData['jobpost_id']?>"> <?php if($hotjobssData['chatbot'] == 0) {  if($hotjobssData['mode'] == "Call") { echo "Apply for Phone Screening"; } else { echo 'Apply for '. $hotjobssData['mode']; } } else { echo "Continue Chatbot Interview"; } ?> </button>
                                  <?php
                                          }
                                      } else {
                                        
                                          if($hotjobssData['mode'] == "Instant screening") {
                                  ?>
                                              <a href="<?php echo base_url();?>login" class="newbuttondesign orangecolor">Apply <?php if($hotjobssData['mode'] == "Call") { echo "for Phone Screening"; } else { echo 'for '. $hotjobssData['mode']; } ?>  </a>
                                  <?php
                                          } else {
                                  ?>
                                              <a href="<?php echo base_url();?>login" class="newbuttondesign orangecolor"><?php if($hotjobssData['chatbot'] == 0) {  if($hotjobssData['mode'] == "Call") { echo "Apply for Phone Screening"; } else { echo 'Apply for '. $hotjobssData['mode']; } } else { echo "Continue Chatbot Interview"; } ?> </a>
                                  <?php
                                          }
                                  
                                      }
                                  ?>
                                  
                                  </form>

                                  </p>

                                  <div class="sharebuttoncustom sharebuttoncustom2">
                                      
                                      <h6>
                                          <span class="number"><?php echo $hotjobssData['sharecount'];?></span><span class="name">Shares</span><i class="fa fa-share-alt"></i></h6>

                                      <div class="sharelinkclass sharelinkclass22">
                                        <ul>
                                            <li><a onclick='sharecountajax("<?php echo $hotjobssData['jobpost_id']; ?>", "<?php echo $sharelinks['facebook']; ?>")' href="javascript:void(0)" title="Facebook Share"><img src="<?php echo base_url();?>webfiles/newone/social/facebook.png"></a></li>

                                            <li> <a onclick='sharecountajax("<?php echo $hotjobssData['jobpost_id']; ?>", "<?php echo $sharelinks['twitter']; ?>")' href="javascript:void(0)" title="Twitter Share"><img src="<?php echo base_url();?>webfiles/newone/social/twitter.png"></a></li>

                                            <li> <a onclick='sharecountajax("<?php echo $hotjobssData['jobpost_id']; ?>", "<?php echo $sharelinks['linkedin']; ?>")' href="javascript:void(0)" title="LinkedIn Share"><img src="<?php echo base_url();?>webfiles/newone/social/linkedin.png"></a></li>

                                            <li> <a onclick='sharecountajax("<?php echo $hotjobssData['jobpost_id']; ?>", "<?php echo $sharelinks['gmail']; ?>")' href="javascript:void(0)" title="Gmail Share"><img src="<?php echo base_url();?>webfiles/newone/social/gmail.png"></a></li>
                                            
                                            <li> <a onclick='sharecountajax("<?php echo $hotjobssData['jobpost_id']; ?>", "<?php echo $sharelinks['whatsapp']; ?>")' href="javascript:void(0)" title="Whatsapp Share"><img src="<?php echo base_url();?>webfiles/newone/social/whatsapp.png"></a></li>
                                            
                                        </ul>
                                      </div>

                                   </div>

                                  </div>
                              </figcaption>
                          </aside>
                      <?php
                          if($this->session->userdata('usersess')) {
                             $recruiterdetail = $this->Jobpost_Model->job_detailLocation_fetch($hotjobssData['comapnyId']);
                             $timeFrom = $recruiterdetail[0]['from_time'];
                             $timeFromm = date('H:i', strtotime($timeFrom));
                             $timeTo = $recruiterdetail[0]['to_time'];
                             $timeToo = date('H:i', strtotime($timeTo));
                      ?>
                          <div class="modal fade" id="exampleModalCenterInstant<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                               <div class="modal-dialog modal-dialog-centered" role="document">
                                  <div class="modal-content" style="box-shadow: none;min-height: 300px!important;">

                                    <div class="InstantBox">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">×</span>
                                        </button>
                                        <h5>Job search made easier right!? You will now begin the Instant Assessment of <?php echo $hotjobssData['name']; ?>. They are looking for people like you but will only know about you once you finish the assessment. Those who finish the assessment quickly, are more likely to get hired! Good Luck from the JobYoDA team!</h5>
                                        <p> Best of luck from the JobYoDA team! </p>
                                        
                                        <p>
                                          <a class="greenbtn" onclick="instantfunction('<?php echo $hotjobssData['jobpost_id']; ?>', '<?php echo $hotjobssData['modeurl']; ?>')" href="javascript:void(0)" style="margin-top: 30px;"> Begin Screening </a>
                                        </p>
                                    </div>
                                  
                                  </div>
                               </div>
                          </div>

                          <div class="modal fade" id="exampleModalCenter100<?php echo $hotjobssData['jobpost_id']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                              <div class="modal-dialog modal-dialog-centered" role="document" style="width: 37%;">
                                 <div class="modal-content" style="min-height: 384px!important;">
                                    <div class="modal-header"> 
                                       <button type="button" class="close updt" data-dismiss="modal" aria-label="Close">
                                       <span aria-hidden="true">×</span>
                                       </button>
                                    </div>
                                    <div class="modal-body">
                                       <div class="formmidaress modpassfull">
                                          <div class="filldetails">
                                             <form>
                                                <div class="addupdatecent">
                                                   <p style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>All the best from the JobYoDA Team!</p> 
                                                  <p style='text-align:center;color:#fbaf31;line-height:28px;margin-bottom:0px;font-weight:500;'>Did you know JobYoDA offers a FREE Venti Coffee to all hires?</p> 
                                                  <p style='text-align:center;color:#fbaf31;line-height:28px;margin-bottom:0px;font-weight:500;'>To claim, all you need to do is:</p> 
                                                  <p style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>1. Declare JobYoDA as the source of your application</p>
                                                  <p style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>2. Send us and email at Help@jobyoda.com to claim your voucher if you get hired</p>
                                                  <p style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>3. We will validate with the recruiter and process</p>
                                                  <p style='text-align:center;color:#00a94f;;line-height:28px;margin-bottom:0px;font-weight:500;'>Good Luck!</p>
                                                   <div class="statsusdd"  style="display: inherit; float: none;">
                                                      <!-- <button type="button" class="updt" id="okup" data-dismiss="modal" aria-label="Close">Ok</button> -->

                                                      <?php if($hotjobssData['chatbot'] == 0) { ?>
                                                     <button  type="button" class="updt" id="okup" data-dismiss="modal" aria-label="Close">Ok</button>
                                                     <?php } else { ?>
                                                     <center style="margin-bottom: 0px"><a href="<?php echo base_url();?>chatbot/<?php echo base64_encode($hotjobssData['jobpost_id']);?>" style="text-decoration: none;" class=" chatbotlink" id="okup">Continue Chat Interview</a></center>
                                                     <?php } ?>

                                                   </div>
                                                </div>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>

                           <div class="modal fade" id="exampleModalCenter900<?php echo $hotjobssData['jobpost_id']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                              <div class="modal-dialog modal-dialog-centered" role="document">
                                 <div class="modal-content" style="min-height: 300px!important;">
                                    <div class="modal-header"> 
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                       <span aria-hidden="true">×</span>
                                       </button>
                                    </div>
                                    <div class="modal-body">
                                       <div class="formmidaress modpassfull">
                                          <div class="filldetails">
                                             <form>
                                                <div class="addupdatecent">
                                                   <img src="<?php echo base_url('webfiles/img/savedbighover.png')?>">
                                                   <p class="jobsuccess1<?php echo $hotjobssData['jobpost_id']?>"></p>
                                                   <div class="statsusdd">
                                                   </div>
                                                </div>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>

                      <?php
                          }
                      ?>
                      <?php
                              $x++;
                              }
                          } else {
                      ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <center><img src="<?php echo base_url();?>webfiles/newone/images/32323.png" style="width:80%"></center>
                                </div>
                            </div>
                      <?php
                          }
                      ?>
                      </div>
                  </div>
              </div>

            </div>
        </div>
    </section>

    <?php
    include_once('footer1.php');
?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>recruiterfiles/css/jquery.timepicker.min.css" />
<script type="text/javascript" src="<?php echo base_url(); ?>recruiterfiles/js/jquery.timepicker.min.js"></script> 
        
        <script>
       $('.scrolllbarr').scroll(function () {

    if ($('.scrolllbarr').scrollTop() >= 115) {
      $('.filterHead button').addClass('active');
//        alert('okk');
    } else {
 $('.filterHead button').removeClass('active');
    }
});
        </script>
<script type="text/javascript">
   $(function () {
      $(".datetimepicker1").datepicker({ 
       dateFormat: "dd/mm/yy", 
        yearRange: '1900:2020', 
        defaultDate: '',
        autoclose: true,
     })
   });
   $('.timepicker').timepicker({ 'timeFormat': 'H:i' });
    
</script>

<script>
   function savedjob(id) {
         $.ajax({
            'type' :'POST',
            'url' :"<?php echo base_url('dashboard/savedjovData') ?>",
            'data' :'jobId='+id,
            'success':function(htmlres)
            {
              console.log(htmlres);
               if(htmlres !='')
               {
               if(htmlres == 1)
               {
                  //$("#test4"+id).hide();
                  $("#test4"+id).html('<i class="fa fa-heart" title="Saved" style="color:#00a94f;"></i>');
                  var msg="Job saved successfully";
                  //myFunction(msg);
               }
               if(htmlres == 2)
               {
                 //$("#test3"+id).hide();
                 $("#test4"+id).html('<i class="fa fa-heart" title="Unsaved" style="color:#b5b5b5;"></i>');
               }
               if(htmlres == 3)
               {
                 //$("#test4"+id).hide();
                 $("#test4"+id).html('<i class="fa fa-heart" title="Saved" style="color:#00a94f;"></i>');
                 var msg="Job saved successfully";
                 //myFunction(msg);
               }
            }
            }
              
          });
      }
   
      function instantfunction(id, url) {
          $.ajax({
             'type' :'POST',
             'url' :"<?php echo base_url('dashboard/savedinstantData') ?>",
             'data' :'jobId='+id,
             'success':function(htmlres) {
                  $(".modal").modal('hide');
                 window.open(url, '_blank');
             }
         });
   }
   
   // function myFunction(msg) {
   //  var x = document.getElementById("snackbar");
   //  x.className = "show";
   //  $("#snackbar").html('<div class="alert alert-success">'+msg+ '</div>');
   //  setTimeout(function(){ x.className = x.className.replace("show", "top"); }, 1000);
   //   }
</script>
    

<script>
  
  function scheduleclick(id, id1) {

      $(".applijoboader").css("display","block");

      var getform = "#formschedule" + id;
      var successmsg = ".jobsuccess"+id;
      var successmsg1 = ".jobsuccess1"+id;
      var successmodal = "#exampleModalCenter100"+id;
      var errormodal = "#exampleModalCenter900"+id;
      var closeModal = "#exampleModalCenterc"+id1;

      $.ajax({
          'type' :'POST',
          'url' :"<?php echo base_url('dashboard/scheduled') ?>",
          'data' :$(getform).serialize(),
          success:function(response) {

            console.log(response);

            $(".applijoboader").css("display","none");

            var jsonData = JSON.parse(response);
              
            if(jsonData.status == "SUCCESS") {

              if(jsonData.chatbot == 0) {
              
                $(successmsg).html(jsonData.message);
                $(successmodal).modal('show');
                $(closeModal).modal('hide');
              
              } else {

                  window.location = "<?php echo base_url();?>chatbot/"+jsonData.jobid;
              }

            } else {
                $(successmsg1).html(jsonData.message);
                $(errormodal).modal('show');
                $(closeModal).modal('hide');

            }
          }
      });
  }

  function scheduleclickmore(id, id1) {
      var getform = "#formschedulemore" + id;
      var successmsg = ".jobsuccess"+id;
      var successmsg1 = ".jobsuccess1"+id;
      var successmodal = "#exampleModalCenter100"+id;
      var errormodal = "#exampleModalCenter900"+id;
      var closeModal = "#exampleModalCenterc"+id1;

      $.ajax({
          'type' :'POST',
          'url' :"<?php echo base_url('dashboard/scheduled') ?>",
          'data' :$(getform).serialize(),
          success:function(response) {

            var jsonData = JSON.parse(response);
            
                if(jsonData.status == "SUCCESS") {

                  if(jsonData.chatbot == 0) {
                 
                      $(successmsg).html(jsonData.message);
                      $(successmodal).modal('show');
                      $(closeModal).modal('hide');
                  } else {

                      window.location = "<?php echo base_url();?>chatbot/"+jsonData.jobid;      
                  }

                } else {
                    $(successmsg1).html(jsonData.message);
                    $(errormodal).modal('show');
                    $(closeModal).modal('hide');
                }
          }
      });
  }

</script>

<script type="text/javascript">
  $(document).ready(function() {
   $(".updt").on("click",function(){
         window.location = "<?php echo base_url(); ?>appliedjobs";
   });
  });
</script>



</body>
<script>
document.addEventListener("DOMContentLoaded", function loadAfterDomContent() {
    let lazytime;
    const hidden = document.querySelectorAll(".hidden");
    const lazySrcSet = Array.from(document.querySelectorAll(".lazy"));
    const lazyImgs = Array.from(document.querySelectorAll(".lazy"));

// Array.from converts HTML collection to array for splice method needed for removal of 404 imgs
   
    function laziness() {
        if (lazytime) {
            clearTimeout(lazytime);
        }

        lazytime = setTimeout(function setLazyTimeout() {
            
          const yOffset = window.pageYOffset;

            const lazyLoadImgs = (img) => {
              if (img.offsetTop < (window.innerHeight + yOffset)) {
                    img.classList.remove('lazy');
                    img.classList.add('lazyloaded');
                }
            }
                
            lazySrcSet.forEach(function srcsetLazyLoad(source, index) {
// check img src for 404s and removes from array to prevent continuous 404 errors - works outside of codepen 
/* 
              const srcUrl = source.dataset.src;
              const testImg = new Image();
              testImg.src = srcUrl;
              const testImgHeight = testImg.height; 
              if(testImgHeight === 0){
                lazySrcSet.splice(index, 1); 
              } 
 */
                if (source.offsetTop < (window.innerHeight + yOffset)) {
                    source.srcset = source.dataset.src;                   
                    lazyLoadImgs(source);
                }
            });

            lazyImgs.forEach(function lazyImgTagCheck(img, index) {        
// check img src for 404s and removes from array to prevent continuous 404 errors - works outside of codepen
/*
              const srcUrl = img.dataset.src;
              const checkSrc = new Image();
              checkSrc.src = srcUrl;
              const heightCheck = checkSrc.height;
              if(heightCheck === 0){
                lazyImgs.splice(index,1);
              } 
*/
                if (img.offsetTop < (window.innerHeight + yOffset)) {
                    img.src = img.dataset.src;                 
                    lazyLoadImgs(img);
                }
            });

            hidden.forEach(function showHiddenContent(elm) {
                if (elm.offsetTop < (window.innerHeight + yOffset)) {
                    elm.classList.remove('hidden');
                    elm.classList.add('visible');
                }
            });

            if (lazyImgs.length === 0 && lazySrcSet === 0 && hidden === 0) {
                document.removeEventListener("scroll", laziness);
                window.removeEventListener("resize", laziness);
                window.removeEventListener("orientationChange", laziness);
                window.removeEventListener("transitionend", laziness);
            }
        }, 10);
    }

    window.addEventListener("DOMContentLoaded", laziness);
    document.addEventListener("scroll", laziness);
    window.addEventListener("resize", laziness);
    window.addEventListener("orientationChange", laziness);
    window.addEventListener("transitionend", laziness);

});
    </script>


<script type="text/javascript">
  function sharecountajax(jobid, url) {
    //console.log(url);

    console.log();
    $.ajax({
        type: 'POST',
        url: "<?php echo base_url(); ?>user/savesharecount",
        data: {job_id:jobid},
        success: function (result) {

            window.open(
              url,
              '_blank'
            );
        }
    });
  }
</script>

<script type="text/javascript">
    $(".filterchekers li").click(function(){
      $(this).toggleClass("selectedgreen");  
    });

    $(document).ready(function() {
        $(".salarycheckbox").on('change', function() {
            $(".salarycheckbox").not(this).prop('checked', false);
        });
    });
</script>

<script type="text/javascript">
  $(document).ready(function(){
    $("#filterbuttonajax").on("click", function(e) {

      e.preventDefault();
        $.ajax({
            type: 'post',
            url: $("#myformfilter").attr("action"),
            data: $("#myformfilter").serialize(),
            beforeSend: function() {
              $(".JobListingLeft").html('<div class="popularloader"><center><img src="https://jobyoda.com/webfiles/newone/loadericon.gif" style="margin-top:200px;width:60px"></center></div>');
            },
            success: function (result) {
                //console.log(result);
                $(".JobListingLeft").html(result);
            }
        });
    });
  });
  $(document).ready(function(){
    $("#searchbuttonajax").on("click", function(e) {
      e.preventDefault();

        $(".salarycheckbox").prop('checked', false);
        $(".filtersorting").prop('checked', false);
        $(".toppicksclass").prop('checked', false);
        $(".filterchekers li").removeClass("selectedgreen");

        $.ajax({
            type: 'post',
            url: $("#myformfilter").attr("action"),
            data: $("#myformfilter").serialize(),
            beforeSend: function() {
              $(".JobListingLeft").html('<div class="popularloader"><center><img src="https://jobyoda.com/webfiles/newone/loadericon.gif" style="margin-top:200px;width:60px"></center></div>');
            },
            success: function (result) {
                $(".JobListingLeft").html(result);
            }
        });
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){
    $("#resetbuttonajax").on("click", function(e) {
        e.preventDefault();

        $("#resethidden").val("reset");

        $.ajax({
            type: 'post',
            url: $("#myformfilter").attr("action"),
            data: $("#myformfilter").serialize(),
            beforeSend: function() {
              $(".JobListingLeft").html('<div class="popularloader"><center><img src="https://jobyoda.com/webfiles/newone/loadericon.gif" style="margin-top:200px;width:60px"></center></div>');
            },
            success: function (result) {
                //console.log(result);
                $(".JobListingLeft").html(result);
            }
        });
    });
  });
</script>
<script type="text/javascript">
  function sortfunction(vall) {

      $.ajax({
          type: 'post',
          url: $("#myformfilter").attr("action"),
          data: $("#myformfilter").serialize(),
          beforeSend: function() {
            $(".JobListingLeft").html('<div class="popularloader"><center><img src="https://jobyoda.com/webfiles/newone/loadericon.gif" style="margin-top:200px;width:60px"></center></div>');
          },
          success: function (result) {
              //console.log(result);
              $(".JobListingLeft").html(result);
          }
      });
  }

  function sortfunctiondiabled() {
    $("#resethidden").val("");
      
    $("#Salary").attr("checked", "false");
    $("#Salary").prop("checked", "false");
    $("#Location").attr("checked", "false");
    $("#Location").prop("checked", "false");
    $("#resetradio").prop("checked", "true");
    $("#resetradio").attr("checked", "true");

      $.ajax({
          type: 'post',
          url: $("#myformfilter").attr("action"),
          data: $("#myformfilter").serialize(),
          beforeSend: function() {
            $(".JobListingLeft").html('<div class="popularloader"><center><img src="https://jobyoda.com/webfiles/newone/loadericon.gif" style="margin-top:200px;width:60px"></center></div>');
          },
          success: function (result) {
              //console.log(result);
              $(".JobListingLeft").html(result);
          }
      });
  }
</script>

<?php
    function getToppickFunction($toppickID) {

        if($toppickID == 1) {
                                                            
            return '<li><figure><img src="'.base_url() .'recruiterfiles/images/m_bonus.png"></figure> Joining Bonus</li>';

        } else if($toppickID == 2) {

            return '<li><figure><img src="'.base_url() .'recruiterfiles/images/m_freefood.png"></figure> Free Food</li>';

        } else if($toppickID == 3) {

            return '<li><figure><img src="'.base_url() .'recruiterfiles/images/m_day_1_hmo.png"></figure> Day 1 HMO</li>';

        } else if($toppickID == 4) {

            return '<li><figure><img src="'.base_url() .'recruiterfiles/images/m_dependent_hmo.png"></figure> Day 1 HMO for Dependent</li>';

        } else if($toppickID == 5) {

            return '<li><figure><img src="'.base_url() .'recruiterfiles/images/m_dayshift.png"></figure> Day Shift</li>';

        } else if($toppickID == 6) {

            return '<li><figure><img src="'.base_url() .'recruiterfiles/images/m_14th_pay.png"></figure> 14th Month Pay </li>';
        
        } else if($toppickID == 7) {

            return '<li><figure><img src="'. base_url(). 'webfiles/newone/images/Searches-2.png"></figure> Work From Home </li>';
        
        } else {
            return "";
        }                                                 
    }

    function withShareLinks($args) {
      $url = urlencode($args['url']);
      $title = urlencode($args['title']);
      $image = urlencode($args['image']);
      $desc = urlencode($args['desc']);
      $redirect_url = urlencode($args['redirecturl']);
      $text = $title;
      
      if($desc) {
        $text .= '%20%3A%20'; # This is just this, " : "
        $text .= $desc;
      }
      
        // conditional check before arg appending
      
      return [
        'facebook'=>'http://www.facebook.com/sharer.php?u=' . $url . '&title=' . $title, 
        'gmail'=>'https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=&su=' . $title . '&body=' . $url,
        'linkedin'=>'https://www.linkedin.com/shareArticle?mini=true&url=' . $url.'&title=' . $title.'&source=jobyooda',
        'twitter'=>'https://twitter.com/intent/tweet?url=' . $url . '&text=' . $text,
        'whatsapp'=>'https://api.whatsapp.com/send?text=' . $text . '%20' . $url,
      ];
    }
?>
<script>
$(document).on('click','.sharebuttoncustom h6',function(){
  $(this).siblings('.sharelinkclass').slideToggle();
});
</script>
<script>
document.addEventListener("mousedown", function (event) {
    if (event.target.closest(".sharebuttoncustom h6,.sharelinkclass"))
        return;
    $('.sharelinkclass').slideUp();
});
    </script>