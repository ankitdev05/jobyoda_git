<?php
    include_once('header2.php');
    // if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {
    //     $requestURI = $_SERVER['REQUEST_URI'];
    //     if($requestURI == "/user/index") {
    //         redirect("https://jobyoda.com/");    
    //     }
    // } else {
    //     $link = "https";
    //     $link .= "://";
    //     $link .= $_SERVER['HTTP_HOST'];
    //     $link .= $_SERVER['REQUEST_URI'];
    //     redirect($link);
    // }

    $userSess = $this->session->userdata('usersess'); 
    if ($this->session->userdata('userfsess')) {
        $userfsess = $this->session->userdata('userfsess');
        $type      = $userfsess['type'];
        $listingTypeFun = "jobs";
    } else {
        $listingTypeFun = "jobs";
    }
?>
<style>
    .salaryColor{color:#fbaf3d;}
    .JobArea .tab-content .tab-pane{min-height: 150px!important;}
    .JobArea .tab-content .tab-pane .nofound{font-size: 20px!important;} 

    .JobArea .tab-content .tab-pane .nofound{
        border: 1px solid #ddd;
    text-align: center;
    padding: 40px 0;
    margin: 40px 0 30px 0;
    border-radius: 7px;
    text-transform: capitalize;
    font-family: Roboto;
    font-weight: 600;
    font-size: 30px !important;
    color: #000;
}



</style>

<!-- <div class='Loader'>
    <div class="Circle"></div>
</div> -->

<section>
        <div class="BannerArea" style="background-image: url('https://jobyoda.com/webfiles/img/newmap.jpg');">
            <h1> 
                EXPLORE JOBS
            </h1>
        </div>
    </section>
    
    
    <section>
        <div class="JobArea" id="popularsection">
            <div class="container">
                <h1 class="Title"> Popular Searches</h1>
                <div class="JobHead">
                    <h2>BPO jobs with best benefits in the Philippines that everyone is searching for</h2>
                    <ul class="nav nav-tabs">
                        
                        <li class="active">
                            <a data-toggle="tab" class="SearchTab" data-id="15" href="#Actively" onclick="changehtmlfunction(15)">
                                <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Mask_Group_21.png"></span>
                                Actively Hiring Jobs
                            </a>
                        </li> 

                        <li class="">
                            <a data-toggle="tab" class="SearchTab" data-id="1" href="#Hot" onclick="changehtmlfunction(1)">
                                <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Searches-1.png"></span>
                                Hot Jobs
                            </a>
                        </li> 

                        <li>
                            <a data-toggle="tab" class="SearchTab" data-id="9" href="#nearby" onclick="changehtmlfunction(9)">
                                <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Searches-3.png"></span>
                                Nearby Jobs
                            </a>
                        </li> 

                        <li>
                            <a data-toggle="tab" class="SearchTab" data-id="13" href="#noexperience" onclick="changehtmlfunction(13)">
                                <span><img src="<?php echo base_url().'webfiles/';?>newone/images/no_experience_B_W.png"></span>
                                Jobs with No Experience
                            </a>
                        </li>
                        
                        <li>
                            <a data-toggle="tab" class="SearchTab" data-id="2" href="#WorkHome" onclick="changehtmlfunction(2)">
                                <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Searches-2.png"></span>
                                Work From Home Jobs
                            </a>
                        </li>

                        <li>
                            <a data-toggle="tab" class="SearchTab" data-id="3" href="#Screening" onclick="changehtmlfunction(3)">
                                <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Rocket.png" style="filter: inherit;"></span>
                                Jobs with Instant Screening 
                            </a>
                        </li> 
                        
                        <li>
                            <a data-toggle="tab" class="SearchTab" data-id="4" href="#HMO" onclick="changehtmlfunction(4)">
                                <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Searches-6.png"></span>
                                Jobs with Day 1 HMO 
                            </a>
                        </li>

                        <li>
                            <a data-toggle="tab" class="SearchTab" data-id="14" href="#noexperience" onclick="changehtmlfunction(14)">
                                <span><img src="<?php echo base_url().'webfiles/';?>newone/images/nursing_jobs_B_W.png"></span>
                                Jobs with Nursing
                            </a>
                        </li>
                        
                        <li>
                            <a data-toggle="tab" class="SearchTab" data-id="5" href="#Food" onclick="changehtmlfunction(5)">
                                <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Searches-7.png"></span>
                                Jobs with Free Food 
                            </a>
                        </li> 
                        
                        <li>
                            <a data-toggle="tab" class="SearchTab" data-id="6" href="#IT" onclick="changehtmlfunction(6)">
                                <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Searches-9.png"></span>
                                IT Jobs  
                            </a>
                        </li> 
                        
                        <li>
                            <a data-toggle="tab" class="SearchTab" data-id="7" href="#Leadership" onclick="changehtmlfunction(7)">
                                <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Searches-10.png"></span>
                                Leadership Position 
                            </a>
                        </li>
                        
                        <li>
                            <a data-toggle="tab" class="SearchTab" data-id="8" href="#Jobs" onclick="changehtmlfunction(8)">
                                <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Searches-4.png"></span>
                                Jobs with 14th Month Pay 
                            </a>
                        </li>

                        <li>
                            <a data-toggle="tab" class="SearchTab" data-id="10" href="#bonus" onclick="changehtmlfunction(10)">
                                <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Searches-8.png"></span>
                                Jobs with Signing Bonus
                            </a>
                        </li>

                        <li>
                            <a data-toggle="tab" class="SearchTab" data-id="11" href="#dayshift" onclick="changehtmlfunction(11)">
                                <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Searches-5.png"></span>
                                Dayshift Jobs
                            </a>
                        </li>

                        <li>
                            <a data-toggle="tab" class="SearchTab" data-id="12" href="#day1hmodependent" onclick="changehtmlfunction(12)">
                                <span><img src="<?php echo base_url().'webfiles/';?>img/toppics/day-1-hmo-for-depended-1.png"></span>
                                Jobs with Day 1 HMO for Dependent
                            </a>
                        </li>

                    </ul>
                </div>

                <div class="JobBody">
                    <div class="tab-content">
                        <div id="Actively" class="PopularTabs PopularTabs1 tab-pane fade in active">
                            <h1>Actively Hiring Jobs   <?php if(count($hotJobs) > 0) { ?> <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/actively_hiring">view all <i class="fa fa-angle-double-right"></i></a> <?php } ?> </h1>
                            <?php
                                if(!empty($hotJobs)) {
                            ?>
                            <div class="owl-carousel owl-theme JobSlider">
                            <?php
                                    $e=1;
                                    foreach($hotJobs as $hotJob) {
                                        if($hotJob['mode'] == "call" || $hotJob['mode'] == "Call") {
                                            $modeText = "OVER THE PHONE INTERVIEW";
                                        } else if($hotJob['mode'] == "Walk-in") {
                                            $modeText = "WALK IN INTERVIEW";
                                        } else if($hotJob['mode'] == "Instant screening") {
                                            $modeText = "INSTANT SCREENING";
                                        }
                                        if($e<=6){
                            ?>
                                        <div class="item">
                                            <div class="JobsBox">
                                                <figure>
                                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($hotJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($hotJob['jobpost_id']); }?>">
                                                        <?php
                                                            if(!empty($hotJob['job_image'])) {
                                                        ?>
                                                                <img src="<?php echo $hotJob['job_image']; ?>">
                                                        <?php
                                                            } else {
                                                        ?>
                                                                <img src="<?php echo base_url().'webfiles/';?>img/user_man.png">
                                                        <?php
                                                            }
                                                        ?>
                                                        <!-- <span class="Logo"><img src="images/Company-2.png"></span> -->
                                                        <!-- <span class="Distance"><i class="fa fa-map-marker"></i> <?php echo $hotJob['distance']; ?> KM</span> -->
                                                    </a>
                                                </figure>
                                                <figcaption>
                                                    <span class="Distance"><i class="fa fa-map-marker"></i> <?php echo $hotJob['distance']; ?> KM</span>
                                                    <h3>
                                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($hotJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($hotJob['jobpost_id']); }?>">
                                                            <?php echo $hotJob['job_title']; ?> | <span class="salaryColor"><?php echo $hotJob['salary']; ?> Monthly</span> | <?php echo $modeText; ?>
                                                        </a>
                                                    </h3>
                                                    <h4><a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($hotJob['comapnyId']);}else{ echo base_url();?>site_details/<?php echo base64_encode($hotJob['comapnyId']); }?>"><?php echo $hotJob['companyName']; ?></a></h4>
                                                    
                                                    <h5>
                                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>company_details/<?php echo base64_encode($hotJob['recruiter_id']);}else{ echo base_url();?>company_details/<?php echo base64_encode($hotJob['recruiter_id']); }?>">

                                                            <?php echo $hotJob['cname']; ?>
                                                        </a>
                                                    </h5>
                                                    
                                                    <p><?php echo substr($hotJob['jobPitch'], 0,45).'...'; ?></p> 
                                                    <ul>
                                                        <?php
                                                            if(!empty($hotJob['toppicks1'])) {
                                                                echo $getToppickByFunction = getToppickFunction($hotJob['toppicks1']);
                                                            }
                                                            if(!empty($hotJob['toppicks2'])) {
                                                                echo $getToppickByFunction = getToppickFunction($hotJob['toppicks2']);
                                                            }
                                                            if(!empty($hotJob['toppicks3'])) {
                                                                echo $getToppickByFunction = getToppickFunction($hotJob['toppicks3']);
                                                            }
                                                        ?>
                                                    </ul>
                                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($hotJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($hotJob['jobpost_id']); }?>" class="GetJob">Get this job</a> 
                                                </figcaption>
                                            </div>
                                        </div>
                            <?php
                                        } else {
                                            break;
                                        }
                                        $e++;
                                    }
                            ?>    
                            </div>
                            <?php
                                } else {
                                    echo "<p class='nofound'> No job found </p>";
                                }
                            ?>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>


<?php
    function getToppickFunction($toppickID) {

        if($toppickID == 1) {
                                                            
            return '<li><img src="'.base_url() .'recruiterfiles/images/m_bonus.png"> Joining Bonus</li>';

        } else if($toppickID == 2) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_freefood.png"> Free Food</li>';

        } else if($toppickID == 3) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_day_1_hmo.png"> Day 1 HMO</li>';

        } else if($toppickID == 4) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_dependent_hmo.png"> Day 1 HMO for Dependent</li>';

        } else if($toppickID == 5) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_dayshift.png"> Day Shift</li>';

        } else if($toppickID == 6) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_14th_pay.png"> 14th Month Pay </li>';
        
        } else if($toppickID == 7) {

            return '<li><img src="'. base_url(). 'webfiles/newone/images/Searches-2.png"> Work From Home </li>';
        
        } else {
            return "";
        }                                                 
    }
?>

<script>
    function changehtmlfunction(id) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>homepage/fetchpopularsearch",
            data:'keyword='+id,
            beforeSend: function() {
              $(".tab-content").html('<div class="popularloader"><center><img src="https://jobyoda.com/webfiles/newone/loadericon.gif"></center></div>');
            },
            success: function(data) {
                
                $(".tab-content").html(data);
                var owl = $(".JobSlider");
                owl.owlCarousel({
                    margin: 0,
                    smartSpeed: 1000,
                    autoplay: 5000, 
                    nav: true,
                    dots:false,
                    autoplayHoverPause: true,
                    navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
                    loop: true,
                    responsive: {
                        0: {
                            items: 1,
                        },
                        600: {
                            items: 2,
                        },
                        1000: {
                            items: 3,
                        },
                    },
                });
            },
            error: function (error_) {
                console.log(error_);
            }
        });
    }
</script>
<?php
    include_once('footer1.php');
?>