<?php include ('header.php'); ?>
<style type="text/css">
   input[type="checkbox"] {
      opacity: 1!important;
      position: relative!important;
      height: 25px!important;
      width: 19px!important;
   }   
</style>

<div class="content custom-scrollbar">
   <div class="doc data-table-doc page-layout simple full-width">
      <!-- HEADER -->
      <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
         <!-- APP TITLE -->
         <div class="col-12 col-sm">
            <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
               <div class="logo-icon mr-3 mt-1">
                  <i class="icon s-6 icon-person-box"></i>
               </div>
               <div class="logo-text">
                  <div class="h4">Jobs for Job Match</div>
                  <div class="">Total: <?php if ($jobArray) { echo count($jobArray); } ?></div>
               </div>
            </div>
         </div>
         <!-- / APP TITLE -->
         <div class="col-auto">
            <!-- <a href="<?php //echo base_url(); ?>administrator/recruiter/addRecruiter" class="btn btn-light">APPLY</a> -->
            <button type="button" data-toggle="modal" data-target="#exampleModal" id="send_btn" class="btn btn-light" disabled="">APPLY</button>
         </div>
      </div>
      <!-- / HEADER -->
      <!-- CONTENT -->
      <div class="page-content p-6">
         <div class="content container">
            <div class="row">
               <div class="col-12">
                  <div class="example ">
                     <div class="source-preview-wrapper">
                        <div class="preview">
                           <div class="preview-elements reculsting pdrct">
                              <table id="sample-data-table" class="table">

                                 <thead>
                                    <tr>
                                       
                                       <th class="secondary-text">
                                          <div class="table-header">
                                             <span class="column-title">SNo</span>
                                          </div>
                                       </th>
                                       <th class="secondary-text">
                                          <div class="table-header">
                                             <span class="column-title">  </span>
                                          </div>
                                       </th>
                                       <th class="secondary-text">
                                          <div class="table-header">
                                             <span class="column-title">Job ID</span>
                                          </div>
                                       </th>
                                       <th class="secondary-text">
                                          <div class="table-header">
                                             <span class="column-title">Job Title</span>
                                          </div>
                                       </th>
                                       <th class="secondary-text">
                                          <div class="table-header">
                                             <span class="column-title">Job Category</span>
                                          </div>
                                       </th>
                                       <th class="secondary-text">
                                          <div class="table-header">
                                             <span class="column-title">Job Sub-Category</span>
                                          </div>
                                       </th>
                                       <th class="secondary-text">
                                          <div class="table-header">
                                             <span class="column-title">Company Name</span>
                                          </div>
                                       </th>
                                       <th class="secondary-text">
                                          <div class="table-header">
                                             <span class="column-title">Site Name</span>
                                          </div>
                                       </th>

                                       <th class="secondary-text">
                                          <div class="table-header">
                                             <span class="column-title">Address</span>
                                          </div>
                                       </th>
                                       <th class="secondary-text">
                                          <div class="table-header">
                                             <span class="column-title">Openings</span>
                                          </div>
                                       </th>
                                       <th class="secondary-text">
                                          <div class="table-header">
                                             <span class="column-title">Salary</span>
                                          </div>
                                       </th>
                                       <th class="secondary-text">
                                          <div class="table-header">
                                             <span class="column-title">Experience</span>
                                          </div>
                                       </th>

                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php
                                       $x1 = 1;
                                       foreach ($jobArray as $List) {
                                    ?>
                                    <tr>
                                       <td><?php echo $x1; ?></td>
                                       <td> <input type="checkbox" name="jobids" id="checkid" class="checkmap" onclick="checkfunction(<?php echo $List['id']; ?>)" value="<?php echo $List['id']; ?>"> </td>

                                       <td><?php echo $List['id']; ?></td>
                                       <td><?php echo $List['jobTitle']; ?></td>
                                       <td><?php echo $List['category']; ?></td>
                                       <td><?php echo $List['subcategory']; ?></td>
                                       <td><?php echo $List['company_name']; ?></td>
                                       <td><?php echo $List['site_name']; ?></td>
                                       <td><?php echo $List['address']; ?></td>
                                       <td><?php echo $List['opening']; ?></td>
                                       <td><?php echo $List['salary']; ?></td>
                                       <td><?php echo $List['experience']; ?></td>
                                    </tr>
                                    <?php
                                          $x1++;
                                       }
                                    ?>
                                 </tbody>

                              </table>
                              <script type="text/javascript">
                                 //$('#sample-data-table').DataTable();

                                 // $(document).ready(function() {
                                 //    // Setup - add a text input to each footer cell
                                 //    $('#sample-data-table tfoot th').each( function () {
                                 //       var title = $(this).text();
                                 //       $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
                                 //    });

                                 //    var table = $('#sample-data-table').DataTable({

                                 //         initComplete: function () {
                                 //             // Apply the search
                                 //             this.api().columns().every( function () {
                                 //                 var that = this;
                                  
                                 //                 $( 'input', this.footer() ).on( 'keyup change clear', function () {
                                 //                     if ( that.search() !== this.value ) {
                                 //                         that
                                 //                             .search( this.value )
                                 //                             .draw();
                                 //                     }
                                 //                 } );
                                 //             } );
                                 //         }
                                 //    });
                                 // });
                                 $(document).ready(function() {
                                    // Setup - add a text input to each footer cell
                                    $dd=1;
                                    $('#sample-data-table thead th').each( function () {
                                       var title = $(this).text();
                                       
                                       if($dd == 4 || $dd == 5 || $dd == 6 || $dd == 12) {
                                          $(this).html( title+'<input type="text" placeholder="Search '+title+'" />' );
                                       } else {
                                          $(this).html( title);
                                       }

                                       $dd++;
                                    });

                                    var table = $('#sample-data-table').DataTable({
                                          "pageLength": 50,
                                          initComplete: function () {
                                             // Apply the search
                                             this.api().columns().every( function () {
                                                 var that = this;
                                  
                                                 $( 'input', this.header() ).on( 'keyup change clear', function () {
                                                     if ( that.search() !== this.value ) {
                                                         that.search( this.value ).draw();
                                                     }
                                                 } );
                                             } );
                                         }
                                    });
                                 });
                              </script>
                           </div>
                        </div>
                        <div class="source custom-scrollbar">
                           <div class="highlight">
                              <pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-html" data-lang="html"></code></pre>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CONTENT -->
   </div>
</div>
</div>
   <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
   </div>
</div>
<nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
</nav>
</main>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Job Apply</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         Are you sure, you want to apply for this jobs?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
         <form method="post" action="<?php echo base_url(); ?>administrator/jobpost/applyjob">
            <input type="hidden" name="jobids" value="" id="getcheckid">
            <input type="hidden" name="userid" value="<?php echo $userid;?>">
            <button type="submit" class="btn btn-primary">Save changes</button>   
         </form>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
   function checkfunction($id) {
      var checkid = $id;
      var appendcheckid = '';

      $('.checkmap:checkbox:checked').each(function () {
            var sThisVal = (this.checked ? $(this).val() : "");

            if(appendcheckid.length > 0) {
                  $('#send_btn').prop('disabled', false);
                  appendcheckid = sThisVal +","+appendcheckid;
            
            } else if(appendcheckid.length < 0) {
                  $('#send_btn').prop('disabled', true);
                  appendcheckid = '';
            
            } else {
                  appendcheckid = sThisVal;
                  $('#send_btn').prop('disabled', false);
            }
      });
      
      $("#getcheckid").val(appendcheckid);
   }
</script>



</body>
</html>

