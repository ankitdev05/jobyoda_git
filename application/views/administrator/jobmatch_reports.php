<?php include('header.php');?>
<style type="text/css">
     div.dataTables_wrapper {
max-width: 1300px;
overflow-x: scroll;
}
.custompagination a {
    padding: 10px;
    background-color: chartreuse;
    margin-left: 10px;
    font-size: 18px;
}
.custompagination strong {
    padding: 10px;
    background-color: chartreuse;
    margin-left: 10px;
    font-size: 18px;
}
table#sample-data-tabletest thead th, table#sample-data-tabletest thead td {
    border-bottom: 1px solid rgba(0,0,0,0.12);
    font-size: 13px;
}
table#sample-data-tabletest tbody td {
    padding: 16px 3px;
    font-size: 12px;
}
.exportclasscss{
      float: right;
    background: #000;
    color: #fff;
    padding: 7px 14px;
}
</style>
<div class="content custom-scrollbar">
   <div class="doc data-table-doc page-layout simple full-width">
      <!-- HEADER -->
      <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
         <!-- APP TITLE -->
         <div class="col-12 col-sm">
            <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
               <div class="logo-icon mr-3 mt-1">
                  <i class="icon-chart-bar-stacked s-6"></i>
               </div>
               <div class="logo-text">
                  <div class="h4">Report Management</div>
               </div>
            </div>
         </div>
         <!-- / APP TITLE -->
      </div>
      <!-- / HEADER -->
      <!-- CONTENT -->
      <div class="page-content p-6">
         <div class="content container">
            <div class="row">
               <div class="col-12">
                  <div class="example scrollmobusr">
                     <div class="source-preview-wrapper" style="overflow-x: scroll; width: 412%;">
                        <div class="preview">
                           <div class="preview-elements dsymgnd">
                              <!-- JAVASCRIPT BEHAVIOR -->
                              <ul class="nav nav-tabs" id="myTab" role="tablist">
                                 <li class="nav-item">
                                    <a class="nav-link  <?php if($checkactive == "tab1") { ?> active <?php } ?>" id="day-tab" data-toggle="tab" href="#day" role="tab" aria-controls="day" aria-expanded="true">Daily</a>
                                 </li>
                                 <li class="nav-item">
                                    <a class="nav-link  <?php if($checkactive == "tab2") { ?> active <?php } ?>" id="weekly-tab" data-toggle="tab" href="#weekly" role="tab" aria-controls="weekly">Weekly</a>
                                 </li>
                                 <li class="nav-item">
                                    <a class="nav-link  <?php if($checkactive == "tab3") { ?> active <?php } ?>" id="monthly-tab" data-toggle="tab" href="#monthly" role="tab" aria-controls="monthly">Monthly</a>
                                 </li>
                                 <li class="nav-item">
                                    <a class="nav-link  <?php if($checkactive == "tab4") { ?> active <?php } ?>" id="yearly-tab" data-toggle="tab" href="#yearly" role="tab" aria-controls="yearly">Yearly</a>
                                 </li>
                                 <li class="nav-item">
                                    <a class="nav-link   <?php if($checkactive == "tab5") { ?> active <?php } ?>" id="date-tab" data-toggle="tab" href="#date" role="tab" aria-controls="date">DateWise</a>
                                 </li>
                              </ul>

                              <div class="tab-content" id="myTabContent">

                                 <div class="tab-pane fade  <?php if($checkactive == "tab1") { ?> show active <?php } ?>" id="day" role="tabpanel" aria-labelledby="day-tab">
                                    <h3>Daily Report    ( <?php echo $resultTotal1; ?>) |  Page: <?php echo $pagenum = $this->uri->segment(4)/10+1; ?></h3>
                                    <!-- Export Data --> 
                                    <a class="exportclasscss" href="<?php echo base_url(); ?>administrator/jobpost/export_csv_report_hiring?tabtype=daywise" target="_blank">Export</a>
                                    <br><br>

                                    <form method="post">
                                      <div class="row">
                                        
                                        <div class="col-md-6">
                                          <select name="status_dropdown" id="status_dropdown_yearly" onchange="this.form.submit()">
                                             <option value="">Select Status</option>
                                             <option value="10" <?php if($searchStatus == "10"){ echo "selected"; }?>>All</option>
                                             <option value="1" <?php if($searchStatus == "1"){ echo "selected"; }?>>New Application</option>
                                             <option value="2" <?php if($searchStatus == "2"){ echo "selected"; }?>>No Show</option>
                                             <option value="3" <?php if($searchStatus == "3"){ echo "selected"; }?>>Fall Out</option>
                                             <option value="4" <?php if($searchStatus == "4"){ echo "selected"; }?>>Refer</option>
                                             <option value="5" <?php if($searchStatus == "5"){ echo "selected"; }?>>On Going Application</option>
                                             <option value="6" <?php if($searchStatus == "6"){ echo "selected"; }?>>Accepted JO</option>
                                             <option value="7" <?php if($searchStatus == "7"){ echo "selected"; }?>>Hired</option>
                                          </select>

                                          <select name="sort_dropdown" id="status_dropdown_sort" onchange="this.form.submit()">
                                             <option value="">Sort Admin Applied</option>
                                             <option value="high_to_low" <?php if($searchSort == "high_to_low"){ echo "selected"; }?>> High To Low </option>
                                             <option value="low_to_high" <?php if($searchSort == "low_to_high"){ echo "selected"; }?>> Low To High </option>
                                          </select>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="input-group mb-3">
                                              <input type="text" name="searchKeyword" class="form-control" placeholder="Search by keyword..." value="<?php echo $searchKeyword; ?>">
                                              <div class="input-group-append">
                                                  <input type="hidden" name="tabtype" value="daywise">
                                                  <input type="submit" name="submitSearch" class="btn btn-outline-secondary" value="Search">
                                                  <input type="submit" name="submitSearchReset" class="btn btn-outline-secondary" value="Reset">
                                              </div>
                                          </div>  
                                        </div>

                                      </div>
                                    </form>

                                    <table id="sample-data-table" class="table">
                                      <thead>
                                         <tr>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Co.Name</span>
                                               </div>
                                            </th>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Site</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Admin Applied</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Id</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Posted By</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Title</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Interview Mode</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Category</span>
                                               </div>
                                            </th>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Sub-Category</span>
                                               </div>
                                            </th>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Candidate ID</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Candidate Name</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Candidate Email</span>
                                               </div>
                                            </th>
                                        
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Candidate Phone</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Experience (Year)</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Experience (Month)</span>
                                               </div>
                                            </th>
                                            
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Education</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Level</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Industry</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Superpower</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Specialization</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Sub Specialization</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Work Mode</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Vaccination</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Relocate</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Platform</span>
                                               </div>
                                            </th>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Search status</span>
                                               </div>
                                            </th>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Last company</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">App Version</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">25 MBPS Internet</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Applied Date</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Salary Offer</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Total Guaranteed Allowance</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Latest Status</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Last Update</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Fall Out Reason</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Hired Date</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Candidate Last Managed By</span>
                                               </div>
                                            </th>
                                            
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Chatbot</span>
                                               </div>
                                            </th>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Chatbot Result</span>
                                               </div>
                                            </th>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Match</span>
                                               </div>
                                            </th>

                                         </tr>
                                      </thead>

                                      <tbody>
                                        <?php 
                                            $sno = $row+1;
                                            foreach($result1 as $data1) {

                                              echo "<tr>";
                                              echo "<td>".$data1['recruiter_cname']."</td>";
                                              echo "<td>".$data1['company_name']."</td>";
                                              echo "<td> <a href='".base_url()."administrator/admin_applied/".$data1['user_id']."' style='color:green'><b>".$data1['adminjobcount']."</b></a></td>";
                                              echo "<td>".$data1['job_id']."</td>";
                                              echo "<td>".$data1['posted_by']."</td>";
                                              echo "<td>".$data1['job_title']."</td>";
                                              echo "<td>".$data1['mode']."</td>";
                                              echo "<td>".$data1['category']."</td>";
                                              echo "<td>".$data1['subcategory']."</td>";
                                              echo "<td>".$data1['user_id']."</td>";
                                              echo "<td>".$data1['user_name']."</td>";
                                              echo "<td>".$data1['user_email']."</td>";
                                              echo "<td>".$data1['user_phone']."</td>";
                                              echo "<td>".$data1['user_exp_year']."</td>";
                                              echo "<td>".$data1['user_exp_month']."</td>";
                                              echo "<td>".$data1['education']."</td>";
                                              echo "<td>".$data1['joblevel']."</td>";
                                              echo "<td>".$data1['industry']."</td>";
                                              echo "<td>".$data1['superpower']."</td>";
                                              echo "<td>".$data1['specialization']."</td>";
                                              echo "<td>".$data1['sub_specialization']."</td>";
                                              echo "<td>".$data1['work_mode']."</td>";
                                              echo "<td>".$data1['vaccination']."</td>";
                                              echo "<td>".$data1['relocate']."</td>";
                                              echo "<td>".$data1['platform']."</td>";
                                              echo "<td>".$data1['jobsearch_status']."</td>";
                                              echo "<td>".$data1['last_company']."</td>";
                                              echo "<td>".$data1['app_version']."</td>";
                                              echo "<td>".$data1['internetspeed']."</td>";
                                              echo "<td>".$data1['created_at']."</td>";
                                              echo "<td>".$data1['basic_salary']."</td>";
                                              echo "<td>".$data1['allowances']."</td>";
                                              echo "<td>".$data1['status']."</td>";
                                              echo "<td>".$data1['apply_date']."</td>";
                                              echo "<td>".$data1['fallout']."</td>";
                                              echo "<td>".$data1['hired_date']."</td>";
                                              echo "<td>".$data1['managed_by']."</td>";
                                              echo "<td>".$data1['chatbot']."</td>";
                                              echo "<td>".$data1['chat_result']."</td>";
                                              echo "<td> <a href='".base_url()."administrator/job_matching/".$data1['job_id']."/".$data1['user_id']."'  class='btn btn-warning' target='_blank'> View </a> </td>";
                                              echo "</tr>";
                                              $sno++;

                                            }
                                            if(count($result1) == 0){
                                              echo "<tr>";
                                              echo "<td colspan='3'>No record found.</td>";
                                              echo "</tr>";
                                            }
                                          ?>
                                      </tbody>
                                    </table>

                                    <!-- Paginate -->
                                    <?php if(count($result1) == 0) { 
                                    ?>
                                        <div class="custompagination" style='margin-top: 10px;text-align: center;'>
                                          <a href="<?php echo $pagination1['base_url']; ?>/<?php echo $pagination1['show_link'] - 10; ?>"><< Previous</a>
                                       </div>

                                    <?php

                                    } else { ?>
                                     <div class="custompagination" style='margin-top: 10px;text-align: center;'>
                                      <?php if($pagination1['show_link'] == 0) {} else { ?>
                                        <a href="<?php echo $pagination1['base_url']; ?>/<?php echo $pagination1['show_link'] - 10; ?>"><< Previous</a>
                                      <?php } ?>

                                      <?php if(count($result1) >= 10) { ?>
                                        <a href="<?php echo $pagination1['base_url']; ?>/<?php echo $pagination1['show_link'] + 10; ?>">Next >></a>
                                      <?php } ?>
                                     </div>

                                    <?php } ?>
                                 </div>

                                 <div class="tab-pane fade <?php if($checkactive == "tab2") { ?> show active <?php } ?>" id="weekly" role="tabpanel" aria-labelledby="weekly-tab">
                                    <h3>Weekly Report   ( <?php echo $resultTotal2; ?>) |  Page: <?php echo $pagenum = $this->uri->segment(4)/10+1; ?> </h3>
                                    <!-- Export Data --> 
                                    <a class="exportclasscss" href="<?php echo base_url(); ?>administrator/jobpost/export_csv_report_hiring?tabtype=weekwise" target="_blank">Export</a>
                                    <br><br>

                                    <form method="post">
                                      <div class="row">
                                        
                                        <div class="col-md-6">
                                          <select name="status_dropdown" id="status_dropdown_yearly" onchange="this.form.submit()">
                                             <option value="">Select Status</option>
                                             <option value="10" <?php if($searchStatus == "10"){ echo "selected"; }?>>All</option>
                                             <option value="1" <?php if($searchStatus == "1"){ echo "selected"; }?>>New Application</option>
                                             <option value="2" <?php if($searchStatus == "2"){ echo "selected"; }?>>No Show</option>
                                             <option value="3" <?php if($searchStatus == "3"){ echo "selected"; }?>>Fall Out</option>
                                             <option value="4" <?php if($searchStatus == "4"){ echo "selected"; }?>>Refer</option>
                                             <option value="5" <?php if($searchStatus == "5"){ echo "selected"; }?>>On Going Application</option>
                                             <option value="6" <?php if($searchStatus == "6"){ echo "selected"; }?>>Accepted JO</option>
                                             <option value="7" <?php if($searchStatus == "7"){ echo "selected"; }?>>Hired</option>
                                          </select>

                                          <select name="sort_dropdown" id="status_dropdown_sort" onchange="this.form.submit()">
                                             <option value="">Sort Admin Applied</option>
                                             <option value="high_to_low" <?php if($searchSort == "high_to_low"){ echo "selected"; }?>> High To Low </option>
                                             <option value="low_to_high" <?php if($searchSort == "low_to_high"){ echo "selected"; }?>> Low To High </option>
                                          </select>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="input-group mb-3">
                                              <input type="text" name="searchKeyword" class="form-control" placeholder="Search by keyword..." value="<?php echo $searchKeyword; ?>">
                                              <div class="input-group-append">
                                                  <input type="hidden" name="tabtype" value="weekwise">
                                                  <input type="submit" name="submitSearch" class="btn btn-outline-secondary" value="Search">
                                                  <input type="submit" name="submitSearchReset" class="btn btn-outline-secondary" value="Reset">
                                              </div>
                                          </div>  
                                        </div>

                                      </div>
                                    </form>

                                    <table id="sample-data-tableweekly" class="table">
                                      <thead>
                                         <tr>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Co.Name</span>
                                               </div>
                                            </th>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Site</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Admin Applied</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Id</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Posted By</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Title</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Interview Mode</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Category</span>
                                               </div>
                                            </th>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Sub-Category</span>
                                               </div>
                                            </th>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Candidate ID</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Candidate Name</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Candidate Email</span>
                                               </div>
                                            </th>
                                        
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Candidate Phone</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Experience (Year)</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Experience (Month)</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Education</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Level</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Industry</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Superpower</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Specialization</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Sub Specialization</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Work Mode</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Vaccination</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Relocate</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Platform</span>
                                               </div>
                                            </th>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Search status</span>
                                               </div>
                                            </th>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Last company</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">App Version</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">25 MBPS Internet</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Applied Date</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Salary Offer</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Total Guaranteed Allowance</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Latest Status</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Last Update</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Fall Out Reason</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Hired Date</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Candidate Last Managed By</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Chatbot</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Chatbot Result</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Match</span>
                                               </div>
                                            </th>
                                            
                                         </tr>
                                      </thead>

                                      <tbody>
                                        <?php 
                                            $sno = $row+1;
                                            foreach($result2 as $data2) {

                                              echo "<tr>";
                                              echo "<td>".$data2['recruiter_cname']."</td>";
                                              echo "<td>".$data2['company_name']."</td>";
                                              echo "<td> <a href='".base_url()."administrator/admin_applied/".$data2['user_id']."' style='color:green'><b>".$data2['adminjobcount']."</b></a></td>";
                                              
                                              echo "<td>".$data2['job_id']."</td>";
                                              echo "<td>".$data2['posted_by']."</td>";
                                              echo "<td>".$data2['job_title']."</td>";
                                              echo "<td>".$data2['mode']."</td>";
                                              echo "<td>".$data2['category']."</td>";
                                              echo "<td>".$data2['subcategory']."</td>";
                                              echo "<td>".$data2['user_id']."</td>";
                                              echo "<td>".$data2['user_name']."</td>";
                                              echo "<td>".$data2['user_email']."</td>";
                                              echo "<td>".$data2['user_phone']."</td>";
                                              echo "<td>".$data2['user_exp_year']."</td>";
                                              echo "<td>".$data2['user_exp_month']."</td>";
                                              echo "<td>".$data2['education']."</td>";
                                              echo "<td>".$data2['joblevel']."</td>";
                                              echo "<td>".$data2['industry']."</td>";
                                              echo "<td>".$data2['superpower']."</td>";
                                              echo "<td>".$data2['specialization']."</td>";
                                              echo "<td>".$data2['sub_specialization']."</td>";
                                              echo "<td>".$data2['work_mode']."</td>";
                                              echo "<td>".$data2['vaccination']."</td>";
                                              echo "<td>".$data2['relocate']."</td>";
                                              echo "<td>".$data2['platform']."</td>";
                                              echo "<td>".$data2['jobsearch_status']."</td>";
                                              echo "<td>".$data2['last_company']."</td>";
                                              echo "<td>".$data2['app_version']."</td>";
                                              echo "<td>".$data2['internetspeed']."</td>";
                                              echo "<td>".$data2['created_at']."</td>";
                                              echo "<td>".$data2['basic_salary']."</td>";
                                              echo "<td>".$data2['allowances']."</td>";
                                              echo "<td>".$data2['status']."</td>";
                                              echo "<td>".$data2['apply_date']."</td>";
                                              echo "<td>".$data2['fallout']."</td>";
                                              echo "<td>".$data2['hired_date']."</td>";
                                              echo "<td>".$data2['managed_by']."</td>";
                                              echo "<td>".$data2['chatbot']."</td>";
                                              echo "<td>".$data2['chat_result']."</td>";
                                              echo "<td> <a href='".base_url()."administrator/job_matching/".$data2['job_id']."/".$data2['user_id']."'  class='btn btn-warning' target='_blank'> View </a> </td>";
                                              echo "</tr>";
                                              $sno++;

                                            }
                                            if(count($result2) == 0){
                                              echo "<tr>";
                                              echo "<td colspan='3'>No record found.</td>";
                                              echo "</tr>";
                                            }
                                          ?>
                                      </tbody>
                                    </table>

                                    <!-- Paginate -->
                                    <?php if(count($result2) == 0) { 
                                    ?>
                                        <div class="custompagination" style='margin-top: 10px;text-align: center;'>
                                          <a href="<?php echo $pagination2['base_url']; ?>/<?php echo $pagination2['show_link'] - 10; ?>"><< Previous</a>
                                       </div>

                                    <?php

                                    } else { ?>
                                     <div class="custompagination" style='margin-top: 10px;text-align: center;'>
                                      <?php if($pagination2['show_link'] == 0) {} else { ?>
                                        <a href="<?php echo $pagination2['base_url']; ?>/<?php echo $pagination2['show_link'] - 10; ?>"><< Previous</a>
                                      <?php } ?>
                                      <?php if(count($result2) >= 10) { ?>
                                        <a href="<?php echo $pagination2['base_url']; ?>/<?php echo $pagination2['show_link'] + 10; ?>">Next >></a>
                                      <?php } ?>
                                     </div>

                                    <?php } ?>
                                 </div>

                                 <div class="tab-pane fade <?php if($checkactive == "tab3") { ?> show active <?php } ?>" id="monthly" role="tabpanel" aria-labelledby="monthly-tab">
                                    <h3>Monthly Report   ( <?php echo $resultTotal3; ?>) |  Page: <?php echo $pagenum = $this->uri->segment(4)/10+1; ?> </h3>
                                    <!-- Export Data --> 
                                    <a class="exportclasscss" href="<?php echo base_url(); ?>administrator/jobpost/export_csv_report_hiring?tabtype=monthwise" target="_blank">Export</a>
                                    <br><br>

                                    <form method="post">
                                      <div class="row">
                                        
                                        <div class="col-md-6">
                                          <select name="status_dropdown" id="status_dropdown_yearly" onchange="this.form.submit()">
                                             <option value="">Select Status</option>
                                             <option value="10" <?php if($searchStatus == "10"){ echo "selected"; }?>>All</option>
                                             <option value="1" <?php if($searchStatus == "1"){ echo "selected"; }?>>New Application</option>
                                             <option value="2" <?php if($searchStatus == "2"){ echo "selected"; }?>>No Show</option>
                                             <option value="3" <?php if($searchStatus == "3"){ echo "selected"; }?>>Fall Out</option>
                                             <option value="4" <?php if($searchStatus == "4"){ echo "selected"; }?>>Refer</option>
                                             <option value="5" <?php if($searchStatus == "5"){ echo "selected"; }?>>On Going Application</option>
                                             <option value="6" <?php if($searchStatus == "6"){ echo "selected"; }?>>Accepted JO</option>
                                             <option value="7" <?php if($searchStatus == "7"){ echo "selected"; }?>>Hired</option>
                                          </select>

                                          <select name="sort_dropdown" id="status_dropdown_sort" onchange="this.form.submit()">
                                             <option value="">Sort Admin Applied</option>
                                             <option value="high_to_low" <?php if($searchSort == "high_to_low"){ echo "selected"; }?>> High To Low </option>
                                             <option value="low_to_high" <?php if($searchSort == "low_to_high"){ echo "selected"; }?>> Low To High </option>
                                          </select>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="input-group mb-3">
                                              <input type="text" name="searchKeyword" class="form-control" placeholder="Search by keyword..." value="<?php echo $searchKeyword; ?>">
                                              <div class="input-group-append">
                                                  <input type="hidden" name="tabtype" value="monthwise">
                                                  <input type="submit" name="submitSearch" class="btn btn-outline-secondary" value="Search">
                                                  <input type="submit" name="submitSearchReset" class="btn btn-outline-secondary" value="Reset">
                                              </div>
                                          </div>  
                                        </div>

                                      </div>
                                    </form>

                                    <table id="sample-data-tablemonthly" class="table">
                                      <thead>
                                         <tr>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Co.Name</span>
                                               </div>
                                            </th>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Site</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Admin Applied</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Id</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Posted By</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Title</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Interview Mode</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Category</span>
                                               </div>
                                            </th>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Sub-Category</span>
                                               </div>
                                            </th>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Candidate ID</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Candidate Name</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Candidate Email</span>
                                               </div>
                                            </th>
                                        
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Candidate Phone</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Experience (Year)</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Experience (Month)</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Education</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Level</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Industry</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Superpower</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Specialization</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Sub Specialization</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Work Mode</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Vaccination</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Relocate</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Platform</span>
                                               </div>
                                            </th>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Search status</span>
                                               </div>
                                            </th>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Last company</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">App Version</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">25 MBPS Internet</span>
                                               </div>
                                            </th>
                                      
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Applied Date</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Salary Offer</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Total Guaranteed Allowance</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Latest Status</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Last Update</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Fall Out Reason</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Hired Date</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Candidate Last Managed By</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Chatbot</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Chatbot Result</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Match</span>
                                               </div>
                                            </th>
                                            
                                         </tr>
                                      </thead>

                                      <tbody>
                                        <?php 
                                            $sno = $row+1;
                                            foreach($result3 as $data3) {

                                              echo "<tr>";
                                              echo "<td>".$data3['recruiter_cname']."</td>";
                                              echo "<td>".$data3['company_name']."</td>";
                                              echo "<td> <a href='".base_url()."administrator/admin_applied/".$data3['user_id']."' style='color:green'><b>".$data3['adminjobcount']."</b></a></td>";
                                              echo "<td>".$data3['job_id']."</td>";
                                              echo "<td>".$data3['posted_by']."</td>";
                                              echo "<td>".$data3['job_title']."</td>";
                                              echo "<td>".$data3['mode']."</td>";
                                              echo "<td>".$data3['category']."</td>";
                                              echo "<td>".$data3['subcategory']."</td>";
                                              echo "<td>".$data3['user_id']."</td>";
                                              echo "<td>".$data3['user_name']."</td>";
                                              echo "<td>".$data3['user_email']."</td>";
                                              echo "<td>".$data3['user_phone']."</td>";
                                              echo "<td>".$data3['user_exp_year']."</td>";
                                              echo "<td>".$data3['user_exp_month']."</td>";
                                              echo "<td>".$data3['education']."</td>";
                                              echo "<td>".$data3['joblevel']."</td>";
                                              echo "<td>".$data3['industry']."</td>";
                                              echo "<td>".$data3['superpower']."</td>";
                                              echo "<td>".$data3['specialization']."</td>";
                                              echo "<td>".$data3['sub_specialization']."</td>";
                                              echo "<td>".$data3['work_mode']."</td>";
                                              echo "<td>".$data3['vaccination']."</td>";
                                              echo "<td>".$data3['relocate']."</td>";
                                              echo "<td>".$data3['platform']."</td>";
                                              echo "<td>".$data3['jobsearch_status']."</td>";
                                              echo "<td>".$data3['last_company']."</td>";
                                              echo "<td>".$data3['app_version']."</td>";
                                              echo "<td>".$data3['internetspeed']."</td>";
                                              echo "<td>".$data3['created_at']."</td>";
                                              echo "<td>".$data3['basic_salary']."</td>";
                                              echo "<td>".$data3['allowances']."</td>";
                                              echo "<td>".$data3['status']."</td>";
                                              echo "<td>".$data3['apply_date']."</td>";
                                              echo "<td>".$data3['fallout']."</td>";
                                              echo "<td>".$data3['hired_date']."</td>";
                                              echo "<td>".$data3['managed_by']."</td>";
                                              echo "<td>".$data3['chatbot']."</td>";
                                              echo "<td>".$data3['chat_result']."</td>";
                                              echo "<td> <a href='".base_url()."administrator/job_matching/".$data3['job_id']."/".$data3['user_id']."'  class='btn btn-warning' target='_blank'> View </a> </td>";
                                              echo "</tr>";
                                              $sno++;

                                            }
                                            if(count($result3) == 0){
                                              echo "<tr>";
                                              echo "<td colspan='3'>No record found.</td>";
                                              echo "</tr>";
                                            }
                                          ?>
                                      </tbody>
                                    </table>

                                    <!-- Paginate -->
                                    <?php if(count($result3) == 0) { 
                                    ?>
                                        <div class="custompagination" style='margin-top: 10px;text-align: center;'>
                                          <a href="<?php echo $pagination3['base_url']; ?>/<?php echo $pagination3['show_link'] - 10; ?>"><< Previous</a>
                                       </div>

                                    <?php

                                    } else { ?>
                                     <div class="custompagination" style='margin-top: 10px;text-align: center;'>
                                      <?php if($pagination3['show_link'] == 0) {} else { ?>
                                        <a href="<?php echo $pagination3['base_url']; ?>/<?php echo $pagination3['show_link'] - 10; ?>"><< Previous</a>
                                      <?php } ?>
                                      <?php if(count($result3) >= 10) { ?>
                                        <a href="<?php echo $pagination3['base_url']; ?>/<?php echo $pagination3['show_link'] + 10; ?>">Next >></a>
                                      <?php } ?>
                                     </div>

                                    <?php } ?>
                                 </div>


                                 <div class="tab-pane fade <?php if($checkactive == "tab4") { ?> show active <?php } ?>" id="yearly" role="tabpanel" aria-labelledby="yearly-tab">
                                    <h3>Yearly Report  ( <?php echo $resultTotal4; ?>) |  Page: <?php echo $pagenum = $this->uri->segment(4)/10+1; ?> </h3>
                                    <!-- Export Data --> 
                                    <a class="exportclasscss" href="<?php echo base_url(); ?>administrator/jobpost/export_csv_report_hiring?tabtype=yearwise" target="_blank">Export</a>
                                    <br><br>

                                    <form method="post">
                                      <div class="row">
                                        
                                        <div class="col-md-6">
                                          <select name="status_dropdown" id="status_dropdown_yearly" onchange="this.form.submit()">
                                             <option value="">Select Status</option>
                                             <option value="10" <?php if($searchStatus == "10"){ echo "selected"; }?>>All</option>
                                             <option value="1" <?php if($searchStatus == "1"){ echo "selected"; }?>>New Application</option>
                                             <option value="2" <?php if($searchStatus == "2"){ echo "selected"; }?>>No Show</option>
                                             <option value="3" <?php if($searchStatus == "3"){ echo "selected"; }?>>Fall Out</option>
                                             <option value="4" <?php if($searchStatus == "4"){ echo "selected"; }?>>Refer</option>
                                             <option value="5" <?php if($searchStatus == "5"){ echo "selected"; }?>>On Going Application</option>
                                             <option value="6" <?php if($searchStatus == "6"){ echo "selected"; }?>>Accepted JO</option>
                                             <option value="7" <?php if($searchStatus == "7"){ echo "selected"; }?>>Hired</option>
                                          </select>

                                          <select name="sort_dropdown" id="status_dropdown_sort" onchange="this.form.submit()">
                                             <option value="">Sort Admin Applied</option>
                                             <option value="high_to_low" <?php if($searchSort == "high_to_low"){ echo "selected"; }?>> High To Low </option>
                                             <option value="low_to_high" <?php if($searchSort == "low_to_high"){ echo "selected"; }?>> Low To High </option>
                                          </select>

                                        </div>
                                        <div class="col-md-6">
                                          <div class="input-group mb-3">
                                              <input type="text" name="searchKeyword" class="form-control" placeholder="Search by keyword..." value="<?php echo $searchKeyword; ?>">
                                              <div class="input-group-append">
                                                  <input type="hidden" name="tabtype" value="yearwise">
                                                  <input type="submit" name="submitSearch" class="btn btn-outline-secondary" value="Search">
                                                  <input type="submit" name="submitSearchReset" class="btn btn-outline-secondary" value="Reset">
                                              </div>
                                          </div>  
                                        </div>

                                      </div>
                                    </form>
                                    
                                    <table id="sample-data-tableyearly" class="table">
                                      <thead>
                                         <tr>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Co.Name</span>
                                               </div>
                                            </th>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Site</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Admin Applied</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Id</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Posted By</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Title</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Interview Mode</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Category</span>
                                               </div>
                                            </th>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Sub-Category</span>
                                               </div>
                                            </th>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Candidate ID</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Candidate Name</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Candidate Email</span>
                                               </div>
                                            </th>
                                        
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Candidate Phone</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Experience (Year)</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Experience (Month)</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Education</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Level</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Industry</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Superpower</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Specialization</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Sub Specialization</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Work Mode</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Vaccination</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Relocate</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Platform</span>
                                               </div>
                                            </th>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Search status</span>
                                               </div>
                                            </th>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Last company</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">App Version</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">25 MBPS Internet</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Applied Date</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Salary Offer</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Total Guaranteed Allowance</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Latest Status</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Last Update</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Fall Out Reason</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Hired Date</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Candidate Last Managed By</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Chatbot</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Chatbot Result</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Match</span>
                                               </div>
                                            </th>
                                            
                                         </tr>
                                      </thead>

                                      <tbody>
                                        <?php 
                                            $sno = $row+1;
                                            foreach($result4 as $data4) {

                                              echo "<tr>";
                                              echo "<td>".$data4['recruiter_cname']."</td>";
                                              echo "<td>".$data4['company_name']."</td>";
                                              echo "<td> <a href='".base_url()."administrator/admin_applied/".$data4['user_id']."' style='color:green'><b>".$data4['adminjobcount']."</b></a></td>";
                                              echo "<td>".$data4['job_id']."</td>";
                                              echo "<td>".$data4['posted_by']."</td>";
                                              echo "<td>".$data4['job_title']."</td>";
                                              echo "<td>".$data4['mode']."</td>";
                                              echo "<td>".$data4['category']."</td>";
                                              echo "<td>".$data4['subcategory']."</td>";
                                              echo "<td>".$data4['user_id']."</td>";
                                              echo "<td>".$data4['user_name']."</td>";
                                              echo "<td>".$data4['user_email']."</td>";
                                              echo "<td>".$data4['user_phone']."</td>";
                                              echo "<td>".$data4['user_exp_year']."</td>";
                                              echo "<td>".$data4['user_exp_month']."</td>";
                                              echo "<td>".$data4['education']."</td>";
                                              echo "<td>".$data4['joblevel']."</td>";
                                              echo "<td>".$data4['industry']."</td>";
                                              echo "<td>".$data4['superpower']."</td>";
                                              echo "<td>".$data4['specialization']."</td>";
                                              echo "<td>".$data4['sub_specialization']."</td>";
                                              echo "<td>".$data4['work_mode']."</td>";
                                              echo "<td>".$data4['vaccination']."</td>";
                                              echo "<td>".$data4['relocate']."</td>";
                                              echo "<td>".$data4['platform']."</td>";
                                              echo "<td>".$data4['jobsearch_status']."</td>";
                                              echo "<td>".$data4['last_company']."</td>";
                                              echo "<td>".$data4['app_version']."</td>";
                                              echo "<td>".$data4['internetspeed']."</td>";
                                              echo "<td>".$data4['created_at']."</td>";
                                              echo "<td>".$data4['basic_salary']."</td>";
                                              echo "<td>".$data4['allowances']."</td>";
                                              echo "<td>".$data4['status']."</td>";
                                              echo "<td>".$data4['apply_date']."</td>";
                                              echo "<td>".$data4['fallout']."</td>";
                                              echo "<td>".$data4['hired_date']."</td>";
                                              echo "<td>".$data4['managed_by']."</td>";
                                              echo "<td>".$data4['chatbot']."</td>";
                                              echo "<td>".$data4['chat_result']."</td>";
                                              echo "<td> <a href='".base_url()."administrator/job_matching/".$data4['job_id']."/".$data4['user_id']."'  class='btn btn-warning' target='_blank'> View </a> </td>";
                                              echo "</tr>";
                                              $sno++;

                                            }
                                            if(count($result4) == 0){
                                              echo "<tr>";
                                              echo "<td colspan='3'>No record found.</td>";
                                              echo "</tr>";
                                            }
                                          ?>
                                      </tbody>
                                    </table>

                                    <!-- Paginate -->
                                    <?php if(count($result4) == 0) { 
                                    ?>
                                        <div class="custompagination" style='margin-top: 10px;text-align: center;'>
                                          <a href="<?php echo $pagination4['base_url']; ?>/<?php echo $pagination4['show_link'] - 10; ?>"><< Previous</a>
                                       </div>

                                    <?php

                                    } else { ?>
                                     <div class="custompagination" style='margin-top: 10px;text-align: center;'>
                                      <?php if($pagination4['show_link'] == 0) {} else { ?>
                                        <a href="<?php echo $pagination4['base_url']; ?>/<?php echo $pagination4['show_link'] - 10; ?>"><< Previous</a>
                                      <?php } ?>
                                      <?php if(count($result4) >= 10) { ?>
                                        <a href="<?php echo $pagination4['base_url']; ?>/<?php echo $pagination4['show_link'] + 10; ?>">Next >></a>
                                      <?php } ?>
                                     </div>

                                    <?php } ?>
                                 </div>



                                 <div class="tab-pane fade <?php if($checkactive == "tab5") { ?> show active <?php } ?>" id="date" role="tabpanel" aria-labelledby="date-tab">
                                    <h3>Datewise Report ( <?php echo $resultTotal5; ?>) |  Page: <?php echo $pagenum = $this->uri->segment(4)/10; ?> </h3>
                                    <!-- Export Data --> 
                                    <a class="exportclasscss" href="<?php echo base_url(); ?>administrator/jobpost/export_csv_report_hiring?tabtype=datewise">Export</a><br><br>
                                    
                                    <form method="post">
                                      
                                      <div class="row">
                                         <div class="col-md-2">  
                                            <input type="date" name="from_date" value="<?php if(strlen($searchFrom)>0){ echo $searchFrom; }?>" id="from_date" class="form-control" placeholder="From Date" />  
                                         </div>
                                         <div class="col-md-2">  
                                            <input type="date" name="to_date" value="<?php if(strlen($searchTo)>0){ echo $searchTo; }?>" id="to_date" class="form-control" placeholder="To Date" />  
                                         </div>
                                         <div class="col-md-2">
                                             <select name="status_dropdown" id="status_dropdown_date" onchange="this.form.submit()">
                                                <option value="">Select Status</option>
                                                <option value="10" <?php if($searchStatus == "10"){ echo "selected"; }?>>All</option>
                                                <option value="1" <?php if($searchStatus == "1"){ echo "selected"; }?>>New Application</option>
                                                <option value="2" <?php if($searchStatus == "2"){ echo "selected"; }?>>No Show</option>
                                                <option value="3" <?php if($searchStatus == "3"){ echo "selected"; }?>>Fall Out</option>
                                                <option value="4" <?php if($searchStatus == "4"){ echo "selected"; }?>>Refer</option>
                                                <option value="5" <?php if($searchStatus == "5"){ echo "selected"; }?>>On Going Application</option>
                                                <option value="6" <?php if($searchStatus == "6"){ echo "selected"; }?>>Accepted JO</option>
                                                <option value="7" <?php if($searchStatus == "7"){ echo "selected"; }?>>Hired</option>
                                             </select>

                                             <select name="sort_dropdown" id="status_dropdown_sort" onchange="this.form.submit()">
                                                <option value="">Sort Admin Applied</option>
                                                <option value="high_to_low" <?php if($searchSort == "high_to_low"){ echo "selected"; }?>> High To Low </option>
                                                <option value="low_to_high" <?php if($searchSort == "low_to_high"){ echo "selected"; }?>> Low To High </option>
                                             </select>

                                         </div>
                                         <div class="col-md-3">
                                              <input type="text" name="searchKeyword" class="form-control" placeholder="Search by keyword..." value="<?php echo $searchKeyword; ?>">
                                           </div>
                                         <div class="col-md-3">  
                                            <input type="hidden" name="tabtype" value="datewise">
                                            <input type="submit" name="submitSearch" class="btn btn-outline-secondary" value="Search">
                                            <input type="submit" name="submitSearchReset" class="btn btn-outline-secondary" value="Reset">
                                         </div>
                                      </div>

                                    </form>

                                    <table id="sample-data-tabledate" class="table">
                                      <thead>
                                         <tr>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Co.Name</span>
                                               </div>
                                            </th>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Site</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Admin Applied</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Id</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Posted By</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Title</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Interview Mode</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Category</span>
                                               </div>
                                            </th>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Sub-Category</span>
                                               </div>
                                            </th>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Candidate ID</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Candidate Name</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Candidate Email</span>
                                               </div>
                                            </th>
                                        
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Candidate Phone</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Experience (Year)</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Experience (Month)</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Education</span>
                                               </div>
                                            </th>
                                            
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Level</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Industry</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Superpower</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Specialization</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Sub Specialization</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Work Mode</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Vaccination</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Relocate</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Platform</span>
                                               </div>
                                            </th>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Search status</span>
                                               </div>
                                            </th>
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Last company</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">App Version</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">25 MBPS Internet</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Applied Date</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Salary Offer</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Total Guaranteed Allowance</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Latest Status</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Last Update</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Fall Out Reason</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Hired Date</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Candidate Last Managed By</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Chatbot</span>
                                               </div>
                                            </th>
                                            
                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Chatbot Result</span>
                                               </div>
                                            </th>

                                            <th class="secondary-text">
                                               <div class="table-header">
                                                  <span class="column-title">Job Match</span>
                                               </div>
                                            </th>

                                         </tr>
                                      </thead>

                                      <tbody>
                                        <?php 
                                            $sno = $row+1;
                                            foreach($result5 as $data5) {

                                              echo "<tr>";
                                              echo "<td>".$data5['recruiter_cname']."</td>";
                                              echo "<td>".$data5['company_name']."</td>";
                                              echo "<td> <a href='".base_url()."administrator/admin_applied/".$data6['user_id']."' style='color:green'><b>".$data6['adminjobcount']."</b></a></td>";
                                              echo "<td>".$data5['job_id']."</td>";
                                              echo "<td>".$data5['posted_by']."</td>";
                                              echo "<td>".$data5['job_title']."</td>";
                                              echo "<td>".$data5['mode']."</td>";
                                              echo "<td>".$data5['category']."</td>";
                                              echo "<td>".$data5['subcategory']."</td>";
                                              echo "<td>".$data5['user_id']."</td>";
                                              echo "<td>".$data5['user_name']."</td>";
                                              echo "<td>".$data5['user_email']."</td>";
                                              echo "<td>".$data5['user_phone']."</td>";
                                              echo "<td>".$data5['user_exp_year']."</td>";
                                              echo "<td>".$data5['user_exp_month']."</td>";
                                              echo "<td>".$data5['education']."</td>";
                                              echo "<td>".$data5['joblevel']."</td>";
                                              echo "<td>".$data5['industry']."</td>";
                                              echo "<td>".$data5['superpower']."</td>";
                                              echo "<td>".$data5['specialization']."</td>";
                                              echo "<td>".$data5['sub_specialization']."</td>";
                                              echo "<td>".$data5['work_mode']."</td>";
                                              echo "<td>".$data5['vaccination']."</td>";
                                              echo "<td>".$data5['relocate']."</td>";
                                              echo "<td>".$data5['platform']."</td>";
                                              echo "<td>".$data5['jobsearch_status']."</td>";
                                              echo "<td>".$data5['last_company']."</td>";
                                              echo "<td>".$data5['app_version']."</td>";
                                              echo "<td>".$data5['internetspeed']."</td>";
                                              echo "<td>".$data5['created_at']."</td>";
                                              echo "<td>".$data5['basic_salary']."</td>";
                                              echo "<td>".$data5['allowances']."</td>";
                                              echo "<td>".$data5['status']."</td>";
                                              echo "<td>".$data5['apply_date']."</td>";
                                              echo "<td>".$data5['fallout']."</td>";
                                              echo "<td>".$data5['hired_date']."</td>";
                                              echo "<td>".$data5['managed_by']."</td>";
                                              echo "<td>".$data5['chatbot']."</td>";
                                              echo "<td>".$data5['chat_result']."</td>";
                                              echo "<td> <a href='".base_url()."administrator/job_matching/".$data5['job_id']."/".$data5['user_id']."'  class='btn btn-warning' target='_blank'> View </a> </td>";
                                              echo "</tr>";
                                              $sno++;

                                            }
                                            if(count($result5) == 0){
                                              echo "<tr>";
                                              echo "<td colspan='3'>No record found.</td>";
                                              echo "</tr>";
                                            }
                                          ?>
                                      </tbody>
                                    </table>

                                    <!-- Paginate -->
                                    <?php 
                                    if(count($result5) == 0) { 
                                    ?>
                                       <div class="custompagination" style='margin-top: 10px;text-align: center;'>
                                          <a href="<?php echo $pagination5['base_url']; ?>/<?php echo $pagination5['show_link'] - 10; ?>"><< Previous</a>
                                       </div>

                                    <?php
                                    } else { 
                                    ?>
                                       
                                       <div class="custompagination" style='margin-top: 10px;text-align: center;'>
                                          <?php if($pagination5['show_link'] == 0) {} else { ?>
                                             <a href="<?php echo $pagination5['base_url']; ?>/<?php echo $pagination5['show_link'] - 10; ?>"><< Previous</a>
                                          <?php } ?>
                                          <?php if(count($result5) >= 10) { ?>
                                             <a href="<?php echo $pagination5['base_url']; ?>/<?php echo $pagination5['show_link'] + 10; ?>">Next >></a>
                                          <?php } ?>
                                       </div>

                                    <?php } ?>
                                 </div>

                              </div>
                           </div>
                        </div>
                        <div class="source custom-scrollbar">
                           <div class="highlight">
                              <pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-html" data-lang="html">
    
                              </code></pre>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CONTENT -->
   </div>
</div>
</div>

<div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
</div>

</div>
<nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
</nav>

</main>

<!-- Modal -->
<div class="modal fade" id="jobmatchModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Job Match</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>


</body>
</html>

<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<!-- / JAVASCRIPT -->
<script type="text/javascript">
   // function getjobmatch(jobid, userid) {
   //    $.ajax({
   //       url:"<?php //echo base_url(); ?>administrator/jobpost/matchingjob";
   //       type: "POST",
   //       data: {jobid, userid},
   //       success:function(response) {

   //       },
   //       error: function(jqXHR, textStatus, errorThrown) {

   //         console.log(textStatus, errorThrown);
   //       }
   //    });
   // }
</script>