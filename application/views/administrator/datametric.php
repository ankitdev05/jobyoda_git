<?php include('header.php'); ?>

<style type="text/css">
  .table thead .column-title {
      font-size: 12px!important;
  }
  .dt-buttons {
    margin-bottom: 10px;
  }
  .tablinks{
      background-color: #3baf62;
      font-weight: 700;
      color: #fff;
      padding: 7px 15px 9px 15px;
      margin-right: 20px;
  }
  .tablinks.activetab {
    background-color: #0a4c89!important;
  }

  table.dataTable tbody tr td {
    padding-left: 24px!important;
  }

  table.dataTable thead tr th {
    padding-left: 24px!important;
  }
</style>

               <div class="content custom-scrollbar">
                  <div class="doc data-table-doc page-layout simple full-width">
                     <!-- HEADER -->
                     <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
                        <!-- APP TITLE -->
                        <div class="col-12 col-sm">
                           <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                              <div class="logo-icon mr-3 mt-1">
                                 <i class="icon s-6 icon-person-box"></i>
                              </div>
                              <div class="logo-text">
                                 <div class="h4">Data Metrics</div>
                                 <div class=""></div>
                              </div>
                           </div>
                        </div>
                        <!-- / APP TITLE -->
                        <!--<div class="col-auto">
                           <a href="<?php echo base_url();?>administrator/recruiter/addRecruiter" class="btn btn-light">ADD NEW</a>
                        </div>-->
                     </div>
                     <!-- / HEADER -->
                     <!-- CONTENT -->
                     <div class="page-content p-6">
                        <div class="content container">
                           <div class="row">
                              <div class="col-12">
                                 <div class="example ">

                                    <div class="source-preview-wrapper">
                                       <div class="preview">
                                          <div class="preview-elements companyldtf">
                                             
                                             <table id="sample-data-tableee" class="table cell-border compact hover nowrap stripe">
                                                <thead>
                                                  
                                                  <tr>
                                                    <th class="secondary-text" colspan="2">
                                                       <div class="table-header">
                                                          <span class="column-title"> JY Client Dashboard </span>
                                                       </div>
                                                    </th>
                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title"> Jan </span>
                                                       </div>
                                                    </th>
                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title"> Feb </span>
                                                       </div>
                                                    </th>
                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title"> Mar </span>
                                                       </div>
                                                    </th>
                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title"> Apr </span>
                                                       </div>
                                                    </th>
                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title"> May </span>
                                                       </div>
                                                    </th>
                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title"> June </span>
                                                       </div>
                                                    </th>
                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title"> July </span>
                                                       </div>
                                                    </th>
                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title"> Aug </span>
                                                       </div>
                                                    </th>
                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title"> Sept </span>
                                                       </div>
                                                    </th>
                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title"> Oct </span>
                                                       </div>
                                                    </th>
                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title"> Nov </span>
                                                       </div>
                                                    </th>
                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title"> Dec </span>
                                                       </div>
                                                    </th>

                                                  </tr>

                                                </thead>   

                                                <tbody>
                                                
                                                <?php
                                                  $yy = 1;
                                                  foreach($totalYear as $totalY) {
                                                ?>
                                                  <tr>
                                                  
                                                  <?php if($yy == 1) { ?>
                                                    <td rowspan="<?php echo count($totalYear); ?>">Total Registrations</td>
                                                  <?php } ?>
                                                    
                                                    <td><?php echo $totalY; ?></td>

                                                  <?php  
                                                    foreach($totalRegistration[$totalY] as $totalReg) {
                                                  ?>

                                                    <td><?php echo $totalReg['record']; ?></td>

                                                  <?php } ?>

                                                  </tr>

                                                <?php
                                                    $yy++;
                                                  }
                                                ?>

                                                <tr style="height:30px">
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                </tr>

                                                <?php
                                                  $yy = 1;
                                                  foreach($totalYear as $totalY) {
                                                ?>
                                                  <tr>
                                                  
                                                  <?php if($yy == 1) { ?>
                                                    <td rowspan="<?php echo count($totalYear); ?>">BPO</td>
                                                  <?php } ?>
                                                    
                                                    <td><?php echo $totalY; ?></td>

                                                  <?php  
                                                    foreach($totalBPO[$totalY] as $totalReg) {
                                                  ?>

                                                    <td><?php echo $totalReg['record']; ?></td>

                                                  <?php } ?>

                                                  </tr>

                                                <?php
                                                    $yy++;
                                                  }
                                                ?>

                                                <tr style="height:30px">
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                </tr>

                                                <?php
                                                  $yy = 1;
                                                  foreach($totalYear as $totalY) {
                                                ?>
                                                  <tr>
                                                  
                                                  <?php if($yy == 1) { ?>
                                                    <td rowspan="<?php echo count($totalYear); ?>">Non BPO</td>
                                                  <?php } ?>
                                                    
                                                    <td><?php echo $totalY; ?></td>

                                                  <?php  
                                                    foreach($totalNonBPO[$totalY] as $totalReg) {
                                                  ?>

                                                    <td><?php echo $totalReg['record']; ?></td>

                                                  <?php } ?>

                                                  </tr>

                                                <?php
                                                    $yy++;
                                                  }
                                                ?>

                                                <tr style="height:30px">
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                </tr>

                                                <?php
                                                  $yy = 1;
                                                  foreach($totalYear as $totalY) {
                                                ?>
                                                  <tr>
                                                  
                                                  <?php if($yy == 1) { ?>
                                                    <td rowspan="<?php echo count($totalYear); ?>">Application</td>
                                                  <?php } ?>
                                                    
                                                    <td><?php echo $totalY; ?></td>

                                                  <?php  
                                                    foreach($totalApp[$totalY] as $totalReg) {
                                                  ?>

                                                    <td><?php echo $totalReg['record']; ?></td>

                                                  <?php } ?>

                                                  </tr>

                                                <?php
                                                    $yy++;
                                                  }
                                                ?>

                                                <tr style="height:30px">
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                </tr>

                                                <?php
                                                  $yy = 1;
                                                  foreach($totalYear as $totalY) {
                                                ?>
                                                  <tr>
                                                  
                                                  <?php if($yy == 1) { ?>
                                                    <td rowspan="<?php echo count($totalYear); ?>">- Instant Screening</td>
                                                  <?php } ?>
                                                    
                                                    <td><?php echo $totalY; ?></td>

                                                  <?php  
                                                    foreach($totalInstant[$totalY] as $totalReg) {
                                                  ?>

                                                    <td><?php echo $totalReg['record']; ?></td>

                                                  <?php } ?>

                                                  </tr>

                                                <?php
                                                    $yy++;
                                                  }
                                                ?>

                                                <tr style="height:30px">
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                </tr>

                                                <?php
                                                  $yy = 1;
                                                  foreach($totalYear as $totalY) {
                                                ?>
                                                  <tr>
                                                  
                                                  <?php if($yy == 1) { ?>
                                                    <td rowspan="<?php echo count($totalYear); ?>">- Call/Walk-In</td>
                                                  <?php } ?>
                                                    
                                                    <td><?php echo $totalY; ?></td>

                                                  <?php  
                                                    foreach($totalCall[$totalY] as $totalReg) {
                                                  ?>

                                                    <td><?php echo $totalReg['record']; ?></td>

                                                  <?php } ?>

                                                  </tr>

                                                <?php
                                                    $yy++;
                                                  }
                                                ?>

                                                <tr style="height:30px">
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                </tr>

                                                

                                                </tbody>
                                             </table>

                                          </div>
                                       </div>

                                       <div class="source custom-scrollbar">
                                          <div class="highlight">
                                             <pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-html" data-lang="html">
    
                                                    </code></pre>
                                          </div>
                                       </div>

                                    </div>


                                    <div class="source-preview-wrapper" style="margin-top: 5px;">
                                       <div class="preview">
                                          <div class="preview-elements companyldtf text-center" style="border-top: 1px solid #ccc;padding-top: 20px;">

                                              <a class="tablinks <?php if($_GET['type']=="all") { echo "activetab"; } ?>" href="https://jobyoda.com/administrator/data_metrics?type=all"> All </a>
                                              <a class="tablinks <?php if($_GET['type']=="daily") { echo "activetab"; } ?>" href="https://jobyoda.com/administrator/data_metrics?type=daily"> Daily </a>
                                              <a class="tablinks <?php if($_GET['type']=="weekly") { echo "activetab"; } ?>" href="https://jobyoda.com/administrator/data_metrics?type=weekly"> Weekly </a>
                                              <a class="tablinks <?php if($_GET['type']=="monthly") { echo "activetab"; } ?>" href="https://jobyoda.com/administrator/data_metrics?type=monthly"> Monthly </a>
                                              <a class="tablinks <?php if($_GET['type']=="yearly") { echo "activetab"; } ?>" href="https://jobyoda.com/administrator/data_metrics?type=yearly"> Yearly </a>

                                          </div>
                                        </div>
                                    </div>


                                    <div class="source-preview-wrapper">
                                       <div class="preview">
                                          <div class="preview-elements companyldtf">
                                             
                                             <table id="sample-data-table" class="table cell-border compact hover nowrap stripe">
                                                <thead>
                                                  
                                                  <tr>
                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title">Metric</span>
                                                       </div>
                                                    </th>
                                                    
                                                    <!-- <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title">Grand Total</span>
                                                       </div>
                                                    </th> -->

                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title">Jobs</span>
                                                       </div>
                                                    </th>

                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title">Active Jobs</span>
                                                       </div>
                                                    </th>

                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title">Jobs expiring in the next 7days</span>
                                                       </div>
                                                    </th>

                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title">Applications</span>
                                                       </div>
                                                    </th>

                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title"> - Instant Screening</span>
                                                       </div>
                                                    </th>

                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title"> - Phone Screening</span>
                                                       </div>
                                                    </th>

                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title"> -Walk-In </span>
                                                       </div>
                                                    </th>

                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title"> Notifications </span>
                                                       </div>
                                                    </th>

                                                  </tr>

                                                </thead>   

                                                <tbody>
                                                
                                                <tr style="background-color: orange;font-weight: 500;">

                                                   <td> Grand Total </td>

                                                   <?php 
                                                      $totalJobs = 0;
                                                      $totalActiveJobs = 0;
                                                      $totalApplyJobs = 0;
                                                      $totalExpireJobs = 0;
                                                      $totalInstantJobs = 0;
                                                      $totalPhoneJobs = 0;
                                                      $totalWalkJobs = 0;
                                                      $totalNotify = 0;

                                                      foreach($totalCompanyArray as $totalComp) {
                                                         $totalJobs = $totalJobs + $totalComp['have_jobs'];
                                                         $totalActiveJobs = $totalActiveJobs + $totalComp['active_jobs'];
                                                         $totalExpireJobs = $totalExpireJobs + $totalComp['upcoming_expire_jobs'];
                                                         $totalApplyJobs = $totalApplyJobs + $totalComp['total_apply'];
                                                         $totalInstantJobs = $totalInstantJobs + $totalComp['instant_apply'];
                                                         $totalPhoneJobs = $totalPhoneJobs + $totalComp['phone_apply'];
                                                         $totalWalkJobs = $totalWalkJobs + $totalComp['walk_apply'];
                                                         $totalNotify = $totalNotify + $totalComp['notification_send'];
                                                      }
                                                   ?>

                                                   <td> <?php echo $totalJobs; ?> </td>
                                                   <td> <?php echo $totalActiveJobs; ?> </td>
                                                   <td> <?php echo $totalExpireJobs; ?> </td>
                                                   <td> <?php echo $totalApplyJobs; ?> </td>
                                                   <td> <?php echo $totalInstantJobs; ?> </td>
                                                   <td> <?php echo $totalPhoneJobs; ?> </td>
                                                   <td> <?php echo $totalWalkJobs; ?> </td>
                                                   <td> <?php echo $totalNotify; ?> </td>
                                                </tr>



                                                <?php
                                                  foreach($totalCompanyArray as $totalComp) {
                                                ?>                                                
                                                  <tr>
                                                     <td style="font-weight: 500;"> <?php echo $totalComp['cname']; ?> </td>
                                                     <td> <?php echo $totalComp['have_jobs']; ?> </td>
                                                     <td> <?php echo $totalComp['active_jobs']; ?> </td>
                                                     <td> <?php echo $totalComp['upcoming_expire_jobs']; ?> </td>
                                                     <td> <?php echo $totalComp['total_apply']; ?> </td>
                                                     <td> <?php echo $totalComp['instant_apply']; ?> </td>
                                                     <td> <?php echo $totalComp['phone_apply']; ?> </td>
                                                     <td> <?php echo $totalComp['walk_apply']; ?> </td>
                                                     <td> <?php echo $totalComp['notification_send']; ?> </td>

                                                  </tr>
                                                
                                                <?php
                                                   }
                                                ?>

                                                </tbody>
                                             </table>

                                          </div>
                                       </div>

                                       <div class="source custom-scrollbar">
                                          <div class="highlight">
                                             <pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-html" data-lang="html">
    
                                                    </code></pre>
                                          </div>
                                       </div>

                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- CONTENT -->
                  </div>
               </div>
            </div>
            
         </div>
         <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
            <!--<a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
            <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
            </a>-->
         </nav>
      </main>

<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>


<script type="text/javascript">
  $('#sample-data-table').DataTable({
    dom: 'lBfrtip',
    buttons: [
        'excel', 'csv'
    ],
    "bPaginate": false,
    "bLengthChange": false,
    "bFilter": false,
    "bInfo": false,
    "bAutoWidth": false,
    "ordering": false, 
  }
  );
</script>

<script type="text/javascript">
  $('#sample-data-tableee').DataTable({
    dom: 'lBfrtip',
    buttons: [
        'excel', 'csv'
    ],
    // "columnDefs": [
    //   {"className": "dt-center", "targets": "_all"}
    // ],
    "bPaginate": false,
    "bLengthChange": false,
    "bFilter": false,
    "bInfo": false,
    "bAutoWidth": false,
    "ordering": false, 
  }
  );
</script>

   </body>
</html>

