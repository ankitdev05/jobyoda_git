<?php include('header.php'); ?>

<style>
   .preview .table thead th{
      vertical-align: center!important;
   }
   .preview .table tbody th{
      vertical-align: center!important;
   }
   .table th, .table td {
       padding: 0rem 1.2rem!important;
      
   }
   .custom_pagination{
       background-color: green;
       color: #fff;
       padding: 8px 8px 8px 8px;
       margin-right: 50px;
       font-weight: 800;
   }
   .custom_pagination.disablecolor {
      background-color: #00801040;
      color: #fff;
   }
   .forminput{
      border: 1px solid #ccc!important;
    padding: 8px;
    width: 300px;
    font-size: 18px;
    margin-top: 20px;
   }
   .formbutton{
          font-size: 18px;
    padding: 8px;
    margin-right: 20px;
    margin-top: 20px;
    cursor: pointer;
   }
   .formexportbutton{
       font-size: 14px;
       padding: 10px;
       margin-right: 20px;
       margin-top: 20px;
       cursor: pointer;
       float: right;
       background-color: #000;
       color: #fff;
   }
</style>

<div class="content custom-scrollbar">
                  <div class="doc data-table-doc page-layout simple full-width">
                     <!-- HEADER -->
                     <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
                        <!-- APP TITLE -->
                        <div class="col-12 col-sm">
                           <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                              <div class="mainheadicos">
                                 <i class="icon s-4 icon-account-search"></i>
                              </div>
                              <div class="logo-text">
                                 <div class="h4">JobSeekers Listing</div>
                                 <div class="">Total: <?php if($row_count){echo $row_count; }?></div>
                              </div>
                           </div>
                        </div>
                        <!-- / APP TITLE -->
                        <div class="col-auto">
                          <!-- <a href="<?php //echo base_url();?>administrator/recruiter/addRecruiter" class="btn btn-light">ADD NEW</a>-->
                        </div>
                     </div>
                     <!-- / HEADER -->
                     <!-- CONTENT -->
                     <div class="page-content p-6">
                        <div class="content container">
                           <div class="row">
                              <div class="col-12">
                                 <div class="example ">
                                    <div class="source-preview-wrapper">

                                       <div class="row">
                                          <div class="col-12">
                                             <a href="<?php echo base_url();?>administrator/JobSeeker/export_csv_report" target="_blank" class="formexportbutton"> Export </a>
                                          </div>
                                          <div class="col-12">

                                             <form method="post" action="<?php echo base_url();?>administrator/jobseekerlist/1" style="float: right;">
                                                   <input type="text" name="search" placeholder="Search" class="forminput">
                                                   <button type="submit" name="submit" value="submit" class="formbutton"> Search </button>
                                                   <button type="submit" name="clear" value="clear" class="formbutton"> Reset </button>
                                             </form>
                                          </div>
                                       </div>
                                       
                                       <div class="preview" style="min-width: 1000px;overflow-x: scroll;">
                                          <div class="preview-elements">
                                             <table id="sample-data-table" class="table">
                                                <thead>
                                                   <tr>
                                                      <th class="secondary-text" style="width:40px;">
                                                         <div class="table-header">
                                                            <span class="column-title">SNo</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Name</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text" >
                                                         <div class="table-header">
                                                            <span class="column-title">Email</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Phone</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Registration Date</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Experience (Year)</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Experience (Month)</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Education</span>
                                                         </div>
                                                      </th>
                                                      
                                                      <th class="secondary-text" style="width: 300px;">
                                                         <div class="table-header">
                                                            <span class="column-title">Location</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">State</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">City</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Interested In</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Benefits</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Salary</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Newsletter</span>
                                                         </div>
                                                      </th>
                                                     
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Job Level</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Industry</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Specialization</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Sub-Specialization</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">25 MBPS Internet</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Work Mode</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Vaccination</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Relocate</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Platform</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Job Search status</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Last company</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">App Version</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Last Used</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Signup Via</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Action</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">App sign in bonus</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Bonus Count</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Withdraw</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Bonus History</span>
                                                         </div>
                                                      </th>
                                                   </tr>
                                                </thead>

                                                <tbody>
                                                <?php 
                                                   $sno = $rowno;
                                                   foreach($result as $data) {

                                                      //if($data['phone'] != 0) {
                                                ?>
                                                   <tr>
                                                      <td> <?php echo $sno; ?> </td>
                                                      <td> <?php echo $data['name']; ?> </td>
                                                      <td> <?php echo $data['email']; ?> </td>
                                                      <td> <?php echo $data['phone']; ?> </td>
                                                      <td> <?php echo date('Y-m-d',strtotime($data['created_at'])); ?> </td>
                                                      <td>
                                                         <?php
                                                         if($data['exp_year']>1){ 
                                                              echo $data['exp_year'].' Years '; 
                                                         }else{ 
                                                              echo $data['exp_year'].' Year ' ;
                                                         }
                                                         ?>
                                                      </td>
                                                      <td>
                                                         <?php
                                                            if($data['exp_month']>1) { 
                                                               echo $data['exp_month'].' Months '; 
                                                            }else{ 
                                                               echo $data['exp_month'].' Month ';
                                                            }
                                                         ?>
                                                      </td>
                                                      <td> <?php echo $data['education']; ?> </td>
                                                      <td style="width: 300px;"> <?php echo $data['location']; ?> </td>
                                                      <td> <?php echo $data['state']; ?> </td>
                                                      <td> <?php echo $data['city']; ?> </td>
                                                      <td> <?php echo $data['jobsInterested']; ?> </td>
                                                      <td> <?php echo $data['jobsbenefits']; ?> </td>
                                                      <td> <?php echo $data['current_salary']; ?> </td>
                                                      <td>
                                                         <?php 
                                                            if($data['newsletters'] == 1) {
                                                               echo $rownewsletters = "Yes"; 
                                                            } else {
                                                               echo $rownewsletters = "No"; 
                                                            }
                                                         ?>
                                                      </td>
                                                      <td> <?php echo $data['jobLevel']; ?> </td>
                                                      <td> <?php echo $data['industry']; ?> </td>
                                                      <td> <?php echo $data['specialization']; ?> </td>
                                                      <td> <?php echo $data['sub_specialization']; ?> </td>
                                                      <td> <?php echo $data['internetspeed']; ?> </td>

                                                      <td> <?php echo $data['work_mode']; ?> </td>
                                                      <td> <?php echo $data['vaccination']; ?> </td>
                                                      <td> <?php echo $data['relocate']; ?> </td>
                                                      <td>
                                                         <?php
                                                            if(!empty($data['platform'])) { 
                                                               
                                                               if($data['platform']=='ios') {  
                                                                     echo $data['platform'] = "iOS";
                                                               } elseif ($data['platform']=='android') {
                                                                     echo $data['platform'] = "Android"; 
                                                               
                                                               } elseif ($data['platform']=='web') {
                                                                  echo $data['platform'] = "Web"; 
                                                               } 
                                                            } else {

                                                               if(!empty($data['type']) && $data['type']=='apple') {
                                                                  
                                                                  echo $data['platform'] = "iOS";

                                                               } else { 

                                                                  echo $data['platform'] = "Web";
                                                               }
                                                            }
                                                         ?>
                                                      </td>
                                                      <td>
                                                         <?php
                                                            if($data['jobsearch_status'] == 1) {
                                                               echo "Actively seeking";
                                                            } else if($data['jobsearch_status'] == 2) {
                                                               echo "Open to offers";
                                                            }  else if($data['jobsearch_status'] == 3) {
                                                               echo "Exploring";
                                                            }
                                                         ?>
                                                      </td>
                                                      <td>
                                                         <?php
                                                            if(strlen($data['topbpo']) > 0) {
                                                                 echo $lastCompany =  $data['topbpo'];
                                                             } else {
                                                                 echo $lastCompany = "";
                                                             }
                                                         ?>
                                                      </td>
                                                      <td> <?php echo $data['app_version']; ?> </td>
                                                      <td> <?php echo $data['last_used']; ?> </td>
                                                      <td> 
                                                         <?php
                                                            if(!empty($data['type'])) { 

                                                               if($data['type']=='gmail') { 
                                                                  echo $data['type'] = "Gmail";
                                                               
                                                               } elseif($data['type']=='facebook') { 
                                                                  echo $data['type'] = "Facebook";
                                                               
                                                               } elseif($data['type']=='apple') { 
                                                                  echo $data['type'] = "Apple";
                                                               
                                                               } else {
                                                                  echo $data['type'] = "Normal";
                                                               }
                                                            }
                                                         ?>
                                                      </td>
                                                      <td> 
                                                         <a title="View" href="<?php echo base_url(); ?>administrator/JobSeeker/fetchJobseeker?id=<?php echo base64_encode($data['id']); ?>" class="btn btn-icon usricos" aria-label="Product details"><i class="icon icon-eye s-4"></i></a> 
                                                         <a title="Active" href="#" data-toggle="modal" id="<?php echo $data['id'];?>" onclick="getaid(this.id)" data-target="#activeModal" class="btn btn-icon usricos" ><i class="icon icon-checkbox-marked-circle-outline s-4"></i></a>
                                                         <a title="Delete" href="#" data-toggle="modal" id="<?php echo $data['id'];?>" onclick="getrid(this.id)" data-target="#myModal1" class="btn btn-icon usricos" ><i class="icon s-4 icon-trash"></i></a>
                                                      </td>
                                                      <td>
                                                         <a title="App Sign in bonus" data-toggle="modal" id="<?php echo $data['id'];?>" onclick="getBonus(this.id)" data-target="#modalBonus" class="btn btn-icon usricos fuse-ripple-ready" aria-label="Product details"><i class="icon icon-login s-4"></i></a>
                                                      </td>
                                                      <td> <?php echo $data['countid']; ?> </td>
                                                      <td> 
                                                         <a title="Withdraw" href="#" data-toggle="modal" id="<?php echo $data['id'];?>" onclick="getuid(this.id)" data-target="#myModal2" class="btn btn-icon usricos fuse-ripple-ready" aria-label="Product details"><i class="icon icon-looks s-4"></i></a>
                                                      </td>
                                                      <td>
                                                         <a title="Bonus History" href="#" data-toggle="modal" id="<?php echo $data['id'];?>" onclick="getid2(this.id)" data-target="#myModal3" class="btn btn-icon usricos fuse-ripple-ready" aria-label="Product details"><i class="icon icon-history s-4"></i></a>
                                                      </td>
                                                   </tr>
                                                <?php
                                                       $sno++;
                                                      //}
                                                   }
                                                ?>
                                                </tbody>
                                                
                                             </table>
                                             
                                          </div>
                                       </div>

                                       <!-- Paginate -->
                                       <div style='margin-top: 10px;'>
                                          <div class="row">
                                             <div class="col-12 text-center" style="margin-bottom: 40px;margin-top: 20px;">
                                                
                                                <a href="<?php echo base_url(); ?>administrator/jobseekerlist/<?php if($row == 1){echo "1";}else{echo $row-1;}?>" class="custom_pagination <?php if($row == 1){ echo "disablecolor"; }else{ echo "activecolor"; } ?>">Previous</a> 
                                                
                                                <a href="<?php echo base_url(); ?>administrator/jobseekerlist/<?php if($row >= 1 && $row<=$num_links){ echo $row+1;} else if($num_links == 0){ echo "1"; } else { echo $num_links; } ?>" class="custom_pagination <?php if($row >= 1 && $row<=$num_links){ echo "activecolor"; }else if($num_links == 0){ echo "disablecolor"; }else{ echo "disablecolor"; } ?>">Next</a>

                                             </div>
                                          </div>
                                       </div>

                                       <div class="source custom-scrollbar">
                                          <div class="highlight">
                                             <pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-html" data-lang="html"></code></pre>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- CONTENT -->
                  </div>
               </div>
            </div>
            <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
               <div class="list-group" class="date">
                  <div class="list-group-item subheader">TODAY</div>
                  <div class="list-group-item two-line">
                     <div class="text-muted">
                        <div class="h1"> Friday</div>
                        <div class="h2 row no-gutters align-items-start">
                           <span> 5</span>
                           <span class="h6">th</span>
                           <span> May</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Events</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Group Meeting</h3>
                        <p>In 32 Minutes, Room 1B</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Public Beta Release</h3>
                        <p>11:00 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Dinner with David</h3>
                        <p>17:30 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Q&amp;A Session</h3>
                        <p>20:30 PM</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Notes</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Best songs to listen while working</h3>
                        <p>Last edit: May 8th, 2015</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Useful subreddits</h3>
                        <p>Last edit: January 12th, 2015</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Quick Settings</div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Notifications</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Cloud Sync</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Retro Thrusters</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
            <!--<a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
            <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
            </a>-->
         </nav>
      </main>

<div class="modal" id="myModal1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete JobSeeker</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Do you want to delete this JobSeeker?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a href="" class="btn btn-primary" id="createLink">Delete</a>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="blockModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Block JobSeeker</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Do you want to block this JobSeeker?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a href="" class="btn btn-primary" id="createbLink">Block</a>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="activeModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Active JobSeeker</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Do you want to active this JobSeeker?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a href="" class="btn btn-primary" id="createaLink">Active</a>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="myModal2" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Withdraw Bonus</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="bonus_message"></p>
            </div>
            <div class="modal-footer">
                <a href="" class="btn btn-secondary" id="createwLink">Yes</a> 
                <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                
            </div>
        </div>
    </div>
</div>

<div class="modal" id="myModal3" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Bonus History</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body bonushstry">
                <table id="sample-data-table" class="table">
                                                <thead>
                                                   <tr>
                                                      <th class="secondary-text" style="width:40px;">
                                                         <div class="table-header">
                                                            <span class="column-title">S No</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Name</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text" >
                                                         <div class="table-header">
                                                            <span class="column-title">Bonus</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Added At</span>
                                                         </div>
                                                      </th>
                                                      
                                                   </tr>
                                                </thead>
                                                <tbody id="bonus_history"> 
                                                   <!-- <?php
                                                      $x1 =1;
                                                      foreach($Lists1 as $List) {
                                                      ?>
                                                   <tr>
                                                      <td style="width:20px;"><?php echo $x1;?></td>
                                                      <td style="width:20px;"><?php echo $List['name'];?></td>
                                                      <td style="width:20px;"><?php echo $List['bonus'];?></td>
                                                      <td style="width:20px;"><?php echo $List['added_at'];?></td>   
                                                      
                                                   </tr>
                                                   <?php
                                                      $x1++;
                                                      }
                                                      ?> -->
                                                </tbody>
                                             </table>
                                             <script type="text/javascript">
                                                //$('#sample-data-table').DataTable();
                                             </script>
            </div>
            <div class="modal-footer"> 
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                
            </div>
        </div>
    </div>
</div>

 <!-- Modal -->
<div id="modalBonus" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <h3>Sign In Bonus</h3>
        <form method="post" action="<?php echo base_url();?>administrator/JobSeeker/bonusamountInsert">
            <div class="form-group">
                <input type="number" name="bonus_amount" class="form-control" placeholder="Enter Amount" required="">
                <label>Amount</label>
                <input type="hidden" name="userId" id="userId">
            </div>
            
            <button type="submit" class="btn btn-info">Submit</button>
            
        </form>

      </div>
      <div class="modal-footer">
         
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
    function getrid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>administrator/JobSeeker/deleteJobSeeker?id="+rid;
        var aa = document.getElementById('createLink');
        aa.setAttribute("href", link);
    }
    function getbid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>administrator/JobSeeker/jobseekerBlock?id="+rid;
        var aa = document.getElementById('createbLink');
        aa.setAttribute("href", link);
    }

    function getaid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>administrator/JobSeeker/jobseekerActive?id="+rid;
        var aa = document.getElementById('createaLink');
        aa.setAttribute("href", link);
    }

    function getuid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>administrator/JobSeeker/withdrawBonus?id="+rid;
        var aa = document.getElementById('createwLink');
        aa.setAttribute("href", link);
        $.ajax({
         type:"POST",
         url:"<?php echo base_url(); ?>/administrator/JobSeeker/fetchBonusAmount",
         data:{
            rid:rid
         },
         success:function(data){
            if(data>0){
               $('#bonus_message').html('Do you want to withdraw the Bonus Amount of'+ data +' Peso ?');
               $('#createwLink').show();
            }else{
               $('#bonus_message').html('You donot have sufficient balance to withdraw');
               $('#createwLink').hide();
            }
            
         }
        });
    }

    function getBonus(rid){
      var userId = document.getElementById('userId');
      userId.setAttribute("value",rid);
    }

    function getid2(id) {
        var rid = id;
        //alert(id);
          var url =   '<?php echo base_url(); ?>administrator/JobSeeker/fetchbonusHistory';
       
          $.ajax({
                   type:"POST",
                   url:url,
                   data:{
                       rid : rid
                   },

                   success:function(data)

                    {
                     //alert(data);
                     
                  $("#bonus_history").html(data);
                    }
                });
    }


</script>
   </body>
</html>

