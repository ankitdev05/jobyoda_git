<?php include('header.php'); ?>

<style type="text/css">
  .table thead .column-title {
      font-size: 12px!important;
  }
  .dt-buttons {
    margin-bottom: 10px;
  }
  .tablinks{
      background-color: #3baf62;
      font-weight: 700;
      color: #fff;
      padding: 7px 15px 9px 15px;
      margin-right: 20px;
  }
  .tablinks.activetab {
    background-color: #0a4c89!important;
  }

  table.dataTable tbody tr td {
    padding-left: 24px!important;
  }

  table.dataTable thead tr th {
    padding-left: 24px!important;
  }
</style>

               <div class="content custom-scrollbar">
                  <div class="doc data-table-doc page-layout simple full-width">
                     <!-- HEADER -->
                     <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
                        <!-- APP TITLE -->
                        <div class="col-12 col-sm">
                           <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                              <div class="logo-icon mr-3 mt-1">
                                 <i class="icon s-6 icon-person-box"></i>
                              </div>
                              <div class="logo-text">
                                 <div class="h4">Company Wise Data Metrics</div>
                                 <div class=""></div>
                              </div>
                           </div>
                        </div>
                        <!-- / APP TITLE -->
                        <!--<div class="col-auto">
                           <a href="<?php echo base_url();?>administrator/recruiter/addRecruiter" class="btn btn-light">ADD NEW</a>
                        </div>-->
                     </div>
                     <!-- / HEADER -->
                     <!-- CONTENT -->
                     <div class="page-content p-6">
                        <div class="content container">
                           <div class="row">
                              <div class="col-12">
                                 <div class="example ">

                                    <div class="source-preview-wrapper">
                                       <div class="preview">
                                          <div class="preview-elements companyldtf">

                                             <div>
                                                <form method="get" action="<?php echo base_url();?>administrator/company_wise_data_metrics">
                                                   <span style="font-size: 18px;">Companies</span>
                                                   <select name="company" style="padding: 6px 5px 6px 5px;">
                                                      <option value=""> Select Company </option>
                                                      <?php
                                                         $c=0;
                                                         foreach($totalCompany as $totalComp) {
                                                      ?>
                                                            <option value="<?php echo $totalComp['company_id'];?>" <?php if(isset($_GET['company'])) { if($_GET['company'] == $totalComp['company_id']){ echo "selected"; } } else { if($c==0) { echo "selected"; } } ?>> <?php echo $totalComp["company_name"];?> </option>
                                                      <?php
                                                            $c++;
                                                         }
                                                      ?>
                                                   </select>
                                                   <input type="submit" class="btn btn-primary" value="Search">
                                                </form>
                                             </div>
                                             
                                             <?php
                                                //if(isset($_GET['company'])) {
                                             ?>


                                             <table id="sample-data-tableee" class="table cell-border compact hover nowrap stripe">
                                                <thead>
                                                  
                                                  <tr>
                                                    <th class="secondary-text" colspan="2">
                                                       <div class="table-header">
                                                          <span class="column-title"> Metric </span>
                                                       </div>
                                                    </th>
                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title"> Jan </span>
                                                       </div>
                                                    </th>
                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title"> Feb </span>
                                                       </div>
                                                    </th>
                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title"> Mar </span>
                                                       </div>
                                                    </th>
                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title"> Apr </span>
                                                       </div>
                                                    </th>
                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title"> May </span>
                                                       </div>
                                                    </th>
                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title"> June </span>
                                                       </div>
                                                    </th>
                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title"> July </span>
                                                       </div>
                                                    </th>
                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title"> Aug </span>
                                                       </div>
                                                    </th>
                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title"> Sept </span>
                                                       </div>
                                                    </th>
                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title"> Oct </span>
                                                       </div>
                                                    </th>
                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title"> Nov </span>
                                                       </div>
                                                    </th>
                                                    <th class="secondary-text">
                                                       <div class="table-header">
                                                          <span class="column-title"> Dec </span>
                                                       </div>
                                                    </th>

                                                  </tr>

                                                </thead>   

                                                <tbody>
                                                
                                                <?php
                                                  $yy = 1;
                                                  foreach($totalYear as $totalY) {
                                                ?>
                                                  <tr>
                                                  
                                                  <?php if($yy == 1) { ?>
                                                    <td rowspan="<?php echo count($totalYear); ?>">Jobs</td>
                                                  <?php } ?>
                                                    
                                                    <td><?php echo $totalY; ?></td>

                                                  <?php  
                                                    foreach($totalJobs[$totalY] as $totalJob) {
                                                  ?>

                                                    <td><?php echo $totalJob['record']; ?></td>

                                                  <?php } ?>

                                                  </tr>

                                                <?php
                                                    $yy++;
                                                  }
                                                ?>

                                                <tr style="height:30px">
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                </tr>

                                                <?php
                                                  $yy = 1;
                                                  foreach($totalYear as $totalY) {
                                                ?>
                                                  <tr>
                                                  
                                                  <?php if($yy == 1) { ?>
                                                    <td rowspan="<?php echo count($totalYear); ?>">Active Jobs</td>
                                                  <?php } ?>
                                                    
                                                    <td><?php echo $totalY; ?></td>

                                                  <?php  
                                                    foreach($totalActives[$totalY] as $totalActive) {
                                                  ?>

                                                    <td><?php echo $totalActive['record']; ?></td>

                                                  <?php } ?>

                                                  </tr>

                                                <?php
                                                    $yy++;
                                                  }
                                                ?>

                                                <tr style="height:30px">
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                </tr>

                                                <?php
                                                  $yy = 1;
                                                  foreach($totalYear as $totalY) {
                                                ?>
                                                  <tr>
                                                  
                                                  <?php if($yy == 1) { ?>
                                                    <td rowspan="<?php echo count($totalYear); ?>">Jobs expiring in the next 7days</td>
                                                  <?php } ?>
                                                    
                                                    <td><?php echo $totalY; ?></td>

                                                  <?php  
                                                    foreach($totalExpires[$totalY] as $totalExpire) {
                                                  ?>

                                                    <td><?php echo $totalExpire['record']; ?></td>

                                                  <?php } ?>

                                                  </tr>

                                                <?php
                                                    $yy++;
                                                  }
                                                ?>

                                                <tr style="height:30px">
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                </tr>

                                                <?php
                                                  $yy = 1;
                                                  foreach($totalYear as $totalY) {
                                                ?>
                                                  <tr>
                                                  
                                                  <?php if($yy == 1) { ?>
                                                    <td rowspan="<?php echo count($totalYear); ?>">Applications</td>
                                                  <?php } ?>
                                                    
                                                    <td><?php echo $totalY; ?></td>

                                                  <?php  
                                                    foreach($totalApplicationInJobs[$totalY] as $totalApplicationInJob) {
                                                  ?>

                                                    <td><?php echo $totalApplicationInJob['record']; ?></td>

                                                  <?php } ?>

                                                  </tr>

                                                <?php
                                                    $yy++;
                                                  }
                                                ?>

                                                <tr style="height:30px">
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                </tr>

                                                <?php
                                                  $yy = 1;
                                                  foreach($totalYear as $totalY) {
                                                ?>
                                                  <tr>
                                                  
                                                  <?php if($yy == 1) { ?>
                                                    <td rowspan="<?php echo count($totalYear); ?>">- Instant Screening</td>
                                                  <?php } ?>
                                                    
                                                    <td><?php echo $totalY; ?></td>

                                                  <?php  
                                                    foreach($totalInstantJobs[$totalY] as $totalInstantJob) {
                                                  ?>

                                                    <td><?php echo $totalInstantJob['record']; ?></td>

                                                  <?php } ?>

                                                  </tr>

                                                <?php
                                                    $yy++;
                                                  }
                                                ?>

                                                <tr style="height:30px">
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                </tr>

                                                <?php
                                                  $yy = 1;
                                                  foreach($totalYear as $totalY) {
                                                ?>
                                                  <tr>
                                                  
                                                  <?php if($yy == 1) { ?>
                                                    <td rowspan="<?php echo count($totalYear); ?>"> - Phone Screening</td>
                                                  <?php } ?>
                                                    
                                                    <td><?php echo $totalY; ?></td>

                                                  <?php  
                                                    foreach($totalPhoneJobs[$totalY] as $totalPhoneJob) {
                                                  ?>

                                                    <td><?php echo $totalPhoneJob['record']; ?></td>

                                                  <?php } ?>

                                                  </tr>

                                                <?php
                                                    $yy++;
                                                  }
                                                ?>

                                                <tr style="height:30px">
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                </tr>

                                                <?php
                                                  $yy = 1;
                                                  foreach($totalYear as $totalY) {
                                                ?>
                                                  <tr>
                                                  
                                                  <?php if($yy == 1) { ?>
                                                    <td rowspan="<?php echo count($totalYear); ?>"> - Walk-In</td>
                                                  <?php } ?>
                                                    
                                                    <td><?php echo $totalY; ?></td>

                                                  <?php  
                                                    foreach($totalWalkInJobs[$totalY] as $totalWalkInJob) {
                                                  ?>

                                                    <td><?php echo $totalWalkInJob['record']; ?></td>

                                                  <?php } ?>

                                                  </tr>

                                                <?php
                                                    $yy++;
                                                  }
                                                ?>

                                                <tr style="height:30px">
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                </tr>

                                                <?php
                                                  $yy = 1;
                                                  foreach($totalYear as $totalY) {
                                                ?>
                                                  <tr>
                                                  
                                                  <?php if($yy == 1) { ?>
                                                    <td rowspan="<?php echo count($totalYear); ?>"> Notifications</td>
                                                  <?php } ?>
                                                    
                                                    <td><?php echo $totalY; ?></td>

                                                  <?php  
                                                    foreach($totalNotifyInJobs[$totalY] as $totalNotifyInJob) {
                                                  ?>

                                                    <td><?php echo $totalNotifyInJob['record']; ?></td>

                                                  <?php } ?>

                                                  </tr>

                                                <?php
                                                    $yy++;
                                                  }
                                                ?>

                                                </tbody>
                                             </table>


                                             <?php
                                                //}
                                             ?>

                                          </div>
                                       </div>

                                       <div class="source custom-scrollbar">
                                          <div class="highlight">
                                             <pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-html" data-lang="html"></code></pre>
                                          </div>
                                       </div>

                                    </div>


                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- CONTENT -->
                  </div>
               </div>
            </div>
            
         </div>
         <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
            <!--<a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
            <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
            </a>-->
         </nav>
      </main>

<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>


<script type="text/javascript">
  $('#sample-data-table').DataTable({
    dom: 'lBfrtip',
    buttons: [
        'excel', 'csv'
    ],
    "bPaginate": false,
    "bLengthChange": false,
    "bFilter": false,
    "bInfo": false,
    "bAutoWidth": false,
    "ordering": false, 
  }
  );
</script>

<script type="text/javascript">
  $('#sample-data-tableee').DataTable({
    dom: 'lBfrtip',
    buttons: [
        'excel', 'csv'
    ],
    // "columnDefs": [
    //   {"className": "dt-center", "targets": "_all"}
    // ],
    "bPaginate": false,
    "bLengthChange": false,
    "bFilter": false,
    "bInfo": false,
    "bAutoWidth": false,
    "ordering": false, 
  }
  );
</script>

   </body>
</html>

