<style>


.preview input[type="checkbox"], input[type="radio"] {
position: absolute;
opacity: 0;
z-index: 9;
margin: 0;
left: 0;
right: 0;
padding: 46px 0;
height: 14px;
width: 19px;
margin: auto;
}

input[type="checkbox"] {
    position: inherit!important;
    opacity: 1!important;
    left: 0;
    right: 0;
    padding: 32px 0;
    height: 38px;
    width: 37px;
    margin: auto;
}

.send_button {
float: right;
width: 100%;
margin-bottom: 20px;
padding: 0px 26px;
}
.send_button button {
float: right;
padding: 10px 13px;
background: #0a4c89;
color: #fff;
border-radius: 10px;
}

.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
input.search_init {
    margin-left: .5em;
    border: 1px solid rgba(0,0,0,0.12);
    padding: 6px 10px;
    font-size: 13px;
    border-radius: 4px;
}

input.search_init {
    margin-left: .5em!important;
    border: 1px solid rgba(0,0,0,0.12)!important;
    padding: 6px 10px!important;
    font-size: 13px!important;
    border-radius: 4px!important;
}

.FilterBox .panel-default{
  float: left; 
  width: 100%;  
  background-color: #f7f6f6;  
  padding: 25px;
}

form#form-filter{}

form#form-filter label{
  display: block;
  color: #000;
  font-weight: 500;
}

form#form-filter .form-control{
  border: 1px solid #ddd;
  box-shadow: none;
  height: 34px;
  padding: 2px 14px;
  border-radius: 5px;
  background-color: #fff;
}

form#form-filter .FilterButton{}

form#form-filter .FilterButton button{
  box-shadow: none;
  padding: 0 30px;
  min-width: inherit;
  margin: 0 10px 0 0;
}


form#form-filter .FilterButton button#btn-reset{
  background: #c8c8c8;
}

</style>

<?php include('header.php'); ?>
<div class="content custom-scrollbar">
  <div class="doc data-table-doc page-layout simple full-width">
   <!-- HEADER -->
    <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
      <!-- APP TITLE -->
      <div class="col-12 col-sm">
        <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
          <div class="mainheadicos">
            <i class="icon s-4 icon-bell"></i>
          </div>
          <div class="logo-text">
            <div class="h4">Notification Management</div> 
          </div>
        </div>
      </div>
      
      <!-- / APP TITLE -->
      <div class="col-auto">
        <!-- <a href="<?php echo base_url();?>administrator/recruiter/addRecruiter" class="btn btn-light">ADD NEW</a>-->
      </div>
    </div>
    <!-- / HEADER -->


<!-- CONTENT -->
<div class="page-content p-6">
  <div class="content container">
    <div class="row">
      <div class="col-12">
        <div class="example ">
          <div class="source-preview-wrapper">
            <div class="preview">
              <div class="preview-elements notifds">
                <div class="send_button">
                  <button class="snotify" data-target="#myModal2" data-toggle="modal">Send Notification</button>
                </div>

                <div class="FilterBox">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">Custom Filter : </h4>
                    </div>
                    <div class="panel-body">
                      <form id="form-filter" class="form-horizontal">
                        <div class="row">
                          <div class="col-4">
                            <label for="superpower" class="control-label">Super Power</label>
                            <input type="text" class="form-control" id="superpower" placeholder="Enter Super Power">
                          </div>
                          <div class="col-4">
                            <label for="Loc" class="control-label">Location</label>
                            <input type="text" class="form-control" id="location" placeholder="Enter Location">
                          </div>
                          <div class="col-4">
                            <div class="col-sm-12">
                              <div class="FilterButton">
                                <label>&nbsp;</label>
                                <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                                <button type="button" id="btn-reset" class="btn btn-default">Reset</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>

                                             <table id="sample-data-table" class="table">

                                                <thead>
                                                   <tr>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title"><input type="checkbox" id="check_all" ></span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">SNo</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Name</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text" >
                                                         <div class="table-header">
                                                            <span class="column-title">Email</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Phone</span>
                                                         </div>
                                                      </th><th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Platform</span>
                                                         </div>
                                                      </th><th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Last Used</span>
                                                         </div>
                                                      </th><th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">App Version</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Superpower</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Location</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Salary</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Experience</span>
                                                         </div>
                                                      </th>
                                                      
                                                   </tr>
                                                </thead>
                                                
                                                
                                                
                                             </table>
                                             </div>
                                             
                                          <script type="text/javascript">

                                            $(document).ready(function() {
                                                // DataTable
                                                var table = $('#sample-data-table').DataTable({
                                                  "lengthMenu": [[10, 25, 50,100,200, 10000], [10, 25, 50, 100, 200, "All"]],
                                                  'processing': true,
                                                   'serverSide': true,
                                                   "order":[],  
                                                   'ajax': {
                                                       'url':'<?php echo base_url('administrator/JobSeeker/');?>fetchnotificationAjax',
                                                       'type':"POST",
                                                       "data": function ( data ) {
                                                            data.superpower = $('#superpower').val();
                                                            data.location = $('#location').val();
                                                        }
                                                   },
                                                   "columnDefs":[  
                                                        {  
                                                              "targets": [ 0 ],
                                                              "orderable":false,  
                                                        },  
                                                    ],
                                              });

                                              $('#btn-filter').click(function(){ //button filter event click
                                                  $("#selected_superpower").val($('#superpower').val());
                                                  $("#selected_location").val($('#location').val());
                                                  table.ajax.reload();  //just reload table
                                              });
                                              $('#btn-reset').click(function(){ //button reset event click
                                                  $('#form-filter')[0].reset();
                                                  table.ajax.reload();  //just reload table
                                              });
                                            });

                                          </script>

                                          </div>
                                       </div>
                                       <div class="source custom-scrollbar">
                                          <div class="highlight">
                                             <pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-html" data-lang="html">
    
                                                    </code></pre>
                                          </div>
                                       </div>
                                    </div>
                                  
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- CONTENT -->
                  </div>
               </div>

               
            </div>
            <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
               <div class="list-group" class="date">
                  <div class="list-group-item subheader">TODAY</div>
                  <div class="list-group-item two-line">
                     <div class="text-muted">
                        <div class="h1"> Friday</div>
                        <div class="h2 row no-gutters align-items-start">
                           <span> 5</span>
                           <span class="h6">th</span>
                           <span> May</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Events</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Group Meeting</h3>
                        <p>In 32 Minutes, Room 1B</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Public Beta Release</h3>
                        <p>11:00 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Dinner with David</h3>
                        <p>17:30 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Q&amp;A Session</h3>
                        <p>20:30 PM</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Notes</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Best songs to listen while working</h3>
                        <p>Last edit: May 8th, 2015</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Useful subreddits</h3>
                        <p>Last edit: January 12th, 2015</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Quick Settings</div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Notifications</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Cloud Sync</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Retro Thrusters</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
            <!--<a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
            <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
            </a>-->
         </nav>
      </main>



<div class="modal" id="myModal2" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Send Notifications</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post">
                  <div class="form-group">
                     <input type="text" name="title" id="title" placeholder="Enter Title" class="form-control" minlength="30" maxlength="60" required="">
                     <label>Title</label>
                  </div>
                  <div class="form-group">
                     <input type="text" name="Message" id="message" placeholder="Enter Message" class="form-control" maxlength="120" required="">
                     <label>Message</label>
                  </div>
                  <div>
                    <h5>Save In-app</h5>
                    <label class="switch" style="width:66px;">
                        <input type="checkbox" style="visibility: hidden;" name="save_status" id="save_status">
                        <span class="slider round" ></span>
                    </label>
                  </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" value="" name="superpower" id="selected_superpower">
                <input type="hidden" value="" name="location" id="selected_location">
                <button type="button" id="send_btn" class="btn btn-secondary" onclick="getids()">Send Notification</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                
            </div>
             </form>
        </div>
    </div>
</div>


                  
                  <div class="Loader" style="display: none;">
                    <img src="<?php echo base_url(); ?>webfiles/img/Loader.gif" width="200px"> 
                  </div>

<style type="text/css">
  .Loader {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.5);
    z-index: 9999;
    align-items: center;
    justify-content: center;
    margin: auto;
    display: flex;
}
</style>
<script type="text/javascript">

    function getids(){
      var checkedValues = $('input:checkbox:checked').map(function() {
        if(this.value !== "on"){
            return this.value;
        }
    
      }).get();
      //console.log(checkedValues);exit();
      if(checkedValues==""){
         alert('Please select at least one checkbox');
      }else{
         var title = $('#title').val();
         var message = $('#message').val();
          var url =   '<?php echo base_url(); ?>/administrator/JobSeeker/send_notification';
          if ($('#save_status').is(':checked')) {
          var save_status = 1;
         }else{
          var save_status = 0;
         }
          if(title==""){
            alert('Please enter title');
            return false;
          }else if(message==''){
            alert('Enter Message');
            return false;
          
          } else {

              if(title.length < 30 || title.length > 60) {
                  alert('Title must be minimum 30 and maximum 60 Characters');
                  return false;

              } else {
                  $.ajax({
                    type:"POST",
                    url:url,
                    data:{
                       title : title,
                       message : message, 
                       save_status : save_status,
                       superpower : $('#selected_superpower').val(),
                       location : $('#selected_location').val(),
                       ids : JSON.stringify(checkedValues),
                    },
                    beforeSend: function() {
                        // setting a timeout
                        $('.Loader').css('display','flex');
                    },
                    success:function(data)
                    {
                      //console.log(data);
                      $('.Loader').css('display','none');
                      $('#myModal2').modal('hide');
                    }
                  });
              }
          }
      }
      
    }


$('#check_all').on('click',function(){
        if(this.checked){
            $('.check_users').each(function(){
                this.checked = true;
            });
        }else{
             $('.check_users').each(function(){
                this.checked = false;
            });
        }
    });
</script>

<script type="text/javascript">
  $(document).ready(function(){
    $(".check_users").click(function(){
      var numberOfChecked = $('.check_users:checked').length;
      //var totalCheckboxes = $('input:checkbox').length;
      if(numberOfChecked >= 1) {
          $(".snotify").css("display","block");
      } else{
        $(".snotify").css("display","none");
      }
    });
  });
</script>
   </body>
</html>

