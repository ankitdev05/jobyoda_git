<style type="text/css">
    .validError{
        color: red;
    }
    .imgmsg{
      font-size: 13px;
      display: block;
      text-align: center;
      margin: 15px 0 0 0;
    }
    #register .form-wrapper {
    width: 80.4rem!important;
    max-width: 80.4rem!important;
    background: #FFFFFF;
    text-align: center;
}
</style>
<?php include('header.php'); ?>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
<div class="content custom-scrollbar">

                    <div id="register" class="p-8">

                        <div class="form-wrapper md-elevation-8 p-8">

                            <div class="title mt-4 mb-8">Add Reviews</div>

                            <form name="registerForm" action="<?php echo base_url();?>administrator/app_review_save" method="post" novalidate enctype="multipart/form-data">

                                <div class="form-group mb-4">
                                    <label>Upload Image</label>
                                    <input type="file" name="user_image" class="form-control" id="imgInp">
                                </div>

                                <span class="imgmsg">*Image dimension should be between 250X150 - 350X250, Image allowed types are jpeg, jpg, png, gif</span>

                                <p id="invalidfileizeee" style="color:red;"> <?php if(!empty($errors['user_image'])){ echo "<span class='err'>Error :".$errors['user_image']."</span>";}?> </p>

                                <br>

                                <div class="form-group mb-4">
                                    <input type="text" name="name" value="<?php if(!empty($userData['name'])){ echo $userData['name']; } ?>" class="form-control" aria-describedby="nameHelp"  >
                                    <label>Candidate Name</label>
                                    <?php if(!empty($errors['name'])){echo "<span class='validError'>".$errors['name']."</span>";}?>
                                </div>

                                <div class="form-group mb-4">
                                    <label>Comment</label>
                                    <textarea rows="5" name="comment" id="content" class="form-control" maxlength="800"></textarea>
                                    
                                    <?php if(!empty($errors['comment'])){echo "<span class='validError'>".$errors['comment']."</span>";}?>
                                </div>

                                <div class="form-group mb-4">
                                    <input type="text" name="rating" value="<?php if(!empty($userData['rating'])){ echo $userData['rating']; } ?>" class="form-control" aria-describedby="nameHelp"  >
                                    <label>Rating</label>
                                    <?php if(!empty($errors['rating'])){echo "<span class='validError'>".$errors['rating']."</span>";}?>
                                </div>

                                <div class="form-group mb-4">
                                    <label>Rating Date</label>

                                    <input type="date" id="start_date" name="rating_date" value="<?php if(!empty($userData['rating_date'])){ echo $userData['rating_date']; } ?>" class="form-control" aria-describedby="nameHelp"  >
                                    
                                    <?php if(!empty($errors['rating_date'])){echo "<span class='validError'>".$errors['rating_date']."</span>";}?>
                                </div>

                                <div class="form-group mb-4">
                                    <label>Company</label>
                                    
                                    <select name="company" class="form-control" aria-describedby="nameHelp"  >
                                        <option value=""> Select company </option>
                                        <?php
                                        foreach($comapnylisting as $comapnyl) {
                                        ?>
                                            <option value="<?php echo $comapnyl['id']; ?>"> <?php echo $comapnyl['cname']; ?> </option>
                                        <?php } ?>
                                    </select>
                                    
                                    <?php if(!empty($errors['rating_date'])){echo "<span class='validError'>".$errors['rating_date']."</span>";}?>
                                </div>

                                <div class="form-group mb-4">
                                    <label>Company Logo</label>
                                    <input type="file" name="company_image" class="form-control" id="compimgInp">
                                </div>

                                <span class="imgmsg">*Image dimension should be 130X25, Image allowed types are jpeg, jpg, png, gif</span>

                                <p id="invalidfileizeeeeeee" style="color:red;"> <?php if(!empty($errors['company_image'])){ echo "<span class='err'>Error :".$errors['company_image']."</span>";}?> </p>

                                <br>

                                <button type="submit" class="submit-button btn btn-block btn-secondary my-4 mx-auto" aria-label="LOG IN">
                                    Submit
                                </button>

                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
        </nav>
    </main>
</body>

</html>

<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type="text/javascript">
   $(function(){  
        $.datepicker.setDefaults({  
            dateFormat: 'yy-mm-dd'   
        }); 
        $("#start_date").datepicker();
        
   });
</script> 

<script type="text/javascript">
    $("#imgInp").change(function() {

      readURL(this);

      var fileUpload = document.getElementById("imgInp");
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png|.gif)$");
        if (regex.test(fileUpload.value.toLowerCase())) {

           if (typeof (fileUpload.files) != "undefined") {

              //Initiate the FileReader object.
              var reader = new FileReader();
              //Read the contents of Image File.
              reader.readAsDataURL(fileUpload.files[0]);
              reader.onload = function (e) {
                  //Initiate the JavaScript Image object.
                  var image = new Image();
                  //Set the Base64 string return from FileReader as source.
                  image.src = e.target.result;
                  //Validate the File Height and Width.
                  image.onload = function () {
                      var height = parseInt(this.height);
                      var width = parseInt(this.width);
                      
                      if (width <= 350 && height <= 250) {

                        if(width >= 250 && height >= 150) {
                            $("#invalidfileizeee").html("");
                        
                        } else {
                            $("#invalidfileizeee").html("Image size must be within 250X150 - 350X250");
                            $('#imgInp').val('');
                            $('#blah').css("display","none");
                        }
                      } else {
                          $("#invalidfileizeee").html("Image size must be within 250X150 - 350X250");
                          $('#imgInp').val('');
                          $('#blah').css("display","none");
                      }
                  };
              }
           } else {
              $("#invalidfileizeee").html("Please select a valid Image file.");
              $('#imgInp').val('');
              $('#blah').css("display","block");
           }
        } else {
           $("#invalidfileizeee").html("Please select a valid Image file.");
           $('#imgInp').val('');
           $('#blah').css("display","block");
        }

    });
</script>

<script type="text/javascript">
    $("#compimgInp").change(function() {

      readURL(this);

      var fileUpload = document.getElementById("compimgInp");
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png|.gif)$");
        if (regex.test(fileUpload.value.toLowerCase())) {

           if (typeof (fileUpload.files) != "undefined") {

              //Initiate the FileReader object.
              var reader = new FileReader();
              //Read the contents of Image File.
              reader.readAsDataURL(fileUpload.files[0]);
              reader.onload = function (e) {
                  //Initiate the JavaScript Image object.
                  var image = new Image();
                  //Set the Base64 string return from FileReader as source.
                  image.src = e.target.result;
                  //Validate the File Height and Width.
                  image.onload = function () {
                      var height = parseInt(this.height);
                      var width = parseInt(this.width);  //  100X20 - 130X25
                      
                      if (width == 130 && height == 25) {

                        $("#invalidfileizeee").html("");

                        // if(width >= 100 && height >= 20) {
                        //     $("#invalidfileizeee").html("");
                        
                        // } else {
                        //     $("#invalidfileizeeeeeee").html("Image size must be within 100X20 - 130X25");
                        //     $('#compimgInp').val('');
                        //     $('#blah').css("display","none");
                        // }
                      } else {
                          $("#invalidfileizeeeeeee").html("Image size must be within 100X20 - 130X25");
                          $('#compimgInp').val('');
                          $('#blah').css("display","none");
                      }
                  };
              }
           } else {
              $("#invalidfileizeeeeeee").html("Please select a valid Image file.");
              $('#compimgInp').val('');
              $('#blah').css("display","block");
           }
        } else {
           $("#invalidfileizeeeeeee").html("Please select a valid Image file.");
           $('#compimgInp').val('');
           $('#blah').css("display","block");
        }

    });
</script>

<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
    <script type="text/javascript">
      CKEDITOR.replace('content');
    </script>