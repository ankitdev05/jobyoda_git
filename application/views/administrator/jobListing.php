<?php include('header.php');?>
<style>
   #sample-data-table #checkid {
          position: relative!important;
          opacity: 1!important;
          padding: 24px 0!important;
          height: 18px!important;
   }
   .snotify{
      position: absolute!important;
      right: 30px!important;
      top: 10px!important;
      background: #27ae62;
   }
 
#sidenav{
   height: 95%;
   overflow: auto !important; 
} 

#sidenav::-webkit-scrollbar {
   width: 6px;
   border-radius: 10px;
}
 
#sidenav::-webkit-scrollbar-track {
   background: #0a4c89;
   border-radius: 6px;
}
 
#sidenav::-webkit-scrollbar-thumb {
   background-color: #aaa;
   border-radius: 6px;
   transition: background-color .2s linear, width .2s ease-in-out;
   -webkit-transition: background-color .2s linear, width .2s ease-in-out;
}
 
 
.jobpostind table#sample-data-table .Address_field{width: 700px !important;  }

</style>
               <div class="content custom-scrollbar">
                  <div class="doc data-table-doc page-layout simple full-width">
                     <!-- HEADER -->
                     <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
                        <!-- APP TITLE -->
                        <div class="col-12 col-sm">
                           <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                              <div class="logo-icon mr-3 mt-1">
                                 <i class="icon-briefcase s-6"></i>
                              </div>
                              <div class="logo-text">
                                 <div class="h4">Job Post Listing</div>
                                 <div class="">Total: <?php if($jobLists){echo count($jobLists); }?></div>
                              </div>
                              <button type="button" class="btn btn-primary snotify" data-toggle="modal" data-target="#myModal">Send Notification</button>
                           </div>
                        </div>
                        <!-- / APP TITLE -->
                        
                     </div>
                     <!-- / HEADER -->
                     <!-- CONTENT -->
                     <div class="page-content p-6">
                        <div class="content container">

                           <div class="row">
                              <div class="col-12">
                                 <div class="example">
                                    <div class="source-preview-wrapper">
                                       <div class="preview">

                                          <div class="preview-elements jobpostind">
                                          <?php if($this->session->set_tempdata('inserted')){?>
                                          <span><?php echo $this->session->tempdata('inserted'); ?></span>
                                          <?php } ?>
                                             <table id="sample-data-table" class="table">
                                                <thead>
                                                   <tr>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title"> Select </span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">SNo</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Job ID</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Job Title</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Category</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Sub-Category</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Job Posted Date</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Job Expiry Date</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">JOB AGEING</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Interview Mode</span>
                                                         </div>
                                                      </th>
                                                      
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Experience</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Company Name</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Target</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Location</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text Address_field">
                                                         <div class="table-header">
                                                            <span class="column-title">Address</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Actively Hiring</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Actively Hiring Status</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Chatbot</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Candidate Status</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Action</span>
                                                         </div>
                                                      </th>
                                                     
                                                   </tr>
                                                </thead>
                                                <tbody>
                                                   <?php
                                                      $x1 =1;
                                                      foreach($jobLists as $jobList) {
                                                      ?>
                                                   <tr>
                                                      <td style="width:40px;">
                                                         <input type="checkbox" name="checkid" id="checkid" class="checkmap" value="<?php echo $jobList['id'];?>" onclick="checkfunction(<?php echo $jobList['id'];?>)">
                                                      </td>
                                                      <td style="width:40px;"><?php echo $x1;?></td>
                                                      <td style="width:100px;"><?php echo $jobList['id'];?></td>
                                                      <td style="width:100px;"><?php echo $jobList['jobtitle'];?></td>
                                                      <td style="width:100px;"><?php echo $jobList['category'];?></td>
                                                      <td style="width:100px;"><?php echo $jobList['subcategory'];?></td>
                                                      <td style="width:100px;"><?php echo date('Y-m-d', strtotime($jobList['created_at']));?></td>
                                                      <td style="width:100px;"><?php echo date('Y-m-d', strtotime($jobList['jobexpire']));?></td>
                                                      <td style="width:70px;"><?php echo $jobList['daysleft'];?></td>
                                                      <td style="width:70px;"><?php echo $jobList['mode'];?></td>
                                                      <td style="width:70px;"><?php echo $jobList['experience'];?></td>
                                                      <td style="width:100px;"><p style="width:140px;"><?php echo $jobList['cname'];?></p></td>
                                                      <td><?php echo $jobList['opening']; ?></td>
                                                      <td style="width:130px;"><?php echo $jobList['site_name'];?></td>
                                                      <td><?php echo $jobList['address'];?></td>

                                                      <td>
                                                      <?php
                                                         if($jobList['actively'] == '1') {
                                                      ?>
                                                            <img src="<?php echo base_url().'recruiterfiles/';?>images/Job_Visible.png" style="width: 30px;" data-toggle="modal" data-target="#activelyDisableModal" onclick="getActivelyDisableid(<?php echo $jobList['id'];?>)">
                                                      <?php
                                                         } else {
                                                      ?>
                                                            <img src="<?php echo base_url().'recruiterfiles/';?>images/Job-Disable.png" style="width: 30px;" data-toggle="modal" data-target="#activelyModal" onclick="getActivelyid(<?php echo $jobList['id'];?>)">
                                                      <?php
                                                         }
                                                      ?>
                                                      </td>

                                                      <td>
                                                      <?php
                                                         if($jobList['actively'] == '1') {
                                                      ?>
                                                            Active
                                                      <?php
                                                         } else if($jobList['actively'] == 0 && !empty($jobList['actively_date'])) {
                                                      ?>
                                                            Expired
                                                      <?php
                                                         } else {
                                                            echo "Inactive";
                                                         }
                                                      ?>
                                                      </td>
                                                      
                                                      <td style="width:70px;">
                                                         <?php if($jobList['chatbot'] == 1){ ?> 
                                                               <a title="View Candidate" href="<?php echo base_url();?>administrator/questionnaire_view/<?php echo base64_encode($jobList['id']);?>" class="btn btn-icon usricos" aria-label="Product details"><i class="icon s-4 icon-eye"></i></a>
                                                         <?php } else { echo "--"; } ?>   
                                                      </td>

                                                      <td style="width:150px;"> 
													                   <?php
                                                            if($jobList['haveList']) {
                                                          ?> 
                                                           <a title="View Candidate" href="<?php echo base_url();?>administrator/Jobpost/candidatelistpage?id=<?php echo base64_encode($jobList['id']);?>" class="btn btn-icon usricos" aria-label="Product details"><i class="icon s-4 icon-eye"></i></a>
                                                           <?php }else{?>
														                <a title="No Candidate Available" href="#" class="btn btn-icon usricos" aria-label="Product details"><i class="icon s-4 icon-eye"></i></a>
                                                          <?php }?>
                                                      </td>
                                                      <td>
                            
                                                        <a title="View Job" href="<?php echo base_url();?>administrator/Jobpost/jobpostviewpage?id=<?php echo base64_encode($jobList['id']);?>&role=<?php echo base64_encode($jobList['company_id']);?>" class="btn btn-icon usricos" aria-label="Product details"><i class="icon s-4 icon-eye"></i></a>  
                             
                                                         <a title="Edit" href="<?php echo base_url();?>administrator/jobpost/updateJobpost?id=<?php echo base64_encode($jobList['id']);?>&role=<?php echo base64_encode($jobList['company_id']);?>" class="btn btn-icon usricos" aria-label="Product details"><i class="icon icon-pencil s-4"></i></a>
                             
                                                        <a title="Delete" href="#" data-toggle="modal" id="<?php echo $jobList['id'];?>" onclick="getrid(this.id)" data-target="#myModal1" class="btn btn-icon usricos" aria-label="Product details"><i class="icon s-4 icon-trash"></i></a>
                             
                                                      </td>
                                                     
                                                   </tr>

                                                   <?php
                                                      $x1++;
                                                      }
                                                      ?>
                                                </tbody>
                                             </table>
                                             
                                          </div>
                                       </div>
                                       <div class="source custom-scrollbar">
                                          <div class="highlight">
                                             <pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-html" data-lang="html"></code></pre>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- CONTENT -->
                  </div>
               </div>
            </div>
            
         </div>
         <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">

         </nav>
      </main>


<div id="activelyDisableModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-dialog-centered">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="padding: 5px;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       
      </div>
      <div class="modal-body">
         <h4 class="modal-title" style="color: #383838;">ACTIVELY HIRING JOB POST</h4>
         <p style="margin-top: 10px;">This job is already Actively Hired</p>
         <p style="margin-top: 20px;"><b>Do you to disable Actively Hired?</b></p>
         
         <form method="POST" action="<?php echo base_url() ?>administrator/jobpost/activelydisable">
            <input type="hidden" name="jid" id="activelyDisableID" value="">
            <p style="float: right;"><button type="submit" class="btn btn-default" style="margin-left: 15px;"> Yes </button> <button type="button" class="btn btn-default" data-dismiss="modal"> No </button></p>
         </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div id="activelyModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-dialog-centered">
    <!-- Modal content-->
    <div class="modal-content" style="z-index: 999 !important;">
      <div class="modal-header" style="padding: 5px;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
         <h4 class="modal-title" style="color: #383838;">ACTIVELY HIRING JOB POST</h4>
         <p style="margin:0;">You can add this job in Actively Hiring.</p>
         <p style="margin-top: 20px;">
            <strong style="color:#000;">Actively Hiring Amount :</strong> <?php echo $activelyAmountFetch[0]['amount']; ?>
         </p>
         <form method="POST" action="<?php echo base_url() ?>administrator/jobpost/activelystatus">
            <input type="hidden" name="jid" id="activelyID" value="">
            <p style="float: right;"><button type="submit" class="btn btn-success" style="margin-left: 15px;"> Yes </button> <button type="button" class="btn btn-default" data-dismiss="modal"> No </button></p>
         </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
   function getActivelyid(id) {
        var aid = id;
        $("#activelyID").val(aid);
   }
   function getActivelyDisableid(id) {
        var aid = id;
        $("#activelyDisableID").val(aid);
   }
</script>


<div class="modal" id="myModal1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete Job Post</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Do you really want to delete?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a href="" class="btn btn-primary" id="createLink">Delete</a>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
         <h5 class="modal-title">Send Notifications</h5>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
         </button>
      </div>
      <div class="modal-body">
         <form method="post" id="notifysubmit" action="<?php echo base_url('administrator/Jobpost/jobnotifications'); ?>">
            <div class="form-group">
               <input type="text" name="title" id="title" placeholder="Enter Title" class="form-control" maxlength="60" required="">
               <label>Title</label>
            </div>
            <div class="form-group">
               <input type="text" name="message" id="message" placeholder="Enter Message" class="form-control" maxlength="120" required="">
               <label>Message</label>
            </div>

            <div class="row">
               <div class="col-md-5">

                  <div class="form-group">
                     <label>Intrested In</label>
                     <select name="intrestedid[]" class="form-control interestselection" multiple="">
                     <?php
                        foreach($intrestedLists as $intrestedList) {

                           if($intrestedList['subcategory'] != "ALL") {
                     ?>
                        <option value="<?php echo $intrestedList['subcategory']; ?>"> <?php echo $intrestedList['subcategory']; ?> </option>
                     <?php
                           }
                        }
                     ?> 
                     </select>
                     <span style="font-size: 12px;"><b># CTRL + click for multiple selection</b></span>
                  </div>
               </div>
               <div class="col-md-1"></div>
               <div class="col-md-5">

                  <div class="form-group">
                     <label>Super Powers</label>
                     <select name="powerid[]" class="form-control powerselection" multiple="">
                     <?php
                        foreach($powerLists as $powerList) {
                     ?>
                        <option value="<?php echo $powerList['category']; ?>"> <?php echo $powerList['category']; ?> </option>
                     <?php

                        }
                     ?> 
                     </select>
                     <span style="font-size: 12px;"><b># CTRL + click for multiple selection</b></span>
                  </div>
               </div>

            <div class="modal-footer">
               <input type="hidden" name="checkarr" id="getcheckid" value="">
               <button type="submit" id="send_btn" class="btn btn-secondary" disabled="disabled">Send Notification</button>
               <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>  
            </div>
         </form>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
   
   $(document).ready(function() {
       $("#send_btn").on('click', function() {
            $(this).prop('disabled', true);
            $('#notifysubmit').submit();
       });
   });

</script>

<script type="text/javascript">
    function getrid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>administrator/jobpost/jobpostdelete?id="+rid;
        var aa = document.getElementById('createLink');
        aa.setAttribute("href", link);
    }
</script>
<script type="text/javascript">
   function checkfunction($id) {
      var checkid = $id;
      var appendcheckid = '';

      $('.checkmap:checkbox:checked').each(function () {
            var sThisVal = (this.checked ? $(this).val() : "");

            if(appendcheckid.length > 0) {
                  $('#send_btn').prop('disabled', false);
                  appendcheckid = sThisVal +","+appendcheckid;
            
            } else if(appendcheckid.length < 0) {
                  appendcheckid = '';
            
            } else {
                  appendcheckid = sThisVal;
                  $('#send_btn').prop('disabled', false);
            }
      });
      
      $("#getcheckid").val(appendcheckid);
   }
   $(document).ready(function() {
      $(".powerselection").on('click', function() {
            $('option', $('.interestselection')).each(function(element) {
               $(this).removeAttr('selected').prop('selected', false);
            });
            $(".interestselection").multiselect('refresh');

            $(".interestselection").removeAttr('required');
      });
   });

   $(document).ready(function() {
      $(".interestselection").on('click', function() {
            $('option', $('.powerselection')).each(function(element) {
               $(this).removeAttr('selected').prop('selected', false);
            });
            $(".powerselection").multiselect('refresh');
      });
   });
</script>

<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type="text/javascript">
   $('#sample-data-table').DataTable({
         "lengthMenu": [[10, 25, 50,100,200, -1], [10, 25, 50, 100,200, "All"]],
           ordering: false,
           dom: 'lBfrtip',
            buttons: [
             'excel', 'csv'
            ],
            destroy:true,
   });
</script>
   </body>
</html>

