<!DOCTYPE html>
<html lang="en-us">
   <head>
      <title>JobYoDA</title>
      <meta charset="UTF-8">
      <meta name="description" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
      <link rel="icon" href="<?php echo base_url();?>recruiterfiles/images/fav.png" type="image/png" sizes="16x16">
      <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700italic,700,900,900italic" rel="stylesheet">
      <!-- STYLESHEETS -->
      <style type="text/css">
         [fuse-cloak],
         .fuse-cloak {
         display: none !important; 
         }
      </style>
      <!-- Icons.css -->
      <link type="text/css" rel="stylesheet" href="<?php echo base_url().'adminfiles/';?>assets/icons/fuse-icon-font/style.css">
      <!-- Animate.css -->
      <link type="text/css" rel="stylesheet" href="<?php echo base_url().'adminfiles/';?>assets/node_modules/animate.css/animate.min.css">
      <!-- PNotify -->
      <link type="text/css" rel="stylesheet" href="<?php echo base_url().'adminfiles/';?>assets/node_modules/pnotify/dist/PNotifyBrightTheme.css">
      <!-- Nvd3 - D3 Charts -->
      <link type="text/css" rel="stylesheet" href="<?php echo base_url().'adminfiles/';?>assets/node_modules/nvd3/build/nv.d3.min.css" />
      <!-- Perfect Scrollbar -->
      <link type="text/css" rel="stylesheet" href="<?php echo base_url().'adminfiles/';?>assets/node_modules/perfect-scrollbar/css/perfect-scrollbar.css" />
      <!-- Fuse Html -->
      <link type="text/css" rel="stylesheet" href="<?php echo base_url().'adminfiles/';?>assets/fuse-html/fuse-html.min.css" />
      <!-- Main CSS -->
      <link type="text/css" rel="stylesheet" href="<?php echo base_url().'adminfiles/';?>assets/css/main.css">
      <link type="text/css" rel="stylesheet" href="<?php echo base_url().'adminfiles/';?>assets/css/style.css">
      <link type="text/css" rel="stylesheet" href="<?php echo base_url().'adminfiles/';?>assets/css/fonts">
<link type="text/css" rel="stylesheet" href="<?php echo base_url().'adminfiles/';?>assets/css/fontawesome.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url().'adminfiles/';?>assets/css/all.css">

     
  

      <!-- / STYLESHEETS -->
      <!-- JAVASCRIPT -->
      <!-- jQuery -->
      <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/jquery/dist/jquery.min.js"></script>
      <!-- Mobile Detect -->
      <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/mobile-detect/mobile-detect.min.js"></script>
      <!-- Perfect Scrollbar -->
      <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
      <!-- Popper.js -->
      <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/popper.js/dist/umd/popper.min.js"></script>
      <!-- Bootstrap -->
      <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
      <!-- Nvd3 - D3 Charts -->
      <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/d3/d3.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/nvd3/build/nv.d3.min.js"></script>
      <!-- Data tables -->
      <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/datatables.net/js/jquery.dataTables.js"></script>
      <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/datatables-responsive/js/dataTables.responsive.js"></script>
      <!-- PNotify -->
      <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/pnotify/dist/iife/PNotify.js"></script>
      <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/pnotify/dist/iife/PNotifyStyleMaterial.js"></script>
      <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/pnotify/dist/iife/PNotifyButtons.js"></script>
      <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/pnotify/dist/iife/PNotifyCallbacks.js"></script>
      <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/pnotify/dist/iife/PNotifyMobile.js"></script>
      <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/pnotify/dist/iife/PNotifyHistory.js"></script>
      <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/pnotify/dist/iife/PNotifyDesktop.js"></script>
      <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/pnotify/dist/iife/PNotifyConfirm.js"></script>
      <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/pnotify/dist/iife/PNotifyReference.js"></script>
      <!-- Fuse Html -->
      <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/fuse-html/fuse-html.min.js"></script>
      <!-- Main JS -->
      <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/js/main.js"></script>
    
   </head>
   <body class="layout layout-vertical layout-left-navigation layout-above-toolbar layout-above-footer">
      <main>
        
         <div id="wrapper">
            <aside id="aside" class="aside aside-left" data-fuse-bar="aside" data-fuse-bar-media-step="md" data-fuse-bar-position="left">
               <div class="aside-content bg-primary-700 text-auto">
                  <div class="aside-toolbar">
                     <img src="<?php echo base_url(); ?>recruiterfiles/images/jobyodahdr.png" style="height:44px;"> 
                     <div class="logo">
                        <!--<span class="logo-icon">F</span>
                        <span class="logo-text">FUSE</span>-->
                     </div>
                     <button id="toggle-fold-aside-button" type="button" class="btn btn-icon d-none d-lg-block" data-fuse-aside-toggle-fold>
                     <i class="icon icon-backburger"></i>
                     </button>
                  </div>
                  <?php echo include_once('sidebar.php');?>
               </div>
            </aside>
            <div class="content-wrapper">
			
			 <nav id="toolbar" class="bg-white">
            <div class="row no-gutters align-items-center flex-nowrap">
               <div class="col">
                  <div class="row no-gutters align-items-center flex-nowrap">
                     <button type="button" class="toggle-aside-button btn btn-icon d-block d-lg-none" data-fuse-bar-toggle="aside">
                     <i class="icon icon-menu"></i>
                     </button>
                     <div class="toolbar-separator d-block d-lg-none"></div>
                     <div class="shortcuts-wrapper row no-gutters align-items-center px-0 px-sm-2">
                        <div class="shortcuts row no-gutters align-items-center d-none d-md-flex">
                           <!--<a href="apps-chat.html" class="shortcut-button btn btn-icon mx-1">
                           <i class="icon icon-hangouts"></i>
                           </a>
                           <a href="apps-contacts.html" class="shortcut-button btn btn-icon mx-1">
                           <i class="icon icon-account-box"></i>
                           </a>
                           <a href="apps-mail.html" class="shortcut-button btn btn-icon mx-1">
                           <i class="icon icon-email"></i>
                           </a>-->
                        </div>
                        <div class="add-shortcut-menu-button dropdown px-1 px-sm-3">
                           <!--<div class="dropdown-toggle btn btn-icon" role="button" id="dropdownShortcutMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="icon icon-star text-amber-600"></i>
                           </div>-->
                           <div class="dropdown-menu" aria-labelledby="dropdownShortcutMenu">
                              <a class="dropdown-item" href="#">
                                 <div class="row no-gutters align-items-center justify-content-between flex-nowrap">
                                    <div class="row no-gutters align-items-center flex-nowrap">
                                       <i class="icon icon-calendar-today"></i>
                                       <span class="px-3">Calendar</span>
                                    </div>
                                    <i class="icon icon-pin s-5 ml-2"></i>
                                 </div>
                              </a>
                              <a class="dropdown-item" href="#">
                                 <div class="row no-gutters align-items-center justify-content-between flex-nowrap">
                                    <div class="row no-gutters align-items-center flex-nowrap">
                                       <i class="icon icon-folder"></i>
                                       <span class="px-3">File Manager</span>
                                    </div>
                                    <i class="icon icon-pin s-5 ml-2"></i>
                                 </div>
                              </a>
                              <a class="dropdown-item" href="#">
                                 <div class="row no-gutters align-items-center justify-content-between flex-nowrap">
                                    <div class="row no-gutters align-items-center flex-nowrap">
                                       <i class="icon icon-checkbox-marked"></i>
                                       <span class="px-3">To-Do</span>
                                    </div>
                                    <i class="icon icon-pin s-5 ml-2"></i>
                                 </div>
                              </a>
                           </div>
                        </div>
                     </div>
                     <div class="toolbar-separator"></div>
                  </div>
               </div>
               <div class="col-auto">
                  <div class="row no-gutters align-items-center justify-content-end">
                     <div class="user-menu-button dropdown">
                        <div class="dropdown-toggle ripple row align-items-center no-gutters px-2 px-sm-4" role="button" id="dropdownUserMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                           <div class="avatar-wrapper">
                              <!--<img class="avatar" src="<?php echo base_url().'adminfiles/';?>assets/images/avatars/profile.jpg">-->
                              <!--<i class="status text-green icon-checkbox-marked-circle s-4"></i>-->
                           </div>
                           <span class="username mx-3 d-none d-md-block">JobYoDA</span>
                        </div>
                        <div class="dropdown-menu" aria-labelledby="dropdownUserMenu">
                           <!--<a class="dropdown-item" href="#">
                              <div class="row no-gutters align-items-center flex-nowrap">
                                 <i class="icon-account"></i>
                                 <span class="px-3">My Profile</span>
                              </div>
                           </a>-->
                           <!--<a class="dropdown-item" href="#">
                              <div class="row no-gutters align-items-center flex-nowrap">
                                 <i class="icon-email"></i>
                                 <span class="px-3">Inbox</span>
                              </div>
                           </a>
                           <a class="dropdown-item" href="#">
                              <div class="row no-gutters align-items-center flex-nowrap">
                                 <i class="status text-green icon-checkbox-marked-circle"></i>
                                 <span class="px-3">Online</span>
                              </div>
                           </a>-->
                           <div class="dropdown-divider"></div>
                           <a class="dropdown-item" href="<?= base_url('administrator/dashboard/logout'); ?>">
                              <div class="row no-gutters align-items-center flex-nowrap">
                                 <i class="icon-logout"></i>
                                 <span class="px-3">Logout</span>
                              </div>
                           </a>
                        </div>
                     </div>
                     <div class="toolbar-separator"></div>
                     <button type="button" class="search-button btn btn-icon">
                     <i class="icon icon-magnify"></i>
                     </button>
                     <div class="toolbar-separator"></div>
                     <div class="language-button dropdown">
                        <!--<div class="dropdown-toggle ripple row align-items-center justify-content-center no-gutters px-0 px-sm-4" role="button" id="dropdownLanguageMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                           <div class="row no-gutters align-items-center">
                              <img class="flag mr-2" src="<?php echo base_url().'adminfiles/';?>assets/images/flags/us.png">
                              <span class="d-none d-md-block">EN</span>
                           </div>
                        </div>
                        <div class="dropdown-menu" aria-labelledby="dropdownLanguageMenu">
                           <a class="dropdown-item" href="#">
                              <div class="row no-gutters align-items-center flex-nowrap">
                                 <img class="flag" src="<?php echo base_url().'adminfiles/';?>assets/images/flags/us.png">
                                 <span class="px-3">English</span>
                              </div>
                           </a>
                           <a class="dropdown-item" href="#">
                              <div class="row no-gutters align-items-center flex-nowrap">
                                 <img class="flag" src="<?php echo base_url().'adminfiles/';?>assets/images/flags/es.png">
                                 <span class="px-3">Spanish</span>
                              </div>
                           </a>
                           <a class="dropdown-item" href="#">
                              <div class="row no-gutters align-items-center flex-nowrap">
                                 <img class="flag" src="<?php echo base_url().'adminfiles/';?>assets/images/flags/tr.png">
                                 <span class="px-3">Turkish</span>
                              </div>
                           </a>
                        </div>-->
                     </div>
                     <div class="toolbar-separator"></div>
                     <!--<button type="button" class="quick-panel-button btn btn-icon" data-fuse-bar-toggle="quick-panel-sidebar">
                     <i class="icon icon-format-list-bulleted"></i>
                     </button>-->
                  </div>
               </div>
            </div>
         </nav>