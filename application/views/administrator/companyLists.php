<?php include('header.php'); ?>


               <div class="content custom-scrollbar">
                  <div class="doc data-table-doc page-layout simple full-width">
                     <!-- HEADER -->
                     <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
                        <!-- APP TITLE -->
                        <div class="col-12 col-sm">
                           <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                              <div class="logo-icon mr-3 mt-1">
                                 <i class="icon s-6 icon-person-box"></i>
                              </div>
                              <div class="logo-text">
                                 <div class="h4">Site Listing</div>
                                 <div class="">Total: <?php if($CompLists){echo count($CompLists); }?></div>
                              </div>
                           </div>
                        </div>
                        <!-- / APP TITLE -->
                        <!--<div class="col-auto">
                           <a href="<?php echo base_url();?>administrator/recruiter/addRecruiter" class="btn btn-light">ADD NEW</a>
                        </div>-->
                     </div>
                     <!-- / HEADER -->
                     <!-- CONTENT -->
                     <div class="page-content p-6">
                        <div class="content container">
                           <div class="row">
                              <div class="col-12">
                                 <div class="example ">
                                    <div class="source-preview-wrapper">
                                       <div class="preview">
                                          <div class="preview-elements companyldtf">
                                             <table id="sample-data-table" class="table">
                                                <thead>
                                                   <tr>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">SNo</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Company</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Recruiter</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Address</span>
                                                         </div>
                                                      </th>
                                                      
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Rating</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Action</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text" style="">
                                                         <div class="table-header">
                                                            <span class="column-title">Glassdoor Rating</span>
                                                         </div>
                                                      </th>
                                                   </tr>
                                                </thead> 
                                                <tbody>
                                                   <?php
                                                      $x1 =1;
                                                      //print_r($CompLists);die;
                                                      foreach($CompLists as $CompList) {
                                                   ?>
                                                   <tr>
                                                      <td><?php echo $x1;?></td>
                                                      <td><?php echo $CompList['cname'];?></td>
                                                      <td><?php echo $CompList['reName'];?></td>
                                                      <td><p><?php echo $CompList['address'];?></p></td>
                                                     
                                                      <td><?php echo $CompList['rating'];?></td>
                                                   
                                                      <td>
                                                       <a title="View Company" href="<?php echo base_url();?>administrator/recruiter/companysiteview?id=<?php echo base64_encode($CompList['rid']);?>" class="btn btn-icon usricos" aria-label="Product details"><i class="icon s-4 icon-eye"></i></a>  

                                                       <a title="Delete" href="#" data-toggle="modal" id="<?php echo $CompList['rid'];?>"  onclick="getrid(this.id)" data-target="#myModal1" class="btn btn-icon usricos" aria-label="Product details"><i class="icon s-4 icon-trash"></i></a>
                                                      </td>

                                                      <td>
                                                       <a title="Rating" href="#" class="glassrating" data-toggle="modal" data-target="#rating_modal"  id="<?php echo $CompList['rid'];?>" onclick="getcid(this.id)" class="btn btn-icon usricos" aria-label="Product details"><i class="icon s-4 icon-eye"></i></a>  
                                                      </td>
                                                   </tr>
                                                   <?php
                                                      $x1++;
                                                      }
                                                   ?>
                                                </tbody> 
                                             </table>
                                             <script type="text/javascript">
                                                $('#sample-data-table').DataTable();
                                             </script>
                                          </div>
                                       </div>
                                       <div class="source custom-scrollbar">
                                          <div class="highlight">
                                             <pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-html" data-lang="html">
    
                                                    </code></pre>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- CONTENT -->
                  </div>
               </div>
            </div>
            <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
               <div class="list-group" class="date">
                  <div class="list-group-item subheader">TODAY</div>
                  <div class="list-group-item two-line">
                     <div class="text-muted">
                        <div class="h1"> Friday</div>
                        <div class="h2 row no-gutters align-items-start">
                           <span> 5</span>
                           <span class="h6">th</span>
                           <span> May</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Events</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Group Meeting</h3>
                        <p>In 32 Minutes, Room 1B</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Public Beta Release</h3>
                        <p>11:00 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Dinner with David</h3>
                        <p>17:30 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Q&amp;A Session</h3>
                        <p>20:30 PM</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Notes</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Best songs to listen while working</h3>
                        <p>Last edit: May 8th, 2015</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Useful subreddits</h3>
                        <p>Last edit: January 12th, 2015</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Quick Settings</div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Notifications</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Cloud Sync</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Retro Thrusters</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
            <!--<a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
            <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
            </a>-->
         </nav>
      </main>

<div class="modal" id="myModal1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete Recruiter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to delete this recruiter?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a href="" class="btn btn-primary" id="createLink">Delete</a>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="rating_modal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Company Rating</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?php echo base_url();?>administrator/recruiter/insertRating" method="post">
                  <div class="txtdesc">
                     <div class="form-group main-hda right">
					  <label>Rating</label>
                        <input type="number" class="form-control" name="ratings" id="grating1" required="required" step="0.1" min="0" max="5">
                       
                     </div>
                     <div class="companymanagesd">
                        <div class="clasetis">
                          <input type="hidden" name="cid" id="cid" value="">
                           <input class="btn btn-secondary" id="submit_rating" type="submit" name="submit" value="Submit">
                        </div>
                        
                     </div>
                  </div>
               </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                
            </div>
        </div>
    </div>
</div>


<div class="modal" id="topRecruiterModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" action="<?php echo base_url(); ?>/administrator/recruiter/toprecruitersave">
               <div class="modal-header">
                   <h5 class="modal-title"> Assign Top Recruiter </h5>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                       <span aria-hidden="true">×</span>
                   </button>
               </div>
               <div class="modal-body">

                  <div class="form-group mb-4">
                     <label> Set Priority </label>
                     <input type="text" name="priority" value="" id="priorityValue" class="form-control md-has-value" aria-describedby="nameHelp" required="">
                     <span class="durationerror"></span>
                  </div>

                  <input type="hidden" id="gettoprecruterid" name="recruiter_id" value="">
               </div>
               <div class="modal-footer">
                   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                   <button type="submit" class="btn btn-primary" id="createbLink">Save</button>
               </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    function getrid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>administrator/recruiter/deleteCompany?id="+rid;
        var aa = document.getElementById('createLink');
        aa.setAttribute("href", link);
    }

    function getcid(id){
      var cid = id;
      $('#cid').val(cid);
      var url="<?php echo base_url(); ?>administrator/recruiter/get_glassdoor_rating";
      $.ajax({
         url:url,
         data:{cid:cid},
         type:"POST",
         success:function(data){
            //alert(data);
            if(parseInt(data)>0){
               $('#grating1').val(parseFloat(data));
               //$('#submit_rating').val('Update');
            }
            
         }

      })
      
    }

    
</script>
   </body>
</html>

