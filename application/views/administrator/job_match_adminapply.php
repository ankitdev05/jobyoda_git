<?php include ('header.php'); ?>
<style type="text/css">
   input[type="checkbox"] {
      opacity: 1!important;
      position: relative!important;
      height: 25px!important;
      width: 19px!important;
   }   
</style>

<div class="content custom-scrollbar">
   <div class="doc data-table-doc page-layout simple full-width">
      <!-- HEADER -->
      <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
         <!-- APP TITLE -->
         <div class="col-12 col-sm">
            <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
               <div class="logo-icon mr-3 mt-1">
                  <i class="icon s-6 icon-person-box"></i>
               </div>
               <div class="logo-text">
                  <div class="h4">Jobs for Job Match</div>
                  <div class="">Total: <?php if ($jobArray) { echo count($jobArray); } ?></div>
               </div>
            </div>
         </div>
         <!-- / APP TITLE -->
         <div class="col-auto">
            
         </div>
      </div>
      <!-- / HEADER -->
      <!-- CONTENT -->
      <div class="page-content p-6">
         <div class="content container">
            <div class="row">
               <div class="col-12">
                  <div class="example ">
                     <div class="source-preview-wrapper">
                        <div class="preview">
                           <div class="preview-elements reculsting pdrct">
                              <table id="sample-data-table" class="table">
                                 <thead>
                                    <tr>
                                       
                                       <th class="secondary-text">
                                          <div class="table-header">
                                             <span class="column-title">SNo</span>
                                          </div>
                                       </th>
                                       <th class="secondary-text">
                                          <div class="table-header">
                                             <span class="column-title">Job ID</span>
                                          </div>
                                       </th>
                                       <th class="secondary-text">
                                          <div class="table-header">
                                             <span class="column-title">Job Title</span>
                                          </div>
                                       </th>
                                       <th class="secondary-text">
                                          <div class="table-header">
                                             <span class="column-title">Job Category</span>
                                          </div>
                                       </th>
                                       <th class="secondary-text">
                                          <div class="table-header">
                                             <span class="column-title">Job Sub-Category</span>
                                          </div>
                                       </th>
                                       <th class="secondary-text">
                                          <div class="table-header">
                                             <span class="column-title">Company Name</span>
                                          </div>
                                       </th>
                                       <th class="secondary-text">
                                          <div class="table-header">
                                             <span class="column-title">Site Name</span>
                                          </div>
                                       </th>

                                       <th class="secondary-text">
                                          <div class="table-header">
                                             <span class="column-title">Address</span>
                                          </div>
                                       </th>
                                       <th class="secondary-text">
                                          <div class="table-header">
                                             <span class="column-title">Openings</span>
                                          </div>
                                       </th>
                                       <th class="secondary-text">
                                          <div class="table-header">
                                             <span class="column-title">Salary</span>
                                          </div>
                                       </th>
                                       <th class="secondary-text">
                                          <div class="table-header">
                                             <span class="column-title">Experience</span>
                                          </div>
                                       </th>

                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php
                                       $x1 = 1;
                                       foreach ($jobArray as $List) {
                                    ?>
                                    <tr>
                                       <td><?php echo $x1; ?></td>
                                       <td><?php echo $List['id']; ?></td>
                                       <td><?php echo $List['jobTitle']; ?></td>
                                       <td><?php echo $List['category']; ?></td>
                                       <td><?php echo $List['subcategory']; ?></td>
                                       <td><?php echo $List['company_name']; ?></td>
                                       <td><?php echo $List['site_name']; ?></td>
                                       <td><?php echo $List['address']; ?></td>
                                       <td><?php echo $List['opening']; ?></td>
                                       <td><?php echo $List['salary']; ?></td>
                                       <td><?php echo $List['experience']; ?></td>
                                    </tr>
                                    <?php
                                          $x1++;
                                       }
                                    ?>
                                 </tbody>
                              </table>
                              <script type="text/javascript">
                                 $('#sample-data-table').DataTable();
                              </script>
                           </div>
                        </div>
                        <div class="source custom-scrollbar">
                           <div class="highlight">
                              <pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-html" data-lang="html"></code></pre>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CONTENT -->
   </div>
</div>
</div>
   <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
   </div>
</div>
<nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
</nav>
</main>


</body>
</html>

