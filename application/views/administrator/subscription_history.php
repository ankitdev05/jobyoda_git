<style>
    .grey{
        background: #8282823b !important;
    }
    .green{
      background: #00c671 !important;  
         text-align: center;
    }
      .green1{
        background: #dbf5ea !important;
    color: #00c671 !important;  
          text-align: center;
    }
        .red1{
   background: #f8e4e5 !important;
    color: #de0e0e !important; 
    }
    .red{
            background: #de0e0e !important;
    }
    .white{
         background: #fff !important;
        color: #000 !important;
    }
    table.dataTable tbody tr td{
        font-weight: 700;
    }

    </style>

<?php include('header.php');?>

<div class="content custom-scrollbar">
   <div class="doc data-table-doc page-layout simple full-width">
      <!-- HEADER -->
      <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
         <!-- APP TITLE -->
         <div class="col-12 col-sm">
            <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
               <div class="logo-icon mr-3 mt-1">
                  <i class="icon-cube-outline s-6"></i>
               </div>
               <div class="logo-text">
                  <div class="h4">Subscription History</div>
               </div>
            </div>
         </div>
      </div>
      <!-- / HEADER -->
      <!-- CONTENT -->
      <div class="page-content p-6">
         <div class="content container">
            <div class="row">
               <div class="col-12">
                  <div class="example ">
                     <div class="source-preview-wrapper">
                        <div class="preview">
                           <div class="preview-elements companytables">
                              <table id="sample-data-table" class="table">
                                 <thead>
                                    <tr>
                                       <th class="secondary-text ">
                                          <div class="table-header">
                                             <span class="column-title">ID</span>
                                          </div>
                                       </th>
                                       <th class="secondary-text ">
                                          <div class="table-header">
                                             <span class="column-title">Plan Name</span>
                                          </div>
                                       </th>
                                       <th class="secondary-text ">
                                          <div class="table-header">
                                             <span class="column-title">Duration</span>
                                          </div>
                                       </th>
                                       <th class="secondary-text ">
                                          <div class="table-header">
                                             <span class="column-title">Start Date</span>
                                          </div>
                                       </th>
                                       <th class="secondary-text green">
                                          <div class="table-header">
                                             <span class="column-title">Expiry Date</span>
                                          </div>
                                       </th>
                                       <th class="secondary-text ">
                                          <div class="table-header">
                                             <span class="column-title">Actual Cost</span>
                                          </div>
                                       </th>
                                       <th class="secondary-text ">
                                          <div class="table-header">
                                             <span class="column-title">Plan Cost</span>
                                          </div>
                                       </th>
                                       <th class="secondary-text ">
                                          <div class="table-header">
                                             <span class="column-title">Status</span>
                                          </div>
                                       </th>
                                       <th class="secondary-text ">
                                          <div class="table-header">
                                             <span class="column-title">View</span>
                                          </div>
                                       </th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php
                                       $x1 =1;
                                       foreach($Lists as $List) {
                                    ?>
                                       <tr>
                                          <td style="width:30px;" class=""><?php echo $x1;?></td>
                                          <td style="width:60px;"  class=""><?php echo $List['plan_name'] ;?></td>
                                          <td style="width:60px;"  class=""><?php echo $List['duration'] ;?></td>
                                          <td style="width:60px;"  class=""><?php echo $List['start_date'] ;?></td>
                                          <td style="width:60px;"  class=""><?php echo $List['end_date'] ;?></td>
                                          <td style="width:60px;"  class=""><?php echo $List['actual_cost'] ;?></td>
                                          <td style="width:60px;"  class=""><?php echo $List['plan_cost'] ;?></td>
                                          <td style="width:60px;"  class="">  
                                             <?php if($List['status'] == 1) { echo "<span style='color:green'>Active</span>";} else { echo "<span style='color:red'>Inactive</span>"; } ?>
                                          </td>
                                          <td style="width:120px;"  class="">
                                             <a title="View Subscription" href="javascript:void(0)" class="btn btn-icon usricos" aria-label="Product details" onclick="subscriptionfunction('<?php echo $List["id"]; ?>')"><i class="icon s-4 icon-eye"></i></a>
                                          </td>
                                       </tr>
                                    <?php
                                       $x1++;
                                       }
                                    ?>
                                 </tbody>
                              </table>
                              <script type="text/javascript">
                                 $('#sample-data-table').DataTable();
                              </script>
                           </div>
                        </div>
                        <div class="source custom-scrollbar">
                           <div class="highlight">
                              <pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-html" data-lang="html">
    
                                                    </code></pre>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CONTENT -->
   </div>
</div>
</div>

</div>
<nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">

</nav>
</main>
<div class="modal" id="myModal1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Delete Recruiter</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
         </div>
         <div class="modal-body">
            <p>Do you really want to delete?</p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <a href="" class="btn btn-primary" id="createLink">Delete</a>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
   function getrid(id) {
       var rid = id;
       var link = "<?php echo base_url();?>administrator/recruiter/deleteRecruiter?id="+rid;
       var aa = document.getElementById('createLink');
       aa.setAttribute("href", link);
   }

   function subscriptionfunction(d1) {
     var url = "<?php echo base_url(); ?>recruiter/recruiter/subscription_history_by_id";
     $.ajax({
        type:"POST",
        url:url,
        data:{
           pid : d1
        },
        success:function(data) {
         
           var record = JSON.parse(data);
           
           $('#e_planname').html(record.plan_name);
           $('#e_plancost').html(record.plan_cost);
           $('#e_planduration').html(record.duration);
           $('#e_startdate').html(record.start_date);
           $('#e_enddate').html(record.end_date);

           $('#exist_1').html(record.total_mail);
           $('#exist_11').html(record.remaining_mail);

           $('#exist_2').html(record.total_job_post);
           $('#exist_22').html(record.remaining_job_post);

           $('#exist_3').html(record.total_resume_access);
           $('#exist_33').html(record.remaining_resume_access);

           $('#exist_4').html(record.total_video_post);
           $('#exist_44').html(record.remaining_video_post);

           $('#exist_5').html(record.total_advertise);
           $('#exist_55').html(record.remaining_advertise);

           $('#exist_6').html(record.total_job_boost);
           $('#exist_66').html(record.remaining_job_boost);

           $('#exist_7').html(record.total_candidate_profile);
           $('#exist_77').html(record.remaining_candidate_profile);

           $('.planimageadd').attr('src',record.plan_image);

           $("#myModal").modal('show');
        }
     });
   }
</script>

<!-- Modal -->
      <div id="myModal" class="modal fade" role="dialog">
         <div class="modal-dialog" style="width: 62%;">
            <!-- Modal content-->
            <div class="modal-content">
               
               <div class="modal-body">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <div class="cdfts">
                     <div class="subscriptionHead">
                        <div class="subbLeft">
                           <h2><span class="subImgs" style="    display: inline-block;width: 75px;margin-right: 10px;"><img src="" class="img-fluid planimageadd" alt="golden"></span>  <span id="e_planname"></span> Plan <span color ="green1"> ₱<span id="e_plancost"></span> </span></h2>
                        </div>
                     </div>
                     <div class="subscriptionTime">
                        <ul>
                           <li>Duration : <span class="green1" id="e_planduration"> </span></li>
                        </ul>
                        <ul>
                           <li>Start Date : <span class="green1" id="e_startdate"></span></li>
                           <li>End Date : <span class="green1" id="e_enddate"></span></li>
                        </ul>
                     </div>
                     <div class="companydtls">
                        <div class="newfiletrsgree grey">
                           <div class="tableList ">
                              <table class="table">
                                 <thead>
                                    <tr>
                                       <th class="White white"></th>
                                       <th class="green">Total</th>
                                       <th class="red">Remaining</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td class="trnasparent grey">Number Of Mass Mails</td>
                                       <td class="greennew green1" id="exist_1"></td>
                                       <td class="rednew red1" id="exist_11"></td>
                                    </tr>
                                    <tr>
                                       <td class="trnasparent grey">Number Of Jobs Posting</td>
                                       <td class="greennew green1" id="exist_2"></td>
                                       <td class="rednew red1" id="exist_22"></td>
                                    </tr>
                                    <tr>
                                       <td class="trnasparent grey">Number Of Resume Accessiblity</td>
                                       <td class="greennew green1" id="exist_3"></td>
                                       <td class="rednew red1" id="exist_33"></td>
                                    </tr>
                                    <tr>
                                       <td class="trnasparent grey">Video Posting</td>
                                       <td class="greennew green1" id="exist_4"></td>
                                       <td class="rednew red1" id="exist_44"></td>
                                    </tr>
                                    <tr>
                                       <td class="trnasparent grey">Advertisment Posting</td>
                                       <td class="greennew green1" id="exist_5"></td>
                                       <td class="rednew red1" id="exist_55"></td>
                                    </tr>
                                    <tr>
                                       <td class="trnasparent grey">Job Boasting</td>
                                       <td class="greennew green1" id="exist_6"></td>
                                       <td class="rednew red1" id="exist_66"></td>
                                    </tr>
                                    </tr>
                                    <tr>
                                       <td class="trnasparent grey">Candidate Profile</td>
                                       <td class="greennew green1" id="exist_7"></td>
                                       <td class="rednew red1" id="exist_77"></td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
</body>
</html>

