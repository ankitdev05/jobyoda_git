<?php include('header.php');?>
<style>
   
.snotify{
   position: absolute!important;
   right: 30px!important;
   top: 10px!important;
   background: #27ae62;
}
#sidenav{
   height: 95%;
   overflow: auto !important; 
} 
#sidenav::-webkit-scrollbar {
   width: 6px;
   border-radius: 10px;
}
#sidenav::-webkit-scrollbar-track {
   background: #0a4c89;
   border-radius: 6px;
}
#sidenav::-webkit-scrollbar-thumb {
   background-color: #aaa;
   border-radius: 6px;
   transition: background-color .2s linear, width .2s ease-in-out;
   -webkit-transition: background-color .2s linear, width .2s ease-in-out;
}
   
   .form-group {
       margin-bottom: 0px!important;
   }
      .questHead h2 {
      font-size: 20px;
      color: #000;
      font-weight: 600;
      margin: 0;
      padding-bottom: 10px;
      border-bottom: 2px solid #00a94f;
      margin-bottom: 10px;
      }
      .questionView {
      background: #fff;
      padding: 35px;
      box-shadow: 0 6px 12px rgba(0,0,0,.175);
      }
      .questionAll h2{
      margin: 0;
      padding: 10px;
      font-size: 14px;
      line-height: 20px;
      color: #000;
      font-weight: 600;
      width: 100%;
      float: left;
      display: block;
      padding-left: 0;
          padding: 0;
      }
      .questionAll label {
      font-size: 13px;
      color: #000;
      font-weight: 600;
      padding: 0;
      margin-bottom: 30px;
      width: 0px !important;
      height: 0px !important;
      float: left;
      width: 100% !important;
      float: left;
      display: block;
      }
      .questionAll label::before {
      content: '';
      position: inherit;
      border: none;
      width: 0px !important;
      height: 0px !important;
      }
      .allOptions li {
      font-size: 13px;
      background: #cccccca6;
      padding: 8px 35px 8px 15px;
      border-radius: 4px;
      font-weight: 600;
      display: inline-block;
      cursor: pointer;
      margin-right: 15px;
      float: left;
      min-width: 175px;
          margin-bottom: 0px;
      }
      .boxs{
      display: block;
      width: 100%;
      float: left;
      }
      .allOptions li.answer{
      color: #00a94f;
      background: #00a94f30;
      }

</style>
               <div class="content custom-scrollbar">
                  <div class="doc data-table-doc page-layout simple full-width">
                     <!-- HEADER -->
                     <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
                        <!-- APP TITLE -->
                        <div class="col-12 col-sm">
                           <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                              <div class="logo-icon mr-3 mt-1">
                                 <i class="icon-briefcase s-6"></i>
                              </div>
                              <div class="logo-text">
                                 <div class="h4">Chatbot Questionnaire</div>
                                 <div class=""></div>
                              </div>
                           </div>
                        </div>
                        <!-- / APP TITLE -->
                        
                     </div>
                     <!-- / HEADER -->
                     <!-- CONTENT -->
                     <div class="page-content p-6">
                        <div class="content container">

                           <div class="row">
                              <div class="col-12">
                                 <div class="example scrollmobusr">
                                    <div class="source-preview-wrapper">
                                       <div class="preview">
                                          <div class="preview-elements jobpostind">

                                             <div class="questionView boxs">
                                                
                                                <?php
                                                $x=1;
                                                if($questionData) {
                                                    foreach($questionData as $questionD) {
                                                ?>

                                                <div class="questionAll boxs">
                                                   <div class="form-group boxs">
                                                      <label>Question <?php echo $x;?></label>
                                                      <h2><?php echo $questionD['question']; ?></h2>
                                                   </div>

                                                   <?php if($questionD['type'] == "text") { } else { ?>
                                                   <div class="form-group boxs">
                                                      <label>Options</label>
                                                      <div class="allOptions boxs">
                                                        <ul>
                                                        <?php
                                                        foreach($questionD['options'] as $optionsValue) {
                                                        ?>
                                                            <li> <?php echo $optionsValue;?> </li>
                                                        <?php
                                                        }
                                                        ?>
                                                        </ul>
                                                      </div>
                                                   </div>
                                                   
                                                   <div class="form-group boxs">
                                                      <label>Answers</label>
                                                      <div class="allOptions boxs">
                                                        <ul>
                                                            <li class="answer"> <?php echo $questionD['answers'];?> </li>
                                                        </ul>
                                                      </div>
                                                   </div>

                                                   <?php } ?>
                                                   
                                                </div>
                                              <?php
                                                    $x++;
                                                  }
                                                } else {
                                              ?>
                                                <div class="questionAll boxs">
                                                   <div class="form-group boxs">
                                                      <label> No record found </label>
                                                   </div>
                                                </div>   
                                              <?php
                                                }
                                              ?>
                                             </div>

                                          </div>
                                       </div>

                                       <div class="source custom-scrollbar">
                                          <div class="highlight">
                                             <pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-html" data-lang="html">
                                             </code></pre>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- CONTENT -->
                  </div>
               </div>
            </div>


         </div>
         <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
           <!-- <a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
            <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
            </a>-->
         </nav>
      </main>

<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

   </body>
</html>

