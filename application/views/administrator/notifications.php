<style>


.preview input[type="checkbox"], input[type="radio"] {
position: absolute;
opacity: 0;
z-index: 9;
margin: 0;
left: 0;
right: 0;
padding: 46px 0;
height: 14px;
width: 19px;
margin: auto;
}

input[type="checkbox"] {
    position: inherit!important;
    opacity: 1!important;
    left: 0;
    right: 0;
    padding: 32px 0;
    height: 38px;
    width: 37px;
    margin: auto;
}

.send_button {
float: right;
width: 100%;
margin-bottom: 20px;
padding: 0px 26px;
}
.send_button button {
float: right;
padding: 10px 13px;
background: #0a4c89;
color: #fff;
border-radius: 10px;
}
</style>

<?php include('header.php'); ?>
<div class="content custom-scrollbar">
                  <div class="doc data-table-doc page-layout simple full-width">
                     <!-- HEADER -->
                     <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
                        <!-- APP TITLE -->
                        <div class="col-12 col-sm">
                           <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                              <div class="mainheadicos">
                                 <i class="icon s-4 icon-bell"></i>
                              </div>
                              <div class="logo-text">
                                 <div class="h4">Notification Management</div>
                                 
                              </div>
                           </div>
                        </div>
                        <!-- / APP TITLE -->
                        <div class="col-auto">
                          <!-- <a href="<?php echo base_url();?>administrator/recruiter/addRecruiter" class="btn btn-light">ADD NEW</a>-->
                        </div>
                     </div>
                     <!-- / HEADER -->
                     <!-- CONTENT -->
                     <div class="page-content p-6">
                        <div class="content container">
                           <div class="row">
                              <div class="col-12">
                                 <div class="example ">
                                    <div class="source-preview-wrapper">
                                       <div class="preview">
                                          <div class="preview-elements notifds">
                        <div class="send_button">
                                       <button class="snotify" data-target="#myModal2" data-toggle="modal" >Send Notification</button>
                                    </div>
                  
                      <div class="">
                                             <table id="sample-data-table" class="table">
                                                <thead>
                                                   <tr>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title"><input type="checkbox" id="check_all" ></span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">SNo</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Name</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text" >
                                                         <div class="table-header">
                                                            <span class="column-title">Email</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Phone</span>
                                                         </div>
                                                      </th><th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Platform</span>
                                                         </div>
                                                      </th><th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Last Used</span>
                                                         </div>
                                                      </th><th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">App Version</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Superpower</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Location</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Salary</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Experience</span>
                                                         </div>
                                                      </th>
                                                      
                                                   </tr>
                                                </thead>
                                                <tbody> 
                                                   <?php
                                                      $x1 =1;
                                                      foreach($Lists as $List) {
                                                        if($List['exp_year'] == 0 && $List['exp_month'] == 0) {
                                                              $exp = "No Experience";
                                                              
                                                          }else if($List['exp_year'] == 0 && $List['exp_month'] < 6) {
                                                              $exp = "less than 6 months";
                                                              
                                                          } else if($List['exp_year'] < 1 && $List['exp_month'] >= 6) {
                                                              $exp = "6 months to 1 year";
                                                              
                                                          } else if($List['exp_year'] < 2 && $List['exp_year'] >= 1) {
                                                              $exp = "1 year to 2 year";
                                                              
                                                          } else if($List['exp_year'] < 3 && $List['exp_year'] >= 2) {
                                                              $exp = "2 year to 3 year";
                                                              
                                                          }else if($List['exp_year'] < 4 && $List['exp_year'] >= 3) {
                                                              $exp = "3 year to 4 year";
                                                              
                                                          }else if($List['exp_year'] < 5 && $List['exp_year'] >= 4) {
                                                              $exp = "4 year to 5 year";
                                                              
                                                          }else if($List['exp_year'] < 6 && $List['exp_year'] >= 5) {
                                                              $exp = "5 year to 6 year";
                                                              
                                                          }else if($List['exp_year'] < 7 && $List['exp_year'] >= 6) {
                                                              $exp = "6 year to 7 year";
                                                              
                                                          } else if($List['exp_year'] >= 7) { 
                                                              $exp = "7 year and up";
                                                              
                                                          }else{
                                                            $exp = "";
                                                          }
                                                      ?>
                                                   <tr>
                                                      <td style="width:30px;"><input type="checkbox" name="check_users[]" class="check_users" value="<?php echo $List['id']; ?>"></td>
                                                      <td><?php echo $x1;?></td>
                                                      <td><?php echo $List['name'];?></td>
                                                      <td><?php echo $List['email'];?></td>
                                                      <td><?php echo $List['phone'];?></td>   
                                                        
                                                      <td><?php echo $List['device_type'];die; if(!empty($List['device_type'])){ if($List['device_type']=='ios'){  echo "iOS";} elseif ($List['device_type']=='android') {
                                                         echo "Android"; } } else{echo "Web";} ?></td>   
                                                      <td><?php if(!empty($List['last_used'])){ echo $List['last_used'];}?></td>  
                                                      <td><?php echo $List['app_version'];?></td>  
                                                      <td><?php echo $List['superpower'];?></td>   
                                                      <td><?php echo $List['location'];?></td>   
                                                      <td><?php if(!empty($List['current_salary'])){ echo $List['current_salary']; }else{ echo "NA"; }?></td>   
                                                      <td><?php echo $exp;?></td> 
                                                      <!-- <td><?php if($List['active'] == 1){echo "Approved";} else{echo "Not Approve";} ?></td> -->
                                                      
                                                   </tr>
                                                   <?php
                                                      $x1++;
                                                      }
                                                      ?>
                                                </tbody>
                                             </table>
                                             </div>
                                             
<script type="text/javascript">
   $('#sample-data-table').DataTable({
        "lengthMenu": [[10, 25, 50,100,200, -1], [10, 25, 50, 100,200, "All"]]
    });
</script>
                                          </div>
                                       </div>
                                       <div class="source custom-scrollbar">
                                          <div class="highlight">
                                             <pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-html" data-lang="html">
    
                                                    </code></pre>
                                          </div>
                                       </div>
                                    </div>
                                  
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- CONTENT -->
                  </div>
               </div>

               
            </div>
            <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
               <div class="list-group" class="date">
                  <div class="list-group-item subheader">TODAY</div>
                  <div class="list-group-item two-line">
                     <div class="text-muted">
                        <div class="h1"> Friday</div>
                        <div class="h2 row no-gutters align-items-start">
                           <span> 5</span>
                           <span class="h6">th</span>
                           <span> May</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Events</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Group Meeting</h3>
                        <p>In 32 Minutes, Room 1B</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Public Beta Release</h3>
                        <p>11:00 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Dinner with David</h3>
                        <p>17:30 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Q&amp;A Session</h3>
                        <p>20:30 PM</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Notes</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Best songs to listen while working</h3>
                        <p>Last edit: May 8th, 2015</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Useful subreddits</h3>
                        <p>Last edit: January 12th, 2015</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Quick Settings</div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Notifications</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Cloud Sync</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Retro Thrusters</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
            <!--<a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
            <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
            </a>-->
         </nav>
      </main>



<div class="modal" id="myModal2" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Send Notifications</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post">
                  <div class="form-group">
                     <input type="text" name="title" id="title" placeholder="Enter Title" class="form-control" maxlength="30" required="">
                     <label>Title</label>
                  </div>
                  <div class="form-group">
                     <input type="text" name="Message" id="message" placeholder="Enter Message" class="form-control" maxlength="100" required="">
                     <label>Message</label>
                  </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="send_btn" class="btn btn-secondary" onclick="getids()">Send Notification</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                
            </div>
             </form>
        </div>
    </div>
</div>


                  
                  <div class="Loader" style="display: none;">
                    <img src="<?php echo base_url(); ?>webfiles/img/Loader.gif" width="200px"> 
                  </div>

<style type="text/css">
  .Loader {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.5);
    z-index: 9999;
    align-items: center;
    justify-content: center;
    margin: auto;
    display: flex;
}
</style>
<script type="text/javascript">

    function getids(){
      var checkedValues = $('input:checkbox:checked').map(function() {
        if(this.value !== "on"){
            return this.value;
        }
    
      }).get();
      //console.log(checkedValues);exit();
      if(checkedValues==""){
         alert('Please select at least one checkbox');
      }else{
         var title = $('#title').val();
         var message = $('#message').val();
          var url =   '<?php echo base_url(); ?>/administrator/JobSeeker/send_notification';
          if(title==""){
            alert('Please enter title');
            return false;
          }else if(message==''){
            alert('Enter Message');
            return false;
          }else{
            $.ajax({
             type:"POST",
             url:url,
             data:{
                 title : title,
                 message : message, 
                 ids : JSON.stringify(checkedValues),
                 
             },
             beforeSend: function() {
                  // setting a timeout
                  $('.Loader').css('display','flex');
              },
             success:function(data)

              {
               //alert("Notification sent successfully");
               $('.Loader').css('display','none');
               $('#myModal2').modal('hide');
                //window.location.href= "<?php echo base_url('administrator/JobSeeker/notifications'); ?>";
           
              }
          });
          }
         
      }
      
    }


$('#check_all').on('click',function(){
        if(this.checked){
            $('.check_users').each(function(){
                this.checked = true;
            });
        }else{
             $('.check_users').each(function(){
                this.checked = false;
            });
        }
    });
</script>

<script type="text/javascript">
  $(document).ready(function(){
    $(".check_users").click(function(){
      var numberOfChecked = $('.check_users:checked').length;
      //var totalCheckboxes = $('input:checkbox').length;
      if(numberOfChecked >= 1) {
          $(".snotify").css("display","block");
      } else{
        $(".snotify").css("display","none");
      }
    });
  });
</script>
   </body>
</html>

