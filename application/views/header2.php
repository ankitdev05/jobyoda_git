<!DOCTYPE html>
<html lang="en-us">
<head>
    <title> <?php if(isset($meta_title)){ echo $meta_title; } else { echo "JobYoDA"; } ?> </title>
    <meta charset="UTF-8" />
    
    <meta name="title" content="<?php if(isset($meta_title)){ echo $meta_title; } ?>">
    <meta name="Description" content="<?php if(isset($meta_description)){ echo $meta_description; } ?>">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="google-site-verification" content="059_geDz93Z9O9EKdpV5BF3oQAB_Vu6keNmwG_l2kc8" />

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-MS37FX8');</script>
    <!-- End Google Tag Manager -->

    <!-- Global site tag (gtag.js) - Google Ads: 384592194 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-384592194"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'AW-384592194');
    </script>

    <?php
        $requestURI = $_SERVER['REQUEST_URI'];
        if($requestURI == "/signup_success") {
      ?>
          <!-- Event snippet for Jobseeker Registeration conversion page -->
          <script>
            gtag('event', 'conversion', {'send_to': 'AW-384592194/IOrgCJCL-I4CEMLSsbcB'});
          </script>
      <?php

        } else if($requestURI == "/thank_you") {
      ?>
            <!-- Event snippet for Submit lead form conversion page -->
            <script>
              gtag('event', 'conversion', {'send_to': 'AW-384592194/zAasCPuTlZMCEMLSsbcB'});
            </script>
      <?php
        }

        if($requestURI == "/job/description/MjgyNQ==") {
      ?>

              <script type="application/ld+json">
    {
      "@context" : "https://schema.org/",
      "@type" : "JobPosting",
      "title" : "Free Call Center Training | SITEL Palawan | Customer Support Representatives",
      "description" : "<p>Sitel is an equal opportunity employer and value diversity and inclusion at our organization. We do not discriminate on the basis of race/origin, religion, color, gender, sexual orientation, age, marital status, veteran status or disability status. As a Customer Service Associate, you will assist customers with concerns regarding their accounts. Recommend potential products or services to management by collecting customer information and analyzing customer needs over the phone. What we have to offer: • Medical coverage for you and your qualified dependents! • Paid leaves on the 91st day of employment • Medicine reimbursement of up to P10k • Career advancement through our Track Trainee program • Earn up to P10k for every successful employee referral What we need for the role: • Good command of English communication skills (both written and verbal) • High school graduates (Old curriculum) or Senior high school graduates • Technical/Vocational graduates • College graduates or college undergraduates with at least 60 units earned with at least 6 months customer service experience is a plus Here’s how you can apply: • Apply online just click the APPLY NOW button • Visit us at G/F Robinson's Place (Near Black Scoop Cafe), Puerto Princesa, Palawan. When you walk-in, kindly inform us that your source is JobYoda • Text your Palawan_Full Name_Address_CSR to 09988898719/ 09066386737 You may refer your friends and earn rewards for every hired referral! Text 09988898719/ 09066386737 to refer. Visit Sitel Recruitment Philippines on Facebook for more details.</p>",
      "identifier": {
        "@type": "PropertyValue",
        "name": "Sitel Palawan",
        "value": "NjY0"
      },
      "datePosted" : "2021-07-16",
      "validThrough" : "2021-07-31",
      "employmentType" : "FULL_TIME",
      "hiringOrganization" : {
        "@type" : "Organization",
        "name" : "Sitel",
        "sameAs" : "https://jobyoda.com/job/description/MjgyNQ==",
        "logo" : "https://jobyoda.com/webfiles/newone/images/logonew.png"
      },
      "jobLocation": {
      "@type": "Place",
        "address": {
        "@type": "PostalAddress",
        "streetAddress": "Eastville Citywalk",
        "addressLocality": " San Pedro National Highway",
        "addressRegion": "Mimaropa",
        "postalCode": "06001",
        "addressCountry": "PH"
        }
      },
      "baseSalary": {
        "@type": "MonetaryAmount",
        "currency": "PHP",
        "value": {
          "@type": "QuantitativeValue",
          "value": 10000,
          "unitText": "Month"
        }
      }
    }
    </script>
  

      <?php
        }
      ?>


      <script type="application/ld+json">
{
  "@context": "https://schema.org/",
  "@type": "JobPosting",
  "title": "Work-At-Home CSR Account | Alorica - By The Bay | Alorica Philippines",
  "description": "This is a general customer service position for a WORK-AT-HOME account. Talk to customers over the phone to resolve their questions or concerns Accurately document and update customer records in the computer system  Upsell products or services to customers, if appropriate Escalate customer complaints and calls to your manager when necessary.",
  "identifier": {
    "@type": "PropertyValue",
    "name": "Alorica Philippines",
    "value": "Nzg5"
  },
  "hiringOrganization" : {
    "@type": "Organization",
    "name": "Alorica Philippines",
    "sameAs": "https://jobyoda.com/company_details/NDk"
  },
  "industry": "Outsourcing/Offshoring",
  "employmentType": "FULL_TIME",
  "workHours": "Night Shift",
  "datePosted": "2021-05-20",
  "validThrough": "2021-05-31",
  "applicantLocationRequirements": {
    "@type": "Country",
    "name": "PH"
  },
  "jobLocationType": "TELECOMMUTE",
  "baseSalary": {
    "@type": "MonetaryAmount",
    "currency": "PHP",
    "value": {
      "@type": "QuantitativeValue",
      "minValue": 18500,
      "maxValue": 20000,
      "unitText": "MONTH"
    }
  },
  "responsibilities": "CSR Account",
  "skills": "• Creative Thinker
• Problem Solving Skills
• Analytical Skills
• Calm Under Pressure
• Open to Flexible Schedules
• Attention to Detail
• Empathetic to Customers
• Positive Attitude
• Strong Communication Skills",
  "qualifications": "High School Graduate"
}
</script>

    <script type="application/ld+json">
    {
     "@context":"http://schema.org",
     "@type":"LocalBusiness",
     "address":{
     "@type": "PostalAddress",
     "streetAddress":"Level 10-1 Fort Legend Tower, 3rd Avenue, 31st Street",
     "addressLocality":"Taguig",
     "addressRegion":"Metro Manila",
     "postalCode":"1634",
     "addressCountry":"Philippines",
     "telephone":"+63 917 872 1630",
    "email": "help@jobyoda.com"
      },
     "name":"Jobyoda",
      "url":"https://jobyoda.com/",
      "image":"https://jobyoda.com/webfiles/newone/images/logonew.png",
     "priceRange":"$$$"    
        }
    </script>

    <script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "WebSite",
      "url": "https://jobyoda.com/",
      "potentialAction": {
        "@type": "SearchAction",
        "target": "https://query.jobyoda.com/search?q={search_term_string}",
        "query-input": "required name=search_term_string"
      }
    }
    </script>

    <script type="application/ld+json">
      {
        "@context": "https://schema.org/",
        "@type": "JobPosting",
        "title": "Corporate Sales Specialist for a Search Engine Account | Concentrix Makati | Instant Screening", 
        "description": "Are you ready to get a boost in your sales career and be part of World’s Largest Search Engine Business? What’s in store for you? • Earn as much as P50,000* salary package monthly • Get P15,000 joining bonus (Wave 2 hires)** • Weekends off *(Salary package includes basic salary, allowances, and potential client bonuses) ** Terms and conditions apply Are you the one we are looking for? • Must have attended 1 to 2-years of College or University • With at least 1 to 2 years Corporate Sales experience in any industry (upselling and promodizing are not considered as sales experience)", 
        "identifier": {
          "@type": "propertyValue",
          "name": "Concentrix", 
          "value": "Nzg5" 
        },
        "datePosted": "2021-04-07",
        "employmentType": "CORPORATE SALES SPECIALIST", 
        "hiringOrganization": {
          "@type": "Organization",
          "name": "Concentrix",
          "sameAs": "https://jobyoda.com/site_details/Nzg5",
          "logo": "https://jobyoda.com/webfiles/newone/images/logonew.png" 
        },
        "jobLocation": {
          "@type": "place",
          "address": {
            "@type": "postalAddress",
            "addressLocality": "Ayala Avenue",
            "addressRegion": "Metro Manila",
            "postalCode": "1200",
            "addressCountry": "PH"
          }
        },
        "applicantLocationRequirements": {
          "@type": "Country",
          "name": "Philippines"
        },
        "jobLocationType": "TELECOMMUTE", 
        "baseSalary": {
          "@type": "MonetaryAmount",
          "currency": "PHP", 
          "value": {
            "@type": "QuantitativeValue",
            "value": 50000,
            "unitText": "MONTH"
          }
        }
      }
    </script>

    <meta name="facebook-domain-verification" content="38r9xjn6bc06n960spmpbqvjtnhntp" />
      
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-R64BFR96SR"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-R64BFR96SR');
    </script>
    <!-- Global site tag (gtag.js) - Google Ads: 851948051 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-851948051"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'AW-851948051');
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-178463617-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-178463617-1');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '197166028031236'); 
    fbq('track', 'PageView');
    </script>

    <noscript>
    <img height="1" width="1"
    src="imageproxy?token=932e1c629e9bf5d4381f631a825d98945b70e7d0e9200d8e3c300c34f4e6bb88&url=https://www.facebook.com/tr?id=197166028031236&ev=PageView
    &noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->


    <script type="text/javascript">

    _linkedin_partner_id = "3329732";

    window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];

    window._linkedin_data_partner_ids.push(_linkedin_partner_id);

    </script><script type="text/javascript">

    (function(l) {

    if (!l){window.lintrk = function(a,b){window.lintrk.q.push([a,b])};

    window.lintrk.q=[]}

    var s = document.getElementsByTagName("script")[0];

    var b = document.createElement("script");

    b.type = "text/javascript";b.async = true;

    b.src = https://snap.licdn.com/li.lms-analytics/insight.min.js;

    s.parentNode.insertBefore(b, s);})(window.lintrk);

    </script>

    <noscript>

    <img height="1" width="1" style="display:none;" alt="" src=https://px.ads.linkedin.com/collect/?pid=3329732&fmt=gif />

    </noscript>

    <link rel="icon" href="<?php echo base_url();?>recruiterfiles/images/fav.png" type="image/png" sizes="16x16">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url().'webfiles/';?>newone/css/style.css" />

    <link href="<?php echo base_url().'webfiles/';?>css/fontawesome.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url().'webfiles/';?>css/style.css" rel="stylesheet" type="text/css">

    <link href="<?php echo base_url().'webfiles/';?>css/owl.carousel.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url().'webfiles/';?>css/owl.theme.default.min.css" rel="stylesheet" type="text/css">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css"/>
    <link type="text/css" rel="stylesheet" href="<?php echo base_url().'webfiles/';?>newone/css/responsive.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url().'webfiles/';?>newone/css/aos.css" />

    <style type="text/css">
        .btn-group{height: 49px!important;}
        .pac-container {
            z-index: 10000 !important;
        }
        label.error{color:red!important;}
        .changemenu a{background:#00a94f!important;}
        .aboutHover li a{background: #084d87!important;}
    </style>
</head>

<body class=""> 
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MS37FX8"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>


<!-- Your Chat Plugin code -->
<div class="fb-customerchat"
  attribution=install_email
  page_id="663516800733629"
  theme_color="#ffc300">
</div>

    <header>
        <div class="Header">
            <nav class="navbar">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#Menu" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a href="<?php echo base_url(); ?>" class="navbar-brand">
                            <img src="<?php echo base_url().'webfiles/';?>newone/images/logonew.png">
                            <p>#1 BPO Job Platform</p>
                        </a>
                    </div>

                    <div class="collapse navbar-collapse" id="Menu">

                        <?php
                            if($this->session->userdata('usersess')) {
                                $usersess = $this->session->userdata('usersess');
                        ?>
                                <ul class="nav navbar-nav navbar-left">
                                    <li class="<?php if($_SERVER['REQUEST_URI']=='/'){?> changemenu <?php }?>"><a href="<?php echo base_url(); ?>">Home</a></li>
                                    <li class="aboutPos <?php if($_SERVER['REQUEST_URI']=='/about' || $_SERVER['REQUEST_URI']=='/how_it_works' || $_SERVER['REQUEST_URI']=='/faq' || $_SERVER['REQUEST_URI']=='/privacy_policy' || $_SERVER['REQUEST_URI']=='/terms'){?> changemenu <?php }?>"><a href="<?php echo base_url(); ?>about">About us</a>
                                      <div class="aboutHover">
                                          <ul>
                                              <li><a href="<?php echo base_url(); ?>how_it_works">How it works</a></li>
                                                <li><a href="<?php echo base_url();?>faq">FAQs</a></li>
                                                <li><a href="<?php echo base_url(); ?>privacy_policy">Privacy Policy </a></li>
                                               <li><a href="<?php echo base_url(); ?>terms">Terms of services</a></li>
                                          </ul>
                                        </div>
                                    </li>
                                    <li class="<?php if($_SERVER['REQUEST_URI']=='/blogs'){?> changemenu <?php }?>"><a href="<?php echo base_url(); ?>blogs">Blogs </a></li>
                                    <li class="<?php if($_SERVER['REQUEST_URI']=='/videos'){?> changemenu <?php }?>"><a href="<?php echo base_url();?>videos">Videos </a></li> 
                                    <li class="aboutPos <?php if($_SERVER['REQUEST_URI']=='/contact'){?> changemenu <?php }?>"><a href="<?php echo base_url();?>contact">Contact us</a>
                                        <div class="aboutHover">
                                          <ul>
                                                <!-- <li><a href="<?php echo base_url();?>contact">Chat</a></li> -->
                                                <li><a href="<?php echo base_url();?>contact">Message</a></li>
                                             
                                          </ul>
                                        </div>
                                    </li>
                                </ul>
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="https://play.google.com/store/apps/details?id=com.jobyodamo&showAllReviews=true" target="_blank" class="logoIn star"><img src="<?php echo base_url();?>webfiles/newone/images/star.png"><span>4</span></a></li> 
                                    <li><a href="https://www.facebook.com/jobyodapage/" target="_blank" class="logoIn"><img src="<?php echo base_url();?>webfiles/newone/images/fb.png"></a></li> 
                                    <li><a href="https://instagram.com/jobyoda_ig?igshid=1u7xaxp20etlb" target="_blank" class="logoIn"><img src="<?php echo base_url();?>webfiles/newone/images/insta.png"></a></li>
                                    <li><a href="<?php echo base_url();?>resume"><?php echo $usersess['name']; ?></a></li>
                                    <li><a href="<?php echo base_url();?>dashboard/logout">Logout</a></li> 
                                </ul>
                        <?php
                            } else {
                        ?>
                                <ul class="nav navbar-nav navbar-left">
                                    <li class="<?php if($_SERVER['REQUEST_URI']=='/'){?> changemenu <?php }?>"><a href="<?php echo base_url(); ?>">Home</a></li>
                                    <li class="aboutPos <?php if($_SERVER['REQUEST_URI']=='/about' || $_SERVER['REQUEST_URI']=='/how_it_works' || $_SERVER['REQUEST_URI']=='/faq' || $_SERVER['REQUEST_URI']=='/privacy_policy' || $_SERVER['REQUEST_URI']=='/terms'){?> changemenu <?php }?>"><a href="<?php echo base_url(); ?>about">About us</a>
                                      <div class="aboutHover">
                                          <ul>
                                              <li><a href="<?php echo base_url(); ?>how_it_works">How it works</a></li>
                                                <li><a href="<?php echo base_url();?>faq">FAQs</a></li>
                                                <li><a href="<?php echo base_url(); ?>privacy_policy">Privacy Policy </a></li>
                                               <li><a href="<?php echo base_url(); ?>terms">Terms of services</a></li>
                                          </ul>
                                        </div>
                                    </li>
                                    <li><a class="<?php if($_SERVER['REQUEST_URI']=='/blogs'){?> changemenu <?php }?>" href="<?php echo base_url(); ?>blogs">Blogs </a></li>
                                    <li><a class="<?php if($_SERVER['REQUEST_URI']=='/videos'){?> changemenu <?php }?>" href="<?php echo base_url();?>videos">Videos </a></li> 
                                    <li class="aboutPos <?php if($_SERVER['REQUEST_URI']=='/contact'){?> changemenu <?php }?>"><a href="<?php echo base_url();?>contact">Contact us</a>
                                             <div class="aboutHover">
                                          <ul>
                                              <!-- <li><a href="<?php echo base_url();?>contact">Chat</a></li> -->
                                              <li><a href="<?php echo base_url();?>contact">Message</a></li>
                                             
                                          </ul>
                                        </div>
                                    </li>
                                </ul>
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="https://play.google.com/store/apps/details?id=com.jobyodamo&showAllReviews=true" target="_blank" class="logoIn star"><img src="<?php echo base_url();?>webfiles/newone/images/star.png"><span>4</span></a></li> 
                                    <li><a href="https://www.facebook.com/jobyodapage/" target="_blank" class="logoIn"><img src="<?php echo base_url();?>webfiles/newone/images/fb.png"></a></li> 
                                    <li><a href="https://instagram.com/jobyoda_ig?igshid=1u7xaxp20etlb" target="_blank" class="logoIn"><img src="<?php echo base_url();?>webfiles/newone/images/insta.png"></a></li>
                                    <li><a href="<?php echo base_url();?>recruiter/" target="_blank">Recruiter Portal</a></li> 
                                    <li><a href="<?php echo base_url();?>login">Jobseeker Log In/Sign Up</a></li>
                                </ul>
                        <?php
                            }
                        ?>
                    </div>
                </div>
            </nav>
        </div>
    </header>

<input type="hidden" name="cur_lat" id="cur_lat" value="">
<input type="hidden" name="cur_long" id="cur_long" value="">
<?php
    $this->session->set_userdata('previous_url', current_url());
?>