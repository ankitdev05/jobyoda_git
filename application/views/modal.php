<?php
   include_once('mapscript.php');
?>
<div class="modal fade" id="gpsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
         </div>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                  <form>
                     <div class="addupdatecent">
                        <p>JobYoDA requires access to location. To enjoy all that JobYoDA has to offer, turn on your GPS and give JobYoDA access to your location.</p>
                        <div class="statsusdd">
                           <button type="button" class="btn btn-success" id="allowGps">OK</button>
                           <button type="button" class="btn btn-success" id="allowGpsclose" style="margin-left:10px">Close</button>
                        </div>
                     </div>
                  </form>
               </div>

               <div class="clear"></div>
               
            </div>
         </div>
      </div>
   </div>
</div>