
<?php include_once('header2.php'); ?>
<style>
  .btnsubmitclass{
    color: #fff;
    border: none;
    padding: 10px 20px;
    font-size: 17px;
  }

    #okup{
    border: 1px solid #ccc!important;
    padding: 3px 21px!important;
    background: #27aa60!important;
    color: #000!important;
    margin-top: 20px!important;
   }

   .addupdatecent p {
    margin: 0;
    font-size: 15px;
}
#exampleModalCenter1000 .modal-dialog.modal-dialog-centered{
    height: 410px!important;
}
#exampleModalCenter1000 .modal-content{
  height: 100%!important; 
}

.filldetails .addupdatecent p{
      padding: 0px 0 8px 0!important;
}
</style>

<section>
   <div class="BannerArea" style="background-image: url('<?php echo base_url(); ?>webfiles/img/newmap.jpg');">
      <h1> </h1>
   </div>
</section>

<section class="otpDesign boxs">
   <div class="container">
      <div class="messanger boxs" style="background: url('<?php echo base_url(); ?>webfiles/newone/images/bg-desk.png') no-repeat center;background-position-y: 25px;background-size: 46%;box-shadow: 0 6px 12px rgba(0,0,0,.175);">
         <h2 style="text-transform: capitalize;"> <?php echo $jobtitle;?> </h2>
         <h4 style="margin: 0;text-align: center;margin-top: 10px;"> <?php echo $cname;?> </h4>
          <?php
            if($questionData) {
          ?>
         
          <form id="chatbotform" action="<?php echo base_url();?>save_chatbot" method="post">

           <div class="messchat boxs" style="padding: 75px 0px 120px;margin-bottom:100px;">
              
              <div class="recivedMess boxs">
                 <p>Thanks for applying! The hiring manger has a few questions to get to know you better, this will also increase your chances of getting hired.All the best!</p>
              </div>
            <?php
              $q = 1;
              $countques = count($questionData);
              foreach($questionData as $questionD) {
            ?>
                <div class="recivedMess boxs ques<?php echo $q;?>" <?php if($q!=1){echo "style='display:none'";}?> >
                   <p> <?php echo $questionD['question']; ?> </p>
                </div>
                
                <?php if($questionD['type'] == "option") { ?> 
                <div class="optBtn boxs opti<?php echo $q;?>" <?php if($q!=1){echo "style='display:none'";}?> >
                   <ul>
                    <?php
                      foreach($questionD['options'] as $option) {
                    ?>
                      <li><button type="button" onclick="chatbutton('<?php echo $option; ?>', <?php echo $q;?>,'<?php echo $questionD['id']; ?>','<?php echo $questionD['answers']; ?>', '<?php echo $countques; ?>', '<?php echo $questionD['haverequired']; ?>')"> <?php echo $option; ?> </button></li>
                    <?php
                      }
                    ?>
                   </ul>
                </div>
                <?php } ?>

                <div class="sendMess boxs useranswer<?php echo $q;?>" style="display:none;">
                  
                </div>
                
                <?php if($questionD['type'] == "text") { ?> 
                 <div class="typeMessage boxs boxtextans<?php echo $q;?>" <?php if($q == 1) {} else { ?> style="display:none;" <?php } ?>>
                   <textarea class="form-control textans<?php echo $q;?>" placeholder="Type Here...."></textarea>
                   <button type="button" onclick="chattextbutton(<?php echo $q;?>,'<?php echo $questionD['id']; ?>','<?php echo $questionD['answers']; ?>', '<?php echo $countques; ?>', '<?php echo $questionD['haverequired']; ?>')"><i class="fa fa-paper-plane" style="margin-right:10px;"></i></button>
                 </div>
                <?php } ?>

            <?php
                $q++;
              }
            ?>

              <div class="recivedMess boxs endMess" style="display:none;">
                  <p>You have successfully applied for your Dream job. To increase your chances of getting hired please make sure you complete your Profile via the JobYoDA App or website</p>
              </div>
    </div>
            <input type="hidden" value="<?php echo $q;?>" id="counterchat">
          
           <divc class="btnsubmitclassdiv" style="display:none;">
             <center style="margin-bottom: 0px;">
                  <!-- <input type="hidden" name="answerchat" value="" id="answerchat"> -->
                  <input type="hidden" name="questionchat" value="" id="questionchat">
                  <input type="hidden" name="job_id" value="<?php echo $jobid;?>" id="jobid">
                  <input type="hidden" name="percentage" value="" id="jobpercent">
                  <p class="btnsubmitclass"> </p>
             </center>
           </divc>

         </form>
      <?php
        }      
      ?>
      </div>
      
   </div>
</section>


<div class="modal fade" id="exampleModalCenter1000" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document" style="width: 37%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close updt" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                  <form>
                     <div class="addupdatecent">
                        <!-- <img src="<?php //echo base_url() . 'webfiles/'; ?>img/savedbighover.png"> -->
                        
                        <!-- <p class="jobsuccess">Job Applied Successfully</p> -->

                        <p style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>All the best from the JobYoDA Team!</p> 
                        <p style='text-align:center;color:#fbaf31;line-height:28px;margin-bottom:0px;font-weight:500;'>Did you know JobYoDA offers a FREE Venti Coffee to all hires?</p> 
                        <p style='text-align:center;color:#fbaf31;line-height:28px;margin-bottom:0px;font-weight:500;'>To claim, all you need to do is:</p> 
                        <p style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>1. Declare JobYoDA as the source of your application</p>
                        <p style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>2. Send us and email at Help@jobyoda.com to claim your voucher if you get hired</p>
                        <p style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>3. We will validate with the recruiter and process</p>
                        <p style='text-align:center;color:#00a94f;;line-height:28px;margin-bottom:0px;font-weight:500;'>Good Luck!</p>

                        <div class="statsusdd" style="display: inherit; float: none;">
                           
                           <button  type="button" class="updt" id="okup" data-dismiss="modal" aria-label="Close">Ok</button>

                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<?php include_once('footer1.php'); ?>

<script type="text/javascript">
    function chattextbutton(val2, val3, val4, val5, val6) {
        var textanswer = ".textans"+val2; 
        var gettextanswer = $(textanswer).val();

        var perVal = 100/parseInt(val5);
        var perPercentage = 0;
        
        if(gettextanswer == val4) {
            perPercentage = perVal;
        } else {
            if(parseInt(val6) == 1) {
                var flaging = 2;
            }
        }

        var answerclass = ".useranswer"+val2;
        $(answerclass).css("display","block");
        $(answerclass).html("<input type='hidden' name='answerchat[]' value='"+gettextanswer+"'><input type='hidden' id='percentans"+val2+"' name='persent' value='"+perPercentage+"'><p>"+gettextanswer+"</p>");

        var incrementIndex = parseInt(val2) + 1;
        var questionchat = $("#questionchat").val();
        if(questionchat.length > 0) {
            questionchat = questionchat +','+val3;
        } else {
            questionchat = val3;
        }
        $("#questionchat").val(questionchat);   

        $(".boxtextans"+val2).css("display","none");
        $(".ques"+incrementIndex).css("display","block");
        $(".opti"+incrementIndex).css("display","block");
        $(".boxtextans"+incrementIndex).css("display","block");

        var counterchat = $("#counterchat").val();
        counterchat = parseInt(counterchat) - 1;
        if(counterchat == val2) {
            $(".btnsubmitclassdiv").css("display","block");
            $(".endMess").css("display","block");

            var totalper = 0;
            for($perr=1; $perr<counterchat; $perr++) {
              var idper = "#percentans"+$perr;
              console.log(idper);
              var peridval = $(idper).val(); 
              totalper = parseInt(totalper) + parseInt(peridval);
              console.log(peridval);
            }
            console.log(totalper);
            $("#jobpercent").val(totalper);
            if(flaging) {
              $("#getpercentage").html("Failed");
            } else {
              $("#getpercentage").html(totalper+"%");
            }

            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>dashboard/chatbot_job_save",
                data: $("#chatbotform").serialize(),
                beforeSend: function() {
                  $(".btnsubmitclass").html('<center><img src="https://jobyoda.com/webfiles/newone/loadericon.gif"></center>');
                },
                success: function(data) { 

                    setTimeout( function() {
                      //window.location.href="<?php //echo base_url();?>appliedjobs";

                      $('#exampleModalCenter1000').modal('show');

                    }, 3000 );
                },
                error: function (error_) {
                    console.log(error_);
                }
            });
        }
    }

   function chatbutton(val1, val2, val3, val4, val5, val6) {
      
      var perVal = 100/parseInt(val5);
      var perPercentage = 0;
      if(val1 == val4) {
          perPercentage = perVal;
      } else {
          if(parseInt(val6) == 1) {
              var flaging = 2;
          }
      }

      var answerclass = ".useranswer"+val2;
      $(answerclass).css("display","block");
      $(answerclass).html("<input type='hidden' name='answerchat[]' value='"+val1+"'><input type='hidden' id='percentans"+val2+"' name='persent' value='"+perPercentage+"'><p>"+val1+"</p>");

      var incrementIndex = parseInt(val2) + 1;

      var questionchat = $("#questionchat").val();
      if(questionchat.length > 0) {
          questionchat = questionchat +','+val3;
      } else {
          questionchat = val3;
      }
      $("#questionchat").val(questionchat);      

      $(".opti"+val2).css("display","none");
      $(".ques"+incrementIndex).css("display","block");
      $(".opti"+incrementIndex).css("display","block");
      $(".boxtextans"+incrementIndex).css("display","block");

      var counterchat = $("#counterchat").val();
      counterchat = parseInt(counterchat) - 1;

      if(counterchat == val2) {
          $(".btnsubmitclassdiv").css("display","block");
          $(".endMess").css("display","block");

          var totalper = 0;
          for($perr=1; $perr<=counterchat; $perr++) {
              var idper = "#percentans"+$perr;
              var peridval = $(idper).val(); 
              totalper = parseInt(totalper) + parseInt(peridval);
    
          }
          $("#jobpercent").val(totalper);
          
          if(flaging) {
            $("#getpercentage").html("Failed");
          } else {
            $("#getpercentage").html(totalper+"%");
          }

          $.ajax({
              type: "POST",
              url: "<?php echo base_url() ?>dashboard/chatbot_job_save",
              data: $("#chatbotform").serialize(),
              beforeSend: function() {
                $(".btnsubmitclass").html('<center><img src="https://jobyoda.com/webfiles/newone/loadericon.gif"></center>');
              },
              success: function(data) { 
                  setTimeout( function() {
                      //window.location.href="<?php //echo base_url();?>appliedjobs";

                      $('#exampleModalCenter1000').modal('show');

                    }, 3000 );
              },
              error: function (error_) {
                  console.log(error_);
              }
          });
      }
   }
</script>

<script type="text/javascript">
   $("#okup").on("click",function(){
         window.location = "<?php echo base_url(); ?>appliedjobs";
   });
</script>