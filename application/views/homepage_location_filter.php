<ul>
 <?php
    if(!empty($citySearchs)) {
 ?>
 <?php
    foreach($citySearchs as $citySearch) {
        $cityArr[] = $citySearch['cityname'];
 ?>
        <li>
            <figure>
                <a href="javascript:void(0)" onclick='onchangecity("<?php echo $citySearch['cityname']; ?>")'>
                    <img src="<?php echo $citySearch['thumbnail']; ?>">
                </a>
            </figure>
            <figcaption>
                <h5 style="font-size: 13px;"> 
                    <a href="javascript:void(0)" onclick='onchangecity("<?php echo $citySearch['cityname']; ?>")'>
                        <?php echo $citySearch['cityname']; ?>
                    </a>
                </h5>
            </figcaption>

        </li>
 <?php
    }
    if($allcities) {
        $arrImg = ["cityimages/small/old/Baguio.jpg","cityimages/small/old/Cebu.jpg","cityimages/small/old/Clark.jpg","cityimages/small/old/Davao.jpg","cityimages/small/old/Iloilo.jpg"];
        foreach($allcities as $allcity) {
            $flag = 1;
            foreach($cityArr as $cityAr) {
                if (strpos($allcity, $cityAr) !== false) {
                    $flag = 2;
                }
            }
            if($flag == 1) {

                $getImgRand = array_rand($arrImg);
 ?>
                <li>
                    <figure>
                        <a href="javascript:void(0)" onclick='onchangecity("<?php echo $allcity; ?>")'>
                            <img src="<?php echo base_url(); ?><?php echo $arrImg[$getImgRand]; ?>" style="height: 77px;">
                        </a>
                    </figure>
                    <figcaption>
                        <h5 style="font-size: 13px;"> 
                            <a href="javascript:void(0)" onclick='onchangecity("<?php echo $allcity; ?>")'>
                                <?php echo $allcity; ?>
                            </a>
                        </h5>
                    </figcaption>

                </li>
 <?php
            }
        }
    }
 }
 ?>
 </ul>