<?php include_once('header2.php'); 
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {

    } else {
        $link = "https";
        $link .= "://";
        $link .= $_SERVER['HTTP_HOST'];
        $link .= $_SERVER['REQUEST_URI'];
        redirect($link);
    }
?>

<style>
  .text-danger{
    font-size: 12px!important;
    color: #a94442!important;
  }
  .multiselect-container.dropdown-menu{
      width: 557px!important;
  }
  .microtext{
        color: #fbaf31;
    font-size: 12px;
    font-weight: 500;
  }
</style>

<section>
   <div class="BannerArea" style="background-image: url('<?php echo base_url(); ?>webfiles/img/newmap.jpg');">
      <h1>Signup Professional Details </h1>
   </div>
</section>

<section class="personalDetail">
   <div class="container">
      <div class="col-sm-6">
         <div class="personalSteps">
            <div class="topSteps topSteps2">
               <ul>
                  <li> <img src="<?php echo base_url(); ?>webfiles/newone/images/step1.svg" class="img-fluid" alt = "step"></li>
                  <li class="color"> <img src="<?php echo base_url(); ?>webfiles/newone/images/stepn.svg" class="img-fluid" alt = "step"></li>
               </ul>
            </div>
            <div class="bgPersonal bgPersonal2">
               <img src="<?php echo base_url(); ?>webfiles/newone/images/bgdet.svg" class="img-fluid" alt = "step">
            </div>
            <p class="bgBottom">"<i>Every accomplishment starts with the decision to try.</i>"</p>
         </div>
      </div>
      <div class="col-sm-6">

         <form method="post" action="<?php echo base_url();?>auth/signupsteptwo" id="signupform" enctype="multipart/form-data">

            <div class="personalRight otpRight">
               <h2 class="head">Professional Details</h2>
               
               <div class="personalFlex">

                 <div class="form-group formPost iconDrop">
                     <label class="lab"></label>
                      <span class="iconsArrow"> <img src="<?php echo base_url(); ?>webfiles/newone/images/arrow.svg" class="img-fluid" alt="icon"></span>
                    
                    <select name="education" class="form-control inputAll" placeholder ="Select your Highest Qualification ">
                       <option value="" class="demo">Select your Highest Qualification </option>
                       <option value="High School Graduate" <?php if(!empty($userData['education']) && $userData['education'] == "High School Graduate"){ echo "selected"; }?>> High School Graduate </option>
                       <option value="Vocational" <?php if(!empty($userData['education']) && $userData['education'] == "Vocational"){ echo "selected"; }?>> Vocational </option>
                       <option value="Undergraduate" <?php if(!empty($userData['education']) && $userData['education'] == "Undergraduate"){ echo "selected"; }?>> Undergraduate </option>
                       <option value="Associate Degree" <?php if(!empty($userData['education']) && $userData['education'] == "Associate Degree"){ echo "selected"; }?>> Associate Degree </option>
                       <option value="College Graduate" <?php if(!empty($userData['education']) && $userData['education'] == "College Graduate"){ echo "selected"; }?>> College Graduate </option>
                       <option value="Post Graduate" <?php if(!empty($userData['education']) && $userData['education'] == "Post Graduate"){ echo "selected"; }?>> Post Graduate </option>
                    </select>

                    <?php if(isset($signuperrors['education'])){echo "<p class='text-danger'>".$signuperrors['education']."</p>"; } ?>
                 </div>

                 <div class="form-group formPost iconDrop">
                    <label class="lab"></label> 
                    <span class="iconsArrow lastbpoarrow"> <img src="<?php echo base_url(); ?>webfiles/newone/images/arrow.svg" class="img-fluid" alt="icon"></span>
                    
                    <select name="topbpo" class="form-control inputAll lastbpo" placeholder ="Select Your Current/Last Company">
                       <option value="">Select Your Current/Last Company </option>
                       <option value="other" <?php if(!empty($userData['topbpo']) && $userData['topbpo'] == "other"){ echo "selected"; }?>> Other </option>
                       <?php
                          foreach($topbpos as $topbpo) {
                       ?>
                             <option value="<?php echo $topbpo['name']; ?>" <?php if(!empty($userData['topbpo']) && $userData['topbpo'] == $topbpo['name']){ echo "selected"; }?>> <?php echo $topbpo['name']; ?> </option>
                       <?php
                          }
                       ?>
                       
                    </select>
                    <?php if(isset($signuperrors['topbpo'])){echo "<p class='text-danger'>".$signuperrors['topbpo']."</p>"; } ?>
                </div>
                
               </div>

               <div class="form-group formPost iconDrop lastbpoother" <?php if(isset($userData['topbpoother'])) {} else { ?> style="display:none" <?php } ?>>
                    <label class="lab"></label> 

                    <input type="text" class="form-control inputAll" name="" placeholder="Your Current/Last Company Other" value="<?php if(!empty($userData['topbpo'])){ echo $userData['topbpoother']; }?>">

                    <?php if(isset($signuperrors['topbpoother'])){echo "<p class='text-danger'>".$signuperrors['topbpoother']."</p>"; } ?>
                </div>


                <div class="form-group">
                  <label class="labelAl">Add Resume<span>(Optional)</span></label>
                  <div class='file-input'>
                     <input type='file' name="resumeFile" accept=".pdf,.doc,.docx" class="resume_upload_class">
                     <span class='button'><img src="<?php echo base_url(); ?>webfiles/newone/images/selecter.svg" class="img-fluid" alt = "selector"></span>
                     <span class='label' data-js-label></label>
                  </div>
                  <?php if(isset($signuperrors['resumeFile'])){echo "<p class='text-danger'>".$signuperrors['resumeFile']."</p>"; } ?>
               </div>


               <div class="form-group formPost iconDrop">
                       <label class="lab"></label>
                    <span class="iconsArrow"> <img src="<?php echo base_url(); ?>webfiles/newone/images/arrow.svg" class="img-fluid" alt="icon"></span>
                  <select name="industry" class="form-control inputAll changeindustry" placeholder ="Select Industry ">
                     <option value="">Select Industry </option>
                     <?php
                        foreach($industries as $industry) {
                     ?>
                           <option value="<?php echo $industry['name']; ?>"> <?php echo $industry['name']; ?> </option>
                     <?php
                        }
                     ?>
                  </select>
                  
                  <span class="microtext"> * So we know what sector you belong to </span>
                  
                  <?php if(isset($signuperrors['industry'])){echo "<p class='text-danger'>".$signuperrors['industry']."</p>"; } ?>
               </div>


               <div class="personalFlex">
                  <div class="form-group formPost iconDrop">
                          <label class="lab"></label>
                       <span class="iconsArrow"> <img src="<?php echo base_url(); ?>webfiles/newone/images/arrow.svg" class="img-fluid" alt="icon"></span>
                     <select name="expyear" class="form-control inputAll" placeholder ="Select Years of Experience ">
                        <option value="">Select Years of Experience </option>
                        <?php for($i=0;$i<=50;$i++) { ?>
                           
                           <option value="<?php echo $i;?>" <?php if(!empty($userData['expyear']) && $userData['expyear'] == $i){ echo "selected"; }?>> <?php echo $i.' '.'years';?> </option>
                        
                        <?php }?>
                     </select>

                     <?php if(isset($signuperrors['expyear'])){echo "<p class='text-danger'>".$signuperrors['expyear']."</p>"; } ?>
                  </div>

                  <div class="form-group formPost iconDrop">
                          <label class="lab"></label>
                       <span class="iconsArrow"> <img src="<?php echo base_url(); ?>webfiles/newone/images/arrow.svg" class="img-fluid" alt="icon"></span>
                     <select name="expmonth" class="form-control inputAll" placeholder ="Select Months of Experience ">
                        <option value="">Select Months of Experience </option>
                        <?php for($i=0;$i<=12;$i++) {?>
                           
                           <option value="<?php echo $i;?>" <?php if(!empty($userData['expmonth']) && $userData['expmonth'] == $i){ echo "selected"; }?>><?php echo $i.' '.'months';?></option>
                        
                        <?php }?>
                     </select>

                     <?php if(isset($signuperrors['expmonth'])){echo "<p class='text-danger'>".$signuperrors['expmonth']."</p>"; } ?>
                  </div>
               </div>
               <input type="hidden" name="bpoyear" value="0">
               <input type="hidden" name="bpomonth" value="0">

               <!-- <div class="personalFlex">
                  <div class="form-group formPost iconDrop">
                          <label class="lab"></label>
                       <span class="iconsArrow"> <img src="<?php //echo base_url(); ?>webfiles/newone/images/arrow.svg" class="img-fluid" alt="icon"></span>
                     <select name="bpoyear" class="form-control inputAll" placeholder ="Select BPO Exp. (Years)">
                        <option value="">Select BPO Exp. (Years) </option>
                        <?php //for($i=0;$i<=50;$i++) { ?>
                           
                           <option value="<?php //echo $i;?>" <?php //if(!empty($userData['bpoyear']) && $userData['bpoyear'] == $i){ echo "selected"; }?>> <?php //echo $i.' '.'years';?> </option>
                        
                        <?php //}?>
                     </select>

                     <?php //if(isset($signuperrors['bpoyear'])){echo "<p class='text-danger'>".$signuperrors['bpoyear']."</p>"; } ?>
                  </div>
                  <div class="form-group formPost iconDrop">
                          <label class="lab"></label>
                       <span class="iconsArrow"> <img src="<?php //echo base_url(); ?>webfiles/newone/images/arrow.svg" class="img-fluid" alt="icon"></span>
                     <select name="bpomonth" class="form-control inputAll" placeholder ="Select BPO Exp. (Months) ">
                        <option value="">Select BPO Exp. (Months) </option>
                        <?php //for($i=0;$i<=12;$i++) {?>
                           
                           <option value="<?php //echo $i;?>" <?php //if(!empty($userData['bpomonth']) && $userData['bpomonth'] == $i){ echo "selected"; }?>><?php //echo $i.' '.'months';?></option>
                        
                        <?php //}?>
                     </select>

                     <?php //if(isset($signuperrors['bpomonth'])){echo "<p class='text-danger'>".$signuperrors['bpomonth']."</p>"; } ?>
                  </div>
               </div> -->

               <div class="personalFlex">
                   <div class="form-group formPost iconDrop">
                           <label class="lab"></label>
                        <span class="iconsArrow"> <img src="<?php echo base_url(); ?>webfiles/newone/images/arrow.svg" class="img-fluid" alt="icon"></span>
                      <select name="specialization" class="form-control inputAll changecat" placeholder ="Select Your Specialization ">
                         <option value="">Select Your Specialization </option>
                         <?php
                            //foreach($categories as $category) {
                         ?>
                               <!-- <option value="<?php //echo $category['category']; ?>"> <?php //echo $category['category']; ?> </option> -->
                         <?php
                            //}
                         ?>
                      </select>

                      <span class="microtext"> * So we know what job you are best at </span>

                      <?php if(isset($signuperrors['specialization'])){echo "<p class='text-danger'>".$signuperrors['specialization']."</p>"; } ?>
                   </div>

                   <div class="form-group formPost iconDrop">
                           <label class="lab"></label>
                           <span class="iconsArrow"> <img src="<?php echo base_url(); ?>webfiles/newone/images/arrow.svg" class="img-fluid" alt="icon"></span>
                      <select name="sub_specialization" class="form-control inputAll getsubcat" placeholder ="Select Your Specialization ">
                         <option value="">Select Your Sub Specialization </option>
                      </select>

                      <?php if(isset($signuperrors['sub_specialization'])){echo "<p class='text-danger'>".$signuperrors['sub_specialization']."</p>"; } ?>
                   </div>
               </div>

               <div class="form-group formPost iconDrop">
                    <label class="lab"></label>

                    <span class="iconsArrow" style="top: 15px;"> <img src="<?php echo base_url(); ?>webfiles/newone/images/arrow.svg" class="img-fluid" alt="icon"></span>
                  
                  <select name="joblevel" class="form-control inputAll" placeholder ="Select your Job Level ">
                     <option value="" class="demo">Select your Job Level </option>
                     <?php
                        foreach($levellists as $levellist) {
                      ?>
                          <option <?php if(!empty($userData['joblevel'])){ if($userData['joblevel']==$levellist['level']) { echo "selected"; } } ?>  value="<?php echo $levellist['level'];?>" > <?php echo $levellist['level'];?> </option>
                     <?php
                        }
                     ?>
                  </select>
                  
                  <?php if(isset($signuperrors['joblevel'])){echo "<p class='text-danger'>".$signuperrors['joblevel']."</p>"; } ?>
               </div>

               <div class="form-group formPost view iconDrop intrestedmainjob" style="display: none;">
<!--                    <label class="lab"></label>-->
                    <span class="interestmsg text-center text-warning"> </span>

                    <span class="iconsArrow"> <img src="<?php echo base_url(); ?>webfiles/newone/images/arrow.svg" class="img-fluid" alt="icon"></span>
                    
                    <select class="multiselect-ui" id="framework" name="intrested[]" multiple style="display: none;">
                       <?php
                          // foreach($intrestedin as $getintrested) {
                          //    if($getintrested['subcategory'] == "ALL") {} else {
                        ?>
                            <!-- <option <?php //if(!empty($userData['intrested'])){ if($userData['intrested']==$getintrested['subcategory']) { echo "selected"; } } ?>  value="<?php //echo $getintrested['subcategory'];?>" > <?php //echo $getintrested['subcategory'];?> </option> -->
                       <?php
                          //    }
                          // }
                       ?>
                    </select>
                  
                  <span class="microtext"> * So we can send you relevant job postings </span>

                  <?php if(isset($signuperrors['intrested'])){echo "<p class='text-danger'>".$signuperrors['intrested']."</p>"; } ?>
               </div>


               <div class="form-group formPost view iconDrop benefitsmainjob">
<!--                    <label class="lab"></label>-->
                    <span class="benefitsmsg text-center text-warning"> </span>

                    <span class="iconsArrow"> <img src="<?php echo base_url(); ?>webfiles/newone/images/arrow.svg" class="img-fluid" alt="icon"></span>
                    
                    <select class="multiselect-ui" id="frameworkone" name="benefits[]" multiple>
                       <?php
                          foreach($jobbenefits as $jobbenefit) {
                        ?>
                            <option <?php if(!empty($userData['benefits'])){ if($userData['benefits']==$jobbenefit['name']) { echo "selected"; } } ?>  value="<?php echo $jobbenefit['name'];?>" > <?php echo $jobbenefit['name'];?> </option>
                       <?php
                          }
                       ?>
                    </select>

                    <span class="microtext"> * 3 Job benefits you would like us to notify you about </span>

                  <?php if(isset($signuperrors['benefits'])){echo "<p class='text-danger'>".$signuperrors['benefits']."</p>"; } ?>
               </div>

               <div class="rememberAl">
                  <div class="new">
                     <div class="form-group">
                        <input type="checkbox" name="newsletters" value="1" id="rem1">
                        <label for="rem1" class="remember remember2">I allow JobYoDA to send me Monthly Newsletters. </label>

                        <?php if(isset($signuperrors['newsletters'])){echo "<p class='text-danger'>".$signuperrors['newsletters']."</p>"; } ?>
                     </div>
                  </div>
               </div>

               <div class ="rememberAl">
                  <div class="new">
                     <div class="form-group">
                        <input type="checkbox" name="option1" id="rem">
                        <label for="rem" class="remember remember2">I accept all <span><a href="<?php echo base_url(); ?>terms" target="_blank">Terms &amp; Conditions</a></span> and <span><a href="<?php echo base_url(); ?>privacy_policy" target="_blank">Privacy Policy.</a></span></label>

                        <?php if(isset($signuperrors['option1'])){echo "<p class='text-danger'>".$signuperrors['option1']."</p>"; } ?>
                     </div>
                  </div>
               </div>
               <div class="resetPass">
                  <button type="submit" class="commonBtn1">Submit</button>
               </div>
            </div>

         </form>
      </div>
   </div>
</section>
<?php include_once('footer1.php'); ?>



<script type="text/javascript">

  $(document).ready(function() {

    $("#signupform").on("submit", function() {
      $(".resetPass").html('<div class="popularloader"><center style="margin-bottom: 7px;"><img src="https://jobyoda.com/webfiles/newone/loadericon.gif" style="width:40px"></center></div>');

      $(".popularloader").fadeIn();
    });//submit
  });//document ready
</script>

<script>
$(document).ready(function() {
    $(".changeindustry").on('change', function() {
        var industry = $(this).val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "auth/getcategory",
            data: {
                industry: industry
            },
            cache: false,
            success: function(htmldata) {
              
                $('.changecat').html(htmldata);
            },
            error: function() {
                console.log('error');
            }
        });
        $('.intrestedmainjob').css("display","none");
        $('#framework').html("");
        $("#framework").multiselect('destroy');

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "auth/getjobintrestedin",
            data: {
                industry: industry
            },
            cache: false,
            success: function(htmldata) {
              
                $('.intrestedmainjob').css("display","block");
                $('#framework').css("display","block");
                $('#framework').html(htmldata);
                addonmultiselect();
            },
            error: function() {
                console.log('error');
            }
        });
    });
});
</script>
<script>
$(document).ready(function() {
    $(".changecat").on('change', function() {
        var cat = $(this).val();
        var industry = $(".changeindustry :selected").val();
        
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "auth/getsubcategory",
            data: {
                cat: cat,
                industry:industry
            },
            cache: false,
            success: function(htmldata) {
         
                $('.getsubcat').html(htmldata);
            },
            error: function() {
                console.log('error');
            }
        });
    });
});
</script>

<script type="text/javascript">
    //$(document).ready(function() {
      function addonmultiselect() {

        $('#framework').multiselect({
            nonSelectedText: "Job Interests (Select any 3)",
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            onChange: function(option, checked) {
                // Get selected options.
                var selectedOptions = $('.intrestedmainjob .multiselect-ui option:selected');
                
                if (selectedOptions.length >= 3) {
                     $('.interestmsg').html("You can select 3 Job interests only");
                }
                if (selectedOptions.length >= 3) {
                    // Disable all other checkboxes.
                    var nonSelectedOptions = $('.intrestedmainjob .multiselect-ui option').filter(function() {
                        return !$(this).is(':selected');
                    });
 
                    nonSelectedOptions.each(function() {
                        var input = $('.intrestedmainjob input[type="checkbox"][value="' + $(this).val() + '"]');
                        input.prop('disabled', true);
                        input.parent('li').addClass('disabled');
                    });
                }
                else {
                    // Enable all checkboxes.
                    $('.intrestedmainjob .multiselect-ui option').each(function() {
                        var input = $('.intrestedmainjob input[type="checkbox"][value="' + $(this).val() + '"]');
                        input.prop('disabled', false);
                        input.parent('li').addClass('disabled');
                    });
                }
            }
        });
      }
    //});

    $('#frameworkone').multiselect({
            nonSelectedText: "Job Benefits (Select any 3)",
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            onChange: function(option, checked) {
                // Get selected options.
                var selectedOptions = $('.benefitsmainjob .multiselect-ui option:selected');
                
                if (selectedOptions.length >= 3) {
                     $('.benefitsmsg').html("You can select 3 Job benefits only");
                }
                if (selectedOptions.length >= 3) {
                    // Disable all other checkboxes.
                    var nonSelectedOptions = $('.benefitsmainjob .multiselect-ui option').filter(function() {
                        return !$(this).is(':selected');
                    });
 
                    nonSelectedOptions.each(function() {
                        var input = $('input[type="checkbox"][value="' + $(this).val() + '"]');
                        input.prop('disabled', true);
                        input.parent('li').addClass('disabled');
                    });
                }
                else {
                    // Enable all checkboxes.
                    $('.benefitsmainjob .multiselect-ui option').each(function() {
                        var input = $('input[type="checkbox"][value="' + $(this).val() + '"]');
                        input.prop('disabled', false);
                        input.parent('li').addClass('disabled');
                    });
                }
            }
        });
      
</script>
<script>
  $(document).ready(function() {
      $(".lastbpo").change(function() {
          var selected = $(this).val();
          if(selected == "other") {
              $(".lastbpoother").css("display","block");
              $(".lastbpoother > input").attr("name","topbpoother");
          } else {
              $(".lastbpoother").css("display","none");
          }
      });
  });
</script>
<script type = "text/javascript" >
    $(document).ready(function() {
        $('#signupform').validate({
            rules: {
                option1: {
                    required: true,
                },
            },
            messages: {
                option1: {
                    required: 'Please accept Terms & Conditions and Privacy Policy to proceed'
                },
            }
        });
    }); 
</script>