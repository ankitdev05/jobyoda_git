<?php $recruiterId = $this->session->userdata('userSession');?>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>JobYoDA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="CreativeLayers">
    <link rel="icon" href="<?php echo base_url().'recruiterfiles/';?>images/fav.png" type="image/png" sizes="16x16">
    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap-grid.css" />
    <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/icons.css">
    <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/all.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/fontawesome.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/responsive.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/colors/colors.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/style.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
</head>
<style>
    a:hover {
        text-decoration: none;
    }

    .tech i.fa.fa-ellipsis-h {
        float: right;
    }

    .progr span.progress-bar-tooltip {
        position: initial;
        color: #fff;
    }

    .manage-jobs-sec {
        float: none;
        width: 100%;
        margin: 0 auto;
    }

    .link a {
        background: #26ae61;
        color: #fff;
        word-break: keep-all;
        font-size: 11px;
        line-height: 0;
        padding: 10px;
    }

    .link {
        float: left;
        width: 100px;
    }

    .btn-extars {
        display: none;
    }

    .inner-header {
        float: left;
        width: 100%;
        position: relative;
        padding-top: 60px;
        padding-bottom: 15px;
        z-index: 0;
    }

    .tree_widget-sec>ul>li.inner-child.active>a {
        color: #26ae61;
    }

    .tree_widget-sec>ul>li.inner-child.active>a i {
        color: #26ae61;
    }

    .contact-edit .srch-lctn:hover {
        background: #26ae61;
        color: #ffffff;
        border-color: #26ae61;
    }

    .contact-edit .srch-lctn {
        color: #26ae61;
        border: 2px solid #26ae61;
        border: 2px solid #26ae61;
    }

    .contact-edit>form button {
        border: 2px solid #26ae61;
    }

    .profile-form-edit>form button:hover,
    .contact-edit>form button:hover {
        background: #26ae61;
        color: #ffffff;
    }

    .step.active i {
        background: #26ae61;
        border-color: #26ae61;
        color: #ffffff;
    }

    .step i {
        color: #26ae61;
    }

    .step.active span {
        color: #000;
    }

    .menu-sec nav>ul>li.menu-item-has-children>a::before {
        display: none;
    }

    .inner-header {
        float: left;
        width: 100%;
        position: relative;
        padding-top: 60px;
        padding-bottom: 15px;
        z-index: 0;
    }

    .manage-jobs-sec>table thead tr td {
        color: #26ae61;
    }

    .extra-job-info>span i {
        float: left;
        font-size: 30px;
        color: #26ae61;
        width: 30px;
        margin-right: 12px;
    }

    .action_job>li span {
        background: #26ae61;
    }

    .action_job>li span::before {
        background: #26ae61;
    }

    .action_job>li {
        float: left;
        margin: 0;
        position: relative;
        width: 15px;
    }

    .manage-jobs-sec>h3 {
        padding-left: 0px;
        margin-top: 40px;
        text-align: center;
    }

    .fall {
        float: left;
        width: 100%;
        border: 1px solid#ddd;
        padding: 23px 10px;
    }

    .progress-bar {
        background-color: #47b476;
    }

    .details {
        float: left;
        width: 100%;
        padding: 5px 17px;
        background-color: #47b476;
        color: #fff;
        margin: 15px;
        border-radius: 20px
    }

    .leftdetails {
        float: left;
        width: 100%;
    }

    .left {
        float: left;
        width: 100%;
        margin: 0px;
        padding: 0px;
    }

    .detail span {
        display: block;
        float: right;
        margin: 12px 8px;
        border: 1px solid#ddd;
        padding: 10px;
    }

    .detail strong {
        float: left;
        width: 100%;
        margin: 0px;
        padding: 0px 55px;
        font-weight: 700;
        font-size: 20px;
    }

    .detail,
    .detai {
        float: left;
        width: 100%;
    }

    .detai span {
        display: -webkit-box;
        width: 100%;
        font-weight: 600;
        margin: 0px;
        padding: 12px 0px;
        font-size: 20px;
    }

    .progras {
        float: left;
        width: 100%;
        border: 1px solid#ddd;
        padding: 10px;
    }

    .detai strong {
        float: right;
        font-size: 20px;
    }

    .detail p,
    .detai p {
        font-weight: 600;
        font-size: 16px;
        color: #000;
    }

    .detail .progress span {
        position: absolute;
        right: 0px;
        font-size: 13px;
        color: #fff;
        border: 0px solid#ddd;
    }

    .head {
        float: left;
        width: 100%;
        padding: 10px;
        background-color: #47b476;
        color: #fff;
        margin-top: 10px;
        margin-bottom: 10px;
    }

    .num {
        float: left;
        margin: 20px 0px;
    }

    .para {
        float: left;
        width: 100%;
        border: 1px solid#dddd;
        padding: 10px;
        background-color: #47b476;
        color: #fff;
        margin-top: 10px;
        margin-bottom: 10px;
    }

    .tech {
        float: left;
        width: 100%;
        border: 1px solid#ddd;
        padding: 10px;
        margin: 10px 0px;
    }

    .search-container {
        float: left;
    }

    .search button {
        padding: 7px 4px;
        color: #fff;
        background-color: #47b476;
        border: none;
        width: 36%;
    }

    .upload a {
        color: #ffffff;
        text-decoration: none;
        background: #47b476;
        padding: 10px;
        border-radius: 50px;
    }

    .search input[type="text"] {
        padding: 9px 6px;
        width: 63%;
        background-color: #fff;
        border: 1px solid#ddd;
    }

    .details i.fa.fa-filter {
        float: right;
        margin: 5px 0px;
        padding: 0px;
    }

    .details p {
        float: left;
        margin: 0px;
        padding: 0px;
        color: #fff;
    }

    .head p {
        color: #fff;
    }

    .para p {
        color: #fff;
    }

    .upload {
        float: right;
        padding: 8px;
        margin: px;
    }

    border: 1px solid#ddd;
    padding: 10px;
    }

    .dot {
        float: right;
    }

    .progr {
        float: left;
        width: 100%;
        margin-bottom: 18px;
    }

    .progr .progress {
        position: absolute;
        left: 9%;
        height: 20px;
        -webkit-border-radius: 8px;
        -moz-border-radius: 8px;
        -ms-border-radius: 8px;
        -o-border-radius: 8px;
        border-radius: 8px;
        top: initial;
        width: 80%;
    }

    .progr .progress-label {
        margin: 12px 0px;
    }

    .progress-bar.progress-bar-primary {
        padding: 0px;
    }

    .texts p {
        position: absolute;
        top: 70%;
        left: 29%;
    }

    p.dropdown-toggle {
        font-size: 34px;
        margin: -30px;
    }

    .dot {
        float: right;
    }

    .texts {
        margin-top: 20px;
    }

    .text p {
        margin: 0px;
        padding: 0px;
        font-size: 13px;
    }

    .right .progress {
        width: 150px;
        height: 150px;
        line-height: 150px;
        background: none;
        margin: 0 auto;
        box-shadow: none;
        position: relative;
    }

    .right .progress:after {
        content: "";
        width: 100%;
        height: 100%;
        border-radius: 50%;
        border: 2px solid #fff;
        position: absolute;
        top: 0;
        left: 0;
    }

    .right .progress>span {
        width: 50%;
        height: 100%;
        overflow: hidden;
        position: absolute;
        top: 0;
        z-index: 1;
    }

    .progress .progress-left {
        left: 0;
    }

    .right .progress .progress-bar {
        width: 100%;
        height: 100%;
        background: none;
        border-width: 2px;
        border-style: solid;
        position: absolute;
        top: 0;
    }

    .right .progress .progress-left .progress-bar {
        left: 100%;
        border-top-right-radius: 80px;
        border-bottom-right-radius: 80px;
        border-left: 0;
        -webkit-transform-origin: center left;
        transform-origin: center left;
    }

    .right .progress .progress-right {
        right: 0;
    }

    .right .progress .progress-right .progress-bar {
        left: -100%;
        border-top-left-radius: 80px;
        border-bottom-left-radius: 80px;
        border-right: 0;
        -webkit-transform-origin: center right;
        transform-origin: center right;
        animation: loading-1 1.8s linear forwards;
    }

    .right .progress .progress-value {
        width: 59%;
        height: 59%;
        border-radius: 50%;
        border: 2px solid #ebebeb;
        font-size: 28px;
        line-height: 82px;
        text-align: center;
        position: absolute;
        top: 7.5%;
        left: 7.5%;
    }

    .right .progress.blue .progress-bar {
        border-color: #049dff;
    }

    .right .progress.blue .progress-value {
        color: #47b476;
    }

    .right .progress.blue .progress-left .progress-bar {
        animation: loading-2 1.5s linear forwards 1.8s;
    }

    .progress.yellow .progress-bar {
        border-color: #fdba04;
    }

    .right .progress.yellow .progress-value {
        color: #fdba04;
    }

    .right .progress.yellow .progress-left .progress-bar {
        animation: loading-3 1s linear forwards 1.8s;
    }

    .right .progress.pink .progress-bar {
        border-color: #ed687c;
    }

    .right .progress.pink .progress-value {
        color: #ed687c;
    }

    .right .progress.pink .progress-left .progress-bar {
        animation: loading-4 0.4s linear forwards 1.8s;
    }

    .right .progress.green .progress-bar {
        border-color: #1abc9c;
    }

    .right .progress.green .progress-value {
        color: #1abc9c;
    }

    .right .progress.green .progress-left .progress-bar {
        animation: loading-5 1.2s linear forwards 1.8s;
    }

    /*    new css code start*/
    .tableList {
        padding: 15px;
        background: #f5f4f3;
    }

    .tableList .White {
        background: #fff !important;
        border: none;
        width: 50%;
    }
    .tableList .table{
        margin: 0;
    }
.tableList .table th, .tableList .table td {

    border-top: 0;
}
    .tableList .red {
        background: #de0e0e !important;
        border: none;
        color: #fff;
        font-weight: 600;
        width: 25%;
    }

    .tableList .green {
        background: #00c671 !important;
        border: none;
        color: #fff;
        font-weight: 600;
        width: 25%;
    }

    .trnasparent {
         background: transparent;
    color: #000 !important;
    font-weight: 700;
    text-align: left !important;
    padding-left: 35px !important;
    }

    .rednew {
        background: #f8e4e5;
        color: #de0e0e !important;
        font-weight: 600;
    }

    .greennew {
        background: #dbf5ea;
        color: #00c671 !important;
        font-weight: 600;
    }

    /*    new css code end*/

    @keyframes loading-1 {
        0% {
            -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(180deg);
            transform: rotate(180deg);
        }
    }

    @keyframes loading-2 {
        0% {
            -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(144deg);
            transform: rotate(144deg);
        }
    }

    @keyframes loading-3 {
        0% {
            -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(90deg);
            transform: rotate(90deg);
        }
    }

    @keyframes loading-4 {
        0% {
            -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(36deg);
            transform: rotate(36deg);
        }
    }

    @keyframes loading-5 {
        0% {
            -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(126deg);
            transform: rotate(126deg);
        }
    }

    @media only screen and (max-width: 990px) {
        .right .progress {
            margin-bottom: 20px;
        }

        .deletes {
            position: absolute;
            top: 7%;
            left: 9% !important;
            background: #47b476;
            padding: 1px 5px;
            border-radius: 10px;
            color: #fff;
        }
    }

    .deletes {
        position: absolute;
        top: 5%;
        left: 13%;
        background: #47b476;
        padding: 1px 5px;
        border-radius: 10px;
        color: #fff;
    }

    .editss {
        position: absolute;
        top: 5%;
        left: 78%;
        background: #47b476;
        padding: 1px 5px;
        border-radius: 10px;
        color: #fff;
    }


    .Delete {
        background-color: #f6ab27;
        width: 25px;
        height: 25px;
        left: auto;
        right: 2px;
        top: 45px;
        border-radius: 50%;
        text-align: center;
        line-height: 23px;
    }

    .Delete a {
        display: block;
        font-size: 12px;
    }

    .Edit {
        position: absolute;
        top: 10px;
        left: auto;
        right: 2px;
        background-color: transparent;
        padding: 0;
        border-radius: 0;
    }

    .Edit a {
        width: 25px;
        height: 25px;
        background-color: #47b476;
        display: block;
        text-align: center;
        font-size: 12px;
        line-height: 25px;
        border-radius: 50%;
    }

    .Delete:hover,
    .Edit a:hover {
        background-color: #224b89
    }

    div#msgModal i.fa.fa-check {
        padding: 29px 27px;
        background: #26ae61;
        color: #fff;
        border-radius: 100%;
        font-size: 30px;
    }

    div#msgModal {
        text-align: center;
    }

    .lightColor {
        color: #5d8e34 !important;
    }

    .redColor {
        color: #c71434 !important;
    }
    .subscriptionHead{
        display: flex;
        justify-content: space-between;
/*        align-items: center;*/
        padding-bottom: 10px;
         padding-top: 10px;
        border-bottom: 1px solid #00c671;
    }
    .subbLeft h2{
        font-weight: 600;
        color: #000;
        font-size: 18px;
        margin-bottom: 0;
    }
    .green1{
        color: #00c671;
    }
    .subbRight ul li{
        display: inline-block;
        margin-right: 8px;
    }
    .subbRight ul li:last-child{
        margin-right: 0;
    }
    .subbRight ul li a{
        font-size: 16px;
    font-weight: 600;
    padding: 6px 15px;
    border-radius: 6px;
    }
    .subscriptionTime{
        display: flex;
        justify-content: space-between;
        padding: 15px 0px;
    }
    .subscriptionTime ul li{
        display: inline-block;
        margin-right: 8px;
        color: #000;
        font-weight: 600;
        margin-bottom: 0;
    }
    .subscriptionTime ul li:last-child{
        margin-right: 0;
    }
    .subImgs{
    display: inline-block;
    width: 50px;
    position: relative;
    top: -5px;
    margin-right: 10px;
    }
</style>

<body>
    <div class="theme-layout" id="scrollup">
        <header class="stick-top nohdascr">
            <div class="menu-sec">
                <div class="container-fluid dasboardareas">

                    <!-- Logo -->
                    <?php include_once('headermenu.php');?>
                    <!-- Menus -->
                </div>
            </div>
        </header>
        <section>
            <div class="block no-padding">
                <div class="container-fluid dasboardareas">
                    <div class="row">
                        <aside class="col-md-3 col-lg-2 column border-right Sidenavbar">
                            <div class="widget">
                                <div class="menuinside">
                                    <?php include_once('sidebar.php'); ?>
                                </div>
                            </div>
                        </aside>
                        <div class="col-md-9 col-lg-10 psthbs cmpprofd MainWrapper">
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="cdfts">

                                        <div class="subscriptionHead">
                                            <div class="subbLeft">
                                                <h2><span class="subImgs"><img src="<?php echo base_url(); ?>webfiles/newone/images/<?php echo $userplan["plan_image"]; ?>" class="img-fluid" alt="golden"></span> <?php if(count($userplan) > 0) { echo $userplan["plan_name"]; } ?> Plan <span color ="green1"> ₱ <?php if(count($userplan) > 0) { echo $userplan["plan_cost"]; } ?></span></h2>
                                            </div>
                                            <div class="subbRight">
                                                <ul>
                                                <?php if(count($userplan) > 0) { ?>
                                                
                                                    <li><a href="<?php echo base_url();?>recruiter/subscription_history" style = "background:#feab28; color:#45729c">Subscription Plan History</a></li>

                                                    <li><a href="<?php echo base_url();?>recruiter/change_subscription" style ="background: #d1e9c9;color: #4aa323;">Renew</a></li>
                                                    <li><a href="<?php echo base_url();?>recruiter/change_subscription" style = "background:#c1d3e1; color:#45729c">Upgrade Subscription Plan</a></li>
                                                
                                                <?php } else { ?>

                                                    <li><a href="<?php echo base_url();?>recruiter/subscription_history" style = "background:#feab28; color:#45729c">Subscription Plan History</a></li>
                                                    
                                                    <li><a href="<?php echo base_url();?>recruiter/change_subscription" style ="background: #d1e9c9;color: #4aa323;">Subscription Plan <i class="fas fa-pen"></i></a></li>

                                                <?php } ?>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="subscriptionTime">
                                            <ul><li>Duration : <span class="green1"><?php if(count($userplan) > 0) { echo $userplan["duration"]; } ?> months</span></li></ul>
                                            <ul> 
                                                <li>Start Date : <span class="green1"><?php if(count($userplan) > 0) { echo $userplan["start_date"]; } ?></span></li>
                                                <li>End Date : <span class="green1"><?php if(count($userplan) > 0) { echo $userplan["end_date"]; } ?></span></li>
                                            </ul>
                                        </div>

                                        <div class="companydtls">

                                            <div class="newfiletrsgree">

                                            <?php
                                                if(count($userplan) > 0) {
                                            ?>
                                                <div class="tableList">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th class="White"></th>
                                                                <th class="green">Used</th>
                                                                <th class="red">Remaining</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td class="trnasparent">Number Of Mass Mails</td>
                                                                <td class="greennew"><?php echo $userplan["total_mail"]; ?></td>
                                                                <td class="rednew"><?php echo $userplan["remaining_mail"]; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="trnasparent">Number Of Jobs Posting</td>
                                                                <td class="greennew"><?php echo $userplan["total_job_post"]; ?></td>
                                                                <td class="rednew"><?php echo $userplan["remaining_job_post"]; ?></td>
                                                            </tr>
                                                       
                                                            <tr>
                                                                <td class="trnasparent">Number Of Resume Accessiblity</td>
                                                                <td class="greennew"><?php echo $userplan["total_resume_access"]; ?></td>
                                                                <td class="rednew"><?php echo $userplan["remaining_resume_access"]; ?></td>
                                                            </tr>
                                                                 <tr>
                                                                <td class="trnasparent">Video Posting</td>
                                                                <td class="greennew"><?php echo $userplan["total_video_post"]; ?></td>
                                                                <td class="rednew"><?php echo $userplan["remaining_video_post"]; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="trnasparent">Advertisement Posting</td>
                                                                <td class="greennew"><?php echo $userplan["total_advertise"]; ?></td>
                                                                <td class="rednew"><?php echo $userplan["remaining_advertise"]; ?></td>
                                                            </tr>
                                                                  <tr>
                                                                <td class="trnasparent">Job Boosting</td>
                                                                <td class="greennew"><?php echo $userplan["total_job_boost"]; ?></td>
                                                                <td class="rednew"><?php echo $userplan["remaining_job_boost"]; ?></td>
                                                            </tr>
                                                            </tr>
                                                                  <tr>
                                                                <td class="trnasparent">Candidate Profile</td>
                                                                <td class="greennew"><?php echo $userplan["total_candidate_profile"]; ?></td>
                                                                <td class="rednew"><?php echo $userplan["remaining_candidate_profile"]; ?></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>

                                            <?php
                                                } else {
                                            ?>
                                                <div class="main-hda right">
                                                    <h3>Please activate plan for use services </h3>
                                                </div>
                                            <?php
                                                }
                                            ?>
                                            </div>



                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </section>

    
    <?php include_once("footer.php");?>
    <?php include_once("modalpassword.php");?>
    <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.min.js"></script>
    <script src="<?php echo base_url().'recruiterfiles/';?>js/popper.min.js"></script>
    <script src="<?php echo base_url().'recruiterfiles/';?>js/bootstrap.js"></script>
    <script src="<?php echo base_url().'recruiterfiles/';?>js/recruiteryoda.js"></script>

    <?php include_once("searchmodalscript.php");?>

    <script type="text/javascript">
        if (window.history.replaceState) {
            window.history.replaceState(null, null, window.location.href);
        }
    </script>
</body>

</html>
