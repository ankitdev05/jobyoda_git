<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>JobYoDA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="CreativeLayers">
    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap-grid.css" />
    <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/icons.css">
    <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/all.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/fontawesome.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/responsive.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/colors/colors.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/style.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/croppie.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
</head>

<style type="text/css">
    .question {
        background: #fff;
        padding: 35px;
        box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
        margin: 60px 0px;
    }

    .questHead h2 {
        font-size: 20px;
        color: #000;
        font-weight: 600;
        margin: 0;
        padding-bottom: 10px;
        border-bottom: 2px solid #00a94f;
        margin-bottom: 10px;
    }

    .questionAll {
        background: #efefef;
        padding: 10px 15px;
        margin-bottom: 20px;
    }

    .questionAll label {
        font-size: 13px;
        color: #000;
        font-weight: 600;
        padding: 0;
        margin-bottom: 30px;
        width: 0px !important;
        height: 0px !important;
        float: left;
        width: 100% !important;
        float: left;
        display: block;
    }

    .questionAll label::before {
        content: '';
        position: inherit;
        border: none;
        width: 0px !important;
        height: 0px !important;
    }

    .questionAll h2,
    .questionAll textarea.form-control {
        margin: 0;
        padding: 10px;
        background: #fff;
        font-size: 13px;
        line-height: 20px;
        color: #000;
        font-weight: 600;
        width: 100%;
        float: left;
        display: block;
    }
    .form-control{
        font-size: 13px;
    }
    .questionAll textarea.form-control {
       min-height: auto;
    border: none;
    height: 36px;
    padding: 6px 10px;
    }
    .form-group {
        margin-bottom: 0.5rem!important;
    }
    .allText .form-control.fornsControl {
        height: 36px;
        min-height: auto !important;
        width: 250px;
        margin-right: 15px;
/*        margin-bottom: 15px;*/
    }

    .allText .form-control.fornsControl:last-child {
        /*        margin-right: 0*/
    }

    .questionAll textarea.form-control:focus {
        border: none;
        box-shadow: none;
        outline: none;
    }

    .boxs {
        width: 100%;
        float: left;
        display: block;
    }

    .allOptions li {
        font-size: 16px;
        background: #fff;
        padding: 13px 35px 13px 15px;
        border-radius: 4px;
        font-weight: 600;
        display: inline-block;
        cursor: pointer;
        margin-right: 15px;
        float: left;
    }

    .allOptions li:last-child {
        margin-right: 0;
    }

    .form-control.fornsControl {
        background: #fff;
        color: #000;
        font-size: 14px;
        font-weight: 500;
        padding: 12px;
        width: 250px;
        border-radius: 5px;
        margin: 0;
    }

    .form-control.fornsControl::placeholder {
        color: #ccc;
    }

    .form-control.fornsControl:focus {
        outline: none;
        border: none;
        box-shadow: none;
    }

    .paddzero {
        padding: 0 !important;
    }

    .allOptions li.btnsAdd {
        padding: 0;
    }

    .btnsAdd button {
        font-size: 14px;
        color: #fff;
        font-weight: 600;
        background: #084d87;
        padding: 14px 20px;
        border-radius: 5px;

    }

    .addQue button {
    float: right;
    padding: 10px 20px;
    color: #fff;
    background: #fbaf31;
    font-weight: 600;
    font-size: 14px;
}

    .btnsAdd button {
        float: left;
    }
    select.form-control:not([size]):not([multiple]) {
    padding: 6px 10px;
    height: 36px;
}
</style>

<body>
    <div class="theme-layout" id="scrollup">
        <header class="stick-top nohdascr">
            <div class="menu-sec">
                <div class="container-fluid dasboardareas">
                    <?php include_once('headermenu.php');?>
                </div>
            </div>
        </header>

        <section>
            <div class="block no-padding">
                <div class="container-fluid dasboardareas">
                    <div class="row">
                        <aside class="col-md-3 col-lg-2 column border-right Sidenavbar">
                            <div class="widget">
                                <div class="menuinside">
                                    <?php include_once('sidebar.php'); ?>
                                </div>
                            </div>
                        </aside>
                        <div class="col-md-9 col-lg-10 recruprogfd MainWrapper">
                            <div class="question boxs">

                                <div class="headsteps-gt questHead">
                                    <h5>Questionnaire</h5>
                                    <img src="<?php echo base_url();?>recruiterfiles/images/fav.png">
                                </div>

                                <div>
                                    <p style="">Great news recruiters!! You can now initiate automated chats and ask relevant questions which you would have otherwise asked during Round1 of the interview. A maximum of 10 questions per job may be asked with multiple choice answers or allowing jobseeker to enter free flow text. </p>

                                </div>
                                
                                <form action="<?php echo base_url();?>recruiter/questionnaire_save" method="post">
                                
                                    <div class="Addquestions">
                                        <div class="questionAll boxs">
                                            
                                            <div class="form-group boxs">
                                                <label>Question Type</label>
                                                <select class="form-control questiontype" name="type[]" onchange="changetype(1, this.value)"> 
                                                    <option value=""> Select Question Type </option>
                                                    <option value="option"> Multiple Choice </option>
                                                    <option value="text"> Free Flow </option>
                                                </select>
                                            </div>

                                            <div class="form-group boxs questionbox1" style="display: none;">
                                                <label>Type Question</label>
                                                <textarea class="form-control" name="question[]" placeholder="type here.."></textarea>
                                            </div>
                                            
                                            <div class="form-group boxs optionbox1" style="display: none;">
                                                <label>Type Multiple Answer</label>
                                                <div class="allOptions boxs">
                                                    <div class="allText ">
                                                        <textarea type="text" name="options1[]" class="form-control fornsControl"  placeholder="type here.."></textarea>
                                                        <textarea type="text" name="options1[]" class="form-control fornsControl" placeholder="Type here..."></textarea>
                                                    </div>
                                                    <div class="btnsAdd">
                                                        <button type="button" class="addOpt">Add More Options</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group boxs answer-box answertype1" style="display: none;">
                                                <label>Answer</label>
                                                <div class="allOptions boxs">
                                                    <div class="allText ">
                                                        <textarea type="text" name="answer[]" class="form-control fornsControl" placeholder="Type here..."></textarea>
                                                    </div>
                                                    <!-- <div class="btnsAdd">
                                                        <button type="button" class="addTxt">Add More Answer</button>
                                                    </div> -->
                                                </div>
                                            </div>
                                            <div class="form-group boxs answer-box qualifier1" style="display: none;">
                                                <label>Is this a qualifier question?</label>
                                                <div class="allOptions boxs">
                                                    <div class="allText ">
                                                        <label class="switch" style="width:66px!important;">
                                                            <input type="checkbox" value="1" name="haverequired[]" >
                                                            <span class="slider" title="If turned ON, applicants who are not meeting these requirements will be automatically rejected."></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="addQue boxs">
                                        <button type="submit" class="addQestt" style="margin-left: 20px;">Save Questions</button>
                                        <button type="button" class="addQest">Add More Question</button>

                                    </div>
                                    <input type="hidden" name="jobid" value="<?php echo $jobid;?>">
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php include_once("footer.php");?>
        <?php include_once("modalpassword.php");?>
        <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url().'recruiterfiles/';?>js/modernizr.js" type="text/javascript"></script>
        <script src="<?php echo base_url().'recruiterfiles/';?>js/script.js" type="text/javascript"></script>
        <script src="<?php echo base_url().'recruiterfiles/';?>js/wow.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url().'recruiterfiles/';?>js/slick.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url().'recruiterfiles/';?>js/parallax.js" type="text/javascript"></script>
        <script src="<?php echo base_url().'recruiterfiles/';?>js/select-chosen.js" type="text/javascript"></script>
        <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.scrollbar.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url().'recruiterfiles/';?>js/popper.min.js"></script>
        <script src="<?php echo base_url().'recruiterfiles/';?>js/bootstrap.js"></script>
        <script src="<?php echo base_url().'recruiterfiles/';?>js/recruiteryoda.js"></script>
        <script type="text/javascript" src="<?php echo base_url().'recruiterfiles/';?>js/croppie.js"></script>
        <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

        <?php include_once("searchmodalscript.php");?>

        <script>
            $(document).on('click', '.addOpt', function() {
                var getindex = $(this).parent('.btnsAdd').parent().parent().parent().index();
                getindex = getindex + 1;
                $(this).parent('.btnsAdd').siblings('.allText').append('<textarea type="text" name="options'+getindex+'[]" class="form-control fornsControl" placeholder="Type here..."></textarea>');
            });
            $(document).on('click', '.addTxt', function() {
                var getindex = $(this).parent('.btnsAdd').parent().parent().parent().index();
                getindex = getindex + 1;
                $(this).parent('.btnsAdd').siblings('.allText').append('<textarea type="text" name="answer'+getindex+'[]" class="form-control fornsControl" placeholder="Type here..."></textarea>');
            });

        </script>

        <script>
            $(document).on('click', '.addQest', function() {
                var numItems = $('.questionAll').length;
                numItems = numItems + 1;
                if(numItems <= 10) {
                    
                    $('.Addquestions').append('<div class="questionAll boxs"><div class="form-group boxs"><label>Question Type</label><select class="form-control questiontype" onchange="changetype('+numItems+', this.value)" name="type[]"> <option value=""> Select Question Type </option><option value="option"> Multiple Choice Answer </option><option value="text"> Free Flow Answer </option></select></div><div class="form-group boxs questionbox'+numItems+'" style="display:none;"><label> Type Question </label><textarea class="form-control" name="question[]" placeholder="type here.."></textarea></div><div class="form-group boxs optionbox'+numItems+'" style="display:none;"><label>Type Multiple Answer</label><div class="allOptions boxs"><div class="allText "><textarea type="text" name="options'+numItems+'[]" class="form-control fornsControl" placeholder="Type here..."></textarea></div><div class="btnsAdd"><button type="button" class="addOpt">Add More Options</button></div></div></div><div class="form-group boxs answertype'+numItems+'" style="display:none;"><label>Answer</label><div class="allOptions boxs"><div class="allText "><textarea type="text" name="answer[]" class="form-control fornsControl" placeholder="Type here..."></textarea></div></div> <div class="form-group boxs answer-box qualifier'+numItems+'" style="display:none;"><label>Is this a qualifier question?</label><div class="allOptions boxs"><div class="allText "><label class="switch" style="width:66px!important;"><input type="checkbox" value="1" name="haverequired[]" ><span class="slider" title="If turned ON, applicants who are not meeting these requirements will be automatically rejected."></span></label></div></div></div> </div></div>');
                } else {
                    alert("You can not add more than 10 questions");
                }
            });

        </script>
        <script type="text/javascript">
            function changetype(index1, val1) {

                if(val1 == "option") {
                  $('.questionbox'+index1).css('display','block');
                  $('.optionbox'+index1).css('display','block');
                  $('.answertype'+index1).css('display','block');
                  $('.qualifier'+index1).css('display','block');
                } else if(val1 == "text") {
                    $('.questionbox'+index1).css('display','block');
                  $('.optionbox'+index1).css('display','none');
                  $('.answertype'+index1).css('display','none');
                  $('.qualifier'+index1).css('display','none');
                }

            }
        </script>
</body>

</html>
