<style>
    .modal-backdrop{
    z-index:1 !important;
    }

</style>

<?php
  $userData = $this->session->userdata('userSession');
  if(!empty($userData['label']) && $userData['label']=='3'){
     $subrecruiter_id = $userData['id'];
     $userSession['id'] = $userData['parent_id'];
  }else{
     $userSession['id'] = $userData['id'];
     $subrecruiter_id = 0;
  }
  $recruiterDatas = $this->Recruit_Model->recruiter_single($userData['id']);
  if($recruiterDatas[0]['email']  !== $userData['email']) {
      $this->session->unset_userdata('userSession');
      $this->session->unset_userdata('userSession11');
  }

  $recruiterDatas11 = $this->Recruit_Model->company_details_single($userData['id']);


  $userSession = $this->session->userdata('userSession');
  if (!empty($userSession['label']) && $userSession['label'] == '3') {
      $userplan_id = $userSession['parent_id'];
  } else {
      $userplan_id = $userSession['id'];
  }
  $getSubscription = $this->Subscription_Model->view_existing($userplan_id);
  $planArrays = [];
  if($getSubscription) {
      $getSubscription = $getSubscription[0];
      $userData11 = $this->Subscription_Model->view($getSubscription["plan_id"]);
      $datauserData = $userData11[0];
      $planArrays = [
          "plan_name" => $datauserData["plan_name"],
          "duration" => $getSubscription["duration"],
          "start_date" => $getSubscription["start_date"],
          "end_date" => $getSubscription["expiry_date"],
          "plan_cost" => $getSubscription["plan_cost"],
          "total_mail" => $getSubscription["total_mail"],
          "remaining_mail" => $getSubscription["remaining_mail"],
          "total_job_post" => $getSubscription["total_job_post"],
          "remaining_job_post" => $getSubscription["remaining_job_post"],
          "total_resume_access" => $getSubscription["total_resume_access"],
          "remaining_resume_access" => $getSubscription["remaining_resume_access"],
          "total_video_post" => $getSubscription["total_video_post"],
          "remaining_video_post" => $getSubscription["remaining_video_post"],
          "total_advertise" => $getSubscription["total_advertise"],
          "remaining_advertise" => $getSubscription["remaning_advertise"],
          "total_job_boost" => $getSubscription["total_job_boost"],
          "remaining_job_boost" => $getSubscription["remaining_job_boost"],
          "total_candidate_profile" => $getSubscription["total_candidate_profile"],
          "remaining_candidate_profile" => $getSubscription["remaining_candidate_profile"],
          "plan_image" => $datauserData["plan_image"],
      ];
      $this->session->set_userdata('userSubscriptionSession', $planArrays);
  } else {
      $this->session->set_userdata('userSubscriptionSession', []);
  }
?>

<div class="Logo">
  <a href="<?php echo base_url();?>recruiter/dashboard">
    <img src="<?php echo base_url().'recruiterfiles/';?>images/jobyoDa.png">
  </a>
</div>
 

  <div class="ProfileArea">
  <div class="Jobyodadata">
  <?php
    $userData11 = $this->session->userdata('userSession11');
  ?>
    <ul>
      <li data-number="<?php echo $userData11['onedata']; ?>">
        <h4>Employment Opportunities</h4>
        <span id="number1"> <?php echo $userData11['onedata']; ?> </span>
        
      </li>
      <li data-number="<?php echo $userData11['twodata']; ?>">
        <h4> Jobseekers</h4>
        <span id="number2"><?php echo $userData11['twodata']; ?></span>
      </li>

      <li>
      <?php
        if($userSession['id'] == 59300000000000000000000000000000000) {
      ?>
          <h4>Search</h4>
          <form action="<?php echo base_url();?>recruiter/searchCandidates" method="post">
            <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
            <div class="searchbarnav">
              <input type="text" placeholder="Search candidates by Job Title" name="keyword" required>
            </div>
          </form>
      <?php
        }
      ?>
      </li>

    </ul>

    <ol>
      
      <li>
         <a href="javascript:void(0)" data-toggle="modal" data-target="#searchingModal">
          <img src="<?php echo base_url().'recruiterfiles/';?>images/recprofile.png">
          Search
        </a> 
      </li>

      <li>
        <a href="<?php echo base_url();?>recruiter/recruiterprofile">
          <img src="<?php echo base_url().'recruiterfiles/';?>images/recprofile.png">
          Recruiter Profile
        </a>
      </li>
      <li>
        <a href="<?php echo base_url();?>recruiter/dashboard/logout">
          <img src="<?php echo base_url().'recruiterfiles/';?>images/logout.png">
          Logout
        </a>
      </li>
    </ol>
  </div>
  
  <div class="FacebookLink">
    <a href="https://www.facebook.com/james.deakin/videos/2645412085501619/">
      <i class="fa fa-facebook"></i>
      <span> James Deakin  </span>
    </a>
  </div>

  <div class="Profilebox">
    <?php
      if($recruiterDatas11) {
        if(strlen($recruiterDatas11[0]['companyPic']) > 0) {
    ?>
          <figure><img src="<?php echo $recruiterDatas11[0]['companyPic']; ?>"></figure>
    
    <?php } else { ?>
    
          <figure><img src="<?php echo base_url().'recruiterfiles/';?>images/placeholder.png"></figure>
    
    <?php } } else {
?>
          <figure><img src="<?php echo base_url().'recruiterfiles/';?>images/placeholder.png"></figure>
<?php
    } ?>
    
    <h1><?php echo $userData['fname'] .' '.$userData['lname']; ?> <BR/>  <!--<span>( <?php //if(count($planArrays) >0) { echo $planArrays['plan_name'];} ?> Plan ) </span>--> </h1> 
  </div>
</div>

  <!-- <div class="FacebookLink">
  <a target="_blank" href="https://www.facebook.com/james.deakin/videos/2645412085501619/">
    <i class="fa fa-facebook"></i>
    <span>James Deakin</span>
  </a>
</div> -->
                   
				  
<!-- <div class="mebilemenuham">
   <i class="fas fa-bars"></i>
   <i class="fas fa-times"></i>
</div>
<div class="btn-extars">
   <a href="#" title="" class="post-job-btn"><i class="la la-plus"></i>Post Jobs</a>
   <ul class="account-btns">
      <li class="signup-popup"><a title=""><i class="la la-key"></i> Sign Up</a></li>
      <li class="signin-popup"><a title=""><i class="la la-external-link-square"></i> Login</a></li>
   </ul>
</div> -->
<!-- Btn Extras -->
<!-- <nav>
  <ul>
    <li class="menu-item-has-children">
        <a href="<?php echo base_url();?>recruiter/recruiter/companyprofile" title="">Company Profile</a>
    </li>
    <li class="menu-item-has-children">
        <a href="<?php echo base_url();?>recruiter/dashboard" title="">Recruiter Dashboard</a>
    </li>
    <li class="menu-item-has-children">
        <a href="<?php echo base_url();?>recruiter/jobpost" title="">Post a job</a>
    </li>
    <li>
      <form action="<?php echo base_url();?>recruiter/searchCandidates" method="post">
        <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
        <div class="searchbarnav">
          <input type="text" placeholder="Search candidates by Job Title" name="keyword" required>
        </div>
      </form>
    </li>
  </ul>
</nav> -->

<!-- Modal -->
<div class="modal fade modalNew modalNew3" id="searchingModal" tabindex="-1" role="dialog" aria-labelledby="searchingModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width: 80%!important;">
    <div class="modal-content">

      <form action="<?php echo base_url();?>recruiter/searchCandidates" method="get">

      <div class="modal-body">

        <div class="modalMail modalMail3">

          <button type="button" class="close closed" data-dismiss="modal">&times;</button>
           <div class="searchHead">
              <h2>Search Resume</h2>
           </div>

           <div class="searchContent">
              <div class="row">
                <div class="col-md-4">
                  <input type="text" name="location" class="formcontrol" id="txtPlacesssss" placeholder="Location">
                </div>
                <div class="col-md-4">
                  <select name="experience" class="formcontrol">

                    <option value=""> Total Experience </option>
                    <?php for($i=0;$i<=50;$i++) { ?>
                       <option value="<?php echo $i;?>"> <?php echo $i;?> </option>
                    <?php }?>

                  </select>
                </div>
                <div class="col-md-4">
                  <input type="text" name="salary" placeholder="Salary Package" class="formcontrol">
                </div>
              </div>

              <div class="row">
                <div class="col-md-4">
                  <input type="text" name="excluding" placeholder="Excluding keywords" class="formcontrol">
                </div>
                <div class="col-md-4">
                  <select name="active" class="formcontrol">
                      <option value=""> Active </option>
                      <option value="week"> Week </option>
                      <option value="day"> Day </option>
                      <option value="3day"> 3 Days </option>
                  </select>
                </div>
                <div class="col-md-4">
                  <?php
                  $getLevel = $this->Recruiteradmin_Model->joblevel_listing();
                  ?>
                  <select name="joblevel" class="formcontrol">
                      <option value=""> Job Level </option>
                    <?php
                    foreach($getLevel as $getLev) {
                    ?>
                      <option value="<?php echo $getLev['id']; ?>"> <?php echo $getLev['level']; ?> </option>
                    <?php
                    }
                    ?>
                  </select>
                </div>
              </div>

              <div class="row">
                <div class="col-md-4">
                  <?php
                  $getCategory = $this->Jobpost_Model->category_lists();
                  ?>
                  <select name="categories" class="formcontrol" id="scategory">
                      <option value=""> Job Categories </option>
                    <?php
                    foreach($getCategory as $getCat) {
                    ?>
                      <option value="<?php echo $getCat['id']; ?>"> <?php echo $getCat['category']; ?> </option>
                    <?php
                    }
                    ?>
                  </select>
                </div>
                <div class="col-md-4">
                  <select name="subcategory" class="formcontrol" id="ssubcategory">
                      <option value=""> Job Sub-Categories </option>
                  </select>
                </div>
                <div class="col-md-4">
                  <select name="radius" class="formcontrol">
                      <option value=""> Radius </option>
                      <option value="1"> 1 </option>
                      <option value="2"> 2 </option>
                      <option value="3"> 3 </option>
                      <option value="4"> 4 </option>
                      <option value="5"> 5 </option>
                      <option value="6"> 6 </option>
                      <option value="7"> 7 </option>
                      <option value="8"> 8 </option>
                      <option value="9"> 9 </option>
                      <option value="10"> 10 </option>
                  </select>
                </div>
              </div>

            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-secondary">Search</button>
      </div>

      </form>

    </div>
  </div>
</div>