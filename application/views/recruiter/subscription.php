<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>JobYoDA - Recruiter</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="keywords" content="">
      <meta name="author" content="CreativeLayers">

      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=G-R64BFR96SR"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-R64BFR96SR');
      </script>
      <!-- Global site tag (gtag.js) - Google Ads: 851948051 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-851948051"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'AW-851948051');
    </script>
    
      <!-- Styles -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap-grid.css" />
      <link rel="icon" href="<?php echo base_url(); ?>recruiterfiles/images/fav.png" type="image/png" sizes="16x16">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/icons.css">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/animate.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/style.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/responsive.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/chosen.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/colors/colors.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap.css" />

      <link href="<?php echo base_url().'webfiles/';?>css/owl.carousel.min.css" rel="stylesheet" type="text/css">
      <link href="<?php echo base_url().'webfiles/';?>css/owl.theme.default.min.css" rel="stylesheet" type="text/css">

      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
      <style type="text/css">
        /*today code start 27-11-2020*/
.sliderJyodonew{
padding: 25px 0px;
    margin-top: 75px;
}
.sliderConInner{
    text-align: center;
}
.sliderConInner h2{
    font-family: "Quicksand", sans-serif;
/*    font-weight: 800;*/
    color: #000;
    font-size: 24px;
}

/*new code start*/
.sliderJyodo .owl-nav button {
  position: absolute;
  top: 50%;
  background-color: #000;
  color: #fff;
  margin: 0;
  transition: all 0.3s ease-in-out;
}
.sliderJyodo .owl-nav button.owl-prev {
  left: 0;
}
.sliderJyodo .owl-nav button.owl-next {
  right: 0;
}

.sliderJyodo .owl-dots {
  text-align: center;
  padding-top: 15px;
}
.sliderJyodo .owl-dots button.owl-dot {
  width: 15px;
  height: 15px;
  border-radius: 50%;
  display: inline-block;
  background: #ccc;
  margin: 0 3px;
}
.sliderJyodo .owl-dots button.owl-dot.active {
  background-color: #000;
}
.sliderJyodo .owl-dots button.owl-dot:focus {
  outline: none;
}
.sliderJyodo .owl-nav button {
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    background: rgba(255, 255, 255, 0.38) !important;
}
.sliderJyodo span {
    font-size: 70px;    
    position: relative;
    top: -5px;
}
.sliderJyodo .owl-nav button:focus {
    outline: none;
}
.sliderJyodo .owl-item.active.center{
   margin-top: 20px;  
}
.sliderJyodo .owl-item.active.center .item.caro{

    background: #00a94f;
}
.sliderJyodo .owl-item.active.center .item.caro h2,.sliderJyodo .owl-item.active.center .pyCon h3,.sliderJyodo .owl-item.active.center .validity p,.sliderJyodo .owl-item.active.center .profileView ul li ,.sliderJyodo .owl-item.active.center .profileView ul li span{
    color: #fff;
}
.sliderJyodo .owl-item.active.center .profileView ul li::before{
    color: #fbaf31;
}
.sliderJyodo .owl-item.active.center .scribeBtn a{
background:#00863f;
}
.item.caro{
    background: #fcfcfc;
    padding: 20px;
    text-align: center;
}
.item.caro h2{
    font-family: "Quicksand", sans-serif;
    font-weight: 800;
    color: #fbaf31;
    font-size: 28px;
/*    padding-bottom: 10px;*/
}
.sliderImg{
    width: 135px;
    margin: 0 auto;
}
.sliderImg img{
    width: 100%;
    height: 100%;
    object-fit: cover;
}
.pyCon span{
width: 22px;
    display: inline-block;
    margin-right: 10px;
    position: relative;
    height: 26px;
    top: 1px;
}
.pyCon span img {
    width: 100%;
    object-fit: cover;
}
.pyCon h3{
    font-size: 32px !important;
    font-family: sans-serif !important;
    color: #fbaf31;
    text-align: center;
    margin-top: 15px;
}
.validity p{
    font-size: 16px;
   color: #707070;
       font-family: "Quicksand", sans-serif;
    margin-bottom: 0;
}
.profileView ul li ,.profileView ul li span{
       font-size: 15px;
   color: #000;
        font-family: Roboto;
    text-align: left;
    position: initial;
}
.profileView{
    padding: 20px 0px;
}
.item.caro h2{
    margin-top: 0;
}
.profileView ul li{
/*    padding-bottom: 5px;*/
    position: relative;
    margin-bottom: 10px;
    padding-left: 85px;
}
.profileView ul li:last-child{
    padding-bottom: 0;
}
.profileView ul li::before{    
content: "\f00c";  /* this is your text. You can also use UTF-8 character codes as I do here */
    font-family: FontAwesome;
    left:45px;
    position:absolute;
    color: #68a83f;
    top:0;
}
.scribeBtn{
    width: 100%;
}
.scribeBtn a{
    width: 100%;
    padding: 15px;
    border-radius: 6px;
    color: #fff;
       font-family: Roboto;
 background: #fbaf31;
    width: 100%;
    font-size: 16px;
    display: inline-block;
}
.sliderJyodo .owl-carousel .owl-nav button.owl-next, .sliderJyodo .owl-carousel .owl-nav button.owl-prev,.sliderJyodo .owl-carousel button.owl-dot{
    color: #cdcdcd !important;
}
.sliderJyodo .owl-nav button.owl-prev span{
    position: absolute;
    left: -55px !important;
    top: -175px;
    font-size: 136px;  
}
.sliderJyodo .owl-nav button.owl-next span{
      right: -55px;
    top: -78px;
    font-size: 136px;
}
.sliderJyodo .owl-nav button span:hover ,.sliderJyodo .owl-nav button span{
    color: #224d87 !important
}
.sliderJyodo .owl-nav button.owl-next:hover,.sliderJyodo .owl-nav button.owl-prev:hover{
    background: transparent !important;
}
.pyCon span img.activefull{
    display: none;
}
.sliderJyodo .owl-item.active.center .pyCon h3 span img.unactive{
    display: none;
}
.sliderJyodo .owl-item.active.center .pyCon h3 span img.activefull{
    display: inline-block;
}
.sliderJyodo .owl-stage-outer.owl-height{
    height: 100% !important;
}
.sliderConInner {
    text-align: center;
    padding-top: 60px;
}






  .modalContent{
    display: flex;
    position: relative;
}
.modalNew .modal-body{
    padding: 0;
}
.modalContLeft{
    width: 35%;
    background: #0a4f8a;
    height: 100%;
}
.modalNew .modal-dialog{
    width: 650px!important;
     max-width: initial;
}
.modalContLeft h2{
    font-family: "Quicksand", sans-serif;
    font-weight: 800;
    color: #fff;
    font-size: 22px;
    padding-bottom: 15px;
}
.modalContRight{
    width: 65%;
    background: #f2f2fa;
      padding: 45px 30px 15px !important;

}
.modalContRight h2,.goldenContent h2{
    font-size: 20px;
}
.iconsGold {
    width: 45px;
    display: inline-block;   
    
}
.goldenImg{
    width: 150px;
    margin: 12px auto 0;
}
.goldenImg.goldenImg2{
    width: 110px;
    margin: inherit;
}
.goldenContent h2{
    color: #fff;
}
.goldenImg img{
    width: 100%;
    object-fit: cover;
}
.iconsBtn{
    background: #0a4f8a;
    color: #fff;
    padding: 4px 10px;
    border-radius: 5px;
}
.goldenContent{
    padding: 0px 35px 0px 25px;
    font-size: 16px;
        color:#000000bf; 
    min-width: 216px;
        display: inline-block;
}
.modalContRight ul li{
    padding: 30px 0px;
    border-top: 1px solid #ccccccb8;
}
.modalContRight ul{
    margin-top: 30px;
}
.modalClass{
    padding: 80px 0px 115px;
        text-align: center;
}
.iconsGold img{
    width: 100%;
    object-fit: cover;
}
.modalNew .modal-content {
    box-shadow: #ccc 0px 1px 2px 0px;
    border: 0;
    border-radius: 20px;
    overflow: hidden;
    
}
.close.closed{
     position: absolute;
    right: 15px;
    top: 15px;
    font-size: 30px;
    font-weight: 200;
    color: #fff;
    padding: 4px 10px;
    border-radius: 100%;
    background: #777;
        opacity: 1
}
.modalMailTop{
      background: #0a4f8a;
    padding: 10px 20px;
    display: flex;
    align-items: center;
}
.inputAll.inputAll2{
    border: none;
    background: #cccccc87;
}
.mainSEnd{
    padding: 15px 25px;
}
textarea{
    resize: none;
}
.textBtn{
    float: right;
    margin-top: 15px;
}
.textBtn button,.subPlanButton button{
    border: none;
        padding: 12px;
    border-radius: 6px;
    color: #fff;
    font-family: Roboto;
    background: #fbaf31;
    font-size: 16px;
    display: inline-block;
    min-width: 100px;
}
.subPlanButton button{
    min-width: 200px;
}
.boxs{
    float: left;
    display: block;
    width: 100%;
}
.modalMail{
    position: relative;
}
.sliderJyodo .owl-item.active.center .pyCon h3{
    margin-top: 5px;
}
.dummyPopup{
    margin: 40px 0px;
}
.SlectContent h2{
    font-size: 20px;
    padding: 20px 0px;
}
.modalMail.modalMail2 {
    position: relative;
    text-align: center;
    padding:35px 30px;
}
.modalNew.modalNew2 .modal-dialog {
    width: 525px;
}
        .modalNew.modalNew22 .modal-dialog{
              display: flex;
    align-items: center;
    justify-content: center;
    height: 90vh;
          }
.dummyPopup ul li{
    display: inline-block;
    font-size: 20px;
    margin-right: 20px;
}
.modalMail.modalMail3{
    padding: 25px;
}
.searchHead h2{
   border-bottom: 1px solid #000;
    font-size: 22px;
    padding-bottom: 15px;
}
.modalNew.modalNew3 .modal-dialog {
    width: 800px;
}
.searchContent{
    padding: 15px 0px;
}
.searchContent .textBtn{
    padding: 15px 0px 20px 0px;
}
.searchContent .inputAll.inputAll2{
        color: #000;
    font-family: 'Open Sans';
    font-weight: 600;
}
.textBtn button:hover, .subPlanButton button:hover{
        background-color: #00a94f;
    color: #fff;
}
          .ClientArea{
              min-height: auto;
          }
      </style>
   
   </head>

   <body style="background: #f5f5f5;">

    <section>
      <div class="ClientArea">
        <div class="ClientLogo" style="margin: 0 0 0px 0;">
          <figure>
            <a href="<?php echo base_url();?>recruiter">
              <img src="<?php echo base_url().'recruiterfiles/';?>images/jobyoDa.png">
            </a>
          </figure>
          <ul>
          <?php
          if($this->session->userdata('userSession')) {
          ?>
            <li><a href="<?php echo base_url();?>recruiter/dashboard">Dashboard</a></li>
            <li><a href="<?php echo base_url();?>recruiter/companyprofile">Company Profile</a></li>
          <?php
          } else {
          ?>
            <li><a href="<?php echo base_url();?>recruiter/login">Sign In</a></li>
            <li><a href="<?php echo base_url();?>recruiter/signup">Sign Up</a></li>
            <li><a href="<?php echo base_url();?>recruiter/subscription">Subscription</a></li>
          <?php
          }
          ?>
          </ul>
          <div class="clear"></div>
        </div>


      </div>
    </section>
               <section class="sliderJyodonew" style="margin-top: 17px;">
                   <div class="container">
                      <div id="carousel6" class="owl-carousel sliderJyodo">
                      <?php

                        if($subscriptions) {

                          $x=0;
                          foreach($subscriptions as $subscription) {

                      ?>
                            <div class="item caro">
                                <h2> <?php echo $subscription['plan_name']; ?> </h2>
                                <div class="sliderImg">

                                    <img src="<?php echo base_url().'webfiles/';?>newone/images/<?php echo $subscription['plan_image']; ?>" class="img-fluid" alt="img">

                                </div>
                                <div class="pyCon">
                                   <h3><span>
                                      <img src="<?php echo base_url().'webfiles/';?>newone/images/p.png" class="img-fluid unactive" alt="p">
                                      <img src="<?php echo base_url().'webfiles/';?>newone/images/p1.png" class="img-fluid activefull" alt="p">
                                      </span> <?php echo $subscription['cost']; ?>
                                   </h3>
                                </div>
                                <div class="validity">
                                   <p>Validity - <?php echo $subscription['duration']; ?> months </p>
                                </div>
                                <div class="profileView">
                                   <ul>
                                      <li>Mass Mail : <span> <?php echo $subscription['total_mail']; ?> </span></li>
                                      <li>Job Posting : <span> <?php echo $subscription['total_job_post']; ?> </span></li>
                                      <li>Resume Download : <span> <?php echo $subscription['total_resume_access']; ?> </span></li>
                                      <li>Video Banner : <span> <?php echo $subscription['total_video_post']; ?> </span></li>
                                      <li>Advertisement : <span> <?php echo $subscription['total_advertise']; ?> </span></li>
                                      <li>Job Boosting: <span> <?php echo $subscription['total_job_boost']; ?> </span></li>
                                      <li>Candidate Profile: <span> <?php echo $subscription['total_candidate_profile']; ?> </span></li>
                                   </ul>
                                </div>
                                <div class="scribeBtn">
                                   <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal" onclick='getplan("<?php echo $subscription['plan_name']; ?>")'>Subscribe</a>
                                </div>
                            </div>
                        <?php
                              $x++;
                            }
                          }
                        ?>

                      </div>
                   </div>
                </section>


<!--modal code for subscribe start-->
<div class="modal fade modalNew" id="myModal">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-body">
            <div class="modalContent">
               <button type="button" class="close closed" data-dismiss="modal">&times;</button>
               <div class="modalContLeft">
                  <div class="modalClass">
                     <h2><span id="selectedplan1"></span> Plan</h2>
                     <div class="goldenImg">
                        <img src="<?php echo base_url();?>webfiles/newone/images/gold.png" class="img-fluid" alt="golden">
                     </div>
                  </div>
               </div>
               <div class="modalContRight">
                  <h2>Call Us or Email Us For Subscription</h2>
                  <ul>
                     <li>
                        <a href="#0" data-toggle="modal" data-target="#myModal2" data-dismiss="modal"><span class="iconsGold"> <img src="<?php echo base_url();?>webfiles/newone/images/mail.png" class="img-fluid" alt="golden"></span><span class="goldenContent">help@jobyoda.com</span><span class="iconsBtn">Mail Us</span></a>
                     </li>
                     <li>
                        <a href="#0"><span class="iconsGold"> <img src="<?php echo base_url();?>webfiles/newone/images/Call.png" class="img-fluid" alt="golden"></span><span class="goldenContent">+632045544</span><span class="iconsBtn">Call Us</span></a>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<!--modal code for mail start-->
<div class="modal fade modalNew modalNew22" id="myModal2">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-body">
            <div class="modalMail">
               <button type="button" class="close closed" data-dismiss="modal">&times;</button>
               <div class="modalMailTop">
                  <div class="goldenImg goldenImg2">
                     <img src="<?php echo base_url();?>webfiles/newone/images/gold.png" class="img-fluid" alt="golden">
                  </div>
                  <div class="goldenContent">
                     <h2>Email Us for Plan</h2>
                  </div>
               </div>
               <div class="mainSEnd boxs">
                  <form method="post" action="<?php echo base_url();?>recruiter/recruiter/subscription_email">
                    <div class="form-group">
                       <input type="text" name="name" class="form-control inputAll inputAll2" placeholder="Enter Name" required="">
                    </div>
                    <div class="form-group">
                       <input type="text" name="email" class="form-control inputAll inputAll2" placeholder="Enter Email" required="">
                    </div>
                    <div class="form-group">
                       <input type="text" name="subject" class="form-control inputAll inputAll2" placeholder="Enter Subject" required="">
                    </div>
                    <div class="form-group">
                       <input type="text" name="phone" class="form-control inputAll inputAll2" placeholder="Enter Phone Number" required="">
                    </div>
                    <div class="form-group">
                       <textarea name="description" class="form-control inputAll inputAll2" placeholder="Enter Description" rows="8" required=""></textarea>
                    </div>
                    <div class="textBtn">
                       <input type="hidden" name="plan" value="" id="selectedplan2">
                       <button type="submit">Send</button>
                    </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!--modal code for mail end-->

      <?php include_once('footer.php'); ?>
      </div>
      <!-- LOGIN POPUP -->
      <!-- SIGNUP POPUP -->
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/modernizr.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/script.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/bootstrap.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/wow.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/slick.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/parallax.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/select-chosen.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.scrollbar.min.js" type="text/javascript"></script>

      <script src="<?php echo base_url().'webfiles/';?>js/owl.carousel.js"></script>

      <?php include_once("searchmodalscript.php");?>

      <script type="text/javascript">

        function getplan(get) {
            $("#selectedplan1").html(get);
            $("#selectedplan2").val(get);
        }

        jQuery("#carousel6").owlCarousel({
          autoplay: false,
          lazyLoad: true,
          loop: true,
          margin: 20,
            center: true,
            dots: false,
           /*
          animateOut: 'fadeOut',
          animateIn: 'fadeIn',
          */
          responsiveClass: true,
          autoHeight: true,
          autoplayTimeout: 7000,
          smartSpeed: 800,
          nav: true,
          responsive: {
            0: {
              items: 1
            },

            600: {
              items: 2
            },

            1024: {
              items: 3
            },

            1366: {
              items: 3
            }
          }
        });
            
      </script>
   </body>
</html>


