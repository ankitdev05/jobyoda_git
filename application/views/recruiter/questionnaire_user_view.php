<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>JobYoDA</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <meta name="author" content="CreativeLayers">
      <!-- Styles -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap-grid.css" />
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/icons.css">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/animate.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/all.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/fontawesome.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/responsive.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/chosen.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/colors/colors.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/style.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/croppie.css" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
      <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
   </head>
   <style type="text/css">
      .questHead h2 {
      font-size: 20px;
      color: #000;
      font-weight: 600;
      margin: 0;
      padding-bottom: 10px;
      border-bottom: 2px solid #00a94f;
      margin-bottom: 10px;
      }
      .questionView {
      background: #fff;
      padding: 35px;
      box-shadow: 0 6px 12px rgba(0,0,0,.175);
      margin: 60px 0px;
      }
      .questionAll h2{
      margin: 0;
      padding: 10px;
      font-size: 14px;
      line-height: 20px;
      color: #000;
      font-weight: 600;
      width: 100%;
      float: left;
      display: block;
      padding-left: 0;
          padding: 0
      }
       .form-group{
           margin-bottom: 10px;
       }
      .questionAll label {
      font-size: 13px;
      color: #000;
      font-weight: 600;
      padding: 0;
      margin-bottom: 22px;
      width: 0px !important;
      height: 0px !important;
      float: left;
      width: 100% !important;
      float: left;
      display: block;
      }
      .questionAll label::before {
      content: '';
      position: inherit;
      border: none;
      width: 0px !important;
      height: 0px !important;
      }
      .allOptions li {
      font-size: 13px;
      background: #cccccca6;
      padding: 13px 35px 13px 15px;
      border-radius: 4px;
      font-weight: 600;
      display: inline-block;
      cursor: pointer;
      margin-right: 15px;
      float: left;
      min-width: 175px;
              margin-bottom: 5px;
      }
      .boxs{
      display: block;
      width: 100%;
      float: left;
      }
      .allOptions li.answer.answer2{
        color: #fff;
    background: #00a94f;
      }
   </style>
   <body>
      <div class="theme-layout" id="scrollup">
      <header class="stick-top nohdascr">
         <div class="menu-sec">
            <div class="container-fluid dasboardareas">
               <?php include_once('headermenu.php');?>
            </div>
         </div>
      </header>
      <section>
         <div class="block no-padding">
            <div class="container-fluid dasboardareas">
               <div class="row">
                  <aside class="col-md-3 col-lg-2 column border-right Sidenavbar">
                     <div class="widget">
                        <div class="menuinside">
                           <?php include_once('sidebar.php'); ?>
                        </div>
                     </div>
                  </aside>
                  <div class="col-md-9 col-lg-10 recruprogfd MainWrapper">
                     <div class="questionView boxs">
                        <div class="headsteps-gt questHead">
                           <h5>Questionnaire  ( <?php echo $score; ?>% Score - <?php echo $scoreResult; ?>)</h5>
                           <img src="<?php echo base_url(); ?>recruiterfiles/images/fav.png">
                        </div>
                        <?php
                        $x=1;
                        if($questionData) {
                            foreach($questionData as $questionD) {
                        ?>

                        <div class="questionAll boxs">
                           <div class="form-group boxs">
                              <label>Question <?php echo $x;?></label>
                              <h2><?php echo $questionD['question']; ?></h2>
                           </div>

                           <?php if($questionD['type'] == "text") { } else { ?>
                           <div class="form-group boxs">
                              <label>Options</label>
                              <div class="allOptions boxs">
                                <ul>
                                <?php
                                foreach($questionD['options'] as $optionsValue) {
                                ?>
                                    <li> <?php echo $optionsValue;?> </li>
                                <?php
                                }
                                ?>
                                </ul>
                              </div>
                           </div>
                           <?php } ?>
                           <div class="form-group boxs">
                              <label>Answers</label>
                              <div class="allOptions boxs">
                                <ul>
                                <?php
                                foreach($questionD['answers'] as $answersValue) {
                                ?>
                                    <li class="answer"> <?php echo $answersValue;?> </li>
                                <?php
                                }
                                ?>
                                </ul>
                              </div>
                           </div>

                           <div class="form-group boxs">
                              <div class="allOptions boxs">
                                <label>User Answer</label>
                                <ul>
                                    <li class="answer answer2"> <?php echo $questionD['useranswer'];?> </li>
                                </ul>
                              </div>
                           </div>
                        </div>
                      <?php
                            $x++;
                          }
                        } else {
                      ?>
                          <div class="questionAll boxs">
                           <div class="form-group boxs">
                              <label> No chat found </label>
                           </div>
                          </div>
                      <?php
                        }
                      ?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <?php include_once("footer.php");?>
      <?php include_once("modalpassword.php");?>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/modernizr.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/script.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/wow.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/slick.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/parallax.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/select-chosen.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.scrollbar.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/popper.min.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/bootstrap.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/recruiteryoda.js"></script>
      <script type="text/javascript" src="<?php echo base_url().'recruiterfiles/';?>js/croppie.js"></script>
      <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
   </body>
</html>

