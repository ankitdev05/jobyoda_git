<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>JobYoDA</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <meta name="author" content="CreativeLayers">
      <!-- Styles -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap-grid.css" />
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/icons.css">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/animate.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/all.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/fontawesome.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/responsive.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/chosen.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/colors/colors.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/style.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/croppie.css" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />

      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/individual/css-managesites.css" />
      
   </head>
   <style> 
    #getlengthtext {
        position: relative!important;
        float: right!important;
        bottom: 10px!important;
        color: #27ae62!important;
    }
    span.err {
    font-size:13px!important;
    }
   </style>
   <body>
      <div class="theme-layout" id="scrollup">
         <header class="stick-top nohdascr">
            <div class="menu-sec">
               <div class="container-fluid dasboardareas">
                  
                  <!-- Logo -->
                    <?php include_once('headermenu.php');?>
                  <!-- Menus -->
               </div>
            </div>
         </header>
         <section>
            <div class="block no-padding">
               <div class="container-fluid dasboardareas">
                  <div class="row">
                     <aside class="col-md-3 col-lg-2 column border-right Sidenavbar">
                        <div class="widget">
                           <div class="menuinside">
                              <?php include_once('sidebar.php'); ?>
                           </div>
                        </div>
                     </aside>
                     <div class="col-md-9 col-lg-10 MainWrapper">
                        <div class="row">
                           <div class="col-md-12 psthbs"> 
                              <div class="companyprofileforms fullwidthform managecompanybch mgsirt">
                 <div class="myhdainside">
                                      <h6 style="text-align: center;">Manage Company Profile</h6>
                                    </div>
                                 <form method="post" action="<?php echo base_url();?>recruiter/recruiter/companyupdate" enctype="multipart/form-data">

                                    <div class="mailayer">
                  <div class="camico">
                    <i class="fas fa-camera"></i>
                                          </div>
                  <div class="uploadareas">                 
                   <input type="file" name="profilePic" id="imgInp" accept="image/x-png,image/jpg,image/jpeg">
                   <div class="myimgprofd" style="width:100%;">

                                                      <!--  <input type="hidden" name="cropimg" id="cropimg" value=""> -->
                                                      <?php if(@getimagesize($companyDetails[0]['companyPic'])){ ?>
                                       
                                                   <img id="blah" src="<?php echo $companyDetails[0]['companyPic'] ?>">
                                                   <?php } else{ ?>
                                                   <img id="blah" src="<?php echo base_url();?>recruiterfiles/images/camera1.png">
                                                   <?php }?>
                                                </div>
                  </div>
                                    </div>

                                    <div class="MandatoryBox">
                                      <p><sup>*</sup> Mandatory company logo(Image size within 500X280 - 600X340) </p>

                                      <p id="invalidfileize" style="color:red;"> <?php if(!empty($errors['profilePic'])){ echo "<span class='err'>".$errors['profilePic']."</span>";}?> </p>
                                    </div>
                  
                                    <!-- <span class="imgmsg">*Image dimension should be within 1376X588 & size upto 5 MB.</span> -->                                       
                                    <div class="filldetails flxdtls">
                  <div class="stepcountfrms">
                    <div class="headsteps-gt">
                                             <h5>Company Information</h5>
                                             <img src="<?php echo base_url(); ?>recruiterfiles/images/fav.png">
                                          </div>
                      
                    <input type="text" class="form-control" name="cname" placeholder="Company Name" value="<?php if(!empty($companie[0]['cname'])){echo $companie[0]['cname'];} ?>">

                    <span class="err"><?php if(!empty($errors['cname'])){ echo $errors['cname']; } ?></span>
                    
                    <input type="text" id="txtPlaces" class="form-control" name="address" placeholder="Address" value="<?php if(!empty($companyDetails[0]['address'])){echo $companyDetails[0]['address'];} ?>">
                    
                    <span class="err"><?php if(!empty($errors['address'])){ echo $errors['address']; } ?></span>
                     </div>
                     
                     
                     <div class="stepcountfrms">
                    <div class="headsteps-gt">
                                             <h5>Company Overview</h5>
                                             <img src="<?php echo base_url(); ?>recruiterfiles/images/fav.png">
                                          </div>
                      
                     <textarea id="lengthtext" name="compDesc" placeholder="Company Description" maxlength="2000"><?php if(isset($companyDetails[0]['companyDesc'])){echo $companyDetails[0]['companyDesc'];} ?></textarea>
                     <span style="color:#27ae62;font-size: 11px;bottom: 14px;position:relative;">Maximum characters limit: 2000 </span>
                      <span id="getlengthtext">  </span>


                     </div>
                     
                     </div> 
                                      <div class="filldetails flxdtls">                      
                      <div class="stepcountfrms">
                    <div class="headsteps-gt">
                                             <h5>Recruitment Leader Information</h5>
                                             <img src="<?php echo base_url(); ?>recruiterfiles/images/fav.png">
                                          </div>
                      
                     <input type="hidden" name="rid" value="<?php if(!empty($companie[0]['id'])){ echo $companie[0]['id']; } ?>">    
                                       <input type="text" class="form-control" name="fname" placeholder="First Name" value="<?php if(!empty($companie[0]['fname'])){echo $companie[0]['fname'];} ?>">
                                       
                                       <span class="err"><?php if(!empty($errors['fname'])){ echo $errors['fname']; } ?></span>

                                       <input type="text" class="form-control" name="lname" placeholder="Last Name" value="<?php if(!empty($companie[0]['lname'])){echo $companie[0]['lname'];} ?>"> 

                                       <span class="err"><?php if(!empty($errors['lname'])){ echo $errors['lname']; } ?></span>
                     
                                       <input type="text" class="form-control" name="email" placeholder="Email id" value="<?php if(!empty($companie[0]['email'])){echo $companie[0]['email'];} ?>">
                                       <span class="err"><?php if(!empty($errors['email'])){ echo $errors['email']; } ?></span>
                                       <div class="divhalfu">
                                       <select name="industry">
                                          <option value=""> Select Industry </option>
                                          <?php
                                               foreach($industryLists as $industryList) {
                                                if($industryList['id'] != 82 && $industryList['id'] != 83) {
                                          ?>
                                                <option <?php if(isset($companyDetails[0]['industry'])) {if($industryList['id']==$companyDetails[0]['industry']){ echo "selected"; } }?> value="<?php echo $industryList['id'];?>"><?php echo $industryList['name'];?></option>
                                          <?php } }?>
                                       </select>
                                       
                                       <select name="phonecode" class="form-control">
                                          <option value=""> Select Phone Code</option>
                                        <?php
                                          foreach($phonecodes as $phonecode) {
                                        ?>
                                            <option <?php if($phonecode['phonecode']==$companyDetails[0]['phonecode']){ echo "selected"; }?> value="<?php echo $phonecode['phonecode'];?>"> <?php echo $phonecode['name'];?> - <?php echo $phonecode['phonecode'];?> </option>
                                        <?php
                                          }
                                        ?>
                                       </select>

                                       <input type="text" class="form-control" name="phone" placeholder="Phone Number" value="<?php if(isset($companyDetails[0]['phone'])){echo $companyDetails[0]['phone'];} ?>">
                                       
                                       

                                      </div>
                                       
                                       <!--<input type="text" class="form-control" name="rating" placeholder="Glass Door Rating" value="<?php //if(isset($companyDetails[0]['rating'])){if($companyDetails[0]['rating'] == 0){}else{echo $companyDetails[0]['rating'];}} ?>">
                                       
                                       <input type="text" class="form-control" name="gross_salary" placeholder="Gross Salary" value="<?php //if(isset($companyDetails[0]['gross_salary'])){if($companyDetails[0]['gross_salary'] == 0){}else{echo $companyDetails[0]['gross_salary'];}} ?>">
                                       -->
                                       <!--<input type="text" class="form-control" name="basic_salary" placeholder="Basic Salary" value="<?php //if(isset($companyDetails[0]['basic_salary'])){if($companyDetails[0]['basic_salary'] == 0){}else{echo $companyDetails[0]['basic_salary'];}} ?>">-->
                                       
                                       <!--<input type="text" class="form-control" name="work_off" placeholder="Work Off" value="<?php //if(isset($companyDetails[0]['work_off'])){echo $companyDetails[0]['work_off'];} ?>">
                                       -->
                                       <!--<input type="text" class="form-control" name="leave" placeholder="Annual leaves Count" value="<?php //if(isset($companyDetails[0]['annual_leave'])){if($companyDetails[0]['annual_leave'] == 0){}else{echo $companyDetails[0]['annual_leave'];}} ?>">
                                       
                                       <input type="text" class="form-control" name="job_type" placeholder="Job Type" value="< ?php //if(isset($companyDetails[0]['job_type'])){echo $companyDetails[0]['job_type'];} ?>">
                                       -->
                     
                     </div>
                     
                     
                     
                     </div>
                                  
                                    <div>
                                         
                                    </div>
                            
                              <!--<?php 
                               //echo "<pre>";
                               //print_r($topPicks2);
                              
                              ?>-->
                
                <div class="filldetails flxdtls">
               
                
                  <div class="stepcountfrms">
                   <div class="headsteps-gt">
                                             <h5>Please select all company-wide benefits</h5>
                                             <img src="<?php echo base_url(); ?>recruiterfiles/images/fav.png">
                                          </div>
                              <?php  ?>            
                              <div class="newfiletrsgree">
                                  <h2></h2>
                                 <div class="main-hda right">
                                    <h6>Top Picks</h6>
                                 </div>
                                 <div class="filterchekers">
                                     <!--<ul>
                                         <li>
                                     <div class="setsboxsone">
                                      <div class="checkflowrt">
                                         <div class="custom-control custom-radio">
                                            <input type="checkbox" class="checked_value" value="1" id="customRadio20" name="toppic[]" <?php //if($topPicks2){if(in_array('1',$topPicks2)){echo "checked";}}?>>
                                            <label class="custom-control-label" for="customRadio20"><img src="<?php //echo base_url().'webfiles/';?>img/car.png">Automotive</label>
                                         </div>
                                      </div></li>
                                      <li>
                                      <div class="checkflowrt">
                                         <div class="custom-control custom-radio">
                                            <input type="checkbox" class="custom-control-input" value="2" id="customRadio21" name="toppic[]" <?php //if($topPicks2){if(in_array('2',$topPicks2)){echo "checked";}}?>>
                                            <label class="custom-control-label" for="customRadio21"><img src="<?php //echo base_url().'webfiles/';?>img/sun.png">Technology</label>
                                         </div>
                                      </div>
                                   </div></li>
                                   </ul>-->
                                    <ul>
                                       <!-- <li class="<?php if($topPicks2){if(in_array(1, $topPicks2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" id="toppic1" type="checkbox" name="toppicks[]" value="1" <?php if($topPicks2){ if(in_array(1, $topPicks2)){ echo "checked";}}?>>
                                          <p class="txtonly">Bonus</p>
                                          <p> Joining<br>Bonus</p>
                                       </li> -->
                                       
                                       <li class="<?php if($topPicks2){if(in_array(2, $topPicks2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" id="toppic2" type="checkbox" name="toppicks[]" value="2" <?php if($topPicks2){ if(in_array(2, $topPicks2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-food.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-food-1.png" class="hvrsicos">
                                          <p> Free <br>Food</p>
                                       </li>

                                       <li class="<?php if($topPicks2){if(in_array(3, $topPicks2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" id="toppic3" type="checkbox" name="toppicks[]" value="3" <?php if($topPicks2){ if(in_array(3, $topPicks2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/day-1-hmo.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/day-1-hmo-white.png" class="hvrsicos">
                                          <p>Day 1 HMO</p>
                                       </li>

                                       <li class="<?php if($topPicks2){if(in_array(4, $topPicks2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" id="toppic4" type="checkbox" name="toppicks[]" value="4" <?php if($topPicks2){ if(in_array(4, $topPicks2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/day-1-hmo-for-depended.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/day-1-hmo-for-depended-1.png" class="hvrsicos">
                                          <p> Day 1 HMO<br> for Dependent</p>
                                       </li>

                                       <!-- <li class="<?php if($topPicks2){if(in_array(5, $topPicks2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" id="toppic5" type="checkbox" name="toppicks[]" value="5" <?php if($topPicks2){ if(in_array(5, $topPicks2)){ echo "checked";}}?>>
                                          <i class="fas fa-sun"></i>
                                          <p>Day Shift</p>
                                       </li> -->

                                       <li class="<?php if($topPicks2){if(in_array(6, $topPicks2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" id="toppic6" type="checkbox" name="toppicks[]" value="6" <?php if($topPicks2){ if(in_array(6, $topPicks2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/14-month-pay.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/14-month-pay-1.png" class="hvrsicos">
                                          <p> 14th Month Pay</p>
                                       </li>

                                        <li class="<?php if($topPicks2){if(in_array(7, $topPicks2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" id="toppic7" type="checkbox" name="toppicks[]" value="7" <?php if($topPicks2){ if(in_array(7, $topPicks2)){ echo "checked";}}?>>
                                          
                                          <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/wfh.png" class="normicos">  
                                          <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/wfhun.png" class="hvrsicos">
                                          <p> Work From Home </p>
                                       </li>

                                    </ul>
                                 </div>
                                 
                              </div>
                              <?php ?>
                              <div class="newfiletrsgree">
                                 <div class="main-hda right">
                                    <h6>Allowances and Incentives</h6>
                                 </div>
                                 <div class="filterchekers">
                                    <ul id="filterchekers">

                                       <li class="tapsct <?php if($allowance2){if(in_array(1, $allowance2)){ echo "selectedgreen"; }}?>" id="tapsct1" data-value="1">
                                          <input class="checked_value" id="allow1" type="checkbox" name="allowances[]" value="1" <?php if($allowance2){ if(in_array(1, $allowance2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/cell-phone-allowance.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/cell-phone-allowance-1.png" class="hvrsicos">
                                          <p> Cellphone <br>Allowance</p>
                                       </li>

                                       <li class="tapsct <?php if($allowance2){if(in_array(2, $allowance2)){ echo "selectedgreen"; }}?>" id="tapsct2" data-value="2">
                                          <input class="checked_value" id="allow2" type="checkbox" name="allowances[]" value="2" <?php if($allowance2){ if(in_array(2, $allowance2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-parking.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-parking-1.png" class="hvrsicos">
                                          <p>Free <br>Parking</p>
                                       </li> 

                                       <li class="tapsct <?php if($allowance2){if(in_array(3, $allowance2)){ echo "selectedgreen"; }}?>" id="tapsct3" data-value="3">
                                          <input class="checked_value" id="allow3" type="checkbox" name="allowances[]" value="3" <?php if($allowance2){ if(in_array(3, $allowance2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-shuttle.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-shuttle-1.png" class="hvrsicos">
                                          <p> Free <br> Shuttle</p>
                                       </li>

                                       <li class="tapsct <?php if($allowance2){if(in_array(4, $allowance2)){ echo "selectedgreen"; }}?>" id="tapsct4" data-value="4">
                                          <input class="checked_value" id="allow4" type="checkbox" name="allowances[]" value="4" <?php if($allowance2){ if(in_array(4, $allowance2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/annual--performance-bonus.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/annual--performance-bonus-1.png" class="hvrsicos">
                                          <p> Annual <br> Performance Bonus</p>
                                       </li>

                                       <li class="tapsct <?php if($allowance2){if(in_array(5, $allowance2)){ echo "selectedgreen"; }}?>" id="tapsct5" data-value="5">
                                          <input class="checked_value" id="allow5" type="checkbox" name="allowances[]" value="5" <?php if($allowance2){ if(in_array(5, $allowance2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/reteirment-benifits.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/reteirments-benefits-1.png" class="hvrsicos">
                                          <p> Retirements <br> Benefits</p>
                                       </li>

                                       <li class="tapsct <?php if($allowance2){if(in_array(6, $allowance2)){ echo "selectedgreen"; }}?>" id="tapsct6" data-value="6">
                                          <input class="checked_value" id="allow6" type="checkbox" name="allowances[]" value="6" <?php if($allowance2){ if(in_array(6, $allowance2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/transportation.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/transportation-1.png" class="hvrsicos">
                                          <p> Transport <br> Allowance</p>
                                       </li>
                                    </ul>
                                 </div>
                                 <div class="filterchekers">
                                    <ul>
                                       <li class="tapsct <?php if($allowance2){if(in_array(7, $allowance2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" id="allow7" type="checkbox" name="allowances[]" value="7" <?php if($allowance2){ if(in_array(7, $allowance2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/monthly-performance-incentive.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/monthly-performance-incentive-1.png" class="hvrsicos">
                                          <p> Monthly Performance <br> Incentives</p>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="newfiletrsgree">
                                 <div class="main-hda right">
                                    <h6>Medical Benefits</h6>
                                 </div>
                                 <div class="filterchekers">
                                    <ul>
                                       <li class="<?php if($medical2){if(in_array(1, $medical2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" type="checkbox" name="medical[]" id="medical1" value="1" <?php if($medical2){ if(in_array(1, $medical2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-hmo-for-dependents.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-hmo-for-dependents-1.png" class="hvrsicos">
                                          <p> Free HMO for<br>Dependents</p>
                                       </li>

                                       <li class="<?php if($medical2){if(in_array(2, $medical2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" type="checkbox" name="medical[]" id="medical2" value="2" <?php if($medical2){ if(in_array(2, $medical2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/critical-illness-benefits.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/critical-illness-benefits1.png" class="hvrsicos">
                                          <p> Critical Illness <br>Benefits</p>
                                       </li>

                                       <li class="<?php if($medical2){if(in_array(3, $medical2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" type="checkbox" name="medical[]" id="medical3" value="3" <?php if($medical2){ if(in_array(3, $medical2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/life-insurence.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/life-insurence-1.png" class="hvrsicos">
                                          <p>Life <br>Insurance</p>
                                       </li>

                                       <li class="<?php if($medical2){if(in_array(4, $medical2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" type="checkbox" name="medical[]" value="4" id="medical4" <?php if($medical2){ if(in_array(4, $medical2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/maternity-assistance.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/maternity-assistance-1.png" class="hvrsicos">
                                          <p> Maternity<br> Assistance</p>
                                       </li>

                                       <li class="<?php if($medical2){if(in_array(5, $medical2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" type="checkbox" name="medical[]" value="5" id="medical5" <?php if($medical2){ if(in_array(5, $medical2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/medicine-reimbursemer.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/medicine-reimbursemer-1.png" class="hvrsicos">
                                          <p>Medicine <br>Reimbursement</p>
                                       </li>
                                    </ul>
                                 </div>
                  
                  
                              </div>
                              <div class="ManagecompanyButton" style="position: relative;bottom: 0; width: 100%">
                                    <button class="updts" type="submit">Save</button>
                              </div>
                             <!--  <div class="newfiletrsgree">
                                 <div class="main-hda right">
                                    <h3>Leaves</h3>
                                 </div>
                                 <div class="filterchekers">
                                    <ul>
                                       <li id="leaves1" class="<?php if($leave2){if(in_array(1, $leave2)){ echo 'selectedgreen'; }}?>">
                                          <input type="checkbox" class="checked_value" id="leave1"  name="leavs[]" value="1" <?php if($leave2){if(in_array(1, $leave2)){ echo "checked"; }}?>>
                                          <i class="fas fa-snowflake"></i>
                                          <p> Weekend Off</p>
                                       </li>
                                       <li id="leaves2" class="<?php if($leave2){if(in_array(2, $leave2)){ echo 'selectedgreen'; }}?>">
                                          <input type="checkbox" class="checked_value" id="leave2"  name="leavs[]" value="2" <?php if($leave2){if(in_array(2, $leave2)){ echo "checked"; }}?>>
                                          <i class="fas fa-snowflake"></i>
                                          <p>Holiday Off</p>
                                -->        </li>
                                    </ul>
                                 </div>
                                 
                              </div>
                              <div class="newfiletrsgree">
                                 <!-- <div class="main-hda right">
                                    <h3>Work Shifts</h3>
                                 </div> -->
                                 <!-- <div class="filterchekers1">
                                    <ul><?php //print_r($workshift2);?>
                                       <li id="workshift1" class="work-class <?php if(!empty($workshift2)){ if($workshift2[0]=="1"){ echo 'selectedgreen';}} ?>">
                                          <input class="work-radio" type="radio" id="works1" name="shifts[]" value="1">
                                          <i class="fas fa-star"></i> 
                                          <p> Mid Shift </p>
                                       </li>

                                       <li id="workshift2" class="work-class <?php if(!empty($workshift2)){ if($workshift2[0]=="2"){ echo 'selectedgreen';}} ?>">
                                          <input class="work-radio" id="works2" type="radio" name="shifts[]" value="2">
                                          <i class="fas fa-moon-o"></i> 
                                          <p> Night Shift </p>
                                       </li>
                                       <li id="workshift3" class="work-class <?php if(!empty($workshift2)){ if($workshift2[0]=="3"){ echo 'selectedgreen';}} ?>">
                                          <input type="radio" id="works3" class="work-radio" name="shifts[]" value="3" >
                                          <p class="txtonly">24/7</p>
                                          <p>24/7 </p>
                                       </li>

                                       
                                    </ul>
                                 </div> -->
                                 </form>
                   </div>
                              </div>
                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
      </div>
      <div id="uploadimageModal" class="modal" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            
            </div>
            <div class="modal-body">
            <div class="row">
               <div class="col-md-12 text-center">
                    <div id="image_demo" style=" margin-top:30px"></div>
               </div>
            </div>
            </div>
            <div class="modal-footer">
            <button class="btn btn-success crop_image">Crop & Upload Image</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
      </div>
    </div>
</div>
      </section>
      <?php include_once("footer.php");?>
      <?php include_once("modalpassword.php");?>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/modernizr.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/script.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/wow.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/slick.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/parallax.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/select-chosen.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.scrollbar.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/popper.min.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/bootstrap.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/recruiteryoda.js"></script>
      <script type="text/javascript" src="<?php echo base_url().'recruiterfiles/';?>js/croppie.js"></script>

      <?php include_once("searchmodalscript.php");?>
      
   </body>

<script>
  $(function () { 
    $('[data-toggle="tooltip"]').tooltip({trigger: 'manual'}).tooltip('show');
  });  
   
  $(".progress-bar").each(function(){
    each_bar_width = $(this).attr('aria-valuenow');
    $(this).width(each_bar_width + '%');
  });
         
</script>
   
   <script>
    $(".filterchekers li").click(function(){
      $(this).toggleClass("selectedgreen");  
    });
   </script>

<script type="text/javascript">
    
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
      }
    }
    
    $("#imgInp").change(function() {

        readURL(this);

        var fileUpload = document.getElementById("imgInp");
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png|.gif)$");
        if (regex.test(fileUpload.value.toLowerCase())) {

           if (typeof (fileUpload.files) != "undefined") {

              //Initiate the FileReader object.
              var reader = new FileReader();
              //Read the contents of Image File.
              reader.readAsDataURL(fileUpload.files[0]);
              reader.onload = function (e) {
                  //Initiate the JavaScript Image object.
                  var image = new Image();
                  //Set the Base64 string return from FileReader as source.
                  image.src = e.target.result;
                  //Validate the File Height and Width.
                  image.onload = function () {
                      var height = parseInt(this.height);
                      var width = parseInt(this.width);
                      
                      console.log(width);
                      console.log(height);

                      if(width <= 600 && height <= 340) {

                          if(width >= 500 && height >= 280) {
                            
                            $("#invalidfileize").html("");
                          
                          } else {
                              $("#invalidfileize").html("Image size must be within 500X280 - 600X340");
                              $('#imgInp').val('');
                              $('#blah').attr('src', "<?php echo base_url();?>recruiterfiles/images/camera1.png");
                          }
                      } else {
                          $("#invalidfileize").html("Image size must be within 500X280 - 600X340");
                          $('#imgInp').val('');
                          $('#blah').attr('src', "<?php echo base_url();?>recruiterfiles/images/camera1.png");
                      }
                  };
              }
           } else {
              $("#invalidfileize").html("Please select a valid Image file.");
              $('#imgInp').val('');
              $('#blah').attr('src', "<?php echo base_url();?>recruiterfiles/images/camera1.png");
           }
        } else {
           $("#invalidfileize").html("Please select a valid Image file.");
           $('#imgInp').val('');
           $('#blah').attr('src', "<?php echo base_url();?>recruiterfiles/images/camera1.png");
        }
    });
    
    $('.work-radio').change(function() {
            $(".work-class").removeClass("selectedgreen");
            if ($(this).is(':checked')){
                $(this).closest("li").addClass("selectedgreen");
              }
              else
                $(this).closest("li").removeClass("selectedgreen");
        });
</script>
<script type="text/javascript">
      var specialChars = "<>@!#$%^&*()_+[]{}?:;|'\"\\.~`="
      var check = function(string){
          for(i = 0; i < specialChars.length;i++){
              if(string.indexOf(specialChars[i]) > -1){
                  return true
              }
          }
          return false;
      }


      $("#txtPlaces").change(function(){
            var textplace = $(this).val();

            if(check($('#txtPlaces').val()) == true){
                  alert("Special characters like !,@,#,$,%,^,&,*,(,),+,= not required");
              }
      });

</script>

<script type="text/javascript">
      $(document).ready(function() {
            var lengthText = $('#lengthtext').val();
            var calcLength = parseInt(2000) - parseInt(lengthText.length);
            var textcount = "Remaining characters : " + calcLength;
            $('#getlengthtext').html(textcount);

            $('#lengthtext').on('keyup', function() {
                  var lengthText = $(this).val();
                  var calcLength = parseInt(2000) - parseInt(lengthText.length);
                  var textcount = "Remaining characters : " + calcLength;
                  $('#getlengthtext').html(textcount);
            });

      });
</script>

<script>  
$(document).ready(function(){

   /*$image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:280,
      height:280,
      type:'square' //circle
    },
    boundary:{
      width:300,
      height:300
    }
  });*/

  /*$('#imgInp').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').modal('show');
  });*/

  /*$('.crop_image').click(function(event){
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
      $.ajax({
        url:"<?php echo base_url(); ?>recruiter/recruiter/upload",
        type: "POST",
        data:{"image": response},
        success:function(data)
        {
         //alert(data);
          $('#uploadimageModal').modal('hide');
          $('#blah').attr('src',data);
          $('#cropimg').val(data);
        }
      });
    })
  });*/

});  
</script>

