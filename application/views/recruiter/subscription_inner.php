<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>JobYoDA</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <meta name="author" content="CreativeLayers">
      <!-- Styles -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap-grid.css" />
      <link rel="icon" href="<?php echo base_url(); ?>recruiterfiles/images/fav.png" type="image/png" sizes="16x16">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/icons.css">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/animate.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/all.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/fontawesome.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/responsive.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/chosen.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/colors/colors.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/style.css" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />

      <link href="<?php echo base_url().'webfiles/';?>css/owl.carousel.min.css" rel="stylesheet" type="text/css">
      <link href="<?php echo base_url().'webfiles/';?>css/owl.theme.default.min.css" rel="stylesheet" type="text/css">
   </head>
   <style>
      a:hover {
      text-decoration: none;
      }
      .tech i.fa.fa-ellipsis-h {
      float: right;
      }
      .progr span.progress-bar-tooltip {
      position: initial;
      color: #fff;
      }
      .filterchekers ul {
      width: 100%;
      }
      .manage-jobs-sec {
      float: none;
      width: 100%;
      margin: 0 auto;
      }
      .link a {
      background: #26ae61;
      color: #fff;
      word-break: keep-all;
      font-size: 11px;
      line-height: 0;
      padding: 10px;
      }
      .link {
      float: left;
      width: 100px;
      }
      .btn-extars{
      display:none;}
      .inner-header {
      float: left;
      width: 100%;
      position: relative;
      padding-top: 60px; padding-bottom: 15px;
      z-index: 0;
      }
      .tree_widget-sec > ul > li.inner-child.active > a {
      color: #26ae61;
      }
      .tree_widget-sec > ul > li.inner-child.active > a i {
      color: #26ae61;}
      .contact-edit .srch-lctn:hover {
      background: #26ae61;
      color: #ffffff;
      border-color: #26ae61;
      }
      .contact-edit .srch-lctn {
      color: #26ae61;
      border: 2px solid #26ae61;
      border: 2px solid #26ae61;
      }
      .contact-edit > form button{
      border: 2px solid #26ae61;}
      .profile-form-edit > form button:hover, .contact-edit > form button:hover {
      background: #26ae61;
      color: #ffffff;
      }
      .step.active i {
      background: #26ae61;
      border-color: #26ae61;
      color: #ffffff;
      }
      .step i{
      color: #26ae61;}
      .step.active span {
      color: #000;
      }
      .menu-sec nav > ul > li.menu-item-has-children > a::before{
      display:none;}
      .inner-header {
      float: left;
      width: 100%;
      position: relative;
      padding-top: 60px; padding-bottom: 15px;
      z-index: 0;
      }
      .manage-jobs-sec > table thead tr td{
      color: #26ae61;}
      .extra-job-info > span i {
      float: left;
      font-size: 30px;
      color: #26ae61;
      width: 30px;
      margin-right: 12px;
      }
      .action_job > li span {
      background: #26ae61;}
      .action_job > li span::before{
      background: #26ae61;}
      .action_job > li {
      float: left;
      margin: 0;
      position: relative;
      width: 15px;
      }
      .manage-jobs-sec > h3 {
      padding-left: 0px; 
      margin-top: 40px;
      text-align: center;
      }
      .fall {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 23px 10px;
      }



.psthbs .companyprofileforms.fullwidthform .filldetails {
      width: 90%!important;
}


   </style>
   <body>
      <div class="theme-layout" id="scrollup">
         <header class="stick-top nohdascr">
            <div class="menu-sec">
               <div class="container-fluid dasboardareas">
                  <!-- Logo -->
                  <?php include_once('headermenu.php');?>
                  <!-- Menus -->
               </div>
            </div>
         </header>
         <section>
            <div class="block no-padding">
               <div class="container-fluid dasboardareas">
                  <div class="row">
                     <aside class="col-md-3 col-lg-2 column border-right Sidenavbar">
                        <div class="widget">
                           <div class="menuinside">
                              <?php include_once('sidebar.php'); ?>
                           </div>
                        </div>
                     </aside>
                     <div class="col-md-9 col-lg-10 MainWrapper">
                        <div class="row">
                           <div class="col-md-12 psthbs">
                              <div class="companyprofileforms fullwidthform">
                                 
                                 <div class="myhdainside">
                                    <h6>Assign a Job that you will be working on</h6>
                                 </div>
                                 
                                 <!-- <div class="filldetails">
                                    <div class="stepcountfrms"> -->
                                          
                                          <section class="sliderJyodonew">
                                           <div class="container">
                                              <div id="carousel6" class="owl-carousel sliderJyodo">
                                              <?php

                                                if($subscriptions) {

                                                  $x=0;
                                                  foreach($subscriptions as $subscription) {

                                              ?>
                                                    <div class="item caro">
                                                        <h2> <?php echo $subscription['plan_name']; ?> </h2>
                                                        <div class="sliderImg">
                                                          <?php
                                                            if($x % 2 == 0) {
                                                          ?>
                                                           <img src="<?php echo base_url().'webfiles/';?>newone/images/free3.png" class="img-fluid" alt="img">
                                                          <?php
                                                            } else {
                                                          ?>
                                                            <img src="<?php echo base_url().'webfiles/';?>newone/images/free1.png" class="img-fluid" alt="img">
                                                          <?php
                                                            }
                                                          ?>
                                                        </div>
                                                        <div class="pyCon">
                                                           <h3><span>
                                                              <img src="<?php echo base_url().'webfiles/';?>newone/images/p.png" class="img-fluid unactive" alt="p">
                                                              <img src="<?php echo base_url().'webfiles/';?>newone/images/p1.png" class="img-fluid activefull" alt="p">
                                                              </span> <?php echo $subscription['cost']; ?>
                                                           </h3>
                                                        </div>
                                                        <div class="validity">
                                                           <p>Validity - <?php echo $subscription['duration']; ?> months </p>
                                                        </div>
                                                        <div class="profileView">
                                                           <ul>
                                                              <li>Mass Mail : <span> <?php echo $subscription['total_mail']; ?> </span></li>
                                                              <li>Job Posting : <span> <?php echo $subscription['total_job_post']; ?> </span></li>
                                                              <li>Resume Download : <span> <?php echo $subscription['total_resume_access']; ?> </span></li>
                                                              <li>Video Banner : <span> <?php echo $subscription['total_video_post']; ?> </span></li>
                                                              <li>Advertisement : <span> <?php echo $subscription['total_advertise']; ?> </span></li>
                                                              <li>Job Boosting: <span> <?php echo $subscription['total_job_boost']; ?> </span></li>
                                                           </ul>
                                                        </div>
                                                        <div class="scribeBtn">
                                                           <a href="javascript:void(0)" data-toggle="modal" data-target="#subscribeModal" onclick='getplan("<?php echo $subscription['plan_name']; ?>")'>Subscribe</a>
                                                        </div>
                                                    </div>
                                                <?php
                                                      $x++;
                                                    }
                                                  }
                                                ?>

                                              </div>
                                           </div>
                                        </section>
                                       
                                    <!-- </div>
                                 </div> -->
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
      </div>


      <!-- <input type="hidden" id="numberofrecord" value="0"> -->
      </section>

            <!-- Modal -->
      <div class="modal fade" id="subscribeModal" tabindex="-1" role="dialog" aria-labelledby="subscribeModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Call Us or Email Us For Subscription</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <p><strong>Email -</strong> <a href="javascript:void(0)" data-toggle="modal" data-target="#emailModal">help@jobyoda.com</a></p>
                <p><strong>Phone No -</strong> <a href="tel:+63 9178721630">+63 9178721630</a></p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

      <!--modal code for mail start-->
<div class="modal fade modalNew modalNew22" id="myModal2">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-body">
            <div class="modalMail">
               <button type="button" class="close closed" data-dismiss="modal">&times;</button>
               <div class="modalMailTop">
                  <div class="goldenImg goldenImg2">
                     <img src="<?php echo base_url();?>webfiles/newone/images/gold.png" class="img-fluid" alt="golden">
                  </div>
                  <div class="goldenContent">
                     <h2>Email Us for Plan</h2>
                  </div>
               </div>
               <div class="mainSEnd boxs">
                  <form method="post" action="<?php echo base_url();?>recruiter/recruiter/subscription_email">
                    <div class="form-group">
                       <input type="text" name="name" class="form-control inputAll inputAll2" placeholder="Enter Name" required="">
                    </div>
                    <div class="form-group">
                       <input type="text" name="email" class="form-control inputAll inputAll2" placeholder="Enter Email" required="">
                    </div>
                    <div class="form-group">
                       <input type="text" name="subject" class="form-control inputAll inputAll2" placeholder="Enter Subject" required="">
                    </div>
                    <div class="form-group">
                       <input type="text" name="phone" class="form-control inputAll inputAll2" placeholder="Enter Phone Number" required="">
                    </div>
                    <div class="form-group">
                       <textarea name="description" class="form-control inputAll inputAll2" placeholder="Enter Description" rows="8" required=""></textarea>
                    </div>
                    <div class="textBtn">
                       <input type="hidden" name="plan" value="" id="selectedplan2">
                       <button type="submit">Send</button>
                    </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!--modal code for mail end-->

      <?php include_once("footer.php");?>
      <?php include_once("modalpassword.php");?>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/modernizr.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/script.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/bootstrap.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/wow.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/slick.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/parallax.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/select-chosen.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.scrollbar.min.js" type="text/javascript"></script>

      <script src="<?php echo base_url().'webfiles/';?>js/owl.carousel.js"></script>

      <script type="text/javascript">

        function getplan(get) {
            $("#selectedplan1").html(get);
            $("#selectedplan2").val(get);
        }

        jQuery("#carousel6").owlCarousel({
          autoplay: false,
          lazyLoad: true,
          loop: true,
          margin: 20,
            center: true,
            dots: false,
           /*
          animateOut: 'fadeOut',
          animateIn: 'fadeIn',
          */
          responsiveClass: true,
          autoHeight: true,
          autoplayTimeout: 7000,
          smartSpeed: 800,
          nav: true,
          responsive: {
            0: {
              items: 1
            },

            600: {
              items: 2
            },

            1024: {
              items: 3
            },

            1366: {
              items: 3
            }
          }
        });
            
      </script>

   </body>
</html>
