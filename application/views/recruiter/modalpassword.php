<style>
    .validErr{
        color:red;
    }
</style>

<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
         <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">Change Password</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
               </div>
               <div class="modal-body">
                  <div class="formmidaress modpassfull">
                     <div class="filldetails">
                        <form method="post" action="<?php echo base_url();?>recruiter/recruiter/changepassword">
                           <div class="forminputspswd">
                              <input type="password" class="form-control" name="oldPass" placeholder="Old Password">
                              <img src="<?php echo base_url().'recruiterfiles/';?>images/keypass.png">
                              <?php if(isset($validation_errors['oldPass'])){ ?><span class="validErr"><?php echo $validation_errors['oldPass']; ?></span><?php }?>
                           </div>
                           <div class="forminputspswd">
                              <input type="password" class="form-control" name="newPass" placeholder="New Password">
                              <img src="<?php echo base_url().'recruiterfiles/';?>images/keypass.png">
                              <?php if(isset($validation_errors['newPass'])){ ?><span class="validErr"><?php echo $validation_errors['newPass']; ?></span><?php }?>
                           </div>
                           <div class="forminputspswd">
                              <input type="password" class="form-control" name="confirmPass" placeholder="Confirm New Password">
                              <img src="<?php echo base_url().'recruiterfiles/';?>images/keypass.png">
                              <?php if(isset($validation_errors['confirmPass'])){ ?><span class="validErr"><?php echo $validation_errors['confirmPass']; ?></span><?php }?>
                           </div>
                           <button type="submit" class="srchbtns">Change</button>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
      <?php
         if(isset($validation_errors)) {
             print_r($validation_errors) ;
             /*if($this->session->tempdata('validationError')){$this->session->unset_tempdata('validationError');}*/
      ?>
            <script type="text/javascript">
                $(window).on('load',function(){
                    $('#exampleModalCenter').modal('show');
                });
                if ( window.history.replaceState ) {
                    window.history.replaceState( null, null, window.location.href );
                    //window.location.href = "<?php echo base_url();?>recruiter/recruiter/index";
                }
            </script>
      <?php
         }
     ?>