<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>JobYoDA</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <meta name="author" content="CreativeLayers">
      <!-- Styles -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap-grid.css" />
      <link rel="icon" href="<?php echo base_url(); ?>recruiterfiles/images/fav.png" type="image/png" sizes="16x16">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/icons.css">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/animate.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/all.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/fontawesome.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/responsive.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/chosen.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/colors/colors.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap.css" />

      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/style.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>recruiterfiles/css/jquery-ui.min.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>recruiterfiles/css/jquery-ui.theme.min.css" />
       <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/modernizr.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/script.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/wow.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/slick.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/parallax.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/select-chosen.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.scrollbar.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/popper.min.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/bootstrap.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/recruiteryoda.js"></script>

      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
      <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script> 
      <!-- <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script> -->
   </head>
   <style>

   #ui-datepicker-div{
      display: none;
   }
   .validerr{
      color: red;
   }
      a:hover {
      text-decoration: none;
      }
      .tech i.fa.fa-ellipsis-h {
      float: right;
      }
      .progr span.progress-bar-tooltip {
      position: initial;
      color: #fff;
      }
      .manage-jobs-sec {
      float: none;
      width: 100%;
      margin: 0 auto;
      }
      .link a {
      background: #26ae61;
      color: #fff;
      word-break: keep-all;
      font-size: 11px;
      line-height: 0;
      padding: 10px;
      }
      .link {
      float: left;
      width: 100px;
      }
      .btn-extars{
      display:none;}
      .inner-header {
      float: left;
      width: 100%;
      position: relative;
      padding-top: 60px; padding-bottom: 15px;
      z-index: 0;
      }
      .tree_widget-sec > ul > li.inner-child.active > a {
      color: #26ae61;
      }
      .tree_widget-sec > ul > li.inner-child.active > a i {
      color: #26ae61;}
      .contact-edit .srch-lctn:hover {
      background: #26ae61;
      color: #ffffff;
      border-color: #26ae61;
      }
      .contact-edit .srch-lctn {
      color: #26ae61;
      border: 2px solid #26ae61;
      border: 2px solid #26ae61;
      }
      .contact-edit > form button{
      border: 2px solid #26ae61;}
      .profile-form-edit > form button:hover, .contact-edit > form button:hover {
      background: #26ae61;
      color: #ffffff;
      }
      .step.active i {
      background: #26ae61;
      border-color: #26ae61;
      color: #ffffff;
      }
      .step i{
      color: #26ae61;}
      .step.active span {
      color: #000;
      }
      .menu-sec nav > ul > li.menu-item-has-children > a::before{
      display:none;}
      .inner-header {
      float: left;
      width: 100%;
      position: relative;
      padding-top: 60px; padding-bottom: 15px;
      z-index: 0;
      }
      .manage-jobs-sec > table thead tr td{
      color: #26ae61;}
      .extra-job-info > span i {
      float: left;
      font-size: 30px;
      color: #26ae61;
      width: 30px;
      margin-right: 12px;
      }
      .action_job > li span {
      background: #26ae61;}
      .action_job > li span::before{
      background: #26ae61;}
      .action_job > li {
      float: left;
      margin: 0;
      position: relative;
      width: 15px;
      }
      .manage-jobs-sec > h3 {
      padding-left: 0px; 
      margin-top: 40px;
      text-align: center;
      }
      .fall {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 23px 10px;
      }
      .progress-bar{
      background-color: #47b476;
      }
      .details {
      float: left;
      width: 100%;
      padding: 5px 17px;
      background-color: #47b476;
      color: #fff;
      margin: 15px;
      border-radius: 20px
      }
      .leftdetails {
      float: left;
      width: 100%;
      }
      .left {
      float: left;
      width: 100%;
      margin: 0px;
      padding: 0px;
      }
      .detail span {
      display: block;
      float: right;
      margin: 12px 8px;  
      border: 1px solid#ddd;
      padding: 10px;
      }
      .detail strong {
      float: left;
      width: 100%;
      margin: 0px;
      padding: 0px 55px;
      font-weight: 700;
      font-size: 20px;
      }
      .detail, .detai {
      float: left;
      width: 100%;
      }
      .detai span {
      display: -webkit-box;
      width: 100%;
      font-weight: 600;
      margin: 0px;
      padding: 12px 0px;
      font-size: 20px;
      }
      .progras {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 10px;
      }
      .detai strong {
      float: right;
      font-size: 20px;
      }
      .detail p, .detai p {
      font-weight: 600;
      font-size: 16px;
      color: #000;
      }
      .detail .progress span {
      position: absolute;
      right: 0px;
      font-size: 13px;
      color: #fff;
      border: 0px solid#ddd;
      }
      .head {
      float: left;
      width: 100%;
      padding: 10px;
      background-color: #47b476;
      color: #fff;
      margin-top: 10px;
      margin-bottom: 10px;
      }
      .num {
      float: left;
      margin: 20px 0px;
      }
      .para {
      float: left;
      width: 100%;
      border: 1px solid#dddd;
      padding: 10px;
      background-color: #47b476;
      color: #fff;
      margin-top: 10px;
      margin-bottom: 10px;
      }
      .tech {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 10px;
      margin: 10px 0px;
      }
      .search-container {
      float: left;
      }
      .search button {
      padding: 7px 4px;
      color: #fff;
      background-color: #47b476;
      border: none;
      width: 36%;
      }
      .upload a {
      color: #ffffff;
      text-decoration: none;
      background: #47b476;
      padding: 10px;
      border-radius: 50px;
      }
      .search input[type="text"] {
      padding: 9px 6px;
      width: 63%;
      background-color: #fff;
      border: 1px solid#ddd;
      }
      .details i.fa.fa-filter {
      float: right;
      margin: 5px 0px;
      padding: 0px;
      }
      .details p {
      float: left;
      margin: 0px;
      padding: 0px;
      color: #fff;
      }
      .head p {
      color: #fff;
      }
      .para p {
      color: #fff;
      }
      .upload {
      float: right;
      padding: 8px;
      margin: px;
      }
      border: 1px solid#ddd;
      padding: 10px;
      }
      .dot {
      float: right;
      }
      .progr {
      float: left;
      width: 100%;
      margin-bottom: 18px;
      }
      .progr .progress {
      position: absolute;
      left: 9%;
      height: 20px;
      -webkit-border-radius: 8px;
      -moz-border-radius: 8px;
      -ms-border-radius: 8px;
      -o-border-radius: 8px;
      border-radius: 8px;
      top: initial;
      width: 80%;
      }
      .progr .progress-label {
      margin: 12px 0px;
      }
      .progress-bar.progress-bar-primary {
      padding: 0px;
      }
      .texts p {
      position: absolute;
      top: 70%;
      left: 29%;
      }
      p.dropdown-toggle {
      font-size: 34px;
      margin: -30px;
      }
      .dot {
      float: right;
      }
      .texts {
      margin-top: 20px;
      }
      .text p {
      margin: 0px;
      padding: 0px;
      font-size: 13px;
      }
      .right .progress{
      width: 150px;
      height: 150px;
      line-height: 150px;
      background: none;
      margin: 0 auto;
      box-shadow: none;
      position: relative;
      }
      .right .progress:after{
      content: "";
      width: 100%;
      height: 100%;
      border-radius: 50%;
      border: 2px solid #fff;
      position: absolute;
      top: 0;
      left: 0;
      }
      .right .progress > span{
      width: 50%;
      height: 100%;
      overflow: hidden;
      position: absolute;
      top: 0;
      z-index: 1;
      }
      .progress .progress-left{
      left: 0;
      }
      .right .progress .progress-bar{
      width: 100%;
      height: 100%;
      background: none;
      border-width: 2px;
      border-style: solid;
      position: absolute;
      top: 0;
      }
      .right .progress .progress-left .progress-bar{
      left: 100%;
      border-top-right-radius: 80px;
      border-bottom-right-radius: 80px; 
      border-left: 0;
      -webkit-transform-origin: center left;
      transform-origin: center left;
      }
      .right .progress .progress-right{
      right: 0;
      }
      .right .progress .progress-right .progress-bar{
      left: -100%;
      border-top-left-radius: 80px;
      border-bottom-left-radius: 80px;
      border-right: 0;
      -webkit-transform-origin: center right;
      transform-origin: center right;
      animation: loading-1 1.8s linear forwards;
      }
      .right .progress .progress-value{
      width: 59%;
      height: 59%;
      border-radius: 50%;
      border: 2px solid #ebebeb;
      font-size: 28px;
      line-height: 82px;
      text-align: center;
      position: absolute;
      top: 7.5%;
      left: 7.5%;
      }
      .right .progress.blue .progress-bar{
      border-color: #049dff;
      }
      .right .progress.blue .progress-value{
      color: #47b476;
      }
      .right .progress.blue .progress-left .progress-bar{
      animation: loading-2 1.5s linear forwards 1.8s;
      }
      .progress.yellow .progress-bar{
      border-color: #fdba04;
      }
      .right .progress.yellow .progress-value{
      color: #fdba04;
      }
      .right .progress.yellow .progress-left .progress-bar{
      animation: loading-3 1s linear forwards 1.8s;
      }
      .right .progress.pink .progress-bar{
      border-color: #ed687c;
      }
      .right .progress.pink .progress-value{
      color: #ed687c;
      }
      .right .progress.pink .progress-left .progress-bar{
      animation: loading-4 0.4s linear forwards 1.8s;
      }
      .right .progress.green .progress-bar{
      border-color: #1abc9c;
      }
      .right .progress.green .progress-value{
      color: #1abc9c;
      }
      .right .progress.green .progress-left .progress-bar{
      animation: loading-5 1.2s linear forwards 1.8s;
      }
      @keyframes loading-1{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(180deg);
      transform: rotate(180deg);
      }
      }
      @keyframes loading-2{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(144deg);
      transform: rotate(144deg);
      }
      }
      @keyframes loading-3{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(90deg);
      transform: rotate(90deg);
      }
      }
      @keyframes loading-4{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(36deg);
      transform: rotate(36deg);
      }
      }
      @keyframes loading-5{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(126deg);
      transform: rotate(126deg);
      }
      }
      @media only screen and (max-width: 990px){
      .right .progress{ margin-bottom: 20px; }
      }
      .shoropnt1 ul li{float:left;}
      .shoropnt1 ul li label{font-size: 13px;}
      .jobidsclassshide span{float: left;}
      .jobidsclassshide .jobsid {
      border: 1px solid #c0c0c0!important;
      padding: 5px!important;
      width: auto!important;
      float: left!important;
      }
     
      .statuspanel button{margin-right: 25px;}
      .modal-backdrop{    position: relative!important;    z-index: 0!important;}
      .managecandi .emply-resume-list.hcr a:hover {
            color: #000;
            }
      .detailscan.jobdesctxr{ border-top: 1px solid #ddd; padding: 6px 0 0 0; flex: 0 0 95%; max-width: 95%; margin: auto;}
.detailscan.jobdesctxr p{ width: 25%; float: left; font-size: 14px;}
.detailscan.jobdesctxr p strong{ margin: 0}
.detailscan h3 { margin: 0 0 10px 0; font-weight: 600; }
.CandidateBox p{ font-size: 13.5px; margin: 0 0 4px; line-height: 22px; padding: 0 0 0 25px; position: relative;}
.CandidateBox p span{ position: absolute; top: 0; left: 0; color: #353535; font-size: 13px;}
.widget.aplreset { margin: 10px 0 10px; }
h3.sb-title.open, .widget > h3 { margin: 10px 0 0; }
.specialism_widget input[type="text"]{ margin: 0px  }
.managecandi .emply-resume-list.hcr a:hover{color:#feab28}
.managecandi .emply-resume-list.hcr a span{background-color:#e4e4e4;padding:5px 11px;display:inline-block;color:#000;border:1px solid #ccc;font-size:10px;border-radius:2px;margin:0 0 0 10px}
.managecandi .emply-resume-list.hcr a span:hover{background-color:#feab28;border-color:#feab28}
.simple-checkbox select { 
    height: auto !important;
}

.statuspanel p.select_candidate+p{ margin-bottom: 10px }

.statuspanel p {margin-bottom: 10px!important}
   </style>
   <body>
      <div class="theme-layout" id="scrollup">
         <header class="stick-top nohdascr">
            <div class="menu-sec">
               <div class="container-fluid dasboardareas">
                 
                  <!-- Logo -->
                  <?php include_once('headermenu.php');?>
                  <!-- Menus -->
               </div>
            </div>
         </header>
         <section>
            <div class="block no-padding">
               <div class="container-fluid dasboardareas">
                  <div class="row">
                     <aside class="col-md-3 col-lg-2 column border-right Sidenavbar">
                        <div class="widget">
                           <div class="menuinside">
                              <?php include_once('sidebar.php'); ?>
                           </div>
                        </div>
                     </aside>
                     <div class="col-md-9 col-lg-10 psthbs MainWrapper">
                        <div class="row">
                           <div class="col-md-12 col-lg-9 managecandi ">
                              <div class="padding-left">
                                 <div class="emply-resume-sec">
                                    <?php
                                       if($candidatesApplied1) {
                                           $xx= 1;
                                         foreach($candidatesApplied1 as $applied) {
                                                
                                                if($applied['status'] == 3){} else {

                                                $questSaved = $this->Questionnaire_Model->questionnaire_user_single_limit($applied['jobpost_id'], $applied['user_id']);
                                    ?>    
                                    <div class="emply-resume-list hcr">
                                       <div class="row">
                                          <div class="col-md-2">
                                             <div class="imgs-thumbsd">
                                                <?php
                                                   if(@getimagesize($applied['profilePic'])) {
                                                   ?>
                                                   <img src="<?php echo $applied['profilePic']; ?>" alt="" style="width:100%;">
                                                
                                                <?php
                                                   } else {
                                                   ?>
                                                   <img src="<?php echo base_url().'recruiterfiles/';?>images/man.png" alt="" style="width:100%;">   
                                                <?php
                                                   }
                                                   ?> 
                                             </div>
                                          </div>
                                          <div class="col-md-10 detailscan">
                                             
                                             <div class="row">
                                                <div class="col-md-8">
                                                      <div class="CandidateBox"> 

                                                            <h3>
                                                                  <a href="<?php echo base_url();?>recruiter/candidate/singleCandidates?id=<?php echo base64_encode($applied['user_id']); ?>" title=""><?php echo $applied['name']; ?>
                                                                       <span>View profile</span> 
                                                                  </a>
                                                                  <?php if($applied['chatbot'] == 1) { ?>
                                                                  <a href="<?php echo base_url();?>recruiter/questionnaire_user_view/<?php echo base64_encode($applied['jobpost_id']); ?>/<?php echo base64_encode($applied['user_id']); ?>" title="" target="_blank">
                                                                       <span>Chatbot</span> 
                                                                  </a>
                                                                  <?php
                                                                        }
                                                                  ?>
                                                            </h3>

                                                            <p>
                                                                  <span><i class="fa fa-envelope" aria-hidden="true"></i></span><?php echo $applied['email']; ?>
                                                            </p>

                                                            <?php if($applied['phone']!=0){ ?>
                                                            <p><span style="transform: rotate(10deg);
"><i class="fa fa-phone" aria-hidden="true"></i></span><?php if($applied['phone']!=0){ ?><a href="tel:<?php echo $applied['country_code'].' '. $applied['phone']; ?>"><?php echo $applied['country_code'].' '.$applied['phone']; ?></a><?php }?></p>
                                                            <?php }?>
                                                            <?php if(!empty($applied['location'])){ ?>

                                                            <p><span><i class="fa fa-map-marker" aria-hidden="true"></i></span><?php echo $applied['location']; ?></p>
                                                            <?php }?>
                                                      </div>


                                                      
                                                   <!-- <span class="icotxtsd"><i class="fa fa-envelope" aria-hidden="true"></i><?php echo $applied['email']; ?></span>
                                                   <?php if($applied['phone']!=0){ ?>
                                                   <span class="icotxtsd pincts"><i class="fa fa-phone" aria-hidden="true"></i><?php if($applied['phone']!=0){ ?><a href="tel:<?php echo $applied['country_code'].' '. $applied['phone']; ?>"><?php echo $applied['country_code'].' '.$applied['phone']; ?></a><?php }?></span>
                                                   <?php }?>
                                                   <?php if(!empty($applied['location'])){ ?>
                                                   <span class="icotxtsd"><i class="fa fa-map-marker" aria-hidden="true"></i><?php echo $applied['location']; ?></span>
                                                   <?php }?> -->



                                                </div>
                                                <div class="col-md-4">
                                                   <?php if($applied['status'] == 7){ ?>   
                                                   
                                                  <?php } else{?>
                                                   <p class="filtsts" data-toggle="modal" data-target="#myModal<?php echo $xx.$xx;?>"><i class="fas fa-edit"></i> Change Status</p>
                                                   <?php }?>
                                                  
                                                <div class="statuspanel"> 
                                                   <p class="select_candidate"><i class="la la-check"></i>
                                                      <?php if($applied['status'] == 1){ echo "New Application"; } ?>
                                                      <?php if($applied['status'] == 2){ echo "No Show"; } ?>
                                                      <?php if($applied['status'] == 3){ echo "Fall Out"; } ?>
                                                      <?php if($applied['status'] == 4){ echo "Refer"; } ?>
                                                      <?php if($applied['status'] == 5){ echo "On Going Application"; } ?>
                                                      <?php if($applied['status'] == 6){ echo "Accepted JO"; } ?>
                                                      <?php if($applied['status'] == 7){ echo "Hired"; } ?>
                                                      <?php if($applied['status'] == 8){ echo "Rechedule"; } ?>
                                                   </p>
                                                   

                                                   <?php if($applied['status'] == 1) { ?>
                                                      <p><b>Applied Date: </b><?php if(!empty($applied['interviewdate'])){ echo date('F d,Y', strtotime($applied['interviewdate'])); }?></p>
                                                   <?php } else if($applied['status'] == 6) { ?>  

                                                   <p><?php if(strtotime($applied['date_day1'])!='0000-00-00'){ echo  date('F d,Y', strtotime($applied['date_day1'])); }else{ echo "";}?></p>
                                                   <?php } else {?>
                                                            <p><?php if(!empty($applied['interviewdate'])){ echo  date('F d,Y', strtotime($applied['interviewdate'])); }?></p>
                                                   <?php } ?>
                                                   <p> Interview Mode: <?php echo $applied['mode'];?> </p>
                                                   
                                                   <p> Chabot Status : <?php if(isset($questSaved)) { ?>
                                                      <?php if($questSaved[0]['result'] == "recruiter review") { ?>
                                                            <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal<?php echo $xx.$xx;?>"> <?php echo $questSaved[0]['result']; ?> </a>
                                                      <?php
                                                            } else {
                                                                  echo $questSaved[0]['result']; 
                                                            }
                                                            }  
                                                      ?> 
                                                   </p>

                                                </div>
                                                </div>
                                                <!-- The Modal -->
                                         <div class="modal" id="myModal<?php echo $xx.$xx;?>">
                                            <div class="modal-dialog modal-dialog-centered bd-example-modal-lg statuspopchang" style="margin-top: 3%;">
                                               <div class="modal-content">
                                                  <!-- Modal Header -->
                                                  <div class="modal-header">
                                                     <h4 class="modal-title">Change Status</h4>
                                                     
                                                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                  </div>
                                                  <!-- Modal body -->
                                                  <div class="modal-body">
                                                     <div class="filterstatsd">
                                                        <div class="">
                                                           <form method="post" action="<?php echo base_url();?>recruiter/candidate/jobstatuschange">
                                                              <div class="shoropnt1">
                                                                 <div class="row">
                                                                    <div class="col-md-12">
                                                                       <p class="remember-label"></p>
                                                                       <input type="hidden" name="recruiter_id" value="<?php if(!empty($applied['recruiter_id'])){ echo $applied['recruiter_id'];} ?>">    
                                                                       <input type="hidden" name="jobtitle" value="<?php if(!empty($applied['jobtitle'])){ echo $applied['jobtitle']; }  ?>">
                                                                    </div>
                                                                 </div>
                                                                 <div class="row">
                                                                    <div class="col-md-12">
                                                                       <ul>
                                                                          <li>
                                                                             <input type="Radio" onclick="myfun(this.value, '<?php echo $xx.$xx.$xx;?>')" class="<?php echo $xx.$xx;?>" name="statusradio" value="1" id="ab<?php echo $xx;?>" <?php if($applied['status'] == 1){ echo "checked"; } ?>> <label for="ab<?php echo $xx;?>">New Application</label>
                                                                          </li>
                                                                          <li>
                                                                             <input type="Radio" onclick="myfun(this.value,'<?php echo $xx.$xx.$xx;?>')" class="<?php echo $xx.$xx;?>" name="statusradio" value="2" id="cd<?php echo $xx;?>" <?php if($applied['status'] == 2){ echo "checked"; } ?>><label for="cd<?php echo $xx;?>">No Show</label>
                                                                          </li>
                                                                          <li>
                                                                             <input type="Radio" onclick="myfun(this.value,'<?php echo $xx.$xx.$xx;?>')" name="statusradio" value="3" class="<?php echo $xx.$xx;?>" id="ef<?php echo $xx;?>" <?php if($applied['status'] == 3){ echo "checked"; } ?>><label for="ef<?php echo $xx;?>"> Fall Out </label>
                                                                          </li>
                                                                          <li>
                                                                             <input type="Radio" onclick="myfun(this.value,'<?php echo $xx.$xx.$xx;?>')" name="statusradio" value="4" class="<?php echo $xx.$xx;?>" id="gh<?php echo $xx;?>" <?php if($applied['status'] == 4){ echo "checked"; } ?>><label for="gh<?php echo $xx;?>"> Refer </label>
                                                                          </li>
                                                                          <li>
                                                                             <input type="Radio" onclick="myfun(this.value,'<?php echo $xx.$xx.$xx;?>')" name="statusradio" value="5" class="<?php echo $xx.$xx;?>" id="ij<?php echo $xx;?>" <?php if($applied['status'] == 5){ echo "checked"; } ?>><label for="ij<?php echo $xx;?>"> On Going Application </label>
                                                                          </li>
                                                                          <li>
                                                                             <input type="Radio" onclick="myfun(this.value,'<?php echo $xx.$xx.$xx;?>')" name="statusradio" class="<?php echo $xx.$xx;?>" value="6" id="kl<?php echo $xx;?>" <?php if($applied['status'] == 6){ echo "checked"; } ?>><label for="kl<?php echo $xx;?>"> Accepted JO </label>
                                                                          </li>
                                                                          <li>
                                                                             <input type="Radio" onclick="myfun(this.value,'<?php echo $xx.$xx.$xx;?>')" name="statusradio" class="<?php echo $xx.$xx;?>" value="7" id="mn<?php echo $xx;?>" <?php if($applied['status'] == 7){ echo "checked"; } ?>><label for="mn<?php echo $xx;?>"> Hired </label>
                                                                          </li>
                                                                       </ul>
                                                                    </div>
                                                                 </div>
                                                                 <div style="clear:both;"></div>
                                                                 <div class="row fallouthide1 falloutshow<?php echo $xx.$xx.$xx;?>" style="display:none;">
                                                                    <div class="col-md-12">
                                                                       <span class="reasonout"> Fall Out Reason : </span>
                                                                       <select name="falloutreason" class="reasonout1" id="fall<?php echo $xx.$xx;?>">
                                                                          <option value=""> Select Reason </option>
                                                                          <!-- <option value="1"> No show </option> -->
                                                                          <option value="2"> Did not meet requirement </option>
                                                                          <option value="3"> Did not Accept Job Offers </option>
                                                                          <!-- <option value="4"> Refer to another job listing </option> -->
                                                                          <option value="5"> Day 1 No Show </option>
                                                                       </select>
                                                                    </div>
                                                                    <span id="fallerr<?php echo $xx.$xx;?>" style="color:red;"></span>
                                                                 </div>
                                                                 <div class="row jobidsclassshide jobidsclasss<?php echo $xx.$xx.$xx;?>" style="display:none;">
                                                                    <div class="col-md-12">
                                                                    <div class="valdydas">
                                                                       <span> Enter Job ID</span>
                                                                       <input type="text" name="jobid" id="job<?php echo $xx.$xx;?>" class="jobsid" >
                                                                        <span  class="errydg" id="validerr<?php echo $xx.$xx;?>" style="color:red;"></span>
                                                                  </div>
                                                                    </div>
                                                                   
                                                                 </div>
                                                                 
                                                                 <!-- <div class="row ongoinghide ongoinghideshow<?php echo $xx.$xx.$xx;?>" style="display:none;">
                                                                    <div class="col-md-12">
                                                                       <span class="reasonout"> On Going Status : </span>
                                                                       <select name="goingreason" class="reasonout1" id="ongo<?php echo $xx.$xx;?>">
                                                                          <option value=""> Select Reason </option>
                                                                          <option value="1"> Passes initial interview </option>
                                                                          <option value="2"> Did not meet requirement </option>
                                                                       </select>
                                                                       
                                                                    </div>
                                                                    <span id="goerr<?php echo $xx.$xx;?>" style="color:red;"></span>
                                                                 </div> -->
                                                                 <div class="row acceptedhide acceptedeshow<?php echo $xx.$xx.$xx;?>" style="display:none;">
                                                                    <div class="col-md-12">
                                                                      <!--  <span class="reasonout"> Accepted JO Status : </span>
                                                                       <select name="acceptedreason" class="reasonout1" id="ajo<?php echo $xx.$xx;?>">
                                                                          <option value=""> Select Reason </option>
                                                                          <option value="1"> Schedule of Day 1 </option>
                                                                           <option value="2"> Rescheduled day JO </option>
                                                                       </select>
 -->
                                                                       <!-- <span class="reasonout"> Enter Schedule of Day 1: </span> -->
                                                                       <input type="text" class="schedule_date" name="schedule_date" id="schedule_date<?php echo $xx.$xx;?>" placeholder="Enter Schedule of Day1" style="    border: 1px solid #000; padding: 11px 0px; width: 46%; margin-top: 15px;">
                                                                        <span id="ajoerr<?php echo $xx.$xx;?>" style="color:red;"></span>
                                                                    </div>
                                                                    
                                                                   
                                                                    
                                                                 </div>
                                                                 <div style="clear:both;"></div>
                                                                 <div class="row">
                                                                    <div class="col-md-12 statuspanel">
                                                                       <p class="select_candidate"><i class="la la-check">
                                                                          </i>
                                                                          <?php if($applied['status'] == 1){ echo "New Application"; } ?>
                                                                          <?php if($applied['status'] == 2){ echo "No Show"; } ?>
                                                                          <?php if($applied['status'] == 3){ echo "Fall Out"; } ?>
                                                                          <?php if($applied['status'] == 4){ echo "Refer"; } ?>
                                                                          <?php if($applied['status'] == 5){ echo "On Going Application"; } ?>
                                                                          <?php if($applied['status'] == 6){ echo "Accepted JO"; } ?>
                                                                          <?php if($applied['status'] == 7){ echo "Hired"; } ?>
                                                                          <?php if($applied['status'] == 8){ echo "Rechedule"; } ?>
                                                                       </p>
                                                                       <input type="hidden" name="appid" value="<?php echo $applied['id']; ?>">
                                                                       <input type="hidden" name="interview_date" value="<?php echo $applied['interviewdate']; ?>">
                                                                       <input type="hidden" name="interview_time" value="<?php echo $applied['interviewtime']; ?>">
                                                                       <input type="hidden" name="jid" value="<?php if(!empty($applied['jobpost_id'])){ echo $applied['jobpost_id']; }?>">
                                                                       <input type="hidden" name="uid" value="<?php echo $applied['user_id']; ?>">
                                                                       <button id="submitchangebtn<?php echo $xx.$xx;?>" type="button" class="statusbutton" data="<?php echo $xx.$xx;?>">Change Status </button>
                                                                    </div>
                                                                 </div>
                                                              </div>
                                                           </form>
                                                        </div>
                                                     </div>
                                                  </div>
                                               </div>
                                            </div>
                                         </div>
                                             </div>
                                             <div class="row">
                                                
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                </div>
                                                <div class="col-md-6">
                                                   <?php if(isset($applied['resume'])) {?>
                                                   <p><i class="fa fa-picture-o" aria-hidden="true"></i><a href="<?php echo $applied['resume']; ?>" target="_blank">Attached resume</a></p>
                                                   <?php }?>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-md-12 detailscan jobdesctxr">
                                             <!-- <p><?php echo $applied['jobDesc']; ?></p> -->
                                             <p><strong><?php if(!empty($applied['jobtitle'])){ echo $applied['jobtitle']; }?></strong></p>
                                             <p>Job ID: <?php if(!empty($applied['jobpost_id'])){ echo $applied['jobpost_id']; }?></p>
                                             <p><?php if(!empty($applied['cname'])){ echo $applied['cname']; }?></p>
                                          </div>
                                       </div>
                                    </div>
                                   
                                        <script type="text/javascript">

                                                $('.statusbutton').click(function(){
                                                      var single_var = $(this).attr('data');
                                                      if($("."+single_var+":checked").val() == '3'){
                                                            if($("#fall"+single_var).val() == ''){
                                                               $("#fallerr"+single_var).text("Select at least one reason");   
                                                            } else {
                                                             $("#submitchangebtn"+single_var).attr('type','submit');
                                                            }
                                                      }else if($("."+single_var+":checked").val() == '4'){
                                                            if($("#job"+single_var).val() == ''){
                                                               $("#validerr"+single_var).text("Job Id is required");   
                                                            } else {
                                                                  var job_id = $("#job"+single_var).val();
                                                                  var url = "<?php echo base_url() ?>recruiter/Candidate/checkjobid";
                                                                   $.ajax({
                                                                         type:"POST",
                                                                         url:url,
                                                                         data:{
                                                                             job_id : job_id
                                                                         },

                                                                         success:function(data1)

                                                                          {
                                                                              //alert(data1.length);
                                                                              if(data1.length==6){
                                                                                    //alert(data) ;
                                                                                    $("#submitchangebtn"+single_var).attr('type','submit');
                                                                              }else{
                                                                                   $("#validerr"+single_var).text("Please Enter Correct Job Id"); 
                                                                              }
                                                                          // alert(data);
                                                                           
                                                                        //$("#submitchangebtn"+single_var).attr('type','submit');
                                                                          }
                                                                   });
                                                             
                                                            }
                                                      } /*else if($("."+single_var+":checked").val() == '5'){
                                                            if($("#ongo"+single_var).val() == ''){
                                                               $("#goerr"+single_var).text("Select at least one reason");   
                                                            } else {
                                                             $("#submitchangebtn"+single_var).attr('type','submit');
                                                            }
                                                      }*/else if($("."+single_var+":checked").val() == '6'){
                                                            /*if($("#ajo"+single_var).val() == ''){
                                                               $("#ajoerr"+single_var).text("Select at least one reason");   
                                                            }else*/ if($("#schedule_date"+single_var).val() == ''){
                                                                  $("#ajoerr"+single_var).text("Please enter date"); 
                                                            } else {

                                                             $("#submitchangebtn"+single_var).attr('type','submit');
                                                            }
                                                      } else {
                                                            $("#submitchangebtn"+single_var).attr('type','submit');
                                                      }

                                                      // var jobid = $('#job<?php echo $xx.$xx.$xx;?>').val();
                                                      // var radio = $('input[name="statusradio"]:checked').val();
                                                      // //alert(radio);
                                                      // if(radio=='4' && jobid==''){
                                                      //       $('.validerr').css('display','block');
                                                      //       $('.validerr').css('color','red');
                                                      //       return false;
                                                      // }
                                                      //return false;
                                                       
                                                });
                                          </script>
                                    <?php
                                       $xx++; }}
                                       } else {
                                    ?>
                                      <center> <h4>No Data Found</h4></center>
                                    <?php
                                       }
                                    ?>    
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-3  filtersnavs">
                              <form method="post" action="<?php echo base_url();?>recruiter/filtermanagecandidate">
							  <div class="filrgthda">
							  <h3>Filters</h3>
							  </div>
							  
                                 <div class="widget aplreset">
                                    <button type="submit">Apply</button>
                                    <a href="<?php echo base_url();?>recruiter/candidate/manageCandidates" class="resetall">Reset All</a>
                                 </div>
                                 <div class="widget">
                                 </div>
                                  <div class="widget">
                                    <h3 class="sb-title open">Candidate Name</h3>
                                    <div class="specialism_widget">
                                       <div class="">
                                          <input type="text" name="candidate_name" value="<?php if(!empty($filterdata)){if(!empty($filterdata['candidate_name'])){echo $filterdata['candidate_name'];}} ?>" autocomplete="off" >
                                       </div>
                                    </div>
                                 </div>

                                 <div class="widget">
                                    <h3 class="sb-title open">Job Title</h3>
                                    <div class="specialism_widget">
                                       <div class="">
                                          <input type="text" name="job_title" value="<?php if(!empty($filterdata)){if(!empty($filterdata['job_title'])){echo $filterdata['job_title'];}} ?>" >
                                       </div>
                                    </div>
                                 </div>
                                 <div class="widget">
                                    <h3 class="sb-title open">Site Name</h3>
                                    <div class="specialism_widget">
                                       <div class="simple-checkbox ">
                                          <select name="site_name">
                                             <option value=""> Select </option>
                                             <?php
                                                foreach($site_name as $sites) {
                                              ?>
                                             <option value="<?php echo $sites['id'];?>" <?php if(!empty($filterdata)){if(!empty($filterdata['site_name'])){if($filterdata['site_name'] == $sites['id']){echo "selected";}}} ?>> <?php echo $sites['cname'];?> </option>
                                             <?php
                                                }
                                                ?>
                                          </select>
                                       </div>
                                    </div>
                                 </div><div class="widget">
                                    <h3 class="sb-title open">Application Status</h3>
                                    <div class="specialism_widget">
                                       <div class="simple-checkbox ">
                                          <select name="app_status">
                                             <option value=""> Select </option>
                                             <option value="1" <?php if(!empty($filterdata['app_status']) && $filterdata['app_status']=='1'){ echo "selected";} ?>>New Application</option>
                                             <option value="2" <?php if(!empty($filterdata['app_status']) && $filterdata['app_status']=='2'){ echo "selected";} ?>>No Show</option>
                                             <option value="3" <?php if(!empty($filterdata['app_status']) && $filterdata['app_status']=='3'){ echo "selected";} ?>>Fall Out</option>
                                             <option value="4" <?php if(!empty($filterdata['app_status']) && $filterdata['app_status']=='4'){ echo "selected";} ?>>Refer</option>
                                             <option value="5" <?php if(!empty($filterdata['app_status']) && $filterdata['app_status']=='5'){ echo "selected";} ?>>On Going Application</option>
                                             <option value="6" <?php if(!empty($filterdata['app_status']) && $filterdata['app_status']=='6'){ echo "selected";} ?>>Accepted JO</option>
                                             <option value="7" <?php if(!empty($filterdata['app_status']) && $filterdata['app_status']=='7'){ echo "selected";} ?>>Hired</option>
                                          </select>
                                       </div>
                                    </div> 
                                 </div><div class="widget">
                                    <h3 class="sb-title open">Jobs Posted By</h3>
                                    <div class="specialism_widget">
                                       <div class="">
                                          <input type="text" name="job_posted" value="<?php if(!empty($filterdata['job_posted'])){echo $filterdata['job_posted'];} ?>" >
                                       </div>
                                    </div>
                                 </div>
                                 <!-- <div class="widget">
                                    <h3 class="sb-title open">Job Level</h3>
                                    <div class="specialism_widget">
                                       <div class="simple-checkbox ">
                                          <select name="level">
                                             <option value=""> Select </option>
                                             <?php
                                                foreach($levels as $level) {
                                              ?>
                                             <option value="<?php echo $level['id'];?>" <?php if(!empty($filterdata)){if(!empty($filterdata['level'])){if($filterdata['level'] == $level['id']){echo "selected";}}} ?>> <?php echo $level['level'];?> </option>
                                             <?php
                                                }
                                                ?>
                                          </select>
                                       </div>
                                    </div>
                                 </div> -->
                                 <!-- <div class="widget">
                                    <h3 class="sb-title open">Job Category</h3>
                                    <div class="specialism_widget">
                                       <div class="simple-checkbox ">
                                          <select name="category" id="category">
                                                      <option value=""> Select Category </option>
                                                      <?php
                                                         if($category) {
                                                             foreach($category as $categorys) {
                                                         ?>
                                                      <option value="<?php echo $categorys['id']; ?>" <?php if(!empty($filterdata['category'])){ if($filterdata['category'] == $categorys['id']) {echo "selected";}} ?>> <?php echo $categorys['category']; ?> </option>
                                                      <?php
                                                         }
                                                         }
                                                         ?>
                                                   </select>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="widget">
                                    <h3 class="sb-title open">Job Subcategory</h3>
                                    <div class="specialism_widget">
                                       <div class="simple-checkbox ">
                                          <select name="subcategory" id="subcategory">
                                             <option value=""> Select </option>
                                             
                                             
                                          </select>
                                       </div>
                                    </div>
                                 </div> -->
                                 <div class="widget">
                                    <h3 class="sb-title open">Education</h3>
                                    <div class="specialism_widget">
                                       <div class="simple-checkbox">
                                          <select name="education">
                                             <option value=""> Select Education Level </option>
                                                      <option value="No Requirement" <?php if(!empty($filterdata['education'])){$filterdata['education'] == 'No Requirement' ? ' selected="selected"' : '';}?>>No Requirement</option>
                                                      <option value="High School Graduate" <?php if(!empty($filterdata['education'])){ $filterdata['education'] == 'High School Graduate' ? ' selected="selected"' : '';}?>>High School Graduate</option>
                                                      <option value="Vocational" <?php if(!empty($filterdata['education'])){ $filterdata['education'] == 'Vocational' ? ' selected="selected"' : '';}?>>Vocational</option>
                                                      <option value="Undergraduate" <?php if(!empty($filterdata['education'])){ $filterdata['education'] == 'Undergraduate' ? ' selected="selected"' : '';}?>>Undergraduate</option>
                                                      <option value="Associate Degree" <?php if(!empty($filterdata['education'])){ $filterdata['education'] == 'Associate Degree' ? ' selected="selected"' : '';}?>>Associate Degree</option>
                                                      <option value="College Graduate" <?php if(!empty($filterdata['education'])){ $filterdata['education'] == 'College Graduate' ? ' selected="selected"' : '';}?>>College Graduate</option>
                                                      <option value="Post Graduate" <?php if(!empty($filterdata['education'])){ $filterdata['education'] == 'Post Graduate' ? ' selected="selected"' : '';}?>>Post Graduate</option>
                                             
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="widget">
                                    <h3 class="sb-title open">Experience</h3>
                                    <div class="specialism_widget">
                                       <div class="simple-checkbox">
                                          <select name="exp">
                                             <option value=""> Select </option>
                                             <option value="All Tenure" <?php if(!empty($filterdata)){if(!empty($filterdata['exp'])){if($filterdata['exp'] == "All Tenure"){echo "selected";}}}?>> All Tenure </option>
                                             <option value="less than 6 months" <?php if(!empty($filterdata)){if(!empty($filterdata['exp'])){if($filterdata['exp'] == "less than 6 months"){echo "selected";}}}?>> less than 6 months </option>
                                             <option value="6mo to 1 yr" <?php if(!empty($filterdata)){if(!empty($filterdata['exp'])){if($filterdata['exp'] == "6mo to 1 yr"){echo "selected";}}}?>> 6mo to 1 yr </option>
                                             <option value="1 yr to 2 yr" <?php if(!empty($filterdata)){if(!empty($filterdata['exp'])){if($filterdata['exp'] == "1 yr to 2 yr"){echo "selected";}}}?>> 1 yr to 2 yr </option>
                                             <option value="2yr to 3 yr" <?php if(!empty($filterdata)){if(!empty($filterdata['exp'])){if($filterdata['exp'] == "2yr to 3 yr"){echo "selected";}}}?>> 2yr to 3 yr </option>
                                             <option value="3yr and up" <?php if(!empty($filterdata)){if(!empty($filterdata['exp'])){if($filterdata['exp'] == "3yr and up"){echo "selected";}}}?>>3yr and up </option>
                                             
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- <div class="widget">
                                    <h3 class="sb-title open">Language</h3>
                                    <div class="specialism_widget">
                                       <div class="simple-checkbox">
                                          <select name="lang">
                                             <option value=""> Select </option>
                                             <?php
                                                foreach($languages as $language) {
                                              ?>
                                             <option value="<?php echo $language['id'];?>" <?php if(!empty($filterdata)){if(!empty($filterdata['lang'])){if($filterdata['lang'] == $language['id']){echo "selected";}}} ?>> <?php echo $language['name'];?> </option>
                                             <?php
                                                }
                                                ?>
                                          </select>
                                       </div>
                                    </div>
                                 </div> -->
                                 <div class="widget">
                                    <h3 class="sb-title open">Clients Supported</h3>
                                    <div class="specialism_widget">
                                       <div class="simple-checkbox">
                                          <select name="clients">
                                             <option value=""> Select </option>
                                             <?php
                                                foreach($clients as $client) {
                                                ?>
                                             <option value="<?php echo $client['clients'];?>" <?php if(!empty($filterdata)){if(!empty($filterdata['clients'])){if($filterdata['clients'] == $client['clients']){echo "selected";}}}?>> <?php echo $client['clients'];?> </option>
                                             <?php
                                                }
                                                ?>
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                                 
                                 <div class="widget">
                                    <h3 class="sb-title open">Industry Expertise</h3>
                                    <div class="specialism_widget">
                                       <div class="simple-checkbox ">
                                          <select name="industry">
                                             <option value=""> Select </option>
                                             <option value="1" <?php if(!empty($filterdata)){if($filterdata['industry']){if($filterdata['industry'] == "1"){echo "selected";}}} ?>> Automotive </option>
                                             <option value="2" <?php if(!empty($filterdata)){if($filterdata['industry']){if($filterdata['industry'] == "2"){echo "selected";}}} ?>> Banking and Financial Services </option>
                                             <option value="3" <?php if(!empty($filterdata)){if($filterdata['industry']){if($filterdata['industry'] == "3"){echo "selected";}}} ?>> Consumer Electronics </option>
                                             <option value="4" <?php if(!empty($filterdata)){if($filterdata['industry']){if($filterdata['industry'] == "4"){echo "selected";}}} ?>> Healthcare and Pharmaceutical </option>
                                             <option value="5" <?php if(!empty($filterdata)){if($filterdata['industry']){if($filterdata['industry'] == "5"){echo "selected";}}} ?>> Media and Communication </option>
                                             <option value="6" <?php if(!empty($filterdata)){if($filterdata['industry']){if($filterdata['industry'] == "6"){echo "selected";}}} ?>> Retail and Ecommerce </option>
                                             <option value="7" <?php if(!empty($filterdata)){if($filterdata['industry']){if($filterdata['industry'] == "7"){echo "selected";}}} ?>> Technology </option>
                                             <option value="8" <?php if(!empty($filterdata)){if($filterdata['industry']){if($filterdata['industry'] == "8"){echo "selected";}}} ?>> Travel, Transportation and Tourism </option>
                                             
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- <div class="widget">
                                    <h3 class="sb-title open">Selected candidate Skills</h3>
                                    <div class="specialism_widget">
                                       <div class="simple-checkbox ">
                                          <select name="skills">
                                             <option value=""> Select </option>
                                             <?php
                                                foreach($skillData as $skillValue) {
                                                ?>
                                             <option value="<?php echo $skillValue['skills'];?>" <?php if($filterdata){if($filterdata['skills']){if($filterdata['skills'] == $skillValue['skills']){echo "selected";}}} ?>> <?php echo $skillValue['skills'];?> </option>
                                             <?php
                                                }
                                                ?>
                                          </select>
                                       </div>
                                    </div>
                                 </div> -->
                                 
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
      </div>
      </section>
      <?php include_once("footer.php");?>
      <?php include_once("modalpassword.php");?>
     
      <script type="text/javascript">
         function myfun(value, class1) {
               var checkval = value;
               var checkval1 = class1;
               $(".fallouthide1").css("display", "none");
               $(".jobidsclassshide").css("display", "none");
               $(".ongoinghide").css("display", "none");
               $(".acceptedhide").css("display", "none");
         
               if(checkval == 3) {    
                   var createclass = ".falloutshow" + checkval1;
                   $(createclass).css("display", "block");
               }
               if(checkval == 4) {
                   var createclass = ".jobidsclasss" + checkval1;
                   $(createclass).css("display", "block");
               }
               if(checkval == 5) {
                   var createclass = ".ongoinghideshow" + checkval1;
                   $(createclass).css("display", "block");
               }
               if(checkval == 6) {
                   var createclass = ".acceptedeshow" + checkval1;
                   $(createclass).css("display", "block");
               }
         }
      </script>

      <script type="text/javascript">
            if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
            }

            $("#category").change(function(){
             //get category value
             var cat_val = $("#category").val();
             //alert(cat_val);
             // put your ajax url here to fetch subcategory
             var url             =   '<?php echo base_url(); ?>/recruiter/Jobpost/fetchSubcategory';
             // call subcategory ajax here 
             $.ajax({
                            type:"POST",
                            url:url,
                            data:{
                                cat_val : cat_val
                            },
         
                            success:function(data)
         
                             {
                              
                           $("#subcategory").html(data);
                        
                             }
                         });
         });
      </script>
 <script type="text/javascript">
         $(function () {
            $(".schedule_date").datepicker({ 
             dateFormat: "yy-mm-dd", 
              minDate: +1
           })
         });
          
    </script>

      <?php include_once("searchmodalscript.php");?>

   </body>
   
</html>


