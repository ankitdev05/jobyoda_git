<?php $recruiterId = $this->session->userdata('userSession');


$userSession = $this->session->userdata('userSession');
if(!empty($userSession['label']) && $userSession['label']=='3'){
   $subrecruiter_id = $userSession['id'];
   $userSession['id'] = $userSession['parent_id'];
}else{
   $userSession['id'] = $userSession['id'];
   $subrecruiter_id = 0;
}
$getSubscription = $this->Subscription_Model->view_existing($userSession['id']);
if($getSubscription) {
   $this->session->set_userdata('userSubscriptionSession', $getSubscription[0]);
} else {
   $this->session->set_userdata('userSubscriptionSession', []);
}
$getPlan = $this->session->userdata('userSubscriptionSession');

?>
<ul>
   <li class="<?php if($_SERVER['REQUEST_URI']=='/jobyoda/recruiter/dashboard'){?> active <?php }?>">
      <a href="<?php echo base_url();?>recruiter/dashboard">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/dashico.png" class="nonhover">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/dashover.png" class="acthover">
         <p>Dashboard</p>
      </a>
   </li>
   
   <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/companyprofile'){?> active <?php }?>">
      <a href="<?php echo base_url();?>recruiter/companyprofile">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/walk.png" class="nonhover">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/walkhover.png" class="acthover">
         <p>Company Profile </p>
      </a>
   </li>

   <!-- <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/subscription_view'){?> active <?php }?>">
      <a href="<?php echo base_url();?>recruiter/subscription_view">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/briefcase.png" class="nonhover">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/briefcasehover.png" class="acthover">
         <p>Subscription</p>
      </a>
   </li> -->
   
   <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/addsubrecruiter' || $_SERVER['REQUEST_URI']=='/recruiter/subrecuriterlist'){?> active <?php }?>">
      <a data-toggle="collapse" href="#collapseRecruiter" role="button" aria-expanded="false" aria-controls="collapseExample" class="collapsed">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/add-rec-2.png" class="nonhover">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/add-rec-1.png" class="acthover">
         <p>Add Recruiters</p>
         <i class="fas fa-plus"></i>
         <i class="fas fa-minus"></i>
      </a>

      <div class="collapse" id="collapseRecruiter">
         <ul class="innersubd">
            <li><a href="<?php echo base_url();?>recruiter/addsubrecruiter">Add Sub-Recruiter</a></li>
            <li><a href="<?php echo base_url();?>recruiter/subrecuriterlist">Sub-Recruiter Listing</a></li>
            <li><a href="<?php echo base_url();?>recruiter/transfer_recruiter">Super User Transfer</a></li>
         </li>   
         </ul>
      </div>
   </li>

   <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/postjob'){?> active <?php }?>">
      <?php
         if(count($getPlan) > 0 && $getPlan['remaining_job_post'] > 0) {
      ?>
         <a href="<?php echo base_url();?>recruiter/postjob">
      <?php
         } else {
      ?>
         <a href="<?php echo base_url();?>recruiter/postjob">
         <!-- <a href="javascript:void(0)" data-toggle="modal" data-target="#planNotifyModal"> -->
      <?php
         }
      ?>
            <img src="<?php echo base_url().'recruiterfiles/';?>images/briefcase.png" class="nonhover">
            <img src="<?php echo base_url().'recruiterfiles/';?>images/briefcasehover.png" class="acthover">
            <p>Post a Job </p>
         </a>
   </li>
   <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/assignjob'){?> active <?php }?>">
      <a href="<?php echo base_url();?>recruiter/assignjob">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/briefcase.png" class="nonhover">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/briefcasehover.png" class="acthover">
         <p> Assign a Job </p>
      </a>
   </li>
   <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/managejobs'){?> active <?php }?>">
      <a href="<?php echo base_url();?>recruiter/managejobs">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/briefcase.png" class="nonhover">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/briefcasehover.png" class="acthover">
         <p>Manage Jobs</p>
      </a>
   </li>

   <!-- <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/managejobs'){?> active <?php }?>">
      <a href="javascript:void(0)" data-toggle="modal" data-target="#searchingModal">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/add-rec-2.png" class="nonhover">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/add-rec-1.png" class="acthover">
         <p>Search Resume</p>
      </a>
   </li> -->

   <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/managecandidates'){?> active <?php }?>">
      <?php
         if(count($getPlan) > 0 && $getPlan['remaining_resume_access'] > 0) {
      ?>
         <a href="<?php echo base_url();?>recruiter/managecandidates">
      <?php
         } else {
      ?>
         <a href="<?php echo base_url();?>recruiter/managecandidates">
         <!-- <a href="javascript:void(0)" data-toggle="modal" data-target="#planNotifyModal"> -->
      <?php
         }
      ?>
         <img src="<?php echo base_url().'recruiterfiles/';?>images/managecandid.png" class="nonhover">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/managecanhover.png" class="acthover">
         <p>Manage Candidates</p>
      </a>
   </li>

<?php
$userSession = $this->session->userdata('userSession');
if(!empty($userSession['label']) && $userSession['label']=='3'){
     $subrecruiter_id = $userSession['id'];
     $userSession['id'] = $userSession['parent_id'];
 }else{
     $userSession['id'] = $userSession['id'];
     $subrecruiter_id = 0;
 }

$pending = $this->Candidate_Model->candidateduenotification_list($userSession['id'],$subrecruiter_id); 
if (!empty($pending)) {
   $totalnotify = count($pending);
} else {
   $totalnotify = 0;
}

?>
   
   
   <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/notification'){?> active <?php }?>">
      <a href="<?php echo base_url();?>recruiter/notification">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/notify.png" class="nonhover">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/notifyhover.png" class="acthover">
         <p>Notification
            <span class="countnoitfy"><?php echo $totalnotify; ?></span>
         </p>
      </a>
   </li>
   <?php if(!empty($recruiterId['label']) && $recruiterId['label']==3){ 
      
         }else{
   ?>
   
   <?php } ?>

   <!-- <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/recruiterprofile'){?> active <?php }?>">
      <a href="<?php echo base_url();?>recruiter/recruiterprofile">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/recprofile.png" class="nonhover"><img src="<?php echo base_url().'recruiterfiles/';?>images/recprofilehover.png" class="acthover">
         <p>Recruiter Profile</p>
      </a>
   </li> -->
   
   <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/rating'){?> active <?php }?>">
      <a href="<?php echo base_url();?>recruiter/rating">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/star.png" class="nonhover"><img src="<?php echo base_url().'recruiterfiles/';?>images/starhover.png" class="acthover">
         <p>Rating & Reviews</p>
      </a>
   </li>

   <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/advertisement' || $_SERVER['REQUEST_URI']=='/recruiter/advertisementlist'){?> active <?php }?>">
      <a data-toggle="collapse" href="#collapsead" role="button" aria-expanded="false" aria-controls="collapseExample" class="collapsed">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/recprofile.png" class="nonhover">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/recprofilehover.png" class="acthover">
         <p>Advertisement Banner</p>
         <i class="fas fa-plus"></i>
         <i class="fas fa-minus"></i>
      </a>

      <div class="collapse" id="collapsead">
         <ul class="innersubd">
            <?php
               if(count($getPlan) > 0 && $getPlan['remaning_advertise'] > 0) {
            ?>
               <li><a href="<?php echo base_url();?>recruiter/advertisement">Add Banner</a></li>
               <li><a href="<?php echo base_url();?>recruiter/advertisementlist">Banner List</a></li>
            <?php
               } else {
            ?>
               <li><a href="<?php echo base_url();?>recruiter/advertisement">Add Banner</a></li>
               <li><a href="<?php echo base_url();?>recruiter/advertisementlist">Banner List</a></li>
               <!-- <li><a href="javascript:void(0)" data-toggle="modal" data-target="#planNotifyModal">Add Banner</a></li>
               <li><a href="javascript:void(0)" data-toggle="modal" data-target="#planNotifyModal">Banner List</a></li> -->
            <?php
               }
            ?>
         </ul>
      </div>
   </li>

   <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/videoadvertisement' || $_SERVER['REQUEST_URI']=='/recruiter/videoadvertisementlist'){?> active <?php }?>">
      <a data-toggle="collapse" href="#collapsead11" role="button" aria-expanded="false" aria-controls="collapseExample" class="collapsed">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/recprofile.png" class="nonhover">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/recprofilehover.png" class="acthover">
         <p>Video Advertisements</p>
         <i class="fas fa-plus"></i>
         <i class="fas fa-minus"></i>
      </a>

      <div class="collapse" id="collapsead11">
         <ul class="innersubd">
            <?php
               if(count($getPlan) > 0 && $getPlan['remaining_video_post'] > 0) {
            ?>
               <li><a href="<?php echo base_url();?>recruiter/videoadvertisement">Add Video Advertisement</a></li>
               <li><a href="<?php echo base_url();?>recruiter/videoadvertisementlist">Video Advertisement List</a></li>
            <?php
               } else {
            ?>
               <li><a href="<?php echo base_url();?>recruiter/videoadvertisement">Add Video Advertisement</a></li>
               <li><a href="<?php echo base_url();?>recruiter/videoadvertisementlist">Video Advertisement List</a></li>
               <!-- <li><a href="javascript:void(0)" data-toggle="modal" data-target="#planNotifyModal">Add Video Advertisement</a></li>
               <li><a href="javascript:void(0)" data-toggle="modal" data-target="#planNotifyModal">Video Advertisement List</a></li> -->
            <?php
               }
            ?>
            
         </ul>
      </div>
   </li>
  
   <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/report' || $_SERVER['REQUEST_URI']=='/recruiter/jobwisereport' || $_SERVER['REQUEST_URI']=='/recruiter/candidatereport'){?> active <?php }?>">
      <a data-toggle="collapse" href="#collapseExample1" role="button" aria-expanded="false" aria-controls="collapseExample" class="collapsed">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/edit.png" class="nonhover">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/edithover.png" class="acthover">
         <p>Reports</p>
         <i class="fas fa-plus"></i>
         <i class="fas fa-minus"></i>
      </a>

      <div class="collapse" id="collapseExample1">
         <ul class="innersubd">
            <li><a href="<?php echo base_url();?>recruiter/report">Hired Report</a></li>
            <li><a href="<?php echo base_url();?>recruiter/jobwisereport">Jobwise C&B Report</a></li>
            <li><a href="<?php echo base_url();?>recruiter/candidatereport">Candidate Report</a></li>
            <li><a href="<?php echo base_url();?>recruiter/screeningreport">Instant screening</a></li>
         </ul>
      </div>
   </li>

   <li>
      <a data-toggle="collapse" href="#collapseExample5" role="button" aria-expanded="false" aria-controls="collapseExample" class="collapsed">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/edit.png" class="nonhover">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/edithover.png" class="acthover">
         <p>Testimonial</p>
         <i class="fas fa-plus"></i>
         <i class="fas fa-minus"></i>
      </a>

      <div class="collapse" id="collapseExample5">
         <ul class="innersubd">
            <li><a href="<?php echo base_url();?>recruiter/testimonialadd">Add Testimonial</a></li> 
            <li><a href="<?php echo base_url();?>recruiter/testimoniallist">Testimonial List</a></li>  
            
         </ul>
      </div>
   </li>
   
   <?php 
      if(!empty($recruiterId['label']) && $recruiterId['label']==3){ 
         if(!empty($recruiterId['invoice_view']) && $recruiterId['invoice_view']==1){
   ?>
            <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/transaction'){?> active <?php }?>">
               <a href="<?php echo base_url();?>recruiter/transaction">
                  <img src="<?php echo base_url().'recruiterfiles/';?>images/transact.png" class="nonhover">
                  <img src="<?php echo base_url().'recruiterfiles/';?>images/transacthover.png" class="acthover">
                  <p>Transaction</p>
               </a>
            </li>
   
   <?php  }
      } else {
   ?>
         <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/transaction'){?> active <?php }?>">
            <a href="<?php echo base_url();?>recruiter/transaction">
               <img src="<?php echo base_url().'recruiterfiles/';?>images/transact.png" class="nonhover">
               <img src="<?php echo base_url().'recruiterfiles/';?>images/transacthover.png" class="acthover">
               <p>Transaction</p>
            </a>
         </li>
   <?php } ?>
   
    
   <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/contact' || $_SERVER['REQUEST_URI']=='/recruiter/faq'){?> active <?php }?>">
      <a data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" class="collapsed">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/mail-1.png" class="nonhover">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/mail-2.png" class="acthover">
         <p>Contact Us</p>
         <i class="fas fa-plus"></i>
         <i class="fas fa-minus"></i>
      </a>

      <div class="collapse" id="collapseExample">
         <ul class="innersubd">
         <li> <a href="<?php echo base_url();?>recruiter/contact">Email Support</a></li>
        <!--  <li><a href="<?php echo base_url();?>recruiter/faq">FAQs</a></li> -->
         </ul>
      </div>
   </li>


<?php
   //if($userSession['id'] == 593 || $userSession['id'] == 618 || $userSession['id'] == 312) {
?>
 
         <?php
            if($userSession['id'] == 593 || $userSession['id'] == 594 || $userSession['id'] == 595) {
         ?>
         <li class="">
            <a href="<?php echo base_url();?>recruiterfiles/downloadresume.xlsx">
               <img src="<?php echo base_url().'recruiterfiles/';?>images/edit.png" class="nonhover">
               <img src="<?php echo base_url().'recruiterfiles/';?>images/edithover.png" class="acthover">
               <p>Download Resume</p>
            </a>
         </li>
         <?php
            } elseif($userSession['id'] == 618 || $userSession['id'] == 687 || $userSession['id'] == 625 || $userSession['id'] == 624 || $userSession['id'] == 623) {
         ?>
         <li class="">
            <a href="<?php echo base_url();?>recruiterfiles/aquire_leads.xlsx">
               <img src="<?php echo base_url().'recruiterfiles/';?>images/edit.png" class="nonhover">
               <img src="<?php echo base_url().'recruiterfiles/';?>images/edithover.png" class="acthover">
               <p>Download Resume</p>
            </a>
         </li>
         <?php
            } elseif($userSession['id'] == 312 || $userSession['id'] == 325 || $userSession['id'] == 326 || $userSession['id'] == 327 || $userSession['id'] == 372 || $userSession['id'] == 373 || $userSession['id'] == 377 || $userSession['id'] == 442 || $userSession['id'] == 608 || $userSession['id'] == 609) {
         ?>
         <li>
            <a href="<?php echo base_url();?>recruiterfiles/USRN_for_HGS.xlsx">
               <img src="<?php echo base_url().'recruiterfiles/';?>images/edit.png" class="nonhover">
               <img src="<?php echo base_url().'recruiterfiles/';?>images/edithover.png" class="acthover">
               <p>Download Resume</p>
            </a>
         </li>
         <?php
            } elseif($userSession['id'] == 750 || $userSession['id'] == 752 || $userSession['id'] == 753 || $userSession['id'] == 755 || $userSession['id'] == 756 || $userSession['id'] == 757 || $userSession['id'] == 758 || $userSession['id'] == 759 || $userSession['id'] == 760 || $userSession['id'] == 761 || $userSession['id'] == 762) {
         ?>
         <li>
            <a href="<?php echo base_url();?>recruiterfiles/sutherland_leads.xlsx">
               <img src="<?php echo base_url().'recruiterfiles/';?>images/edit.png" class="nonhover">
               <img src="<?php echo base_url().'recruiterfiles/';?>images/edithover.png" class="acthover">
               <p>Download Resume</p>
            </a>
         </li>
         <?php
            } elseif($userSession['id'] == 780) {
         ?>
               <li>
                  <a href="<?php echo base_url();?>recruiterfiles/Metro_Manila_TSRs_fresh_TDCX.xlsx">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edit.png" class="nonhover">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edithover.png" class="acthover">
                     <p>Download Metro_Manila_TDCX</p>
                  </a>
               </li>
         <?php
            } elseif($userSession['id'] == 785) {
         ?>
               <li>
                  <a href="<?php echo base_url();?>recruiterfiles/CEBU_TSR_fresh_TDCX.xlsx">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edit.png" class="nonhover">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edithover.png" class="acthover">
                     <p>Download CEBU_TDCX</p>
                  </a>
               </li>
         <?php
            } elseif($userSession['id'] == 488) {
         ?>
               <li>
                  <a href="<?php echo base_url();?>recruiterfiles/Metro_Manila_TSRs_fresh_TDCX.xlsx">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edit.png" class="nonhover">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edithover.png" class="acthover">
                     <p>Download Metro_Manila_TDCX</p>
                  </a>
               </li>

               <li>
                  <a href="<?php echo base_url();?>recruiterfiles/CEBU_TSR_fresh_TDCX.xlsx">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edit.png" class="nonhover">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edithover.png" class="acthover">
                     <p>Download CEBU_TDCX</p>
                  </a>
               </li>

         <?php
            } elseif($userSession['id'] == 782) {
         ?>
               <li>
                  <a href="<?php echo base_url();?>recruiterfiles/cnxtech0610.xlsx">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edit.png" class="nonhover">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edithover.png" class="acthover">
                     <p>Download CNX_LEADS</p>
                  </a>
               </li>

               <li>
                  <a href="<?php echo base_url();?>recruiterfiles/CNXMakati_alabang.xlsx">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edit.png" class="nonhover">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edithover.png" class="acthover">
                     <p>Download CNXMakati_alabang</p>
                  </a>
               </li>      

               <li>
                  <a href="<?php echo base_url();?>recruiterfiles/Techincal_Leads.xlsx">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edit.png" class="nonhover">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edithover.png" class="acthover">
                     <p>Download CNX_TECHNICAL_LEADS</p>
                  </a>
               </li>

               <li>
                  <a href="<?php echo base_url();?>recruiterfiles/sales_leads.xlsx">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edit.png" class="nonhover">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edithover.png" class="acthover">
                     <p>Download CNX_SALES_LEADS</p>
                  </a>
               </li>

               

               <li>
                  <a href="<?php echo base_url();?>recruiterfiles/CNCX_qc_manda.xlsx">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edit.png" class="nonhover">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edithover.png" class="acthover">
                     <p>Download CNCX_qc_manda</p>
                  </a>
               </li>

               <li>
                  <a href="<?php echo base_url();?>recruiterfiles/MOA_MAKATI_ALABANG_LEADS.xlsx">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edit.png" class="nonhover">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edithover.png" class="acthover">
                     <p>Download MOA_MAKATI_ALABANG_LEADS</p>
                  </a>
               </li>

               <li>
                  <a href="<?php echo base_url();?>recruiterfiles/cnx_bacolod_baguio_cebu_clark_davao_taguig_sa_lazro_nuvali.xlsx">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edit.png" class="nonhover">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edithover.png" class="acthover">
                     <p>Download cnx_bacolod_baguio_cebu_clark_davao_taguig_sa_lazro_nuvali</p>
                  </a>
               </li>

               <li>
                  <a href="<?php echo base_url();?>recruiterfiles/alabang 11052021.xlsx">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edit.png" class="nonhover">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edithover.png" class="acthover">
                     <p>Download alabang 11052021</p>
                  </a>
               </li>
               <li>
                  <a href="<?php echo base_url();?>recruiterfiles/cLARK 11052021.xlsx">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edit.png" class="nonhover">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edithover.png" class="acthover">
                     <p>Download clark 11052021</p>
                  </a>
               </li>
               <li>
                  <a href="<?php echo base_url();?>recruiterfiles/CUBAO and Bridgetowne11052021.xlsx">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edit.png" class="nonhover">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edithover.png" class="acthover">
                     <p>Download CUBAO and Bridgetowne 11052021</p>
                  </a>
               </li>
               <li>
                  <a href="<?php echo base_url();?>recruiterfiles/North EDSA and ETON 11052021.xlsx">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edit.png" class="nonhover">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edithover.png" class="acthover">
                     <p>Download North EDSA and ETON 11052021</p>
                  </a>
               </li>
         <?php
            } elseif($userSession['id'] == 499) {
         ?>
               <li>
                  <a href="<?php echo base_url();?>recruiterfiles/Alorica.xlsx">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edit.png" class="nonhover">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edithover.png" class="acthover">
                     <p>Download</p>
                  </a>
               </li>
         <?php
            } elseif($userSession['id'] == 729) {
         ?>
               <li>
                  <a href="<?php echo base_url();?>recruiterfiles/ResultsCSRHealthCareTSRaugtooct2021.xlsx">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edit.png" class="nonhover">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edithover.png" class="acthover">
                     <p>Download</p>
                  </a>
               </li>

               <li>
                  <a href="<?php echo base_url();?>recruiterfiles/caintataytay.xlsx">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edit.png" class="nonhover">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edithover.png" class="acthover">
                     <p>Download Cainta/Taytay</p>
                  </a>
               </li>

               <li>
                  <a href="<?php echo base_url();?>recruiterfiles/caintamandamarikinaqcpasig.xlsx">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edit.png" class="nonhover">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edithover.png" class="acthover">
                     <p>Download Cainta mandaluyong pasig qc leads</p>
                  </a>
               </li>

               <li>
                  <a href="<?php echo base_url();?>recruiterfiles/CAINTATAYTAYANTIPOLOMARIKINAMANDAQCPASIF.xlsx">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edit.png" class="nonhover">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/edithover.png" class="acthover">
                     <p>Download 5 City Leads</p>
                  </a>
               </li>
         <?php
            }
         ?>
<?php
   //}   
?>
   <!-- <li>
      <a href="<?php echo base_url();?>recruiter/dashboard/logout">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/logout.png" class="nonhover">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/logouthover.png" class="acthover">
         <p>Logout</p>
      </a>
   </li> -->
</ul>


<style type="text/css">
   ul.innersubd li {
    display: block;
    width: 100%;
    margin: 0 0 6px 0;
}

ul.innersubd {
    margin: 7px 0 0 0;
    float: left;
    width: 100%;
    padding-left: 29px;
}

div#collapseExample, #collapseExample1 {
    float: left;
    width: 100%;
}

</style>