

<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>JobYoda</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <meta name="author" content="CreativeLayers">
      <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
      <link rel="icon" href="<?php echo base_url().'recruiterfiles/';?>images/fav.png" type="image/png" sizes="16x16">
      <!-- Styles -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap-grid.css" />
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/icons.css">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/animate.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/all.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/fontawesome.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/responsive.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/chosen.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/colors/colors.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/style.css" />
   </head>
   <style>
      .dataTables_length label{text-transform: capitalize!important;}
      .success_msg {
      color: #26ae61;
      margin-bottom: 0px;
      font-size: 16px;
      }
      table.dataTable tbody td button {
      font-size: 13px;
      padding: 8px 12px;
      }
      .lightColor {
      color: #5d8e34 !important;
      }
      .redColor {
      color: #c71434 !important;
      }
      .subscriptionHead{
      display: flex;
      justify-content: space-between;
      /*        align-items: center;*/
      padding-bottom: 10px;
      padding-top: 10px;
      border-bottom: 1px solid #00c671;
      }
      .subbLeft h2{
      font-weight: 600;
      color: #000;
      font-size: 18px;
      margin-bottom: 0;
      }
      .green1{
      color: #00c671;
      }
      .subbRight ul li{
      display: inline-block;
      margin-right: 8px;
      }
      .subbRight ul li:last-child{
      margin-right: 0;
      }
      .subbRight ul li a{
      font-size: 16px;
      font-weight: 600;
      padding: 6px 15px;
      border-radius: 6px;
      }
      .subscriptionTime{
      display: flex;
      justify-content: space-between;
      padding: 15px 0px;
      }
      .subscriptionTime ul li{
      display: inline-block;
      margin-right: 8px;
      color: #000;
      font-weight: 600;
      margin-bottom: 0;
      }
      .subscriptionTime ul li:last-child{
      margin-right: 0;
      }
      .subImgs{
      display: inline-block;
      width: 50px;
      position: relative;
      top: -5px;
      margin-right: 10px;
      }
       .table td, .table th,.table thead th{
           text-align: left;
       }
       .tableList .White {
    background: #fff !important;
    border: none;
    width: 50%;
}
       .tableList .green {
    background: #00c671 !important;
    border: none;
    color: #fff;
    font-weight: 600;
    width: 25%;
               text-align: center;
}
       .tableList .red {
    background: #de0e0e !important;
    border: none;
    color: #fff;
    font-weight: 600;
    width: 25%;
               text-align: center;
}
       .trnasparent {
    background: transparent;
    color: #000 !important;
    font-weight: 700;
    text-align: left !important;
    padding-left: 35px !important;
}
       .greennew {
    background: #dbf5ea;
    color: #00c671 !important;
    font-weight: 600;
               text-align: center !important;
}
       .rednew {
    background: #f8e4e5;
    color: #de0e0e !important;
    font-weight: 600;
               text-align: center !important;
}
       .cdfts,.modal-body{
               background: #f5f4f3;
       }
       .tableList .table th, .tableList .table td {
    border-top: 0;
}
   </style>
   <body>
      <div class="theme-layout" id="scrollup">
         <header class="stick-top nohdascr">
            <div class="menu-sec">
               <div class="container-fluid dasboardareas">
                  <!-- Logo -->
                  <?php include_once('headermenu.php');?>
                  <!-- Menus -->
               </div>
            </div>
         </header>
         <section>
            <div class="block no-padding">
               <div class="container-fluid dasboardareas">
                  <div class="row">
                     <aside class="col-md-3 col-lg-2 column border-right Sidenavbar">
                        <div class="widget">
                           <div class="menuinside">
                              <?php include_once('sidebar.php'); ?>
                           </div>
                        </div>
                     </aside>
                     <div class="col-md-9 col-lg-10 column psthbs MainWrapper">
                        <div class="padding-left transactdto">
                           <div class="manage-jobs-sec">
                              <h3>Subscription History Listing</h3>
                           </div>
                           <div class="manage-jobs-sec boostjbs">
                              <div class="col-md-12">
                                 <div class="main">
                                 </div>
                              </div>
                              <div class="table table-responsive">
                                 <table id="sample-data-table" class="table">
                                    <thead>
                                       <tr>
                                          <th class="secondary-text">
                                             <div class="table-header">
                                                <span class="column-title">Sr.no.</span>
                                             </div>
                                          </th>
                                          <th class="secondary-text">
                                             <div class="table-header">
                                                <span class="column-title">Plan Name</span>
                                             </div>
                                          </th>
                                          <th class="secondary-text">
                                             <div class="table-header">
                                                <span class="column-title">Duration</span>
                                             </div>
                                          </th>
                                          <th class="secondary-text">
                                             <div class="table-header">
                                                <span class="column-title">Start Date</span>
                                             </div>
                                          </th>
                                          <th class="secondary-text">
                                             <div class="table-header">
                                                <span class="column-title">Expiry Date</span>
                                             </div>
                                          </th>
                                          <th class="secondary-text">
                                             <div class="table-header">
                                                <span class="column-title">Actual Cost</span>
                                             </div>
                                          </th>
                                          <th class="secondary-text">
                                             <div class="table-header">
                                                <span class="column-title">Plan Cost</span>
                                             </div>
                                          </th>
                                          <th class="secondary-text">
                                             <div class="table-header">
                                                <span class="column-title">Action</span>
                                             </div>
                                          </th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <?php
                                          $x1 =1;
                                          if(isset($userplan)) {
                                          
                                            foreach($userplan as $userpla) {
                                          ?>
                                       <tr>
                                          <td><?php echo $x1;?></td>
                                          <td><?php echo $userpla['plan_name']; ?></td>
                                          <td><?php echo $userpla['duration']; ?></td>
                                          <td><?php echo $userpla['start_date']; ?></td>
                                          <td><?php echo $userpla['end_date']; ?></td>
                                          <td><?php echo $userpla['actual_cost']; ?></td>
                                          <td><?php echo $userpla['plan_cost']; ?></td>
                                          <td><a title="View" href="javascript:void(0)" class="editicd btn-icon usricos" aria-label="Product details" onclick="subscriptionfunction('<?php echo $userpla["id"]; ?>')"><i class="fa fa-eye"></i></a></td>
                                       </tr>
                                       <?php
                                          $x1++;
                                          }
                                          }
                                          ?>
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
      <?php include_once("footer.php");?>
      <?php include_once("modalpassword.php");?>
      <!-- SIGNUP POPUP -->
      <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/jquery/dist/jquery.min.js"></script>
      <!-- <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.min.js" type="text/javascript"></script>-->
      <script src="<?php echo base_url().'recruiterfiles/';?>js/modernizr.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/script.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/wow.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/slick.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/parallax.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/select-chosen.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.scrollbar.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/popper.min.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/bootstrap.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/recruiteryoda.js"></script>
      <!-- Data tables -->
      <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/datatables.net/js/jquery.dataTables.js"></script>
      <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/datatables-responsive/js/dataTables.responsive.js"></script>           
      <!-- Data tables -->
      <script type="text/javascript">
         $(document).ready(function(){  
           $('#sample-data-table').DataTable({
                "searching": true,
           });
         });
         
         function subscriptionfunction(d1) {
           var url = "<?php echo base_url(); ?>recruiter/recruiter/subscription_history_by_id";
           $.ajax({
              type:"POST",
              url:url,
              data:{
                 pid : d1
              },
              success:function(data) {
               
                 var record = JSON.parse(data);
                 
                 $('#e_planname').html(record.plan_name);
                 $('#e_plancost').html(record.plan_cost);
                 $('#e_planduration').html(record.duration);
                 $('#e_startdate').html(record.start_date);
                 $('#e_enddate').html(record.end_date);

                 $('#exist_1').html(record.total_mail);
                 $('#exist_11').html(record.remaining_mail);

                 $('#exist_2').html(record.total_job_post);
                 $('#exist_22').html(record.remaining_job_post);

                 $('#exist_3').html(record.total_resume_access);
                 $('#exist_33').html(record.remaining_resume_access);

                 $('#exist_4').html(record.total_video_post);
                 $('#exist_44').html(record.remaining_video_post);

                 $('#exist_5').html(record.total_advertise);
                 $('#exist_55').html(record.remaining_advertise);

                 $('#exist_6').html(record.total_job_boost);
                 $('#exist_66').html(record.remaining_job_boost);

                 $('#exist_7').html(record.total_candidate_profile);
                 $('#exist_77').html(record.remaining_candidate_profile);

                 $('.planimageadd').attr('src',record.plan_image);

                 $("#myModal").modal('show');
              }
           });
         }
      </script>
      <!-- Modal -->
      <div id="myModal" class="modal fade" role="dialog">
         <div class="modal-dialog" style="width: 62%;">
            <!-- Modal content-->
            <div class="modal-content">
               
               <div class="modal-body">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <div class="cdfts">
                     <div class="subscriptionHead">
                        <div class="subbLeft">
                           <h2><span class="subImgs"><img src="" class="img-fluid planimageadd" alt="golden"></span>  <span id="e_planname"></span> Plan <span color ="green1"> ₱<span id="e_plancost"></span> </span></h2>
                        </div>
                     </div>
                     <div class="subscriptionTime">
                        <ul>
                           <li>Duration : <span class="green1" id="e_planduration"> </span></li>
                        </ul>
                        <ul>
                           <li>Start Date : <span class="green1" id="e_startdate"></span></li>
                           <li>End Date : <span class="green1" id="e_enddate"></span></li>
                        </ul>
                     </div>
                     <div class="companydtls">
                        <div class="newfiletrsgree">
                           <div class="tableList">
                              <table class="table">
                                 <thead>
                                    <tr>
                                       <th class="White"></th>
                                       <th class="green">Total</th>
                                       <th class="red">Remaining</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td class="trnasparent">Number Of Mass Mails</td>
                                       <td class="greennew" id="exist_1"></td>
                                       <td class="rednew" id="exist_11"></td>
                                    </tr>
                                    <tr>
                                       <td class="trnasparent">Number Of Jobs Posting</td>
                                       <td class="greennew" id="exist_2"></td>
                                       <td class="rednew" id="exist_22"></td>
                                    </tr>
                                    <tr>
                                       <td class="trnasparent">Number Of Resume Accessiblity</td>
                                       <td class="greennew" id="exist_3"></td>
                                       <td class="rednew" id="exist_33"></td>
                                    </tr>
                                    <tr>
                                       <td class="trnasparent">Video Posting</td>
                                       <td class="greennew" id="exist_4"></td>
                                       <td class="rednew" id="exist_44"></td>
                                    </tr>
                                    <tr>
                                       <td class="trnasparent">Advertisement Posting</td>
                                       <td class="greennew" id="exist_5"></td>
                                       <td class="rednew" id="exist_55"></td>
                                    </tr>
                                    <tr>
                                       <td class="trnasparent">Job Boosting</td>
                                       <td class="greennew" id="exist_6"></td>
                                       <td class="rednew" id="exist_66"></td>
                                    </tr>
                                    </tr>
                                    <tr>
                                       <td class="trnasparent">Candidate Profile</td>
                                       <td class="greennew" id="exist_7"></td>
                                       <td class="rednew" id="exist_77"></td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>
