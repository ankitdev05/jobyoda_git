
<div class="footerrecruiter">
  <div class="container">
    <div class="addressar">
    <!-- 
      <p> <strong>Office Address:</strong> Level 10-1 Fort Legend Tower, 3rd Avenue Bonifacio Global City 31st Street Fort Bonifacio TAGUIG CITY, FOURTH DISTRICT, NCR, Philippines, 1634</p> 
    -->
      <p><strong>Office Address:</strong>
        Level 10-1 Fort Legend Tower, 3rd Avenue Bonifacio Global City 31st Street Fort Bonifacio Taguig City, Fourth District, NCR, Philippines, 1634
      </p>
    </div>
  
    <div class="alinflow">
      <p><strong>Email -</strong> <a href="mailto:help@jobyoda.com">help@jobyoda.com</a></p>
      <p><strong>Phone No -</strong> <a href="tel:+63 9178721630">+63 9178721630</a></p>
    </div>
  </div>
</div>

<!-- Plan notify Modal -->
<div class="modal fade" id="planNotifyModal" tabindex="-1" role="dialog" aria-labelledby="planNotifyModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-body">
          <div class="modalMail modalMail2">
            <button type="button" class="close closed" data-dismiss="modal">&times;</button>

            <div class="imgSlecet">
              <img src="<?php echo base_url();?>webfiles/newone/images/Alert.png" class="img-fluid" alt="golden">
            </div>
            <div class="SlectContent">
              <h2>Please Subscribe/Renew plan for use services</h2>
            </div>
            <div class="subPlanButton">
                <center> <a href="<?php echo base_url();?>recruiter/subscription" class="btn btn-warning"> Check Your Subscription plan </a></center>
            </div>
          </div>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div> -->
    </div>
  </div>
</div>

<script type="text/javascript">
    $("#scategory").change(function() {

       var cat_val = $("#scategory").val();
       var url =  '<?php echo base_url(); ?>/recruiter/Jobpost/fetchSubcategory';
    
       $.ajax({
                type:"POST",
                url:url,
                data:{
                    cat_val : cat_val
                },
                success:function(data) {
                    $("#ssubcategory").html(data);
            
                }
            });
   });
</script>
<script>
  $.fn.jQuerySimpleCounter = function( options ) {
    var settings = $.extend({
        start:  0,
        end:    100,
        easing: 'swing',
        duration: 400,
        complete: ''
    }, options );

    var thisElement = $(this);

    $({count: settings.start}).animate({count: settings.end}, {
    duration: settings.duration,
    easing: settings.easing,
    step: function() {
      var mathCount = Math.ceil(this.count);
      thisElement.text(mathCount);
    },
    complete: settings.complete
  });
};

<?php
    $userData11 = $this->session->userdata('userSession11');
?>

$('#number1').jQuerySimpleCounter({end: <?php echo $userData11['onedata']; ?>,duration: 3000});
$('#number2').jQuerySimpleCounter({end: <?php echo $userData11['twodata']; ?>,duration: 3000});

$('#number3').jQuerySimpleCounter({end: 50000,duration: 4000});
$('#number4').jQuerySimpleCounter({end: 1000,duration: 2500});

$('.about-me-img').hover(function(){
  $('.authorWindowWrapper').stop().fadeIn('fast').find('p').addClass('trans');
}, function(){
  $('.authorWindowWrapper').stop().fadeOut('fast').find('p').removeClass('trans');
});
</script>

<style>
  .formcontrol{
      border: 1px solid #b5aeae!important;
      padding: 10px 6px!important;
      width:100%;
  }
</style>
<style>
  .modalContent{
    display: flex;
    position: relative;
}
.modalNew .modal-body{
    padding: 0;
}
.modalContLeft{
    width: 35%;
    background: #0a4f8a;
    height: 100%;
}
.modalNew .modal-dialog{
    width: 650px;
}
.modalContLeft h2{
    font-family: "Quicksand", sans-serif;
    font-weight: 800;
    color: #fff;
    font-size: 22px;
    padding-bottom: 15px;
}
.modalContRight{
    width: 65%;
    background: #f2f2fa;
      padding: 45px 30px;

}
.modalContRight h2,.goldenContent h2{
    font-size: 20px;
}
.iconsGold {
    width: 45px;
    display: inline-block;   
    
}
.goldenImg{
    width: 150px;
    margin: 12px auto 0;
}
.goldenImg.goldenImg2{
    width: 110px;
    margin: inherit;
}
.goldenContent h2{
    color: #fff;
}
.goldenImg img{
    width: 100%;
    object-fit: cover;
}
.iconsBtn{
    background: #0a4f8a;
    color: #fff;
    padding: 4px 10px;
    border-radius: 5px;
}
.goldenContent{
    padding: 0px 35px 0px 25px;
    font-size: 16px;
        color:#000000bf; 
    min-width: 216px;
        display: inline-block;
}
.modalContRight ul li{
    padding: 30px 0px;
    border-top: 1px solid #ccccccb8;
}
.modalContRight ul{
    margin-top: 30px;
}
.modalClass{
    padding: 80px 0px 115px;
        text-align: center;
}
.iconsGold img{
    width: 100%;
    object-fit: cover;
}
.modalNew .modal-content {
    box-shadow: #ccc 0px 1px 2px 0px;
    border: 0;
    border-radius: 20px;
    overflow: hidden;
    
}
.close.closed{
     position: absolute;
    right: 15px;
    top: 15px;
    font-size: 30px;
    font-weight: 200;
    color: #fff;
    padding: 4px 10px;
    border-radius: 100%;
    background: #777;
        opacity: 1
}
.modalMailTop{
      background: #0a4f8a;
    padding: 10px 20px;
    display: flex;
    align-items: center;
}
.inputAll.inputAll2{
    border: none;
    background: #cccccc87;
}
.mainSEnd{
    padding: 25px;
}
textarea{
    resize: none;
}
.textBtn{
    float: right;
}
.textBtn button,.subPlanButton button{
    border: none;
        padding: 12px;
    border-radius: 6px;
    color: #fff;
    font-family: Roboto;
    background: #fbaf31;
    font-size: 16px;
    display: inline-block;
    min-width: 100px;
}
.subPlanButton button{
    min-width: 200px;
}
.boxs{
    float: left;
    display: block;
    width: 100%;
}
.modalMail{
    position: relative;
}
.sliderJyodo .owl-item.active.center .pyCon h3{
    margin-top: 5px;
}
.dummyPopup{
    margin: 40px 0px;
}
.SlectContent h2{
    font-size: 20px;
    padding: 20px 0px;
}
.modalMail.modalMail2 {
    position: relative;
    text-align: center;
    padding:35px 30px;
}
.modalNew.modalNew2 .modal-dialog {
    width: 525px;
}
.dummyPopup ul li{
    display: inline-block;
    font-size: 20px;
    margin-right: 20px;
}
.modalMail.modalMail3{
    padding: 25px;
}
.searchHead h2{
   border-bottom: 1px solid #000;
    font-size: 22px;
    padding-bottom: 15px;
}
.modalNew.modalNew3 .modal-dialog {
    width: 800px;
}
.searchContent{
    padding: 15px 0px;
}
.searchContent .textBtn{
    padding: 15px 0px 20px 0px;
}
.searchContent .inputAll.inputAll2{
        color: #000;
    font-family: 'Open Sans';
    font-weight: 600;
}
.textBtn button:hover, .subPlanButton button:hover{
        background-color: #00a94f;
    color: #fff;
}
</style>