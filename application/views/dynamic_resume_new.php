<!DOCTYPE html>
<html lang="en">

<head>
    <title>Resume</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
</head>
<style>
    body,
    h2,
    p,
    li,
    ul,
    h3 {
        margin: 0;
        padding: 0;
        font-family: sans-serif;
    }
    .DetailsTable tbody,
    .DetailsTable tbody tr,
    .DetailsTable tbody tr td{ display: block; width: 100% }
    .FLex{ display: flex; }
    html { -webkit-print-color-adjust: exact; }
</style>

<body style="padding:0;margin:0;float:left;width:100%;display:block;"> 
    <table style="box-sizing: border-box; width: 100%; background: #94b6d2;">
        <tr>
            <td style="width: 30%; float: left; padding: 15px; box-sizing: border-box; background: #94b6d2; border-radius: 0px 0px 50px 0px">
                <table style= "box-sizing: border-box; width: 100%;"> 
                    <tr>
                        <td colspan="3">
                            <figure style="margin: 0 auto 20px; height: 150px; width: 150px; overflow: hidden; border-radius: 100%;">
                                <img src="http://18.191.12.133/webfiles/img/profilepic.png" style="width: 100%;" />
                            </figure>
                        </td>
                    </tr>
                    <tr>
                        <td style="color: #fff; font-weight: 600; width: 100%; font-size: 13px; text-align: left; padding-bottom: 20px">
                            <h4 style="font-size: 14px; font-weight: 600; color: #fff; margin:0 0 7px; ">
                                JOB SEARCH STATUS :
                            </h4>
                            <p style="font-size: 13px; font-weight: 500; color: #fff">
                                Active
                            </p>
                        </td> 
                    </tr>
                    <tr>
                        <td style="color: #fff; font-weight: 600; width: 100%; font-size: 13px; text-align: left; padding-bottom: 20px">
                            <h4 style="font-size: 14px; font-weight: 600; color: #fff; margin:0 0 7px; ">
                                SPECIALIZATION :
                            </h4>
                            <p style="font-size: 13px; font-weight: 500; color: #fff">
                                Active
                            </p>
                        </td> 
                    </tr>
                    
                    <tr>
                        <td style="color: #fff; font-weight: 600; width: 100%; font-size: 13px; text-align: left; padding-bottom: 20px">
                            <h4 style="font-size: 14px; font-weight: 600; color: #fff; margin:0 0 7px; ">
                                JOBS INTERESTED IN :
                            </h4>
                            <p style="font-size: 13px; font-weight: 500; color: #fff">
                                Active
                            </p>
                        </td> 
                    </tr>
                    
                    <tr>
                        <td style="color: #fff; font-weight: 600; width: 100%; font-size: 13px; text-align: left; padding-bottom: 20px">
                            <h4 style="font-size: 14px; font-weight: 600; color: #fff; margin:0 0 7px; ">
                                CURRENT/ LAST BPO :
                            </h4>
                            <p style="font-size: 13px; font-weight: 500; color: #fff">
                                Active
                            </p>
                        </td> 
                    </tr>
                    
                    <tr>
                        <td style="color: #fff; font-weight: 600; width: 100%; font-size: 13px; text-align: left; padding-bottom: 20px">
                            <h4 style="font-size: 14px; font-weight: 600; color: #fff; margin:0 0 7px; ">
                                BPO EXPERIENCE :
                            </h4>
                            <p style="font-size: 13px; font-weight: 500; color: #fff">
                                Active
                            </p>
                        </td>
                    </tr>
                    
                    <tr>
                        <td style="color: #fff; font-weight: 600; width: 100%; font-size: 13px; text-align: left; padding-bottom: 20px">
                            <h4 style="font-size: 14px; font-weight: 600; color: #fff; margin:0 0 7px; ">
                                CONTACT INFO:
                            </h4>
                            <p style="font-size: 13px; font-weight: 500; color: #fff; margin-bottom: 10px">
                                Phone : 678-555-0103
                            </p>
                            <p style="font-size: 14px; font-weight: 500; color: #fff; margin-bottom: 10px">
                                EMAIL : someone@example.com
                            </p>
                        </td>
                    </tr>
                    
                    <tr>
                        <td style="color: #fff; font-weight: 600; width: 100%; font-size: 13px; text-align: left; padding-bottom: 20px">
                            <h4 style="font-size: 14px; font-weight: 600; color: #fff; margin:0 0 7px; ">
                                BEST TIME TO CONTACT :
                            </h4>
                            <p style="font-size: 13px; font-weight: 500; color: #fff">
                                Active
                            </p>
                        </td>
                    </tr> 
                    <tr>
                        <td style="color: #fff; font-weight: 600; width: 100%; font-size: 13px; text-align: left; padding-top: 100px">
                            <h4 style="font-size: 14px; font-weight: 600; color: #fff; margin:0 0 7px; ">
                                RESUME CREATED BY :
                            </h4>
                            <figure style=" margin:0px 0 0 0; background-color: #fff; display: block; padding: 10px 20px; ">
                                <img src="https://jobyoda.com/webfiles/newone/images/logonew.png" style="width: 150px">
                            </figure>
                        </td> 
                    </tr>
                </table>
            </td>

            <td style=" width: 70%; float: left; box-sizing: border-box; margin: 0px 0px 0;  border-radius: 50px 0 0 0;  background-color: #fff;"> 

                <table class="DetailsTable" style="box-sizing: border-box; width: 100%; display: block; padding:30px">
                    <tr>
                        <td style="box-sizing: border-box; padding: 0px 0 10px; width: 50%;">
                            <h3 style="font-size: 40px; margin: 0 0 10px; color: #000;">HYGEIA ZAFRA</h3>
                            <p style="font-size: 14px; font-weight: 600; color: #000; margin:0 0 7px;">job Title : </p>
                            <p style="font-size: 14px; font-weight: 600; color: #000; margin:0 0 7px;">Location : </p>
                        </td> 
                    </tr>

                    <tr>
                        <td style="color: #000; font-weight: 600; width: 100%; font-size: 13px; text-align: left; padding-bottom: 20px">
                            <h4 style="font-size: 14px; font-weight: 600; color: #000; margin:0 0 7px; ">
                                EDUCATIONAL ATTAINMENT :
                            </h4>
                            <p style="font-size: 14px; font-weight: 500; color: #000">
                                Active
                            </p>
                        </td> 
                    </tr>

                    <tr>
                        <td style="color: #000; font-weight: 600; width: 100%; font-size: 13px; text-align: left; padding-bottom: 20px">
                            <h4 style="font-size: 14px; font-weight: 600; color: #000; margin:0 0 7px; ">
                                WORK EXPERIENCE :
                            </h4>
                            <p style="font-size: 14px; font-weight: 500; color: #000">
                                Active
                            </p>
                        </td> 
                    </tr> 

                    <tr>
                        <td style="color: #000; font-weight: 600; width: 100%; font-size: 13px; text-align: left; padding-bottom: 20px">
                            <h4 style="font-size: 14px; font-weight: 600; color: #000; margin:0 0 7px; ">
                                SPECIALIZATION :
                            </h4>
                            <p style="font-size: 14px; font-weight: 500; color: #000">
                                Active
                            </p>
                        </td> 
                    </tr>

                    <tr>
                        <td style="color: #000; font-weight: 600; width: 100%; font-size: 13px; text-align: left; padding-bottom: 20px">
                            <h4 style="font-size: 14px; font-weight: 600; color: #000; margin:0 0 7px; ">
                                PROFESSIONAL SKILLS :
                            </h4>
                            <p style="font-size: 14px; font-weight: 500; color: #000">
                                Active
                            </p>
                        </td> 
                    </tr>

                    <tr>
                        <td style="color: #000; font-weight: 600; width: 100%; font-size: 13px; text-align: left; padding-bottom: 20px">
                            <h4 style="font-size: 14px; font-weight: 600; color: #000; margin:0 0 7px; ">
                                LANGUAGES SPOKEN :
                            </h4>
                            <p style="font-size: 14px; font-weight: 500; color: #000">
                                Active
                            </p>
                        </td> 
                    </tr>

                    <tr>
                        <td style="color: #000; font-weight: 600; width: 100%; font-size: 13px; text-align: left; padding-bottom: 20px">
                            <h4 style="font-size: 14px; font-weight: 600; color: #000; margin:0 0 7px; ">
                                TOP CLIENT SUPPORTED :
                            </h4>
                            <p style="font-size: 14px; font-weight: 500; color: #000">
                                Active
                            </p>
                        </td> 
                    </tr> 

                    <tr>
                        <td style="box-sizing: border-box; padding: 0px 0 0; width: 100%;">
                            <h4 style="font-size: 14px; font-weight: 600; color: #000; margin: 0px 0 0px; ">
                                User Assessment
                            </h4> 
                        </td> 
                    </tr>

                    <tr>
                        <td style="box-sizing: border-box; padding: 5px 0;"> 

                            <table style="box-sizing: border-box; width: 100%; display: block;">
                                <tr>
                                    <td style="font-size: 14px; color: #000; margin-bottom: 0; width: 200px; font-weight: 500; padding:10px 0">Verbal</td>
                                    <td>
                                        <div style="background:#ccc;width:100%;height:18px;border-radius:0px;position:relative">
                                            <div style="position:absolute;top:0;left:0;width:55%;height:18px;background:#26ae61;border-radius:0px;">
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="font-size: 14px; color: #000; margin-bottom: 0; width: 200px; font-weight: 500; padding:10px 0">Written</td>
                                    <td>
                                        <div style="background:#ccc;width:100%;height:18px;border-radius:0px;position:relative">
                                            <div style="position:absolute;top:0;left:0;width:55%;height:18px;background:#26ae61;border-radius:0px;">
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="font-size: 14px; color: #000; margin-bottom: 0; width: 200px; font-weight: 500; padding:10px 0">Listening</td>
                                    <td>
                                        <div style="background:#ccc;width:100%;height:18px;border-radius:0px;position:relative">
                                            <div style="position:absolute;top:0;left:0;width:55%;height:18px;background:#26ae61;border-radius:0px;">
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="font-size: 14px; color: #000; margin-bottom: 0; width: 200px; font-weight: 500; padding:10px 0">Problem Solving</td>
                                    <td>
                                        <div style="background:#ccc;width:100%;height:18px;border-radius:0px;position:relative">
                                            <div style="position:absolute;top:0;left:0;width:55%;height:18px;background:#26ae61;border-radius:0px;">
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="font-size: 14px; color: #000; margin-bottom: 0; width: 200px; font-weight: 500; padding:10px 0">Problem Solving</td>
                                    <td>
                                        <div style="background:#ccc;width:100%;height:18px;border-radius:0px;position:relative">
                                            <div style="position:absolute;top:0;left:0;width:55%;height:18px;background:#26ae61;border-radius:0px;">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table> 
                        </td>
                    </tr>  
                </table>
            </td> 
        </tr>
    </table>
</body>
</html>