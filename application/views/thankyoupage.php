<?php
    include_once('header2.php');
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {

    } else {
        $link = "https";
        $link .= "://";
        $link .= $_SERVER['HTTP_HOST'];
        $link .= $_SERVER['REQUEST_URI'];
        redirect($link);
    }
    $userSess = $this->session->userdata('usersess'); 
    if ($this->session->userdata('userfsess')) {
        $userfsess = $this->session->userdata('userfsess');
        $type      = $userfsess['type'];
    }
?>

    <section style="    margin-top: 89px;">
        <div class="SliderArea" style="padding: 14px 0 10px 0;">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="SliderText">

                            <h4>Best BPO jobs in the Philippines in one single site sorted by benefits and distance</h4>

                            <ul class="hotJobs">
                                <li><i class="fa fa-tags"></i> Trending Keywords :</li>
                                <li><a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/hotjob">Hot Jobs,</a></li>
                                <li><a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/work_from_home">Work From Home Jobs,</a></li> 
                                <li><a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/14_month_pay">Jobs With 14th Month Pay,</a></li>
                                <li><a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/day_shift">Day Shift Jobs</a></li> 
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="DownloadApp">
                            <a href="https://apps.apple.com/us/app/jobyoda/id1471619860?ls=1" class="download-btn" target="_blank">
                                        <span>
                                            <!-- <img src="https://jobyoda.com/webfiles/img/home/apple.png"> -->
                                            <img src="<?php echo base_url();?>webfiles/newone/social/ios_download_jobyoda.png">

                                        </span>
                                        <!-- <p>
                                            <small>Download On</small>
                                            <br>
                                            App Store
                                        </p> -->
                                    </a>
                                    <a href="https://play.google.com/store/apps/details?id=com.jobyodamo" class="download-btn" target="_blank">
                                        <span>
                                            <!-- <img src="https://jobyoda.com/webfiles/img/home/appstore.png"> -->
                                            <img src="<?php echo base_url();?>webfiles/newone/social/android_download_jobyoda.png">
                                        </span>
                                        <!-- <p>
                                            <small>Get It On</small>
                                            <br>
                                            Google Play
                                        </p> -->
                                    </a>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    
    <section>
        <div class="StatsArea" style="padding: 0px 0 0;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <center style="margin-bottom: 0px;"><img src="<?php echo base_url().'webfiles/';?>newone/images/THANK_YOU_PAGE_main_photo_SEPT.png" style="width:60%"></center>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php
    function getToppickFunction($toppickID) {

        if($toppickID == 1) {
                                                            
            return '<li><img src="'.base_url() .'recruiterfiles/images/m_bonus.png"> Joining Bonus</li>';

        } else if($toppickID == 2) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_freefood.png"> Free Food</li>';

        } else if($toppickID == 3) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_day_1_hmo.png"> Day 1 HMO</li>';

        } else if($toppickID == 4) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_dependent_hmo.png"> Day 1 HMO for Dependent</li>';

        } else if($toppickID == 5) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_dayshift.png"> Day Shift</li>';

        } else if($toppickID == 6) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_14th_pay.png"> 14th Month Pay </li>';
        
        } else if($toppickID == 7) {

            return '<li><img src="'. base_url(). 'webfiles/newone/images/Searches-2.png"> Work From Home </li>';
        
        } else {
            return "";
        }                                                 
    }
?>
<?php
    include_once('footer1.php');
?>