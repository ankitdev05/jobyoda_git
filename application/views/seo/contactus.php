<?php include('header.php'); 
//print_r($jobtitleLists);
?>
<div class="content custom-scrollbar">
<div class="doc data-table-doc page-layout simple full-width">


 <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
                        <!-- APP TITLE -->
                        <div class="col-12 col-sm">
                           <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                              <div class="logo-icon mr-3 mt-1">
                                 <i class="icon s-6 icon-text-shadow"></i>
                              </div>
                              <div class="logo-text">
                                 <div class="h4">Contact Us</div>
                                 
                              </div>
                           </div>
                        </div>
                        <!-- / APP TITLE -->
						</div>
						
						<div class="page-content p-6">
					 <div class="content container">
                           <div class="row">
                              <div class="col-12"> 
					 
					 <div class="example">
					 
					  <div class="source-preview-wrapper">
                                       <div class="preview">
									    <div class="preview-elements">
										
                    <div id="registers">

                        <div class="form-wrapper">

                            
                    <div class="mainjob editablepartdgf">
                            <form name="registerForm" action="<?php echo base_url();?>seo/recruiter/updateContactDetail" method="post" novalidate>
                        <div class="job">
                                <div class="form-group mb-4">
                                    <input type="text" name="email" class="form-control" id="registerFormInputName" aria-describedby="nameHelp" required="required" value="<?php if(!empty($contentSingle['email'])){ echo $contentSingle['email']; }?>" />
                                    <label for="registerFormInputName">Email</label>
                                    <span style="color: red;"><?php if(!empty($errors['email'])){ echo $errors['email']; } ?></span>
                                </div>
                                <div class="form-group mb-4">
                                    <textarea name="address" class="form-control"  aria-describedby="nameHelp" required="required" ><?php if(!empty($contentSingle['address'])){ echo $contentSingle['address']; }?></textarea>
                                    <label for="registerFormInputName">Address</label>
                                    <span style="color: red;"><?php if(!empty($errors['address'])){ echo $errors['address']; } ?></span>
                                    
                                </div>
                                 <div class="form-group mb-4">
                                    <textarea name="meta_tag" class="form-control"  aria-describedby="nameHelp" required="required" ><?php if(!empty($contentSingle['meta_tag'])){ echo $contentSingle['meta_tag']; }?></textarea>
                                    <label for="registerFormInputName">Meta Tag</label>
                                    <span style="color: red;"><?php if(!empty($errors['meta_tag'])){ echo $errors['meta_tag']; } ?></span>
                                    
                                </div>

                                 <div class="form-group mb-4">
                                    <textarea name="meta_description" class="form-control"  aria-describedby="nameHelp" required="required" ><?php if(!empty($contentSingle['meta_description'])){ echo $contentSingle['meta_description']; }?></textarea>
                                    <label for="registerFormInputName">Meta Description</label>
                                    <span style="color: red;"><?php if(!empty($errors['meta_description'])){ echo $errors['meta_description']; } ?></span>
                                    
                                </div>
						</div>		
                           <div class="jobbtn">     
                                <button type="submit" class="submit-button btn btn-block btn-secondary my-4 mx-auto" aria-label="LOG IN">
                                   Update
                                </button>
							</div>	
							</div>
                       
                            </form>
                        </div>
                    </div>
					
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					
                     </div>
                </div>
            </div>
            

        </div>
        <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
        </nav>
    </main>

    <script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
    <script type="text/javascript">
    //  CKEDITOR.replace('content');
    </script>
</body>

</html>