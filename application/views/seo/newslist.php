<?php include('header.php'); ?>


               <div class="content custom-scrollbar">
                  <div class="doc data-table-doc page-layout simple full-width">
                     <!-- HEADER -->
                     <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
                        <!-- APP TITLE -->
                        <div class="col-12 col-sm">
                           <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                              <div class="logo-icon mr-3 mt-1">
                                 <i class="icon s-6 icon-person-box"></i>
                              </div>
                              <div class="logo-text">
                                 <div class="h4">News Listing</div>
                                 <div class="">Total: <?php if($advertiseLists){echo count($advertiseLists); }?></div>
                              </div>
                           </div>
                        </div>
                        
                     </div>
                     <!-- / HEADER -->
                     <!-- CONTENT -->
                     <div class="page-content p-6">
                        <div class="content container">
                           <div class="row">
                              <div class="col-12">
                                 <div class="example ">
                                    <div class="source-preview-wrapper">
                                       <div class="preview">
                                          <div class="preview-elements reculsting"> 
                                             <table id="sample-data-table" class="table">
                                                <thead>
                                                   <tr>
                                                      <th class="secondary-text sorting_asc" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-sort="ascending" aria-label="ID : activate to sort column descending" style="width: 40px;">
                                                         <div class="table-header">
                                                            <span class="column-title">Sno</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-label="
                                                         
                                                            Name
                                                         
                                                      : activate to sort column ascending" style="width: 102px;">
                                                         <div class="table-header">
                                                            <span class="column-title">Banner</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-label="
                                                         
                                                            Name
                                                         
                                                      : activate to sort column ascending" style="width: 102px;">
                                                         <div class="table-header">
                                                            <span class="column-title">Title</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-label="
                                                         
                                                            Name
                                                         
                                                      : activate to sort column ascending" style="width: 102px;">
                                                         <div class="table-header">
                                                            <span class="column-title">Description</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-label="
                                                         
                                                            Name
                                                         
                                                      : activate to sort column ascending" style="width: 102px;">
                                                         <div class="table-header">
                                                            <span class="column-title">Posted Date</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-label="
                                                         
                                                            Action
                                                         
                                                      : activate to sort column ascending" style="width: 68px;">
                                                         <div class="table-header">
                                                            <span class="column-title">Action</span>
                                                         </div>
                                                      </th>
                                                   </tr>
                                                </thead>
                                                <tbody>
                                                   <?php
                                                    $x1 =1;
                                                    foreach($advertiseLists as $advertiseList) {
                                                   ?>
                                                    <tr role="row" class="odd">
                                                      <td class="sorting_1"><?php echo $x1;?></td>
                                                      
                                                      <td><img src="<?php if(!empty($advertiseList['banner'])){ echo $advertiseList['banner'];}?>" style="width: 37px;"></td>
                                                      
                                                      <td><?php echo $advertiseList['title'];?></td>
                                                      <td><?php echo strip_tags(substr($advertiseList['description'],0,100));?></td>
                                                      <td><?php echo $advertiseList['updated_at'];?></td>
                                                      <td>
                                                        <a title="Edit" href="<?php echo base_url(); ?>administrator/recruiter/editnewsadvertise?id=<?php echo $advertiseList['id'] ?>" class="btn btn-icon usricos fuse-ripple-ready" aria-label="Product details"><i class="icon s-4 icon-pencil"></i></a>
                                                        
                                                        <a title="Delete"  href="<?php echo base_url(); ?>administrator/recruiter/deletenews?id=<?php echo $advertiseList['id'] ?>" class="btn btn-icon usricos fuse-ripple-ready" aria-label="Product details"><i class="icon s-4 icon-trash"></i></a>
                                                      </td>
                                                   </tr>
                                                   <?php $x1++;
                                                      }
                                                   ?>
                                                </tbody>
                                             </table>
                                             <script type="text/javascript">
                                                $('#sample-data-table').DataTable();
                                             </script>
                                          </div>
                                       </div>
                                       <div class="source custom-scrollbar">
                                          <div class="highlight">
                                             <pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-html" data-lang="html">
                                             </code></pre>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- CONTENT -->
                  </div>
               </div>
            </div>

         </div>
         <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
            <!--<a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
            <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
            </a>-->
         </nav>
      </main>
    </main>
    <script type="text/javascript">

    </script>
   </body>
</html>

