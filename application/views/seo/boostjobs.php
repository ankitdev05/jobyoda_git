<style>
   
.modal input[type="text"] {
    border-bottom: 1px solid #052645;
    width: 100%;
}
</style>
<?php include('header.php');?>
               <div class="content custom-scrollbar">
                  <div class="doc data-table-doc page-layout simple full-width">
                     <!-- HEADER -->
                     <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
                        <!-- APP TITLE -->
                        <div class="col-12 col-sm">
                           <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                              <div class="logo-icon mr-3 mt-1">
                                 <i class="icon-briefcase s-6"></i>
                              </div>
                              <div class="logo-text">
                                 <div class="h4">Boost Job Listing</div>
                                 <div class="">Total: <?php if($jobLists){echo count($jobLists); }?></div>
                              </div>
                           </div>
                        </div>
                        <!-- / APP TITLE -->
                        
                     </div>
                     <!-- / HEADER -->
                     <!-- CONTENT -->
                     <div class="page-content p-6">
                        <div class="content container">
                           <div class="row">
                              <div class="col-12">
                                 <div class="example ">
                                    <div class="source-preview-wrapper">
                                       <div class="preview">
                                          <div class="preview-elements bootjobdfs">
                                             <table id="sample-data-table" class="table">
                                                <thead>
                                                   <tr>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">ID</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Recruiter Name</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Recruiter Email</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Job Title</span>
                                                         </div>
                                                      </th>
                                                      
                                                     <!--  <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Send Invoice</span>
                                                         </div>
                                                      </th> -->
                                                      
                                                     
                                                   </tr>
                                                </thead>
                                                <tbody>
                                                   <?php
                                                      $x1 =1;
                                                      foreach($jobLists as $jobList) {
                                                      ?>
                                                   <tr>
                                                      <td><?php echo $x1;?></td>
                                                      <td><?php echo $jobList['cname'];?></td>
                                                      <td><?php echo $jobList['email'];?></td>
                                                      <td><?php echo $jobList['jobtitle'];?></td>
                                                    <!--   
                                                      <td>
                                                           <a href="#" onclick="getid(this.id)" id="<?= $jobList['id'] ?>" data-target="#myModal1" data-toggle="modal"  class=" btn btn-block btn-secondary fuse-ripple-ready" aria-label="Product details">Send Invoice</a>
                                                           
                                                      </td> -->
                                                      
                                                     
                                                   </tr>
                                                   <?php
                                                      $x1++;
                                                      }
                                                      ?>
                                                </tbody>
                                             </table>
                                             
                                          </div>
                                       </div>
                                       <div class="source custom-scrollbar">
                                          <div class="highlight">
                                             <pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-html" data-lang="html">
    
                                                    </code></pre>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- CONTENT -->
                  </div>
               </div>
            </div>
            <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
               <div class="list-group" class="date">
                  <div class="list-group-item subheader">TODAY</div>
                  <div class="list-group-item two-line">
                     <div class="text-muted">
                        <div class="h1"> Friday</div>
                        <div class="h2 row no-gutters align-items-start">
                           <span> 5</span>
                           <span class="h6">th</span>
                           <span> May</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Events</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Group Meeting</h3>
                        <p>In 32 Minutes, Room 1B</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Public Beta Release</h3>
                        <p>11:00 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Dinner with David</h3>
                        <p>17:30 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Q&amp;A Session</h3>
                        <p>20:30 PM</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Notes</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Best songs to listen while working</h3>
                        <p>Last edit: May 8th, 2015</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Useful subreddits</h3>
                        <p>Last edit: January 12th, 2015</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Quick Settings</div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Notifications</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Cloud Sync</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Retro Thrusters</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
           <!-- <a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
            <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
            </a>-->
         </nav>
      </main>

<div class="modal" id="myModal1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create Invoice</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                  <input type="number" name="amount" id="amount" class="form-control" value="<?php echo $boost_amount[0]['boost_amount']; ?>" disabled>
                  <label for="registerFormInputName">Amount</label>
                  <input type="hidden" name="jobpost_id" id="jobpost_id" value="">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" onclick="generatepdf()">Generate Invoice</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function getid(id) {
        $('#jobpost_id').val(id);
        //var url = "<?php echo base_url() ?>administrator/jobpost/fetchhiredcandidates?job_id="+id;
    }

    function generatepdf(){
      var jobpost_id =  $('#jobpost_id').val();
      var amount = $('#amount').val();
      var url="<?php echo base_url() ?>administrator/jobpost/generatepdf?job_id="+jobpost_id+"&amount="+amount;
      if(amount==""){
        alert('Enter amount');
         return false;
        }
        //aa.setAttribute("href", link);
        else{
          window.open(url, "_blank");  
          $('#myModal1').modal('hide');
          window.location.href = "<?php echo base_url() ?>administrator/Jobpost/boostjobs";
        }
    }
</script>
<script type="text/javascript">
   $('#sample-data-table').DataTable();
</script>
   </body>
</html>

