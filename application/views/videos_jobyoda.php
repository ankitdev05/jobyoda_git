<?php include_once('header2.php'); 
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {

    } else {
        $link = "https";
        $link .= "://";
        $link .= $_SERVER['HTTP_HOST'];
        $link .= $_SERVER['REQUEST_URI'];
        redirect($link);
    }
?>

<style type="text/css">
    .VideoBox figcaption p {
            padding-top: 10px!important;
            color: #777474!important;
            font-size: 12px!important;
            text-align: justify!important;
    }
</style>
	<section>
        <div class="BannerArea" style="background-image: url('https://jobyoda.com/webfiles/img/newmap.jpg');">
            <h1>VIDEOS  </h1> 
        </div>
    </section>
 
    <section>
        <div class="VideoArea" style="margin-top: 30px;">
            <div class="container">
                <h1>Check It Out </h1>
                <div class="owl-carousel owl-theme" id="Check">
                <?php
                    if($videoListings) {

                        foreach($videoListings as $videoListing) {
                ?>
                            <div class="item">
                                <div class="VideoBox">
                                    <a href="javascript:void(0);" onclick="videofunction('<?php echo $videoListing['video']; ?>')">
                                        <figure>
                                        <?php
                                            if(strlen($videoListing['banner']) > 0) {
                                        ?>
                                            <img src="<?php echo $videoListing['banner']; ?>">
                                        <?php
                                            } else {
                                                echo '<img src="'.base_url().'webfiles/img/user_man.png">';
                                            }
                                        ?>
                                        </figure>
                                        <figcaption>
                                            <h5><?php echo $videoListing['title']; ?></h5> 
                                            <p> <?php echo $videoListing['desc']; ?> </p>
                                        </figcaption>
                                        <span><i class="fa fa-play" aria-hidden="true"></i></span>
                                    </a>
                                </div>
                            </div>
                <?php
                        }
                    }
                ?>
                </div>
            </div>
        </div>
    </section>

<div class="ModalBox">
   <div class="modal fade" id="VideoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content">
            <div class="modal-body">
               <a href="javascript:void(0);" class="Close" id="modalCloseClass" data-dismiss="modal">×</a>
               <iframe id="videoframe" src="" autoplay></iframe>
            </div>
         </div>
      </div>
   </div>
</div>

<?php include_once('footer1.php'); ?>

<script>
   function videofunction(video) {
      $('#videoframe').attr('src', video);
      $('#VideoModal').addClass('in');
      $('body').addClass('modalbackground');
      $('#VideoModal').show();

   }
   $(document).ready(function() {
      $('#modalCloseClass').on('click', function() {
         $('body').removeClass('modalbackground');
         $('#VideoModal').removeClass('in');
         $('#VideoModal').css('display','none');
         $('#videoframe').attr('src', '');
      });
   });
</script>