<?php include_once('header2.php'); 
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {

    } else {
        $link = "https";
        $link .= "://";
        $link .= $_SERVER['HTTP_HOST'];
        $link .= $_SERVER['REQUEST_URI'];
        redirect($link);
    }
?>

<style>
  .text-danger{
    font-size: 12px!important;
    color: #a94442!important;
          font-family: 'Poppins', sans-serif !important;
  }
  .bannertopheadingparah{
        z-index: 1;
    position: relative;
    /* margin: 0; */
    color: #fbaf31;
    font-size: 18px;
    margin-top: 10px;
  }
  .microtext {
    color: #fbaf31;
    font-size: 12px;
    font-weight: 500;
}
</style>
<section>
   <div class="BannerArea" style="background-image: url('<?php echo base_url(); ?>webfiles/img/newmap.jpg');">
      <h1>Signup  </h1>
      <p class="bannertopheadingparah">“Its free and takes less than 60 seconds”</p>
   </div>
</section>

<section class="personalDetail">
   <div class="container">
      <div class="col-md-7 col-sm-6">
         <div class="personalSteps">
            <div class="topSteps">
               <ul>
                  <li> <img src="<?php echo base_url(); ?>webfiles/newone/images/step1.svg" class="img-fluid" alt = "step"></li>
                  <li> <img src="<?php echo base_url(); ?>webfiles/newone/images/stepn.svg" class="img-fluid" alt = "step"></li>
               </ul>
            </div>
            <div class="bgPersonal">
               <img src="<?php echo base_url(); ?>webfiles/newone/images/bgper.svg" class="img-fluid" alt = "step">
            </div>
            <p class="bgBottom">"<i>The first step toward success is taken when you refuse to be a captive of the environment in which you first find yourself.</i>"</p>
         </div>
      </div>
      <div class="col-md-5 col-sm-6" >
         <div class="personalRight otpRight">
            <h2 class="head">Personal Details</h2>
            
            <form method="post" action="<?php echo base_url();?>auth/signupstepone" id="signupform">

               <div class="personalFlex">
                  <div class="form-group formPost">
                      <label class ="lab"></label>
                     <input type="text" class="form-control inputAll" name="fname" id="fname" placeholder="First Name" maxlength="20" autocomplete="off" value="<?php if(!empty($userData['fname'])){ echo $userData['fname']; }?>">

                     <?php if(isset($signuperrors['fname'])){echo "<p class='text-danger'>".$signuperrors['fname']."</p>"; } ?>
                  </div>
                  <div class="form-group formPost">
                      <label class ="lab"></label>
                     <input type="text" class="form-control inputAll" name="lname" id="lname" placeholder="Last Name" maxlength="20" autocomplete="off" value="<?php if(!empty($userData['lname'])){ echo $userData['lname']; }?>">

                     <?php if(isset($signuperrors['lname'])){echo "<p class='text-danger'>".$signuperrors['lname']."</p>"; } ?>
                  </div>
               </div>

               <div class="form-group formPost ">
                   <label class ="lab"></label>
                  <input type="text" class="form-control inputAll" name="email" id="email" autocomplete="off" placeholder="Email" value="<?php if(!empty($userData['email'])){ echo $userData['email']; }?>" maxlength="150" >
                  <span class="iconsAl iconsAl2">
                  <img src="<?php echo base_url(); ?>webfiles/newone/images/message.svg" class="img-fluid" alt = "message">
                  </span>

                  <?php if(isset($signuperrors['email'])){echo "<p class='text-danger'>".$signuperrors['email']."</p>"; } ?>
               </div>
               
               <!-- <div class="personalFlex"> -->
                  
                  <div class="form-group formPost">
                   <label class ="lab"></label>
                     <input type="text" class="form-control inputAll" name="location" id="location" autocomplete="off" placeholder="Address" value="<?php if(!empty($userData['location'])){ echo $userData['location']; }?>">

                     <span class="iconsAl iconsAl2">
                     <img src="<?php echo base_url(); ?>webfiles/newone/images/location.svg" class="img-fluid loctrack" alt = "location">
                     </span>

                     <span class="microtext"> * Your address helps us Notify you about Nearby jobs </span>
                     
                     <?php if(isset($signuperrors['location'])){echo "<p class='text-danger'>".$signuperrors['location']."</p>"; } ?>
                    
                  </div>

                  <input type="hidden" name="state" value="<?php if(!empty($userData['state'])){ echo $userData['state']; }?>" id="getStateMap">
                  <input type="hidden" name="city" value="<?php if(!empty($userData['city'])){ echo $userData['city']; }?>" id="getcityMap">
                  <input type="hidden" name="nationality" value="">

               <!-- </div> -->

               <!-- <div class="personalFlex"> -->

                  <!-- <div class="form-group formPost iconDrop">
                    <label class ="lab"></label>
                    <span class="iconsArrow"> <img src="<?php //echo base_url(); ?>webfiles/newone/images/arrow.svg" class="img-fluid" alt="icon"></span>
                    <select name="state" class="form-control inputAll changestate" placeholder =" Select State ">
                      <option value=""> Select State </option>
                      <?php
                         //foreach($states as $getstates) {
                      ?>
                            <option value="<?php //echo $getstates['state'];?>"> <?php //echo $getstates['state'];?> </option>
                      <?php
                         //}
                      ?>
                    </select>
                    <?php //if(isset($signuperrors['state'])){echo "<p class='text-danger'>".$signuperrors['state']."</p>"; } ?>
                  </div> -->
                  
                  <!-- <div class="form-group formPost iconDrop">
                      <label class ="lab"></label>
                            <span class="iconsArrow"> <img src="<?php //echo base_url(); ?>webfiles/newone/images/arrow.svg" class="img-fluid" alt="icon"></span>
                     <select name="city" placeholder="City/Town" class="form-control inputAll changecity">
                         <option value="">Select City</option>
                      </select>

                     <?php //if(isset($signuperrors['city'])){echo "<p class='text-danger'>".$signuperrors['city']."</p>"; } ?>
                  </div> -->
                  <!-- <div class="form-group formPost iconDrop">
                      <label class ="lab"></label>
                            <span class="iconsArrow"> <img src="<?php //echo base_url(); ?>webfiles/newone/images/arrow.svg" class="img-fluid" alt="icon"></span>
                    <select name="nationality" placeholder="Nationality" class="form-control inputAll" placeholder ="Select Nationality">
                       <option value="">Select Nationality</option>
                       <option value="Filipino">Filipino</option>
                       <?php
                          //foreach($nations as $nation) {
                            //if($nation['nationality'] == "Filipino") {
                            //} else {
                        ?>
                              <option <?php //if(!empty($userData['nationality'])){ if($userData['nationality']==$nation['nationality']) { echo "selected"; } } ?>  value="<?php //echo $nation['nationality'];?>" > <?php //echo $nation['nationality'];?> </option>
                       <?php
                            //}
                          //}
                       ?>
                    </select>
                    
                    <?php //if(isset($signuperrors['nationality'])){echo "<p class='text-danger'>".$signuperrors['nationality']."</p>"; } ?>
                  </div> -->
               <!-- </div> -->

               <div class="personalFlex">
                   <div class="form-group formPost iconDrop">
                      <label class ="lab"></label>
                      <span class="iconsArrow"> <img src="<?php echo base_url(); ?>webfiles/newone/images/arrow.svg" class="img-fluid" alt="icon"></span>
                      <select name="country_code" class="form-control inputAll" placeholder ="Select Country Code">
                  
                          <option value="">Select Country Code</option> 
                          <?php
                             foreach($phonecodes as $phonecode) {
                          ?>
                              <option <?php if(!empty($userData['country_code'])){ if($userData['country_code']=='+'.$phonecode['phonecode']) { echo "selected"; } } ?>  value="+<?php echo $phonecode['phonecode'];?>" <?php if($phonecode["phonecode"]=='63'){ echo "selected";} ?>> <?php echo $phonecode['name'];?> - <?php echo $phonecode['phonecode'];?> </option>
                          <?php
                             }
                          ?>
                       </select>

                       <?php if(isset($signuperrors['country_code'])){echo "<p class='text-danger'>".$signuperrors['country_code']."</p>"; } ?>
                   </div>

                   <div class="form-group formPost ">
                       <label class ="lab"></label>
                      <input type="text" name="phone" value="<?php if(!empty($userData['phone'])){ echo $userData['phone']; }?>" autocomplete="off" class="form-control inputAll phoneinput" minlength="8" maxlength="10" placeholder="Phone Number">
                      <span class="iconsAl iconsAl2">
                      <img src="<?php echo base_url(); ?>webfiles/newone/images/phone.svg" class="img-fluid" alt="phone">
                      </span>

                      <?php if(isset($signuperrors['phone'])){echo "<p class='text-danger'>".$signuperrors['phone']."</p>"; } ?>
                   </div>
               </div>
               <input type="hidden" name="refer_code" value="">
               <!-- <div class="personalFlex"> -->
                  <!-- <div class="form-group formPost ">
                      <label class ="lab"></label>
                      <input type="text" class="form-control inputAll" name="refer_code" id="refer_code" placeholder="Promocode (optional)" value="<?php //if(!empty($userData['refer_code'])){ echo $userData['refer_code']; }?>" maxlength="60" onkeyup="referralCode()" >

                      <p class='text-danger' id='refer_err'></p>
                  </div> -->

                 <!-- <div class="form-group formPost iconDrop">
                    <label class ="lab"></label>
                    <select name="timeforcall" class="form-control inputAll" placeholder ="Select Best time to call">
                        <option value="" class="demo">Select Best time to call</option>
                        <option value="10 AM-1 PM" <?php //if(!empty($userData['timeforcall'])){ if($userData['timeforcall']=="10 AM-1 PM") { echo "selected"; } } ?>> 10 AM - 1 PM </option>
                        <option value="1 PM-4 PM" <?php //if(!empty($userData['timeforcall'])){ if($userData['timeforcall']=="1 PM-4 PM") { echo "selected"; } } ?>> 1 PM - 4 PM </option>
                        <option value="4 PM-7 PM" <?php //if(!empty($userData['timeforcall'])){ if($userData['timeforcall']=="4 PM-7 PM") { echo "selected"; } } ?>> 4 PM - 7 PM </option>
                        <option value="7 PM-10 PM" <?php //if(!empty($userData['timeforcall'])){ if($userData['timeforcall']=="7 PM-10 PM") { echo "selected"; } } ?>> 7 PM - 10 PM </option>
                        <option value="10 PM-1 AM" <?php //if(!empty($userData['timeforcall'])){ if($userData['timeforcall']=="10 PM-1 AM") { echo "selected"; } } ?>> 10 PM - 1 AM </option>
                        <option value="1 AM-4 AM" <?php //if(!empty($userData['timeforcall'])){ if($userData['timeforcall']=="1 AM-4 AM") { echo "selected"; } } ?>> 1 AM - 4 AM </option>
                        <option value="4 AM-7 AM" <?php //if(!empty($userData['timeforcall'])){ if($userData['timeforcall']=="4 AM-7 AM") { echo "selected"; } } ?>> 4 AM - 7 AM </option>
                        <option value="7 AM-10 AM" <?php //if(!empty($userData['timeforcall'])){ if($userData['timeforcall']=="7 AM-10 AM") { echo "selected"; } } ?>> 7 AM - 10 AM </option>
                    </select>

                    <?php //if(isset($signuperrors['timeforcall'])){echo "<p class='text-danger'>".$signuperrors['timeforcall']."</p>"; } ?>
                 </div> -->
               <!-- </div> -->

               <?php if(isset($type) && !empty($type)) { } else { ?>

                <div class="personalFlex">
                   <div class="form-group formPost ">
                       <label class ="lab"></label>
                      <input type="password" name="pass" id="pass" class="form-control inputAll show-password" autocomplete="off" placeholder="Password">
                      <span class="iconsAl iconsAl2">
                      <img src="<?php echo base_url(); ?>webfiles/newone/images/password.svg" onclick="passiconclick()" class="img-fluid" alt = "key">
                      </span>

                      <?php if(isset($signuperrors['pass'])){echo "<p class='text-danger'>".$signuperrors['pass']."</p>"; } ?>
                   </div>

                   <div class="form-group formPost ">
                       <label class ="lab"></label>
                      <input type="password" name="cpass" id="cpass" class="form-control inputAll show-password" autocomplete="off" placeholder="Confirm Password">
                      <span class="iconsAl iconsAl2">
                      <img src="<?php echo base_url(); ?>webfiles/newone/images/password.svg" onclick="passiconclickk()" class="img-fluid" alt = "key">
                      </span>

                      <?php if(isset($signuperrors['cpass'])){echo "<p class='text-danger'>".$signuperrors['cpass']."</p>"; } ?>
                   </div>
                </div>

               <?php } ?>

               <div class="resetPass">
                  <input type="hidden" id="signupType" name="signupType" value="<?php if(!isset($type) && empty($type)){ echo "normal"; } else { echo $type; } ?>">
                  <input type="hidden" value="" name="timeforcall">
                  <button type="submit" class="commonBtn1">Continue</button>
               </div>

            </form>

         </div>
      </div>
   </div>
</section>
<?php include_once('footer1.php'); ?>

<script type="text/javascript">
    function passiconclick() {
        if($('#pass').hasClass('show-password')) {
            
            $('#pass').attr('type','text');
            $('#pass').removeClass('show-password');
            $('#pass').addClass('hide-password');
        } else {
            $('#pass').attr('type','password');
            $('#pass').removeClass('hide-password');
            $('#pass').addClass('show-password');
        }
    }

    function passiconclickk() {
        if($('#cpass').hasClass('show-password')) {
            
            $('#cpass').attr('type','text');
            $('#cpass').removeClass('show-password');
            $('#cpass').addClass('hide-password');
        } else {
            $('#cpass').attr('type','password');
            $('#cpass').removeClass('hide-password');
            $('#cpass').addClass('show-password');
        }
    }
</script>

<script type="text/javascript">
    function referralCode() {
        var refer_code = $('#refer_code').val();
        if (refer_code != '') {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "user/checkreferral",
                data: {
                    refer_code: refer_code
                },
                success: function(data) {
                    if (data == 1) {
                        $('#refer_err').html("This Referral code does not exist");
                    } else {
                        $('#refer_err').html('');
                    }
                }
            })
        }
    } 

    // $(document).ready(function () {
    //     $(".phoneinput").on("keyup", function () {
    //         var textLength = $(this).val();
    //         if(textLength.length) {

    //         }
    //     });
    // });
    // $('.phoneinput').on('keyup keydown change', function(e){
        
    //     if ($(this).val().length > 100 
    //         && e.keyCode !== 46
    //         && e.keyCode !== 8
    //        ) {
    //        e.preventDefault();  
    //        $(this).val(100);
    //     }
    // });
</script>

<script type = "text/javascript" >
    $(document).ready(function() {
        $('#signupform').validate({
            rules: {
                fname: {
                    required: true,
                    minlength: 1,
                    maxlength: 30
                },
                lname: {
                    required: true,
                    minlength: 1,
                    maxlength: 30
                },
                email: {
                    required: true,
                    email: true,
                    maxlength: 100
                },
                phone: {
                    required: true,
                    number: true,
                    minlength: 8,
                    maxlength: 10
                },
                location: {
                    required: true,
                },
                nationality: {
                    required: true,
                },
                pass: {
                    required: true,
                    minlength: 5,
                },
                cpass: {
                    minlength: 5,
                    equalTo: '[name="pass"]'
                },
                timeforcall:{
                  required: true,
                },
                // state:{
                //   required: true,
                // },
                // city:{
                //   required: true,
                // }
            },
            messages: {
                fname: {
                    required: 'The first name field is required'
                },
                lname: {
                    required: 'The last name field is required'
                },
                email: {
                    required: 'The email filed is required',
                    remote: "This Email id is already registered.",
                    maxlength: "The email must not contain more than 100 characters"
                },
                phone: {
                    required: "The phone field is required",
                },
                location: {
                    required: "The location field is required",
                },
                nationality: {
                    required: "The nationality field is required",
                },
                pass: {
                    required: 'The password field is required',
                    minlength: 'Password must be at least 5 characters long'
                },
                cpass: {
                    required: "The confirm password field is required",
                    equalTo: " Please enter the same password"
                },
                timeforcall: {
                    required: "The Best time to call field is required",
                },
                // state: {
                //     required: "The state field is required",
                // },
                // city: {
                //     required: "The city field is required",
                // },
            }
        });
    }); 
</script>

<script>
$(document).ready(function() {
    $(".changestate").on('change', function() {
        var userstate = $(this).val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "auth/getownstate",
            data: {
                state: userstate
            },
            cache: false,
            success: function(htmldata) {
                $('.changecity').html(htmldata);
            },
            error: function() {
                console.log('error');
            }
        });
    });
});
</script>
<script type="text/javascript">
  $('#signupform').bind("keypress", function(e) {
  if (e.keyCode == 13) {               
    e.preventDefault();
    return false;
  }
});

</script>