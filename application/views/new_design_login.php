<?php include_once('header2.php'); 
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {

    } else {
        $link = "https";
        $link .= "://";
        $link .= $_SERVER['HTTP_HOST'];
        $link .= $_SERVER['REQUEST_URI'];
        redirect($link);
    }
?>

<section>
   <div class="BannerArea" style="background-image: url('<?php echo base_url(); ?>webfiles/img/newmap.jpg');">
      <h1>Login  </h1>
   </div>
</section>
<section class="loginAll">
   <div class="container">
      <div class="row">
         <div class="col-sm-8">
            <div class="loginLeft">
               <img src="<?php echo base_url(); ?>webfiles/newone/images/bglogin.svg" class="img-fluid" alt = "bglogin">
            </div>
         </div>
         <div class="col-sm-4">
            <form method="post">
                
                <p id="errorfield" class="text-danger text-center text-center1"></p>
                <p id="errorfield1" class="text-success text-center errorfield1"></p>

                <?php if(isset($signupsuccess['success'])){echo "<p class='text-center text-success'>".$signupsuccess['success']."</p>"; } ?>

               <div class="loginRight">
                  <h2 class="head">Sign In</h2>
                  <div class="signForm">
                     <div class="form-group formPost">
                         <label class="lab"></label>
                        <input type="text" name="email" id="loginemail" class="form-control inputAll" value="<?php if(!empty($_COOKIE['emaill'])){echo $_COOKIE['emaill'];}?>" placeholder="Enter Email" required="required">
                        <span class="iconsAl"><img src="<?php echo base_url(); ?>webfiles/newone/images/message.svg" class="img-fluid" alt = "message"></span>
                     </div>
                     <div class="form-group formPost">
                           <label class="lab"></label>
                        <input type="password" name="password" id="loginpass" value="<?php if(!empty($_COOKIE['passwordd'])){echo $_COOKIE['passwordd'];}?>" class="form-control inputAll show-password" placeholder="Enter Password" required="required">
                        <span class="iconsAl"><img src="<?php echo base_url(); ?>webfiles/newone/images/password.svg" class="img-fluid" onclick="passiconclick()" alt = "password"></span>
                     </div>
                     <div class ="rememberAl">
                        <div class="new">
                           <div class="form-group">
                              <input type="checkbox" class="form-check-input" id="check1" name="option1" value="1" <?php if(!empty($_COOKIE['checkval'])){echo 'checked';}?>> 
                              <label for="check1" class="remember">Remember me</label>
                           </div>
                        </div>
                        <div class="forget">
                           <a href="<?php echo base_url(); ?>forgot_password">Forgot Password?</a>
                        </div>
                     </div>
                  </div>
                  <div class="loginBtn">
                     <button type="button" class="btnCom" id="loginjob" onclick="loginnn()">Login</button>
                  </div>
                  <div class="accountInfo">
                     <p>Don't have an account? <a href="<?php echo base_url(); ?>signup">Sign Up</a></p>
                     <span class="or">Or</span>
                  </div>
                  <div class="socialIcon">
                     <ul>
                        <li><a href="<?php if(!empty($authUrl)) echo $authUrl; ?>"><img src="<?php echo base_url(); ?>webfiles/newone/images/facebook.svg" class="img-fluid" alt = "icon">
                           </a>
                        </li>
                        <li><a href="<?php if(!empty($loginURL)) echo $loginURL;?>"><img src="<?php echo base_url(); ?>webfiles/newone/images/gogle.svg" class="img-fluid" alt = "icon">
                           </a>
                        </li>
                     </ul>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</section>

<script type="text/javascript">
    function loginnn() {
        var email = $("#loginemail").val();
        var pass = $("#loginpass").val();
        var checkval = $('#check1:checked').val();
        var cur_lat = $('#cur_lat').val();
        var cur_long = $('#cur_long').val();
        var currentURL = "<?php echo $this->session->userdata('previous_url_again'); ?>";
        //var currentURL = "";
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "auth/logincheck",
            data: {
                email: email,
                password: pass,
                rememberval: checkval,
                cur_lat: cur_lat,
                cur_long: cur_long
            },
            success: function(data) {
                //console.log(data);
                var result = JSON.parse(data);
                if(result.success == "logindone") {
                    if(result.url.length > 0) {
                      window.location = result.url;
                    } else {
                      window.location = "<?php echo base_url();?>/resume";
                    }
                    exit();

                }else if(result.success == "redirect") {
                    window.location = result.url;
                    exit();
                } else {
                    $('p.text-center1').text(result.error);
                }
            },
            error: function() {
                console.log('error');
            }
        });
    } 
</script>
<script type="text/javascript">
    function passiconclick() {
        if($('#loginpass').hasClass('show-password')) {
            
            $('#loginpass').attr('type','text');
            $('#loginpass').removeClass('show-password');
            $('#loginpass').addClass('hide-password');
        } else {
            $('#loginpass').attr('type','password');
            $('#loginpass').removeClass('hide-password');
            $('#loginpass').addClass('show-password');
        }
    }
</script>

<?php include_once('footer1.php'); ?>

