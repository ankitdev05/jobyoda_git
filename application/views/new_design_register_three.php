<?php include_once('header2.php'); 
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {

    } else {
        $link = "https";
        $link .= "://";
        $link .= $_SERVER['HTTP_HOST'];
        $link .= $_SERVER['REQUEST_URI'];
        redirect($link);
    }
?>

<style>
   .otpRight h2 {
    font-size: 19px!important;
   }
</style>
<section>
   <div class="BannerArea" style="background-image: url('<?php echo base_url(); ?>webfiles/img/newmap.jpg');">
      <h1> Other Details </h1>
   </div>
</section>

<section class="personalDetail">
   <div class="container">
      <div class="col-md-7 col-sm-6">
         <div class="personalSteps" style="margin-top: 15%;">
            <div class="bgPersonal1">
               <img src="<?php echo base_url(); ?>webfiles/newone/images/detial.svg" class="img-fluid" alt = "step" style="width: 70%;">
            </div>
            <p class="bgBottom">"<i>Don't be afraid of change. You may lose something good, but you may gain something better.</i>"</p>
         </div>
      </div>
      <div class="col-md-5 col-sm-6" >
         
         <form method="post" action="<?php echo base_url();?>auth/otherdetailupdate" id="signupform">

            <div class="personalRight otpRight">
               <h2 class="head">Let’s Check if you are Work From Home Ready!</h2>
               <!-- <div class="form-group formPost iconDrop">
                   <label class="lab"></label>
                  <input type="text" name="haveinternet" class="form-control inputAll staleAll" value="<?php //if(!empty($userData['haveinternet'])){ echo $userData['haveinternet']; }?>" placeholder="Do you have a fixed internet connection?" readonly>
                  <span class="iconsAl iconsAl2 arrowbtn">
                  <img src="<?php //echo base_url(); ?>webfiles/newone/images/arrow.svg" class="img-fluid" alt = "arrow">
                  </span>
                  <div class="dropAl">
                     <ul>
                        <li>Yes</li>
                        <li>No</li>
                     </ul>
                  </div>

                  <?php //if(isset($signuperrors['haveinternet'])){echo "<p class='text-danger'>".$signuperrors['haveinternet']."</p>"; } ?>
               </div> -->

               <div class="form-group formPost iconDrop">
                     <label class="lab"></label>
                      <span class="iconsArrow"> <img src="<?php echo base_url(); ?>webfiles/newone/images/arrow.svg" class="img-fluid" alt="icon"></span>
                    <select name="jobsearch" class="form-control inputAll" placeholder ="Select Job Search Status ">
                        <option value="">Select Job Search Status </option>
                        <option value="1" <?php if(!empty($userData['jobsearch']) && $userData['jobsearch'] == 1){ echo "selected"; }?>> Actively seeking </option>
                        <option value="2" <?php if(!empty($userData['jobsearch']) && $userData['jobsearch']  == 2){ echo "selected"; }?>> Open to offers </option>
                        <option value="3" <?php if(!empty($userData['jobsearch']) && $userData['jobsearch']  == 3){ echo "selected"; }?>> Exploring </option>
                    </select>

                    <?php if(isset($signuperrors['jobsearch'])){echo "<p class='text-danger'>".$signuperrors['jobsearch']."</p>"; } ?>
                 </div>
                 
               <div class="form-group formPost iconDrop">
                    <label class="lab"></label>
                  <input type="text" name="internetspeed" class="form-control inputAll staleAll" value="<?php if(!empty($userData['internetspeed'])){ echo $userData['internetspeed']; }?>" placeholder="Are you Work@Home Ready with High Speed Internet?" readonly>
                  <span class="iconsAl iconsAl2 arrowbtn">
                  <img src="<?php echo base_url(); ?>webfiles/newone/images/arrow.svg" class="img-fluid" alt = "arrow">
                  </span>
                  <div class="dropAl">
                     <ul>
                        <li>Yes</li>
                        <li>No</li>
                     </ul>
                  </div>

                  <?php if(isset($signuperrors['internetspeed'])){echo "<p class='text-danger'>".$signuperrors['internetspeed']."</p>"; } ?>
               </div>

               <div class="form-group formPost iconDrop">
                    <label class="lab"></label>
                  <input type="number" name="current_salary" class="form-control inputAll" value="<?php if(!empty($userData['current_salary'])){ echo $userData['current_salary']; }?>" placeholder="Current Salary">

                  <?php if(isset($signuperrors['current_salary'])){echo "<p class='text-danger'>".$signuperrors['current_salary']."</p>"; } ?>
               </div>

               <div class="form-group formPost iconDrop">
                  <label class="lab"></label>
                  <span class="iconsAl iconsAl2 arrowbtn">
                  <img src="<?php echo base_url(); ?>webfiles/newone/images/arrow.svg" class="img-fluid" alt = "arrow">
                  </span>
                  <select name="timeforcall" class="form-control inputAll" placeholder ="Select Best time to call">
                        <option value="" class="demo">Select Best time to call</option>
                        <option value="9 AM-12 PM" <?php if(!empty($userData['timeforcall'])){ if($userData['timeforcall']=="9 AM-12 PM") { echo "selected"; } } ?>> 9 AM - 12 PM </option>
                        <option value="12 PM-3 PM" <?php if(!empty($userData['timeforcall'])){ if($userData['timeforcall']=="12 PM-3 PM") { echo "selected"; } } ?>> 12 PM - 3 PM </option>
                        <option value="3 PM-6 PM" <?php if(!empty($userData['timeforcall'])){ if($userData['timeforcall']=="3 PM-6 PM") { echo "selected"; } } ?>> 3 PM - 6 PM </option>
                        <option value="6 PM-9 PM" <?php if(!empty($userData['timeforcall'])){ if($userData['timeforcall']=="6 PM-9 PM") { echo "selected"; } } ?>> 6 PM - 9 PM </option>
                        <option value="9 PM-12 AM" <?php if(!empty($userData['timeforcall'])){ if($userData['timeforcall']=="9 PM-12 AM") { echo "selected"; } } ?>> 9 PM - 12 AM </option>
                        <option value="12 AM-3 AM" <?php if(!empty($userData['timeforcall'])){ if($userData['timeforcall']=="12 AM-3 AM") { echo "selected"; } } ?>> 12 AM - 3 AM </option>
                        <option value="3 AM-6 AM" <?php if(!empty($userData['timeforcall'])){ if($userData['timeforcall']=="3 AM-6 AM") { echo "selected"; } } ?>> 3 AM - 6 AM </option>
                        <option value="6 AM-9 AM" <?php if(!empty($userData['timeforcall'])){ if($userData['timeforcall']=="6 AM-9 AM") { echo "selected"; } } ?>> 6 AM - 9 AM </option>
                    </select>

                  <?php if(isset($signuperrors['timeforcall'])){echo "<p class='text-danger'>".$signuperrors['timeforcall']."</p>"; } ?>
               </div>

               <div class="form-group formPost iconDrop">
                  <label class="lab"></label>
                  <input type="text" name="work_mode" class="form-control inputAll staleAll" value="<?php if(!empty($userData['work_mode'])){ echo $userData['workmode']; }?>" placeholder="What is your preferred work mode?" readonly>
                  <span class="iconsAl iconsAl2 arrowbtn">
                  <img src="<?php echo base_url(); ?>webfiles/newone/images/arrow.svg" class="img-fluid" alt = "arrow">
                  </span>
                  <div class="dropAl">
                     <ul>
                        <li>Onsite</li>
                        <li>Work At Home</li>
                        <li>Either as long as I get a job</li>
                     </ul>
                  </div>

                  <?php if(isset($signuperrors['work_mode'])){echo "<p class='text-danger'>".$signuperrors['work_mode']."</p>"; } ?>
               </div>

               <div class="form-group formPost iconDrop">
                  <label class="lab"></label>
                  <input type="text" name="vaccination" class="form-control inputAll staleAll" value="<?php if(!empty($userData['vaccination'])){ echo $userData['vaccination']; }?>" placeholder="Vaccination Status?" readonly>
                  <span class="iconsAl iconsAl2 arrowbtn">
                  <img src="<?php echo base_url(); ?>webfiles/newone/images/arrow.svg" class="img-fluid" alt = "arrow">
                  </span>
                  <div class="dropAl">
                     <ul>
                        <li>Not Vaccinated</li>
                        <li>1st dose completed</li>
                        <li>Fully Vaccinated</li>
                     </ul>
                  </div>

                  <?php if(isset($signuperrors['vaccination'])){echo "<p class='text-danger'>".$signuperrors['vaccination']."</p>"; } ?>
               </div>

               <div class="form-group formPost iconDrop">
                  <label class="lab"></label>
                  <input type="text" name="relocate" class="form-control inputAll staleAll" value="<?php if(!empty($userData['relocate'])){ echo $userData['relocate']; }?>" placeholder="Are you willing to relocate?" readonly>
                  <span class="iconsAl iconsAl2 arrowbtn">
                  <img src="<?php echo base_url(); ?>webfiles/newone/images/arrow.svg" class="img-fluid" alt = "arrow">
                  </span>
                  <div class="dropAl">
                     <ul>
                        <li>Yes</li>
                        <li>No</li>
                     </ul>
                  </div>

                  <?php if(isset($signuperrors['relocate'])){echo "<p class='text-danger'>".$signuperrors['relocate']."</p>"; } ?>
               </div>

               <div class="resetPass">
                  <button type="submit" class="commonBtn1">Continue</button>
               </div>
            </div>

         </form>

      </div>
   </div>
</section>
<?php include_once('footer1.php'); ?>

<div id="myModalPopup" class="modal fade">
    <div class="modal-dialog" style="width: 40%;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" style="font-size: 26px;font-weight: 700;">Congratulations</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body text-center">
                  
                  <img src="<?php echo base_url(); ?>webfiles/newone/images/congru.svg" class="img-fluid" alt = "congru">

                  <p style="font-weight: 500;font-size: 16px;">Your sign up process is complete. <br> A few more questions to check if you are Work From Home ready.</p>
                
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $("#myModalPopup").modal('show');
    });
</script>