<?php include_once('header2.php'); 
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {

    } else {
        $link = "https";
        $link .= "://";
        $link .= $_SERVER['HTTP_HOST'];
        $link .= $_SERVER['REQUEST_URI'];
        redirect($link);
    }
?>


	<section>
        <div class="BannerArea" style="background-image: url('https://jobyoda.com/webfiles/img/newmap.jpg');">
            <h1>FAQs  </h1> 
        </div>
    </section>

 
 
    <section>
        <div class="FaqArea">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="FaqLeft">
                            <div class="panel-group" id="accordion">
                                <div class="panel">
                                <?php
                                    $xx=1;
                                    foreach($content as $cont) {
                                ?>

                                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapse<?php echo $xx; ?>">
                                        <h4> <?php echo $cont['title']; ?> </h4>
                                    </div>
                                    <div id="collapse<?php echo $xx; ?>" class="panel-collapse collapse <?php if($xx == 1){ ?> in <?php } ?>">
                                        <div class="panel-body">
                                            <p> <?php echo $cont['content']; ?> </p>
                                        </div>
                                    </div>
                                <?php
                                    $xx++;
                                    }
                                ?>
                                </div> 

                            </div> 
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </section>

<?php include_once('footer1.php'); ?>