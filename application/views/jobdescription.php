<?php 
   include_once ('header.php'); 
   $this->session->set_userdata('previous_url_again', current_url());
   if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {
   
       } else {
           $link = "https";
           $link .= "://";
           $link .= $_SERVER['HTTP_HOST'];
           $link .= $_SERVER['REQUEST_URI'];
           redirect($link);
       }
   ?>
<style type="text/css">
   .modal-opennn {
   overflow: hidden;
   }
   .modal-opennn .modal{
   overflow-x: hidden!important;
   overflow-y: auto!important;
   }
   .JobDescriptionScroll{float: left; width: 100%; height: 400px; overflow: auto; margin: 0 0 50px 0; padding: 0 20px 0 0;}
   .managerpart .jobdeskyod .jobtxtsdesc.rfed.fullph .greenbtn{background-color:#fbaf31;color:#fff;padding:9px 25px;border-radius:5px;font-family:Roboto;font-size:14px;display:inline-block; box-shadow: none;}
   .managerpart .jobdeskyod .jobtxtsdesc.rfed.fullph .greenbtn:hover{background-color:#00a94f}
   .jobtxtsdesc.picsd .provd.usericonds p{
   font-family: Roboto;
   font-size: 12px;
   font-weight: 500;
   letter-spacing: 0.4px;
   margin: 15px 0 0 0 !important;
   line-height: 20px;
   }
   .jobtxtsdesc p {
   font-size: 14px;
   color: #787878;
   font-family: Roboto;
   line-height: 26px;
   }
   .InstantBox{
   padding: 30px;
   }
   .InstantBox button{
   position: absolute;
   top: -15px;
   right: -15px;
   width: 30px;
   height: 30px;
   background-color: #00a94f;
   opacity: 1;
   text-shadow: none;
   color: #fff;
   border-radius: 50%;
   font-size: 16px;
   }
   .InstantBox h5{
   font-family: Roboto;
   font-size: 14px;
   font-weight: 400;
   line-height: 25px;
   text-align: center;
   color: #000;
   margin: 0 0 10px 0;
   }
   .InstantBox h5 span{
   display: block;
   font-weight: 500;
   }
   .InstantBox p{
   margin: 0;
   text-align: center;
   }
   .InstantBox p a{
   background-color: #00a94f;
   color: #fff;
   padding: 7px 25px;
   border-radius: 5px;
   font-family: Roboto;
   font-size: 14px;
   display: inline-block;
   box-shadow: none;
   }
   .InstantBox p a:hover{
   background-color: #fbaf31;
   }
   .chatbotlink{
   font-size: 15px!important;
   background-color: #fbaf31!important;
   color: #fff!important;
   padding: 10px!important;
   }
   .addupdatecent p {
      padding:0px!important;
   }
   #okup{
    border: 1px solid #ccc!important;
    padding: 3px 21px!important;
    background: #27aa60!important;
    color: #000!important;
    margin-top: 20px!important;
   }

   .applijoboader{
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    background-color: #ffffffa3;
    height: 100%;
    z-index: 999;
    text-align: center;
    display: none;
}
.sub_applijoboader {
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
}

</style>

<div class="applijoboader">
  <div class="sub_applijoboader">
    <img src="https://jobyoda.com/webfiles/newone/loadericon.gif" style="width:100px">
  </div>
</div>

<div class="managerpart">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-9 expanddiv jobdeskyod" style="    margin-bottom: 40px;">
            <div class="innerbglay">
               <div class="mainheadings-yoda">
               </div>
               <div class="adminopnts">
                  <div class="row">
                     <div class="col-md-6">
                        <div class="adminopnts" style="box-shadow: none;">
                           <div class="jobtxtsdesc comprofilepicent">
                              <section id="demos" class="agentssliders propertcomps">
                                 <div class="owl-carousel owl-theme">
                                    <?php
                                       if(!empty($jobImg)){
                                        foreach ($jobImg as $images) {
                                        
                                       ?>
                                    <div class="item">
                                       <div class="innersliders" style="margin: 0; padding:0;">
                                          <img src="<?php echo $images['pic']; ?>">
                                       </div>
                                    </div>
                                    <?php }}else{ ?>
                                    <div class="item">
                                       <div class="innersliders">
                                          <img src="<?php echo base_url();?>recruiterupload/IMG_20190712_000134_724.jpg">
                                       </div>
                                    </div>
                                    <?php }?>
                                 </div>
                              </section>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="adminopnts jbdtprt">
                           <div class="jobsetstype">
                              <div class="jobinfodtls">
                                 <p class="positionlok"><?php echo $jobdetail['job_title']; ?></p>
                                 <p class="organisationm"><a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($comapnyDetail['comapnyId']);}else{ echo base_url();?>site_details/<?php echo base64_encode($comapnyDetail['comapnyId']); }?>"><?php echo $comapnyDetail['name']; ?></a></p>
                                 <!-- <p class="organisationm recnmes"><?php echo $comapnyDetail['recruiterName']; ?></p> -->
                                 <p class="organisationm recnmes"> <a href="<?php if (!empty($userSess)) { echo base_url();?>company_details/<?php echo base64_encode($comapnyDetail['recruiter_id']);}else{ echo base_url();?>company_details/<?php echo base64_encode($comapnyDetail['recruiter_id']); }?>"> <?php echo $comapnyDetail['recruiterName']; ?> </a> </p>
                              </div>
                          

                           </div>
                           <div class="customesets">
                              <div class="provd locnwdth">
                                 <img src="<?php echo base_url() . 'webfiles/'; ?>img/mapsm.png"> 
                                 <p><?php echo $jobdetail['location']; ?></p>
                              </div>
                              <div class="provd">
                                 <img src="<?php echo base_url() . 'webfiles/'; ?>img/openings.png"> 
                                 <p><?php echo $jobdetail['opening']; ?><?php if($jobdetail['opening']>1){ echo " Openings"; }else{ echo " Opening"; } ?></p>
                              </div>
                              <div class="provd">
                                 <img src="<?php echo base_url() . 'webfiles/'; ?>img/bagsm.png"> 
                                 <p><?php echo $jobdetail['experience']; ?></p>
                              </div>
                              <div class="provd">
                                 <img src="<?php echo base_url() . 'webfiles/'; ?>img/salsm.png">
                                 <?php
                                    $total_sal = $jobdetail['salary']??0 + $jobdetail['allowances']??0;
                                       $allow = substr($total_sal, 0, 2);
                                       if (!empty($allow)) {
                                       ?>
                                 <p><?php echo $allow; ?>k/Month</p>
                                 <?php
                                    }else{ ?>
                                 <p>Confidential</p>
                                 <?php }?>
                              </div>
                           </div>
                           <div class="jobtxtsdesc basictextsal">
                              <p>Basic Salary : <?php 
                                 if (!empty($jobdetail['salary'])) {echo $jobdetail['salary']; 
                                 ?>/Month <?php }else{ echo "Confidential"; }?></p>
                              <p>Total Guaranteed Allowance : <?php if(!empty($jobdetail['allowances'])){ echo $jobdetail['allowances']; ?>/Month <?php }else{ echo "Confidential"; }?>  </p>
                           </div>
                           <?php if($jobdetail['status']=='1')
                              {
                                  $status_save =" Got Hired? Write to help@jobyoda.com  to claim your free Venti coffee. *T&C Apply";
                              }else if($jobdetail['status']=='2')
                              {
                                  $status_save ="Your Application Status is 'No Show'";
                              }
                              else if($jobdetail['status']=='3')
                              {
                                  $status_save = "Your Application Status is 'Fall Out'";
                              }
                              else if($jobdetail['status']=='4')
                              {
                                $status_save = "Your Application Status is 'Refer'";
                              }else if($jobdetail['status']=='5')
                              {
                                $status_save= "Your Application Status is 'On Going Application'";
                              }else if($jobdetail['status']=='6')
                              {
                                $status_save = "Your Application Status is 'Accepted JO'";
                              }else if($jobdetail['status']=='7')
                              {
                                $status_save = "Your Application Status is 'Hired'";
                              }
                              ?>   
                           <?php if($jobdetail['status']>0)
                              {?> 
                           <div class="jobtxtsdesc rfed">
                              <button class="greenbtn" type="button"><?php echo $status_save ;?></button>
                           </div>
                           <?php }else{?>
                           <div class="jobtxtsdesc rfed fullph">
                              <div class="contopsd">
                                 <!-- <h4>Contact</h4>
                                    <div class="pincts">
                                    <?php //if(!empty($jobdetail['companyphone'])){ ?>
                                    <p><a href='tel:" <?php //echo '+'. $jobdetail['phonecode'].' '. $jobdetail['companyphone']; ?>"'><img src="https://jobyoda.com/webfiles/img/phonetag.png" class="acthover" style="width: 20px;"><?php //echo '+'. $jobdetail['phonecode'].' '. $jobdetail['companyphone']; ?></a></p>
                                    <?php //}?>
                                    <?php //if(!empty($jobdetail['landline'])){?>
                                      <p><a href='tel:" <?php //echo $jobdetail['landline']; ?>"'><img src="https://jobyoda.com/webfiles/img/conta.png" class="acthover" style="width: 20px;"><?php //if(!empty($jobdetail['landline'])){ echo $jobdetail['landline']; }?></a></p>
                                    <?php //}?>
                                     <?php //if(!empty($jobdetail['email'])){?>
                                    <p><a href='mailto:" <?php //echo $jobdetail['email']; ?>"'><img src="https://jobyoda.com/webfiles/img/mail-2.png" class="acthover" style="width: 20px;"><?php //echo $jobdetail['email']; ?></a></p>
                                    <?php //}?>
                                    </div> -->
                              </div>
            
                               
                                             
                              <div class="newShareBtns newShareBtns22">
                              
                              <form id="formschedule">

                                <input type="hidden" name="scheduledate"> 
                                <input type="hidden" name="scheduletime"> 
                                <input type="hidden" name="listing" id="jobId"  value="<?php echo base64_decode($_GET['listing']); ?>">

                                <?php 
                                   if($jobdetail['mode'] == "Instant screening") {
                                ?>
                                      <button type="button" class="greenbtn" data-toggle="modal" data-target="#exampleModalCenterInstant"><?php if($jobdetail['chatbot'] == 0) { if($jobdetail['mode'] == "Call") { echo "Apply for Phone Screening"; } else { echo 'Apply for '. $jobdetail['mode']; } } else { echo "Continue Chatbot Interview"; } ?> </button>
                                <?php
                                   } else {
                                ?>
                                      <button class="greenbtn" id="schedule"> <?php if($jobdetail['chatbot'] == 0) { if($jobdetail['mode'] == "Call") { echo "Apply for Phone Screening"; } else { echo 'Apply for '. $jobdetail['mode']; } } else { echo "Continue Chatbot Interview"; } ?> </button>
                                <?php } ?>

                              </form>
                                  
                                  <div class="sharebuttoncustom sharebuttoncustom2">
                                    <?php 

                                       $shareurl = base_url()."job/description/". base64_encode($jobdetail['jobpost_id']);
                                       
                                       $shareargs = ["url"=>$shareurl, "title"=>$jobdetail['job_title'], "image"=>$jobImg[0]['pic'], "desc"=>$jobdetail['job_title'], "redirecturl"=>$shareurl];



                                       $sharelinks = withShareLinks($shareargs);

                                    ?>                                                 
                                      <h6 class="yellow">
                                          <span class="number"><?php echo $jobdetail['sharecount']; ?></span><span class="name">Shares</span><i class="fa fa-share-alt"></i></h6>

                                      <div class="sharelinkclass sharelinkclass22">
                                        <ul>
                                            <li><a onclick='sharecountajax("<?php echo $jobdetail['jobpost_id']; ?>", "<?php echo $sharelinks['facebook']; ?>")' href="javascript:void(0)" title="Facebook Share"><img src="<?php echo base_url();?>webfiles/newone/social/facebook.png"></a></li>

                                            <li> <a onclick='sharecountajax("<?php echo $jobdetail['jobpost_id']; ?>", "<?php echo $sharelinks['twitter']; ?>")' href="javascript:void(0)" title="Twitter Share"><img src="<?php echo base_url();?>webfiles/newone/social/twitter.png"></a></li>

                                            <li> <a onclick='sharecountajax("<?php echo $jobdetail['jobpost_id']; ?>", "<?php echo $sharelinks['linkedin']; ?>")' href="javascript:void(0)" title="LinkedIn Share"><img src="<?php echo base_url();?>webfiles/newone/social/linkedin.png"></a></li>

                                            <li> <a onclick='sharecountajax("<?php echo $jobdetail['jobpost_id']; ?>", "<?php echo $sharelinks['gmail']; ?>")' href="javascript:void(0)" title="Gmail Share"><img src="<?php echo base_url();?>webfiles/newone/social/gmail.png"></a></li>
                                            
                                            <li> <a onclick='sharecountajax("<?php echo $jobdetail['jobpost_id']; ?>", "<?php echo $sharelinks['whatsapp']; ?>")' href="javascript:void(0)" title="Whatsapp Share"><img src="<?php echo base_url();?>webfiles/newone/social/whatsapp.png"></a></li>
                                            
                                        </ul>
                                      </div>

                                   </div>
                                  </div>
                           </div>
                           <?php }?>
                            
                        </div>
                     </div>
                  </div>
               </div>
               <div class="adminopnts jobdtlfs">
                  <div class="row">
                     <div class="col-sm-6">
                        <div class="JobDescriptionScroll">
                           <div class="jobtxtsdesc">
                              <h4>Job Pitch</h4>
                              <p><?php echo $jobdetail['jobPitch']; ?></p>
                           </div>
                           <div class="jobtxtsdesc">
                              <h4>Job Description</h4>
                              <p>
                                <?php 
                                  $jobdetail['jobDesc'] = preg_replace('/\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', '', $jobdetail['jobDesc']);
                                  
                                  $jobdetail['jobDesc'] = preg_replace('/([0-9]+[\- ]?[0-9]+)/', '', $jobdetail['jobDesc']);

                                    echo $jobdetail['jobDesc']; 
                                ?>        
                              </p>
                           </div>
                           <?php if(!empty($jobskills)){?>
                           <div class="jobtxtsdesc">
                              <h4>Skill Required</h4>
                              <ul>
                                 <?php
                                    foreach ($jobskills as $skills) {
                                    ?>
                                 <li><?php echo $skills['skill']; ?></li>
                                 <?php
                                    }
                                    ?>
                              </ul>
                           </div>
                           <?php }?>
                           <?php if(!empty($jobdetail['education'])){?>
                           <div class="jobtxtsdesc">
                              <h4>Qualification Required</h4>
                              <ul>
                                 <li><?php echo $jobdetail['education']; ?></li>
                              </ul>
                           </div>
                           <?php }?>
                           
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="JobDescriptionScroll">
                          <?php if (!empty($jobTop)) {?>
                           <div class="jobtxtsdesc picsd">
                              <h4>Top Picks</h4>
                              <div class="provd usericonds">
                                 <?php 
                                    foreach($jobTop as $jobTops) {
                                      if ($jobTops['picks_id'] == '1') {
                                    ?>
                                 <li>
                                    <div class="icodtds">
                                       <img title="Joining Bonus" src="<?php echo base_url(); ?>webfiles/img/toppics/joining-bonus.png">
                                    </div>
                                    <p>Joining <br> Bonus</p>
                                 </li>
                                 <?php
                                    }
                                    if ($jobTops['picks_id'] == '2') {
                                    ?>
                                 <li>
                                    <div class="icodtds"><img title="Free Food" src="<?php echo base_url(); ?>webfiles/img/toppics/free-food.png">
                                    </div>
                                    <p>Free <br>Food</p>
                                 </li>
                                 <?php
                                    }
                                    if ($jobTops['picks_id'] == '3') {
                                    ?>
                                 <li>
                                    <div class="icodtds"><img title="Day 1 HMO" src="<?php echo base_url(); ?>webfiles/img/toppics/day-1-hmo.png"></div>
                                    <p>Day 1 <br>HMO</p>
                                 </li>
                                 <?php
                                    }
                                    if ($jobTops['picks_id'] == '4') {
                                    ?>
                                 <li>
                                    <div class="icodtds"><img title="Day 1 HMO for Dependent" src="<?php echo base_url(); ?>webfiles/img/toppics/day-1-hmo-for-depended.png">
                                    </div>
                                    <p>Day 1 HMO <br>for Dependent</p>
                                 </li>
                                 <?php
                                    }
                                    if ($jobTops['picks_id'] == '5') {
                                    ?>
                                 <li>
                                    <div class="icodtds"><img title="Day Shift" src="<?php echo base_url(); ?>webfiles/img/toppics/day-shift.png"></div>
                                    <p>Day <br>Shift</p>
                                 </li>
                                 <?php
                                    }
                                    if ($jobTops['picks_id'] == '6') {
                                    ?>
                                 <li>
                                    <div class="icodtds"><img title="14th Month Pay" src="<?php echo base_url(); ?>webfiles/img/toppics/14-month-pay.png"></div>
                                    <p>14th Month<br> Pay</p>
                                 </li>
                                 <?php
                                    }
                                    }
                                    
                                    ?>
                              </div>
                           </div>
                           <?php  } if (!empty($jobAllowance)) {?>
                           <div class="jobtxtsdesc picsd">
                              <h4>Allowances and Incentives</h4>
                              <div class="provd usericonds">
                                 <?php
                                    foreach($jobAllowance as $jobAllowances) {
                                       if ($jobAllowances['allowances_id'] == '1') {
                                    ?>
                                 <li>
                                    <div class="icodtds">  <img title="Cell Phone Allowances" src="<?php echo base_url(); ?>webfiles/img/toppics/cell-phone-allowance.png">
                                    </div>
                                    <p>Cell Phone <br>Allowance</p>
                                 </li>
                                 <?php
                                    }
                                    if ($jobAllowances['allowances_id'] == '2') {
                                    ?>
                                 <li>
                                    <div class="icodtds"><img title="Annual Performance Bonus" src="<?php echo base_url(); ?>webfiles/img/toppics/annual--performance-bonus.png"></div>
                                    <p>Annual <br>Performance Bonus</p>
                                 </li>
                                 <?php
                                    }
                                    if ($jobAllowances['allowances_id'] == '3') {
                                    ?>
                                 <li>
                                    <div class="icodtds"> <img title="Free Parking" src="<?php echo base_url(); ?>webfiles/img/toppics/free-parking.png"></div>
                                    <p>Free <br>Parking</p>
                                 </li>
                                 <?php
                                    }
                                    if ($jobAllowances['allowances_id'] == '4') {
                                    ?>
                                 <li>
                                    <div class="icodtds"><img title="Free Shuttle" src="<?php echo base_url(); ?>webfiles/img/toppics/free-shuttle.png"></div>
                                    <p>Free<br> Shuttle</p>
                                 </li>
                                 <?php
                                    }
                                    if ($jobAllowances['allowances_id'] == '5') {
                                    ?>
                                 <li>
                                    <div class="icodtds"> <img title="Retirement Benefits" src="<?php echo base_url(); ?>webfiles/img/toppics/reteirment-benifits.png"></div>
                                    <p>Retirement <br> Benefits</p>
                                 </li>
                                 <?php
                                    }
                                    if ($jobAllowances['allowances_id'] == '6') {
                                    ?>
                                 <li>
                                    <div class="icodtds"><img title="Transport Allowance" src="<?php echo base_url(); ?>webfiles/img/toppics/transportation.png"></div>
                                    <p>Transport <br> Allowance</p>
                                 </li>
                                 <?php
                                    }
                                    if ($jobAllowances['allowances_id'] == '7') {
                                    ?>
                                 <li>
                                    <div class="icodtds"><img title="Monthly Performance Incentive" src="<?php echo base_url(); ?>webfiles/img/toppics/monthly-performance-incentive.png">
                                    </div>
                                    <p>Monthly <br> Performance Incentive</p>
                                 </li>
                                 <?php
                                    }
                                    }
                                    
                                    ?>
                              </div>
                           </div>
                           <?php } if (!empty($jobMedical)) {?>
                           <div class="jobtxtsdesc picsd">
                              <h4>Medical Benefits</h4>
                              <div class="provd usericonds">
                                 <?php
                                    foreach($jobMedical as $jobMedicals) {
                                       if ($jobMedicals['medical_id'] == '1') {
                                    ?>
                                 <li>
                                    <div class="icodtds"><img title="Medicine Reimbursement" src="<?php echo base_url(); ?>webfiles/img/toppics/medicine-reimbursemer.png"></div>
                                    <p>Medicine <br> Reimbursement</p>
                                 </li>
                                 <?php
                                    }
                                    if ($jobMedicals['medical_id'] == '2') {
                                    ?>
                                 <li>
                                    <div class="icodtds"><img title="Free HMO for Dependents Allowances" src="<?php echo base_url(); ?>webfiles/img/toppics/free-hmo-for-dependents.png">
                                    </div>
                                    <p>Free HMO for <br>Dependents Allowances</p>
                                 </li>
                                 <?php
                                    }
                                    if ($jobMedicals['medical_id'] == '3') {
                                    ?>
                                 <li>
                                    <div class="icodtds"> <img title="Cretical illness Benefits" src="<?php echo base_url(); ?>webfiles/img/toppics/critical-illness-benefits.png"></div>
                                    <p>Cretical <br> illness Benefits</p>
                                 </li>
                                 <?php
                                    }
                                    if ($jobMedicals['medical_id'] == '4') {
                                    ?>
                                 <li>
                                    <div class="icodtds"><img title="Life Insurance" src="<?php echo base_url(); ?>webfiles/img/toppics/life-insurence.png"></div>
                                    <p>Life Insurance</p>
                                 </li>
                                 <?php
                                    }
                                    if ($jobMedicals['medical_id'] == '5') {
                                    ?>
                                 <li>
                                    <div class="icodtds"><img title="Maternity Assistance" src="<?php echo base_url(); ?>webfiles/img/toppics/maternity-assistance.png"></div>
                                    <p>Maternity Assistance</p>
                                 </li>
                                 <?php
                                    }
                                    }
                                    
                                    ?>
                              </div>
                           </div>
                           <?php  }
                              if (!empty($jobShift) || !empty($jobLeaves)) {?>
                           <div class="jobtxtsdesc picsd">
                              <h4>Work Shift/Schedule</h4>
                              <div class="provd usericonds">
                                 <?php
                                    if (!empty($jobShift)) {
                                    foreach($jobShift as $jobShifts) {
                                       if ($jobShifts['workshift_id'] == '1') {
                                    ?>
                                 <li>
                                    <div class="icodtds"><img title="Mid Shift" src="<?php echo base_url(); ?>webfiles/img/toppics/mid-shift.png"></div>
                                    <p>Mid Shift</p>
                                 </li>
                                 <?php
                                    }
                                    if ($jobShifts['workshift_id'] == '2') {
                                    ?>
                                 <li>
                                    <div class="icodtds"><img title="Night Shift" src="<?php echo base_url(); ?>webfiles/img/toppics/night-shift.png"></div>
                                    <p>Night Shift</p>
                                 </li>
                                 <?php
                                    }
                                    if ($jobShifts['workshift_id'] == '3') {
                                    ?>
                                 <li>
                                    <div class="icodtds">
                                       <img title="24/7" src="<?php echo base_url(); ?>webfiles/img/toppics/24.png">
                                    </div>
                                    <p>24/7"</p>
                                 </li>
                                 <?php
                                    }
                                    }
                                    }
                                    ?>
                                 <?php
                                    if (!empty($jobLeaves)) {
                                    foreach($jobLeaves as $jobLeave) {
                                       if ($jobLeave['leaves_id'] == '41') {
                                    ?>
                                 <li>
                                    <div class="icodtds"><img title="Weekends Off" src="<?php echo base_url(); ?>webfiles/img/toppics/weekend-off.png"></div>
                                    <p>Weekends Off</p>
                                 </li>
                                 <?php
                                    }
                                    if ($jobLeave['leaves_id'] == '42') {
                                    ?>
                                 <li>
                                    <div class="icodtds"><img title="Holidays Off" src="<?php echo base_url(); ?>webfiles/img/toppics/holiday-off.png"></div>
                                    <p>Holidays Off</p>
                                 </li>
                                 <?php
                                    }
                                    }
                                    }
                                    ?>
                              </div>
                           </div>
                           <?php }?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php include_once ('footer.php'); ?>
<?php
   $listingPostid = base64_decode($_GET['listing']);
   $jobDetails = $this->Jobpost_Model->job_detail_fetch($listingPostid);
   //echo $this->db->last_query();die;
   //print_r($jobDetails);die;
   $recruiterdetail = $this->Jobpost_Model->job_detailLocation_fetch($jobDetails[0]['company_id']);
   $timeFrom = $recruiterdetail[0]['from_time'];
   $timeFromm = date('H:i', strtotime($timeFrom));
   $timeTo = $recruiterdetail[0]['to_time'];
   $timeToo = date('H:i', strtotime($timeTo));
   ?>

<div class="modal fade" id="exampleModalCenterInstant" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content" style="box-shadow: none;">
         <div class="InstantBox">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
            <!-- <h5>You will now be redirected to the Instant Assessment page. Your time invested now will speed up the hiring process later. JobYoDA wishes you all the best! Remember, 
               <span>"Every pro was once an amature. Every expert was once a beginner. So dream big and start now".</span></h5> -->

            <h5>Job search made easier right!? You will now begin the Instant Assessment of <?php echo $comapnyDetail['name']; ?>. They are looking for people like you but will only know about you once you finish the assessment. Those who finish the assessment quickly, are more likely to get hired! Good Luck from the JobYoDA team!</h5>
            <p> Best of luck from the JobYoDA team! </p>
            <p>
               <p class="greenbtn" onclick="instantfunction('<?php echo $jobdetail['jobpost_id']; ?>', '<?php echo $jobdetail['modeurl']; ?>')" > Begin Screening </p>
            </p>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="exampleModalapply" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                  <div class="addupdatecent">
                     <!--  <form method="post" action="<?php //echo base_url();
                        ?>dashboard/resumeUpload" enctype="multipart/form-data"> -->
                     <div class="profileupload">
                        <img src="<?php echo base_url() . 'webfiles/'; ?>img/savedbighover.png">
                     </div>
                     <p id="setimgres111"></p>
                     <input type="hidden" id="imagedata">
                     <?php if (!empty($checkResume[0]['resume'])) { ?>
                     <p>Got a few more minutes? Update your Profile and have a higher chance to get that Dream BPO job!</p>
                     <?php }else{?>
                     <p>Please add your Resume </p>
                     <?php }?>
                     <div class="statsusdd">
                        <!--    <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Close</p> -->
                        <!--  <p class="norm" class="close" data-toggle="modal" data-target="#applyModalCenter" data-dismiss="modal">No</p> -->
                        <p class="norm" onclick="noonclick()">No</p>
                        <input type="hidden" name="listing" value="<?php echo $_GET['listing']; ?>">
                        <?php if (strlen($checkResume[0]['resume']) < 1) { ?>
                        <div class="shwnbts">
                           <input name="resumeFile" id="file_img" type="file" class="yesuplds" onchange="getFilename()">
                           <p class="btns-yes">Yes</p>
                        </div>
                        <?php
                           } else { 
                           ?>
                        <div class="shwnbts">
                           <input name="resumeFile" id="file_img" type="file" class="yesuplds" onchange="getFilename()">
                           <p class="btns-yes">Upload</p>
                        </div>
                        <?php
                           } ?>
                     </div>
                     <!-- </form> -->
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="applyModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                  <!-- <form id="formschedule"> -->
                     <div class="schedulejobgs">
                        <h6>Apply for Interview</h6>
                        <!-- <p>Select Date</p>
                        <div class="forminputspswd">
                           <p>Date</p>
                           <input type="text" name="scheduledate" class="form-control datetimepicker11" id="datetimepicker1" autocomplete="off" placeholder="mm/dd/yy" onchange="getdayname()">
                           <span id="setval" style="color:red"></span>
                           <input type="hidden" id="dayfromm" value="<?php //echo $recruiterdetail[0]['dayfrom']; ?>">
                           <input type="hidden" id="daytoo" value="<?php //echo $recruiterdetail[0]['dayto']; ?>">
                           <input type="hidden" id="timefromm" value="<?php //echo $timeFromm; ?>">
                           <input type="hidden" id="timetoo" value="<?php //echo $timeToo; ?>">
                           <i class="far fa-calendar-alt fieldicons"></i>
                        </div> -->
                        <input type="hidden" name="scheduledate"> 
                        <div class="forminputspswd">
                           <!-- <p>Time</p>
                              <input type="text" name="scheduletime" class="form-control bootstrap-timepicker timepicker" onchange="getTime()" id="datepicker1"  data-format="hh:mm:ss" required="required"> 
                              <span id="setvall" style="color:red"></span>
                              <i class="far fa-clock"></i> -->
                           <input type="hidden" name="scheduletime"> 
                        </div>
                        <div class="statsusdd">
                           <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                           <input type="hidden" name="listing" id="jobId"  value="<?php echo base64_decode($_GET['listing']); ?>">
                           <button type="submit" class="updt" > Apply </button>
                        </div>
                     </div>
                  <!-- </form> -->
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="exampleModalCenter1000" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document" style="width: 37%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close updt" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                  <form>
                     <div class="addupdatecent">
                        <!-- <img src="<?php //echo base_url() . 'webfiles/'; ?>img/savedbighover.png"> -->
                        
                        <!-- <p class="jobsuccess">Job Applied Successfully</p> -->

                        <p style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>All the best from the JobYoDA Team!</p> 
                        <p style='text-align:center;color:#fbaf31;line-height:28px;margin-bottom:0px;font-weight:500;'>Did you know JobYoDA offers a FREE Venti Coffee to all hires?</p> 
                        <p style='text-align:center;color:#fbaf31;line-height:28px;margin-bottom:0px;font-weight:500;'>To claim, all you need to do is:</p> 
                        <p style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>1. Declare JobYoDA as the source of your application</p>
                        <p style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>2. Send us and email at Help@jobyoda.com to claim your voucher if you get hired</p>
                        <p style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>3. We will validate with the recruiter and process</p>
                        <p style='text-align:center;color:#00a94f;;line-height:28px;margin-bottom:0px;font-weight:500;'>Good Luck!</p>

                        <div class="statsusdd" style="display: inherit; float: none;">
                           <?php if($jobdetail['chatbot'] == 0) { ?>
                           <button  type="button" class="updt" id="okup" data-dismiss="modal" aria-label="Close">Ok</button>
                           <?php } else { ?>
                           <center style="margin-bottom: 0px"><a href="<?php echo base_url();?>chatbot/<?php echo base64_encode($jobdetail['jobpost_id']);?>" style="text-decoration: none;" class="updt chatbotlink" id="okup">Continue Chat Interview</a></center>
                           <?php } ?>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="exampleModalCenter2000" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                  <form>
                     <div class="addupdatecent">
                        <img src="<?php echo base_url() . 'webfiles/'; ?>img/savedbighover.png">
                        <p class="jobsuccess1"></p>
                        <div class="statsusdd">
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<!--   <script src="<?php echo base_url() . 'webfiles/'; ?>js/bootstrap-timepicker.js"></script>-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>recruiterfiles/css/jquery.timepicker.min.css" />
<script type="text/javascript" src="<?php echo base_url(); ?>recruiterfiles/js/jquery.timepicker.min.js"></script>
<script type="text/javascript">
   $(function () {
      $(".datetimepicker11").datepicker({ 
       dateFormat: "yy-mm-dd", 
        defaultDate: '',
        autoclose: true,
     })
   });
   $('.timepicker').timepicker({ 'timeFormat': 'H:i' });
    
    function instantfunction(id, url) {
           $.ajax({
              'type' :'POST',
              'url' :"<?php echo base_url('dashboard/savedinstantData') ?>",
              'data' :'jobId='+id,
              'success':function(htmlres) {
                  $('#exampleModalCenterInstant').modal('hide');
                  window.open(url, '_blank');
                  console.log(htmlres);
              }
          });
    }
</script>
<?php
   if (isset($_GET["msg"]) && $_GET["msg"] == "uploadcomplete") {
   ?>
<script type="text/javascript">
   $(window).on('load',function(){
       $('#applyModalCenter').modal('show');
   });
</script>
<?php
   }
   ?>
<!--  call ajax for schedule -->
<script>
   $("#formschedule").submit(function(e) {

      $(".applijoboader").css("display","block");

       e.preventDefault();    
       $.ajax({
           'type' :'POST',
           'url' :"<?php echo base_url('dashboard/scheduled') ?>",
           'data' :$(this).serialize(),
           success:function(response) {
              
              $(".applijoboader").css("display","none");

             var jsonData = JSON.parse(response);
               
             if(jsonData.status == "SUCCESS") {
              
              if(jsonData.chatbot == 0) {

                $('.jobsuccess').html(jsonData.message);
                $('#exampleModalCenter1000').modal('show');
                $('#applyModalCenter').modal('hide');
              
              } else {

                  window.location = "<?php echo base_url();?>chatbot/"+jsonData.jobid;
              }
   
             } else {
                 $('.jobsuccess1').html(jsonData.message);
                 $('#exampleModalCenter2000').modal('show');
                 $('#applyModalCenter').modal('hide');
             }
           }
       });
   });
   
</script>
</body>
<script type="text/javascript">
   $("#okup").on("click",function(){
         window.location = "<?php echo base_url(); ?>appliedjobs";
   });
</script>
<script>
   function getdayname() {
   
       var jobId= $("#jobId").val();
       var dayfrom = $("#dayfromm").val();
       var dayto   = $("#daytoo").val();
       var getdate= $("#datetimepicker1").val();
       
        $.ajax({
          'type' :'POST',
          'url' :"<?php echo base_url('dashboard/checkdateschedule') ?>",
          'data' :'dateValue='+getdate+'&jobId='+jobId,
          'success':function(htmlres) {
   
            htmlres = jQuery.parseJSON(htmlres);
   
             if(htmlres.status == "error") {
   
                $("#setval").html(htmlres.message);
                $("#datetimepicker1").val(''); 
                $('#schedule').attr('disabled',true);
   
             } else {
                 $('#schedule').attr('disabled',false);
                 $("#setval").html("");
                 $("#datetimepicker1").val(getdate);
             }
          }
        });
   }
</script>
<script>
   function getTime()
   {
      var jobId= $("#jobId").val();
      var timefrom= $("#timefromm").val();
      var timetoo= $("#timetoo").val();
      var gettime= $("#datepicker1").val();
       $.ajax({
         'type' :'POST',
         'url' :"<?php echo base_url('dashboard/checktimeschedule') ?>",
         'data' :'timeValue='+gettime+'&jobId='+jobId,
         'success':function(htmlres) {
            if(htmlres == 1) {
              $('#schedule').attr('disabled',false);
              $("#setvall").html("");
              $("#datepicker1").val(gettime);
            } else {
                $("#setvall").html("Please Select Schedule time between ("+timefrom+" to "+timetoo+")");
                $("#datepicker1").val(''); 
                $('#schedule').attr('disabled',true);
            }
         }
       });
   }
</script>
<script>
   function getFilename() {
      var name = document.getElementById("file_img").files[0].name;
      var filetype= $("#file_img").val();
      var ext = filetype.split('.').pop();
   
      if(ext =="jpg" || ext =="jpeg" || ext =="png"){
        $("#setimgres111").html("<span style='color:red'>plese select file format(pdf/doc/docx)</span>");
        return false;
     
     } else {
     
      $("#setimgres111").html("");
      var form_data = new FormData();
      form_data.append("file", document.getElementById('file_img').files[0]);
      $.ajax({
        url:'<?php echo base_url('dashboard/imageUpload') ?>',
        method:"POST",
        data: form_data,
        contentType: false,
        cache: false,
        processData: false,
        success:function(imagedata) { 
           $("#imagedata").val(imagedata);
           $("#file_img").val('');
           $("#setimgres111").html(imagedata);
           PdfImagesend();
        }
      });
     }
   }
</script>
<script type="text/javascript">
   function PdfImagesend() {
      var image= $("#imagedata").val();
      console.log(image);
        $.ajax({
             'type' :'POST',
             'url' :"<?php echo base_url('dashboard/fileInsert') ?>",
             'data' :'imagedata='+image,
             'success':function(htmlres) {
               console.log(htmlres);
               if(htmlres==1) {
                 $('#exampleModalapply').modal('hide');
                 $("#applyModalCenter").modal('show');
               }
               if(htmlres==2) {
                 $('#exampleModalapply').modal('hide');
                 $("#applyModalCenter").modal('show');
               }
             }
         });
   }
   
   function noonclick() {
       $('#exampleModalapply').modal('hide');
       $("#applyModalCenter").modal('show');
       $('body').addClass('modal-opennn');
   }
</script>
<script>
$(document).on('click','.sharebuttoncustom h6',function(){
  $('.sharelinkclass').slideToggle();
});
</script>
<script>
document.addEventListener("mousedown", function (event) {
    if (event.target.closest(".sharebuttoncustom h6,.sharelinkclass"))
        return;
    $('.sharelinkclass').slideUp();
});
    </script>

<?php

   function withShareLinks($args) {
      $url = urlencode($args['url']);
      $title = urlencode($args['title']);
      $image = urlencode($args['image']);
      $desc = urlencode($args['desc']);
      $redirect_url = urlencode($args['redirecturl']);
      $text = $title;
      
      if($desc) {
        $text .= '%20%3A%20'; # This is just this, " : "
        $text .= $desc;
      }
      
        // conditional check before arg appending
      
      return [
        'facebook'=>'http://www.facebook.com/sharer.php?u=' . $url . '&title=' . $title, 
        'gmail'=>'https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=&su=' . $title . '&body=' . $url,
        'linkedin'=>'https://www.linkedin.com/sharing/share-offsite/?mini=true&url=' . $url.'&title=' . $title.'&source=jobyooda',
        'twitter'=>'https://twitter.com/intent/tweet?url=' . $url . '&text=' . $text,
        'whatsapp'=>'https://api.whatsapp.com/send?text=' . $text . '%20' . $url,
      ];
    }

?>

<script type="text/javascript">
  $("document").ready(function() {

      var cont = $(this).attr('content');

      if(cont == "Facebook Share") {

          window.open("<?php echo $sharelinks['facebook']; ?>", "_blank");

      } else if(cont == "Twitter Share") {
          
          window.open("<?php echo $sharelinks['twitter']; ?>", "_blank");

      } else if(cont == "LinkedIn Share") {

        window.open("<?php echo $sharelinks['linkedin']; ?>", "_blank");
        
      } else if(cont == "Gmail Share") {

        window.open("<?php echo $sharelinks['gmail']; ?>", "_blank");
        
      } else if(cont == "Whatsapp Share") {

        window.open("<?php echo $sharelinks['whatsapp']; ?>", "_blank");
        
      }

  });

</script>
<script type="text/javascript">
  function sharecountajax(jobid, url) {
    //console.log(url);

    console.log();
    $.ajax({
        type: 'POST',
        url: "<?php echo base_url(); ?>user/savesharecount",
        data: {job_id:jobid},
        success: function (result) {

            window.open(
              url,
              '_blank'
            );
        }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
   $(".updt").on("click",function(){
         window.location = "<?php echo base_url(); ?>appliedjobs";
   });
  });
</script>