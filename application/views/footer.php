 <style>
    .pac-container {
            z-index: 10000 !important;
        }
 </style>
  <div class="clear"></div>    
  
      <footer class="homefooter" id="fooapp">
         <div class="darkbgafootdiv">
            <div class="container">
               <div class="row">
                  <div class="col-md-4 col-lg-4">
                     <div class="widgetsfoot">
                        <img src="<?php echo base_url().'recruiterfiles/';?>images/jobyoDa.png" style="width:179px;">
                        <p> <span><i class="fas fa-map-marker-alt"></i></span> Level 10-1 Fort Legend Tower, 3rd Avenue Bonifacio Global City 31st Street Fort Bonifacio TAGUIG CITY, FOURTH DISTRICT, NCR, Philippines, 1634</p>
                        <p><span><i class="fas fa-envelope"></i></span><a href="mailto:info@jobhunt.com">help@jobyoda.com</a></p>
                        <!-- <h6><a href="tel:+63 9178721630">+63 9178721630</a></h6> -->
                     </div>
                  </div>
                  <div class="col-md-4 col-lg-4">
                     <div class="widgetsfoot informar" style="width: 100%">
                        <h5>Useful Links</h5>
                        <div class="halfdividedft" style="width: 100%; margin: 0">
                           <ul>
                              <li><a href="<?php echo base_url(); ?>privacy_policy">Privacy Policy </a></li>
                              <li><a href="<?php echo base_url(); ?>terms">Terms of Service </a></li>
                              <li><a href="<?php echo base_url(); ?>contact">Contact Us </a></li>
                              <li><a href="<?php echo base_url(); ?>about">About Us </a></li> 
                              <li><a href="<?php echo base_url(); ?>how_it_works">How it Works </a></li>
                           </ul>
                        </div>
                        
                     </div>
                  </div>
                  <div class="col-md-4 col-lg-4">
                     <div class="widgetsfoot informar">
                        <h5>Get It On</h5>
                        
                        <div class="halfdividedft morearea">
                           <a href="https://apps.apple.com/us/app/jobyoda/id1471619860?ls=1" class="download-btn" target="_blank">
                                        <span>
                                            <!-- <img src="https://jobyoda.com/webfiles/img/home/apple.png"> -->
                                            <img src="<?php echo base_url();?>webfiles/newone/social/ios_download_jobyoda.png">

                                        </span>
                                        <!-- <p>
                                            <small>Download On</small>
                                            <br>
                                            App Store
                                        </p> -->
                                    </a>
                                    <a href="https://play.google.com/store/apps/details?id=com.jobyodamo" class="download-btn" target="_blank">
                                        <span>
                                            <!-- <img src="https://jobyoda.com/webfiles/img/home/appstore.png"> -->
                                            <img src="<?php echo base_url();?>webfiles/newone/social/android_download_jobyoda.png">
                                        </span>
                                        <!-- <p>
                                            <small>Get It On</small>
                                            <br>
                                            Google Play
                                        </p> -->
                                    </a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
          
          <div class="Copyright">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <p>Copyright JobYoDA © 2020. All Rights Reserved</p>
                        </div>
                        <div class="col-sm-6">
                            <ul>
                                <li>
                                  <a href="https://www.facebook.com/jobyodapage/" target="_blank">
                                    <i class="fab fa-facebook-f"></i>
                                  </a>
                                </li>
                             
                              <li>
                                <a href="https://instagram.com/jobyoda_ig?igshid=1u7xaxp20etlb" target="_blank">
                                  <i class="fab fa-instagram"></i>
                                </a>
                              </li> 
                              <!-- <li>
                                <a href="#">
                                  <i class="fab fa-google"></i>
                                </a>
                              </li> -->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

      </footer>
	

 <script src="<?php echo base_url().'webfiles/';?>js/popper.min.js"></script>
 <script src="<?php echo base_url().'webfiles/';?>js/bootstrap.js"></script> 
 <script src="<?php echo base_url().'webfiles/';?>js/owl.carousel.js"></script> 


<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script> 
   <!--   <script src="<?php echo base_url().'webfiles/';?>js/bootstrap-timepicker.js"></script>-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>recruiterfiles/css/jquery.timepicker.min.css" />
<script type="text/javascript" src="<?php echo base_url(); ?>recruiterfiles/js/jquery.timepicker.min.js"></script> <script src='https://cdn.jsdelivr.net/jquery.counterup/1.0/jquery.counterup.min.js'></script>

<script src="<?php echo base_url().'webfiles/';?>js/adminyoda.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk&libraries=places&callback=initMap"></script> -->

 <div class="modal fade" id="statusModalCenter36" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
     <div class="modal-dialog modal-dialog-centered howshireds jbsrchstatus" role="document">
        <div class="modal-content">
           <div class="modal-header">
		   
		   <div class="salryedits" style="width:100%;">
            <h4>Job Search Status</h4>						 
          </div>

              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              </button>
           </div>
            <span id="errorstatus"></span>
           <div class="modal-body">
              <div class="formmidaress modpassfull">
                 <div class="filldetails">
                 <form method="post" action="<?php echo base_url(); ?>Dashboard/jobsearchstatus">
      					   <div class="ratesuld">
      							<ul>
      								<li>
      								<div class="custom-control custom-radio">
                        <input type="radio" name="searchstatus" value="1" <?php if($checkStatus[0]['jobsearch_status']=='1'){ echo "checked"; } ?> class="custom-control-input" id="customCheck45">
                        <label class="custom-control-label" for="customCheck45">Actively Seeking</label>
                      </div>
      								</li>
      								<li>
      									<div class="custom-control custom-radio">
                        <input type="radio" name="searchstatus" value="2" <?php if($checkStatus[0]['jobsearch_status']=='2'){ echo "checked"; } ?> class="custom-control-input" id="customCheck46">
                        <label class="custom-control-label" for="customCheck46">Open to Offers</label>
                      </div>
      								</li>
      								<li>
      								<div class="custom-control custom-radio">
                        <input type="radio" name="searchstatus" value="3" <?php if($checkStatus[0]['jobsearch_status']=='3'){ echo "checked"; } ?> class="custom-control-input" id="customCheck47">
                        <label class="custom-control-label" for="customCheck47">Not open</label>
                      </div>
      				
      								</li>
      							</ul>
      					   </div>
                       
                   <button type="submit" id="changestatusbtn" class="srchbtns">Done</button>  
					       </form>
					  
                 </div>
              </div>
           </div>
        </div>
     </div>
  </div>

  <div class="modal fade" id="exampleModalCenter8" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                  <form method="post" action="<?php echo base_url();?>user/resumeUpload" enctype="multipart/form-data">
                   <div class="addupdatecent">
                     <div class="profileupload">
                        <img src="<?php echo base_url().'webfiles/';?>img/savedbighover.png">
                         <input type="file"  id="file" onchange="getFilename()" name="resumeFile" data-validation="required" >
                        <?php if(!empty($checkResume[0]['resume'])) {?>
                        <p>Would you like to update your Resume?</p>
                        <?php } else { ?>
                        <p>Please Add Your Resume</p>
                        <?php } ?>
                        <div class="statsusdd">
                           <p class="norm" class="close" data-dismiss="modal" aria-label="Close">No</p>
                          <button type="button" class="updt uploadboxclasss">Yes</button>
                          <button type="submit" class="updt" id="resumebtn" style="display: none;">Update</button>
                        </div>
                        <p id="setimgres"></p>
                     </div>
                    </div>
                      <input type="hidden" value="<?php echo current_url(); ?>" name="currentURL"> 
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="exampleModalCenter10" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                  <form>
                     <div class="addupdatecent">
                        <img src="<?php echo base_url().'webfiles/';?>img/savedbighover.png">
                        <p>Resume Updated Successfully</p>
                        <div class="statsusdd">
                           <p class="updt" data-dismiss="modal" aria-label="Close">Ok</p>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
         <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">Change Password</h5>
                  
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <span id="errorpassword" class="text-danger text-center errorfield1"></span>
               <div class="modal-body">
                  <div class="formmidaress modpassfull">
                 <div class="filldetails">
                       <div class="forminputspswd">
                          <input type="password" class="form-control" placeholder="Enter Old Password" name="oldpass" id="oldpass">
                          <img src="<?php echo base_url().'webfiles/';?>img/keypass.png">
                       </div>
                       <div class="forminputspswd">
                          <input type="password" class="form-control" placeholder="Enter New Password" name="newpass" id="newpass">
                          <img src="<?php echo base_url().'webfiles/';?>img/keypass.png">
                       </div>
                       <div class="forminputspswd">
                          <input type="password" class="form-control" placeholder="Confirm New Password" name="confpass" id="confpass">
                          <img src="<?php echo base_url().'webfiles/';?>img/keypass.png">
                       </div>
                       <button type="button" id="changepassbtn" class="srchbtns">Change</button>
                 </div>
              </div>
               </div>
            </div>
         </div>
      </div>

      <div class="modal fade" id="socialModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
         <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <!-- <h5 class="modal-title" id="exampleModalLongTitle">Change Password</h5> -->
                  <?php $userSess = $this->session->userdata('usersess');
                  $type = $usersess['type'];
                  ?>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <!-- <span id="errorpassword" class="text-danger text-center errorfield1"></span> -->
               <div class="modal-body">
                <div class="baseyoda">
                 <div class="thmb"> <img src="<?php echo base_url(); ?>webfiles/img/yellos.png"></div>
                  <p>You are not allowed to change password because you logged in via <?php echo $type; ?></p>
                </div>
               </div>
            </div>
         </div>
      </div>

<div class="modal fade" id="profileModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered" role="document">
  <div class="modal-content">
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
     </div>
     <div class="modal-body">
        <div class="formmidaress modpassfull">
           <div class="filldetails">
              <form>
                 <div class="addupdatecent">
                    <img src="<?php echo base_url().'webfiles/';?>img/savedbighover.png">
                    <p><?php if(!empty($this->session->tempdata('updatemsg'))){ echo $this->session->tempdata('updatemsg'); }?></p>
                    <div class="statsusdd">
                       <!-- <p class="updt" data-dismiss="modal" aria-label="Close">Ok</p> -->
                    </div>
                 </div>
              </form>
           </div>
        </div>
     </div>
  </div>
</div>
</div>

<style type="text/css">
  .btn-group{width:100%!important;}
  .multiselect.dropdown-toggle{background-color: #d4cfcf!important;height: 40px!important;width:100%!important;}
  .filter-option{font-size: 13px!important;font-weight: 100!important;}
</style>

<script type="text/javascript">
  $('.uploadboxclasss').click(function() { 
      $('#file').trigger('click');
      $('.uploadboxclasss').css('display','none');
      $('#resumebtn').css('display','block');

  });
</script>

<?php if($this->session->tempdata('updatemsg') != null) {
             if($this->session->tempdata('updatemsg')){$this->session->unset_tempdata('updatemsg');}?>
<script type="text/javascript">
    $(window).on('load',function(){
        $('#profileModal').modal('show');
    });
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>
<?php } ?>


<script>
    $(document).ready(function(){   
    $("#changepassbtn").click(function() {
      var oldpass = $("#oldpass").val();
      var newpass = $("#newpass").val();
      var confpass = $("#confpass").val();
      $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>" + "user/changepass",
          data: {oldpass:oldpass, newpass:newpass,confpass:confpass},
          cache:false,
          success:function(htmldata){
          //alert(htmldata);
             // console.log(data);
             // var logdone = "logindone";
             // var n = data.includes(logdone);
              //if(n) {
             //   window.location = "<?php echo base_url(); ?>dashboard";
             // } else {
               $('#errorpassword').html(htmldata);
              //}
          },
          error:function(){
            console.log('error');
          }
      });
  });
});
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />

<script> 
$("#fooapptops").click(function() {
    $([document.documentElement, document.body]).animate({
        scrollTop: $("#fooapp").offset().top
    }, 2000);
});
</script> 

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk&libraries=places&callback=initMap"></script>
 <script type="text/javascript">
   google.maps.event.addDomListener(window, 'load', function () {
      var places1 = new google.maps.places.Autocomplete(document.getElementById('location'));
    
       google.maps.event.addListener(places1, 'place_changed', function () {
         
           var place = places1.getPlace();
           var address = place1.formatted_address;
           var latitude = place1.geometry.location.A;
           var longitude = place1.geometry.location.F;
           var mesg = "Address: " + address;
           mesg += "\nLatitude: " + latitude;
           mesg += "\nLongitude: " + longitude;
       });
   });
</script>