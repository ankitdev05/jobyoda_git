<html>
   <head>
      <title>JobYoDA</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
      <link href="<?php echo base_url().'webfiles/';?>css/all.css" rel="stylesheet" type="text/css">
      <link href="<?php echo base_url().'webfiles/';?>css/fontawesome.css" rel="stylesheet" type="text/css">
      <link href="<?php echo base_url().'webfiles/';?>css/bootstrap.css" rel="stylesheet" type="text/css">
      <link href="<?php echo base_url().'webfiles/';?>css/style.css" rel="stylesheet" type="text/css">
	  <link href="<?php echo base_url().'webfiles/';?>css/owl.carousel.min.css" rel="stylesheet" type="text/css">
	  <link href="<?php echo base_url().'webfiles/';?>css/owl.theme.default.min.css" rel="stylesheet" type="text/css">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
      <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
      <link href="<?php echo base_url().'webfiles/';?>css/timepicker.less" rel="stylesheet" type="text/css" />
      <link rel="icon" href="<?php echo base_url(); ?>recruiterfiles/images/fav.png" type="image/png" sizes="16x16">
      
      <style type="text/css">
         #signupform .error{color:#f00;}
         
      </style>
	  
	 	 <script src="<?php echo base_url().'webfiles/';?>js/jquery.min.js"></script> 
     <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>recruiterfiles/css/jquery.timepicker.min.css" />
    <script type="text/javascript" src="<?php echo base_url();?>recruiterfiles/js/jquery.timepicker.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>     
   </head>
   <body>
   <?php
      if($this->session->userdata('usersess')) {
         $usersess = $this->session->userdata('usersess');
   ?>
    
      <header class="mainhead nohdascr">

         <div class="container-fluid">
            <div class="row">
               <div class="logomain col-md-3">
                  <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url().'webfiles/';?>img/Final11.png" style="width:179px;"></a>
               </div>
               <div class="menurightopn col-md-9">
                  <div class="rightpan">
					 <a href="javascript:void(0)" id="fooapptops" class="launchbtn">Launch App</a> 
				  <div class="dropdwnshre">
						<ul>
							<li><a href="<?php echo base_url();?>user/homeafterlogin"><i class="fas fa-home"></i> <p>Home</p></a></li>
							<li><a href="<?php echo base_url();?>dashboard/logout"><i class="fas fa-sign-out-alt"></i> <p>Logout</p></a></li>
						</ul>
					 </div> 
					 
					 
                     <a href="#"> 
                      <?php
                           if(!empty($usersess['profilePic'])){
                        ?>
                           <img  class="dropthumbds" src="<?php echo $usersess['profilePic'];?>" style="width:50px;height:50px;border-radius: 50%;">
                        <?php
                           } else {
                        ?>
                           <img class="dropthumbds" src="<?php echo base_url().'webfiles/';?>img/profilepic.png" style="width:50px;height:50px;border-radius: 50%;">
                        <?php
                           }
                        ?>
                       <!-- <img src="<?php //echo base_url().'webfiles/';?>img/ali.png">-->
                     </a>    
                     <h5 class="dropthumbds"><?php echo $usersess['name']; ?></h5>
                     
                  </div>
				  
				  <div class="searchfiltersars homeheaders">
            <form method="post" id="searchform" action="<?php echo base_url();?>dashboard/homejobLists" novalidate="novalidate">
               <div class="srchkeywidgets homesrch">
             <div class="rgtpos" style="">
                <img src="<?php echo base_url();?>webfiles/img/home/targetloc.png"></div>
                  <input type="text" class="form-controls shwinptre" name="location" id="txtPlacess" placeholder="Location" autocomplete="off">
                  <input type="hidden" name="lat" id="lat">
                  <input type="hidden" name="long" id="long">
                                    <button type="submit" class="showsrchd" ><img src="<?php echo base_url();?>webfiles/img/home/search.png"></button>
                                 </div>
            </form>
            <div class="advanceare">
               <a href="#" data-toggle="modal" data-target="#exampleModalCenter14"> + Advance Search</a>
            </div>
         </div>
		 
               </div>
            </div>
			
			 <div class="newparts">
					<div class="appparts">
						<a href="https://play.google.com/store/apps/details?id=com.jobyodamo" target="_blank"><img src="<?php echo base_url().'webfiles/';?>img/getit.png"></a>
						<a href="https://apps.apple.com/us/app/jobyoda/id1471619860?ls=1" target="_blank"><img src="<?php echo base_url().'webfiles/';?>img/appdown.png"></a>
					</div>
					
					<div class="menulinksyodas">
						<ul>
							<li><a href="<?php echo base_url(); ?>sitemap">Site Map</a></li>
							<li><a href="<?php echo base_url();?>contact">Contact us</a></li>
							<li><a href="<?php echo base_url();?>faq">FAQs</a></li>
							<li><a href="<?php echo base_url(); ?>dashboard/job_listing?cat=Ng==">Jobs</a></li>
					
						</ul>
					</div>
			   </div>
			   
         </div>
      </header>

      <!-- <div class="">
         <h4>Welcome <span class="username"><?php echo $usersess['name'];?></span></h4>
      </div> -->
   <?php
      } else{
   ?>

      <header class="mainhead nohdascr">
         <div class="container-fluid">
         <?php 
         if(!empty($this->session->flashdata('msg')))
          {
           echo $this->session->flashdata('msg');
          }

            ?>
            <div class="row">
               <div class="logomain col-md-3">
                  <a href="<?php echo base_url()?>"><img src="<?php echo base_url().'webfiles/';?>img/Final11.png" style="width:179px;"></a> 
               </div>
               <div class="menurightopn col-md-9">
			    <div class="rightpan">
					<div class="onedrimenu">
                     <!--<a href="#" class="btn-transparent"> + Post A Job </a>-->
					 <div class="setbt">
						<p class="tgs">Got a job to offer?</p>
                     <a href="<?php echo base_url();?>recruiter/recruiter/">Recruiter's Portal</a>
					 </div>
					 <div class="setbt">
					 <p class="tgs">Got skills to offer ?</p>
                     <a href="#" data-toggle="modal" data-toggle="modal"  data-target="#exampleModalCenter4">Jobseekers</a>
					 </div>
					 </div>
					
                  </div>
                  <div class="searchfiltersars homeheaders">
                     <form method="post" id="searchform" action="<?php echo base_url();?>user/home_search" novalidate="novalidate">
                        <div class="srchkeywidgets homesrch">
                      <div class="rgtpos" style="">
                         <img src="<?php echo base_url();?>webfiles/img/home/targetloc.png"></div>
                           <input type="text" class="form-controls shwinptre" name="location" id="txtPlacess" placeholder="Location" autocomplete="off">
                           <input type="hidden" name="lat" id="lat">
                           <input type="hidden" name="long" id="long">
                          <button class="showsrchd" type="submit"><img src="<?php echo base_url();?>webfiles/img/home/search.png"></button>
                                          </div>
                     </form>
                     <div class="advanceare">
                        <a href="#" data-toggle="modal" data-target="#exampleModalCenter14"> + Advance Search</a>
                     </div>
                  </div>
                 
               </div>
			   
			   <div class="newparts">
					<div class="appparts">
						<a href="https://play.google.com/store/apps/details?id=com.jobyodamo" target="_blank"><img src="<?php echo base_url().'webfiles/';?>img/getit.png"></a>
						<a href="https://apps.apple.com/us/app/jobyoda/id1471619860?ls=1" target="_blank"><img src="<?php echo base_url().'webfiles/';?>img/appdown.png"></a>
					</div>
					
					<div class="menulinksyodas">
						<ul>
							<li><a href="<?php echo base_url(); ?>sitemap">Site Map</a></li>
							<li><a href="<?php echo base_url();?>contact">Contact us</a></li>
              <li><a href="<?php echo base_url();?>faq">FAQs</a></li>
							<li><a href="https://jobyoda.com/blogs/">Blogs</a></li>
							<li><a href="<?php echo base_url(); ?>user/job_listing?cat=Mw==">Jobs</a></li>
							<li> <a href="#" data-toggle="modal" data-target="#exampleModalCenter5">Sign Up</a></li>
						</ul>
					</div>
			   </div>
            </div>
         </div>
      </header>
   <?php
      }
   ?>
   <input type="hidden" name="cur_lat" id="cur_lat" value="">
    <input type="hidden" name="cur_long" id="cur_long" value="">


<?php

if(!empty($featuredJob)){
 $x=30;
        
    foreach($featuredJob as $featured) {
       $jobDetails = $this->Jobpost_Model->job_detail_fetch($featured['jobpost_id']);
      $recruiterdetail = $this->Jobpost_Model->job_detailLocation_fetch($jobDetails[0]['company_id']);
      $timeFrom = $recruiterdetail[0]['from_time'];
      $timeFromm = date('H:i', strtotime($timeFrom));
      $timeTo   = $recruiterdetail[0]['to_time'];
      $timeToo = date('H:i', strtotime($timeTo));
  ?>

<div class="modal fade" id="applyModal8<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
             <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                   <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                      </button>
                   </div>
                   <div class="modal-body">
                      <div class="formmidaress modpassfull">
                         <div class="filldetails">
                            <form> 
                               <div class="addupdatecent">
                                  <div class="profileupload">
                                   <img src="<?php echo base_url().'webfiles/';?>img/savedbighover.png">
                                   <!-- <input type="file" id="filel<?php echo $x;?>" title="" name="resumeFile" onchange='getFilenamel("<?php echo $x ?>")'> -->
                                   </div>
                                     <p id="setimgresl<?php echo $x ?>"></p>
                                   <input type="hidden" id="imagedatal<?php echo $x ?>">
                                  <?php if (!empty($checkResume[0]['resume'])) { ?>
                                   <p>Would you like to update Your Resume?</p>
                                   <?php }else{?>
                                   <p>Please add your Resume</p>
                                   <?php }?>
                                  <div class="statsusdd">
                                     <p class="norm" class="close" data-toggle="modal" data-target="#featuredModal<?php echo $x;?>" data-dismiss="modal">No</p>
                                     <input type="hidden" name="type" value="<?php echo $x;?>">
                                    <div class="shwnbts">
                                   <input name="resumeFile" id="filel<?php echo $x;?>" type="file" class="yesuplds" onchange="getFilenamel('<?php echo $x ?>')">

                                   <?php if (!empty($checkResume[0]['resume'])) { ?>
                                    <p class="btns-yes">Yes</p>
                                    <?php }else{?>
                                     <p class="btns-yes">Upload</p>
                                    <?php }?>
                                    </div>
                                  </div>
                               </div>
                            </form>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
            </div>

            <div class="modal fade" id="featuredModal<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="formmidaress modpassfull">
                                <div class="filldetails">
                                    <form method="post" action="<?php echo base_url();?>dashboard/scheduledListing">
                                        <div class="schedulejobgs">
                                            <h6>Schedule interview</h6>
                                            <p>Select Date & Time</p>
                                            <div class="forminputspswd">
                                                <p>Date</p>
                                                <input type="text" name="scheduledate" class="form-control datetimepicker1" id="datetimepickerf<?php echo $featured['jobpost_id']?>" autocomplete="off" placeholder="mm/dd/yy" onchange="getdaynamef('<?php echo $featured['jobpost_id']?>')" required="required">
                                                <span id="setval<?php echo $featured['jobpost_id']?>" style="color:red"></span>
                                                <input type="hidden" id="dayfromf<?php echo $featured['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayfrom'];?>">
                                                <input type="hidden" id="daytof<?php echo $featured['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayto'];?>">

                                                <input type="hidden" id="timefromf<?php echo $featured['jobpost_id']?>" value="<?php echo $timeFromm;?>">
                                                <input type="hidden" id="timetof<?php echo $featured['jobpost_id']?>" value="<?php echo $timeToo;?>">
                                                <i class="far fa-calendar-alt fieldicons"></i>
                                            </div>
                                            <div class="forminputspswd">
                                                <p>Time</p>
                                                <input type="text" name="scheduletime" autocomplete="off" class="form-control bootstrap-timepicker timepicker" onchange="getTimef('<?php echo $featured['jobpost_id']?>')" id="datepickerf<?php echo $featured['jobpost_id']?>" data-format="hh:mm:ss" required="required">
                                                <span id="setvallf<?php echo $featured['jobpost_id']?>" style="color:red"></span>
                                                <i class="far fa-clock"></i>
                                            </div>
                                            <div class="statsusdd">
                                                <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                                                <input type="hidden" name="listing" value="<?php echo $featured['jobpost_id'];?>">
                                                <input type="hidden" name="type" value="<?php echo $x;?>">
                                                <button type="submit" class="updt" id="schedulef<?php echo $featured['jobpost_id']?>" disabled>Schedule & Apply</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

           <!-- <div class="modal fade" id="exampleModalCenterdd<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                     <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="formmidaress modpassfull">
                                <div class="filldetails">
                                    <form method="post" action="<?php echo base_url();?>dashboard/scheduledListing">
                                        <div class="schedulejobgs">
                                            <h6>Schedule interview</h6>
                                            <p>Select Date & Time</p>
                                            <div class="forminputspswd">
                                                <p>Date</p>
                                                <input type="text" name="scheduledate" class="form-control datetimepicker1" id="datetimepicker12<?php echo $featured['jobpost_id']?>" placeholder="mm/dd/yy" onchange="getdaynamee('<?php echo $featured['jobpost_id']?>')" required="required" autocomplete="off">
                                                <input type="hidden" id="dayfrommm<?php echo $featured['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayfrom'];?>">
                                                <input type="hidden" id="daytooo<?php echo $featured['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayto'];?>">

                                                <input type="hidden" id="timefrommm<?php echo $featured['jobpost_id']?>" value="<?php echo $timeFromm;?>">
                                                <input type="hidden" id="timetooo<?php echo $featured['jobpost_id']?>" value="<?php echo $timeToo;?>">
                                                <span id="setval12<?php echo $featured['jobpost_id']?>" style="color:red"></span>
                                                <i class="far fa-calendar-alt fieldicons"></i>
                                            </div>
                                            <div class="forminputspswd">
                                                <p>Time</p>
                                                <input type="text" name="scheduletime" autocomplete="off" class="form-control bootstrap-timepicker timepicker" onchange="getTimee('<?php echo $featured['jobpost_id']?>')" id="datepicker13<?php echo $featured['jobpost_id']?>" data-format="hh:mm:ss" required="required">
                                                <span id="setvall12<?php echo $featured['jobpost_id']?>" style="color:red"></span>
                                                <i class="far fa-clock"></i>
                                            </div>
                                            <div class="statsusdd">
                                                <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                                                <input type="hidden" name="listing" value="<?php echo $featured['jobpost_id'];?>">
                                                <input type="hidden" name="type" value="<?php echo $x;?>">
                                                <button type="submit" class="updt" id="schedulees<?php echo $featured['jobpost_id']?>" disabled>Schedule & Apply</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
 -->
  <?php

  $x++;

  }
}
?>

<?php

if(!empty($nearJob)){
 $y=30;
        
    foreach($nearJob as $featured) {
       $jobDetails = $this->Jobpost_Model->job_detail_fetch($featured['jobpost_id']);
      $recruiterdetail = $this->Jobpost_Model->job_detailLocation_fetch($jobDetails[0]['company_id']);
      $timeFrom = $recruiterdetail[0]['from_time'];
      $timeFromm = date('H:i', strtotime($timeFrom));
      $timeTo   = $recruiterdetail[0]['to_time'];
      $timeToo = date('H:i', strtotime($timeTo));
  ?>

<div class="modal fade" id="applynearModal<?php echo $y;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
             <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                   <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                      </button>
                   </div>
                   <div class="modal-body">
                      <div class="formmidaress modpassfull">
                         <div class="filldetails">
                            <form> 
                               <div class="addupdatecent">
                                  <div class="profileupload">
                                   <img src="<?php echo base_url().'webfiles/';?>img/savedbighover.png">
                                   <!-- <input type="file" id="file<?php echo $y;?>" title="" name="resumeFile" onchange='getFilename("<?php echo $y ?>")'> -->
                                   </div>
                                     <p id="setimgres<?php echo $y ?>"></p>
                                   <input type="hidden" id="imagedata<?php echo $y ?>">
                                   <?php if (!empty($checkResume[0]['resume'])) { ?>
                                  <p>Would you like to update Your Resume?</p>
                                  <?php }else{?>
                                   <p>Please add your Resume</p>
                                   <?php }?>
                                  <div class="statsusdd">
                                     <p class="norm" class="close" data-toggle="modal" data-target="#exampleModalCenterc<?php echo $y;?>" data-dismiss="modal">No</p>
                                     <input type="hidden" name="type" value="<?php echo $y;?>">
                                     <div class="shwnbts">
                                     <input name="resumeFile" id="file<?php echo $y;?>" type="file" class="yesuplds" onchange="getFilename('<?php echo $y ?>')">

                                     <?php if (!empty($checkResume[0]['resume'])) { ?>
                                      <p class="btns-yes">Yes</p>
                                      <?php }else{?>
                                       <p class="btns-yes">Upload</p>
                                      <?php }?>
                                     </div>
                                  </div>
                               </div>
                            </form>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
            </div>

            <div class="modal fade" id="exampleModalCenterc<?php echo $y;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="formmidaress modpassfull">
                                <div class="filldetails">
                                    <form method="post" action="<?php echo base_url();?>dashboard/scheduledListing">
                                        <div class="schedulejobgs">
                                            <h6>Schedule interview</h6>
                                            <p>Select Date & Time</p>
                                            <div class="forminputspswd">
                                                <p>Date</p>
                                                <input type="text" name="scheduledate" class="form-control datetimepicker1" id="datetimepicker1<?php echo $featured['jobpost_id']?>" autocomplete="off" placeholder="mm/dd/yy" onchange="getdayname('<?php echo $featured['jobpost_id']?>')" required="required">
                                                <span id="setval<?php echo $featured['jobpost_id']?>" style="color:red"></span>
                                                <input type="hidden" id="dayfromm<?php echo $featured['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayfrom'];?>">
                                                <input type="hidden" id="daytoo<?php echo $featured['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayto'];?>">

                                                <input type="hidden" id="timefromm<?php echo $featured['jobpost_id']?>" value="<?php echo $timeFromm;?>">
                                                <input type="hidden" id="timetoo<?php echo $featured['jobpost_id']?>" value="<?php echo $timeToo;?>">
                                                <i class="far fa-calendar-alt fieldicons"></i>
                                            </div>
                                            <div class="forminputspswd">
                                                <p>Time</p>
                                                <input type="text" name="scheduletime" autocomplete="off" class="form-control bootstrap-timepicker timepicker" onchange="getTime('<?php echo $featured['jobpost_id']?>')" id="datepicker1<?php echo $featured['jobpost_id']?>" data-format="hh:mm:ss" required="required">
                                                <span id="setvall<?php echo $featured['jobpost_id']?>" style="color:red"></span>
                                                <i class="far fa-clock"></i>
                                            </div>
                                            <div class="statsusdd">
                                                <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                                                <input type="hidden" name="listing" value="<?php echo $featured['jobpost_id'];?>">
                                                <input type="hidden" name="type" value="<?php echo $y;?>">
                                                <button type="submit" class="updt" id="schedule<?php echo $featured['jobpost_id']?>" disabled>Schedule & Apply</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- <div class="modal fade" id="exampleModalCenterdd<?php echo $y;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="formmidaress modpassfull">
                                <div class="filldetails">
                                    <form method="post" action="<?php echo base_url();?>dashboard/scheduledListing">
                                        <div class="schedulejobgs">
                                            <h6>Schedule interview</h6>
                                            <p>Select Date & Time</p>
                                            <div class="forminputspswd">
                                                <p>Date</p>
                                                <input type="text" name="scheduledate" class="form-control datetimepicker1" id="datetimepicker12<?php echo $featured['jobpost_id']?>" placeholder="mm/dd/yy" onchange="getdaynamee('<?php echo $featured['jobpost_id']?>')" required="required" autocomplete="off">
                                                <input type="hidden" id="dayfrommm<?php echo $featured['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayfrom'];?>">
                                                <input type="hidden" id="daytooo<?php echo $featured['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayto'];?>">

                                                <input type="hidden" id="timefrommm<?php echo $featured['jobpost_id']?>" value="<?php echo $timeFromm;?>">
                                                <input type="hidden" id="timetooo<?php echo $featured['jobpost_id']?>" value="<?php echo $timeToo;?>">
                                                <span id="setval12<?php echo $featured['jobpost_id']?>" style="color:red"></span>
                                                <i class="far fa-calendar-alt fieldicons"></i>
                                            </div>
                                            <div class="forminputspswd">
                                                <p>Time</p>
                                                <input type="text" name="scheduletime" autocomplete="off" class="form-control bootstrap-timepicker timepicker" onchange="getTimee('<?php echo $featured['jobpost_id']?>')" id="datepicker13<?php echo $featured['jobpost_id']?>" data-format="hh:mm:ss" required="required">
                                                <span id="setvall12<?php echo $featured['jobpost_id']?>" style="color:red"></span>
                                                <i class="far fa-clock"></i>
                                            </div>
                                            <div class="statsusdd">
                                                <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                                                <input type="hidden" name="listing" value="<?php echo $featured['jobpost_id'];?>">
                                                <input type="hidden" name="type" value="<?php echo $y;?>">
                                                <button type="submit" class="updt" id="schedulees<?php echo $featured['jobpost_id']?>" disabled>Schedule & Apply</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->

  <?php

  $y++;

  }
}
?>

<?php

if(!empty($featuredJob)){
 $x=30;
        
    foreach($featuredJob as $featured) {
       $jobDetails = $this->Jobpost_Model->job_detail_fetch($featured['jobpost_id']);
      $recruiterdetail = $this->Jobpost_Model->job_detailLocation_fetch($jobDetails[0]['company_id']);
      $timeFrom = $recruiterdetail[0]['from_time'];
      $timeFromm = date('H:i', strtotime($timeFrom));
      $timeTo   = $recruiterdetail[0]['to_time'];
      $timeToo = date('H:i', strtotime($timeTo));
      if(!empty($featured['toppicks1']) || !empty($featured['toppicks2']) || !empty($featured['toppicks3'])) {
      if($featured['toppicks1']=='5' || $featured['toppicks2']=='5' || $featured['toppicks3']=='5') {
  ?>

<div class="modal fade" id="dayapplyModal<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
             <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                   <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                      </button>
                   </div>
                   <div class="modal-body">
                      <div class="formmidaress modpassfull">
                         <div class="filldetails">
                            <form> 
                               <div class="addupdatecent">
                                  <div class="profileupload">
                                   <img src="<?php echo base_url().'webfiles/';?>img/savedbighover.png">
                                   <!-- <input type="file" id="filed<?php echo $x;?>" title="" name="resumeFile" onchange='getFilenamed("<?php echo $x ?>")'> -->
                                   </div>
                                     <p id="setimgresd<?php echo $x ?>"></p>
                                   <input type="hidden" id="imagedatad<?php echo $x ?>">
                                  <?php if (!empty($checkResume[0]['resume'])) { ?>
                                   <p>Would you like to update Your Resume?</p>
                                   <?php }else{?>
                                   <p>Please add your Resume</p>
                                   <?php }?>
                                  <div class="statsusdd">
                                     <p class="norm" class="close" data-toggle="modal" data-target="#exampleModalCenterf<?php echo $x;?>" data-dismiss="modal">No</p>
                                     <input type="hidden" name="type" value="<?php echo $x;?>">
                                     <div class="shwnbts">
                                     <input name="resumeFile" id="filed<?php echo $x;?>" type="file" class="yesuplds" onchange="getFilenamed('<?php echo $x ?>')">

                                     <?php if (!empty($checkResume[0]['resume'])) { ?>
                                      <p class="btns-yes">Yes</p>
                                      <?php }else{?>
                                       <p class="btns-yes">Upload</p>
                                      <?php }?>
                                      </div>
                                     <!-- <?php if(!empty($checkResume[0]['resume'])) {?>
                                         <button type="button" onclick="PdfImagesendd('<?php echo $x ?>')"  class="updty updt" data-toggle="modal" data-target="#exampleModalCenterdf<?php echo $x;?>" data-dismiss="modal">Yes</button>
                                      <?php } else {?>
                                       <button type="button" onclick="PdfImagesendd('<?php echo $x ?>')" data-toggle="modal" data-target="#exampleModalCenterdf<?php echo $x;?>" data-dismiss="modal"  class="updty updt">Yes</button>
                                     <?php }?> -->
                                  </div>
                               </div>
                            </form>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
            </div>

            <div class="modal fade" id="exampleModalCenterf<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="formmidaress modpassfull">
                                <div class="filldetails">
                                    <form method="post" action="<?php echo base_url();?>dashboard/scheduledListing">
                                        <div class="schedulejobgs">
                                            <h6>Schedule interview</h6>
                                            <p>Select Date & Time</p>
                                            <div class="forminputspswd">
                                                <p>Date</p>
                                                <input type="text" name="scheduledate" class="form-control datetimepicker1" id="datetimepickerr12<?php echo $featured['jobpost_id']?>" autocomplete="off" placeholder="mm/dd/yy" onchange="getdaynamee1('<?php echo $featured['jobpost_id']?>')" required="required">
                                                <span id="setval<?php echo $featured['jobpost_id']?>" style="color:red"></span>
                                                <input type="hidden" id="dayfrommm1<?php echo $featured['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayfrom'];?>">
                                                <input type="hidden" id="daytooo1<?php echo $featured['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayto'];?>">

                                                <input type="hidden" id="timefrommm1<?php echo $featured['jobpost_id']?>" value="<?php echo $timeFromm;?>">
                                                <input type="hidden" id="timetooo1<?php echo $featured['jobpost_id']?>" value="<?php echo $timeToo;?>">
                                                <i class="far fa-calendar-alt fieldicons"></i>
                                            </div>
                                            <div class="forminputspswd">
                                                <p>Time</p>
                                                <input type="text" name="scheduletime" autocomplete="off" class="form-control bootstrap-timepicker timepicker" onchange="getTimee1('<?php echo $featured['jobpost_id']?>')" id="datepickerr13<?php echo $featured['jobpost_id']?>" data-format="hh:mm:ss" required="required">
                                                <span id="setvall123<?php echo $featured['jobpost_id']?>" style="color:red"></span>
                                                <i class="far fa-clock"></i>
                                            </div>
                                            <div class="statsusdd">
                                                <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                                                <input type="hidden" name="listing" value="<?php echo $featured['jobpost_id'];?>">
                                                <input type="hidden" name="type" value="<?php echo $x;?>">
                                                <button type="submit" class="updt" id="scheduleesss<?php echo $featured['jobpost_id']?>" disabled>Schedule & Apply</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- <div class="modal fade" id="exampleModalCenterdf<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="formmidaress modpassfull">
                                <div class="filldetails">
                                    <form method="post" action="<?php echo base_url();?>dashboard/scheduledListing">
                                        <div class="schedulejobgs">
                                            <h6>Schedule interview</h6>
                                            <p>Select Date & Time</p>
                                            <div class="forminputspswd">
                                                <p>Date</p>
                                                <input type="text" name="scheduledate" class="form-control datetimepicker1" id="datetimepicker12<?php echo $featured['jobpost_id']?>" placeholder="mm/dd/yy" onchange="getdaynamee('<?php echo $featured['jobpost_id']?>')" required="required" autocomplete="off">
                                                <input type="hidden" id="dayfrommm<?php echo $featured['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayfrom'];?>">
                                                <input type="hidden" id="daytooo<?php echo $featured['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayto'];?>">

                                                <input type="hidden" id="timefrommm<?php echo $featured['jobpost_id']?>" value="<?php echo $timeFromm;?>">
                                                <input type="hidden" id="timetooo<?php echo $featured['jobpost_id']?>" value="<?php echo $timeToo;?>">
                                                <span id="setval12<?php echo $featured['jobpost_id']?>" style="color:red"></span>
                                                <i class="far fa-calendar-alt fieldicons"></i>
                                            </div>
                                            <div class="forminputspswd">
                                                <p>Time</p>
                                                <input type="text" name="scheduletime" autocomplete="off" class="form-control bootstrap-timepicker timepicker" onchange="getTimee('<?php echo $featured['jobpost_id']?>')" id="datepicker13<?php echo $featured['jobpost_id']?>" data-format="hh:mm:ss" required="required">
                                                <span id="setvall12<?php echo $featured['jobpost_id']?>" style="color:red"></span>
                                                <i class="far fa-clock"></i>
                                            </div>
                                            <div class="statsusdd">
                                                <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                                                <input type="hidden" name="listing" value="<?php echo $featured['jobpost_id'];?>">
                                                <input type="hidden" name="type" value="<?php echo $x;?>">
                                                <button type="submit" class="updt" id="schedulees<?php echo $featured['jobpost_id']?>" disabled>Schedule & Apply</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->

  <?php

  $x++;

  }
}}}
?>

<?php

if(!empty($featuredJob)){
 $x=30;
        
    foreach($featuredJob as $featured) {
       $jobDetails = $this->Jobpost_Model->job_detail_fetch($featured['jobpost_id']);
      $recruiterdetail = $this->Jobpost_Model->job_detailLocation_fetch($jobDetails[0]['company_id']);
      $timeFrom = $recruiterdetail[0]['from_time'];
      $timeFromm = date('H:i', strtotime($timeFrom));
      $timeTo   = $recruiterdetail[0]['to_time'];
      $timeToo = date('H:i', strtotime($timeTo));
      if(!empty($featured['toppicks1']) || !empty($featured['toppicks2']) || !empty($featured['toppicks3'])) {
      if($featured['toppicks1']=='3' || $featured['toppicks2']=='3' || $featured['toppicks3']=='3') {
  ?>

<div class="modal fade" id="hmoapplyModal<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
             <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                   <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                      </button>
                   </div>
                   <div class="modal-body">
                      <div class="formmidaress modpassfull">
                         <div class="filldetails">
                            <form> 
                               <div class="addupdatecent">
                                  <div class="profileupload">
                                   <img src="<?php echo base_url().'webfiles/';?>img/savedbighover.png">
                                   <!-- <input type="file" id="file1<?php echo $x;?>" title="" name="resumeFile" onchange='getFilenamee("<?php echo $x ?>")'> -->
                                   </div>
                                     <p id="setimgresp11<?php echo $x ?>"></p>
                                   <input type="hidden" id="imagedata11<?php echo $x ?>">
                                  <?php if (!empty($checkResume[0]['resume'])) { ?>
                                   <p>Would you like to update Your Resume?</p>
                                   <?php }else{?>
                                   <p>Please add your Resume</p>
                                   <?php }?>
                                  <div class="statsusdd">
                                     <p class="norm" class="close" data-toggle="modal" data-target="#exampleModalCenterch<?php echo $x;?>" data-dismiss="modal">No</p>
                                     <input type="hidden" name="type" value="<?php echo $x;?>">
                                     <div class="shwnbts">
                                     <input name="resumeFile" id="file1<?php echo $x;?>" type="file" class="yesuplds" onchange="getFilenamee('<?php echo $x ?>')">

                                     <?php if (!empty($checkResume[0]['resume'])) { ?>
                                      <p class="btns-yes">Yes</p>
                                      <?php }else{?>
                                       <p class="btns-yes">Upload</p>
                                      <?php }?>
                                      </div>
                                     <!-- <?php if(!empty($checkResume[0]['resume'])) {?>
                                         <button type="button" onclick="PdfImagesendh('<?php echo $x ?>')"  class="updty updt" data-toggle="modal" data-target="#exampleModalCenterdh<?php echo $x;?>" data-dismiss="modal">Yes</button>
                                      <?php } else {?>
                                       <button type="button" onclick="PdfImagesendh('<?php echo $x ?>')" data-toggle="modal" data-target="#exampleModalCenterdh<?php echo $x;?>" data-dismiss="modal"  class="updty updt">Yes</button>
                                     <?php }?> -->
                                  </div>
                               </div>
                            </form>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
            </div>

            <div class="modal fade" id="exampleModalCenterch<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="formmidaress modpassfull">
                                <div class="filldetails">
                                    <form method="post" action="<?php echo base_url();?>dashboard/scheduledListing">
                                        <div class="schedulejobgs">
                                            <h6>Schedule interview</h6>
                                            <p>Select Date & Time</p>
                                            <div class="forminputspswd">
                                                <p>Date</p>
                                                <input type="text" name="scheduledate" class="form-control datetimepicker1" id="datetimepickerr1<?php echo $featured['jobpost_id']?>" autocomplete="off" placeholder="mm/dd/yy" onchange="getdayname1('<?php echo $featured['jobpost_id']?>')" required="required">
                                                <span id="setval1<?php echo $featured['jobpost_id']?>" style="color:red"></span>
                                                <input type="hidden" id="dayfromm1<?php echo $featured['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayfrom'];?>">
                                                <input type="hidden" id="daytoo1<?php echo $featured['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayto'];?>">

                                                <input type="hidden" id="timefromm1<?php echo $featured['jobpost_id']?>" value="<?php echo $timeFromm;?>">
                                                <input type="hidden" id="timetoo1<?php echo $featured['jobpost_id']?>" value="<?php echo $timeToo;?>">
                                                <i class="far fa-calendar-alt fieldicons"></i>
                                            </div>
                                            <div class="forminputspswd">
                                                <p>Time</p>
                                                <input type="text" name="scheduletime" autocomplete="off" class="form-control bootstrap-timepicker timepicker" onchange="getTime1('<?php echo $featured['jobpost_id']?>')" id="datepickerr1<?php echo $featured['jobpost_id']?>" data-format="hh:mm:ss" required="required">
                                                <span id="setvall1<?php echo $featured['jobpost_id']?>" style="color:red"></span>
                                                <i class="far fa-clock"></i>
                                            </div>
                                            <div class="statsusdd">
                                                <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                                                <input type="hidden" name="listing" value="<?php echo $featured['jobpost_id'];?>">
                                                <input type="hidden" name="type" value="<?php echo $x;?>">
                                                <button type="submit" class="updt" id="schedule1<?php echo $featured['jobpost_id']?>" disabled>Schedule & Apply</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- <div class="modal fade" id="exampleModalCenterdh<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="formmidaress modpassfull">
                                <div class="filldetails">
                                    <form method="post" action="<?php echo base_url();?>dashboard/scheduledListing">
                                        <div class="schedulejobgs">
                                            <h6>Schedule interview</h6>
                                            <p>Select Date & Time</p>
                                            <div class="forminputspswd">
                                                <p>Date</p>
                                                <input type="text" name="scheduledate" class="form-control datetimepicker1" id="datetimepicker12<?php echo $featured['jobpost_id']?>" placeholder="mm/dd/yy" onchange="getdaynamee('<?php echo $featured['jobpost_id']?>')" required="required" autocomplete="off">
                                                <input type="hidden" id="dayfrommm<?php echo $featured['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayfrom'];?>">
                                                <input type="hidden" id="daytooo<?php echo $featured['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayto'];?>">

                                                <input type="hidden" id="timefrommm<?php echo $featured['jobpost_id']?>" value="<?php echo $timeFromm;?>">
                                                <input type="hidden" id="timetooo<?php echo $featured['jobpost_id']?>" value="<?php echo $timeToo;?>">
                                                <span id="setval12<?php echo $featured['jobpost_id']?>" style="color:red"></span>
                                                <i class="far fa-calendar-alt fieldicons"></i>
                                            </div>
                                            <div class="forminputspswd">
                                                <p>Time</p>
                                                <input type="text" name="scheduletime" autocomplete="off" class="form-control bootstrap-timepicker timepicker" onchange="getTimee('<?php echo $featured['jobpost_id']?>')" id="datepicker13<?php echo $featured['jobpost_id']?>" data-format="hh:mm:ss" required="required">
                                                <span id="setvall12<?php echo $featured['jobpost_id']?>" style="color:red"></span>
                                                <i class="far fa-clock"></i>
                                            </div>
                                            <div class="statsusdd">
                                                <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                                                <input type="hidden" name="listing" value="<?php echo $featured['jobpost_id'];?>">
                                                <input type="hidden" name="type" value="<?php echo $x;?>">
                                                <button type="submit" class="updt" id="schedulees<?php echo $featured['jobpost_id']?>" disabled>Schedule & Apply</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->

  <?php

  $x++;

  }
}}}
?>

<div class="modal fade" id="gpsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button> -->
         </div>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                  <form>
                     <div class="addupdatecent">
                        <p>JobYoDA requires access to location. To enjoy all that JobYoDA has to offer, turn on your GPS and give JobYoDA access to your location.</p>
                        <div class="statsusdd">
                           <button type="button" class="btn btn-success" id="allowGps">OK</button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</body>
<script>
$(".showsrchd").click(function (e) {
    e.stopPropagation();
    $(".homeheaders").toggleClass('opensidemenu');
});
</script> 


<script type="text/javascript">
 $('document').ready(function(){
    if(localStorage.getItem('test')){
            
    }else{


   if ("geolocation" in navigator){
       navigator.geolocation.getCurrentPosition(function(position){ 
         var currentLatitude = position.coords.latitude;
         var currentLongitude = position.coords.longitude;
         var latlng = new google.maps.LatLng(currentLatitude, currentLongitude);
           var geocoder = geocoder = new google.maps.Geocoder();
           geocoder.geocode({ 'latLng': latlng }, function (results, status) {
               if (status == google.maps.GeocoderStatus.OK) {
                   if (results[1]) {
                       var fill_address = results[1].formatted_address;
                      // window.location = "<?php echo base_url(); ?>user/index";
                       //$('#txtPlacess').val(fill_address);
                   }
               }
           });
         
         $('#cur_lat').val(currentLatitude);
         $('#cur_long').val(currentLongitude);
         $('#lat').val(currentLatitude);
         $('#long').val(currentLongitude);
         if (localStorage) {
          /*else{
           
          }*/
          localStorage.setItem('currentLatitude', currentLatitude);
          localStorage.setItem('currentLongitude', currentLongitude);
          localStorage.setItem('test','1');
           window.location="<?php base_url(); ?>";
         }
         
         

         /*if(currentLatitude.length > 0 && currentLongitude.length > 0) {*/
          //alert(currentLongitude);
            $.ajax({
                 type: "POST",
                 url: "<?php echo base_url(); ?>" + "user/fetch_location",
                 data: {currentLatitude:currentLatitude,currentLongitude:currentLongitude},
                 success:function(data) {
                    // alert(data);
                     console.log(data);
                     if(data) {
                       //alert(data);
                     } else {
                       //alert(data);
                     }
                 },
                 error:function(){
                   console.log('error');
                 }
             });
         /*}*/
       },showError);
       
     }
  }
     
$('#allowGps').on('click',function(){
  //alert('hi');
    window.location = "<?php echo base_url(); ?>user/index";
});

function showError(error) {
      switch(error.code) {
        case error.PERMISSION_DENIED:
          $('#gpsModal').modal('show');
          break;
        case error.POSITION_UNAVAILABLE:
          alert("Location information is unavailable.")
          break;
        case error.TIMEOUT:
          alert("The request to get user location timed out.")
          break;
        case error.UNKNOWN_ERROR:
          alert("An unknown error occurred.")
          break;
      }
    }

$('.rgtpos').click(function(){
      if ("geolocation" in navigator){
      navigator.geolocation.getCurrentPosition(function(position){ 
        var currentLatitudes = position.coords.latitude;
        var currentLongitudes = position.coords.longitude;
        var latlng = new google.maps.LatLng(currentLatitudes, currentLongitudes);
          var geocoder = geocoder = new google.maps.Geocoder();
          geocoder.geocode({ 'latLng': latlng }, function (results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                  if (results[1]) {
                      var fill_address = results[1].formatted_address;
                      $('#txtPlacess').val(fill_address);
                  }
              }
          });
        
        $('#lat').val(currentLatitudes);
        $('#long').val(currentLongitudes);
      });
    }
    });
    /*if(localStorage.getItem('currentLatitude')===null){
      alert('JobyoDA requires access to location. To enjoy all that JobyoDA has to offer, turn on your GPS and give JobyoDA access to your location.');
    }*/
 })
</script>

