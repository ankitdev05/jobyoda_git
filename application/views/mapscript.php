<script type="text/javascript">


 $('document').ready(function(){
    if(localStorage.getItem('lastactivity')) {

        var date1 = localStorage.getItem('lastactivity');

        console.log(localStorage.getItem('currentLatitude'));
        console.log(localStorage.getItem('currentLongitude'));

             var date2 = Date.now();
              if (date1 !== undefined) {
                  let difference = date2 - date1;
                  let diff = ((difference) / 1000).toFixed(0);
                
                  if (diff > 1000) {
                    localStorage.clear();

                      if ("geolocation" in navigator) {

                      navigator.geolocation.getCurrentPosition(function(position) { 
                        var currentLatitude = position.coords.latitude;
                        var currentLongitude = position.coords.longitude;
                        var latlng = new google.maps.LatLng(currentLatitude, currentLongitude);
                        var geocoder = geocoder = new google.maps.Geocoder();
                        geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                               
                           if (status == google.maps.GeocoderStatus.OK) {
                               if (results[1]) {
                                   var fill_address = results[1].formatted_address;
                               }
                           }
                        });
                       
                        $('#cur_lat').val(currentLatitude);
                        $('#cur_long').val(currentLongitude);
                        $('#lat').val(currentLatitude);
                        $('#long').val(currentLongitude);
                      
                        //if (localStorage) {

                          localStorage.setItem('currentLatitude', currentLatitude);
                          localStorage.setItem('currentLongitude', currentLongitude);
                          localStorage.setItem('test','1');
                          var date = Date.now();
                          localStorage.setItem("lastactivity", date);

                          $.ajax({
                             type: "POST",
                             url: "<?php echo base_url(); ?>" + "homepage/fetch_location",
                             data: {currentLatitude:currentLatitude,currentLongitude:currentLongitude},
                             success:function(data) {
                                //console.log(data);
                                location.reload();
                             },
                             error:function() {
                                //console.log("error in ajax");
                                location.reload();
                             }
                          });
                          
                          
                        //}
                      },showError);
                    }
                }
          }

      } else {

            if ("geolocation" in navigator) {

              navigator.geolocation.getCurrentPosition(function(position) { 

                var currentLatitude = position.coords.latitude;
                var currentLongitude = position.coords.longitude;
                var latlng = new google.maps.LatLng(currentLatitude, currentLongitude);
                var geocoder = geocoder = new google.maps.Geocoder();
                geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                       
                   if (status == google.maps.GeocoderStatus.OK) {
                       if (results[1]) {
                           var fill_address = results[1].formatted_address;
                       }
                   }
                });

                //alert(currentLatitude);
               
                $('#cur_lat').val(currentLatitude);
                $('#cur_long').val(currentLongitude);
                $('#lat').val(currentLatitude);
                $('#long').val(currentLongitude);
              
                //if (localStorage) {

                  localStorage.setItem('currentLatitude', currentLatitude);
                  localStorage.setItem('currentLongitude', currentLongitude);
                  localStorage.setItem('test','1');
                  var date = Date.now();
                  localStorage.setItem("lastactivity", date);

                  $.ajax({
                     type: "POST",
                     url: "<?php echo base_url(); ?>" + "homepage/fetch_location",
                     data: {currentLatitude:currentLatitude,currentLongitude:currentLongitude},
                     success:function(data) {
                         //console.log(data);
                         location.reload();
                     },
                     error:function(){
                        //console.log('error');
                        location.reload();
                     }
                  });
               
              },showError);
            }
        }
      

        function showError(error) {
          switch(error.code) {
            case error.PERMISSION_DENIED:
              $('#gpsModal').modal('show');
              break;
            case error.POSITION_UNAVAILABLE:
              alert("Location information is unavailable.")
              break;
            case error.TIMEOUT:
              alert("The request to get user location timed out.")
              break;
            case error.UNKNOWN_ERROR:
              alert("An unknown error occurred.")
              break;
          }
        }

    $('.rgtpos').click(function(){
      if ("geolocation" in navigator){
        navigator.geolocation.getCurrentPosition(function(position){ 
          var currentLatitudes = position.coords.latitude;
          var currentLongitudes = position.coords.longitude;
          var latlng = new google.maps.LatLng(currentLatitudes, currentLongitudes);
            var geocoder = geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        var fill_address = results[1].formatted_address;
                        $('#txtPlacess').val(fill_address);
                    }
                }
            });
          
          $('#lat').val(currentLatitudes);
          $('#long').val(currentLongitudes);
        });
      }
    });
 })
</script>


<?php 
if($this->session->userdata('userfsess') != null) {
       $userfsess = $this->session->userdata('userfsess');
       $name = $userfsess['name'];
       $name = explode(" ",$name);
       $fname = $name[0];
       $lname = $name[1];
?>
    <script type="text/javascript">
        var fname = '<?php echo $fname ?>';
        var lname = '<?php echo $lname ?>';
        var email = '<?php echo $userfsess['email']; ?>';
        
        $(window).on('load',function(){
           $('#exampleModalCenter5').modal('show');
           $('#fname').val(fname);
           $('#lname').val(lname);
           $('#email').val(email);
        });
    </script>
<?php
  }
?>

<script>

$(document).ready(function() {
  $("#allowGps").click(function (e) {
      location.reload(); 
  }); 
});

$(document).ready(function() {
  $("#allowGpsclose").click(function (e) {

      $.ajax({
         type: "POST",
         url: "<?php echo base_url(); ?>" + "homepage/fetch_location",
         data: {currentLatitude:'14.5676285',currentLongitude:'120.7397551'},
         success:function(data) {
             
             localStorage.setItem('currentLatitude', '14.5676285');
              localStorage.setItem('currentLongitude', '120.7397551');
              localStorage.setItem('test','1');
              var date = Date.now();
              localStorage.setItem("lastactivity", date);

            $("#gpsModal").modal("hide");
         },
         error:function(){
            
            location.reload();
         }
      });

  }); 
});

$(".bcnormals").click(function (e) {
    $(".yodserchfilts").removeClass('openyodsrch');
}); 
</script>

<?php if($this->session->tempdata('homeerr')){?>
  <script type="text/javascript">
    $(".yodserchfilts").addClass('openyodsrch');
  </script>
<?php }?>


<?php $requestURI = $_SERVER['REQUEST_URI'];?>

<?php if($requestURI == "/") { ?>
<script type="text/javascript">
  $(function () {
     $(".datetimepicker1").datepicker({ 
       dateFormat: "yy-mm-dd", 
       autoclose: true,
       minDate: +1
      })
  });
  $('.timepicker').timepicker({ 'timeFormat': 'H:i' });
</script>
<?php } ?>

<script type="text/javascript">
   $(document).ready(function() { 
      
      $('.loctrack').click(function(){

        if ("geolocation" in navigator){
          navigator.geolocation.getCurrentPosition(function(position){ 
            var currentLatitudes = position.coords.latitude;
            var currentLongitudes = position.coords.longitude;
            var geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(currentLatitudes, currentLongitudes);
            
            if(geocoder) {
              geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                  if (status == google.maps.GeocoderStatus.OK) {
                      //console.log(results[1].address_components);
                      if (results[1]) {
                          var fill_address = results[1].formatted_address;
                          $('#location').val(fill_address);

                          for(var j=0;j<results[1].address_components.length;j++) {

                              //console.log(results[1].address_components[j]);

                              if(results[1].address_components[j]['types'][0] == 'administrative_area_level_1')
                              {
                                var state = results[1].address_components[j]['long_name'];
                              }
                              if(results[1].address_components[j]['types'][0] == 'locality')
                              {
                                var city = results[1].address_components[j]['long_name'];
                              }
                          }

                          $('#cityii').val(city);
                          $('#getcityMap').val(city);
                          $('#getStateMap').val(state);
                      }
                  }
              });
            
              $('#lati').val(currentLatitudes);
              $('#longi').val(currentLongitudes);
            }
          });
        }
      });

      $("#signupjob").click(function() {
         var form = $("#form").serialize();
          $.ajax({
              type: "POST",
              url: "<?php echo base_url(); ?>" + "login",
              data: {form:form},
              cache:false,
              success:function(data) {
                console.log(data);
                if(length > 9) {
                  return false;
                }
              }
          });
      });
  });
</script>

<?php if($requestURI == "/") { ?>
<script>
   $("#fname").keypress(function(e){
   if (window.event)
       code = e.keyCode;
   else
       code = e.which;
   if(code == 32 || (code>=97 && code<=122)|| (code>=65 && code<=90))
       return true;
   else
       return false;
   });
   
   $("#lname").keypress(function(e){
   if (window.event)
       code = e.keyCode;
   else
       code = e.which;
   if(code == 32 || (code>=97 && code<=122)|| (code>=65 && code<=90))
       return true;
   else
       return false;
   });
</script>

<script>
  $(document).ready(function () {
    $("#phone").keypress(function (e){
      if(e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
         return false;
      }
    });
  });
</script>
<script>
   function checksession() {
      $("#exampleModalCenter4").modal('show');
   }
</script>

<?php } ?>

<script>
  function selectCountry(val) {
    $(".inputDetails").css("display","none");
    $("#myInput").val(val);
  }
</script>
<script>
  function initMap() {}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk&libraries=places&callback=initMap"></script>


<script>
   function onchangecity(id) {

      var geocoder = new google.maps.Geocoder();
      var address = id + " philippines";
      geocoder.geocode( { 'address': address}, function(results, status) {

      if (status == google.maps.GeocoderStatus.OK) {
          //console.log(results[0].geometry.location);
          var latitude = results[0].geometry.location.lat();
          var longitude = results[0].geometry.location.lng();
          //alert(latitude);
            $("#lati").val(latitude);
            $("#longi").val(longitude);
          } 
      }); 

      $("#txtPlaces").val(id);
      $(".locationDetail").css("display","none");
   }

   function onchangecategory(id) {
     $("#myInput").val(id);
      $(".searhDrop").css("display","none"); 
   }
   function onchangejobtype(id) {
     $("#myInput").val(id);
     $(".searhDrop").css("display","none"); 
   }
   function onchangecompanytype(id) {
     $("#myInput").val(id);
      $('html, body').animate({
        scrollTop: $(".AdvertisementArea").offset().top
      }, 2000);

      $(".searhDrop").css("display","none"); 
   }
</script>
    <!-- <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk&callback=initMap">
    </script> -->
     
<script type="text/javascript">
   google.maps.event.addDomListener(window, 'load', function () {
      var places = new google.maps.places.Autocomplete(document.getElementById('txtPlaces'));
    
       google.maps.event.addListener(places, 'place_changed', function () {
         
           var place = places.getPlace();
           var address = place.formatted_address;
           var latitude = place.geometry.location.A;
           var longitude = place.geometry.location.F;
           $('#lati').val(place.geometry.location.lat());
           $('#longi').val(place.geometry.location.lng());
           $('#cityi').val(place.name);
           var mesg = "Address: " + address;
           mesg += "\nLatitude: " + latitude;
           mesg += "\nLongitude: " + longitude;
       });
   });
</script>

<script type="text/javascript">
   google.maps.event.addDomListener(window, 'load', function () {
      var places1 = new google.maps.places.Autocomplete(document.getElementById('location'));
    
       google.maps.event.addListener(places1, 'place_changed', function () {
         
           var place = places1.getPlace();
           
           var address = place.formatted_address;
           var latitude = place.geometry.location.A;
           var longitude = place.geometry.location.F;
           $('#latii').val(place.geometry.location.lat());
           $('#longii').val(place.geometry.location.lng());

           var addressComponent = place.address_components;
            
           for(var j=0;j<addressComponent.length;j++) {

              if(addressComponent[j]['types'][0] == 'administrative_area_level_1')
              {
                var state = addressComponent[j]['long_name'];
              }
              if(addressComponent[j]['types'][0] == 'locality')
              {
                var city = addressComponent[j]['long_name'];
              }
            }

            $('#cityii').val(city);
            $('#getcityMap').val(city);
            $('#getStateMap').val(state);

           var mesg = "Address: " + address;
           mesg += "\nLatitude: " + latitude;
           mesg += "\nLongitude: " + longitude;
       });
   });
</script>

<script type="text/javascript">
   google.maps.event.addDomListener(window, 'load', function () {
      var places = new google.maps.places.Autocomplete(document.getElementById('txtPlacess'));
       google.maps.event.addListener(places, 'place_changed', function () {
           var place = places.getPlace();
           var address = place.formatted_address;
           var latitude = place.geometry.location.A;
           var longitude = place.geometry.location.F;
           var mesg = "Address: " + address;
           $('#lat').val(place.geometry.location.lat());
           $('#long').val(place.geometry.location.lng());
           mesg += "\nLatitude: " + latitude;
           mesg += "\nLongitude: " + longitude;
       });
   });
</script>

