
    <div class="JobListingBox">
    <?php 
        if(!empty($hotjobss)) {
            $x=1;
           foreach($hotjobss as $hotjobssData) {   

            if($hotjobssData['mode'] == "call" || $hotjobssData['mode'] == "Call") {
                $modeText = "OVER THE PHONE INTERVIEW";
            } else if($hotjobssData['mode'] == "Walk-in") {
                $modeText = "WALK IN INTERVIEW";
            } else if($hotjobssData['mode'] == "Instant screening") {
                $modeText = "INSTANT SCREENING";
            }
    ?>
        <aside class="lazy11">
            <?php
                    if($this->session->userdata('usersess')) {
                        if($hotjobssData['savedjob']==1) {
                            $title="Saved";
                ?>
                            <span id="test3<?php echo $hotjobssData['jobpost_id'];?>" onclick="savedjob('<?php echo $hotjobssData['jobpost_id']?>')" class="Wishlist"> <i class="fa fa-heart" title="<?php echo $title;?>"></i> </span>
                <?php
                        } if($hotjobssData['savedjob']==0) {
                ?>
                            <span id="test4<?php echo $hotjobssData['jobpost_id'];?>" onclick="savedjob('<?php echo $hotjobssData['jobpost_id']?>')" class="Wishlist"> <i class="fa fa-heart" title="<?php echo $title;?>" style="color:#b5b5b5;"></i> </span>    
                <?php
                        }
                    }
                ?>
            <figure>
                
                <?php 
                    if($hotjobssData['actively'] == 1) {
                ?>

                  <img src="<?php echo base_url().'webfiles/newone/images/Actively_Job.png';?>" style="position: absolute;top: 10px;left: 10px;width: 40px;">
                  
                <?php } ?>
                
                <a href="<?php  if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($hotjobssData['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($hotjobssData['jobpost_id']); }?>">
                <?php
                    if(!empty($hotjobssData['job_image'])) {
                ?>
                        <img src="<?php echo $hotjobssData['job_image'];?>">
                <?php
                    } else {
                ?>
                        <img src="<?php echo base_url().'webfiles/';?>img/user_man.png">
                <?php
                    }
                ?>
                    
                </a>
                
            </figure>
            <figcaption>
              
                <h1>
                    <a href="<?php  if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($hotjobssData['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($hotjobssData['jobpost_id']); }?>"><?php echo $hotjobssData['job_title'];?> | <span class="salaryColor"><?php echo $hotjobssData['salary']; ?> Monthly</span> | <?php echo $modeText; ?></a></a>
                </h1>
                
                <h2>
                    <a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($hotjobssData['comapnyId']);}else{ echo base_url();?>site_details/<?php echo base64_encode($hotjobssData['comapnyId']); }?>"><?php echo $hotjobssData['companyName']; ?></a>

                    <span class="Distance"><i class="fa fa-map-marker"></i> <?php echo $hotjobssData['distance'];?> KM</span>
                </h2>
                
                <h3>
                    <a href="<?php if (!empty($userSess)) { echo base_url();?>company_details/<?php echo base64_encode($hotjobssData['recruiter_id']);}else{ echo base_url();?>company_details/<?php echo base64_encode($hotjobssData['recruiter_id']); }?>"> <?php echo $hotjobssData['cname'];?></a>
                </h3>
                
                <h4>
                    <span><i class="fa fa-map" aria-hidden="true"></i></span>
                    <?php echo $hotjobssData['companyAddress']; ?>
                </h4>
                
                <h5>
                    <?php echo substr($hotjobssData['jobPitch'],0, 100); ?>
                </h5>

                <ul>
                    <?php
                        if(!empty($hotjobssData['toppicks1'])) {
                            echo $getToppickByFunction = getToppickFunction($hotjobssData['toppicks1']);
                        }
                        if(!empty($hotjobssData['toppicks2'])) {
                            echo $getToppickByFunction = getToppickFunction($hotjobssData['toppicks2']);
                        }
                        if(!empty($hotjobssData['toppicks3'])) {
                            echo $getToppickByFunction = getToppickFunction($hotjobssData['toppicks3']);
                        }
                    ?>
                </ul>
                
                <div class="newShareBtns">

                  <form id="formschedule<?php echo $hotjobssData['jobpost_id']?>">

                    <input type="hidden" id="dayfromm<?php echo $hotjobssData['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayfrom'];?>">
                    <input type="hidden" id="daytoo<?php echo $hotjobssData['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayto'];?>">
                    <input type="hidden" id="timefromm<?php echo $hotjobssData['jobpost_id']?>" value="<?php echo $timeFromm;?>">
                    <input type="hidden" id="timetoo<?php echo $hotjobssData['jobpost_id']?>" value="<?php echo $timeToo;?>">
                    <input type="hidden" name="scheduledate">
                    <input type="hidden" name="scheduletime"> 
                    <input type="hidden" name="listing" value="<?php echo $hotjobssData['jobpost_id'];?>">
                    <input type="hidden" name="type" value="<?php echo $x;?>">

                    <p class="demoP">

                      <a class="newbuttondesign lightgreen" href="<?php  if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($hotjobssData['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($hotjobssData['jobpost_id']); }?>"> View More Details  </a>
                      
                    <?php
                        if($this->session->userdata('usersess')) {

                            if($hotjobssData['mode'] == "Instant screening") {
                    ?>
                                <a href="javascript:void(0)" class="newbuttondesign orangecolor" data-toggle="modal" data-target="#exampleModalCenterInstant<?php echo $x;?>">Apply <?php if($hotjobssData['mode'] == "Call") { echo "for Phone Screening"; } else { echo 'for '. $hotjobssData['mode']; } ?>  </a>
                    <?php
                            } else {
                    ?>
                                <button type="button" class="ApplyJob newbuttondesign orangecolor" onclick='scheduleclick("<?php echo $hotjobssData['jobpost_id']?>", "<?php echo $x;?>")' id="schedule<?php echo $hotjobssData['jobpost_id']?>"> <?php if($hotjobssData['chatbot'] == 0) {  if($hotjobssData['mode'] == "Call") { echo "Apply for Phone Screening"; } else { echo 'Apply for '. $hotjobssData['mode']; } } else { echo "Continue Chatbot Interview"; } ?> </button>
                    <?php
                            }
                        } else {
                                        
                                          if($hotjobssData['mode'] == "Instant screening") {
                                  ?>
                                              <a href="<?php echo base_url();?>login" class="newbuttondesign orangecolor">Apply <?php if($hotjobssData['mode'] == "Call") { echo "for Phone Screening"; } else { echo 'for '. $hotjobssData['mode']; } ?>  </a>
                                  <?php
                                          } else {
                                  ?>
                                              <a href="<?php echo base_url();?>login" class="newbuttondesign orangecolor"><?php if($hotjobssData['chatbot'] == 0) {  if($hotjobssData['mode'] == "Call") { echo "Apply for Phone Screening"; } else { echo 'Apply for '. $hotjobssData['mode']; } } else { echo "Continue Chatbot Interview"; } ?> </a>
                                  <?php
                                          }
                                  
                                      }
                    ?>
                    </p>

                  </form>


                  <div class="sharebuttoncustom sharebuttoncustom2">
                      
                      <h6>
                          <span class="number"><?php echo $hotjobssData['sharecount'];?></span><span class="name">Shares</span><i class="fa fa-share-alt"></i></h6>

                      <div class="sharelinkclass sharelinkclass22">
                        <ul>
                            <li><a onclick='sharecountajax("<?php echo $hotjobssData['jobpost_id']; ?>", "<?php echo $sharelinks['facebook']; ?>")' href="javascript:void(0)" title="Facebook Share"><img src="<?php echo base_url();?>webfiles/newone/social/facebook.png"></a></li>

                            <li> <a onclick='sharecountajax("<?php echo $hotjobssData['jobpost_id']; ?>", "<?php echo $sharelinks['twitter']; ?>")' href="javascript:void(0)" title="Twitter Share"><img src="<?php echo base_url();?>webfiles/newone/social/twitter.png"></a></li>

                            <li> <a onclick='sharecountajax("<?php echo $hotjobssData['jobpost_id']; ?>", "<?php echo $sharelinks['linkedin']; ?>")' href="javascript:void(0)" title="LinkedIn Share"><img src="<?php echo base_url();?>webfiles/newone/social/linkedin.png"></a></li>

                            <li> <a onclick='sharecountajax("<?php echo $hotjobssData['jobpost_id']; ?>", "<?php echo $sharelinks['gmail']; ?>")' href="javascript:void(0)" title="Gmail Share"><img src="<?php echo base_url();?>webfiles/newone/social/gmail.png"></a></li>
                            
                            <li> <a onclick='sharecountajax("<?php echo $hotjobssData['jobpost_id']; ?>", "<?php echo $sharelinks['whatsapp']; ?>")' href="javascript:void(0)" title="Whatsapp Share"><img src="<?php echo base_url();?>webfiles/newone/social/whatsapp.png"></a></li>
                            
                        </ul>
                      </div>

                   </div>
                </div>
            </figcaption>
        </aside>
    <?php
        if($this->session->userdata('usersess')) {
           $recruiterdetail = $this->Jobpost_Model->job_detailLocation_fetch($hotjobssData['comapnyId']);
           $timeFrom = $recruiterdetail[0]['from_time'];
           $timeFromm = date('H:i', strtotime($timeFrom));
           $timeTo = $recruiterdetail[0]['to_time'];
           $timeToo = date('H:i', strtotime($timeTo));
    ?>
        <div class="modal fade" id="exampleModalCenterInstant<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
             <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content" style="box-shadow: none;min-height: 300px!important;">

                  <div class="InstantBox">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>

                      <h5>Job search made easier right!? You will now begin the Instant Assessment of <?php echo $hotjobssData['name']; ?>. They are looking for people like you but will only know about you once you finish the assessment. Those who finish the assessment quickly, are more likely to get hired! Good Luck from the JobYoDA team!</h5>
                                        <p> Best of luck from the JobYoDA team! </p>
                      
                      <p>
                        <a class="greenbtn" onclick="instantfunction('<?php echo $hotjobssData['jobpost_id']; ?>', '<?php echo $hotjobssData['modeurl']; ?>')" href="javascript:void(0)"> Begin Screening </a>
                      </p>
                  </div>
                
                </div>
             </div>
        </div>

       <div class="modal fade" id="exampleModalCenter100<?php echo $hotjobssData['jobpost_id']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                              <div class="modal-dialog modal-dialog-centered" role="document" style="width: 37%;">
                                 <div class="modal-content" style="min-height: 384px!important;">
                                    <div class="modal-header"> 
                                       <button type="button" class="close updt" data-dismiss="modal" aria-label="Close">
                                       <span aria-hidden="true">×</span>
                                       </button>
                                    </div>
                                    <div class="modal-body">
                                       <div class="formmidaress modpassfull">
                                          <div class="filldetails">
                                             <form>
                                                <div class="addupdatecent">
                                                   <p style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>All the best from the JobYoDA Team!</p> 
                                                  <p style='text-align:center;color:#fbaf31;line-height:28px;margin-bottom:0px;font-weight:500;'>Did you know JobYoDA offers a FREE Venti Coffee to all hires?</p> 
                                                  <p style='text-align:center;color:#fbaf31;line-height:28px;margin-bottom:0px;font-weight:500;'>To claim, all you need to do is:</p> 
                                                  <p style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>1. Declare JobYoDA as the source of your application</p>
                                                  <p style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>2. Send us and email at Help@jobyoda.com to claim your voucher if you get hired</p>
                                                  <p style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>3. We will validate with the recruiter and process</p>
                                                  <p style='text-align:center;color:#00a94f;;line-height:28px;margin-bottom:0px;font-weight:500;'>Good Luck!</p>
                                                   <div class="statsusdd"  style="display: inherit; float: none;">
                                                      <!-- <button type="button" class="updt" id="okup" data-dismiss="modal" aria-label="Close">Ok</button> -->

                                                      <?php if($hotjobssData['chatbot'] == 0) { ?>
                                                     <button  type="button" class="updt" id="okup" data-dismiss="modal" aria-label="Close">Ok</button>
                                                     <?php } else { ?>
                                                     <center style="margin-bottom: 0px"><a href="<?php echo base_url();?>chatbot/<?php echo base64_encode($hotjobssData['jobpost_id']);?>" style="text-decoration: none;" class=" chatbotlink" id="okup">Continue Chat Interview</a></center>
                                                     <?php } ?>

                                                   </div>
                                                </div>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>

                           <div class="modal fade" id="exampleModalCenter900<?php echo $hotjobssData['jobpost_id']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                              <div class="modal-dialog modal-dialog-centered" role="document">
                                 <div class="modal-content" style="min-height: 300px!important;">
                                    <div class="modal-header"> 
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                       <span aria-hidden="true">×</span>
                                       </button>
                                    </div>
                                    <div class="modal-body">
                                       <div class="formmidaress modpassfull">
                                          <div class="filldetails">
                                             <form>
                                                <div class="addupdatecent">
                                                   <img src="<?php echo base_url('webfiles/img/savedbighover.png')?>">
                                                   <p class="jobsuccess1<?php echo $hotjobssData['jobpost_id']?>"></p>
                                                   <div class="statsusdd">
                                                   </div>
                                                </div>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>

    <?php
        }
    ?>
    <?php
            $x++;
            }
        } else {
                      ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <center><img src="<?php echo base_url();?>webfiles/newone/images/32323.png" style="width:80%"></center>
                                </div>
                            </div>
                      <?php
              }
    ?>
    </div>

<?php
    function getToppickFunction($toppickID) {

        if($toppickID == 1) {
                                                            
            return '<li><figure><img src="'.base_url() .'recruiterfiles/images/m_bonus.png"></figure> Joining Bonus</li>';

        } else if($toppickID == 2) {

            return '<li><figure><img src="'.base_url() .'recruiterfiles/images/m_freefood.png"></figure> Free Food</li>';

        } else if($toppickID == 3) {

            return '<li><figure><img src="'.base_url() .'recruiterfiles/images/m_day_1_hmo.png"></figure> Day 1 HMO</li>';

        } else if($toppickID == 4) {

            return '<li><figure><img src="'.base_url() .'recruiterfiles/images/m_dependent_hmo.png"></figure> Day 1 HMO for Dependent</li>';

        } else if($toppickID == 5) {

            return '<li><figure><img src="'.base_url() .'recruiterfiles/images/m_dayshift.png"></figure> Day Shift</li>';

        } else if($toppickID == 6) {

            return '<li><figure><img src="'.base_url() .'recruiterfiles/images/m_14th_pay.png"></figure> 14th Month Pay </li>';
        
        } else if($toppickID == 7) {

            return '<li><figure><img src="'. base_url(). 'webfiles/newone/images/Searches-2.png"></figure> Work From Home </li>';
        
        } else {
            return "";
        }                                                 
    }
?>