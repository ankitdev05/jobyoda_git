<?php include_once('header2.php'); 
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {

    } else {
        $link = "https";
        $link .= "://";
        $link .= $_SERVER['HTTP_HOST'];
        $link .= $_SERVER['REQUEST_URI'];
        redirect($link);
    }
?>

<style>
  .text-danger {
      color: #a94442!important;
  }
  .text-success {
      color: #3c763d!important;
  }
</style>

<section>
   <div class="BannerArea" style="background-image: url('<?php echo base_url(); ?>webfiles/img/newmap.jpg');">
      <h1>Forgot Password  </h1>
   </div>
</section>

<section class="forgetPass">
   <div class="container">
      <div class="row">
         <div class="col-sm-6 col-md-6">
            <div class="bgForget"><center style="margin:0px;">
               <img src="<?php echo base_url(); ?>webfiles/newone/images/bgforget.svg" class="img-fluid" alt = "bgforget" style="width:80%;">
             </center>
            </div>
         </div>
         <div class="col-sm-6 col-md-6">
            <div class="forgetRight otpRight">
               <h2 class="head">Forgot Password</h2>
               <p>Please enter your email address to reset your account.</p>
               <div class="form-group formPost formPost2">
                  <label class="lab"></label>
                  <input type="text" class="form-control inputAll inputAll2" placeholder="Enter your Email Id" name="user_email" id="user_email">
                  <div class="preloader-set"></div>
                  <p id="errorfieldd" class="text-danger text-center"></p>
                  <p id="confrmMsg" class="text-success text-center"></p>
               </div>
               <div class="resetPass">
                  <button type="button" class="commonBtn1" id="forgetpass">Reset password</button>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>

<?php include_once('footer1.php'); ?>

<script>
    $(document).ready(function() {
        $("#forgetpass").click(function() {
            var user_email = $("#user_email").val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "user/forgetpassword",
                data: {
                    email: user_email
                },
                cache: false,
                beforeSend: function() {
                  $(".preloader-set").html('<div class="popularloader"><center><img src="https://jobyoda.com/webfiles/newone/loadericon.gif"></center></div>');
                },
                success: function(htmldata) {
                    htmldata = htmldata.trim();
                    if (htmldata == 'We have sent you a link and verification code to reset your password.') {
                        $('.preloader-set').css('display','none');
                        $('#confrmMsg').html(htmldata);
                        $('#user_email').attr('readonly', true);
                        $('.resetPass').css("display","none");
                        // setTimeout(function() {
                        //     $('#confrmMsg').css('display','none');
                        // }, 5000);

                    } else {
                        $(".preloader-set").css("display","none");
                        $('#errorfieldd').html(htmldata);
                    }
                },
                error: function() {
                    console.log('error');
                }
            });
        });
    });

</script>
