<?php include_once('header.php'); ?>
<style type="text/css">
  .InstantBox{
    padding: 30px;
}

.InstantBox button{
    position: absolute;
    top: -15px;
    right: -15px;
    width: 30px;
    height: 30px;
    background-color: #00a94f;
    opacity: 1;
    text-shadow: none;
    color: #fff;
    border-radius: 50%;
    font-size: 16px;
}

.InstantBox h5{
    font-family: Roboto;
    font-size: 14px;
    font-weight: 400;
    line-height: 25px;
    text-align: center;
    color: #000;
    margin: 0 0 10px 0;
}

.InstantBox h5 span{
    display: block;
    font-weight: 500;
}

.InstantBox p{
    margin: 0;
    text-align: center;
}

.InstantBox p a{
  background-color: #00a94f;
    color: #fff;
    padding: 7px 25px;
    border-radius: 5px;
    font-family: Roboto;
    font-size: 14px;
    display: inline-block;
    box-shadow: none;
}

.InstantBox p a:hover{
    background-color: #fbaf31;
}

.addupdatecent p {
  padding: 0 !important;
}

.applijoboader{
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    background-color: #ffffffa3;
    height: 100%;
    z-index: 999;
    text-align: center;
    display: none;
}
.sub_applijoboader {
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
}

#okup {
    border: 1px solid #ccc!important;
    padding: 3px 21px!important;
    background: #27aa60!important;
    color: #000!important;
    margin-top: 20px!important;
}

</style>

<div class="applijoboader">
  <div class="sub_applijoboader">
    <img src="https://jobyoda.com/webfiles/newone/loadericon.gif" style="width:100px">
  </div>
</div>

      <div class="managerpart showarbnd">
         <div class="container-fluid">
            <div class="">
              
               <div class="expanddiv srcflows jobflowsd listsjbs">
			   <div class="innerbglay">
                 
                  
                  <div class="">
                     
                     <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-home">
                           <div class="tabledivsdsn">
                              
                              <?php 
                            
                              if(!empty($hotjobss))
                              {   $x = 30;
                                  foreach($hotjobss as $hotjobssData)
                                  {
                                    if($hotjobss){
                                      $recruiterdetail = $this->Jobpost_Model->job_detailLocation_fetch($hotjobss[0]['comapnyId']);
                                     $timeFrom = $recruiterdetail[0]['from_time'];
                                     $timeFromm = date('H:i', strtotime($timeFrom));
                                     $timeTo   = $recruiterdetail[0]['to_time'];
                                     $timeToo = date('H:i', strtotime($timeTo));
                                    }
                              
                              ?>
							  <div class="featurerowjob">
                              <div class="row">
                                  <?php if(!empty($hotjobssData['jobpost_id'])) {?>
                                 <div class="col-md-3  col-lg-4 tumb">
                                   <!-- <h6>Applied Jobs</h6>-->
                                    <div class="imagethumbs">
                                         <?php 
                                         if(!empty($hotjobssData['job_image']))
                                         {
                                      ?>
                                        <a href="<?php echo base_url();?>job/description/<?php echo base64_encode($hotjobssData['jobpost_id']);?>">
										<div class="thumbimpr">
										<img src="<?php echo $hotjobssData['job_image'];?>" style="">
										</div>
										</a>
                                     <?php }
                                       else
                                       {
                                        ?>
                                       
                                        <a href="<?php echo base_url();?>job/description/<?php echo base64_encode($hotjobssData['jobpost_id']);?>">
										<div class="thumbimpr"><img src="<?php echo base_url().'webfiles/';?>img/thumpost.jpg"></div></a>
                                        <?php 
                                       }
                                     ?>
                                    </div>
                                 </div>
                                 <?php }?>
                                  <div class="col-md-9  col-lg-8 tumb">
                                  <div class="row" style="padding-right:15px;">
                                 <div class="col-md-6 col-lg-9">
                                    <div class="positionarea">
                                       <!--<h6>Position</h6>-->
                                       <a href="<?php echo base_url();?>job/description/<?php echo base64_encode($hotjobssData['jobpost_id']);?>">
                                     <!--  <a href="jobdescription.html">-->
                                          <p class="posttypes"><?php echo $hotjobssData['job_title'];?></p>
                                         
                                       </a>
                                       <a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($hotjobssData['comapnyId']);}else{ echo base_url();?>site_details/<?php echo base64_encode($hotjobssData['comapnyId']); }?>">

                                          <h3><?php echo $hotjobssData['companyName']; ?></h3>
                                       </a>
                                         
                                        <p class="posttypes dire">
                                          <a href="<?php if (!empty($userSess)) { echo base_url();?>company_details/<?php echo base64_encode($hotjobssData['recruiter_id']);}else{ echo base_url();?>company_details/<?php echo base64_encode($hotjobssData['recruiter_id']); }?>">
                                            <?php echo $hotjobssData['cname'];?>
                                          </a>
                                        </p>
										                    

                                        <?php if(!empty($hotjobssData['companyAddress'])) {?>
                                       <p class="locationtype"><img src="<?php echo base_url().'webfiles/';?>img/locmap.png"><?php echo $hotjobssData['companyAddress']; ?></p>

                                        <p class="" style="font-size:12px; display:none;"><?php echo $hotjobssData['jobPitch'];?></p>
                                      
                                       <?php }?>
									   
									   <div class="salicoftr"> 
                                        <?php
                                    if(!empty($hotjobssData['toppicks1']))
                                      {
                                       if($hotjobssData['toppicks1']=='1')
                                       {
                                       ?>
									   <div class="salry">
                                      <img title="Bonus" src="<?php echo base_url(); ?>recruiterfiles/images/m_bonus.png">
                                      <span>Joining Bonus</span>
									 </div>
                                       <?php 
                                           
                                       }
                                       
                                       if($hotjobssData['toppicks1']=='2')
                                       {
                                       ?>
									    
                                     <div class="salry">
                                      <img title="Free Food" src="<?php echo base_url(); ?>recruiterfiles/images/m_freefood.png">
                                      <span>Free Food</span>
									  </div>
                                       <?php }
                                        if($hotjobssData['toppicks1']=='3')
                                         {
                                       ?>
                                      
									  
									  <div class="salry">
                                      <img title="Day 1 HMO" src="<?php echo base_url(); ?>recruiterfiles/images/m_day_1_hmo.png">
                                       <span>Day 1 HMO</span>
									   </div>
                                        <?php }
                                         if($hotjobssData['toppicks1']=='4')
                                         {
                                        ?>
										
										
										<div class="salry">
                                           <img title="Day 1 HMO for Dependent" src="<?php echo base_url(); ?>recruiterfiles/images/m_dependent_hmo.png">
                                           <span>Day 1 HMO for Dependent</span>
										 </div>
                                      <?php }
                                        if($hotjobssData['toppicks1']=='5')
                                        {
                                          ?>
										  <div class="salry">
                                           <img title="Day Shift" src="<?php echo base_url(); ?>recruiterfiles/images/m_dayshift.png">
                                           <span>Day Shift</span>
										   </div>
                                        <?php
                                        }
                                        if($hotjobssData['toppicks1']=='6')
                                        {
                                        ?>
										<div class="salry">
                                         <img title="14th Month Pay" src="<?php echo base_url(); ?>recruiterfiles/images/m_14th_pay.png">
                                         <span>14th Month Pay</span>
										 </div>
                                        <?php
                                        }}


                                    if(!empty($hotjobssData['toppicks2']))
                                      {
                                       if($hotjobssData['toppicks2']=='1')
                                       {
                                       ?>
									   <div class="salry">
                                     <img title="Bonus" src="<?php echo base_url(); ?>recruiterfiles/images/m_bonus.png">
                                     <span>Joining Bonus</span>
									 </div>
                                       <?php 
                                           
                                       }
                                       
                                       if($hotjobssData['toppicks2']=='2')
                                       {
                                       ?>
                                     <div class="salry">
                                       <img title="Free Food" src="<?php echo base_url(); ?>recruiterfiles/images/m_freefood.png">
                                       <span>Free Food</span>
                                       </div>
                                       <?php }
                                        if($hotjobssData['toppicks2']=='3')
                                         {
                                       ?>
                                      <div class="salry">
                                      <img title="Day 1 HMO" src="<?php echo base_url(); ?>recruiterfiles/images/m_day_1_hmo.png">
                                      <span>Day 1 HMO</span>
                                      </div>
                                        <?php }
                                         if($hotjobssData['toppicks2']=='4')
                                         {
                                        ?>
                                        <div class="salry">
                                      <img title="Day 1 HMO for Dependent" src="<?php echo base_url(); ?>recruiterfiles/images/m_dependent_hmo.png">
                                      <span>Day 1 HMO for Dependent</span>
                                      </div>
                                      <?php }
                                        if($hotjobssData['toppicks2']=='5')
                                        {
                                          ?>
                                          <div class="salry">
                                          <img title="Day Shift" src="<?php echo base_url(); ?>recruiterfiles/images/m_dayshift.png">
                                          <span>Day Shift</span>
                                          </div>
                                        <?php
                                        }
                                        if($hotjobssData['toppicks2']=='6')
                                        {
                                        ?>
                                        <div class="salry">
                                        <img title="14th Month Pay" src="<?php echo base_url(); ?>recruiterfiles/images/m_14th_pay.png">
                                        <span>14th Month Pay</span>
                                        </div>
                                        <?php
                                        }}


                                    if(!empty($hotjobssData['toppicks3']))
                                      {
                                       if($hotjobssData['toppicks3']=='1')
                                       {
                                       ?>
                                       <div class="salry">
                                      <img title="Bonus" src="<?php echo base_url(); ?>recruiterfiles/images/m_bonus.png">
                                      <span>Joining Bonus</span>
                                      </div>
                                       <?php 
                                           
                                       }
                                       
                                       if($hotjobssData['toppicks3']=='2')
                                       {
                                       ?>
                                     <div class="salry">
                                      <img title="Free Food" src="<?php echo base_url(); ?>recruiterfiles/images/m_freefood.png">
                                       <span>Free Food</span>
                                       </div>
                                       <?php }
                                        if($hotjobssData['toppicks3']=='3')
                                         {
                                       ?>
                                      
                                      <div class="salry">
                                       <img title="Day 1 HMO" src="<?php echo base_url(); ?>recruiterfiles/images/m_day_1_hmo.png">
                                       <span>Day 1 HMO</span>
                                       </div>
                                        <?php }
                                         if($hotjobssData['toppicks3']=='4')
                                         {
                                        ?>
                                        <div class="salry">
                                       <img title="Day 1 HMO for Dependent" src="<?php echo base_url(); ?>recruiterfiles/images/m_dependent_hmo.png">
                                       <span>Day 1 HMO for Dependent</span>
                                      </div>
                                      <?php }
                                        if($hotjobssData['toppicks3']=='5')
                                        {
                                          ?>
                                          <div class="salry">
                                         <img title="Day Shift" src="<?php echo base_url(); ?>recruiterfiles/images/m_dayshift.png">
                                         <span>Day Shift</span>
                                         </div>
                                        <?php
                                        }
                                        if($hotjobssData['toppicks3']=='6')
                                        {
                                        ?>
                                        <div class="salry">
                                        <img title="14th Month Pay" src="<?php echo base_url(); ?>recruiterfiles/images/m_14th_pay.png">
                                        <span>14th Month Pay</span>
                                        </div>
                                        <?php
                                        }}
                                      ?>
                                    </div>
									                                        <p class="posttypes drdmn"><i class="fas fa-map-marked-alt"></i> <?php echo $hotjobssData['distance'];?>KM</p>
                                    </div>
                                    
                                 </div>
                             
                              
                                 <div class="col-md-9 col-lg-3 lefthalf appset ">
                                    <div class="salryedits">
									
										<p>
                                      <?php 
                                           if($hotjobssData['save_status']==1)
                                           {
                                            $title="Saved";
                                           ?>
                                          <span id="test3<?php echo $hotjobssData['jobpost_id'];?>"  onclick="savedjob('<?php echo $hotjobssData['jobpost_id']?>')">
                                          <i class="fa fa-heart" title="<?php echo $title;?>" style="color:red;font-size: 24px;cursor:pointer;"></i>
                                          
                                           
                                        </span>
                                           <?php 
                                           }
                                           if($hotjobssData['save_status']==0)
                                           {
                                             $title="Unsaved";
                                           
                                         ?>
                                     
                                         <span id="test4<?php echo $hotjobssData['jobpost_id'];?>" title="<?php echo $title;?>" onclick="savedjob('<?php echo $hotjobssData['jobpost_id']?>')">
                                          <i class="fa fa-heart" title="<?php echo $title;?>" style="color:#b5b5b5;font-size: 24px;cursor:pointer;"></i>

                                         

                                        </span>
                                      <?php }?>
                                                                          <span id="checkedstattus<?php echo $hotjobssData['jobpost_id'];?>" onclick="savedjob('<?php echo $hotjobssData['jobpost_id']?>')"></span>   
                                    </p>

                      

                                        
                                            
                                      <!-- <h6>Salary</h6>-->
                                     <?php  
                                     $mynumber=substr($hotjobssData['salary'], 0, 2);
                                     if(!empty($hotjobssData['salary'])){
                                      ?>
                                       <h4><?php echo $hotjobssData['salary']; ?>/Month</h4>
                                       <?php 
                                      }else{?>
                                      <h4>Salary confidential</h4>
                                     <?php }?>
									 
                                              
                                    </div>
                                 </div>
								 
								</div>
                                              <div class="newShareBtns newShareBtns22" style ="padding-right:15px;">
                          
                                   <form id="formschedule<?php echo $hotjobssData['jobpost_id']?>">

                            <input type="hidden" id="dayfromm<?php echo $hotjobssData['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayfrom'];?>">
                            <input type="hidden" id="daytoo<?php echo $hotjobssData['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayto'];?>">
                            <input type="hidden" id="timefromm<?php echo $hotjobssData['jobpost_id']?>" value="<?php echo $timeFromm;?>">
                            <input type="hidden" id="timetoo<?php echo $hotjobssData['jobpost_id']?>" value="<?php echo $timeToo;?>">
                            <input type="hidden" name="scheduledate">
                            <input type="hidden" name="scheduletime"> 
                            <input type="hidden" name="listing" value="<?php echo $hotjobssData['jobpost_id'];?>">
                            <input type="hidden" name="type" value="<?php echo $x;?>">

                          <?php 
                            if($hotjobssData['mode'] == "Instant screening") {
                          ?>
                              <button  type="button" class="greenbtn ApplyJob" data-toggle="modal" data-target="#exampleModalCenterInstant<?php echo $x;?>"> <?php if($hotjobssData['chatbot'] == 0) {  if($hotjobssData['mode'] == "Call") { echo "Apply for Phone Screening"; } else { echo 'Apply for '. $hotjobssData['mode']; } } else { echo "Continue Chatbot Interview"; }  ?> </button>
                          <?php
                            } else {
                          ?>
                               <button  type="button" class="greenbtn ApplyJob" onclick='scheduleclick("<?php echo $hotjobssData['jobpost_id']?>", "<?php echo $x;?>")' id="schedule<?php echo $hotjobssData['jobpost_id']?>"> <?php if($hotjobssData['chatbot'] == 0) {  if($hotjobssData['mode'] == "Call") { echo "Apply for Phone Screening"; } else { echo 'Apply for '. $hotjobssData['mode']; } } else { echo "Continue Chatbot Interview"; } ?> </button>
                          <?php 
                              $shareurl = base_url()."job/description/". base64_encode($hotjobssData['jobpost_id']);
                            } 
                          ?>

                          </form>



                                  <div class="sharebuttoncustom sharebuttoncustom2">
                                      
                                      <h6 class="yellow">
                                          <span class="number"><?php echo $jobListing['sharecount'];?></span><span class="name">Shares</span><i class="fa fa-share-alt"></i></h6>

                                      <div class="sharelinkclass sharelinkclass22">
                                        <ul>
                                            <li><a onclick='sharecountajax("<?php echo $jobListing['jobpost_id']; ?>", "<?php echo $sharelinks['facebook']; ?>")' href="javascript:void(0)" title="Facebook Share"><img src="<?php echo base_url();?>webfiles/newone/social/facebook.png"></a></li>

                                            <li> <a onclick='sharecountajax("<?php echo $jobListing['jobpost_id']; ?>", "<?php echo $sharelinks['twitter']; ?>")' href="javascript:void(0)" title="Twitter Share"><img src="<?php echo base_url();?>webfiles/newone/social/twitter.png"></a></li>

                                            <li> <a onclick='sharecountajax("<?php echo $jobListing['jobpost_id']; ?>", "<?php echo $sharelinks['linkedin']; ?>")' href="javascript:void(0)" title="LinkedIn Share"><img src="<?php echo base_url();?>webfiles/newone/social/linkedin.png"></a></li>

                                            <li> <a onclick='sharecountajax("<?php echo $jobListing['jobpost_id']; ?>", "<?php echo $sharelinks['gmail']; ?>")' href="javascript:void(0)" title="Gmail Share"><img src="<?php echo base_url();?>webfiles/newone/social/gmail.png"></a></li>
                                            
                                            <li> <a onclick='sharecountajax("<?php echo $jobListing['jobpost_id']; ?>", "<?php echo $sharelinks['whatsapp']; ?>")' href="javascript:void(0)" title="Whatsapp Share"><img src="<?php echo base_url();?>webfiles/newone/social/whatsapp.png"></a></li>
                                            
                                        </ul>
                                      </div>

                                   </div>
                                  </div>
                      
                                      </div>
                              </div>
							  </div>

                <div class="modal fade" id="exampleModalCenterInstant<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                           <div class="modal-content" style="box-shadow: none;">
                              
                              <div class="InstantBox">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                  </button>

                                  <h5>Job search made easier right!? You will now begin the Instant Assessment of <?php echo $comapnyDetail['name']; ?>. They are looking for people like you but will only know about you once you finish the assessment. Those who finish the assessment quickly, are more likely to get hired! Good Luck from the JobYoDA team!</h5>

                                  <p>
                                    <a class="greenbtn" onclick="instantfunction('<?php echo $hotjobssData['jobpost_id']; ?>', '<?php echo $hotjobssData['modeurl']; ?>')" href="javascript:void(0)"> Begin Screening </a>
                                  </p>
                              </div>
                           </div>
                        </div>
                     </div>
                     
                <div class="modal fade" id="exampleModalCenterb<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                           <div class="modal-dialog modal-dialog-centered" role="document">
                              <div class="modal-content">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                    </button>
                                 </div>
                                 <div class="modal-body">
                                    <div class="formmidaress modpassfull">
                                       <div class="filldetails">
                                             <div class="addupdatecent">
                                             <form method="post" action="<?php echo base_url();?>dashboard/resumeUploadListing" enctype="multipart/form-data">
                                             <div class="profileupload">
                                                <img src="<?php echo base_url().'webfiles/';?>img/savedbighover.png" style="width:50px; height:auto;">
                                                <!-- <input type="file"  class="resumeupdtf" id="file<?php echo $x;?>" title="" name="resumeFile" onchange='getFilename("<?php echo $x ?>")'> -->
                                             </div>  

                                                <?php if (!empty($checkResume[0]['resume'])) { ?>
                                                 <p>Got a few more minutes? Update your Profile and have a higher chance to get that Dream BPO job!</p>
                                                 <?php }else{?>
                                                 <p>Please add your Resume</p>
                                                 <?php }?>
                                                <div class="statsusdd">
                                    
                                                   <p class="norm" class="close" data-toggle="modal" data-target="#exampleModalCenterc<?php echo $x;?>" data-dismiss="modal">No</p>
                                                   <input type="hidden" name="type" value="<?php echo $x;?>">
                                                   <div class="shwnbts">
                                                   <input name="resumeFile" id="file<?php echo $x;?>" type="file" class="yesuplds" onchange="getFilename('<?php echo $x ?>')">

                                                   <?php if (!empty($checkResume[0]['resume'])) { ?>
                                                    <p class="btns-yes">Yes</p>
                                                    <?php }else{?>
                                                     <p class="btns-yes">Upload</p>
                                                    <?php }?>
                                                    </div>
                                                   <!-- <button type="button" onclick="PdfImagesend('<?php echo $x ?>')" data-toggle="modal" data-target="#exampleModalCenterdd<?php echo $x;?>" data-dismiss="modal"  class="updty updt">Yes</button> -->
                                                </div>
                                             </form>
                                             </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                                     <div class="modal fade" id="exampleModalCenterc<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                       <div class="modal-dialog modal-dialog-centered" role="document">
                                          <div class="modal-content">
                                             <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                                </button>
                                             </div>
                                             <div class="modal-body">
                                                <div class="formmidaress modpassfull">
                                                   <div class="filldetails">
                                                      <form  id="formschedule<?php echo $hotjobssData['jobpost_id']?>">
                                                         <div class="schedulejobgs">
                                                            <h6>Schedule interview</h6>
                                                            <!-- <p>Select Date & Time</p> -->
                                                            <p> Do you want to apply for this job? </p>
                                                            <div class="forminputspswd">
                                                               <!-- <p>Date</p>
                                              <input type="text" name="scheduledate" class="form-control datetimepicker1" id="datetimepicker1<?php //echo $hotjobssData['jobpost_id']?>" autocomplete="off" placeholder="mm/dd/yy" onchange="getdayname('<?php //echo $hotjobssData['jobpost_id']?>')"  required="required"> -->
                                              <span id="setval<?php echo $hotjobssData['jobpost_id']?>" style="color:red"></span>
                                              <input type="hidden" id="dayfromm<?php echo $hotjobssData['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayfrom'];?>">
                                                <input type="hidden" id="daytoo<?php echo $hotjobssData['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayto'];?>">

                                                <input type="hidden" id="timefromm<?php echo $hotjobssData['jobpost_id']?>" value="<?php echo $timeFromm;?>">
                                                <input type="hidden" id="timetoo<?php echo $hotjobssData['jobpost_id']?>" value="<?php echo $timeToo;?>">
                                             <!-- <i class="far fa-calendar-alt fieldicons"></i> -->
                                             <input type="hidden" name="scheduledate"> 
                                              </div>
                                            <div class="forminputspswd">
                                              <!-- <p>Time</p>
                                            <input type="text" name="scheduletime" autocomplete="off" class="form-control bootstrap-timepicker timepicker" onchange="getTime('<?php //echo $hotjobssData['jobpost_id']?>')" id="datepicker1<?php //echo $hotjobssData['jobpost_id']?>"  data-format="hh:mm:ss" required="required">  -->
                                             <!-- <span id="setvall<?php //echo $hotjobssData['jobpost_id']?>" style="color:red"></span>
                                             <i class="far fa-clock"></i> -->
                                             <input type="hidden" name="scheduletime">
                                                            </div>
                                              <div class="statsusdd">
                                                <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                                                <input type="hidden" name="listing" value="<?php echo $hotjobssData['jobpost_id'];?>">
                                                <input type="hidden" name="type" value="<?php echo $x;?>">
                                                 <button type="button" class="updt" onclick='scheduleclick("<?php echo $hotjobssData['jobpost_id']?>", "<?php echo $x;?>")' id="schedule<?php echo $hotjobssData['jobpost_id']?>">Apply Job</button>
                                              </div>
                                            </div>
                                          </form>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                                                                            
                                    <div class="modal fade" id="exampleModalCenterdd<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">×</span>
                                 </button>
                              </div>
                              <div class="modal-body">
                                 <div class="formmidaress modpassfull">
                                    <div class="filldetails">
                                       <form id="formschedulemore<?php echo $jobListing['jobpost_id']?>">
                                          <div class="schedulejobgs">
                                             <h6>Schedule interview</h6>
                                             <!-- <p>Select Date & Time</p> -->
                                             <p> Do you want to apply for this job? </p>
                                             <div class="forminputspswd">
                                                <!-- <p>Date</p>
                                                <input type="text" name="scheduledate" class="form-control datetimepicker1" id="datetimepicker12<?php //echo $jobListing['jobpost_id']?>" placeholder="mm/dd/yy" onchange="getdaynamee('<?php //echo $jobListing['jobpost_id']?>')"  required="required" autocomplete="off"> -->
                                                <input type="hidden" id="dayfrommm<?php echo $hotjobssData['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayfrom'];?>">
                                                <input type="hidden" id="daytooo<?php echo $hotjobssData['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayto'];?>">
                                                <input type="hidden" id="timefrommm<?php echo $hotjobssData['jobpost_id']?>" value="<?php echo $timeFromm;?>">
                                                <input type="hidden" id="timetooo<?php echo $hotjobssData['jobpost_id']?>" value="<?php echo $timeToo;?>">
                                                <span id="setval12<?php echo $hotjobssData['jobpost_id']?>" style="color:red"></span>
                                                <!-- <i class="far fa-calendar-alt fieldicons"></i> -->
                                                <input type="hidden" name="scheduledate"> 
                                             </div>
                                             <div class="forminputspswd">
                                                <!-- <p>Time</p>
                                                <input type="text" name="scheduletime" autocomplete="off" class="form-control bootstrap-timepicker timepicker" onchange="getTimee('<?php //echo $jobListing['jobpost_id']?>')" id="datepicker13<?php //echo $jobListing['jobpost_id']?>"  data-format="hh:mm:ss" required="required">  -->
                                                <!-- <span id="setvall12<?php //echo $jobListing['jobpost_id']?>" style="color:red"></span>
                                                <i class="far fa-clock"></i> -->
                                                <input type="hidden" name="scheduletime"> 
                                             </div>
                                             <div class="statsusdd">
                                                <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                                                <input type="hidden" name="listing" value="<?php echo $hotjobssData['jobpost_id'];?>">
                                                <input type="hidden" name="type" value="<?php echo $x;?>">
                                                <button type="button" onclick='scheduleclickmore("<?php echo $hotjobssData['jobpost_id']?>", "<?php echo $x;?>")' class="updt" id="schedulees<?php echo $hotjobssData['jobpost_id']?>">Apply Job</button>
                                             </div>
                                          </div>
                                       </form>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>


                     <div class="modal fade" id="exampleModalCenter900<?php echo $jobListing['jobpost_id']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                           <div class="modal-content">
                              <div class="modal-header"> 
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">×</span>
                                 </button>
                              </div>
                              <div class="modal-body">
                                 <div class="formmidaress modpassfull">
                                    <div class="filldetails">
                                       <form>
                                          <div class="addupdatecent">
                                             <img src="<?php echo base_url('webfiles/img/savedbighover.png')?>">
                                             <p class="jobsuccess1<?php echo $hotjobssData['jobpost_id']?>"></p>
                                             <div class="statsusdd">
                                             </div>
                                          </div>
                                       </form>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  

                         <?php $x++; }}?>
                              
                           </div>
                        </div>
                        <div class="tab-pane fade show active" id="nav-profile" style="display:none">
                           <div class="tabledivsdsn">
          
                             
                              
                           </div>
                        </div>

                     </div>
                  </div>
				  </div>
               </div>
         
            </div>
         </div>
      </div>
      
      
      
      
        
      <div class="modal fade" id="exampleModalCenterjob" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
         <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-header">
                   <h5 class="modal-title" id="exampleModalLongTitle">
                     <span id="savedjobmessage"></span>
                   </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               
               <div class="modal-body">
                  <div class="formmidaress modpassfull">
                 <div class="filldetails">
                 
                 </div>
              </div>
               </div>
            </div>
         </div>
      </div>

       <!-- Modal -->
 

 <div class="modal fade" id="exampleModalCenter100<?php echo $savedjobssData['jobpost_id']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document"  style="width: 37%;">
                           <div class="modal-content">
                              <div class="modal-header"> 
                                 <button type="button" class="close updt" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">×</span>
                                 </button>
                              </div>
                              <div class="modal-body">
                                 <div class="formmidaress modpassfull">
                                    <div class="filldetails">
                                       <form>
                                          <div class="addupdatecent">
                                             <p style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>All the best from the JobYoDA Team!</p> 
                                            <p style='text-align:center;color:#fbaf31;line-height:28px;margin-bottom:0px;font-weight:500;'>Did you know JobYoDA offers a FREE Venti Coffee to all hires?</p> 
                                            <p style='text-align:center;color:#fbaf31;line-height:28px;margin-bottom:0px;font-weight:500;'>To claim, all you need to do is:</p> 
                                            <p style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>1. Declare JobYoDA as the source of your application</p>
                                            <p style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>2. Send us and email at Help@jobyoda.com to claim your voucher if you get hired</p>
                                            <p style='text-align:center;line-height:28px;margin-bottom:0px;font-weight:500;'>3. We will validate with the recruiter and process</p>
                                            <p style='text-align:center;color:#00a94f;;line-height:28px;margin-bottom:0px;font-weight:500;'>Good Luck!</p>
                                            
                                             <div class="statsusdd">
                                                <div class="statsusdd"  style="display: inherit; float: none;">
                                          <!-- <button type="button" class="updt" id="okup" data-dismiss="modal" aria-label="Close">Ok</button> -->

                                          <?php if($hotjobssData['chatbot'] == 0) { ?>
                           <button  type="button" class="updt" id="okup" data-dismiss="modal" aria-label="Close">Ok</button>
                           <?php } else { ?>
                           <center style="margin-bottom: 0px"><a href="<?php echo base_url();?>chatbot/<?php echo base64_encode($hotjobssData['jobpost_id']);?>" style="text-decoration: none;" class=" chatbotlink" id="okup">Continue Chat Interview</a></center>
                           <?php } ?>
                                       </div>
                                             </div>
                                          </div>
                                       </form>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

  
</div>

<div id="snackbar"></div>
 
     
        <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> -->
     <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
   <!--   <script src="<?php echo base_url().'webfiles/';?>js/bootstrap-timepicker.js"></script>-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>recruiterfiles/css/jquery.timepicker.min.css" />
<script type="text/javascript" src="<?php echo base_url(); ?>recruiterfiles/js/jquery.timepicker.min.js"></script>
    
                <script type="text/javascript">
  function sharecountajax(jobid, url) {
    //console.log(url);

    console.log();
    $.ajax({
        type: 'POST',
        url: "<?php echo base_url(); ?>user/savesharecount",
        data: {job_id:jobid},
        success: function (result) {

            window.open(
              url,
              '_blank'
            );
        }
    });
  }
</script>
 
  <script>
    function savedjob(id)
       {
         
          $.ajax({
             'type' :'POST',
             'url' :"<?php echo base_url('dashboard/savedjovData') ?>",
             'data' :'jobId='+id,
             'success':function(htmlres)
             {
                //alert(htmlres);return false;  
                if(htmlres !='')
                {
                if(htmlres == 1)
                {
                   $("#test4"+id).hide();
                   $("#checkedstattus"+id).html('<i class="fa fa-heart" title="Saved" style="color:red;font-size: 24px;cursor:pointer;"></i>');
                  var msg="job saved successfully";
                  myFunction(msg);
                 
                  
                }
                if(htmlres == 2)
                {
                  $("#test3"+id).hide();
                  $("#checkedstattus"+id).html('<i class="fa fa-heart" title="Unsaved" style="color:#b5b5b5;font-size: 24px;cursor:pointer;"></i>');
                  var msg="job unsaved successfully";
              
                  myFunction(msg);
                   
                }
                 if(htmlres == 3)
                {
                  $("#test4"+id).hide();
                  $("#checkedstattus"+id).html('<i class="fa fa-heart" title="Saved" style="color:red;font-size: 24px;cursor:pointer;"></i>');
                  var msg="job saved successfully";
                  myFunction(msg);
                

                }
             }
             }
               
           });
       }

       function instantfunction(id, url) {
           $.ajax({
              'type' :'POST',
              'url' :"<?php echo base_url('dashboard/savedinstantData') ?>",
              'data' :'jobId='+id,
              'success':function(htmlres) {
                  window.open(url, '_blank');
              }
          });
    }


function myFunction(msg) {
     var x = document.getElementById("snackbar");
     x.className = "show";
     $("#snackbar").html('<div class="alert alert-success">'+msg+ '</div>');
     setTimeout(function(){ x.className = x.className.replace("show", "top"); }, 3000);
      }

      function getFilename(id)
       {
          var name = document.getElementById("file"+id).files[0].name;
          var filetype= $("#file"+id).val();
          var ext = filetype.split('.').pop();
          if(ext =="jpg" || ext =="jpeg" || ext =="png"){
            $("#setimgres"+id).html("<span style='color:red'>plese select file format(pdf/doc/docx)</span>");
            return false;
         } 
         else
         {
          $("#setimgres"+id).html("");
          var form_data = new FormData();
          form_data.append("file", document.getElementById('file'+id).files[0]);
          $.ajax({
            url:'<?php echo base_url('dashboard/imageUpload')?>',
            method:"POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            success:function(imagedata)
            { 
              $("#imagedata"+id).val(imagedata);
              $("#file"+id).val('');
              $("#setimgres"+id).html(imagedata);
              PdfImagesend(id);
            }
          });
         }
       }

       function PdfImagesend(id)
        {
          //alert('hi');
          var image= $("#imagedata"+id).val();
            $.ajax({
                   'type' :'POST',
                   'url' :"<?php echo base_url('dashboard/fileInsert') ?>",
                   'data' :'imagedata='+image,
                   'success':function(htmlres)
                   {

                     if(htmlres==1)
                     {
                      
                      $("#exampleModalCenterc"+id).modal('show');
                     }
                     if(htmlres==2)
                     {
                      
                       $("#exampleModalCenterc"+id).modal('show');
                     }
                   }
             });
         }
 </script>
   <script type="text/javascript">
         $(function () {
            $(".datetimepicker1").datepicker({ 
             dateFormat: "dd/mm/yy", 
              yearRange: '1900:2020', 
              defaultDate: '',
              autoclose: true,
           })
             $('.timepicker').timepicker({ 'timeFormat': 'H:i' });
         });
       
          
    </script>

<script>
  
  function scheduleclick(id, id1) {

    $(".applijoboader").css("display","block");

      var getform = "#formschedule" + id;
      var successmsg = ".jobsuccess"+id;
      var successmsg1 = ".jobsuccess1"+id;
      var successmodal = "#exampleModalCenter100"+id;
      var errormodal = "#exampleModalCenter900"+id;
      var closeModal = "#exampleModalCenterc"+id1;

      $.ajax({
          'type' :'POST',
          'url' :"<?php echo base_url('dashboard/scheduled') ?>",
          'data' :$(getform).serialize(),
          success:function(response) {

            $(".applijoboader").css("display","none");

            var jsonData = JSON.parse(response);
              
            if(jsonData.status == "SUCCESS") {

              if(jsonData.chatbot == 0) {
              
                  $(successmodal).modal('show');
                  $(closeModal).modal('hide');
              } else {
                window.location = "<?php echo base_url();?>chatbot/"+jsonData.jobid;    
              }

            } else {
                $(successmsg1).html(jsonData.message);
                $(errormodal).modal('show');
                $(closeModal).modal('hide');
            }
          }
      });
  }

  function scheduleclickmore(id, id1) {
      var getform = "#formschedulemore" + id;
      var successmsg = ".jobsuccess"+id;
      var successmsg1 = ".jobsuccess1"+id;
      var successmodal = "#exampleModalCenter100"+id;
      var errormodal = "#exampleModalCenter900"+id;
      var closeModal = "#exampleModalCenterc"+id1;

      $.ajax({
          'type' :'POST',
          'url' :"<?php echo base_url('dashboard/scheduled') ?>",
          'data' :$(getform).serialize(),
          success:function(response) {

            var jsonData = JSON.parse(response);
              
            if(jsonData.status == "SUCCESS") {

              if(jsonData.chatbot == 0) {
              
                  $(successmsg).html(jsonData.message);
                  $(successmodal).modal('show');
                  $(closeModal).modal('hide');

              } else {
                window.location = "<?php echo base_url();?>chatbot/"+jsonData.jobid;    
              }
              
            } else {
                $(successmsg1).html(jsonData.message);
                $(errormodal).modal('show');
                $(closeModal).modal('hide');
            }
          }
      });
  }

</script>
<script type="text/javascript">
   $(".updt").on("click",function(){
         window.location = "<?php echo base_url(); ?>appliedjobs";
   });
</script>

<script>
  function getdayname(id)
     {
      var getdate = $("#datetimepicker1"+id).val();
      var dayfrom = $("#dayfromm"+id).val();
      var dayto   = $("#daytoo"+id).val();
      
      $.ajax({
        'type' :'POST',
        'url' :"<?php echo base_url('dashboard/checkdateschedule') ?>",
        'data' :'dateValue='+getdate+'&jobId='+id,
        'success':function(htmlres) {
            htmlres = jQuery.parseJSON(htmlres);

            if(htmlres.status == "error") {

                $("#setval"+id).html(htmlres.message);
                $("#datetimepicker1"+id).val(''); 
                $('#schedule'+id).attr('disabled',true);

            } else {
                 $('#schedule'+id).attr('disabled',false);
                 $("#setval"+id).html("");
                 $("#datetimepicker1"+id).val(getdate);
            }
          }      
       });
      }
   </script>

<script>
    function getTime(id)
       {
          var gettime= $("#datepicker1"+id).val();
          var timefrom= $("#timefromm"+id).val();
          var timetoo= $("#timetoo"+id).val();
          //return false;
          $.ajax({
             'type' :'POST',
             'url' :"<?php echo base_url('dashboard/checktimeschedule') ?>",
             'data' :'timeValue='+gettime+'&jobId='+id,
             'success':function(htmlres)
             {
                if(htmlres !='')
                {
                if(htmlres == 1)
                {
                  $('#schedule'+id).attr('disabled',false);
                  $("#setvall"+id).html("");
                  $("#datepicker1"+id).val(gettime);
                }
                else
                {
                    //alert('Please Select Schedule time');
                   $("#setvall"+id).html("Please Select Schedule time between ("+timefrom+" to "+timetoo+")");
                   $("#datepicker1"+id).val(''); 
                    $('#schedule'+id).attr('disabled',true);
                }
             }
             }
               
           });
       }
 </script>

 <script>
  function getdaynamee(id)
     {

      var getdate= $("#datetimepicker12"+id).val();
      var dayfrom = $("#dayfrommm"+id).val();
      var dayto   = $("#daytooo"+id).val();

      $.ajax({
        'type' :'POST',
        'url' :"<?php echo base_url('dashboard/checkdateschedule') ?>",
        'data' :'dateValue='+getdate+'&jobId='+id,
        'success':function(htmlres) {
            htmlres = jQuery.parseJSON(htmlres);

            if(htmlres.status == "error") {

                $("#setval12"+id).html(htmlres.message);
                $("#datetimepicker12"+id).val(''); 
                $('#schedulees'+id).attr('disabled',true);

            } else {
                 $('#schedulees'+id).attr('disabled',false);
                 $("#setval12"+id).html("");
                 $("#datetimepicker12"+id).val(getdate);
            }
          }
               
       });
      }
   
   </script>

 <script>
    function getTimee(id)
       {
          var gettime= $("#datepicker13"+id).val();
          var timefrom= $("#timefrommm"+id).val();
          var timetoo= $("#timetooo"+id).val();
          $.ajax({
             'type' :'POST',
             'url' :"<?php echo base_url('dashboard/checktimeschedule') ?>",
             'data' :'timeValue='+gettime+'&jobId='+id,
             'success':function(htmlres)
             {
                if(htmlres !='')
                {
                if(htmlres == 1)
                {
                  $('#schedulees'+id).attr('disabled',false);
                  $("#setvall12"+id).html("");
                    $("#datepicker13"+id).val(gettime);
                }
                else
                {
                    //alert('Please Select Schedule time');
                   $("#setvall12"+id).html("Please Select Schedule time between ("+timefrom+" to "+timetoo+")");
                   $("#datepicker13"+id).val(''); 
                    $('#schedulees'+id).attr('disabled',true);
                }
             }
             }
               
           });
       }
 </script>
<?php include_once('footer.php'); ?>
<script>
$(document).on('click','.sharebuttoncustom h6',function(){
  $(this).siblings('.sharelinkclass').slideToggle();
});
</script>
<script>
document.addEventListener("mousedown", function (event) {
    if (event.target.closest(".sharebuttoncustom h6,.sharelinkclass"))
        return;
    $('.sharelinkclass').slideUp();
});
    </script>
   </body>
</html>

